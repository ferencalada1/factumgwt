package com.giga.factum.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BackupServlet extends HttpServlet {

	    private final String repository = "/backup/";

	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
			ServletContext servletContext = getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);	    	
	        String fileName = request.getParameter("fileName");

	        // Security: '..' in the filename will let sneaky users access files
	        // not in your repository.
	        //filename = filename.replace("..", "");
	        System.out.println("contextPath: "+contextPath);
	        File file = new File(contextPath+repository + fileName);
	        if (!file.exists())
	            throw new FileNotFoundException(file.getAbsolutePath());

	        response.setHeader("Content-Type","application/pdf");
	        response.setHeader("Content-Length", String.valueOf(file.length()));
	        response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");

	        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
	        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
	        byte[] buf = new byte[1024];
	        while (true) {
	            int length = bis.read(buf);
	            if (length == -1)
	                break;

	            bos.write(buf, 0, length);
	        }
	        bos.flush();
	        bos.close();
	        bis.close();
	    }
	}
