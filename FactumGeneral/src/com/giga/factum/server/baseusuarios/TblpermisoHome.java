package com.giga.factum.server.baseusuarios;
// Generated 16/01/2018 16:53:38 by Hibernate Tools 5.1.0.Alpha1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.server.DAOGenericoUsers;

/**
 * Home object for domain model class Tblpermiso.
 * @see baseusuarios.Tblpermiso
 * @author Hibernate Tools
 */
public class TblpermisoHome extends DAOGenericoUsers<Tblpermiso>{

	/*private static final Log log = LogFactory.getLog(TblpermisoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Tblpermiso transientInstance) {
		log.debug("persisting Tblpermiso instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Tblpermiso instance) {
		log.debug("attaching dirty Tblpermiso instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Tblpermiso instance) {
		log.debug("attaching clean Tblpermiso instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Tblpermiso persistentInstance) {
		log.debug("deleting Tblpermiso instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Tblpermiso merge(Tblpermiso detachedInstance) {
		log.debug("merging Tblpermiso instance");
		try {
			Tblpermiso result = (Tblpermiso) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Tblpermiso findById(int id) {
		log.debug("getting Tblpermiso instance with id: " + id);
		try {
			Tblpermiso instance = (Tblpermiso) sessionFactory.getCurrentSession().get("baseusuarios.Tblpermiso", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Tblpermiso instance) {
		log.debug("finding Tblpermiso instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("baseusuarios.Tblpermiso")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}*/
}
