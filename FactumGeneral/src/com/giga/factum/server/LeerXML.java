package com.giga.factum.server;
import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class LeerXML {
    public static void main(String args[]) {
        try {
        	File archivo = new File("D:/Workspace EGIT/FactumGeneral/war/WEB-INF/parametrosSistema.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document document = documentBuilder.parse(archivo);
            document.getDocumentElement().normalize();
            System.out.println("Elemento raiz:" + document.getDocumentElement().getNodeName());
        	ArrayList<String> tags = new ArrayList<String>();
            ArrayList<String> valores = new ArrayList<String>();
            NodeList listaParametros = document.getElementsByTagName("parametrosSistema");
            System.out.println("Numero Elemento:" +listaParametros.getLength() ); 
            //for (int i = 0; i < listaParametros.getLength(); i++) {
              Node node = listaParametros.item(0);
              //if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Element eElement = (Element) node;
               // if(eElement.hasChildNodes()) {
                  NodeList nl = node.getChildNodes();
                  for(int j=0; j<nl.getLength(); j++) {
                    Node nd = nl.item(j);
                    String tag=nd.getNodeName().trim();
                    String valor=nd.getTextContent().substring(0,nd.getTextContent().length()).trim();
                    System.out.print(nd.getTextContent());
                    //System.out.println(tag+": "+valor);
                    tags.add(tag+":"+valor);
                    //tags.add(tag);
                  }
                //}
              //}
           // }
           System.out.println(tags);
           for(int i=0;i<tags.size();i++){
           tags.remove(i);
           }
           System.out.println(tags);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}