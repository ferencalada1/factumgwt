package com.giga.factum.server;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import com.smartgwt.client.widgets.grid.ListGridRecord;

import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.UnderlineStyle;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import jxl.Cell;
import jxl.CellType;
import jxl.read.biff.BiffException;

public class CreateExel {
	
	
	private WritableCellFormat timesBoldUnderline;
	private WritableCellFormat times;
	private LinkedList<String []> listado=new LinkedList<String []>();
	private String inputFile;
        static List<String> Reporte=null;

    public CreateExel(){
    	
    }
    public void setOutputFile(String inputFile) {
    	this.inputFile = inputFile;
	}
    public void listgridToExel(LinkedList<String[]> listado)throws IOException, WriteException{
    	this.listado=listado;
    	File file = new File(inputFile);
        if(file.exists()){
        	file.delete();
        	file = new File(inputFile);
        	//throw new IOException(inputFile + "(El archivo ya existe)");
        }
            
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        workbook.createSheet("Reporte", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        createHead(excelSheet);
        createContent(excelSheet);
        workbook.write();
		workbook.close();
    }
	public void write() throws IOException, WriteException {
		File file = new File(inputFile);
               /* if(file.exists())
                    throw new IOException(inputFile + "(El archivo ya existe)");*/
		WorkbookSettings wbSettings = new WorkbookSettings();

		wbSettings.setLocale(new Locale("en", "EN"));
		WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
		workbook.createSheet("Reporte", 0);
		WritableSheet excelSheet = workbook.getSheet(0);
		createLabel(excelSheet);
		createContent(excelSheet);
		workbook.write();
		workbook.close();
	}
	private void createHead(WritableSheet sheet)throws WriteException{
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);
		// Create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(
				WritableFont.TIMES, 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);
		timesBoldUnderline.setBorder(Border.ALL, BorderLineStyle.DOUBLE);

		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		//cv.setAutosize(true);
        cv.setSize(200);
		// Creamos las cabeceras del reporte
        String[] atributos=listado.getFirst();
        for(int i=0;i<listado.getFirst().length;i++){
        	sheet.setColumnView(i, 15);
        	addCaption(sheet, i, 0, atributos[i]);
        }
    }
	private void createLabel(WritableSheet sheet)
			throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 14);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);
		// Create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(
				WritableFont.TIMES, 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);
			
		

		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		//cv.setAutosize(true);
                cv.setSize(200);

		// Creamos las cabeceras del reporte
		addCaption(sheet, 0, 0, "Núm. Orden");
		addCaption(sheet, 1, 0, "Cliente");
                addCaption(sheet, 2, 0, "Descripción Equipo");
                addCaption(sheet, 3, 0, "Nro. de Serie");
                addCaption(sheet, 4, 0, "Tipo de Orden");
                addCaption(sheet, 5, 0, "Estado");
                addCaption(sheet, 6, 0, "Ubicación");
                addCaption(sheet, 7, 0, "Informe Técnico");
                addCaption(sheet, 8, 0, "Técnico a Cargo");
                addCaption(sheet, 9, 0, "Fecha de Ingreso");
                addCaption(sheet, 10, 0, "Fecha de Diagnóstico");
                addCaption(sheet, 11, 0, "Fecha Prometida");
                addCaption(sheet, 12, 0, "Recibido por");
                addCaption(sheet, 13, 0, "Costo");
                addCaption(sheet, 14, 0, "Lugar de Entrega");


	}

	private void createContent(WritableSheet sheet) throws WriteException,
			RowsExceededException {
		int i =0;
		for(i=0;i<listado.size();i++){
			String[] fila=listado.get(i);		
			for(int j=0;j<fila.length;j++){
				System.out.print(fila[j]+" ");
				//HOJA      COLUMNA     FILA    TEXTO
				addLabel(sheet, j, i,fila[j]);
			}
			System.out.println(" ");
		}
	}

	private void addCaption(WritableSheet sheet, int column, int row, String s)
			throws RowsExceededException, WriteException {
		Label label;
		label = new Label(column, row, s, timesBoldUnderline);
		sheet.addCell(label);
	}

	private void addNumber(WritableSheet sheet, int column, int row,
			Integer integer) throws WriteException, RowsExceededException {
		Number number;
		number = new Number(column, row, integer, times);
		sheet.addCell(number);
	}

	private void addLabel(WritableSheet sheet, int column, int row, String s)
			throws WriteException, RowsExceededException {
		Label label;
		label = new Label(column, row, s, times);
		sheet.addCell(label);
	}
	public static String ExportarArchivo(LinkedList<String[]> listado,String nombre,String nombre1) {
        try{
        	CreateExel test = new CreateExel();
        	
            test.setOutputFile(nombre);//"C:/johny/lars.xls");
    		test.listgridToExel(listado);
			//test.write();
			//System.out.println("Please check the result file under c:/temp/lars.xls ");
            return (nombre1);
        }
        catch(Exception e){
            return ("Se ha producido un error: "+e.getLocalizedMessage());
        }
	}
	/*public static String ExportarArchivo(List<String> Listado, String path, String nombre) {  
        try{
        	CreateExel test = new CreateExel();
            Reporte = Listado;
            String ruta = path.trim() + nombre.trim();
    		test.setOutputFile("C:/Instaladores/lars.xls");//"C:/johny/lars.xls");
			test.write();
			//System.out.println("Please check the result file under c:/temp/lars.xls ");
            return "exito";
        }
        catch(Exception e){
            return ("Se ha producido un error: "+e.getLocalizedMessage());
        }
	}*/
	// Nueva funcion para leer excel

	  public void setInputFile(String inputFile) {
	    this.inputFile = inputFile;
	 //   String f = "C:/Exportarfact/";
	  }
	
	  public LinkedList <String []> read() throws IOException  {
		    LinkedList<String []> elemento = new LinkedList ();
		    //File inputWorkbook = new File("C:/Exportarfact.xls");
		    File inputWorkbook = new File("nuevo/"+inputFile);
		   System.out.println ("ruta:"+inputFile);
		   System.out.println ("root:"+inputWorkbook.getAbsolutePath());
		    Workbook w;
		    try {
		      w = Workbook.getWorkbook(inputWorkbook);
		      // Get the first sheet
		      Sheet sheet = w.getSheet(0);
		      // Loop over first 10 column and lines

		      for (int j = 0; j < sheet.getRows() ; j++) {
		        
		    	  String [] columna =  new String [sheet.getColumns()];
		    	  for (int i = 0; i < sheet.getColumns(); i++) {
		          Cell cell = sheet.getCell(i, j);
		          CellType type = cell.getType();
		          if (type == CellType.LABEL) {
		            System.out.println("I got a label "
		                + cell.getContents());
		                columna[i]= cell.getContents();
		            
		          }else {
		          System.out.println("Nada "
			                + cell.getContents());
		          }
		         
		        }
		    	  elemento.add (columna);
		      }
		    } catch (BiffException e) {
		      e.printStackTrace();
		    }
		    return elemento;
		  }
	
}
