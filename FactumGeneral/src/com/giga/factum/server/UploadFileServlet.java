package com.giga.factum.server;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.giga.factum.client.Factum;

public class UploadFileServlet extends HttpServlet {   
	//private static final String UPLOAD_DIRECTORY = "images"+File.separator+"catalogo";
	//private static final String UPLOAD_DIRECTORY = "/IMAGENES"+File.separator+"catalogo";
	private static final String UPLOAD_DIRECTORY = Factum.banderaImagenes;
	private static final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
	private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
	
	/*public void doPost(HttpServletRequest req,HttpServletResponse res){ 
    File uploadedFile;
    System.out.print("on server");
		try{    
		PrintWriter out=res.getWriter();    
		String contentType = req.getContentType();   
		int flag=0;   
		FileInputStream fis=null;   
		FileOutputStream fileOut=null;   
		if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0))   {   
			DataInputStream in = new DataInputStream(req.getInputStream());   
			int formDataLength = req.getContentLength();   
			byte dataBytes[] = new byte[formDataLength];   
			int byteRead = 0;   
			int totalBytesRead = 0;   
			while (totalBytesRead < formDataLength) {   
				byteRead = in.read(dataBytes, totalBytesRead,formDataLength);   
				totalBytesRead += byteRead;   
			}   
			res.setContentType("application/octet-stream");
			String file = new String(dataBytes);   
			//for saving the file name   
			String saveFile = file.substring(file.indexOf("filename=\"") + 10);   
			saveFile = saveFile.substring(0, saveFile.indexOf("\n"));   
			out.println("dataalength"+String.valueOf(formDataLength));
			out.println("savefiledddd"+saveFile);   
			int extension_save=saveFile.lastIndexOf("\"");   
			String extension_saveName=saveFile.substring(extension_save);   
			  
			//Here we are invoking the absolute path out of the encrypted data   
			  
			saveFile = saveFile.substring(saveFile.lastIndexOf("\\")+ 1,saveFile.indexOf("\""));   
			int lastIndex = contentType.lastIndexOf("=");   
			String boundary = contentType.substring(lastIndex + 1,contentType.length());   
			int pos;   
			  
			//extracting the index of file   
			pos = file.indexOf("filename=\"");   
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;   
			pos = file.indexOf("\n", pos) + 1;   
			int boundaryLocation = file.indexOf(boundary, pos) - 4;   
			int startPos = ((file.substring(0, pos)).getBytes()).length;   
			int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;   
			out.println("b,s,e="+String.valueOf(boundaryLocation)+","+String.valueOf(startPos)+","+String.valueOf(endPos));  
			out.println("savefile"+saveFile);   
			  
			int file_No=22;  
			ServletContext servletContext = getServletContext();
			String contextPath = servletContext.getRealPath(File.separator);
			uploadedFile=new File(contextPath+File.separator+"images"+File.separator+"catalogo"+File.separator);
		
			uploadedFile.mkdir();
		
		
		    String kk=uploadedFile.getAbsolutePath();
			  
		
	     	String pathname_dir=kk+File.separator+saveFile;   
			    
			File filepath=new File(pathname_dir);   
			out.println("filepath_  "+filepath);   
			fileOut = new FileOutputStream(filepath);   
			fileOut.write(dataBytes, startPos, (endPos - startPos));   
			fileOut.flush();   
			out.println("<h1> your files are saved</h1></body></html>");   
			out.close();   
			 
			File database_filename=new File(pathname_dir);   
	        fis=new FileInputStream(database_filename);   
	        flag=1;   
		}   
		  
			if(flag==1)   
			{   
			fileOut.close();   
			fis.close();   
			}   
		}catch(Exception e){   
			System.out.println("Exception Due to"+e);   
			e.printStackTrace();   
		}   
		}*/
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		PrintWriter out=response.getWriter();
//		response.setContentType("application/octet-stream");
//		if (!ServletFileUpload.isMultipartContent(request)) {
//            out.println("Request does not contain upload data");
//            out.flush();
//            return;
//        }
//		
//		
//		 DiskFileItemFactory factory = new DiskFileItemFactory();
//	        factory.setSizeThreshold(THRESHOLD_SIZE);
//	        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
//	         
//	        ServletFileUpload upload = new ServletFileUpload(factory);
//	        upload.setFileSizeMax(MAX_FILE_SIZE);
//	        upload.setSizeMax(MAX_REQUEST_SIZE);
//	         
//	        // constructs the directory path to store upload file
//	        String uploadPath = getServletContext().getRealPath("")
//	            + File.separator + UPLOAD_DIRECTORY;
//	        // creates the directory if it does not exist
//	        File uploadDir = new File(uploadPath);
//	        if (!uploadDir.exists()) {
//	            uploadDir.mkdir();
//	        }
//	         
//	        try {
//	            // parses the request's content to extract file data
//	            List formItems = upload.parseRequest(request);
//	            Iterator iter = formItems.iterator();
//	             
//	            // iterates over form's fields
//	            while (iter.hasNext()) {
//	                FileItem item = (FileItem) iter.next();
//	                // processes only fields that are not form fields
//	                if (!item.isFormField()) {
//	                    String fileName = new File(item.getName()).getName();
//	                    String filePath = uploadPath + File.separator + fileName;
//	                    File storeFile = new File(filePath);
//	                     
//	                    // saves the file on disk
//	                    item.write(storeFile);
//	                }
//	            }
//	            
//	            //response.setStatus(response.SC_ACCEPTED);
//	            out.println(uploadPath);
//	            out.println("Subido correctamente");
//	            out.close();
//	            //request.setAttribute("message", scriptOnFileUploadFinished.toString());
//	        } catch (Exception ex) {
//	            //request.setAttribute("message", "There was an error: " + ex.getMessage());
//	        }
//	        //getServletContext().getRequestDispatcher("/message.jsp").forward(request, response);
//	    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Post upload path file");

		PrintWriter out = response.getWriter();
		//response.setContentType("application/octet-stream");
		response.setContentType("text/html");
		if (!ServletFileUpload.isMultipartContent(request)) {
			out.println("Request does not contain upload data");
			out.flush();
			return;
		}

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);

		// constructs the directory path to store upload file
		String uploadPath = getServletContext().getRealPath("")
				//+ File.separator
		// + UPLOAD_DIRECTORY
		;
		
		System.out.println("ContextRealPath " + uploadPath);
		List formItems;
		try {
			formItems = upload.parseRequest(request);

			FileItem path = (FileItem) formItems.get(0);
			//uploadPath += path.getFieldName();
			String separador=Matcher.quoteReplacement(File.separator);
			System.out.println(path.getFieldName()+"  "+"\\"+" Separador:"+separador+" es "+separador.equals("\\/"));
			// separador=File.separator;
			String[] pathParte=uploadPath.split(separador);
			//System.out.println(path.getFieldName()+"  "+"\\"+" Separador:"+separador+" es "+separador.equals("\\/"));
			/*if(path.getFieldName().indexOf("\\")>=0) path.getFieldName().replaceAll("\\",separador);
			if(path.getFieldName().indexOf("\\/")>=0) path.getFieldName().replaceAll("\\/",separador);*/
			pathParte[pathParte.length-1]=FilenameUtils.separatorsToSystem(path.getFieldName());
			/*pathParte[pathParte.length-1]=(separador.equals("\\/"))?path.getFieldName().replaceAll("\\",separador):
				path.getFieldName().replaceAll("\\/",separador);*/
			System.out.println(pathParte[pathParte.length-1]+"  "+"\\"+" Separador:"+separador+"  "+FilenameUtils.separatorsToSystem(path.getFieldName()));
			uploadPath=String.join(separador, pathParte);
			File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
			FileItem imagn = (FileItem) formItems.get(1);
			if (!imagn.isFormField()) {
				System.out.println("FileItem "+imagn.getName());

				String fileName = new File(imagn.getName()).getName();
				String filePath = uploadPath + separador + fileName;
				File storeFile = new File(filePath);
				// saves the file on disk
				System.out.println("En servlet El archivo si existe "+filePath+" "+storeFile.exists());
				/*while (storeFile.exists()) {
					System.out.println("El archivo si existe ");
					storeFile.delete();
					System.out.println("Se elimino aun existe: "+storeFile.exists());
				}*/
				imagn.write(storeFile);
			}
			// creates the directory if it does not exist
			System.out.println(uploadPath);
			out.println("<script>");
	        out.println("alert('Subido correctamente');");
	        out.println("</script>");
			//out.println("Subido correctamente");
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getStackTrace());
			out.println("<script>");
	        out.println("alert('Error"+e.getMessage()+"');");
	        out.println("</script>");
			//out.println("Error: "+e.getMessage());
			e.printStackTrace(System.out);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getStackTrace());
			out.println("<script>");
	        out.println("alert('Error"+e.getMessage()+"');");
	        out.println("</script>");
			//out.println("Error: "+e.getMessage());
			e.printStackTrace(System.out);
			
		}
		out.close();
	}
	} 
