package com.giga.factum.server;


import java.awt.image.BufferedImage;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import org.hibernate.classic.Session;
import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.Contenedor;
import com.giga.factum.client.Factum;
import com.giga.factum.client.Factura;
import com.giga.factum.client.GreetingService;
import com.giga.factum.client.ProductoCliente;
import com.giga.factum.client.ProductoProveedor;
import com.giga.factum.client.ReporteRetencion;
import com.giga.factum.client.RetencionDetalle;
import com.giga.factum.client.User;
import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.BodegaProdDTO;
import com.giga.factum.client.DTO.CargoDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CoreDTO;
import com.giga.factum.client.DTO.CorreoRespaldoDTO;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.CuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.CuentaPlanCuentaIdDTO;
import com.giga.factum.client.DTO.DocumentoSaveDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.DtoelectronicoDTO;
import com.giga.factum.client.DTO.EmpleadoDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.EquipoDTO;
import com.giga.factum.client.DTO.EquipoOrdenDTO;
import com.giga.factum.client.DTO.EstablecimientoDTO;
import com.giga.factum.client.DTO.FacturaProduccionDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.MayorizacionDTO;
import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaId;
import com.giga.factum.client.DTO.MovimientoDTO;
import com.giga.factum.client.DTO.OrdenDTO;
import com.giga.factum.client.DTO.PedidoDTO;
import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.PosicionDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoElaboradoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.ProveedorDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.ReporteVentasEmpleadoDTO;
import com.giga.factum.client.DTO.ReportesDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.SerieDTO;
import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.DTO.TblpermisoDTO;
import com.giga.factum.client.DTO.TblpermisorolpestanaDTO;
import com.giga.factum.client.DTO.TblpermisorolpestanaIdDTO;
import com.giga.factum.client.DTO.TblpestanaDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.giga.factum.client.DTO.TblrolDTO;
import com.giga.factum.client.DTO.TbltipodocumentoDTO;
import com.giga.factum.client.DTO.TbltipoempresaDTO;
import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.TmpAuxgeneralDTO;
import com.giga.factum.client.DTO.TmpAuxresultadosDTO;
import com.giga.factum.client.DTO.TmpComprobacionDTO;
import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.DTO.detalleVentasATSDTO;
import com.giga.factum.server.base.*;
import com.giga.factum.server.baseusuarios.Tblpermiso;
import com.giga.factum.server.baseusuarios.Tblpermisorolpestana;
import com.giga.factum.server.baseusuarios.TblpermisorolpestanaHome;
import com.giga.factum.server.baseusuarios.TblpermisorolpestanaId;
import com.giga.factum.server.baseusuarios.Tblpestanas;
import com.giga.factum.server.baseusuarios.Tblrol;
import com.giga.factum.server.baseusuarios.TblrolHome;
import com.giga.factum.shared.FieldVerifier;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.itextpdf.text.DocumentException;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;

import org.apache.commons.fileupload.FileItem;
//import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {
	Iterator iterDetalles;
	private static String USER_SESSION;
	
	@Override
	public BodegaDTO buscarBodegaSegunNombre(String nombre) {
		TblbodegaHome accesobodega = new TblbodegaHome();
		Tblbodega tblbodega= new Tblbodega();
		BodegaDTO bodega= new BodegaDTO();
		bodega=null;
		String mensaje = "";
		try {
			try {
				System.out.println(nombre);
				tblbodega=accesobodega.findByNombre(nombre, "nombre");//(Integer.valueOf(idBodega));
				bodega=new BodegaDTO(tblbodega.getIdBodega(),tblbodega.getNombre()
						,tblbodega.getUbicacion(),tblbodega.getTelefono(),tblbodega.getIdEmpresa(),tblbodega.getEstablecimiento());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return bodega;
	}
	
	
/************** 	FUNCIONES PARA CONTROL DE SESIONES      *******************/
	
	private void setUserInSession(User usuario) {
        HttpSession session = getThreadLocalRequest().getSession();
        USER_SESSION = session.getId();
        usuario.setSessionID(session.getId());
        session.setAttribute(USER_SESSION, usuario);
        session.setMaxInactiveInterval(3600);//TIEMPO MAXIMO DE INACTIVIDAD ES 30 minutos
        //Guardamos los datos de ID, Nombre de usuario y mensaje en blanco "" en la tabla temporal de la base
    }
	
	public User getUserFromSession() {
        HttpSession session = getThreadLocalRequest().getSession(false);
        if (session != null) {
            return (User) session.getAttribute(session.getId());
        } else {
            return null;
        }
    }

	public String getSessionId() {
		
		TblservicioHome servHome= new TblservicioHome();
		Tblservicio servicio=servHome.findByCampo("factum", "nombre");
		Date hoy=fechaServidor();
		if (servicio!=null){
			 TblsuscripcionHome suscHome= new TblsuscripcionHome();
				Tblsuscripcion suscripcion=suscHome.findByCampo(String.valueOf(servicio.getIdServicio()), "id.idServicio");
				if (suscripcion!=null){
					 System.out.println("comparar fecha servidor suscripcion: "+hoy.compareTo(suscripcion.getFechaFin()));
					 System.out.println("estado activacion: "+suscripcion.getEstado()+" "+suscripcion.getEstado().equals("activate"));
					 if (suscripcion.getEstado().equals("activate") && hoy.compareTo(suscripcion.getFechaFin())<0){
				        HttpSession session = getThreadLocalRequest().getSession(false);
				        
				        
				        
				        javax.servlet.http.Cookie[] cookies=getThreadLocalRequest().getCookies();
				        javax.servlet.http.Cookie cookie;
				        String cookiesS="";
				        for (int i=0;i<cookies.length;i++){
				        	cookie=cookies[i];
				//        	Date now = new Date();
				//				long nowLong = now.getTime();
				//				nowLong = nowLong + (1000 * 60 * 60 * 8);//8 horas
				//				now.setTime(nowLong);
								System.out.println("Cookie "+cookie.getName()+": "+ cookie.getValue()+", "
								+ cookie.getDomain()+", "+cookie.getPath()+", "+cookie.getSecure());
				//        	Cookies.setCookie(cookie.getName(), cookie.getValue(), now, cookie.getDomain(),cookie.getPath(),cookie.getSecure());
								//Cookies.setCookie(cookie.getName(), cookie.getValue());
				        	cookiesS+=cookie.getName()+"="+cookie.getValue();
				        	if (i<cookies.length-1) cookiesS+="; ";
				        }
				//        Enumeration<String> namesEn =getThreadLocalRequest().getAttributeNames();
				//        String names="names= ";
				//        while (namesEn.hasMoreElements()){
				//        names+=", "+(String) namesEn.nextElement();
				//        }
				        if (session != null) {
				        	return cookiesS;
				//            return cookiesS+" "+session.getId()+" "+names;
				        } else {
				            return null;
				        }
					 }else{
						 suscripcion.setEstado("expired");
						 try {
							suscHome.modificar(suscripcion);
						} catch (ErrorAlGrabar e) {
							// TODO Auto-generated catch block
							e.printStackTrace(System.out);
							e.printStackTrace();
						}
						 return null;
					 }
				}else return null;
		}else return null;
    }
	
	public ArrayList<String> LeerXML() throws IllegalArgumentException, IOException{
		
		TblservicioHome servHome= new TblservicioHome();
		Tblservicio servicio=servHome.findByCampo("factum", "nombre");
		Date hoy=fechaServidor();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 
					 
		//ArrayList<String> tags = new ArrayList<String>();
		ArrayList<String> valores = new ArrayList<String>();
		//File archivo = new File("C:/Users/User/git/FACTUM2.1/FactumGeneral/war/WEB-INF/parametrosSistema.xml");
		String pathxml = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/WEB-INF/");
		File archivo = new File(pathxml+File.separatorChar+"/parametrosSistema.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = dbf.newDocumentBuilder();
		
		Document document = documentBuilder.parse(archivo);
		document.getDocumentElement().normalize();
		System.out.println("Elemento raiz:" + document.getDocumentElement().getNodeName());
		Node node=document.getChildNodes().item(0);
		NodeList nl = node.getChildNodes();
		for(int j=0; j<nl.getLength(); j++) {
			Node nd = nl.item(j);
			if (nd.getNodeType() == Node.ELEMENT_NODE) {
				String tag=nd.getNodeName().trim();
				String valor=nd.getTextContent().substring(0,nd.getTextContent().length()).trim();
				//System.out.print(nd.getTextContent());
				System.out.println(tag+": "+valor);
				if (valor.isEmpty() || valor.equals("") || valor.equals(null)){
					throw new IllegalArgumentException("Parametros de configuracion incorrectos");
				}

				valores.add(valor);
				//                    tags.add(tag);
			}
		}

		//System.out.println(valores);
		System.out.println("Tama�o:"+valores.size());
		/*for(int i=0;i<valores.size();i++){
        	   valores.remove(i);
           }*/
		System.out.println("Valores:"+valores);
		System.out.println("Tama�o:"+valores.size());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
			e.printStackTrace();
		}
		//System.out.println("Tama�o:"+valores);
		/*if (servicio!=null){
			 TblsuscripcionHome suscHome= new TblsuscripcionHome();
				Tblsuscripcion suscripcion=suscHome.findByCampo(String.valueOf(servicio.getIdServicio()), "id.idServicio");
				if (suscripcion!=null){
					 System.out.println("comparar fecha servidor suscripcion: "+hoy.compareTo(suscripcion.getFechaFin()));
					 System.out.println("estado activacion: "+suscripcion.getEstado()+" "+suscripcion.getEstado().equals("activate"));
					 if (suscripcion.getEstado().equals("activate") && hoy.compareTo(suscripcion.getFechaFin())<0){*/
						 return valores;
					 /*}else{
						 suscripcion.setEstado("expired");
						 try {
							suscHome.modificar(suscripcion);
						} catch (ErrorAlGrabar e) {
							// TODO Auto-generated catch block
							e.printStackTrace(System.out);
							e.printStackTrace();
						}
						 return null;
					 }
				}else return null;
		}else return null;*/
	}
    /**
     * FUNCION PARA TERMINAR LA SESION
     * @return
     */
	
	
    public List<TbltipodocumentoDTO> listarTiposDocumento(Integer ini, Integer fin){
		TbltipodocumentoHome acceso = new TbltipodocumentoHome(); 
		List<Tbltipodocumento> listado = null;
		List<TbltipodocumentoDTO> listadoDTO;
		//try {
			listado=acceso.getList();
			listadoDTO = new ArrayList<TbltipodocumentoDTO>();
            if (listado != null) {
                for (Tbltipodocumento TipoDoc : listado) {
                    listadoDTO.add(crearTipoDoc(TipoDoc));
                }
            }
		/*} catch (RuntimeException re) {
			throw re;
		}*/
		return listadoDTO;
	}
    public String cerrar_sesion(String NombreBD, String PasswordBD, String UsuarioBD, String SO,String PasswordComprimido, String destinoBackup) {   	
        try {
            HttpSession session = getThreadLocalRequest().getSession();
            session.invalidate();
            backupDB(true,NombreBD, PasswordBD,UsuarioBD, SO, PasswordComprimido, destinoBackup);
            //System.out.println("Valores de BACKUP----"+backupDB(true,Factum.banderaNombreBD, Factum.banderaPasswordBD, Factum.banderaUsuarioBD, Factum.banderaSO, Factum.banderaPasswordComprimido, Factum.destinoBackup));
            //backupDB(true);
            return "La sesion ha terminado";
        } catch (Exception e) {
            return "Se ha producido un error: /n" + e.getMessage();
        }
    }
    public TbltipodocumentoDTO crearTipoDoc(Tbltipodocumento tipoDoc){
		TbltipodocumentoDTO tipodocumentoDTO = new TbltipodocumentoDTO(tipoDoc.getTipoDoc(), tipoDoc.getIdPlan(), tipoDoc.getIdCuenta(), tipoDoc.getIdTipo());
		tipodocumentoDTO.setIdTipoDoc(tipoDoc.getIdTipoDoc());
		return tipodocumentoDTO;
	}
    public List<TbltipodocumentoDTO> TiposDocumento(Integer ini, Integer fin){
		TbltipodocumentoHome acceso = new TbltipodocumentoHome(); 
		List<Tbltipodocumento> listado = null;
		List<TbltipodocumentoDTO> listadoDTO;
		//try {
			listado=acceso.getList();
			listadoDTO = new ArrayList<TbltipodocumentoDTO>();
            if (listado != null) {
                for (Tbltipodocumento TipoDoc : listado) {
                    listadoDTO.add(crearTipoDoc(TipoDoc));
                }
            }
		/*} catch (RuntimeException re) {
			throw re;
		}*/
		return listadoDTO;
	}
    
    public User verificar_sesion() {
        HttpSession session = getThreadLocalRequest().getSession(false);
        if (session != null) {
            return (User) session.getAttribute(session.getId());
        } else {
            return null;
        }
    }
    
    /**
     * Esta funcion verifica en la base de datos si el usuario es un empleado, reciebiendo la CI y clave
     * @param username De tipo STRING es la Cedula del usuario
     * @param pass   De tipo STRING es la calve del usuario
     */
    public User checkLogin(String username, String pass, String planCuentaID){    	
    	TblpersonaHome  acceso=new TblpersonaHome();
    	Tblpersona usuario = (Tblpersona) acceso.findByNombre(username, "cedulaRuc");
    	if(usuario==null)
    	{//El usuario ingresado no existe
    		return null;
    	}else{
    		User user = new User();
    		if(usuario.getEstado()=='1'){
    			//Quiere decir que el usuario esta activo
    			Iterator iter = usuario.getTblempleados().iterator();
                Tblempleado empleado = (Tblempleado)iter.next();
    			//Ahora comparamos si la clave ingresada es la correcta 
                if(empleado.getPassword().equals(pass)){
                	//El password es correcto
                	user.setUserName(usuario.getNombreComercial()+" "+usuario.getRazonSocial());
                    user.setIdUsuario(usuario.getIdPersona());
                    user.setCedulaRuc(usuario.getCedulaRuc());
                    user.setNivelAcceso(empleado.getNivelAcceso());  
                    user.setIdEmpresa(empleado.getIdEmpresa());
                    user.setPlanCuenta(planCuentaID);
                    setUserInSession(user);
                }else{
                	//El password es incorrecto
                	user.setUserName("ERRORCLAVE");
                } 
        		return user;
    		}else{
    			//Quiere decir que el usuario esta inactivo
        		user.setUserName("INACTIVO");
        		return user;
    		}
    	}
    }
    
    /**
     * Esta funcion toma los datos del usuario logeado por Zuul
     * @param username De tipo STRING es la Cedula del usuario
     * @param pass   De tipo STRING es la calve del usuario
     */
    public User checkLogin(String username, String planCuentaID){    	
    	TblpersonaHome  acceso=new TblpersonaHome();
    	Tblpersona usuario = (Tblpersona) acceso.findByNombre(username, "cedulaRuc");
    	if(usuario==null)
    	{//El usuario ingresado no existe
    		return null;
    	}else{
    		User user = new User();
    		if(usuario.getEstado()=='1'){
    			//Quiere decir que el usuario esta activo
    			Iterator iter = usuario.getTblempleados().iterator();
                Tblempleado empleado = (Tblempleado)iter.next();

                	user.setUserName(usuario.getNombreComercial());
                    user.setIdUsuario(usuario.getIdPersona());
                    user.setCedulaRuc(usuario.getCedulaRuc());
                    user.setNivelAcceso(empleado.getNivelAcceso());  
                    user.setIdEmpresa(empleado.getIdEmpresa());
                    user.setEstablecimiento(empleado.getEstablecimiento());
                    user.setPlanCuenta(planCuentaID);
                    user.setIdEmpresa(empleado.getIdEmpresa());
                    setUserInSession(user);
        		return user;
    		}else{
    			//Quiere decir que el usuario esta inactivo
        		user.setUserName("INACTIVO");
        		return user;
    		}
    	}
    }
	/******************************************************************************/
    //Funciones listar Rol
    public TblrolDTO crearRolReg(Tblrol rol){
		return new TblrolDTO(rol.getIdEmpresa(),rol.getIdRol(), rol.getDescripcion());
	}
	public List<TblrolDTO> listarRol(int ini, int fin) {
		List<TblrolDTO> RolReg = null;
		List<Tblrol> listado = null;
		TblrolHome acceso = new TblrolHome();
		try {
			listado=acceso.getList(ini, fin);
			RolReg = new ArrayList<TblrolDTO>();
            if (listado != null) {
                for (Tblrol Rol : listado) {
                	RolReg.add(crearRolReg(Rol));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return RolReg;
	}
	
	public List<TblrolDTO> listarRolCombo(int ini, int fin) {
		List<TblrolDTO> RolReg = null;
		List<Tblrol> listado = null;
		TblrolHome acceso = new TblrolHome();
		try {
			listado=acceso.getListCombo(ini, fin);
			RolReg = new ArrayList<TblrolDTO>();
            if (listado != null) {
                for (Tblrol Rol : listado) {
                	RolReg.add(crearRolReg(Rol));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return RolReg;
	}



	//Listar por nivel y por pestana
    public TblpermisorolpestanaDTO listarpermisosLecturap(int nivel, String pestana){
     	TblpermisorolpestanaDTO listapermisosl = null;
     	Tblpermisorolpestana listado= new Tblpermisorolpestana();
     	TblpermisorolpestanaHome acceso= new TblpermisorolpestanaHome();
     	System.out.println("Listar lectura ");
     	try {
     	listado=acceso.findIdPrpLectura(nivel, pestana);
     	
     	if(listado !=null){
     		listapermisosl=crearTblpermisorolpestana(listado) ;
     		System.out.println("Encontrado ");
     		/*for(Tblpermisorolpestana permiso:listado){
     			listapermisosl.add(crearTblpermisorolpestana(permiso));
     		}*/
     	}
     	}catch(RuntimeException re){
     		throw re;
     	}
     	return listapermisosl;
     	}

    public TblpermisorolpestanaDTO crearTblpermisorolpestana(Tblpermisorolpestana tp){
 		return new TblpermisorolpestanaDTO(crearTblPermisorolPestanaIdDTO(tp.getId())
 				,crearTblPermisoDTO(tp.getTblpermiso()),crearTblPestanaDTO(tp.getTblpestanas()), crearTblrolDTO(tp.getTblrol()));
 	}
    
 	public TblpermisorolpestanaIdDTO crearTblPermisorolPestanaIdDTO(TblpermisorolpestanaId id){
 		TblpermisorolpestanaIdDTO permisorolpestanaId = new TblpermisorolpestanaIdDTO();
 		permisorolpestanaId.setIdPermisorolpestana(id.getIdPermisorolpestana());
 		permisorolpestanaId.setIdRol(id.getIdRol());
 		return permisorolpestanaId;
 	}
 	
 	public TblpermisoDTO crearTblPermisoDTO(Tblpermiso id){
 		TblpermisoDTO permisoId = new TblpermisoDTO();
 		permisoId.setIdEmpresa(id.getIdEmpresa());
 		permisoId.setIdPermiso(id.getIdPermiso());
 		permisoId.setDescripcion(id.getDescripcion());
 		return permisoId;
 	}
 	
 	public TblpestanaDTO crearTblPestanaDTO(Tblpestanas id){
 		TblpestanaDTO pestanaId = new TblpestanaDTO();
 		pestanaId.setIdPestanas(id.getIdPestanas());
 		pestanaId.setDescripcion(id.getDescripcion());
 		return pestanaId;
 	}
 	
 	public TblrolDTO crearTblrolDTO(Tblrol id){
 		TblrolDTO rolId = new TblrolDTO();
 		rolId.setIdEmpresa(id.getIdEmpresa());
 		rolId.setIdRol(id.getIdRol());
 		rolId.setDescripcion(id.getDescripcion());
 		return rolId;
 	}

	public int numeroRegistrosPersona(String tabla) {
		TblpersonaHome  acceso=new TblpersonaHome();
		int r=acceso.countPersona(tabla);
		return r;
	}
	public String grabar() throws IllegalArgumentException
	{
		return "";
	}	
	/*public void Cedula(){
		List<Tblpersona> list;
		TblpersonaHome acceso=new TblpersonaHome();
		list=acceso.getList(0,88888);
		for(int i=0;i<list.size();i++){
			Tblpersona per=new Tblpersona();
			per=list.get(i);
			per.setCedulaRuc("0"+per.getCedulaRuc());
			try {
				acceso.modificar(per);
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	public String grabarEmpleado(PersonaDTO personadto){
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		TblpersonaHome acceso = new TblpersonaHome();
		boolean ban=true;
		
		try{
			per=acceso.findByNombre(persona.getCedulaRuc(),"cedulaRuc");
			String ap=per.getRazonSocial();
			System.out.println(per.getCedulaRuc());
			if(per.getRazonSocial().length()>0){
				mensaje="Persona ya Ingresado "+per.getRazonSocial();
				if(!per.getTblempleados().isEmpty()){
					ban=false;
					mensaje="Usuario ya Ingresado "+per.getRazonSocial();
				}else{
					mensaje="modificar usuario";
					TblempleadoHome acc=new TblempleadoHome();
					Tblempleado empleado = new Tblempleado(personadto.getEmpleadoU());
					Tblcargo as = new Tblcargo(personadto.getEmpleadoU().getTblcargo());
					//Tblrol rol=new Tblrol(personadto.getEmpleadoU().getTblrol());
					as.getTblempleados().add(empleado);
					//rol.getTblempleados().add(empleado);
					empleado.setTblcargo(as);
					//empleado.setTblrol(rol);
					empleado.setTblpersona(persona);
					persona.getTblempleados().add(empleado);
					Tblcliente cliente = new Tblcliente(personadto.getClienteU());
					cliente.setTblpersona(persona);
					persona.getTblclientes().add(cliente);
					acceso.modificar(persona);
					mensaje="Usuario Grabado";
				}
			}else{
				/*Tblempleado empleado = new Tblempleado(personadto.getEmpleadoU());
				Tblcargo as = new Tblcargo(personadto.getEmpleadoU().getTblcargo());
				as.getTblempleados().add(empleado);
				empleado.setTblcargo(as);
				empleado.setTblpersona(persona);
				persona.getTblempleados().add(empleado);
				acceso.grabar(persona);*/
				mensaje="Usuario Grabado";
			}
		}catch(Exception e){
			System.out.println(mensaje="Perrsona NO Ingresada "+e.getMessage());
			/*Tblempleado empleado = new Tblempleado(personadto.getEmpleadoU());
			Tblcargo as = new Tblcargo(personadto.getEmpleadoU().getTblcargo());
			as.getTblempleados().add(empleado);
			empleado.setTblcargo(as);
			empleado.setTblpersona(persona);
			persona.getTblempleados().add(empleado);
			Tblcliente cliente = new Tblcliente(personadto.getClienteU());
			cliente.setTblpersona(persona);
			persona.getTblclientes().add(cliente);
			try {
				acceso.grabar(persona);
				mensaje="Empleado Grabado";
			} catch (ErrorAlGrabar e1) {

				mensaje="Error: "+e1.getMessage();
				e1.printStackTrace();
			}*/
			
			
		}
		return mensaje;
	}
	
	public String GrabarProv(PersonaDTO personadto ){
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		TblpersonaHome perHome = new TblpersonaHome();
		per=perHome.findByCedula(persona.getCedulaRuc()); 
			//System.out.println(cli.getTblpersona().getApellidos());
			System.out.print("pas1");
			try{
				if(per.getTblproveedors().size()>0){
					mensaje="Proveedor ya Ingresado "+per.getTblclientes().size();
					System.out.println("Proveedor ya Ingresado ");
				}else{
					System.out.print("pas2");
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						if (personadto.getClienteU()!=null){
							if (per.getTblclientes().size()>0){
								personadto.setClienteU(null);
							}
						}
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Proveedor");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Proveedor";
						personadto.setIdPersona(per.getIdPersona());
						System.out.print("pas3");
						mensaje=modificarPersona(personadto);
						System.out.print("pas4");
						
					}else{
						System.out.println("Persona ni Proveedor existen listo para grabarrrrrrrrrrrroooo");
						mensaje="Persona ni Proveedor existen listo para grabarrrrrrrrrrrrrroooo";
						System.out.print("pas5");
						mensaje=grabarCliente(personadto);
					}
				}
			}catch(Exception e){
				try{
					System.out.print("pas6");
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Proveedorrrr");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Proveedorrrr";
						mensaje=modificarPersona(personadto);
					}else{
						System.out.println("Persona ni Proveedor existen listo para grabarrrrrrrrrrrr");
						mensaje="Persona ni Proveedor existen listo para grabarrrrrrrrrrrrrr";
						mensaje=grabarCliente(personadto);
					}
				}catch(Exception ex){
					mensaje = "Error"+ex;
				}
				
			}
		return mensaje;
	}
	
	
	public String GrabarCli(PersonaDTO personadto){
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		TblpersonaHome perHome = new TblpersonaHome();
			per=perHome.findByCedula(persona.getCedulaRuc()); 
			//System.out.println(cli.getTblpersona().getApellidos());
			
			try{
				if(per.getTblclientes().size()>0){
					mensaje="Cliente ya Ingresado "+per.getTblclientes().size();
					System.out.println("Cliente ya Ingresado ");
				}else{
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el cliente");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el cliente";
						personadto.setIdPersona(per.getIdPersona());
						mensaje=modificarPersona(personadto);
					}else{
						
					}
				}
			}catch(Exception e){
				try{
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el clienteeeee");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el clienteeeeeeeeeee";
						mensaje=modificarPersona(personadto);
					}else{
						System.out.println("Persona ni cliente existen listo para grabarrrrrrrrrrrr");
						mensaje="Persona ni cliente existen listo para grabarrrrrrrrrrrrrr";
						mensaje=grabarCliente(personadto);
					}
				}catch(Exception ex){
					
				}
				
			}
		return mensaje;
	}
	
	public String GrabarEmp(PersonaDTO personadto){
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		TblpersonaHome perHome = new TblpersonaHome();
		per=perHome.findByCedula(persona.getCedulaRuc()); 
			//System.out.println(cli.getTblpersona().getApellidos());
			
			try{
				if(per.getTblempleados().size()>0){
					mensaje="Usuario ya Ingresado "+per.getTblclientes().size();
					System.out.println("Empleado ya Ingresado ");
				}else{
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el usuario");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el usuario";
						personadto.setIdPersona(per.getIdPersona());
						mensaje=modificarPersona(personadto);
					}else{
						System.out.println("Persona ni Empleado existen listo para grabarrrrrrrrrrrroooo");
						mensaje="Persona ni Empleado existen listo para grabarrrrrrrrrrrrrroooo";
					}
				}
			}catch(Exception e){
				try{
					per=perHome.findByCedula(persona.getCedulaRuc());
					if(per!=null){
						System.out.println("Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Empleadoooo");
						mensaje="Persona ya Ingresada "+per.getRazonSocial()+ "Solo grabar el Empleadoooo";
						mensaje=modificarPersona(personadto);
					}else{
						System.out.println("Persona ni Empleado existen listo para grabarrrrrrrrrrrr");
						mensaje="Persona ni Empleado existen listo para grabarrrrrrrrrrrrrr";
						mensaje=grabarCliente(personadto);
					}
				}catch(Exception ex){
					
				}
				
			}
		return mensaje;
	}
	
	//Funcion para grabar una persona y al mismo tiempo un registro en las tablas
	//Cliente, Persona, Empleados 
	
public String grabarCliente(PersonaDTO personadto){
		
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		TblpersonaHome acceso = new TblpersonaHome();
		boolean ban=true;
		
		try{
			per=acceso.findByNombre(persona.getCedulaRuc(),"cedulaRuc");
			String ap=per.getRazonSocial();
			System.out.println(per.getCedulaRuc());
			if(per.getCedulaRuc().length()>0){
				ban=false;
				mensaje="Persona ya Ingresada "+per.getRazonSocial();
			}
		}catch(Exception e){
			System.out.println(mensaje="Perrsona NO Ingresada "+e.getMessage());
		}
		System.out.println(String.valueOf(ban));
		if(ban){
			if(personadto.getClienteU() != null) 
			{ 
				System.out.println("Cliente");
				//CLIENTE
				Tblcliente cliente = new Tblcliente(personadto.getClienteU());
				cliente.setTblpersona(persona);
				persona.getTblclientes().add(cliente);
			}
			if(personadto.getProveedorU() != null)
			{
				System.out.println("Proveedor");
				//PROVEEDOR
				Tblproveedor proveedor = new Tblproveedor(personadto.getProveedorU());
				proveedor.setTblpersona(persona);
				persona.getTblproveedors().add(proveedor);
			}
			if(personadto.getEmpleadoU() != null)
			{
				System.out.println("Empleado");
				//EMPLEADO
				Tblempleado empleado = new Tblempleado(personadto.getEmpleadoU());
				//Tblrol rol= new Tblrol(personadto.getEmpleadoU().getTblrol());
				Tblcargo as = new Tblcargo(personadto.getEmpleadoU().getTblcargo());
				//rol.getTblempleados().add(empleado);
				as.getTblempleados().add(empleado);
				empleado.setTblcargo(as);
				//empleado.setTblrol(rol);
				empleado.setTblpersona(persona);
				persona.getTblempleados().add(empleado);
				mensaje=as.getDescripcion()+" "+empleado.getSueldo()+" "+persona.getRazonSocial();
			}
			
			try {
				persona.setEstado('1');
				acceso.grabar(persona);
				//mensaje=persona.getCedulaRuc();
				mensaje="Grabado con Exito";
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje = e.getMessage();
			}
		}else{
			//mensaje="Persona ya Ingresada en el Sistema";
		}
		
		return mensaje;
	}
	
	@Override
	public String modificarPersona(PersonaDTO personadto) {
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		persona.setIdPersona(personadto.getIdPersona());
		TblpersonaHome acceso = new TblpersonaHome();
			if(personadto.getClienteU() != null)
			{ 
				//CLIENTE
				Tblcliente cliente = new Tblcliente(personadto.getClienteU());
				cliente.setIdCliente(personadto.getClienteU().getIdCliente());
				cliente.setTblpersona(persona);
				persona.getTblclientes().add(cliente);
				mensaje="Grabado"+String.valueOf(cliente.getIdCliente());
				System.out.printf("paso cliente");
			}
			if(personadto.getProveedorU() != null)
			{
				//PROVEEDOR
				Tblproveedor proveedor = new Tblproveedor(personadto.getProveedorU());
				proveedor.setIdProveedor(personadto.getProveedorU().getIdProveedor());
				proveedor.setTblpersona(persona);
				persona.getTblproveedors().add(proveedor);
				System.out.printf("paso proveedor");
			}
			if(personadto.getEmpleadoU() != null)
			{
				System.out.printf("paso empleado");
				//EMPLEADO
				Tblempleado empleado = new Tblempleado(personadto.getEmpleadoU());
				Tblcargo as = new Tblcargo(personadto.getEmpleadoU().getTblcargo());
				empleado.setIdEmpleado(personadto.getEmpleadoU().getIdEmpleado());
				as.getTblempleados().add(empleado);
				empleado.setTblcargo(as);
				empleado.setTblpersona(persona);
				persona.getTblempleados().add(empleado);
				mensaje="id cargo: "+as.getIdCargo()+"idempleado: "+empleado.getIdEmpleado()+"idpersona "+persona.getIdPersona();
			}
			
			try {
				acceso.modificar(persona);
				mensaje=" actualizado "+String.valueOf(personadto.getCedulaRuc());
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje = e.getMessage();
			}
		
		return mensaje;
	}
		
	
	//++++++++++   PARA LAS PERSONAS QUE TIENEN INCORRECTO EL RUC ++++++++++++++++++++
	public String modificarPersonaRUC(PersonaDTO personadto) 
	{
		String mensaje = "";
		Tblpersona persona = new Tblpersona(personadto);
		persona.setIdPersona(personadto.getIdPersona());
		TblpersonaHome acceso = new TblpersonaHome();			
		try {
				acceso.modificar(persona);
				mensaje="actualizado "+String.valueOf(personadto.getIdPersona());
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje = e.getMessage();
			}		
		return mensaje;
	}
	@Override
	public String eliminarPersona(PersonaDTO persona) {
		PersonaDTO per=new PersonaDTO();
		per=persona;
		per.setEstado('0');
		return modificarPersona(persona);
	}
	@Override
	public PersonaDTO buscarPersona(PersonaDTO pers, int tipo) {
		String mensaje = "";
		//Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		PersonaDTO persona=new PersonaDTO();
		TblpersonaHome acceso = new TblpersonaHome();
		
		try{
			per=acceso.findByNombre(pers.getCedulaRuc(),"cedulaRuc");
		}catch(Exception e){
			per=null;
		}
		if(per!=null){
			persona.setRazonSocial(per.getRazonSocial());
			persona.setIdPersona(per.getIdPersona());
			persona.setCedulaRuc(per.getCedulaRuc());
			persona.setDireccion(per.getDireccion());
			persona.setNombreComercial(per.getNombreComercial());
			persona.setEstado(per.getEstado());
			if(per.getMail()!=null){
				persona.setMail(per.getMail());
			}
			if(per.getObservaciones()!=null){
				persona.setObservaciones(per.getObservaciones());
			}
			if(per.getLocalizacion()!=null){
				persona.setLocalizacion(per.getLocalizacion());
			}
			if(per.getTelefono1()!=null){
				persona.setTelefono1(per.getTelefono1());
			}
			if(per.getTelefono2()!=null){
				persona.setTelefono2(per.getTelefono2());
			}
			if(tipo==1){
				TblempleadoHome accesoEmpleado= new TblempleadoHome();
				Tblempleado tblempleado= new Tblempleado();
				try{
					tblempleado=accesoEmpleado.findByNombre(String.valueOf(persona.getIdPersona()),"tblpersona");
					if(tblempleado!= null){
						EmpleadoDTO emp =new EmpleadoDTO();
						emp.setIdEmpresa(tblempleado.getIdEmpresa());
						emp.setEstablecimiento(tblempleado.getEstablecimiento());
						emp.setFechaContratacion(tblempleado.getFechaContratacion());
						emp.setHorasExtras(tblempleado.getHorasExtras());
						emp.setIdEmpleado(tblempleado.getIdEmpleado());
						emp.setIngresosExtras(tblempleado.getIngresosExtras());
						emp.setIdRol(tblempleado.getIdRol());
						emp.setNivelAcceso(tblempleado.getNivelAcceso());
						emp.setEstado(tblempleado.getEstado());
						//emp.setTblrol(new TblrolDTO(tblempleado.getTblrol().getIdRol(),tblempleado.getTblrol().getDescripcion()));
						emp.setPassword(tblempleado.getPassword());
						emp.setSueldo(tblempleado.getSueldo());
						emp.setTblcargo(new CargoDTO(tblempleado.getTblcargo().getIdEmpresa(),tblempleado.getTblcargo().getIdCargo(),tblempleado.getTblcargo().getDescripcion()));
						persona.setEmpleadoU(emp);
					}else{
						persona=null;
					}
				}catch (Exception e){
					persona=null;
				}
			}else if(tipo==0){
				TblclienteHome accesoCliente= new TblclienteHome();
				Tblcliente tblcliente= new Tblcliente();
				try{
					try{
						tblcliente=accesoCliente.findByNombre(String.valueOf(persona.getIdPersona()),"tblpersona");
						if(tblcliente!= null){
							ClienteDTO cli =new ClienteDTO();
							cli.setIdEmpresa(tblcliente.getIdEmpresa());
							cli.setCupoCredito(tblcliente.getCupoCredito());
							cli.setMontoCredito(tblcliente.getMontoCredito());
							cli.setIdCliente(tblcliente.getIdCliente());
							cli.setCodigoTarjeta(tblcliente.getCodigoTarjeta());
							cli.setPuntos(tblcliente.getPuntos());
							cli.setContribuyenteEspecial(tblcliente.getContribuyenteEspecial());
							cli.setObligadoContabilidad(tblcliente.getObligadoContabilidad());
							
							persona.setClienteU(cli);
						}else{
							persona=null;
						}
					}catch(Exception e){
						persona=null;
					}
				}catch(Exception e){
					persona.setRazonSocial(e.getMessage());
				}
			}else if(tipo==2){
				TblproveedorHome accesoProveedor= new TblproveedorHome();
				Tblproveedor tblproveedor= new Tblproveedor();
				try{
					tblproveedor=accesoProveedor.findByNombre(String.valueOf(persona.getIdPersona()),"tblpersona");
					if(tblproveedor!= null){
						ProveedorDTO pro =new ProveedorDTO();
						pro.setIdEmpresa(tblproveedor.getIdEmpresa());
						pro.setCupoCredito(tblproveedor.getCupoCredito());
						pro.setContacto(tblproveedor.getContacto());
						pro.setIdProveedor(tblproveedor.getIdProveedor());
						persona.setProveedorU(pro);
					}else{
						persona=null;
					}
				}catch (Exception e){
					persona=null;
				}
			}
			
			
		}
		return persona;
	}

	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public PersonaDTO buscarPersona2(PersonaDTO pers, int tipo) {
		String mensaje = "";
		//Tblpersona persona = new Tblpersona(personadto);
		Tblpersona per = new Tblpersona();
		PersonaDTO persona=new PersonaDTO();
		TblpersonaHome acceso = new TblpersonaHome();
		
		try{
			per=acceso.findByNombre2(pers.getCedulaRuc(),"cedulaRuc");
		}catch(Exception e){
			per=null;
		}
		if(per!=null){
			persona.setRazonSocial(per.getRazonSocial());
			persona.setIdPersona(per.getIdPersona());
			persona.setCedulaRuc(per.getCedulaRuc());
			persona.setDireccion(per.getDireccion());
			persona.setNombreComercial(per.getNombreComercial());
			persona.setEstado(per.getEstado());
			if(per.getMail()!=null){
				persona.setMail(per.getMail());
			}
			if(per.getObservaciones()!=null){
				persona.setObservaciones(per.getObservaciones());
			}
			if(per.getLocalizacion()!=null){
				persona.setLocalizacion(per.getLocalizacion());
			}
			if(per.getTelefono1()!=null){
				persona.setTelefono1(per.getTelefono1());
			}
			if(per.getTelefono2()!=null){
				persona.setTelefono2(per.getTelefono2());
			}
			if(tipo==1){
				TblempleadoHome accesoEmpleado= new TblempleadoHome();
				Tblempleado tblempleado= new Tblempleado();
				try{
					tblempleado=accesoEmpleado.findByNombre2(String.valueOf(persona.getIdPersona()),"tblpersona");
					if(tblempleado!= null){
						EmpleadoDTO emp =new EmpleadoDTO();
						emp.setIdEmpresa(tblempleado.getIdEmpresa());
						emp.setEstablecimiento(tblempleado.getEstablecimiento());
						emp.setEstado(tblempleado.getEstado());
						emp.setFechaContratacion(tblempleado.getFechaContratacion());
						emp.setHorasExtras(tblempleado.getHorasExtras());
						emp.setIdEmpleado(tblempleado.getIdEmpleado());
						emp.setIngresosExtras(tblempleado.getIngresosExtras());
						emp.setIdRol(tblempleado.getIdRol());
						emp.setNivelAcceso(tblempleado.getNivelAcceso());
						emp.setPassword(tblempleado.getPassword());
						emp.setSueldo(tblempleado.getSueldo());
						emp.setTblcargo(new CargoDTO(tblempleado.getTblcargo().getIdEmpresa(),tblempleado.getTblcargo().getIdCargo(),tblempleado.getTblcargo().getDescripcion()));
						persona.setEmpleadoU(emp);
					}else{
						persona=null;
					}
				}catch (Exception e){
					persona=null;
				}
			}else if(tipo==0){
				TblclienteHome accesoCliente= new TblclienteHome();
				Tblcliente tblcliente= new Tblcliente();
				try{
					try{
						tblcliente=accesoCliente.findByNombre2(String.valueOf(persona.getIdPersona()),"tblpersona");
						if(tblcliente!= null){
							ClienteDTO cli =new ClienteDTO();
							cli.setIdEmpresa(tblcliente.getIdEmpresa());
							cli.setCupoCredito(tblcliente.getCupoCredito());
							cli.setMontoCredito(tblcliente.getMontoCredito());
							cli.setIdCliente(tblcliente.getIdCliente());
							cli.setContribuyenteEspecial(tblcliente.getContribuyenteEspecial());
							cli.setObligadoContabilidad(tblcliente.getObligadoContabilidad());
							persona.setClienteU(cli);
						}else{
							persona=null;
						}
					}catch(Exception e){
						persona=null;
					}
				}catch(Exception e){
					persona.setRazonSocial(e.getMessage());
				}
			}else if(tipo==2){
				TblproveedorHome accesoProveedor= new TblproveedorHome();
				Tblproveedor tblproveedor= new Tblproveedor();
				try{
					tblproveedor=accesoProveedor.findByNombre2(String.valueOf(persona.getIdPersona()),"tblpersona");
					if(tblproveedor!= null){
						ProveedorDTO pro =new ProveedorDTO();
						pro.setIdEmpresa(tblproveedor.getIdEmpresa());
						pro.setCupoCredito(tblproveedor.getCupoCredito());
						pro.setContacto(tblproveedor.getContacto());
						pro.setIdProveedor(tblproveedor.getIdProveedor());
						persona.setProveedorU(pro);
					}else{
						persona=null;
					}
				}catch (Exception e){
					persona=null;
				}
			}
			
			
		}
		return persona;
	}
	/**
	 * M�todo para listar los empleados con sus respectivos cargos
	 */
	public List<PersonaDTO> ListEmpleados(int ini,int fin){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		Iterator resultado = acceso.ListEmpleados(ini, fin);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpersona persona = (Tblpersona) pair[0];
			Tblempleado empleado = (Tblempleado) pair[1];
			System.out.println(empleado.getIdEmpleado());
			System.out.println(persona.getIdPersona());
			Tblcargo cargo=(Tblcargo) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			System.out.println(personaD.getIdPersona()+" persona");
			EmpleadoDTO emp=new EmpleadoDTO();
			emp.setIdEmpresa(empleado.getIdEmpresa());
			emp.setEstablecimiento(empleado.getEstablecimiento());
			emp.setIdEmpleado(empleado.getIdEmpleado());
			emp.setEstado(empleado.getEstado());
			emp.setFechaContratacion(empleado.getFechaContratacion());
			emp.setHorasExtras(empleado.getHorasExtras());
			emp.setIngresosExtras(empleado.getHorasExtras());
			emp.setSueldo(empleado.getSueldo());
			emp.setPassword(empleado.getPassword());
			emp.setIdRol(empleado.getIdRol());
			emp.setNivelAcceso(empleado.getNivelAcceso());
			CargoDTO car=new CargoDTO();
			car.setIdEmpresa(cargo.getIdEmpresa());
			car.setDescripcion(cargo.getDescripcion());
			car.setIdCargo(cargo.getIdCargo());
			emp.setTblcargo(car);
			personaD.setEmpleadoU(emp);
			list.add(personaD);
		}
		return list;
	}
	
	public List<PersonaDTO> ListJoinPersona(String tabla, int ini,int fin){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		Iterator resultado = acceso.ListJoin(tabla, ini, fin);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpersona persona = (Tblpersona) pair[0];
			System.out.println("PERSONA LOCALIZACION "+(persona.getLocalizacion()!=null));
			//System.out.println("PERSONA LOCALIZACION "+(persona.getLocalizacion().equals(null)));
			if(tabla.equals("tblclientes")){
				Tblcliente cliente = (Tblcliente) pair[1];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				ClienteDTO emp=new ClienteDTO();
				emp.setIdEmpresa(cliente.getIdEmpresa());
				emp.setIdCliente(cliente.getIdCliente());
				emp.setMontoCredito(cliente.getMontoCredito());
				emp.setCupoCredito(cliente.getCupoCredito());
				emp.setCodigoTarjeta(cliente.getCodigoTarjeta());
				emp.setPuntos(cliente.getPuntos());
				emp.setContribuyenteEspecial(cliente.getContribuyenteEspecial());
				emp.setObligadoContabilidad(cliente.getObligadoContabilidad());
				personaD.setClienteU(emp);
				list.add(personaD);
			}else if(tabla.equals("tblproveedors")){
				Tblproveedor prov = (Tblproveedor) pair[1];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				ProveedorDTO emp=new ProveedorDTO();
				emp.setIdEmpresa(prov.getIdEmpresa());
				emp.setIdProveedor(prov.getIdProveedor());
				emp.setContacto(prov.getContacto());
				emp.setCupoCredito(prov.getCupoCredito());
				personaD.setProveedorU(emp);
				list.add(personaD);
				
			}
			
			
		}
		return list;
	}
	
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public List<PersonaDTO> ListJoinPersona2(String tabla){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		Iterator resultado = acceso.ListJoin2(tabla);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpersona persona = (Tblpersona) pair[0];
			if(tabla.equals("tblclientes")){
				Tblcliente cliente = (Tblcliente) pair[1];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				ClienteDTO emp=new ClienteDTO();
				emp.setIdEmpresa(cliente.getIdEmpresa());
				emp.setIdCliente(cliente.getIdCliente());
				emp.setMontoCredito(cliente.getMontoCredito());
				emp.setCupoCredito(cliente.getCupoCredito());
				emp.setContribuyenteEspecial(cliente.getContribuyenteEspecial());
				emp.setObligadoContabilidad(cliente.getObligadoContabilidad());
				personaD.setClienteU(emp);
				list.add(personaD);
			}else if(tabla.equals("tblproveedors")){
				Tblproveedor prov = (Tblproveedor) pair[1];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				ProveedorDTO emp=new ProveedorDTO();
				emp.setIdEmpresa(prov.getIdEmpresa());
				emp.setIdProveedor(prov.getIdProveedor());
				emp.setContacto(prov.getContacto());
				emp.setCupoCredito(prov.getCupoCredito());
				personaD.setProveedorU(emp);
				list.add(personaD);
				
			}
			
			
		}
		return list;
	}
	public List<PersonaDTO> findJoin(String variable,String tabla, String campo){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		if(tabla.equals("tblempleados")){
			Iterator result = acceso.ListEmpleadosLike(variable, campo);
			while ( result.hasNext() ) {
				Object[] pair = (Object[])result.next();
				Tblpersona persona = (Tblpersona) pair[0];
				Tblempleado empleado = (Tblempleado) pair[1];
				Tblcargo cargo=(Tblcargo) pair[2];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				EmpleadoDTO emp=new EmpleadoDTO();
				emp.setIdEmpresa(empleado.getIdEmpresa());
				emp.setEstablecimiento(empleado.getEstablecimiento());
				emp.setIdEmpleado(empleado.getIdEmpleado());
				emp.setEstado(empleado.getEstado());
				emp.setFechaContratacion(empleado.getFechaContratacion());
				emp.setHorasExtras(empleado.getHorasExtras());
				emp.setIngresosExtras(empleado.getHorasExtras());
				emp.setSueldo(empleado.getSueldo());
				emp.setPassword(empleado.getPassword());
				emp.setIdRol(empleado.getIdRol());
				emp.setNivelAcceso(empleado.getNivelAcceso());
				CargoDTO car=new CargoDTO();
				car.setIdEmpresa(empleado.getIdEmpresa());
				car.setDescripcion(cargo.getDescripcion());
				car.setIdCargo(cargo.getIdCargo());
				emp.setTblcargo(car);
				personaD.setEmpleadoU(emp);
				list.add(personaD);
			}
		}else{
			Iterator resultado = acceso.findJoinPerso(variable, tabla,  campo);
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblpersona persona = (Tblpersona) pair[0];
				if(tabla.equals("tblclientes")){
					Tblcliente cliente = (Tblcliente) pair[1];
					PersonaDTO personaD = new PersonaDTO();
					personaD = crearPersonaDTO(persona);
					personaD.setIdPersona(persona.getIdPersona());
					ClienteDTO emp=new ClienteDTO();
					emp.setIdEmpresa(cliente.getIdEmpresa());
					emp.setIdCliente(cliente.getIdCliente());
					emp.setMontoCredito(cliente.getMontoCredito());
					emp.setCupoCredito(cliente.getCupoCredito());
					emp.setContribuyenteEspecial(cliente.getContribuyenteEspecial());
					emp.setObligadoContabilidad(cliente.getObligadoContabilidad());
					personaD.setClienteU(emp);
					list.add(personaD);
				}else if(tabla.equals("tblproveedors")){
					Tblproveedor prov = (Tblproveedor) pair[1];
					PersonaDTO personaD = new PersonaDTO();
					personaD = crearPersonaDTO(persona);
					personaD.setIdPersona(persona.getIdPersona());
					ProveedorDTO emp=new ProveedorDTO();
					emp.setIdEmpresa(prov.getIdEmpresa());
					emp.setIdProveedor(prov.getIdProveedor());
					emp.setContacto(prov.getContacto());
					emp.setCupoCredito(prov.getCupoCredito());
					personaD.setProveedorU(emp);
					list.add(personaD);
					
				}
			}
		}
		
			
	/*		Object[] pair = (Object[])resultado.next();
			Tblpersona persona = (Tblpersona) pair[0];
			Tblcliente cliente = (Tblcliente) pair[1];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			personaD.setClienteU(
					new ClienteDTO(null, cliente.getMontoCredito(),cliente.getCupoCredito()));
			list.add(personaD);*/
		return list;
	}
		
	
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public List<PersonaDTO> findJoin2(String variable,String tabla, String campo){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		if(tabla.equals("tblempleados")){
			Iterator result = acceso.ListEmpleadosLike2(variable, campo);
			while ( result.hasNext() ) {
				Object[] pair = (Object[])result.next();
				Tblpersona persona = (Tblpersona) pair[0];
				Tblempleado empleado = (Tblempleado) pair[1];
				Tblcargo cargo=(Tblcargo) pair[2];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				EmpleadoDTO emp=new EmpleadoDTO();
				emp.setIdEmpresa(empleado.getIdEmpresa());
				emp.setEstablecimiento(empleado.getEstablecimiento());
				emp.setIdEmpleado(empleado.getIdEmpleado());
				emp.setEstado(empleado.getEstado());
				emp.setFechaContratacion(empleado.getFechaContratacion());
				emp.setHorasExtras(empleado.getHorasExtras());
				emp.setIngresosExtras(empleado.getHorasExtras());
				emp.setSueldo(empleado.getSueldo());
				emp.setPassword(empleado.getPassword());
				CargoDTO car=new CargoDTO();
				car.setDescripcion(cargo.getDescripcion());
				car.setIdCargo(cargo.getIdCargo());
				emp.setTblcargo(car);
				personaD.setEmpleadoU(emp);
				list.add(personaD);
			}
		}else{
			Iterator resultado = acceso.findJoinPerso2(variable, tabla,  campo);
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblpersona persona = (Tblpersona) pair[0];
				if(tabla.equals("tblclientes")){
					Tblcliente cliente = (Tblcliente) pair[1];
					PersonaDTO personaD = new PersonaDTO();
					personaD = crearPersonaDTO(persona);
					personaD.setIdPersona(persona.getIdPersona());
					ClienteDTO emp=new ClienteDTO();
					emp.setIdEmpresa(cliente.getIdEmpresa());
					emp.setIdCliente(cliente.getIdCliente());
					emp.setMontoCredito(cliente.getMontoCredito());
					emp.setCupoCredito(cliente.getCupoCredito());
					emp.setCodigoTarjeta(cliente.getCodigoTarjeta());
					emp.setPuntos(cliente.getPuntos());
					emp.setContribuyenteEspecial(cliente.getContribuyenteEspecial());
					emp.setObligadoContabilidad(cliente.getObligadoContabilidad());
					personaD.setClienteU(emp);
					list.add(personaD);
				}else if(tabla.equals("tblproveedors")){
					Tblproveedor prov = (Tblproveedor) pair[1];
					PersonaDTO personaD = new PersonaDTO();
					personaD = crearPersonaDTO(persona);
					personaD.setIdPersona(persona.getIdPersona());
					ProveedorDTO emp=new ProveedorDTO();
					emp.setIdEmpresa(prov.getIdEmpresa());
					emp.setIdProveedor(prov.getIdProveedor());
					emp.setContacto(prov.getContacto());
					emp.setCupoCredito(prov.getCupoCredito());
					personaD.setProveedorU(emp);
					list.add(personaD);
					
				}
			}
		}
		
		return list;
	}

	//++++++++++++++++++++       PARA LISTAR LAS CEDULAS INCORRECTAS       ++++++++++++++++++
	public List<PersonaDTO> findJoinCedulas(String variable, String campo){
		List<PersonaDTO> list = new ArrayList<PersonaDTO>();
		TblpersonaHome acceso = new TblpersonaHome();
		//if(tabla.equals("tblempleados")){
			Iterator result = acceso.ListCedulasIncorrectas(variable, campo);
			while ( result.hasNext() ) {
				//Object[] pair = (Object[])result.next();
				Tblpersona persona = (Tblpersona) result.next();
		//		Tblempleado empleado = (Tblempleado) pair[1];
		//		Tblcargo cargo=(Tblcargo) pair[2];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
		/*		EmpleadoDTO emp=new EmpleadoDTO();
				emp.setIdEmpleado(empleado.getIdEmpleado());
				emp.setFechaContratacion(empleado.getFechaContratacion());
				emp.setHorasExtras(empleado.getHorasExtras());
				emp.setIngresosExtras(empleado.getHorasExtras());
				emp.setSueldo(empleado.getSueldo());
				emp.setPassword(empleado.getPassword());
				CargoDTO car=new CargoDTO();
				car.setDescripcion(cargo.getDescripcion());
				car.setIdCargo(cargo.getIdCargo());
				emp.setTblcargo(car);
				personaD.setEmpleadoU(emp);*/
				list.add(personaD);
			}
	//		}
		
		return list;
	}

	

	public ProductoDTO crearProductoDTO(Tblproducto producto){
		ProductoDTO pro=new ProductoDTO();
		pro.setIdEmpresa(producto.getIdEmpresa());
		pro.setEstablecimiento(producto.getEstablecimiento());
		pro.setIdProducto(producto.getIdProducto());
		pro.setCodigoBarras(producto.getCodigoBarras());
		pro.setDescripcion(producto.getDescripcion());
		pro.setEstado(pro.getEstado());
		pro.setFifo(producto.getFifo());
		pro.setTblmultiImpuesto(crearSetProductoMultiImpuestoDTO(producto.getTblProdMultiImpuesto()));
//		pro.setImpuesto(producto.getImpuesto());
		pro.setLifo(producto.getLifo());
		pro.setPromedio(producto.getPromedio());
		pro.setStock(producto.getStock());
		return pro;
		
	}
	public Set<TblproductoMultiImpuestoDTO> crearSetProductoMultiImpuestoDTO(Set<TblproductoMultiImpuesto> productosMultiImpuesto){
		Set<TblproductoMultiImpuestoDTO> prodMul =new HashSet(0);
		for (TblproductoMultiImpuesto productoMultiImpuesto:productosMultiImpuesto){
			prodMul.add(crearProductoMultiImpuestoDTO(productoMultiImpuesto));
		}
		return prodMul;
	}
	public TblproductoMultiImpuestoDTO crearProductoMultiImpuestoDTO(TblproductoMultiImpuesto productoMultiImpuesto){
			TblproductoMultiImpuestoDTO proMul = new TblproductoMultiImpuestoDTO();
			proMul.setIdtblproducto_tblmulti_impuesto(productoMultiImpuesto.getIdtblproducto_tblmulti_impuesto());
			proMul.setTblmultiImpuesto(crearMultiImpuestoDTO(productoMultiImpuesto.getTblmultiImpuesto()));
		return proMul;
	}
	
	public TblmultiImpuestoDTO crearMultiImpuestoDTO(TblmultiImpuesto multiImpuesto){
		TblmultiImpuestoDTO multiImp = new TblmultiImpuestoDTO();
		multiImp.setDescripcion(multiImpuesto.getDescripcion());
		multiImp.setIdmultiImpuesto(multiImpuesto.getIdmultiImpuesto());
		multiImp.setPorcentaje(multiImpuesto.getPorcentaje());
		multiImp.setTipo(multiImpuesto.getTipo());
		return multiImp;
	}
	/**
	 * 			FUNCION DE ACCESO PARA LAS OPERACIONES DE FACTURACION
	 */
	
	
	public String greetServer(String input) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo
				+ ".<br><br>It looks like you are using:<br>" + userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}


	

	
	/**
	 * M�todo para grabar una Marca
	 * @param MarcaDTO
	 */
	public String grabarMarca(MarcaDTO entidad) throws IllegalArgumentException {
		TblmarcaHome accesmarca = new TblmarcaHome();
		Tblmarca tblmarca= new Tblmarca();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tblmarca=accesmarca.findByNombre(entidad.getMarca(),"marca");
				ban=1;
				mensaje="marca ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesmarca.grabar(new Tblmarca(entidad));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public String modificarMarca(MarcaDTO entidad) throws IllegalArgumentException {
		TblmarcaHome accesoMarca = new TblmarcaHome();
		String mensaje = "";
		try {
			try {
				accesoMarca.modificar(new Tblmarca(entidad));
				mensaje="Marca modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public MarcaDTO buscarMarca(String idMarca) {
		TblmarcaHome accesoMarca = new TblmarcaHome();
		Tblmarca tblmarca= new Tblmarca();
		MarcaDTO marca= new MarcaDTO();
		marca=null;
		String mensaje = "";
		try {
			try {
			//	System.out.println(idMarca);
				tblmarca=accesoMarca.findById(Integer.valueOf(idMarca));
				marca=new MarcaDTO(tblmarca.getIdEmpresa(),tblmarca.getIdMarca(),tblmarca.getMarca());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return marca;
	}
	
	public MarcaDTO crearMarcaReg(Tblmarca marc){
		return new MarcaDTO(marc.getIdEmpresa(),marc.getIdMarca(), marc.getMarca());
	}
	
	
	@Override
	public String eliminarMarca(MarcaDTO marca) {
		TblmarcaHome accesmarca = new TblmarcaHome();
		String mensaje = "";
		try {
			try {
				accesmarca.eliminacionFisica(new Tblmarca(marca));
				mensaje = "Objeto eliminado realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje=e.getMessage();
				mensaje="No se puede eliminar la Marca porque est\u00e1 siendo utilizada en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	
	@Override
	public List<MarcaDTO> listarMarca(int ini, int fin) {
		List<MarcaDTO> MarcaReg = null;
		List<Tblmarca> listado = null;
		TblmarcaHome acceso = new TblmarcaHome();
		try {
			listado=acceso.getList(ini, fin);
			MarcaReg = new ArrayList<MarcaDTO>();
            if (listado != null) {
                for (Tblmarca Marca : listado) {
                    MarcaReg.add(crearMarcaReg(Marca));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MarcaReg;
	}
	@Override
	public List<MarcaDTO> listarMarcaCombo(int ini, int fin) {
		List<MarcaDTO> MarcaReg = null;
		List<Tblmarca> listado = null;
		TblmarcaHome acceso = new TblmarcaHome();
		try {
			listado=acceso.getListCombo(ini, fin);
			MarcaReg = new ArrayList<MarcaDTO>();
            if (listado != null) {
                for (Tblmarca Marca : listado) {
                    MarcaReg.add(crearMarcaReg(Marca));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MarcaReg;
	}
	
	@Override
	public List<MarcaDTO> listarMarcaLike(String busqueda, String campo) {
		List<MarcaDTO> MarcaReg = null;
		List<Tblmarca> listado = null;
		TblmarcaHome acceso = new 	TblmarcaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			MarcaReg = new ArrayList<MarcaDTO>();
            if (listado != null) {
                for (Tblmarca Marca : listado) {
                	MarcaReg.add(crearMarcaReg(Marca));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MarcaReg;
	}
	@Override
	public int numeroRegistrosMarca(String tabla) {
		TblmarcaHome accesomarca = new TblmarcaHome();
		int registros=0;
		try {
				registros=accesomarca.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	// FUNCIONES UNIDAD////////////////////////////////////////////////////////////////
	/**
	 * M�todo para grabar una Unidad
	 * @param UnidadDTO
	 */
	
	public String grabarUnidad(UnidadDTO entidad) throws IllegalArgumentException {
		TblunidadHome accesoUnidad = new TblunidadHome();
		String mensaje = "";
		Tblunidad tblunidad= new Tblunidad();
		
		int ban=0;
		try {
			try {
				tblunidad=accesoUnidad.findByNombre(entidad.getUnidad(),"nombre");
				ban=1;
				mensaje="Unidad ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesoUnidad.grabar(new Tblunidad(entidad));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public String modificarUnidad(UnidadDTO entidad) throws IllegalArgumentException {
		TblunidadHome accesoUnidad = new TblunidadHome();
		String mensaje = "";
		try {
			try {
				accesoUnidad.modificar(new Tblunidad(entidad));
				mensaje = "Unidad Modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public UnidadDTO buscarUnidad(String idUnidad) {
		TblunidadHome accesoUnidad = new TblunidadHome();
		Tblunidad tblunidad= new Tblunidad();
		UnidadDTO unidad= new UnidadDTO();
		unidad=null;
		String mensaje = "";
		try {
			try {
				//System.out.println(idUnidad);
				tblunidad=accesoUnidad.findById(Integer.valueOf(idUnidad));
				unidad=new UnidadDTO(tblunidad.getIdUnidad(),tblunidad.getNombre());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return unidad;
	}
	
	@Override
	public String eliminarUnidad(UnidadDTO unidad) {
		TblunidadHome accesounidad = new TblunidadHome();
		String mensaje = "";
		try {
			try {
				accesounidad.eliminacionFisica(new Tblunidad(unidad));
				mensaje = "Objeto eliminado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la Unidad porque est\u00e1 siendo utilizada en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public UnidadDTO crearUnidadReg(Tblunidad uni){
		return new UnidadDTO(uni.getIdUnidad(), uni.getNombre());
	}
	
	@Override
	public List<UnidadDTO> listarUnidad(int ini, int fin) {
		List<UnidadDTO> UnidadReg = null;
		List<Tblunidad> listado = null;
		TblunidadHome acceso = new TblunidadHome();
		try {
			listado=acceso.getList(ini, fin);
			UnidadReg = new ArrayList<UnidadDTO>();
            if (listado != null) {
                for (Tblunidad Unidad : listado) {
                	UnidadReg.add(crearUnidadReg(Unidad));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return UnidadReg;
		
	}
	@Override
	public List<UnidadDTO> listarUnidadCombo(int ini, int fin) {
		List<UnidadDTO> UnidadReg = null;
		List<Tblunidad> listado = null;
		TblunidadHome acceso = new TblunidadHome();
		try {
			listado=acceso.getListCombo(ini, fin);
			UnidadReg = new ArrayList<UnidadDTO>();
            if (listado != null) {
                for (Tblunidad Unidad : listado) {
                	UnidadReg.add(crearUnidadReg(Unidad));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return UnidadReg;
		
	}
	
	@Override
	public List<UnidadDTO> listarUnidadLike(String busqueda, String campo) {
		List<UnidadDTO> UnidadReg = null;
		List<Tblunidad> listado = null;
		TblunidadHome acceso = new 	TblunidadHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			UnidadReg = new ArrayList<UnidadDTO>();
            if (listado != null) {
                for (Tblunidad Unidad : listado) {
                	UnidadReg.add(crearUnidadReg(Unidad));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return UnidadReg;
	}
	@Override
	public int numeroRegistrosUnidad(String tabla) {
		TblunidadHome accesounidad = new TblunidadHome();
		int registros=0;
		try {
				registros=accesounidad.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	//metodos para CATEGORIA /////////////////////////////////////////
	@Override
	public String grabarCategoria(CategoriaDTO entidad){
		TblcategoriaHome accesoCategoria = new TblcategoriaHome();
		System.out.print("Categoria1 "+entidad.getCategoria()+" empresa: "+entidad.getIdEmpresa());
		Tblcategoria tblcategoria=new Tblcategoria();
		int ban=0;
		String mensaje = "";
		try {
			try {
				tblcategoria=accesoCategoria.findByNombre(entidad.getCategoria(),"categoria");
				ban=1;
				mensaje="Categor�a ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					System.out.print("Categoria2 "+entidad.getCategoria()+" empresa: "+entidad.getIdEmpresa());
					tblcategoria=new Tblcategoria(entidad);
					System.out.print("Categoria3 "+tblcategoria.getCategoria()+" empresa: "+tblcategoria.getIdEmpresa());
					accesoCategoria.grabar(tblcategoria);
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String modificarCategoria(CategoriaDTO entidad) {
		// TODO Auto-generated method stub
		TblcategoriaHome accesoCategoria = new TblcategoriaHome();
		String mensaje = "";
		try {
			try {
				accesoCategoria.modificar(new Tblcategoria(entidad));
				mensaje = "Categor\u00EDa modificada con con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String eliminarCategoria(CategoriaDTO categoria) {
		TblcategoriaHome accesounidad = new TblcategoriaHome();
		String mensaje = "";
		try {
			try {
				accesounidad.eliminacionFisica(new Tblcategoria(categoria));
				mensaje = "Objeto eliminado realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la Categor\u00eda porque est\u00e1 siendo utilizada en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public CategoriaDTO buscarCategoria(String idCategoria) {
		TblcategoriaHome accesoCategoria = new TblcategoriaHome();
		Tblcategoria tblcategoria= new Tblcategoria();
		CategoriaDTO categoria= new CategoriaDTO();
		categoria=null;
		String mensaje = "";
		try {
			try {
				tblcategoria=accesoCategoria.findById(Integer.valueOf(idCategoria));
				categoria=new CategoriaDTO(tblcategoria.getIdEmpresa(),tblcategoria.getIdCategoria(),tblcategoria.getCategoria());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return categoria;
		
	}
	public CategoriaDTO crearCategoriaReg(Tblcategoria cat){
		return new CategoriaDTO(cat.getIdEmpresa(),cat.getIdCategoria(), cat.getCategoria());
	}
	@Override
	public List<CategoriaDTO> listarCategoria(int ini, int fin) {
		List<CategoriaDTO> CategoriaReg = null;
		List<Tblcategoria> listado = null;
		TblcategoriaHome acceso = new TblcategoriaHome();
		try {
			listado=acceso.getList(ini, fin);
			CategoriaReg = new ArrayList<CategoriaDTO>();
            if (listado != null) {
                for (Tblcategoria Categoria : listado) {
                	CategoriaReg.add(crearCategoriaReg(Categoria));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CategoriaReg;
	}
	
	public List<CategoriaDTO> listarCategoriaCombo(int ini, int fin) {
		List<CategoriaDTO> CategoriaReg = null;
		List<Tblcategoria> listado = null;
		TblcategoriaHome acceso = new TblcategoriaHome();
		try {
			listado=acceso.getListCombo(ini, fin);
			CategoriaReg = new ArrayList<CategoriaDTO>();
            if (listado != null) {
                for (Tblcategoria Categoria : listado) {
                	CategoriaReg.add(crearCategoriaReg(Categoria));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CategoriaReg;
	}
	
	
	
	public int numeroRegistrosBodega(String tabla) {
			
			TblbodegaHome accesobodega = new TblbodegaHome();
			int registros=0;
			try {
					registros=accesobodega.count(tabla);
			} catch (RuntimeException re) {
				throw re;	
			} finally {
			}
			return registros;
		}
	@Override
	public List<CategoriaDTO> listarCategoriaLike(String busqueda, String campo) {
		List<CategoriaDTO> CategoriaReg = null;
		List<Tblcategoria> listado = null;
		TblcategoriaHome acceso = new 	TblcategoriaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			CategoriaReg = new ArrayList<CategoriaDTO>();
            if (listado != null) {
                for (Tblcategoria Categoria : listado) {
                	CategoriaReg.add(crearCategoriaReg(Categoria));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CategoriaReg;
	}
	@Override
	public int numeroRegistrosCategoria(String tabla) {
		TblcategoriaHome accesocategoria = new TblcategoriaHome();
		int registros=0;
		try {
				registros=accesocategoria.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	//METODOS PARA BODEGA //////////////////////////////////////////////////////////
	
	@Override
	public String grabarBodega(BodegaDTO entidad){
		Tblbodega tblbodega =new Tblbodega();
		TblbodegaHome accesobodega = new TblbodegaHome();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tblbodega=accesobodega.findByNombre(entidad.getBodega(),"nombre");
				ban=1;
				mensaje="Bodega ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesobodega.grabar(new Tblbodega(entidad));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje=e.getMessage();
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String modificarBodega(BodegaDTO entidad) {
		TblbodegaHome accesoBodega = new TblbodegaHome();
		String mensaje = "";
		try {
			try {
				accesoBodega.modificar(new Tblbodega(entidad));
				mensaje = "Bodega modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String eliminarBodega(BodegaDTO bodega) {
		TblbodegaHome accesbodega = new TblbodegaHome();
		String mensaje = "";
		try {
			try {
				accesbodega.eliminacionFisica(new Tblbodega(bodega));
				mensaje = "Transacci\u00f3n realizada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la Bodega porque est\u00e1 siendo utilizada en un documento";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public BodegaDTO buscarBodega(String idBodega) {
		TblbodegaHome accesobodega = new TblbodegaHome();
		Tblbodega tblbodega= new Tblbodega();
		BodegaDTO bodega= new BodegaDTO();
		bodega=null;
		String mensaje = "";
		try {
			try {
				//System.out.println(idBodega);
				tblbodega=accesobodega.findById(Integer.valueOf(idBodega));
				bodega=new BodegaDTO(tblbodega.getIdBodega(),tblbodega.getNombre()
						,tblbodega.getUbicacion(),tblbodega.getTelefono(),tblbodega.getIdEmpresa(),tblbodega.getEstablecimiento());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return bodega;
	}
	
	public BodegaDTO crearBodegaReg(Tblbodega cat){
		return new BodegaDTO(cat.getIdBodega(), cat.getNombre(),cat.getUbicacion(),
				cat.getTelefono(), cat.getIdEmpresa(),cat.getEstablecimiento());
	}
	@Override
	public List<BodegaDTO> listarBodega(int ini, int fin) {
		System.out.print("LISTADO DE TODAS LAS BODEGAS");
		List<BodegaDTO> BodegaReg = null;
		List<Tblbodega> listado = null;
		TblbodegaHome acceso = new 	TblbodegaHome();
		try {
			listado=acceso.getList(ini, fin);
			BodegaReg = new ArrayList<BodegaDTO>();
            if (listado != null) {
                for (Tblbodega Bodega : listado) {
                	BodegaReg.add(crearBodegaReg(Bodega));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return BodegaReg;
	}
	
	public List<BodegaDTO> listarBodegaProducto(int ini, int fin, int idProducto) {
		List<BodegaDTO> BodegaReg = null;
		List<Tblbodega> listado = null;
		TblbodegaHome acceso = new 	TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		try {
			listado=acceso.getList(ini, fin);
			BodegaReg = new ArrayList<BodegaDTO>(); 
            if (listado != null) {
                for (Tblbodega Bodega : listado) {
                	TblproductoTblbodega tblproductotblbodega = tblproductotblbodegahome.findByQueryUnique("where u.tblbodega.idBodega='"+Bodega.getIdBodega()+"' and u.tblproducto.idProducto='"+idProducto+"'");
                	BodegaDTO bodegadto= crearBodegaReg(Bodega);
                	System.out.println("Bodega-> "+tblproductotblbodega);
                	//System.out.println(bodegadto.getCantidad());
                	if(tblproductotblbodega!=null){
                		bodegadto.setCantidad(tblproductotblbodega.getCantidad());
                	}else{
                		bodegadto.setCantidad(null);
                	}
                	BodegaReg.add(bodegadto);                	
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		//System.out.print(BodegaReg.size());
		return BodegaReg;
	}
	
	@Override
	public List<BodegaDTO> listarBodegaLike(String busqueda, String campo) {
		List<BodegaDTO> BodegaReg = null;
		List<Tblbodega> listado = null;
		TblbodegaHome acceso = new 	TblbodegaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			BodegaReg = new ArrayList<BodegaDTO>();
            if (listado != null) {
                for (Tblbodega Bodega : listado) {
                	BodegaReg.add(crearBodegaReg(Bodega));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return BodegaReg;
	}

	
	//METODOS PARA PRODUCTO//////////////////////////////////
	/*public List<ProductoDTO> listaProductosBodega(Integer inicio, Integer numReg,String bodega) {
		List<ProductoDTO> ProductosReg = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			listado=acceso.findJoinPro(nombre, "TblproductoTblbodega", "");
			ProductosReg = new ArrayList<ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {
                    ProductosReg.add(crearProductoReg(Producto));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	}*/
	
	@Override
	public LinkedHashMap<Integer,ProductoDTO> listarProductoLike(String busqueda, String campo) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			listado=acceso.findLikePro(busqueda, campo);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {
                	ProductoDTO productodto = new ProductoDTO();
                	productodto=crearProductoReg(Producto, -1);
                	System.out.println("Producto creado like "+Producto.getDescripcion());
                    ProductosReg.put(productodto.getIdProducto(),productodto); 
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	
	}
	
	//FILTRO PARA PRODUCTOS ELIMINADOS Y BUSQUEDA DE CAMPOS SOLO CON ELIMINADOS
	public LinkedHashMap<Integer,ProductoDTO> listarProductoLikeEliminados(String busqueda, String campo,int opcion) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;  
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			if(campo.equals("descripcion"))
			{
				busqueda=busqueda.replace('%', ' ');
				boolean espBlanco=true;
                while(espBlanco==true)
                {
                    int longitud=busqueda.length();
                    if(longitud>0)
                    {
                    	if(busqueda.charAt(longitud-1)==' ')
	                    {
	                       busqueda=busqueda.substring(0,longitud-1);
	                    }
	                    else
	                    {espBlanco=false;}
                    }
                    else
                    {espBlanco=false;}	
                }
				String busquedaAvanzada=" like '%";
				
				for(int i=0;i<busqueda.length()-1;i++)
				{
					if(busqueda.charAt(i)==(' '))
					{
						busquedaAvanzada=busquedaAvanzada+"%' and u.descripcion like '%";
					}else	
					{	
							busquedaAvanzada=busquedaAvanzada+busqueda.charAt(i);
					}					
				}
				if(busqueda.length()>0)
				{	
					busquedaAvanzada=busquedaAvanzada+busqueda.charAt(busqueda.length()-1)+"%'";
				}
				else
				{
					busquedaAvanzada=busquedaAvanzada+"%'";
				}	
			//	System.out.println("busquedaAvanzada "+busquedaAvanzada);
				listado=acceso.findLikeProDescripcionEliminados(busquedaAvanzada, campo,opcion);
			}else
			{
				listado=acceso.findLikeProEliminados(busqueda, campo,opcion);
			}
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {
                    ProductosReg.put(Producto.getIdProducto(),crearProductoReg(Producto, -1)); 
    				System.out.println("Producto creado likeelim "+Producto.getDescripcion());
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	
	}
	
	
	//++++++++++++++++++  PARA QUE BUSQUE TODOS LOS PRODUCTOS EN EL frmProducto ++++++++++++++++++++++++
	public LinkedHashMap<Integer,ProductoDTO> listarProductoLike2(String busqueda, String campo,int stockProd, int Tipo, int Jerarquia, int TipoPrecio,int StockNegativo) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;  
		List<BodegaDTO> listbodegadto = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		TblbodegaHome tblbodegahome = new TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		try {
			if(campo.equals("descripcion"))
			{
				busqueda=busqueda.replace('%', ' ');
				boolean espBlanco=true;
                while(espBlanco==true)
                {
                    int longitud=busqueda.length();
                    if(longitud>0){
                    	if(busqueda.charAt(longitud-1)==' '){
	                       busqueda=busqueda.substring(0,longitud-1);
	                    }
	                    else
	                    {espBlanco=false;}
                    }
                    else
                    {espBlanco=false;}	
                }
				String busquedaAvanzada=" like '%";
				
				for(int i=0;i<busqueda.length()-1;i++)
				{
					if(busqueda.charAt(i)==(' '))
					{
						busquedaAvanzada=busquedaAvanzada+"%' and u.descripcion like '%";
					}else	
					{	
							busquedaAvanzada=busquedaAvanzada+busqueda.charAt(i);
					}					
				}
				if(busqueda.length()>0)
				{	
					busquedaAvanzada=busquedaAvanzada+busqueda.charAt(busqueda.length()-1)+"%'";
				}
				else
				{
					busquedaAvanzada=busquedaAvanzada+"%'";
				}	
			//	System.out.println("busquedaAvanzada "+busquedaAvanzada);
				listado=acceso.findLikeProDescripcion(busquedaAvanzada, campo,stockProd, Tipo , Jerarquia);
			}else
			{
				listado=acceso.findLikePro2(busqueda, campo,stockProd , Tipo , Jerarquia);
			}
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {
                	ProductoDTO productodto=crearProductoReg(Producto, TipoPrecio);
    				System.out.println("Producto creado like2 "+Producto.getDescripcion());
                	if (productodto!=null){
                	List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+Producto.getIdProducto());
                	if(listtblproductotblbodega != null){	
                		listbodegadto = new ArrayList<BodegaDTO>();
                		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
                			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	                		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	                		BodegaDTO bodegadto =crearBodega(tblbodega);
	                		listbodegadto.add(bodegadto);   
                			}
                		}
                		
                	}
                	//root(√(16*x^(6)),3)
                	productodto.setTblbodega(listbodegadto);
                	ProductosReg.put(productodto.getIdProducto(), productodto);
                	}
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	
	}
	
	
	public LinkedHashMap<Integer,ProductoDTO> listarProductoMateriaPrimaLike(String busqueda, String campo,int stockProd) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			if(campo.equals("descripcion"))
			{
				busqueda=busqueda.replace('%', ' ');
				boolean espBlanco=true;
                while(espBlanco==true)
                {
                    int longitud=busqueda.length();
                    if(longitud>0)
                    {
                    	if(busqueda.charAt(longitud-1)==' ')
	                    {
	                       busqueda=busqueda.substring(0,longitud-1);
	                    }
	                    else
	                    {espBlanco=false;}
                    }
                    else
                    {espBlanco=false;}	
                }
				String busquedaAvanzada=" like '%";
				
				for(int i=0;i<busqueda.length()-1;i++)
				{
					if(busqueda.charAt(i)==(' '))
					{
						busquedaAvanzada=busquedaAvanzada+"%' and u.descripcion like '%";
					}else	
					{	
							busquedaAvanzada=busquedaAvanzada+busqueda.charAt(i);
					}					
				}
				if(busqueda.length()>0)
				{	
					busquedaAvanzada=busquedaAvanzada+busqueda.charAt(busqueda.length()-1)+"%'";
				}
				else
				{
					busquedaAvanzada=busquedaAvanzada+"%'";
				}	
			//	System.out.println("busquedaAvanzada "+busquedaAvanzada);
				listado=acceso.findLikeProDescripcion(busquedaAvanzada, campo,stockProd, 1, 1);//REVISAR HECHO AL PURO
			}else
			{
				listado=acceso.findLikeProductoMateriaPrima(busqueda, campo,stockProd);
			}
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {
                    ProductosReg.put(Producto.getIdProducto(),crearProductoReg(Producto, -1)); 
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	
	}
	
	
	@Override
	public int numeroRegistrosProducto(String tabla) {
		TblproductoHome acceso = new TblproductoHome();
		int registros=0;
		try {
				registros=acceso.countProd(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	@Override
	/**
	 * Metodo para buscar productos seg�n la caegoria,marca,unidad
	 */

	public LinkedHashMap<Integer,ProductoDTO> listarProductoJoin(String nombre,String tabla, String campo, Integer Tipo, Integer Jerarquia, Integer TipoPrecio,int StockNegativo ) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;  
		TblproductoHome acceso = new TblproductoHome();
		TblbodegaHome tblbodegahome = new TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		List<BodegaDTO> listbodegadto = null;
		if(tabla.equals("tblbodegas")){
			Iterator res=acceso.ListJoinProBod(nombre,0,0, Tipo, Jerarquia);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
			while ( res.hasNext() ) {
				Object[] pair = (Object[])res.next();
				TblproductoTblbodega pb=new TblproductoTblbodega();
				pb=(TblproductoTblbodega) pair[0];
				Tblproducto producto = (Tblproducto) pair[2];
				Tblbodega bod=(Tblbodega) pair[1];
				ProductoDTO productoD = new ProductoDTO();
				productoD = crearProductoReg(producto, TipoPrecio);
				System.out.println("Producto creado join "+producto.getDescripcion());
				if (productoD!=null){
				Float stock=(float) pb.getCantidad();
				productoD.setStock(stock);
				List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+producto.getIdProducto());
            	if(listtblproductotblbodega != null){	
            		listbodegadto = new ArrayList<BodegaDTO>();
            		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
            			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	            		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	            		BodegaDTO bodegadto =crearBodega(tblbodega);
	            		listbodegadto.add(bodegadto);
            			}
            		}
            	}
            	productoD.setTblbodega(listbodegadto);
            	ProductosReg.put(productoD.getIdProducto(), productoD);
				}
			}
		}else{
			Iterator resultado = acceso.findJoinPro(nombre, tabla, campo, Tipo, Jerarquia);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblproducto producto = (Tblproducto) pair[0];
				ProductoDTO productoD = new ProductoDTO();
				if(tabla.equals("tblcategoria")){
					Tblcategoria categoria = (Tblcategoria) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblmarca(producto.getTblmarca().getIdMarca());
					productoD.setTblunidad(producto.getTblunidad().getIdUnidad());
					CategoriaDTO cat=new CategoriaDTO();
					cat.setIdEmpresa(categoria.getIdEmpresa());
					cat.setIdCategoria(categoria.getIdCategoria());
					cat.setCategoria(categoria.getCategoria());
					productoD.setTblcategoria(cat.getIdCategoria());
					//list.add(productoD);
				}else if(tabla.equals("tblunidad")){
					Tblunidad unidad = (Tblunidad) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblmarca(producto.getTblmarca().getIdMarca());
					productoD.setTblcategoria(producto.getTblcategoria().getIdCategoria());
					UnidadDTO uni=new UnidadDTO();
					uni.setIdUnidad(unidad.getIdUnidad());
					uni.setUnidad(unidad.getNombre());
					productoD.setTblunidad(uni.getIdUnidad());
					//list.add(productoD); 
				}
				else if(tabla.equals("tblmarca")){
					Tblmarca marca = (Tblmarca) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblcategoria(producto.getTblcategoria().getIdCategoria());
					productoD.setTblunidad(producto.getTblunidad().getIdUnidad());
					MarcaDTO mar=new MarcaDTO();
					mar.setIdEmpresa(marca.getIdEmpresa());
					mar.setIdMarca(marca.getIdMarca());
					mar.setMarca(marca.getMarca());
					productoD.setTblmarca(mar.getIdMarca());
					//list.add(productoD);	
				}
				List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+producto.getIdProducto());
            	if(listtblproductotblbodega != null){	
            		listbodegadto = new ArrayList<BodegaDTO>();
            		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
            			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	            		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	            		BodegaDTO bodegadto =crearBodega(tblbodega);
	            		listbodegadto.add(bodegadto);
            			}
            		}
            	}
            	productoD.setTblbodega(listbodegadto);
            	ProductosReg.put(productoD.getIdProducto(), productoD);
			}
		}
			
		
		
		return ProductosReg;
	}
	public String impresion(String canvas){
		System.out.println("Entro a impresion");
		PrinterJob job = PrinterJob.getPrinterJob();
		Canvas canv= new Canvas();
		canv.setContents(canvas.replace("\\/", "/"));
		System.out.println("Entro a impresion3");
		job.setPrintable((Printable) canv);
		System.out.println("Entro a impresion4");
		String mensaje="";
		try {
			job.print();
			mensaje="Impreso correcto";
			System.out.println(mensaje);
		} catch (PrinterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mensaje="No se imprimio: "+e.getMessage();
			System.out.println(mensaje);
		}
		return (mensaje);
	}
	//++++++++++++++++++  PARA QUE BUSQUE TODOS LOS PRODUCTOS EN EL frmProducto ++++++++++++++++++++++++ 
	public LinkedHashMap<Integer,ProductoDTO> listarProductoJoin2(String nombre,String tabla, String campo,int stockProd, int Tipo, int Jerarquia, int TipoPrecio, int StockNegativo) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
		TblproductoHome acceso = new TblproductoHome();
		TblbodegaHome tblbodegahome = new TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		List<BodegaDTO> listbodegadto = null;
		if(tabla.equals("tblbodegas")){
			Iterator res;
			/*if (BodegaUbicacion==1){
				res=acceso.ListJoinProBodUbic2(nombre,0,campo,stockProd, Tipo, Jerarquia);
			}else{
				res=acceso.ListJoinProBod2(nombre,0,campo,stockProd, Tipo, Jerarquia);
			}*/
			System.out.println("Producto join 2 "+nombre+" "+campo);
			res=acceso.ListJoinProBod2(nombre,0,campo,stockProd, Tipo, Jerarquia);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>(0);
			while ( res.hasNext() ) {
				Object[] pair = (Object[])res.next();
				TblproductoTblbodega pb=new TblproductoTblbodega();
				pb=(TblproductoTblbodega) pair[0];
				Tblproducto producto = (Tblproducto) pair[2];
				Tblbodega bod=(Tblbodega) pair[1];
				ProductoDTO productoD = new ProductoDTO();
				productoD = crearProductoReg(producto, TipoPrecio);
				System.out.println("Producto creado join2 "+producto.getDescripcion());
				if (productoD!=null){
				Float stock=(float) pb.getCantidad();
				productoD.setStock(stock);
				List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+producto.getIdProducto());
            	if(listtblproductotblbodega != null){	
            		listbodegadto = new ArrayList<BodegaDTO>();
            		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
            			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	            		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	            		BodegaDTO bodegadto =crearBodega(tblbodega);
	            		listbodegadto.add(bodegadto);
            			}
            		}
            	}
            	productoD.setTblbodega(listbodegadto);
            	ProductosReg.put(productoD.getIdProducto(), productoD);
				}
			}
		}else{
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
			Iterator resultado = acceso.findJoinPro2(nombre, tabla, campo,stockProd, Tipo, Jerarquia);
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblproducto producto = (Tblproducto) pair[0];
				ProductoDTO productoD = new ProductoDTO();
				if(tabla.equals("tblcategoria")){
/*					Tblcategoria categoria = (Tblcategoria) pair[1];
					ProductoDTO productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblmarca(producto.getTblmarca().getIdMarca());
					productoD.setTblunidad(producto.getTblunidad().getIdUnidad());
					CategoriaDTO cat=new CategoriaDTO();
					cat.setIdCategoria(categoria.getIdCategoria());
					cat.setCategoria(categoria.getCategoria());
					productoD.setTblcategoria(cat.getIdCategoria());
					list.add(productoD);*/
					Tblcategoria pb=new Tblcategoria();
					pb=(Tblcategoria) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoReg(producto, TipoPrecio);
					CategoriaDTO cat=new CategoriaDTO();
					cat.setIdEmpresa(pb.getIdEmpresa());
					cat.setIdCategoria(pb.getIdCategoria());
					cat.setCategoria(pb.getCategoria());
					productoD.setTblcategoria(cat.getIdCategoria());
					//list.add(productoD);
				}else if(tabla.equals("tblunidad")){
/*					Tblunidad unidad = (Tblunidad) pair[1];
					ProductoDTO productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblmarca(producto.getTblmarca().getIdMarca());
					productoD.setTblcategoria(producto.getTblcategoria().getIdCategoria());
					UnidadDTO uni=new UnidadDTO();
					uni.setIdUnidad(unidad.getIdUnidad());
					uni.setUnidad(unidad.getNombre());
					productoD.setTblunidad(uni.getIdUnidad());
					list.add(productoD); */
					Tblunidad pb=new Tblunidad();
					pb=(Tblunidad) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoReg(producto, TipoPrecio);
					UnidadDTO uni=new UnidadDTO();
					uni.setIdUnidad(pb.getIdUnidad());
					uni.setUnidad(pb.getNombre());
					productoD.setTblunidad(uni.getIdUnidad());
					//list.add(productoD);

				}
				else if(tabla.equals("tblmarca")){
/*					Tblmarca marca = (Tblmarca) pair[1];
					ProductoDTO productoD = new ProductoDTO();
					productoD = crearProductoDTO(producto);
					productoD.setTblcategoria(producto.getTblcategoria().getIdCategoria());
					productoD.setTblunidad(producto.getTblunidad().getIdUnidad());
					MarcaDTO mar=new MarcaDTO();
					mar.setIdMarca(marca.getIdMarca());
					mar.setMarca(marca.getMarca());
					productoD.setTblmarca(mar.getIdMarca());
					list.add(productoD);	*/
					
					Tblmarca pb=new Tblmarca();
					pb=(Tblmarca) pair[1];
					productoD = new ProductoDTO();
					productoD = crearProductoReg(producto, TipoPrecio);
					MarcaDTO mar=new MarcaDTO();
					mar.setIdEmpresa(pb.getIdEmpresa());
					mar.setIdMarca(pb.getIdMarca());
					mar.setMarca(pb.getMarca());
					productoD.setTblmarca(mar.getIdMarca());
					//Float stock=(float) producto..getCantidad();
					//productoD.setStock(stock);
					//list.add(productoD);
						
				}
				
				List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+producto.getIdProducto());
            	if(listtblproductotblbodega != null){	
            		listbodegadto = new ArrayList<BodegaDTO>();
            		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
            			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
                		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
                		BodegaDTO bodegadto =crearBodega(tblbodega);
                		listbodegadto.add(bodegadto);     
            			}
            		}
            		
            	}
            	//root(√(16*x^(6)),3)
    	productoD.setTblbodega(listbodegadto);
    	ProductosReg.put(productoD.getIdProducto(),productoD);
			}
		}
			
		
		
		return ProductosReg;
	}

	/**
	 * M�todo para buscar un producto en cierta bodega
	 * @param bodega
	 * @param descripcion
	 * @return
	 */
	public LinkedHashMap<Integer,ProductoDTO> BuscarProductoJoin(String bodega,String descripcion) {
		LinkedHashMap<Integer,ProductoDTO> list = new LinkedHashMap<Integer,ProductoDTO>();
		TblproductoHome acceso = new TblproductoHome();
		
		Iterator res=acceso.BuscarJoinProBod(bodega,descripcion);
		while ( res.hasNext() ) {
			Object[] pair = (Object[])res.next();
			TblproductoTblbodega pb=new TblproductoTblbodega();
			pb=(TblproductoTblbodega) pair[0];
			Tblproducto producto = (Tblproducto) pair[2];
			Tblbodega bod=(Tblbodega) pair[1];
			ProductoDTO productoD = new ProductoDTO();
			Float stock=(float) pb.getCantidad();
			productoD = crearProductoReg(producto, -1);
			productoD.setStock(stock);
			/*productoD.setTblcategoria(producto.getTblcategoria().getIdCategoria());
			productoD.setTblunidad(producto.getTblunidad().getIdUnidad());
			productoD.setTblmarca(producto.getTblmarca().getIdMarca());
			MarcaDTO mar=new MarcaDTO();
			productoD.setTblmarca(mar.getIdMarca());*/
			//System.out.println("id Prod= "+productoD.getIdProducto()+" Stock= "+producto.getStock());
			list.put(productoD.getIdProducto(), productoD);
		}
		return list;
	}
	@Override
	public String grabarProducto(ProductoDTO pro) {
		String mensaje="";
		System.out.println("Entro a grabar Produc");
		if(this.buscarProducto(pro.getCodigoBarras(),"codigoBarras")==null){
			Set<Tblbodega> bodegas = new HashSet<Tblbodega>();
			Tblbodega bodega=new Tblbodega();
			Set<Tbltipoprecio> precios = new HashSet<Tbltipoprecio>();
			ProductoTipoPrecioDTO[] preciosL = new ProductoTipoPrecioDTO[pro.getProTip().size()];
			Tbltipoprecio tipo=new Tbltipoprecio();
				try {
					System.out.println("1");
					Tblcategoria cat=new Tblcategoria();
					cat.setIdCategoria(pro.getTblcategoria());
					System.out.println("2");
					Tblunidad unidad=new Tblunidad();
					unidad.setIdUnidad(pro.getTblunidad());
					System.out.println("3");
					Tblmarca marca =new Tblmarca();
					marca.setIdMarca(pro.getTblmarca());
					System.out.println("4");
					Tblproducto producto = new Tblproducto(pro);
					producto.setTblcategoria(cat);
					producto.setTblmarca(marca);
					producto.setTblunidad(unidad);
					System.out.println("GrabarprodBodeg");
					//SC.say(String.valueOf(pro.getTblbodega().size()));
					for(int i=0;i<pro.getTblbodega().size();i++){
						bodega =new Tblbodega(pro.getTblbodega().get(i));
						bodegas.add(bodega);
					}
					for(int i=0;i<pro.getProTip().size();i++){
						System.out.println("Tipo precio "+pro.getProTip().get(i).getTbltipoprecio().getIdTipoPrecio());
						tipo=new Tbltipoprecio(pro.getTbltipoprecio().get(i));
						precios.add(tipo);
						preciosL[i]=pro.getProTip().get(i);
					}					
					
					producto.setTblbodegas(bodegas);
					producto.setTbltipoprecios(precios);
					
					Set<TblproductoMultiImpuesto> prodMultiImp=multiImpuestoDTOtoMultiImp(pro.getTblmultiImpuesto());
					Set<TblproductoMultiImpuesto> prodMultiImpN=new HashSet(0);
					Set<TblmultiImpuesto> multiImpSet=new HashSet(0);
					for (TblproductoMultiImpuesto multImp:prodMultiImp) {
						multImp.setTblproducto(producto);
						prodMultiImpN.add(multImp);
						multiImpSet.add(multImp.getTblmultiImpuesto());
					}
//					producto.setTblProdMultiImpuesto(prodMultiImpN);
					producto.setTblmultiimpuestos(multiImpSet);
					System.out.println("5");
					TblproductoHome prod = new TblproductoHome();
					//mensaje="bodega encontrada "+bodega.getNombre()+" "+tipo.getIdTipoPrecio()+ " "+tipo.getTipoPrecio();
					prod.grabar(producto);
					//System.out.println(this.grabarProductoTipoPrecio(preciosL,preciosL.length));
					System.out.println("6");
					mensaje="Producto Grabado";
				} catch (ErrorAlGrabar e) {
					e.printStackTrace(System.out);
					mensaje = "Error "+e.toString();
				}
			
		}
		//si existe un producto dado de baja
		else if(this.buscarProductoEliminado(pro.getCodigoBarras(),"codigoBarras")!=null){
			mensaje="baja";			 	
		}
		else{
			
			mensaje="El producto ya existe";
		}
			return mensaje;
	}
	public String modificarProducto(ProductoDTO pro) {
		String mensaje="";
		Set<Tblbodega> bodegas = new HashSet<Tblbodega>();
		Tblbodega bodega=new Tblbodega();
		Set<Tbltipoprecio> precios = new HashSet<Tbltipoprecio>();
		Tbltipoprecio tipo=new Tbltipoprecio();
		boolean ban=false;
		TblproductoHome prod = new TblproductoHome();
		if(prod.findById(pro.getIdProducto()).getCodigoBarras().equals(pro.getCodigoBarras())){
			ban=true;
		}else{
			if(this.buscarProducto(pro.getCodigoBarras(),"codigoBarras")==null){
				ban=true;
			}
		}
		if(ban){
			try {
				Tblproducto producto = new Tblproducto();				
				producto=prod.findById(pro.getIdProducto());
				Tblcategoria cat=new Tblcategoria();
				cat.setIdCategoria(pro.getTblcategoria());
				Tblunidad unidad=new Tblunidad();
				unidad.setIdUnidad(pro.getTblunidad());
				Tblmarca marca =new Tblmarca();
				marca.setIdMarca(pro.getTblmarca());
				producto.setTblcategoria(cat);
				producto.setTblmarca(marca);
				producto.setTblunidad(unidad);
				//producto.setStock(pro.getStock());
				producto.setCodigoBarras(pro.getCodigoBarras());
				producto.setDescripcion(pro.getDescripcion());
				
//				producto.setImpuesto(pro.getImpuesto());
				producto.setPromedio(pro.getPromedio()); //cambiar luego
				producto.setFifo(pro.getFifo());
				producto.setLifo(pro.getLifo());
				producto.setImagen(pro.getImagen());
				producto.setDescuento(pro.getDescuento());
				producto.setCantidadunidad(pro.getCantidadunidad());

				Set<TblproductoMultiImpuesto> prodMultiImp=multiImpuestoDTOtoMultiImp(pro.getTblmultiImpuesto());
				Set<TblproductoMultiImpuesto> prodMultiImpN=new HashSet(0);
				Set<TblmultiImpuesto> multiImpSet=new HashSet(0);
				for (TblproductoMultiImpuesto multImp:prodMultiImp) {
					multImp.setTblproducto(producto);
					prodMultiImpN.add(multImp);
					multiImpSet.add(multImp.getTblmultiImpuesto());
				}
//				producto.setTblProdMultiImpuesto(prodMultiImpN);
				producto.setTblmultiimpuestos(multiImpSet);
				//mensaje="bodega encontrada "+bodega.getNombre()+" "+tipo.getIdTipoPrecio()+ " "+tipo.getTipoPrecio();
				
				prod.modificar(producto);
				mensaje="Producto Modificado";
			} catch (ErrorAlGrabar e) {
				mensaje = e.toString();
			}
		}else{
			mensaje="Error C�digo de Barras Repetido";
		}
		return mensaje;
}
	public Set<TblproductoMultiImpuesto> multiImpuestoDTOtoMultiImp(Set<TblproductoMultiImpuestoDTO> multiDTO ){
		Set<TblproductoMultiImpuesto> tblmultiImpuesto= new HashSet(0);
		for(TblproductoMultiImpuestoDTO multImpDTO:multiDTO){
			TblproductoMultiImpuesto multiImp= new TblproductoMultiImpuesto();
			multiImp.setIdtblproducto_tblmulti_impuesto(multImpDTO.getIdtblproducto_tblmulti_impuesto());
			multiImp.setTblmultiImpuesto(new TblmultiImpuesto(multImpDTO.getTblmultiImpuesto()));
			tblmultiImpuesto.add(multiImp);
		}
		return tblmultiImpuesto;
	}
	
	public String modificarProductoEliminado(ProductoDTO pro) {
		String mensaje="";
		
		boolean ban=false;
		TblproductoHome prod = new TblproductoHome();
		
			if(this.buscarProducto(pro.getCodigoBarras(),"codigoBarras")!=null){
				ban=true;
			}
			
		if(ban){
			try {
				Tblproducto producto = new Tblproducto(pro);
				
				//producto=prod.findById(pro.getIdProducto());
				producto=prod.findByNombre(pro.getCodigoBarras(),"codigoBarras");
				Tblcategoria cat=new Tblcategoria();
				cat.setIdCategoria(pro.getTblcategoria());
				Tblunidad unidad=new Tblunidad();
				unidad.setIdUnidad(pro.getTblunidad());
				Tblmarca marca =new Tblmarca();
				marca.setIdMarca(pro.getTblmarca());
				producto.setTblcategoria(cat);
				producto.setTblunidad(unidad);
				producto.setTblmarca(marca);
				producto.setCodigoBarras(pro.getCodigoBarras());
				producto.setDescripcion(pro.getDescripcion());
				producto.setStock(pro.getStock());
				producto.setFifo(pro.getFifo());
				producto.setLifo(pro.getLifo());				
				producto.setPromedio(pro.getPromedio()); //cambiar luego
				producto.setEstado('1');//ESTO DA DE ALTA NUEVAMENTE A LOS PRODUCTOS
				producto.setTipo(pro.getTipo());
				producto.setDescuento(pro.getDescuento());
				pro.setTblmultiImpuesto(crearSetProductoMultiImpuestoDTO(producto.getTblProdMultiImpuesto()));
//				producto.setImpuesto(pro.getImpuesto());
				producto.setImagen(pro.getImagen());
				producto.setJerarquia(pro.getJerarquia());
				producto.setCantidadunidad(pro.getCantidadunidad());
				prod.modificar(producto);
				mensaje="Producto dado de Alta exitosamente";
			} catch (ErrorAlGrabar e) {
				mensaje = e.toString();
				//System.out.println("error 1");
			}
		}else{
			mensaje="Error C�digo de Barras Repetido ERROR 2";
			//System.out.println("error 2");
		}
		return mensaje;
}
	
	@Override
	public String eliminarProducto(ProductoDTO pro) {
		String mensaje="";
		Tblproducto producto=new Tblproducto();
		TblproductoHome accesoproducto=new TblproductoHome();
		producto=(Tblproducto) accesoproducto.findByNombre(pro.getCodigoBarras(),"codigoBarras");
		if(producto!=null){
			producto.setEstado('0');
			try {
				accesoproducto.modificar(producto);
				mensaje="Producto Eliminado";
			} catch (ErrorAlGrabar e) {
				//mensaje=e.getMessage();
				mensaje="No se puede eliminar el Producto porque esta siendo utilizado en otro documento";
			}
		}else{
			mensaje="Producto No encontrado";
		}
		return mensaje;
	}
	@Override
	public ProductoDTO buscarProducto(String nombre,String campo) {
		TblproductoHome accesoproducto = new TblproductoHome();
		Tblproducto tblproducto= new Tblproducto();
		ProductoDTO producto= new ProductoDTO();
		producto=null;
		String mensaje = "";
		try {
			try {
				tblproducto=(Tblproducto) accesoproducto.findByNombre(nombre,campo);
				producto=crearProductoReg(tblproducto, -1);
				/*producto.setCodigoBarras(tblproducto.getCodigoBarras());
				producto.setDescripcion(tblproducto.getDescripcion());
				producto.setFifo(tblproducto.getFifo());
				producto.setIdProducto(tblproducto.getIdProducto());
				producto.setImpuesto(tblproducto.getImpuesto());
				producto.setLifo(tblproducto.getLifo());
				producto.setPromedio(tblproducto.getPromedio());
				producto.setStock(tblproducto.getStock());
				*/
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con exito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		//System.out.println(producto.getProTip().size());
		return producto;
	}
	public ProductoDTO buscarProductoCodBar(String nombre,String campo, Integer Tipo, int StockNegativo) {
		List<BodegaDTO> listbodegadto = null;
		TblproductoHome accesoproducto = new TblproductoHome();
		TblbodegaHome tblbodegahome = new TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		Tblproducto tblproducto= new Tblproducto();
		ProductoDTO producto= new ProductoDTO();
		producto=null;
		String mensaje = "";
		try {
			try {
				tblproducto=(Tblproducto) accesoproducto.findByNombre(nombre,campo);
				producto=crearProductoReg(tblproducto, Tipo);
				/*producto.setCodigoBarras(tblproducto.getCodigoBarras());
				producto.setDescripcion(tblproducto.getDescripcion());
				producto.setFifo(tblproducto.getFifo());
				producto.setIdProducto(tblproducto.getIdProducto());
				producto.setImpuesto(tblproducto.getImpuesto());
				producto.setLifo(tblproducto.getLifo());
				producto.setPromedio(tblproducto.getPromedio());
				producto.setStock(tblproducto.getStock());
				*/
				List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+tblproducto.getIdProducto());
                	//System.out.println(listtblproductotblbodega.size());
                	//System.out.println(Factum.banderaStockNegativo==1);
                	if(listtblproductotblbodega != null){	
                		listbodegadto = new ArrayList<BodegaDTO>();
                		
                		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
                			
                			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	                		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	                		BodegaDTO bodegadto =crearBodega(tblbodega);
	                		listbodegadto.add(bodegadto);
                			}
                		}
                	}
                	producto.setTblbodega(listbodegadto);		
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con exito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return producto;
	}
	/*public ProductoDTO buscarProductoCodBar(String nombre,String campo, Integer Tipo) {
		TblproductoHome accesoproducto = new TblproductoHome();
		Tblproducto tblproducto= new Tblproducto();
		ProductoDTO producto= new ProductoDTO();
		producto=null;
		String mensaje = "";
		try {
			try {
				tblproducto=(Tblproducto) accesoproducto.findByNombre(nombre,campo);
				producto=crearProductoReg(tblproducto, Tipo);
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con exito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return producto;
	}*/
	
	
	@Override
	public ProductoDTO buscarProductoEliminado(String nombre, String campo) {
		TblproductoHome accesoproducto = new TblproductoHome();
		Tblproducto tblproducto= new Tblproducto();
		ProductoDTO producto= new ProductoDTO();
		producto=null;
		String mensaje = "";
		try {
			try {
				tblproducto=(Tblproducto) accesoproducto.findByNombreEliminado(nombre,campo);
				producto=crearProductoReg(tblproducto, -1);
					
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con exito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return producto;
	}

	
	@Override
	public List<ProductoDTO> listarProducto(int ini, int fin) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public float ValorDelInventario(){
		TblproductoHome acceso=new TblproductoHome();
		return acceso.ValorDelInventario();
	}
	
	// FUNCIONES AREA////////////////////////////////////////////////////////////////
	/**
	 * M�todo para grabar una Area
	 * @param AreaDTO
	 */
	
	public String grabarArea(AreaDTO entidad) throws IllegalArgumentException {
		TblareaHome accesoarea = new TblareaHome();
		String mensaje = "";
		Tblarea tblarea= new Tblarea();
		
		int ban=0;
		try {
			try {
				tblarea=accesoarea.findByNombre(entidad.getNombre(),"nombre");
				ban=1;
				mensaje="area ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesoarea.grabar(new Tblarea(entidad.getIdEmpresa(),entidad.getEstablecimiento(),entidad.getNombre(), entidad.getImagen(), entidad.getImagenOcupado()));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public String modificarArea(AreaDTO entidad) throws IllegalArgumentException {
		TblareaHome accesoarea = new TblareaHome();
		String mensaje = "";
		try {
			try {
				accesoarea.modificar(new Tblarea(entidad.getIdEmpresa(),entidad.getEstablecimiento(),entidad.getIdArea(), entidad.getNombre(), entidad.getImagen(), entidad.getImagenOcupado()));
				mensaje = "area Modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public AreaDTO buscarArea(String idarea) {
		TblareaHome accesoarea = new TblareaHome();
		Tblarea tblarea= new Tblarea();
		AreaDTO areadto= new AreaDTO();
		areadto=null;
		String mensaje = "";
		try {
			try {
				//System.out.println(idarea);
				tblarea=accesoarea.findById(Integer.valueOf(idarea));
				areadto=pasarArea(tblarea);
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return areadto;
	}
	
	@Override
	public String eliminarArea(AreaDTO areadto) {
		TblareaHome accesoarea = new TblareaHome();
		String mensaje = "";
		try {
			try {
				accesoarea.eliminacionFisica(new Tblarea(areadto.getIdEmpresa(),areadto.getEstablecimiento(),areadto.getIdArea(), areadto.getNombre(), areadto.getImagen(), areadto.getImagenOcupado()));
				mensaje = "Objeto eliminado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la area porque est\u00e1 siendo utilizada en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public AreaDTO crearareaReg(Tblarea tblarea){
		return pasarArea(tblarea);
	}
	
	/**
	 * Metodo para listar las Areas de la base de datos.
	 * @return
	 */
	public List<AreaDTO> listarMesasOcupadas(){
		System.out.println("Entro a listar areas");
		List<AreaDTO> listaAreaDTO= new ArrayList<AreaDTO>();
		List<Tblarea> listaTblarea= new ArrayList<Tblarea>();
		TblareaHome accesoArea = new TblareaHome();
		TblmesaHome accesoMesa = new TblmesaHome();
		AreaDTO areadto = new AreaDTO();
		listaTblarea= accesoArea.getList();
		Set<MesaDTO> setmesas = new HashSet<MesaDTO>();
			for(Tblarea tblarea:listaTblarea){
				setmesas = new HashSet<MesaDTO>();
				areadto=pasarArea(tblarea);
				Iterator iteratortblmesa=accesoMesa.getList("join x.tblarea a where a.idArea='"+ String.valueOf(tblarea.getIdArea())+"' and x.estado='1' order by nombreMesa");
			    if(iteratortblmesa.hasNext()){
					while(iteratortblmesa.hasNext()){
						Object[] pair = (Object[])iteratortblmesa.next();
						Tblmesa mesa = (Tblmesa) pair[0];
						MesaDTO mesadto=pasarMesa(mesa);
						setmesas.add(mesadto);
				    }
			    }
			    areadto.setMesaDTO(setmesas);
			    listaAreaDTO.add(areadto);
			}
		return listaAreaDTO;	
	}
	
	@Override
	public List<AreaDTO> listarArea(int ini, int fin) {
		List<AreaDTO> areaReg = null;
		List<Tblarea> listado = null;
		TblareaHome tblareahome = new TblareaHome();
		try {
			listado=tblareahome.getList(ini, fin);
			areaReg = new ArrayList<AreaDTO>();
            if (listado != null) {
                for (Tblarea tblarea : listado) {
                	System.out.println("AREA: "+tblarea.getIdArea()+"-"+tblarea.getNombre());
                	areaReg.add(crearareaReg(tblarea));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return areaReg;
		
	}
	@Override
	public List<AreaDTO> listarAreaCombo(int ini, int fin) {
		List<AreaDTO> areaReg = null;
		List<Tblarea> listado = null;
		TblareaHome acceso = new TblareaHome();
		try {
			listado=acceso.getListCombo(ini, fin);
			areaReg = new ArrayList<AreaDTO>();
            if (listado != null) {
                for (Tblarea area : listado) {
                	areaReg.add(crearareaReg(area));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return areaReg;
		
	}
	
	@Override
	public List<AreaDTO> listarAreaLike(String busqueda, String campo) {
		List<AreaDTO> areaReg = null;
		List<Tblarea> listado = null;
		TblareaHome acceso = new 	TblareaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			areaReg = new ArrayList<AreaDTO>();
            if (listado != null) {
                for (Tblarea area : listado) {
                	areaReg.add(crearareaReg(area));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return areaReg;
	}
	@Override
	public int numeroRegistrosArea(String tabla) {
		TblareaHome tblareahome = new TblareaHome();
		int registros=0;
		try {
				registros=tblareahome.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	
	// FUNCIONES MESA////////////////////////////////////////////////////////////////
	/**
	 * M�todo para grabar una Mesa
	 * @param MesaDTO
	 */
	
	public String grabarMesa(MesaDTO mesadto) throws IllegalArgumentException {
		TblmesaHome accesoMesa = new TblmesaHome();
		String mensaje = "";
		Tblmesa tblmesa= new Tblmesa();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea= new Tblarea();
		int ban=0;
		try {
			try {
				tblmesa=accesoMesa.findByNombre(mesadto.getNombreMesa(),"nombreMesa");
				ban=1;
				mensaje="Mesa ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					tblarea=tblareahome.findById(mesadto.getTblarea().getIdArea());
					char estado=mesadto.getEstado().charAt(0);
					accesoMesa.grabar(new Tblmesa(mesadto.getIdEmpresa(),mesadto.getEstablecimiento(),tblarea, mesadto.getNombreMesa(),estado, mesadto.getImagen(), mesadto.getImagenOcupado(), mesadto.getX(), mesadto.getY()));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public String modificarMesa(MesaDTO mesadto) throws IllegalArgumentException {
		TblmesaHome accesoMesa = new TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea= new Tblarea();
		String mensaje = "";
		try {
			try {
				tblarea=tblareahome.findById(mesadto.getTblarea().getIdArea());
				char estado=mesadto.getEstado().charAt(0);
				Tblmesa tblmesa = new Tblmesa(mesadto.getIdEmpresa(),mesadto.getEstablecimiento(),tblarea,mesadto.getNombreMesa(),estado, mesadto.getImagen(), mesadto.getImagenOcupado(), mesadto.getX(), mesadto.getY());
				tblmesa.setIdMesa(mesadto.getIdMesa());
				accesoMesa.modificar(tblmesa);
				
				mensaje = "Mesa Modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public MesaDTO buscarMesa(String idMesa) {
		TblmesaHome tblmesahome = new TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea= new Tblarea();
		Tblmesa tblmesa= new Tblmesa();
		MesaDTO mesadto= new MesaDTO();
		mesadto=null;
		String mensaje = "";
		try {
			try {
				//System.out.println(idMesa);
				tblmesa=tblmesahome.findById(Integer.valueOf(idMesa));
				tblarea=tblareahome.findById(tblmesa.getTblarea().getIdArea());
				tblmesa.setTblarea(tblarea);
				mesadto=pasarMesa(tblmesa);
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mesadto;
	}
	
	@Override
	public String eliminarMesa(MesaDTO mesadto) {
		TblmesaHome accesoMesa = new TblmesaHome();
		String mensaje = "";
		try {
			try {
				Tblmesa tblmesa = new Tblmesa();
				tblmesa.setIdMesa(mesadto.getIdMesa());
				accesoMesa.eliminacionFisica(tblmesa);
				mensaje = "Objeto eliminado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la Mesa porque est\u00e1 siendo utilizada en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	
	public MesaDTO crearMesaReg(Tblmesa tblmesa){
		return pasarMesa(tblmesa);
	}
	
	@Override
	public List<MesaDTO> listarMesa(int ini, int fin) {
		List<MesaDTO> MesaReg = null;
		List<Tblmesa> listado = null;
		TblmesaHome tblmesahome = new TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea = new Tblarea();
		try {
			listado=tblmesahome.getList(ini, fin);
			MesaReg = new ArrayList<MesaDTO>();
            if (listado != null) {
                for (Tblmesa tblmesa : listado) {
                	tblarea=tblareahome.findById(tblmesa.getTblarea().getIdArea());
                	tblmesa.setTblarea(tblarea);
                	MesaReg.add(crearMesaReg(tblmesa));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MesaReg;
		
	}
	@Override
	public List<MesaDTO> listarMesaCombo(int ini, int fin) {
		List<MesaDTO> MesaReg = null;
		List<Tblmesa> listado = null;
		TblmesaHome acceso = new TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea = new Tblarea();		
		try {
			listado=acceso.getListCombo(ini, fin);
			MesaReg = new ArrayList<MesaDTO>();
            if (listado != null) {
                for (Tblmesa tblmesa : listado) {
                	tblarea=tblareahome.findById(tblmesa.getTblarea().getIdArea());
                	tblmesa.setTblarea(tblarea);
                	MesaReg.add(crearMesaReg(tblmesa));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MesaReg;
		
	}
	
	@Override
	public List<MesaDTO> listarMesaLike(String busqueda, String campo) {
		List<MesaDTO> MesaReg = null;
		List<Tblmesa> listado = null;
		TblmesaHome acceso = new 	TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		Tblarea tblarea = new Tblarea();			
		try {
			listado=acceso.findLike(busqueda, campo);
			MesaReg = new ArrayList<MesaDTO>();
            if (listado != null) {
                for (Tblmesa tblmesa : listado) {
                	tblarea=tblareahome.findById(tblmesa.getTblarea().getIdArea());
                	tblmesa.setTblarea(tblarea);
                	MesaReg.add(crearMesaReg(tblmesa));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return MesaReg;
	}
	@Override
	public int numeroRegistrosMesa(String tabla) {
		TblmesaHome accesoMesa = new TblmesaHome();
		int registros=0;
		try {
				registros=accesoMesa.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	
	
	//////METODOS PARA EMPRESA ///////////////////////////////////////////////////////////////////////
	
	@Override
	public String grabarEmpresa(EmpresaDTO entidad){
		TblempresaHome accesempresa = new TblempresaHome();
		Tblempresa tblempresa= new Tblempresa();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tblempresa=accesempresa.findByNombre(entidad.getNombre(),"nombre");
				ban=1;
				mensaje="empresa ya ingresada";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					accesempresa.grabar(new Tblempresa(entidad));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String modificarEmpresa(EmpresaDTO entidad) {
		TblempresaHome accesempresa = new TblempresaHome();
		String mensaje = "";
		try {
			try {
				accesempresa.modificar(new Tblempresa(entidad));
				mensaje = "Empresa modificada con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo grabar el dato";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String eliminarEmpresa(EmpresaDTO empresa) {
		TblempresaHome accesempresa = new TblempresaHome();
		String mensaje = "";
		try {
			try {
				accesempresa.eliminacionFisica(new Tblempresa(empresa));
				mensaje = "Objeto eliminador con \u00e9xito ";
			} catch (ErrorAlGrabar e) {
				mensaje="Elemento no Eliminado "+e.getMessage();
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public EmpresaDTO buscarEmpresa(String idempresa) {
		TblempresaHome accesoempresa = new TblempresaHome();
		Tblempresa tblempresa= new Tblempresa();
		EmpresaDTO empresa= new EmpresaDTO();
		empresa=null;
		String mensaje = "";
		try {
			try {
				System.out.println(idempresa);
				tblempresa=accesoempresa.findById(Integer.valueOf(idempresa));
				empresa=new EmpresaDTO(tblempresa.getIdEmpresa(),tblempresa.getNombre());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con \u00e9xito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return empresa;
	}
	public EmpresaDTO crearEmpresaReg(Tblempresa cat){
		EmpresaDTO empresadto = new EmpresaDTO(cat.getIdEmpresa(), cat.getNombre());
		empresadto.setContribuyenteEspecial(cat.getContribuyenteEspecial());
		empresadto.setDireccionMatriz(cat.getDireccionMatriz());
		empresadto.setObligadoContabilidad(cat.getObligadoContabilidad());
		empresadto.setRazonSocial(cat.getRazonSocial());
		empresadto.setRUC(cat.getRUC());
		return empresadto;
	}
	@Override
	public List<EmpresaDTO> listarEmpresa(int ini, int fin) {
		List<EmpresaDTO> EmpresaReg = null;
		List<Tblempresa> listado = null;
		TblempresaHome acceso = new 	TblempresaHome();
		try {
			listado=acceso.getList(ini, fin);
			EmpresaReg = new ArrayList<EmpresaDTO>();
            if (listado != null) {
                for (Tblempresa Empresa : listado) {
                	EmpresaReg.add(crearEmpresaReg(Empresa));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return EmpresaReg;
	}
	@Override
	public EmpresaDTO buscarEmpresaNombre(String nombre, String campo) {
		TblempresaHome accesoempresa = new TblempresaHome();
		Tblempresa tblempresa= new Tblempresa();
		EmpresaDTO empresa= new EmpresaDTO();
		empresa=null;
		String mensaje = "";
		try {
			try {
				tblempresa=accesoempresa.findByNombre(nombre, campo);
				empresa=new EmpresaDTO(tblempresa.getIdEmpresa(),tblempresa.getNombre());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return empresa;
	}
	public int numeroRegistros(String tabla) {
		
		TblempresaHome accesoempresa = new TblempresaHome();
		int registros=0;
		try {
				registros=accesoempresa.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	@Override
	public List<EmpresaDTO> listarEmpresaLike(String busqueda, String campo) {
		List<EmpresaDTO> EmpresaReg = null;
		List<Tblempresa> listado = null;
		TblempresaHome acceso = new 	TblempresaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			EmpresaReg = new ArrayList<EmpresaDTO>();
            if (listado != null) {
                for (Tblempresa Empresa : listado) {
                	EmpresaReg.add(crearEmpresaReg(Empresa));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return EmpresaReg;
	}
	
	//METODOS DE TIPO PRECIO ////////////////////////////////////////////
	
	@Override
	public String grabarTipoprecio(TipoPrecioDTO tipoprecio){
		TbltipoprecioHome accesotipoprecio = new TbltipoprecioHome();
		Tbltipoprecio tbltipoprecio= new Tbltipoprecio();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tbltipoprecio=accesotipoprecio.findByNombre(tipoprecio.getTipoPrecio(),"tipoPrecio");
				ban=1;
				mensaje="Tipo de Precio ya ingresado";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesotipoprecio.grabar(new Tbltipoprecio(tipoprecio));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String modificarTipoprecio(TipoPrecioDTO entidad) {
		TbltipoprecioHome accesotipoprecio = new TbltipoprecioHome();
		String mensaje = "";
		try {
			try {
				accesotipoprecio.modificar(new Tbltipoprecio(entidad));
				mensaje = "Tipo de precio modificado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo grabar el dato";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String eliminarTipoprecio(TipoPrecioDTO tipoprecio) {
		TbltipoprecioHome accesotipoprecio = new TbltipoprecioHome();
		String mensaje = "";
		try {
			try {
				accesotipoprecio.eliminacionFisica(new Tbltipoprecio(tipoprecio));
				mensaje = "Objeto eliminado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				//mensaje="Elemento no Eliminado";
				mensaje="No se puede eliminar la Tipo de Precio porque est\u00e1 siendo usado en un producto";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public TipoPrecioDTO buscarTipoprecio(String idTipoPrecio) {
		TbltipoprecioHome accesotipoprecio = new TbltipoprecioHome();
		Tbltipoprecio tbltipoprecio= new Tbltipoprecio();
		TipoPrecioDTO tipoprecio= new TipoPrecioDTO();
		tipoprecio=null;
		String mensaje = "";
		try {
			try {
				System.out.println(idTipoPrecio);
				tbltipoprecio=accesotipoprecio.findById(Integer.valueOf(idTipoPrecio));
				tipoprecio=new TipoPrecioDTO(tbltipoprecio.getIdEmpresa(),tbltipoprecio.getIdTipoPrecio(),tbltipoprecio.getTipoPrecio()
						,tbltipoprecio.getOrden());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			mensaje = "Busqueda realizada con \u00e9xito";
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return tipoprecio;
	}
	public TipoPrecioDTO crearTipoprecioReg(Tbltipoprecio cat){
		return new TipoPrecioDTO(cat.getIdEmpresa(),cat.getIdTipoPrecio(), cat.getTipoPrecio(),cat.getOrden());
	}
	@Override
	public List<TipoPrecioDTO> listarTipoprecio(int ini, int fin) {
		List<TipoPrecioDTO> TipoprecioReg = null;
		List<Tbltipoprecio> listado = null;
		TbltipoprecioHome acceso = new 	TbltipoprecioHome();
		try {
			listado=acceso.getList(ini, fin);
			TipoprecioReg = new ArrayList<TipoPrecioDTO>();
            if (listado != null) {
                for (Tbltipoprecio Tipoprecio : listado) {
                	TipoprecioReg.add(crearTipoprecioReg(Tipoprecio));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return TipoprecioReg;
	}
	@Override
	public List<TipoPrecioDTO> listarTipoprecioLike(String busqueda,
			String campo) {
		List<TipoPrecioDTO> TipoprecioReg = null;
		List<Tbltipoprecio> listado = null;
		TbltipoprecioHome acceso = new 	TbltipoprecioHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			TipoprecioReg = new ArrayList<TipoPrecioDTO>();
            if (listado != null) {
                for (Tbltipoprecio Tipoprecio : listado) {
                	TipoprecioReg.add(crearTipoprecioReg(Tipoprecio));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return TipoprecioReg;
	}
	@Override
	public int numeroRegistrosTipoprecio(String tabla) {

		TbltipoprecioHome accesotipoprecio = new TbltipoprecioHome();
		int registros=0;
		try {
				registros=accesotipoprecio.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	// TIPO DE CUENTA ////////////////////////////////////////////////////////////////////////////////////////////
	
	
	@Override
	public String grabarTipoCuenta(TipoCuentaDTO tipocuenta)
			throws IllegalArgumentException {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		Tbltipocuenta tbltipocuenta= new Tbltipocuenta();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tbltipocuenta=accesotipocuenta.findByNombre(tipocuenta.getNombre(),"nombre");
				ban=1;
				mensaje="Tipo de cuenta ya ingresado";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accesotipocuenta.grabar(new Tbltipocuenta(tipocuenta));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String modificarTipoCuenta(TipoCuentaDTO tipocuenta) {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		String mensaje = "";
		try {
			try {
				accesotipocuenta.modificar(new Tbltipocuenta(tipocuenta));
				mensaje="Tipo de Cuenta modificado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo grabar el dato";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String eliminarTipoCuenta(TipoCuentaDTO tipocuenta) {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		String mensaje = "";
		try {
			try {
				accesotipocuenta.eliminacionFisica(new Tbltipocuenta(tipocuenta));
				
				mensaje = "Objeto eliminador con �xito realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="Elemento no Eliminado";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public TipoCuentaDTO buscarTipoCuenta(String idtipocuenta) {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		Tbltipocuenta tbltipocuenta= new Tbltipocuenta();
		TipoCuentaDTO tipocuenta= new TipoCuentaDTO();
		tipocuenta=null;
		String mensaje = "";
		try {
			try {
				tbltipocuenta=accesotipocuenta.findById(Integer.valueOf(idtipocuenta));
				tipocuenta=new TipoCuentaDTO(tbltipocuenta.getIdTipoCuenta(),tbltipocuenta.getNombre());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return tipocuenta;
	}
	public TipoCuentaDTO crearTipocuentaReg(Tbltipocuenta cat){
		return new TipoCuentaDTO(cat.getIdTipoCuenta(), cat.getNombre());
	}
	@Override
	public List<TipoCuentaDTO> listarTipoCuenta(int ini, int fin) {
		List<TipoCuentaDTO> TipocuentaReg = null;
		List<Tbltipocuenta> listado = null;
		TbltipocuentaHome acceso = new 	TbltipocuentaHome();
		try {
			listado=acceso.getList(ini,fin);
			TipocuentaReg = new ArrayList<TipoCuentaDTO>();
            if (listado != null) {
                for (Tbltipocuenta Tipocuenta : listado) {
                	TipocuentaReg.add(crearTipocuentaReg(Tipocuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return TipocuentaReg;
	}
	@Override
	public TipoCuentaDTO buscarTipoCuentaNombre(String idTipoCuenta, String campo) {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		Tbltipocuenta tbltipocuenta= new Tbltipocuenta();
		TipoCuentaDTO tipocuenta= new TipoCuentaDTO();
		tipocuenta=null;
		String mensaje = "";
		try {
			try {
				tbltipocuenta=accesotipocuenta.findByNombre(idTipoCuenta, campo);
				tipocuenta=new TipoCuentaDTO(tbltipocuenta.getIdTipoCuenta(),tbltipocuenta.getNombre());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return tipocuenta;
	}
	@Override
	public List<TipoCuentaDTO> listarTipoCuentaLike(String busqueda,
			String campo) {
		List<TipoCuentaDTO> TipocuentaReg = null;
		List<Tbltipocuenta> listado = null;
		TbltipocuentaHome acceso = new 	TbltipocuentaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			TipocuentaReg = new ArrayList<TipoCuentaDTO>();
            if (listado != null) {
                for (Tbltipocuenta Tipocuenta : listado) {
                	TipocuentaReg.add(crearTipocuentaReg(Tipocuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return TipocuentaReg;
	}
	@Override
	public int numeroRegistrosTipocuenta(String tabla) {
		TbltipocuentaHome accesotipocuenta = new TbltipocuentaHome();
		int registros=0;
		try {
				registros=accesotipocuenta.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	
	
	///METODOS PARA CUENTA////////////////////////////////////////////////////
	@Override
	public String grabarCuenta(CuentaDTO cuenta,String planCuentaID)
			throws IllegalArgumentException {
		TblcuentaHome accesocuenta = new TblcuentaHome();
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
		Tblcuenta tblcuenta= new Tblcuenta();
		TblcuentaPlancuenta tblcuentaplancuenta= new TblcuentaPlancuenta();
		String mensaje = "";
		int ban=0;
			try {
				tblcuentaplancuenta=tblcuentaplancuentahome.findByQueryUnique("where u.Tblcuenta.nombreCuenta ='"+cuenta.getNombreCuenta()+"' and u.Tblplancuenta.idPlan='"+planCuentaID+"'");
				ban=1;
				mensaje="Cuenta ya ingresada";
			} catch (Exception e) {
				ban=0;
			}
			if(ban==0){
				try {
					tblcuentaplancuenta=tblcuentaplancuentahome.findByQueryUnique("where u.Tblcuenta.codigo ='"+cuenta.getcodigo()+"' and u.Tblplancuenta.idPlan='"+planCuentaID+"'");
					ban=1;
					mensaje="Cuenta ya ingresada";
				} catch (Exception e) {
					ban=0;
				}
			}
			if(ban==0){
				TblplancuentaHome tblplancuentahome=new TblplancuentaHome();
				Tblplancuenta tblplancuenta=new Tblplancuenta();
				tblplancuenta=tblplancuentahome.findById(Integer.parseInt(planCuentaID));
				if(tblplancuenta!=null){
					try {
						//mensaje="entro en el try";
						Set<Tblplancuenta> settblplancuenta = new HashSet<Tblplancuenta>();
						tblcuenta.setCodigo(cuenta.getcodigo());
						tblcuenta.setIdCuenta(cuenta.getIdCuenta());
						tblcuenta.setNivel(cuenta.getNivel());
						tblcuenta.setNombreCuenta(cuenta.getNombreCuenta());
						tblcuenta.setPadre(cuenta.getPadre());
						settblplancuenta.add(tblplancuenta);
						tblcuenta.setTblplancuentas(settblplancuenta);
						mensaje=tblplancuenta.getFechaCreacion()+ " "+tblplancuenta.getPeriodo()+ " ";
						tblcuenta.setTbltipocuenta(new Tbltipocuenta(cuenta.getTbltipocuenta()));
						accesocuenta.grabar(tblcuenta);
						mensaje="Cuenta Grabada";
					} catch (ErrorAlGrabar e) {
						mensaje=e.getMessage();
					}
					catch(Exception e){
						mensaje="error "+e.getMessage();
					}
					
				}else{
					mensaje = "Periodo no registrado";
				}
			}
		return mensaje;
	}
	public CuentaDTO crearCuentaReg(Tblcuenta cat){
		TipoCuentaDTO tipoc=new TipoCuentaDTO(cat.getTbltipocuenta().getIdTipoCuenta(),cat.getTbltipocuenta().getNombre());
		return new CuentaDTO(cat.getIdEmpresa(),tipoc,cat.getIdCuenta(),cat.getNombreCuenta(),
				cat.getPadre(),cat.getNivel(),cat.getCodigo());
	}
	@Override
	public List<CuentaDTO> listarCuenta(int ini, int fin, String planCuentaID) {
		List<CuentaDTO> CuentaReg = null;
		List<TblcuentaPlancuenta> listadotblcuentaplancuenta = null;
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
		TblcuentaHome tblcuentahome  = new 	TblcuentaHome();
		try {
			listadotblcuentaplancuenta=tblcuentaplancuentahome.findByQueryWithLenght("where u.tblplancuenta.idPlan='"+planCuentaID+"'", ini, fin);
			CuentaReg = new ArrayList<CuentaDTO>();
            if (listadotblcuentaplancuenta != null) {
                for (TblcuentaPlancuenta tblcuentaplancuenta : listadotblcuentaplancuenta) {
                	Tblcuenta tblcuenta  = new 	Tblcuenta();
                	System.out.println("ID CUENTA"+tblcuentaplancuenta.getTblcuenta().getIdCuenta());
                	tblcuenta=tblcuentahome.findById(tblcuentaplancuenta.getTblcuenta().getIdCuenta());
                	CuentaReg.add(crearCuentaReg(tblcuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CuentaReg;
	}
	public int numeroRegistroscuenta(String tabla, String planCuentaID) {
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
		int registros=0;
		try {
			registros=tblcuentaplancuentahome.countWithWhere(tabla, "where u.tblplancuenta.idPlan='"+planCuentaID+"'");
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	@Override
	public String eliminarCuenta(CuentaDTO cuenta) {
		TblcuentaHome accescuenta = new TblcuentaHome();
		String mensaje = "";
		try {
			try {
				accescuenta.eliminacionFisica(new Tblcuenta(cuenta));
				mensaje = "Objeto eliminador con exito realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="Elemento no Eliminado";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}

	public String modificarCuenta(CuentaDTO cuenta) {
		TblcuentaHome accesocuenta = new TblcuentaHome();
		String mensaje = "";
		try {
			try {
				accesocuenta.modificarCuenta(new Tblcuenta(cuenta));
				mensaje = "Cuenta Modificada";
			} catch (ErrorAlGrabar e) {
				System.out.println("Error al modificar cuenta "+e.getMessage());
				e.printStackTrace(System.out);
				System.out.println("Error al modificar cuenta causa: "+e.getCause());
				mensaje="No se pudo grabar el dato";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public CuentaDTO buscarCuenta(String idcuenta) {
		TblcuentaHome accesocuenta = new TblcuentaHome();
		Tblcuenta tblcuenta= new Tblcuenta();
		CuentaDTO cuenta= new CuentaDTO();
		cuenta=null;
		String mensaje = "";
		try {
			try {
				tblcuenta=accesocuenta.findById(Integer.valueOf(idcuenta));
				TipoCuentaDTO tipoc=new TipoCuentaDTO(tblcuenta.getTbltipocuenta().getIdTipoCuenta(),tblcuenta.getTbltipocuenta().getNombre());
				cuenta=new CuentaDTO(tblcuenta.getIdEmpresa(),tipoc,tblcuenta.getIdCuenta(),tblcuenta.getNombreCuenta(),
						tblcuenta.getPadre(),tblcuenta.getNivel(),tblcuenta.getCodigo());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return cuenta;
	}
	@Override
	public List<CuentaDTO> listarCuentaLike(String busqueda, String campo, String planCuentaID) {
		List<CuentaDTO> cuentaReg = null;
		List<TblcuentaPlancuenta> listtblcuentaplancuenta = null;
		TblcuentaHome tblcuentahome = new 	TblcuentaHome();
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new 	TblcuentaPlancuentaHome();
		try {
			listtblcuentaplancuenta=tblcuentaplancuentahome.findByQuery("where u.tblcuenta."+campo+" like " +"'%"+busqueda+"%' and u.tblplancuenta.idPlan='"+planCuentaID+"'");
			cuentaReg = new ArrayList<CuentaDTO>();
            if (listtblcuentaplancuenta != null) {
                for (TblcuentaPlancuenta tblcuentaplancuenta : listtblcuentaplancuenta) {
                	Tblcuenta tblcuenta = new Tblcuenta();
                	tblcuenta=tblcuentahome.findById(tblcuentaplancuenta.getTblcuenta().getIdCuenta());
                	cuentaReg.add(crearCuentaReg(tblcuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return cuentaReg;
	}
	@Override
	public List<CuentaDTO> listarCuentaQuery(String query) {
		List<CuentaDTO> cuentaReg = null;
		List<TblcuentaPlancuenta> listadotblcuentaplancuenta = null;
		TblcuentaHome tblcuentahome = new 	TblcuentaHome();
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
		try {
			listadotblcuentaplancuenta=tblcuentaplancuentahome.findByQuery(query);
			cuentaReg = new ArrayList<CuentaDTO>();
            if (listadotblcuentaplancuenta != null) {
                for (TblcuentaPlancuenta tblcuentaplancuenta : listadotblcuentaplancuenta) {
                	Tblcuenta tblcuenta = new 	Tblcuenta();
                	tblcuenta=tblcuentahome.findById(tblcuentaplancuenta.getTblcuenta().getIdCuenta());
                	cuentaReg.add(crearCuentaReg(tblcuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return cuentaReg;
	}
	@Override
	public CuentaDTO buscarCuentaNombre(String idCuenta, String campo) {
		TblcuentaHome accesocuenta = new TblcuentaHome();
		Tblcuenta tblcuenta= new Tblcuenta();
		CuentaDTO cuenta= new CuentaDTO();
		cuenta=null;
		String mensaje = "";
		try {
			try {
				tblcuenta=accesocuenta.findByNombre(idCuenta, campo);
				TipoCuentaDTO tipoc=new TipoCuentaDTO(tblcuenta.getTbltipocuenta().getIdTipoCuenta(),tblcuenta.getTbltipocuenta().getNombre());
				cuenta=new CuentaDTO(tblcuenta.getIdEmpresa(),tipoc,tblcuenta.getIdCuenta(),tblcuenta.getNombreCuenta(),
						tblcuenta.getPadre(),tblcuenta.getNivel(),tblcuenta.getCodigo());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return cuenta;
	}
	
	//PLAN DE CUENTAS ///////////////////////////////////////////////////////////////
	@Override
	public String grabarPlanCuenta(PlanCuentaDTO plancuenta)
			throws IllegalArgumentException {
		TblplancuentaHome accesoplan = new TblplancuentaHome();
		Tblplancuenta tblplancuenta= new Tblplancuenta(plancuenta);
		String mensaje = "";
		
		try {
				try {
					accesoplan.grabar(new Tblplancuenta(plancuenta));
					mensaje = "Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}								
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	
	@Override
	public EstablecimientoDTO buscarEstablecimiento(Integer idEmpresa) {
		TblestablecimientoHome accesocuenta = new TblestablecimientoHome();
		Tblestablecimiento tblplancuenta= new Tblestablecimiento();
		EstablecimientoDTO plancuenta= new EstablecimientoDTO();
		plancuenta=null;
		String mensaje = "";
		try {
			try {
				tblplancuenta=accesocuenta.findByQueryUnique(
						"where u.id.idEmpresa ='"+idEmpresa
						+"'");
				//tblplancuenta=accesocuenta.findByNombre(idEmpresa, "idEmpresa");
//				plancuenta=new EstablecimientoDTO(tblplancuenta.getId().getIdEmpresa(),
//						tblplancuenta.getDireccion(),tblplancuenta.getId().getEstablecimiento(),
//						tblplancuenta.getNick(), tblplancuenta.getMail(), tblplancuenta.getActivo());
				plancuenta=new EstablecimientoDTO(tblplancuenta.getId().getIdEmpresa(),
						tblplancuenta.getDireccion(),tblplancuenta.getId().getEstablecimiento(),
						tblplancuenta.getNick(), tblplancuenta.getMail(), tblplancuenta.getActivo()
						,correosRespaldo(tblplancuenta.getTblcorreosForEstablecimiento()));
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return plancuenta;
	}
	
	public Set<CorreoRespaldoDTO> correosRespaldo(Set<Tblcorreorespaldo> tblcorreosrespaldo){
		Set<CorreoRespaldoDTO> correosRespaldoDTO = new HashSet<CorreoRespaldoDTO>();
		CorreoRespaldoDTO correoResp;
		for (Tblcorreorespaldo correo:tblcorreosrespaldo){
//			correoResp = new CorreoRespaldoDTO(correo);
			correoResp = new CorreoRespaldoDTO(correo.getIdtblcorreo_respaldo(),correo.getMail());
			//correoResp.setEstablecimiento(establecimiento);
			correosRespaldoDTO.add(correoResp);
		}
		return correosRespaldoDTO;
	}
	
	public List<EstablecimientoDTO> buscarEstablecimientos(Integer idEmpresa) {
		TblestablecimientoHome accesocuenta = new TblestablecimientoHome();
		Tblestablecimiento tblplancuenta= new Tblestablecimiento();
		EstablecimientoDTO plancuenta= new EstablecimientoDTO();
		List<Tblestablecimiento> tblEstablecimientos;
		List<EstablecimientoDTO> establecimientos=new ArrayList<EstablecimientoDTO>();
		//plancuenta=null;
		String mensaje = "";
		try {
			try {
				tblEstablecimientos=accesocuenta.findByQuery(
						//"where u.TblestablecimientoTblempresaId.idEmpresa ="+idEmpresa
						"where u.id.idEmpresa ="+idEmpresa
						+"");
				for (Tblestablecimiento establ:tblEstablecimientos){
				//tblplancuenta=accesocuenta.findByNombre(idEmpresa, "idEmpresa");
					plancuenta=new EstablecimientoDTO(establ.getId().getIdEmpresa(),
							establ.getDireccion(),establ.getId().getEstablecimiento(), establ.getNick(), establ.getMail(), establ.getActivo()
							, correosRespaldo(establ.getTblcorreosForEstablecimiento()));
					establecimientos.add(plancuenta);
				}
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(System.out);
				mensaje="No se pudo recuperar";
			}
			
		} catch (RuntimeException re) {
			re.printStackTrace(System.out);
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return establecimientos;
	}
	
	@Override
	public PuntoEmisionDTO buscarPuntoemision(Integer idEmpresa, String establecimiento) {
		TblpuntoemisionHome accesocuenta = new TblpuntoemisionHome();
		Tblpuntoemision tblplancuenta= new Tblpuntoemision();
		PuntoEmisionDTO plancuenta= new PuntoEmisionDTO();
		plancuenta=null;
		String mensaje = "";
		try {
			try {
				
				tblplancuenta=accesocuenta.findByQueryUnique(
						"where u.id.idEmpresa ="+idEmpresa
						+" and u.id.establecimiento='"+establecimiento+"'");
				plancuenta=new PuntoEmisionDTO(tblplancuenta.getId().getIdEmpresa(),tblplancuenta.getId().getEstablecimiento(),
						tblplancuenta.getPuntoemision(),tblplancuenta.getDescripcion(),tblplancuenta.getNick(),tblplancuenta.getActivo());
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return plancuenta;
	}
	
	public List<PuntoEmisionDTO> buscarPuntosemision(Integer idEmpresa, String establecimiento) {
		TblpuntoemisionHome accesocuenta = new TblpuntoemisionHome();
		Tblpuntoemision tblplancuenta= new Tblpuntoemision();
		PuntoEmisionDTO plancuenta= new PuntoEmisionDTO();
		List<Tblpuntoemision> tblPuntosemision;
		List<PuntoEmisionDTO> puntosemision=new ArrayList<PuntoEmisionDTO>();
		plancuenta=null;
		String mensaje = "";
		try {
			try {
				System.out.println("Busqueda, empresa "+idEmpresa
						+" establecimiento "+establecimiento);
				tblPuntosemision=accesocuenta.findByQuery(
						"where u.id.idEmpresa ="+idEmpresa
						+" and u.id.establecimiento='"+establecimiento+"'");
				for (Tblpuntoemision puntoemision:tblPuntosemision){
					System.out.println("Busqueda, empresa "+puntoemision.getId().getIdEmpresa()
							+"establecimiento "+puntoemision.getId().getEstablecimiento());
					plancuenta=new PuntoEmisionDTO(puntoemision.getId().getIdEmpresa(),puntoemision.getId().getEstablecimiento(),
							puntoemision.getId().getPuntoemision(),puntoemision.getDescripcion(),puntoemision.getNick(),puntoemision.getActivo());
					puntosemision.add(plancuenta);
				}
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(System.out);
				mensaje="No se pudo recuperar";
			}
			
		} catch (RuntimeException re) {
			re.printStackTrace(System.out);
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return puntosemision;
	}
	
	@Override
	public PlanCuentaDTO buscarPlanCuenta(String idPlanCuenta) {
		TblplancuentaHome accesocuenta = new TblplancuentaHome();
		Tblplancuenta tblplancuenta= new Tblplancuenta();
		PlanCuentaDTO plancuenta= new PlanCuentaDTO();
		plancuenta=null;
		String mensaje = "";
		try {
			try {
				tblplancuenta=accesocuenta.findById(Integer.valueOf(idPlanCuenta));
				EmpresaDTO tipoc=new EmpresaDTO(tblplancuenta.getTblempresa().getIdEmpresa(),
						tblplancuenta.getTblempresa().getNombre(), tblplancuenta.getTblempresa().getRUC(),
						tblplancuenta.getTblempresa().getRazonSocial(),tblplancuenta.getTblempresa().getDireccionMatriz(),
						tblplancuenta.getTblempresa().getContribuyenteEspecial(),tblplancuenta.getTblempresa().getObligadoContabilidad(),
						tblplancuenta.getTblempresa().getFirma(),tblplancuenta.getTblempresa().getClave(), 
						tblplancuenta.getTblempresa().getFechaexpiracion());
				plancuenta=new PlanCuentaDTO(tipoc,tblplancuenta.getIdPlan(),String.valueOf(tblplancuenta.getFechaCreacion()),tblplancuenta.getPeriodo());
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return plancuenta;
	}
	@Override
	public List<PlanCuentaDTO> listarPlanCuenta(int ini, int fin) {
		List<PlanCuentaDTO> PlanCuentaReg = null;
		List<Tblplancuenta> listado = null;
		TblplancuentaHome acceso = new 	TblplancuentaHome();
		try {
			listado=acceso.getList(ini,fin);
			PlanCuentaReg = new ArrayList<PlanCuentaDTO>();
            if (listado != null) {
                for (Tblplancuenta Cuenta : listado) {
                	PlanCuentaReg.add(crearPlanCuentaReg(Cuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return PlanCuentaReg;
	}
	@Override
	public int numeroRegistrosPlanCuenta(String tabla) {
		TblplancuentaHome accesoplan = new TblplancuentaHome();
		int registros=0;
		try {
				registros=accesoplan.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	@Override
	public String eliminarPlanCuenta(String id) {
		TblplancuentaHome accesplancuenta = new TblplancuentaHome();
		String mensaje = "";
		try {
			mensaje =accesplancuenta.eliminarID(id);
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public List<PlanCuentaDTO> listarPlanCuentaLike(String busqueda,
			String campo) {
		List<PlanCuentaDTO> plancuentaReg = null;
		List<Tblplancuenta> listado = null;
		TblplancuentaHome acceso = new 	TblplancuentaHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			plancuentaReg = new ArrayList<PlanCuentaDTO>();
            if (listado != null) {
                for (Tblplancuenta plancuenta : listado) {
                	plancuentaReg.add(crearPlanCuentaReg(plancuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return plancuentaReg;
	}
	@Override
	public PlanCuentaDTO buscarPlanCuentaNombre(String idPlanCuenta,
			String campo) {
		TblplancuentaHome accesoplancuenta = new TblplancuentaHome();
		Tblplancuenta tblplancuenta= new Tblplancuenta();
		PlanCuentaDTO plancuenta= new PlanCuentaDTO();
		plancuenta=null;
		String mensaje = "";
		try {
			try {
				tblplancuenta=accesoplancuenta.findByNombre(idPlanCuenta, campo);
				EmpresaDTO tipoc=new EmpresaDTO(tblplancuenta.getTblempresa().getIdEmpresa(),tblplancuenta.getTblempresa().getNombre());
				plancuenta=new PlanCuentaDTO(tipoc,tblplancuenta.getIdPlan(),String.valueOf(tblplancuenta.getFechaCreacion()).toString(),
				tblplancuenta.getPeriodo());
			} catch (Exception e) {
				mensaje="No se pudo recuperar";				
			}			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return plancuenta;
	}
	
	private int ingresos=0;
//	@Override
//	public Integer ingresar(Factura factura) {
//        ingresos+=1;
//        System.out.println(ingresos+" "+factura.toString());
//        return ingresos;
//    }
	@Override
	public PlanCuentaDTO ultimoPlanCuenta(){
		PlanCuentaDTO plancuentadto= null;
		Tblplancuenta tblplancuenta=null;
		EmpresaDTO empresadto=new EmpresaDTO();
		Tblempresa tblempresa = new Tblempresa();
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		TblempresaHome tblempresahome = new TblempresaHome();
		tblplancuenta=tblplancuentahome.ultimoPlanCuenta();
		
		System.out.println("IdPlan: "+tblplancuenta.getIdPlan()+" IdEmpresa "+tblplancuenta.getTblempresa().getIdEmpresa());
		//empresadto.setIdEmpresa(tblplancuenta.getTblempresa().getIdEmpresa());
		System.out.println(tblplancuenta.getTblempresa().getIdEmpresa()+" - "+
				tblplancuenta.getTblempresa().getNombre()+" - "+ tblplancuenta.getTblempresa().getRUC()+" - "+
				tblplancuenta.getTblempresa().getRazonSocial()+" - "+tblplancuenta.getTblempresa().getDireccionMatriz()+" - "+
				tblplancuenta.getTblempresa().getContribuyenteEspecial()+" - "+tblplancuenta.getTblempresa().getObligadoContabilidad()+" - "+
				tblplancuenta.getTblempresa().getFirma()+" - "+tblplancuenta.getTblempresa().getClave()+" - "+
				tblplancuenta.getTblempresa().getFechaexpiracion());
		empresadto=new EmpresaDTO(tblplancuenta.getTblempresa().getIdEmpresa(),
				tblplancuenta.getTblempresa().getNombre(), tblplancuenta.getTblempresa().getRUC(),
				tblplancuenta.getTblempresa().getRazonSocial(),tblplancuenta.getTblempresa().getDireccionMatriz(),
				tblplancuenta.getTblempresa().getContribuyenteEspecial(),tblplancuenta.getTblempresa().getObligadoContabilidad(),
				tblplancuenta.getTblempresa().getFirma(),tblplancuenta.getTblempresa().getClave(), 
				tblplancuenta.getTblempresa().getFechaexpiracion()
				,tblplancuenta.getTblempresa().getMail(), tblplancuenta.getTblempresa().getDecimales(), 
				tblplancuenta.getTblempresa().getAmbiente(), tblplancuenta.getTblempresa().getTipo_contribuyente(), 
				tblplancuenta.getTblempresa().getAgente_retencion(), tblplancuenta.getTblempresa().getMicroempresa()
				,tblplancuenta.getTblempresa().getExportador(), tblplancuenta.getTblempresa().getSector_publico(), 
				new TbltipoempresaDTO(tblplancuenta.getTblempresa().getTipoempresa().getId_tipo_empresa(),
						tblplancuenta.getTblempresa().getTipoempresa().getTipo_empresa()), tblplancuenta.getTblempresa().getResolucion_agente()
				
				);
		List<EstablecimientoDTO> establecimientos=buscarEstablecimientos(tblplancuenta.getTblempresa().getIdEmpresa());
		List<EstablecimientoDTO> establecimientosCp=new ArrayList<EstablecimientoDTO>();
		for (EstablecimientoDTO establ:establecimientos){
			System.out.println("empresa: "+tblplancuenta.getTblempresa().getIdEmpresa()
					+" establecimiento "+establ.getEstablecimiento());
			List<PuntoEmisionDTO> puntosemision=buscarPuntosemision(tblplancuenta.getTblempresa().getIdEmpresa(),establ.getEstablecimiento());
			establ.setPuntoemision(puntosemision);
			establecimientosCp.add(establ);
		}
		empresadto.setEstablecimientos(establecimientosCp);
		plancuentadto= new PlanCuentaDTO(empresadto,tblplancuenta.getIdPlan(),tblplancuenta.getFechaCreacion().toString(), tblplancuenta.getPeriodo());
		return plancuentadto;
	}
	
	@Override
	public PlanCuentaDTO ultimoPlanCuenta(String idEmpresa){
		PlanCuentaDTO plancuentadto= null;
		Tblplancuenta tblplancuenta=null;
		EmpresaDTO empresadto=new EmpresaDTO();
		Tblempresa tblempresa = new Tblempresa();
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		TblempresaHome tblempresahome = new TblempresaHome();
		tblplancuenta=tblplancuentahome.ultimoPlanCuenta(idEmpresa);
		
		System.out.println("IdPlan: "+tblplancuenta.getIdPlan()+" IdEmpresa "+tblplancuenta.getTblempresa().getIdEmpresa());
		//empresadto.setIdEmpresa(tblplancuenta.getTblempresa().getIdEmpresa());
		System.out.println(tblplancuenta.getTblempresa().getIdEmpresa()+" - "+
				tblplancuenta.getTblempresa().getNombre()+" - "+ tblplancuenta.getTblempresa().getRUC()+" - "+
				tblplancuenta.getTblempresa().getRazonSocial()+" - "+tblplancuenta.getTblempresa().getDireccionMatriz()+" - "+
				tblplancuenta.getTblempresa().getContribuyenteEspecial()+" - "+tblplancuenta.getTblempresa().getObligadoContabilidad()+" - "+
				tblplancuenta.getTblempresa().getFirma()+" - "+tblplancuenta.getTblempresa().getClave()+" - "+
				tblplancuenta.getTblempresa().getFechaexpiracion());
		/*empresadto=new EmpresaDTO(tblplancuenta.getTblempresa().getIdEmpresa(),
				tblplancuenta.getTblempresa().getNombre(), tblplancuenta.getTblempresa().getRUC(),
				tblplancuenta.getTblempresa().getRazonSocial(),tblplancuenta.getTblempresa().getDireccionMatriz(),
				tblplancuenta.getTblempresa().getContribuyenteEspecial(),tblplancuenta.getTblempresa().getObligadoContabilidad(),
				tblplancuenta.getTblempresa().getFirma(),tblplancuenta.getTblempresa().getClave(), 
				tblplancuenta.getTblempresa().getFechaexpiracion());*/
		empresadto=new EmpresaDTO(tblplancuenta.getTblempresa().getIdEmpresa(),
				tblplancuenta.getTblempresa().getNombre(), tblplancuenta.getTblempresa().getRUC(),
				tblplancuenta.getTblempresa().getRazonSocial(),tblplancuenta.getTblempresa().getDireccionMatriz(),
				tblplancuenta.getTblempresa().getContribuyenteEspecial(),tblplancuenta.getTblempresa().getObligadoContabilidad(),
				tblplancuenta.getTblempresa().getFirma(),tblplancuenta.getTblempresa().getClave(), 
				tblplancuenta.getTblempresa().getFechaexpiracion()
				,tblplancuenta.getTblempresa().getMail(), tblplancuenta.getTblempresa().getDecimales(), 
				tblplancuenta.getTblempresa().getAmbiente(), tblplancuenta.getTblempresa().getTipo_contribuyente(), 
				tblplancuenta.getTblempresa().getAgente_retencion(), tblplancuenta.getTblempresa().getMicroempresa()
				,tblplancuenta.getTblempresa().getExportador(), tblplancuenta.getTblempresa().getSector_publico(), 
				new TbltipoempresaDTO(tblplancuenta.getTblempresa().getTipoempresa().getId_tipo_empresa(),
						tblplancuenta.getTblempresa().getTipoempresa().getTipo_empresa()), tblplancuenta.getTblempresa().getResolucion_agente()
				
				);
		List<EstablecimientoDTO> establecimientos=buscarEstablecimientos(tblplancuenta.getTblempresa().getIdEmpresa());
		List<EstablecimientoDTO> establecimientosCp=new ArrayList<EstablecimientoDTO>();
		for (EstablecimientoDTO establ:establecimientos){
			System.out.println("empresa: "+tblplancuenta.getTblempresa().getIdEmpresa()
					+" establecimiento "+establ.getEstablecimiento());
			List<PuntoEmisionDTO> puntosemision=buscarPuntosemision(tblplancuenta.getTblempresa().getIdEmpresa(),establ.getEstablecimiento());
			establ.setPuntoemision(puntosemision);
			establecimientosCp.add(establ);
		}
		empresadto.setEstablecimientos(establecimientosCp);
		plancuentadto= new PlanCuentaDTO(empresadto,tblplancuenta.getIdPlan(),tblplancuenta.getFechaCreacion().toString(), tblplancuenta.getPeriodo());
		return plancuentadto;
	}
	
	
	//METODOS PARA CARGO
	@Override
	public CargoDTO buscarCargo(String idCargo) {
		TblcargoHome acceso = new TblcargoHome();
		Tblcargo tblcargo= new Tblcargo();
		CargoDTO cargo= new CargoDTO();
		cargo=null;
		String mensaje = "";
		try {
			try {
				System.out.println(idCargo);
				tblcargo=acceso.findById(Integer.valueOf(idCargo));
				cargo=new CargoDTO(tblcargo.getIdEmpresa(),tblcargo.getIdCargo(),tblcargo.getDescripcion());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return cargo;
	}
	@Override
	public String eliminarCargo(CargoDTO cargo) {
		TblcargoHome acceso = new TblcargoHome();
		String mensaje = "";
		try {
			try {
				acceso.eliminacionFisica(new Tblcargo(cargo));
				mensaje = "Objeto eliminado con �xito realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="Elemento no Eliminado";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public String grabarCargo(CargoDTO cargo) {
		TblcargoHome accescargo = new TblcargoHome();
		Tblcargo tblcargo= new Tblcargo();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tblcargo=accescargo.findByNombre(cargo.getDescripcion(),"descripcion");
				ban=1;
				mensaje="Cargo ya Ingresado";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					
					accescargo.grabar(new Tblcargo(cargo));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato";
				}
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public CargoDTO crearCargoReg(Tblcargo marc){
		return new CargoDTO(marc.getIdEmpresa(),marc.getIdCargo(), marc.getDescripcion());
	}
	@Override
	public List<CargoDTO> listarCargo(int ini, int fin) {
		List<CargoDTO> CargoReg = null;
		List<Tblcargo> listado = null;
		TblcargoHome acceso = new TblcargoHome();
		try {
			listado=acceso.getList(ini, fin);
			CargoReg = new ArrayList<CargoDTO>();
            if (listado != null) {
                for (Tblcargo Cargo : listado) {
                	CargoReg.add(crearCargoReg(Cargo));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CargoReg;
	}
	@Override
	public List<CargoDTO> listarCargoLike(String busqueda, String campo) {
		List<CargoDTO> CargoReg = null;
		List<Tblcargo> listado = null;
		TblcargoHome acceso = new 	TblcargoHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			CargoReg = new ArrayList<CargoDTO>();
            if (listado != null) {
                for (Tblcargo Cargo : listado) {
                	CargoReg.add(crearCargoReg(Cargo));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return CargoReg;
	}
	@Override
	public String modificarCargo(CargoDTO cargo) {
		TblcargoHome accesoCargo = new TblcargoHome();
		String mensaje = "";
		try {
			try {
				accesoCargo.modificar(new Tblcargo(cargo));
				mensaje="Cargo modificado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo modificar ";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public int numeroRegistrosCargo(String tabla) {
		TblcargoHome accesocargo = new TblcargoHome();
		int registros=0;
		try {
				registros=accesocargo.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	@Override
	public String grabarPersona(PersonaDTO persona)
			throws IllegalArgumentException {
		return null;
	}
	@Override
	public String grabarProductoBodega(ProductoBodegaDTO[] proBod, int length)
			throws IllegalArgumentException {
		int len=length;
		TblproductoTblbodegaHome acceso = new TblproductoTblbodegaHome();
		TblproductoTblbodega[] tblprobod= new TblproductoTblbodega[len];
		String mensaje = "";
		Double stock=0.0;
		Double cant=0.0;
		ProductoBodegaDTO pb= new ProductoBodegaDTO();
		//try {
				//try {
					if(proBod!=null){
						Double TotalS=0.0;
						pb=new ProductoBodegaDTO();
						for(int i=0;i<len;i++){
							pb=new ProductoBodegaDTO();
							pb=proBod[i];
							try{
								cant=pb.getCantidad();
								//TotalS=TotalS+cant;
								tblprobod[i]=new TblproductoTblbodega(pb);
								stock= acceso.findStock(String.valueOf(pb.getTblproducto().getIdProducto()),
								String.valueOf(pb.getTblbodega().getIdBodega()));
								pb.setCantidad(stock+cant); 
								tblprobod[i]=new TblproductoTblbodega(pb);
								actualizarStockProducto(pb.getTblproducto().getIdProducto());
							}catch(Exception e){//por si el producto estba creado antes que la bodega 
								try {
									acceso.grabar(tblprobod[i]); 
									mensaje="Grabado";
									actualizarStockProducto(pb.getTblproducto().getIdProducto());
								} catch (ErrorAlGrabar e1) {
									mensaje=e1.getMessage()+"no puede grabar tbl";
								}
								
							}
						}
						/*System.out.println("cantidad"+TotalS);
						try {
							System.out.println("Entero= "+len);
							acceso.grabarLista(tblprobod);
							mensaje="Detalle de Bodegas Guardado ";
							TblproductoHome pro=new TblproductoHome();
							Tblproducto prod=new Tblproducto();
							prod=pro.findById(pb.getTblproducto().getIdProducto());
							TotalS=TotalS+prod.getStock();
							System.out.println("cantidad"+TotalS);
							prod.setStock(TotalS);
							pro.modificar(prod);
						} catch (ErrorAlGrabar e) {
							mensaje=e.getMessage()+"  NO se pudo guardar";
						}*/
						
						
					}
					/*try{
						if(proTip!=null){
							this.grabarProductoTipoPrecio(proTip,l);
							//accesotip.grabarList(tblprotip, length);
							mensaje=mensaje+"Detalle de tipos de Precio guardados";
							System.out.println("Guardados tipo_precio ");
							actualizarStockProducto(pb.getTblproducto().getIdProducto());
						}
						mensaje="Ingreso realizado con \u00e9xito";
					} catch (Exception e) {
						mensaje=e.getMessage()+"tipo de precio";
					} */
		/*} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}*/
					try {
						System.out.println("Entero= "+len);
						mensaje="Detalle de Bodegas Guardado ";
						if (len>0){ 
							acceso.grabarLista(tblprobod);
							System.out.println("A actualizar "+String.valueOf(pb.getTblproducto().getIdProducto()));
							actualizarStockProducto(pb.getTblproducto().getIdProducto());
							System.out.println("Stock Actualizado "+len);
						}else{
							mensaje+="\n"+"ADVERTENCIA:"
							+"\n"+"No se modifico el stock del produnto en bodegas."
							+"\n"+"Verifique que el producto este asignado a una bodega antes de realizar transacciones.";
							System.out.println("No hay bodegas que asignar");
						}
						
						/*
						TblproductoHome pro=new TblproductoHome();
						Tblproducto prod=new Tblproducto();
						prod=pro.findById(pb.getTblproducto().getIdProducto());
						//TotalS=TotalS+prod.getStock();
						System.out.println("cantidad"+TotalS);
						prod.setStock(TotalS);
						pro.modificar(prod);
						*/
					} catch (ErrorAlGrabar e) {
						mensaje=e.getMessage()+"  NO se pudo guardar";
					}			
					
		return mensaje;
	
	}

	public String grabarProductoBodegaAnt(ProductoBodegaDTO[] proBod, int length,ProductoTipoPrecioDTO[] proTip,
			int l)
			throws IllegalArgumentException {
		int len=length;
		TblproductoTblbodegaHome acceso = new TblproductoTblbodegaHome();
		TblproductoTblbodega[] tblprobod= new TblproductoTblbodega[len];
		String mensaje = "";
		Double stock=0.0;
		Double cant=0.0;
		ProductoBodegaDTO pb= new ProductoBodegaDTO();
		//try {
				//try {
					if(proBod!=null){
						Double TotalS=0.0;
						pb=new ProductoBodegaDTO();
						for(int i=0;i<len;i++){
							pb=new ProductoBodegaDTO();
							pb=proBod[i];
							try{
								cant=pb.getCantidad();
								//TotalS=TotalS+cant;
								tblprobod[i]=new TblproductoTblbodega(pb);
								stock= acceso.findStock(String.valueOf(pb.getTblproducto().getIdProducto()),
								String.valueOf(pb.getTblbodega().getIdBodega()));
								pb.setCantidad(stock+cant); 
								tblprobod[i]=new TblproductoTblbodega(pb);
								actualizarStockProducto(pb.getTblproducto().getIdProducto());
							}catch(Exception e){//por si el producto estba creado antes que la bodega 
								try {
									acceso.grabar(tblprobod[i]); 
									mensaje="Grabado";
									actualizarStockProducto(pb.getTblproducto().getIdProducto());
								} catch (ErrorAlGrabar e1) {
									mensaje=e1.getMessage()+"no puede grabar tbl";
								}
								
							}
						}
						/*System.out.println("cantidad"+TotalS);
						try {
							System.out.println("Entero= "+len);
							acceso.grabarLista(tblprobod);
							mensaje="Detalle de Bodegas Guardado ";
							TblproductoHome pro=new TblproductoHome();
							Tblproducto prod=new Tblproducto();
							prod=pro.findById(pb.getTblproducto().getIdProducto());
							TotalS=TotalS+prod.getStock();
							System.out.println("cantidad"+TotalS);
							prod.setStock(TotalS);
							pro.modificar(prod);
						} catch (ErrorAlGrabar e) {
							mensaje=e.getMessage()+"  NO se pudo guardar";
						}*/
						
						
					}
					try{
						if(proTip!=null){
							this.grabarProductoTipoPrecio(proTip,l);
							//accesotip.grabarList(tblprotip, length);
							mensaje=mensaje+"Detalle de tipos de Precio guardados";
							System.out.println("Guardados tipo_precio ");
							actualizarStockProducto(pb.getTblproducto().getIdProducto());
						}
						mensaje="Ingreso realizado con \u00e9xito";
					} catch (Exception e) {
						mensaje=e.getMessage()+"tipo de precio";
					} 
		/*} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}*/
					try {
						System.out.println("Entero= "+len);
						mensaje="Detalle de Bodegas Guardado ";
						if (len>0){ 
							acceso.grabarLista(tblprobod);
							System.out.println("A actualizar "+String.valueOf(pb.getTblproducto().getIdProducto()));
							actualizarStockProducto(pb.getTblproducto().getIdProducto());
							System.out.println("Stock Actualizado "+len);
						}else{
							mensaje+="\n"+"ADVERTENCIA:"
							+"\n"+"No se modifico el stock del produnto en bodegas."
							+"\n"+"Verifique que el producto este asignado a una bodega antes de realizar transacciones.";
							System.out.println("No hay bodegas que asignar");
						}
						
						/*
						TblproductoHome pro=new TblproductoHome();
						Tblproducto prod=new Tblproducto();
						prod=pro.findById(pb.getTblproducto().getIdProducto());
						//TotalS=TotalS+prod.getStock();
						System.out.println("cantidad"+TotalS);
						prod.setStock(TotalS);
						pro.modificar(prod);
						*/
					} catch (ErrorAlGrabar e) {
						mensaje=e.getMessage()+"  NO se pudo guardar";
					}			
					
		return mensaje;
	
	}
	
	public void actualizarStockProducto(Integer idproducto){
		TblproductoTblbodegaHome acceso = new TblproductoTblbodegaHome();
		System.out.println("A sumar stock");
		Double TotalS=acceso.SumaStockBodegas(idproducto);
		System.out.println("CANTIDAD total en las bodegas "+TotalS +" del producto: "+idproducto);
		try {
			//System.out.println("Entero= "+len);
			//acceso.grabarLista(tblprobod);
			//mensaje="Detalle de Bodegas Guardado ";
			TblproductoHome pro=new TblproductoHome();
			
			
			Tblproducto prod=new Tblproducto();
			prod=pro.findById(idproducto);
			//TotalS=TotalS+prod.getStock();
			System.out.println("cantidad"+TotalS);
			prod.setStock(TotalS);
			pro.modificar(prod);
		} catch (ErrorAlGrabar e) {
			//mensaje=e.getMessage()+"  NO se pudo guardar";
		}
	}
	@Override
	public String grabarProductoMultiImpuesto(TblproductoMultiImpuestoDTO[] proTip) throws IllegalArgumentException {
		TblproductoMultiImpuestoHome acceso = new TblproductoMultiImpuestoHome();
		System.out.println("En grabarproducto multiimpuesto");
		TblproductoMultiImpuesto[] tblprotip= new TblproductoMultiImpuesto[proTip.length];
		System.out.println("En grabarproducto multiimpuesto prodMult length "+proTip.length);
		String mensaje = "";
		try {
				try {
					for(int i=0;i<proTip.length;i++){
						TblproductoMultiImpuestoDTO pt=new TblproductoMultiImpuestoDTO();
						pt=proTip[i];
						tblprotip[i]=new TblproductoMultiImpuesto(pt);
						
						System.out.println("multi impuesto: " + tblprotip[i]);
						
						//tblprotip[i].setIdtblproducto_tblmulti_impuesto(26297);
						
						System.out.println("esta null? " + tblprotip[i].getIdtblproducto_tblmulti_impuesto());
						
						System.out.println("En grabarproducto multiimpuesto prodMult en for "+tblprotip[i].getIdtblproducto_tblmulti_impuesto()+", "+
								tblprotip[i].getTblproducto().getIdProducto()+", "+tblprotip[i].getTblmultiImpuesto().getIdmultiImpuesto());
						
//						System.out.println("En grabarproducto multiimpuesto prodMult en for "+26297+", "+
//								tblprotip[i].getTblproducto().getIdProducto()+", "+tblprotip[i].getTblmultiImpuesto().getIdmultiImpuesto());
						//26297
					}
					acceso.grabarList(tblprotip);
					mensaje="Ingreso realizado con \u00e9xito";
					System.out.println("En grabarproducto multiimpuesto luego de grabar "+mensaje);
				} catch (ErrorAlGrabar e) {
					System.out.println(e.getLocalizedMessage());
					System.out.println("Error "+e.getMessage());
					e.printStackTrace(System.out);
					mensaje=e.getMessage();
				}
		} catch (RuntimeException re) {
			System.out.println(re.getLocalizedMessage());
			System.out.println("Error "+re.getMessage());
			re.printStackTrace(System.out);
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	
	}
	//METODOS PARA IMPUESTO
	
	@Override
	public String grabarProductoTipoPrecio(ProductoTipoPrecioDTO[] proTip,
			int length) throws IllegalArgumentException {
		TblproductoTipoprecioHome acceso = new TblproductoTipoprecioHome();
		TblproductoTipoprecio[] tblprotip= new TblproductoTipoprecio[length];
		String mensaje = "";
		try {
				try {
					for(int i=0;i<length;i++){
						ProductoTipoPrecioDTO pt=new ProductoTipoPrecioDTO();
						pt=proTip[i];
						tblprotip[i]=new TblproductoTipoprecio(pt);
					}
					acceso.grabarList(tblprotip, length);
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje=e.getMessage();
				}
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	
	}
	
	public LinkedList<String[]> listarCuentaPlanCuenta(String idEmpresa){
		LinkedList<String[]> list = new LinkedList<String[]>();
		TblcuentaPlancuentaHome acceso = new TblcuentaPlancuentaHome();
			Iterator result = acceso.ListarTblcuentaPlancuenta(idEmpresa);
			while ( result.hasNext() ) {
				Object[] pair = (Object[])result.next();
				String[] cuenta=new String[3];
				int a=(Integer) pair[0];
				cuenta[0]=String.valueOf(a);
				a=(Integer) pair[2];
				cuenta[1]=(String) pair[1];
				cuenta[2]=String.valueOf(a);
				list.add(cuenta);
			}
		return list;
	}
	
	public List<CuentaDTO> listarCuentaPlanCuentas(String planCuentaID){
		List<CuentaDTO> cuentaReg = new ArrayList<CuentaDTO>(0);
		List<TblcuentaPlancuenta> listado = null;
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new 	TblcuentaPlancuentaHome();
		TblcuentaHome tblcuentahome = new 	TblcuentaHome();
		Tblcuenta tblcuenta = new Tblcuenta();
		try {
			listado=tblcuentaplancuentahome.findByQuery("where u.tblplancuenta.idPlan='"+planCuentaID+"'");
            if (listado != null) {
                for (TblcuentaPlancuenta tblcuentaplancuenta : listado) {
                	tblcuenta=tblcuentahome.findById(tblcuentaplancuenta.getTblcuenta().getIdCuenta());
                	cuentaReg.add(crearCuentaReg(tblcuenta));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return cuentaReg;
		
	}
	
	@Override
	public ImpuestoDTO buscarImpuesto(String idImpuesto) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String eliminarImpuesto(ImpuestoDTO impuesto) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String grabarImpuesto(ImpuestoDTO entidad) {
		TblimpuestoHome acces = new TblimpuestoHome();
		Tblimpuesto tblimpuesto= new Tblimpuesto();
		String mensaje = "";
		int ban=0;
		try {
			/*try {
				tblimpuesto=acces.findByNombre(String.valueOf(entidad.getCodigo()),"codigo");
				ban=1;
				mensaje="Impuesto ya ingresado Codigo repetido";
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}*/
			if(ban==0){
				try {
					
					acces.grabar(new Tblimpuesto(entidad));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato "+e.getMessage();
				}
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public List<TblmultiImpuestoDTO> listarMultiImpuesto(int ini, int fin) {
		List<TblmultiImpuestoDTO> ImpuestoReg = null;
		List<TblmultiImpuesto> listado = null;
		TblmultiImpuestoHome acceso = new TblmultiImpuestoHome();
		try {
			listado=acceso.getList(ini, fin);
			ImpuestoReg = new ArrayList<TblmultiImpuestoDTO>();
            if (listado != null) {
                for (TblmultiImpuesto Impuesto : listado) {
                	ImpuestoReg.add(crearMultiImpuestoDTO(Impuesto));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ImpuestoReg;
	}
	@Override
	public List<ImpuestoDTO> listarImpuesto(int ini, int fin) {
		List<ImpuestoDTO> ImpuestoReg = null;
		List<Tblimpuesto> listado = null;
		TblimpuestoHome acceso = new TblimpuestoHome();
		try {
			listado=acceso.getList(ini, fin);
			ImpuestoReg = new ArrayList<ImpuestoDTO>();
            if (listado != null) {
                for (Tblimpuesto Impuesto : listado) {
                	ImpuestoReg.add(crearImpuestoReg(Impuesto));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ImpuestoReg;
	}
	@Override
	public List<ImpuestoDTO> listarImpuesto(int tipo) {
		List<ImpuestoDTO> ImpuestoReg = null;
		List<Tblimpuesto> listado = null;
		TblimpuestoHome acceso = new TblimpuestoHome();
		System.out.println("listar impuesto tipo "+tipo);
//		try {
//			listado=acceso.findJoinOtherTable("join tblcuenta c on u.idCuenta=c.idCuenta where c.idTipoCuenta="+tipo);
			Iterator iterator=acceso.findJoinOtherTable("join tblcuenta c on u.idCuenta=c.idCuenta where c.idTipoCuenta="+tipo);
			ImpuestoReg = new ArrayList<ImpuestoDTO>();
			while (iterator.hasNext() ){
				Object[] pair = (Object[])iterator.next();
				Tblimpuesto impuesto=new Tblimpuesto((Integer)pair[0],(String)pair[1],(Integer)pair[2],(Double)pair[3],
						(Integer)pair[4],(Integer)pair[5],(Integer)pair[6],(String)pair[7]);
				ImpuestoReg.add(crearImpuestoReg(impuesto));
//				for (Object o:pair){
//					System.out.println(o.getClass());
//				}
//				cont++;
//				System.out.println(cont);
			}
			
//            if (listado != null) {
//                for (Tblimpuesto Impuesto : listado) {
//                	ImpuestoReg.add(crearImpuestoReg(Impuesto));
//                }
//            }
//		} catch (RuntimeException re) {
//			throw re;
//		} 
		return ImpuestoReg;
	}
	
	@Override
	public List<ImpuestoDTO> listarImpuestoLike(String busqueda, String campo) {
		List<ImpuestoDTO> ImpuestoReg = null;
		List<Tblimpuesto> listado = null;
		TblimpuestoHome acceso = new 	TblimpuestoHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			ImpuestoReg = new ArrayList<ImpuestoDTO>();
            if (listado != null) {
                for (Tblimpuesto Impuesto : listado) {
                	ImpuestoReg.add(crearImpuestoReg(Impuesto));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ImpuestoReg;
	}
	@Override
	public String modificarImpuesto(ImpuestoDTO entidad) {
		TblimpuestoHome acces = new TblimpuestoHome();
		String mensaje = "";
		try {
			acces.modificar(new Tblimpuesto(entidad));
			mensaje="Impuesto modificado con \u00e9xito";
		} catch (ErrorAlGrabar e) {
			mensaje="No se pudo grabar el dato "+e.getMessage();	
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public int numeroRegistrosImpuesto(String tabla) {
		TblimpuestoHome acceso = new TblimpuestoHome();
		int registros=0;
		try {
				registros=acceso.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		return registros;
	}
	 // METODOS PARA RETENCIONES
	
	
	
	
	//METODOS PARA TIPO DE PAGO
	public List<DtocomercialDTO> buscarCuentasCobrar(String nombre,String campo)
	{
		System.out.println("En buscar cuentas cobrar nombre: "+nombre+" campo:"+campo);
		List<DtocomercialDTO> cuentascob = new ArrayList<DtocomercialDTO>();
		TbldtocomercialHome accesodtocomercial = new TbldtocomercialHome();

		
		TblpersonaHome accesopersona = new TblpersonaHome();
		Tblpersona tblpersonacargar= new Tblpersona();
		DtocomercialDTO documento=new DtocomercialDTO();
		PersonaDTO personadto = new PersonaDTO();
		PersonaDTO personaFactDto = new PersonaDTO();
		Tblbodega tblbodega= new Tblbodega();
		TblbodegaHome accesoBodega=new TblbodegaHome();
		
		Tblproducto tblproducto=new Tblproducto();
		TblproductoHome accesoproducto=new TblproductoHome();
		TbldtocomercialdetalleHome accesodetalle=new TbldtocomercialdetalleHome();
		
		BodegaDTO bodegadto= new BodegaDTO();
		ProductoDTO productodto= new ProductoDTO();
		DtoComDetalleDTO dtocomdetalle=new DtoComDetalleDTO();
		
		TblpagoHome accesopago=new TblpagoHome();
		List<Tblpago> tblpagolist= new ArrayList<Tblpago>();
		TblpagoDTO tblpagodto=new TblpagoDTO();
		Tblpago tblpago=new Tblpago();
		Iterator iterator = null;
		List<Tbldtocomercialdetalle>listtbldtocomercialdetalle=new ArrayList<Tbldtocomercialdetalle>();;
	    List<Tblbodega>listbodega= new ArrayList<Tblbodega>();
	    List<Tblproducto>listproducto= new ArrayList<Tblproducto>();
	    try{
	    	if(campo.equals("razonSocial")){
	    		iterator=accesodtocomercial.getList("join x.tblpagos p where x.tblpersonaByIdPersona.razonSocial like'"+nombre+"%' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
			}else if (campo.equals("fechaVencimiento"))
			{
				System.out.println("fecha venc "+nombre);
				iterator=accesodtocomercial.getList("join x.tblpagos p where p.fechaVencimiento ='"+nombre+"' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
				
			}
	    	/*
			else if (campo.equals("subtotal"))
			{
				double x=Integer.parseInt(nombre);
				
				iterator=accesodtocomercial.getList("join x.tblpagos p where x.subtotal ='"+nombre+"' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
				
			}*/
			else if(campo.equals("Dias"))
			{
				Calendar calendar = new GregorianCalendar();; //obtiene la fecha de hoy 
				System.out.println("fecha act"+calendar.getTime());
				calendar.add(Calendar.DATE, -Integer.valueOf(nombre)); //el -3 indica que se le restaran 3 dias 
				System.out.println(calendar.getTime());
				
				iterator=accesodtocomercial.getList2(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));	
			}else if(campo.equals("fecha"))
			{
				System.out.println("fecha "+nombre);
				iterator=accesodtocomercial.getList("join x.tblpagos p where x.fecha ='"+nombre+"' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
				
			}else if(campo.equalsIgnoreCase("Buscar por") && nombre.equals(""))
			{
				iterator=accesodtocomercial.getList("join x.tblpagos p where p.estado = '0' or p.estado = '1' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
			}
			else if(nombre.equalsIgnoreCase("Buscar"))
			{
				SC.say("Se encuentra vacio el campo de busqueda");
			}
			
	    if(iterator!=null)
	    	{
	    	cuentascob = new ArrayList<DtocomercialDTO>();
	    	Tbldtocomercial tbldtocomercial;
	    	if(iterator.hasNext()){
	    	while (iterator.hasNext()) {
	    		Object[] pair = (Object[])iterator.next();//recupera el iterador 
	    			Set<TblpagoDTO> listpagodto=new HashSet<TblpagoDTO>();
	    			tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
					tblpago = (Tblpago) pair[1];//recupera tbldtocomercial en posicion 1
			
					documento = new DtocomercialDTO();		
					tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
					personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
					//tblpagolist=accesopago.getList();
					
					tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdFacturaPor().getIdPersona());
					personaFactDto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
					
					tblpagodto = new TblpagoDTO(tblpago.getIdEmpresa(),tblpago.getEstablecimiento(),documento, tblpago.getFechaVencimiento(), tblpago.getFechaRealPago(), tblpago.getValor(), tblpago.getEstado(),tblpago.getFormaPago());
					listpagodto.add(tblpagodto);

					Set<DtoComDetalleDTO> dtocomdetalledto =  new HashSet<DtoComDetalleDTO>();
					listtbldtocomercialdetalle=accesodetalle.findByQuery("where u.tbldtocomercial.idDtoComercial='"+tbldtocomercial.getIdDtoComercial()+"'");
					for(Tbldtocomercialdetalle tbldtocomercialdetalle: listtbldtocomercialdetalle){
						bodegadto=new BodegaDTO();
						bodegadto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
						bodegadto.setIdBodega(tbldtocomercialdetalle.getTblbodega().getIdBodega());
						productodto= new ProductoDTO();
						productodto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
						productodto.setEstablecimiento(tbldtocomercial.getEstablecimiento());
						productodto.setIdProducto(tbldtocomercialdetalle.getTblproducto().getIdProducto());
						dtocomdetalle= new DtoComDetalleDTO(tbldtocomercialdetalle.getIdDtoComercialDetalle(), bodegadto, productodto, tbldtocomercialdetalle.getCantidad(), tbldtocomercialdetalle.getPrecioUnitario(), tbldtocomercialdetalle.getDepartamento(), 
//								tbldtocomercialdetalle.getImpuesto(), 
								tbldtocomercialdetalle.getTotal(), tbldtocomercialdetalle.getDesProducto());				
						dtocomdetalledto.add(dtocomdetalle);
					}
					documento.setIdEmpresa(tbldtocomercial.getIdEmpresa());
					documento.setEstablecimiento(tbldtocomercial.getEstablecimiento());
					documento.setPuntoEmision(tbldtocomercial.getPuntoEmision());
					documento.setTblpagos(listpagodto);
					documento.setTbldtocomercialdetalles(dtocomdetalledto);
					documento.setTblpersonaByIdPersona(personadto);
					documento.setTblpersonaByIdFacturaPor(personaFactDto);
					documento.setIdDtoComercial(tbldtocomercial.getIdDtoComercial());
					documento.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
					documento.setFecha(tbldtocomercial.getFecha().toString());
					documento.setSubtotal(tbldtocomercial.getSubtotal());
					cuentascob.add(documento);
	    		}			
	    	}
	    	}
	    }catch (RuntimeException re) {
			throw re;
		} 
		return cuentascob;			

	}
	@Override
	public TipoPagoDTO buscarTipopago(String Tipopago) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String eliminarTipopago(TipoPagoDTO tipopago) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String grabarTipopago(TipoPagoDTO tipopago) {
		TbltipopagoHome acces = new TbltipopagoHome();
		Tbltipopago tblpago= new Tbltipopago();
		String mensaje = "";
		int ban=0;
		try {
			try {
				tblpago=acces.findByIdPlanIdCuenta(String.valueOf(tipopago.getIdPlan()),String.valueOf(tipopago.getIdCuenta()));
				ban=1;
				mensaje="Tipo de Pago ya ingresado en el plan de cuentas con C�digo"+String.valueOf(tipopago.getIdPlan());
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			if(ban==0){
				try {
					acces.grabar(new Tbltipopago(tipopago));
					mensaje="Ingreso realizado con \u00e9xito";
				} catch (ErrorAlGrabar e) {
					mensaje="No se pudo grabar el dato "+e.getMessage();
				}
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	@Override
	public List<TipoPagoDTO> listarTipopago(int ini, int fin) {
		// TODO Auto-generated method stub
		TbltipopagoHome acceso = new TbltipopagoHome(); 
		List<Tbltipopago> listado = null;
		List<TipoPagoDTO> listadoDTO;
		//try {
			listado=acceso.getList();
			listadoDTO = new ArrayList<TipoPagoDTO>();
            if (listado != null) {
                for (Tbltipopago TipoPago : listado) {
                    listadoDTO.add(crearTipoPago(TipoPago));
                }
            }
		/*} catch (RuntimeException re) {
			throw re;
		}*/
		return listadoDTO;
	}
	@Override
	public List<TipoPagoDTO> listarTipopagoLike(String busqueda, String campo) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String modificarTipopago(TipoPagoDTO Tipopago) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int numeroRegistrosTipopago(String tabla) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * 			FUNCION DE ACCESO PARA LAS OPERACIONES DE FACTURACION
	 */
	public <T> String grabar(T entidad) {
		String mensaje = "";
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.save(entidad);
			session.getTransaction().commit();
			mensaje = "Ingreso realizado con exito";
		} catch (RuntimeException re) {
			session.getTransaction().rollback();
			throw re;
		} finally {
			session.close();
		}
		return mensaje; 
	}
	
	public ProductoDTO crearProductoReg(Tblproducto prod, int TipoPrecio){
		ProductoDTO producto= new ProductoDTO(prod.getIdEmpresa(),prod.getEstablecimiento(),prod.getIdProducto(), prod.getCodigoBarras(),
				prod.getDescripcion(), prod.getStock(),
				crearSetProductoMultiImpuestoDTO(prod.getTblProdMultiImpuesto()),
//				prod.getImpuesto(),
				prod.getLifo(), prod.getFifo(), prod.getPromedio(),
				prod.getTblunidad().getIdUnidad(),prod.getTblcategoria().getIdCategoria(),
				prod.getTblmarca().getIdMarca(), prod.getEstado(), prod.getTipo(),prod.getImagen(), prod.getJerarquia(), prod.getCantidadunidad(),prod.getStockMinimo(),prod.getStockMax());
 		
		ProductoTipoPrecioDTO precioDTO=new ProductoTipoPrecioDTO();
		LinkedList<ProductoTipoPrecioDTO> precios = new LinkedList<ProductoTipoPrecioDTO>();
		
		ProductoTipoPrecioDTO productotipopreciodto = new ProductoTipoPrecioDTO();
		if(TipoPrecio==-1){
			TbltipoprecioHome tbltipopreciohome = new TbltipoprecioHome();
			tbltipopreciohome.getList();
			for(Tbltipoprecio tbltipoprecio: tbltipopreciohome.getList()){
			int idtipoprecio = tbltipoprecio.getIdTipoPrecio();
			productotipopreciodto=new ProductoTipoPrecioDTO();		
			productotipopreciodto = obtenerPrecio(prod.getIdProducto(),tbltipoprecio.getIdTipoPrecio());
			if (productotipopreciodto!=null){	
			if(idtipoprecio == 2){
				producto.setPVP(productotipopreciodto.getPorcentaje());
				}
				
			productotipopreciodto.setTblproducto(producto);
			precios.add(productotipopreciodto);
				}
			/*
		precioDTO=new ProductoTipoPrecioDTO();
		precioDTO.setTblproducto(producto);
		precioDTO.setPorcentaje(this.obtenerPrecio(prod.getIdProducto(),1));
		precios.add(precioDTO);
		
		precioDTO=new ProductoTipoPrecioDTO();
		precioDTO.setTblproducto(producto);
		double porcentaje=this.obtenerPrecio(prod.getIdProducto(),2);
		producto.setPVP(porcentaje);
		precioDTO.setPorcentaje(porcentaje);
		precios.add(precioDTO);
		
		precioDTO=new ProductoTipoPrecioDTO();
		precioDTO.setTblproducto(producto); 
		precioDTO.setPorcentaje(this.obtenerPrecio(prod.getIdProducto(),3));
		precios.add(precioDTO);
		}else{
			System.out.println("numero tipo precio: "+TipoPrecio);
			precioDTO=new ProductoTipoPrecioDTO();
			precioDTO.setTblproducto(producto);
			precioDTO.setPorcentaje(this.obtenerPrecio(prod.getIdProducto(),TipoPrecio));
			precios.add(precioDTO);
			*/
			}
		}else{
			System.out.println("numero tipo precio: "+TipoPrecio);
			productotipopreciodto=new ProductoTipoPrecioDTO();			
			productotipopreciodto = obtenerPrecio(prod.getIdProducto(),TipoPrecio);
			if (productotipopreciodto!=null){
			productotipopreciodto.setTblproducto(producto);
			precios.add(productotipopreciodto);
			}
			
		}
		System.out.println("cantPrecios: "+String.valueOf(precios.size()));
		
		producto.setProTip(precios);
		
		String cad="";
		for(int i=0;i<producto.getProTip().size();i++){
			cad=cad+ " "+producto.getProTip().get(i).getPorcentaje();
		}
		
		TblproductoMultiImpuestoHome tblmultiImpH= new TblproductoMultiImpuestoHome();
		List<TblproductoMultiImpuesto> tblmultiImp=tblmultiImpH.getListProMultImp(prod.getIdProducto());
		Set<TblproductoMultiImpuesto> multiImpuestos= new HashSet(0);
		for (TblproductoMultiImpuesto tblmi:tblmultiImp){
			multiImpuestos.add(tblmi);
		}
		producto.setTblmultiImpuesto(crearSetProductoMultiImpuestoDTO(multiImpuestos));
		System.out.println("REVISAR PRODUCTO: "+prod.getIdProducto()+" "+ prod.getCodigoBarras()+" "+
				prod.getDescripcion()+" "+ prod.getStock()+
//				" "+ prod.getImpuesto()+" "+
				"ProductoMultiImpuesto "+crearSetProductoMultiImpuestoDTO(multiImpuestos).size()+
				prod.getLifo()+" "+ prod.getFifo()+" "+ prod.getPromedio()+" "+
				prod.getTblunidad().getIdUnidad()+" "+prod.getTblcategoria().getIdCategoria()+" "+
				prod.getTblmarca().getIdMarca()+" "+ prod.getEstado()+" "+ prod.getImagen()+" "+ prod.getJerarquia()+" "+ prod.getCantidadunidad());
		
		if (precios.size()==0) producto=null;
		return producto;
	}
	
	public TipoPagoDTO crearTipoPago(Tbltipopago tp){
		return new TipoPagoDTO(tp.getIdTipoPago(),tp.getTipoPago(),tp.getIdPlan(), tp.getIdCuenta());
	}
	
	public PersonaDTO crearPersonaDTO(Tblpersona persona){
		System.out.println ("persona:"+persona.getCedulaRuc()+persona.getNombreComercial()+persona.getRazonSocial()+
				persona.getDireccion()+persona.getTelefono1()+persona.getTelefono2()+
				persona.getObservaciones()+persona.getMail()+persona.getEstado());
		// Para evitar error por los valores nulos en observaciones de la bd  
		if (persona.getObservaciones()== null )
		  {
			  persona.setObservaciones("");
		  }
		return new PersonaDTO(persona.getCedulaRuc(),persona.getNombreComercial(),persona.getRazonSocial(),
				persona.getDireccion(),persona.getLocalizacion(),persona.getTelefono1(),persona.getTelefono2(),
				persona.getObservaciones(),persona.getMail(),persona.getEstado()/*,
				persona.getTbldtocomercialsForIdPersona(),persona.getTblproveedors(),
				persona.getTblempleados(),persona.getTblclientes(),
				persona.getTbldtocomercialsForIdVendedor()*/);
	}
	
	public List<TipoPagoDTO> listaTipoPago(){
		List<TipoPagoDTO> TiposPago = null;
		List<Tbltipopago> listado = null;
		TbltipopagoHome acceso = new TbltipopagoHome();
		try {
			//Considerando que va a extraer como maximo 10 tipos de pago unicamente
			listado=acceso.getList(0, 10);
			TiposPago = new ArrayList<TipoPagoDTO>();
            if (listado != null) {
                for (Tbltipopago TipoPago : listado) {
                    TiposPago.add(crearTipoPago(TipoPago));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return TiposPago;
	}
	/*public ProductoTipoPrecioDTO listaProdTipoPrecio(int idProd, int idTipo){
		ProductoTipoPrecioDTO prodTipPrecio=new ProductoTipoPrecioDTO();
		
		return prodTipPrecio;
	}*/
	//Funcion para extraer listados de Productos
	public LinkedHashMap<Integer,ProductoDTO> listaProductos(Integer inicio, Integer numReg, Integer Tipo, Integer Jerarquia, Integer TipoPrecio,int StockNegativo) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;  
		//List<ProductoDTO> ProductosReg = null;
		List<BodegaDTO> listbodegadto = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		TblbodegaHome tblbodegahome = new TblbodegaHome();
		TblproductoTblbodegaHome tblproductotblbodegahome = new TblproductoTblbodegaHome();
		try {
			listado=acceso.getListProd(inicio, numReg, Tipo,Jerarquia);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {    
                	ProductoDTO productodto = new ProductoDTO();
                	productodto = crearProductoReg(Producto, TipoPrecio);
                	if (productodto!=null){
                	List<TblproductoTblbodega> listtblproductotblbodega=tblproductotblbodegahome.findByQuery("where u.tblproducto.idProducto="+Producto.getIdProducto());
                	//System.out.println(listtblproductotblbodega.size());
                	//System.out.println(Factum.banderaStockNegativo==1);
                	if(listtblproductotblbodega != null){	
                		listbodegadto = new ArrayList<BodegaDTO>();
                		
                		for(TblproductoTblbodega tblproductotblbodega: listtblproductotblbodega){
                			
                			if(tblproductotblbodega.getCantidad()>0 || StockNegativo==1){
	                		Tblbodega tblbodega = tblbodegahome.findById(tblproductotblbodega.getTblbodega().getIdBodega());
	                		BodegaDTO bodegadto =crearBodega(tblbodega);
	                		listbodegadto.add(bodegadto);
                			}
                		}
                	}
                	productodto.setTblbodega(listbodegadto);
                	ProductosReg.put(productodto.getIdProducto(),productodto);
                	}
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	}
	
	//Funcion para extraer listados de Productos sin paginacion
	public LinkedHashMap<Integer,ProductoDTO> listaProductos() {
		
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			listado=acceso.getListProd();
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {  
                	ProductoDTO productodto = new ProductoDTO();
                	productodto=crearProductoReg(Producto, -1);
                	ProductosReg.put(productodto.getIdProducto(), productodto);
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	}
	
	//listado que varia entre las opciones de stock 
	@Override
	public LinkedHashMap<Integer,ProductoDTO> listaProductosConFiltro(int ini, int fin, int op) {
		LinkedHashMap<Integer,ProductoDTO> ProductosReg = null;
		List<Tblproducto> listado = null;
		TblproductoHome acceso = new TblproductoHome();
		try {
			listado=acceso.getListProdConFiltro(ini, fin,op);
			ProductosReg = new LinkedHashMap<Integer,ProductoDTO>();
            if (listado != null) {
                for (Tblproducto Producto : listado) {     	
                	ProductosReg.put(Producto.getIdProducto(),crearProductoReg(Producto, -1));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return ProductosReg;
	}
	
	public List<TbltipodocumentoDTO>listarTipoDocumento()
	{
		List<TbltipodocumentoDTO> listadatos=new ArrayList<TbltipodocumentoDTO>();
		List<Tbltipodocumento>obtenerlista=new ArrayList<Tbltipodocumento>();
		TbltipodocumentoHome acceso=new TbltipodocumentoHome();
		TbltipodocumentoDTO setlista=new TbltipodocumentoDTO();
		obtenerlista=acceso.getList();
		for(Tbltipodocumento tbltipodocumento: obtenerlista)
		{
			setlista=new TbltipodocumentoDTO();
			setlista.setIdTipoDoc(tbltipodocumento.getIdTipoDoc());
			setlista.setTipoDoc(tbltipodocumento.getTipoDoc());
			listadatos.add(setlista);
		}
		return listadatos;
	}
	public List<MayorizacionDTO> listarMayorizacion(String FechaI, String FechaF,String nrt,String tipo)
	{
	
		List<MayorizacionDTO> mayorizaciondto = new ArrayList<MayorizacionDTO>();
		List<Mayorizacion> listmayorizacion=new ArrayList<Mayorizacion>();
		MayorizacionHome accesomayorizacion=new MayorizacionHome();
		MayorizacionDTO mayo= new MayorizacionDTO();
		List<DtocomercialDTO> setlista = new ArrayList<DtocomercialDTO>();
		List<Tbldtocomercial> recuperalista=new ArrayList<Tbldtocomercial>();
		TbldtocomercialHome acceso=new TbldtocomercialHome();
		DtocomercialDTO dtocomercial= new DtocomercialDTO();
		if(nrt.equals("") && tipo.equals("0"))
		{
			//System.out.println("tipo txt: "+tipo);
			listmayorizacion=accesomayorizacion.findByQuery("where u.id.fecha >= '"+FechaI+"' and u.id.fecha <='"+FechaF+"'");
					
			for (Mayorizacion mayorizacion: listmayorizacion) 
			{
				recuperalista=acceso.findByQuery("where u.idDtoComercial = '"+mayorizacion.getId().getIdDtocomercial()+"'");
				for(Tbldtocomercial tblcomercial: recuperalista)
				{
					mayo= new MayorizacionDTO();
					mayo.setFecha(mayorizacion.getId().getFecha());
					mayo.setCodigo(mayorizacion.getId().getCodigo());
					mayo.setOidTipoCuenta(mayorizacion.getId().getOidTipoCuenta());
					mayo.setCuenta(mayorizacion.getId().getCuenta());
					mayo.setDescripcion(mayorizacion.getId().getDescripcion());
					mayo.setnumeroDocumento(mayorizacion.getId().getIdDtocomercial());
					mayo.setnumeroTransaccion(tblcomercial.getNumRealTransaccion());
					mayo.setSigno(mayorizacion.getId().getSigno());
					mayo.setValor(mayorizacion.getId().getValor());
					mayo.setNivel(mayorizacion.getId().getNivel());
					mayo.setPadre(mayorizacion.getId().getPadre());
					mayo.setPeriodoAnterior(mayorizacion.getId().getPeriodoAnterior());
					mayorizaciondto.add(mayo);	
				}
			}
		}
		else{
			
			recuperalista=acceso.findByQuery("where u.numRealTransaccion = '"+nrt+"' and u.tipoTransaccion = '"+tipo+"'");
			for(Tbldtocomercial tblcomercial: recuperalista)
			{
				System.out.println("Dto comercial"+tblcomercial.getIdDtoComercial() +" "+tblcomercial.getNumRealTransaccion());
				listmayorizacion=accesomayorizacion.findByQuery("where u.id.idDtocomercial = '"+tblcomercial.getIdDtoComercial()+"'");
				for (Mayorizacion mayorizacion: listmayorizacion) 
				{	
					mayo= new MayorizacionDTO();
					mayo.setFecha(mayorizacion.getId().getFecha());
					mayo.setCodigo(mayorizacion.getId().getCodigo());
					mayo.setOidTipoCuenta(mayorizacion.getId().getOidTipoCuenta());
					mayo.setCuenta(mayorizacion.getId().getCuenta());
					mayo.setDescripcion(mayorizacion.getId().getDescripcion());
					mayo.setnumeroDocumento(mayorizacion.getId().getIdDtocomercial());
					mayo.setnumeroTransaccion(tblcomercial.getNumRealTransaccion());
					mayo.setSigno(mayorizacion.getId().getSigno());
					mayo.setValor(mayorizacion.getId().getValor());
					mayo.setNivel(mayorizacion.getId().getNivel());
					mayo.setPadre(mayorizacion.getId().getPadre());
					mayo.setPeriodoAnterior(mayorizacion.getId().getPeriodoAnterior());
					mayorizaciondto.add(mayo);	
				}
			}
		
		}
		return mayorizaciondto;
	}
	public List<DtocomercialDTO> tbldtocomercialToDto(List<Tbldtocomercial> listado, String Tipo){
		List<DtocomercialDTO> docs = new ArrayList<DtocomercialDTO>();
		List<Tbldtoelectronico> lsttbldtoelectronico = null;
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		TbldtoelectronicoHome tbldtoelectronicohome = new TbldtoelectronicoHome(); 
		
		for (Tbldtocomercial doc : listado) {
			DtocomercialDTO documento=new DtocomercialDTO(); 
			DtoelectronicoDTO dtoelectronicodto=new DtoelectronicoDTO(); 

			documento=crearDtocomercialDTO(doc);
			if(doc.getTipoTransaccion()==7){
				try {
					DtocomercialDTO documentoDetalle=getDtoID(Integer.parseInt(documento.getAutorizacion()));
					
					documento.setTbldtocomercialdetalles(documentoDetalle.getTbldtocomercialdetalles());
					documento.setTbldtocomercialTbltipopagos(documentoDetalle.getTbldtocomercialTbltipopagos());
					documento.setTblpagos(documentoDetalle.getTblpagos());
					documento.setTblretencionsForIdFactura(documentoDetalle.getTblretencionsForIdFactura());
					doc.setIdDtoComercial(documentoDetalle.getIdDtoComercial());
				} catch (NumberFormatException id) {

				}

			}
			double retIVA=0.0;
			double retRENTA=0.0;
			String numeroRetencion="";
			Iterator iterRet = doc.getTblretencionsForIdFactura().iterator(); 
			dtoelectronicodto=null;
			lsttbldtoelectronico=tbldtoelectronicohome.findByQuery("where u.tbldtocomercial.idDtoComercial='"+doc.getIdDtoComercial()+"'");
			if(lsttbldtoelectronico.size()>0){
				for(Tbldtoelectronico tbldtoelectronico: lsttbldtoelectronico){
					dtoelectronicodto=crearDtoelectronicoDTO(tbldtoelectronico);
				}
			}else{
				dtoelectronicodto=null;
			}
			System.out.println("dtoelectronicodto: "+dtoelectronicodto);
			documento.setDtoelectronico(dtoelectronicodto);
			System.out.println("Seteado dtoelectronicodto");
			while (iterRet.hasNext()) {
				System.out.println("En while iter ret");
				Tblretencion retencion=new Tblretencion();
				retencion=(Tblretencion) iterRet.next();
//				System.out.println("Nueva retencion "+retencion.getIdRetencion());
				retIVA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "0");
//				System.out.println("retIVA: "+retIVA);
				retRENTA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "1");
//				System.out.println("retRENTA: "+retRENTA);
				if(Tipo.equals("1")|| Tipo.equals("0")){
					numeroRetencion=acceso.NumRetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()));
				}
//				System.out.println("numeroRetencion: "+numeroRetencion);
				break;
			}
			if(retIVA==0.0 && retRENTA==0.0){
				System.out.println("retIva retRenta cero");
				documento.setTotalRetencionIVA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"0"));
				documento.setTotalRetencionRENTA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"1")); 
				documento.setTotalRetencion();
				System.out.println("SettotalesRet ");
			}else{
				System.out.println("retIva retRenta NO cero");
				documento.setTotalRetencionIVA(retIVA);
				documento.setTotalRetencionRENTA(retRENTA);//cambiar luego
				if(Tipo.equals("1") || Tipo.equals("0") || Tipo.equals("7")){
					documento.setNumeroRetencion(numeroRetencion);
				}
				documento.setTotalRetencion();
				System.out.println("SettotalesRet ");
			}
			System.out.println("doc set iddtocomercial");
			doc.setIdDtoComercial(documento.getIdDtoComercial());
			System.out.println("doc seteado iddtocomercial");
			//System.out.println("vendedor: "+documento.getTblpersonaByIdVendedor().getApellidos());
			docs.add(documento);
			System.out.println("docs add coumento");
		}
		System.out.println("despues for tbldtocomercialToDTO");
		return docs;
	}
	//
	public List<DtocomercialDTO> listarFacturas(String FechaI, String FechaF,String Tipo) {
		List<DtocomercialDTO> docs = null;
		List<Tbldtocomercial> listado = null;
		List<Tbldtoelectronico> lsttbldtoelectronico = null;
		try {
			String fechaI=FechaI;
			String fechaF=FechaF;

			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TbldtoelectronicoHome tbldtoelectronicohome = new TbldtoelectronicoHome(); 

			listado=acceso.ReproteCaja(fechaI,fechaF,Tipo);
			docs = new ArrayList<DtocomercialDTO>();
			if (listado != null) {

				for (Tbldtocomercial doc : listado) {
					DtocomercialDTO documento=new DtocomercialDTO(); 
					DtoelectronicoDTO dtoelectronicodto=new DtoelectronicoDTO(); 

					documento=crearDtocomercialDTO(doc);
					if(doc.getTipoTransaccion()==7){
						try {
							DtocomercialDTO documentoDetalle=getDtoID(Integer.parseInt(documento.getAutorizacion()));

							documento.setTbldtocomercialdetalles(documentoDetalle.getTbldtocomercialdetalles());
							documento.setTbldtocomercialTbltipopagos(documentoDetalle.getTbldtocomercialTbltipopagos());
							documento.setTblpagos(documentoDetalle.getTblpagos());
							documento.setTblretencionsForIdFactura(documentoDetalle.getTblretencionsForIdFactura());
							doc.setIdDtoComercial(documentoDetalle.getIdDtoComercial());
						} catch (NumberFormatException id) {

						}

					}
					double retIVA=0.0;
					double retRENTA=0.0;
					String numeroRetencion="";
					Iterator iterRet = doc.getTblretencionsForIdFactura().iterator(); 
					dtoelectronicodto=null;
					lsttbldtoelectronico=tbldtoelectronicohome.findByQuery("where u.tbldtocomercial.idDtoComercial='"+doc.getIdDtoComercial()+"'");
					System.out.println("lsttbldtoelectronico : "+lsttbldtoelectronico.size());
					if(lsttbldtoelectronico.size()>0){
						System.out.println("lsttbldtoelectronico >0: "+lsttbldtoelectronico.size());
						for(Tbldtoelectronico tbldtoelectronico: lsttbldtoelectronico){
							dtoelectronicodto=crearDtoelectronicoDTO(tbldtoelectronico);
						}
					}else{
						System.out.println("lsttbldtoelectronico null");
						dtoelectronicodto=null;
					}
					System.out.println("dtoelectronicodto: "+dtoelectronicodto);
					documento.setDtoelectronico(dtoelectronicodto);
					System.out.println("Seteado dtoelectronicodto");
					while (iterRet.hasNext()) {
						System.out.println("While iter");
						Tblretencion retencion=new Tblretencion();
						retencion=(Tblretencion) iterRet.next();
						System.out.println("Retencion: "+retencion.getIdRetencion());
						retIVA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "0");
						System.out.println("RetIva: "+retIVA);
						retRENTA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "1");
						System.out.println("RetRenta: "+retRENTA);
						if(Tipo.equals("1")|| Tipo.equals("0")){
							numeroRetencion=acceso.NumRetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()));
						}
						System.out.println("numeroRetencion: "+numeroRetencion);
						break;
					}
					if(retIVA==0.0 && retRENTA==0.0){
						System.out.println("RetiVA y renta 0");
						documento.setTotalRetencionIVA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"0"));
						documento.setTotalRetencionRENTA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"1")); 
						documento.setTotalRetencion();
					}else{
						System.out.println("RetIva y Renta no 0 ");
						documento.setTotalRetencionIVA(retIVA);
						documento.setTotalRetencionRENTA(retRENTA);//cambiar luego
						if(Tipo.equals("1") || Tipo.equals("0") || Tipo.equals("7")){
							documento.setNumeroRetencion(numeroRetencion);
						}
						documento.setTotalRetencion();
					}
					System.out.println("after retenciones ");
					doc.setIdDtoComercial(documento.getIdDtoComercial());
					System.out.println("renew id dto");
					//System.out.println("vendedor: "+documento.getTblpersonaByIdVendedor().getApellidos());
					docs.add(documento);
				}
			}
		} catch (RuntimeException re) {
			throw re;
		} /*catch (ParseException e1) {
	     System.out.println("Error al convertir fecha: "+e1);
	    } */
		return docs;
	}
	 
	 public List<DtocomercialDTO> listarReportesCliente(String FechaI, String FechaF,String Tipo,String ruc, String nombres, String razonSocial) {
	  List<DtocomercialDTO> docs = null;
	  List<Tbldtocomercial> listado = null;
	     try {
	      String fechaI=FechaI;
	      String fechaF=FechaF;
	     
	      TbldtocomercialHome acceso = new TbldtocomercialHome();
	     
	      listado=acceso.ReporteFacCliente(fechaI,fechaF,Tipo,ruc,nombres,razonSocial);
	      docs = new ArrayList<DtocomercialDTO>();
	            if (listado != null) {
	            	docs=tbldtocomercialToDto(listado,Tipo); 
//	             for (Tbldtocomercial doc : listado) {
//	                  DtocomercialDTO documento=new DtocomercialDTO(); 
//	                  documento=crearDtocomercialDTO(doc);
//	                  if(doc.getTipoTransaccion()==7){
//							try {
//								DtocomercialDTO documentoDetalle=getDtoID(Integer.parseInt(documento.getAutorizacion()));
//
//								documento.setTbldtocomercialdetalles(documentoDetalle.getTbldtocomercialdetalles());
//								documento.setTbldtocomercialTbltipopagos(documentoDetalle.getTbldtocomercialTbltipopagos());
//								documento.setTblpagos(documentoDetalle.getTblpagos());
//								documento.setTblretencionsForIdFactura(documentoDetalle.getTblretencionsForIdFactura());
//								doc.setIdDtoComercial(documentoDetalle.getIdDtoComercial());
//							} catch (NumberFormatException id) {
//
//							}
//
//						}
//	                  double retIVA=0.0;
//	                  double retRENTA=0.0;
//	                  String numeroRetencion="";
//	                  Iterator iterRet = doc.getTblretencionsForIdFactura().iterator();   
//	                  while (iterRet.hasNext()) {
//	               Tblretencion retencion=new Tblretencion();
//	               retencion=(Tblretencion) iterRet.next();
//	               retIVA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "0");
//	               retRENTA=acceso.RetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()), "1");
//	               if(Tipo.equals("1") || Tipo.equals("0")){
//	            	  numeroRetencion=acceso.NumRetencionPorTblFactura(String.valueOf(doc.getIdDtoComercial()));
//	              }
//	               break;
//	               }
//	                  if(retIVA==0.0 && retRENTA==0.0){
//	                   documento.setTotalRetencionIVA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"0"));
//	                   documento.setTotalRetencionRENTA(acceso.RetencionPorFactura(String.valueOf(doc.getIdDtoComercial()),"1")); 
//	                   documento.setTotalRetencion();
//	                  }else{
//	                   documento.setTotalRetencionIVA(retIVA);
//	                   documento.setTotalRetencionRENTA(retRENTA);
//	                   		if(Tipo.equals("1") || Tipo.equals("0")){
//		                   documento.setNumeroRetencion(numeroRetencion);
//		                   }
//	                   documento.setTotalRetencion();
//	                  }
//	                  doc.setIdDtoComercial(documento.getIdDtoComercial());
//	                  docs.add(documento);
//	                }
	           }
	     } catch (RuntimeException re) {
	      throw re;
	     } 
	     return docs;
	 }
	
	/*
	 * 				FUNCION CREADA POR JOHNY 05 - JULIO - 2012
	 */
	public List<DtocomercialDTO> listarFacturasProducto(String FechaI, String FechaF,String idProducto,String Tipo) {
		
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			Iterator resultado = acceso.ReporteCajaProducto(fechaI, fechaF,idProducto,Tipo);
			while ( resultado.hasNext() ) { 
				Object[] pair = (Object[])resultado.next();
				Tbldtocomercialdetalle detalle = (Tbldtocomercialdetalle) pair[1];
				Tbldtocomercial doc = (Tbldtocomercial) pair[0];
				if(doc!=null){
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto = crearDtocomercialDTO(doc);
//					list.add(docdto);
					listado.add(doc);
				}
			}
			list=tbldtocomercialToDto(listado,Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha en listadofacturasproducto: "+e1);
		}
		return list;
	}
	
	
	
	
	
	@Override
	public List<DtocomercialDTO> listarFacturasCliente(String FechaI, String FechaF,String idCliente,String Tipo) {
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		TblpersonaHome accesopersona = new TblpersonaHome();
		TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome(); 
		try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			Iterator resultado = acceso.ReproteCajaCliente(fechaI, fechaF,idCliente,Tipo);
			while ( resultado.hasNext() ) { 
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}
			System.out.println("listarfacturascliente antes listado "+listado.size());
			list=tbldtocomercialToDto(listado, Tipo);
			System.out.println("listarfacturascliente despues listado "+list.size());
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha en listarFacturasCliente: "+e1);
		}
		return list;
	}
	/**
	 * M�todo para generar un reporte de caja por Vendedor
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param idCliente id del cliente
	 * @return Lista de DtocomercialDTO
	 */
	public List<DtocomercialDTO> listarFacturasVendedor(String FechaI, String FechaF,String idVendedor,String Tipo) {
		
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			Iterator resultado = acceso.ReproteCajaVendedor(fechaI, fechaF,idVendedor,Tipo);
		
			while ( resultado.hasNext() ) {
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}
			list=tbldtocomercialToDto(listado, Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasVendedor: "+e1);
		}
		return list;
	}
	public List<DtocomercialDTO> listarFacturasVendedorCliente(String FechaI, String FechaF,String idCliente,String idVendedor,String Tipo) {				
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			Iterator resultado = acceso.ReproteCajaVendedorCliente(fechaI, fechaF,idVendedor,idCliente,Tipo);
		
			while ( resultado.hasNext() ) {
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}
			list=tbldtocomercialToDto(listado, Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasVendedorCliente: "+e1);
		}
		return list;
		
		
		
	}
				
	
	
	
	/**
	 * Funcion para listar Documentos segun Vendedor, Cliente y Producto
	 * @param FechaI		Tipo String Fecha de Inicio
	 * @param FechaF		Tipo String Fecha de Fin
	 * @param idCliente		Tipo String con el ID del cliente
	 * @param idVendedor	Tipo String con el ID del vendedor
	 * @param Tipo			String Tipo que indica el tipo de documento
	 * @return		Regresa un listado de objetos de tipo DtocomercialDTO
	 */
	public List<DtocomercialDTO> listarFacturasVendedorClienteProducto(String FechaI, String FechaF,String idCliente,String idVendedor, String idProducto, String Tipo) {
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			Iterator resultado = acceso.ReporteCajaVendedorClienteProducto(fechaI, fechaF,idVendedor,idCliente, idProducto, Tipo);
		
			while ( resultado.hasNext() ) { 
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}							
			list=tbldtocomercialToDto(listado, Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasVendedorClienteProducto: "+e1);
		}
		return list;
		
		
	}
	
	
	public List<DtocomercialDTO> listarFacturasClienteProducto(String FechaI, String FechaF,
			String idCliente, String idProducto, String Tipo) {

		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			Iterator resultado = acceso.ReporteCajaClienteProducto(fechaI, fechaF,idCliente, idProducto, Tipo);
		
			while ( resultado.hasNext() ) { 
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}
			list=tbldtocomercialToDto(listado, Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasClienteProducto: "+e1);
		}
		return list;
		
		
	}
	
	public List<DtocomercialDTO> listarFacturasNumDto(String FechaI, String FechaF,
			String Tipo, String NumDto) {
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			List<Tbldtocomercial> resultado = new ArrayList<Tbldtocomercial>();
			if(Tipo.equals("0") || Tipo.equals("2")){
				resultado = acceso.ReporteCajaNumDtoVenta(fechaI, fechaF,Tipo, NumDto);
			}else if(Tipo.equals("1") || Tipo.equals("10")){
				resultado = acceso.ReporteCajaNumDtoCompra(fechaI, fechaF,Tipo, NumDto);
			}
			list=tbldtocomercialToDto(resultado, Tipo);
		
//			for ( Tbldtocomercial doc: resultado ) { 
//				//Object[] pairdoc = (Object[])resultado.next();				
//				//Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
//				System.out.println("CAST TBLDTOCOMERCIAL");
//				if(doc!=null){
//					System.out.println("NO ES NULL EL OBJETO");
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					System.out.println("CASTA TBLCLIENTE");
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					System.out.println("CASTA TBLVENDEDOR");
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);
//							System.out.println("CASTA DTOCOMERCIAL");
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
//					
//				}
//			}							
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasNumDto: "+e1);
		}
		return list;
	}
	
	
	
	public List<DtocomercialDTO> listarFacturasVendedorProducto(String FechaI, String FechaF,
			String idVendedor, String idProducto, String Tipo) {
		List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
	    try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			TblpersonaHome accesopersona = new TblpersonaHome();
			TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
			Iterator resultado = acceso.ReporteCajaVendedorProducto(fechaI, fechaF,idVendedor, idProducto, Tipo);
		
			while ( resultado.hasNext() ) { 
				Object[] pairdoc = (Object[])resultado.next();				
				Tbldtocomercial doc = (Tbldtocomercial) pairdoc[0];
				if(doc!=null){
//					Tblpersona tblpersona= new Tblpersona();
//					DtocomercialDTO docdto = new DtocomercialDTO();
//					docdto.setAutorizacion(doc.getAutorizacion());
//					docdto.setEstado(doc.getEstado());
//					docdto.setExpiracion(String.valueOf(doc.getExpiracion()));
//					docdto.setFecha(String.valueOf(doc.getFecha()));
//					docdto.setIdDtoComercial(doc.getIdDtoComercial());
//					docdto.setNumPagos(doc.getNumPagos());
//					docdto.setNumRealTransaccion(doc.getNumRealTransaccion());
//					docdto.setSubtotal(doc.getSubtotal());
//					docdto.setTipoTransaccion(doc.getTipoTransaccion());
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdPersona().getIdPersona());
//					PersonaDTO perC=new PersonaDTO();
//					perC.setIdPersona(tblpersona.getIdPersona());
//					perC.setRazonSocial(tblpersona.getRazonSocial());
//					perC.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdPersona(perC);
//					tblpersona= new Tblpersona();
//					tblpersona=accesopersona.findById(doc.getTblpersonaByIdVendedor().getIdPersona());
//					PersonaDTO perV=new PersonaDTO();
//					perV.setIdPersona(tblpersona.getIdPersona());
//					perV.setRazonSocial(tblpersona.getRazonSocial());
//					perV.setNombreComercial(tblpersona.getNombreComercial());
//					docdto.setTblpersonaByIdVendedor(perV);	
//					
//					DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
//					Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
//					System.out.println("Detalles Documento: "+ doc.getTbldtocomercialdetalles().size());
//					Iterator resultadodetalles = doc.getTbldtocomercialdetalles().iterator();					
//					while(resultadodetalles.hasNext()){						
//						Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
//						if (docdet!=null){
//							
//							Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
//							tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
//							dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), tbldtocomercialdetalle.getImpuesto(), tbldtocomercialdetalle.getTotal()); 
//									
//							tbldtocomercialdetalles.add(dtocomdetalledto);							
//						}
//					}
//					docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
//					
//					list.add(docdto);
					listado.add(doc);
				}
			}					
			list=tbldtocomercialToDto(listado, Tipo);
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha listarFacturasVendedorProducto: "+e1);
		}
		return list;
	}
	/**
	 * 			FUNCION DE ACCESO PARA LAS OPERACIONES DE FACTURACION
	 */
	
	/**
	 * 			FUNCION DE ACCESO PARA LAS OPERACIONES DE FACTURACION
	 */
	public void grabarDetalleMultImp(Set<TbldtocomercialdetalleTblmultiImpuesto> detalleMultImpDTO,Integer id){
		System.out.println("En grabar detalle de multi impuestos "+id+" :"+detalleMultImpDTO.size());
		List<Tbldtocomercialdetalle>listtbldtocomercialdetalle=new ArrayList<Tbldtocomercialdetalle>();
		TbldtocomercialdetalleTblmultiImpuestoHome accesodetMultImp=new TbldtocomercialdetalleTblmultiImpuestoHome();
		 TbldtocomercialdetalleHome accesodetalle=new TbldtocomercialdetalleHome();
		listtbldtocomercialdetalle = accesodetalle.findByQuery("where u.tbldtocomercial.idDtoComercial='"+id+"'");
		System.out.println("Lista de detalles "+listtbldtocomercialdetalle.size());
		Set<TbldtocomercialdetalleTblmultiImpuesto> detalleMultImpDTO2= new HashSet<TbldtocomercialdetalleTblmultiImpuesto>(0);
		for (Tbldtocomercialdetalle det:listtbldtocomercialdetalle){ 
			System.out.println("En fors "+det.getIdDtoComercialDetalle()+" prod "+det.getTblproducto().getIdProducto());
			System.out.println("Lista de detalles parametro"+detalleMultImpDTO.size());
			 for (TbldtocomercialdetalleTblmultiImpuesto detMultImp:detalleMultImpDTO){
				 System.out.println("En fors "+det.getIdDtoComercialDetalle()+" prod "+det.getTblproducto().getIdProducto());
				 
				 Tbldtocomercialdetalle detM=detMultImp.getTbldtocomercialdetalle();
				 System.out.println("En fors prod a grabar "+detM.getTblproducto().getIdProducto());
				 System.out.println("En fors productos "+detM.getTblproducto().getIdProducto().equals(det.getTblproducto().getIdProducto()));
				 if (detM.getTblproducto().getIdProducto().equals(det.getTblproducto().getIdProducto())){
					 detMultImp.setTbldtocomercialdetalle(det);
					 try {
						accesodetMultImp.grabar(detMultImp);
//						break;
					} catch (ErrorAlGrabar e) {
						// TODO Auto-generated catch block
						e.printStackTrace(System.out);
						e.printStackTrace();
					}
//					 detalleMultImpDTO2.add(detMultImp)
				 }
			 }
		 }
	}
//	FUNCION PARA GRABAR UN DOCUMENTO COMERCIAL
	public String grabarDocumento(DtocomercialDTO documentoDTO, Integer ind, Double iva, Integer anular, String responsable, String planCuentaID, int ConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales){
		System.out.println("INDICADOR DE + O - INVENTARIO: "+ind);
		String mensaje = "";
		Boolean pagoAsociado = false;
		Integer cuentaGastoIngreso=0;
		try{
			System.out.println("ATES DE "+documentoDTO.getEstado()+"tipo"+documentoDTO.getTipoTransaccion());
			System.out.println("NO DEBERIA ANULAR CON PAGOS-anularca: "+anular);
			System.out.println("Num de pagos: "+documentoDTO.getTblpagos().size());
		}catch(Exception e){
			e.printStackTrace(System.out);
			System.out.println("ERROR en grabarDocumento pagos: "+e.getMessage());
			System.out.println("ERROR en grabarDocumento pagos: "+e.getStackTrace().toString());
			System.out.println("ERROR en grabarDocumento pagos: "+e.getCause());
			
		}

		if(pagoAsociado){
			mensaje="El documento no se puede eliminar, tiene pagos Asociados.";
		}
		else{	
			//Aqui analizamos si se va a eliminar un gasto o ingreso par extraer el id de la cuenta y setear el variable anular
			if(documentoDTO.getTipoTransaccion()==7 || documentoDTO.getTipoTransaccion()==12)
			{
				if(anular>3000)
				{//Aqui restamos 3000 para extraer el id de la cuenta y seteamos la variable anular a -10
					cuentaGastoIngreso=anular-3000;
					anular=-10;
				}
				else
				{
					cuentaGastoIngreso=anular;
				}
			}
			System.out.println("ID CUENTA ANULACION GASTO: " + cuentaGastoIngreso);
			//Extraigo cliente y vendedor
			TblpersonaHome accesoP = new TblpersonaHome();
			//Long id = (long) documentoDTO.getTblpersonaByIdPersona().getIdPersona();
			Tblpersona cliente = accesoP.load(documentoDTO.getTblpersonaByIdPersona().getIdPersona());
			//id = (long) documentoDTO.getTblpersonaByIdVendedor().getIdPersona();//.getTblpersonaByIdPersona().getIdPersona();
			Tblpersona vendedor = accesoP.load(documentoDTO.getTblpersonaByIdVendedor().getIdPersona());
			// Add Vendedor
			//Tblpersona nombreVendedor = accesoP.load(id);
			
			Tblpersona facturaPor = accesoP.load(documentoDTO.getTblpersonaByIdFacturaPor().getIdPersona());

			if(documentoDTO.getTblretencionsForIdFactura() != null)		
				System.out.println("NUMERO DE RETENCIONES DETALLE: "+documentoDTO.getTblretencionsForIdFactura().size());

			Tbldtocomercial documento = new Tbldtocomercial(documentoDTO, cliente, vendedor, facturaPor);

			TblpagoDTO pagoDTO = new TblpagoDTO();	
			DtocomercialTbltipopagoDTO tipopagoDTO = new DtocomercialTbltipopagoDTO();
			System.out.println("Anticipo: "+documentoDTO.getIdAnticipo());
			System.out.println("Nota Credito: "+documentoDTO.getIdNotaCredito());

			Set<Tbldtocomercialdetalle> detalles = new HashSet<Tbldtocomercialdetalle>();
			Set<Tblretencion> retenciones = new HashSet<Tblretencion>();
			Set<TbldtocomercialTbltipopago> pagosAsociados = new HashSet<TbldtocomercialTbltipopago>();
			Set<Tblpago> pagos = new HashSet<Tblpago>();
			//Tblpago pago = new Tblpago();
			Set<Tblproducto> productos = new HashSet<Tblproducto>();
			Set<TblproductoTblbodega> prodBods = new HashSet<TblproductoTblbodega>();
			//try{
			Set<DtoComDetalleDTO> detallesDTO = documentoDTO.getTbldtocomercialdetalles();
			Set<RetencionDTO> listaRetenciones = documentoDTO.getTblretencionsForIdFactura();		
			//PASO DE LAS RETENCIONES AL DTODOCUMENTO
			iterDetalles = listaRetenciones.iterator();
			RetencionDTO retDTO = new RetencionDTO();
			while (iterDetalles.hasNext()) {
				
				retDTO = (RetencionDTO) iterDetalles.next();
				System.out.println("Tiene retencion "+ retDTO.getNumRealRetencion());
				retDTO.setTbldtocomercialByIdFactura(null);
				
				Tblretencion ret = new Tblretencion(retDTO);
				Integer idRetencion=null;
				try{
					idRetencion=retDTO.getIdRetencion();
				}catch(Exception e){
					idRetencion=null;
				}
				ret.setIdRetencion(idRetencion);
				if (retDTO.getTbldtocomercialByIdDtoComercial()!=null){
				cliente = accesoP.load(retDTO.getTbldtocomercialByIdDtoComercial().getTblpersonaByIdPersona().getIdPersona());
				vendedor = accesoP.load(retDTO.getTbldtocomercialByIdDtoComercial().getTblpersonaByIdVendedor().getIdPersona());
				facturaPor = accesoP.load(retDTO.getTbldtocomercialByIdDtoComercial().getTblpersonaByIdFacturaPor().getIdPersona());

				Tbldtocomercial documentoRet = new Tbldtocomercial(retDTO.getTbldtocomercialByIdDtoComercial(), cliente, vendedor, facturaPor);
				ret.setTbldtocomercialByIdDtoComercial(documentoRet);
				}
				retenciones.add(ret);
				System.out.println("Tiene retencion");
			}	
			
			Set<TbldtocomercialdetalleTblmultiImpuesto> detalleMultImpDTO= new HashSet<TbldtocomercialdetalleTblmultiImpuesto>(0);
			
			if(documentoDTO.getTipoTransaccion()!=7 && documentoDTO.getTipoTransaccion()!=4	&& documentoDTO.getTipoTransaccion()!=12&& documentoDTO.getTipoTransaccion()!=15){
				System.out.println("IDDTO: "+documentoDTO.getTipoTransaccion());
				iterDetalles = detallesDTO.iterator();
				//A continuacion vamos a enlazar todos los detalles del documento al documento

				Integer detal =0;
				DtoComDetalleDTO detalleDTO = new DtoComDetalleDTO();
				//documento.setTbldtocomercialdetalles(new HashSet());
				//CREAMOS UN OBJETO DE ACCESO PARA LA CLASE PRODUCTO

				TblproductoHome accesoProd = new TblproductoHome();
				Tblproducto prod = new Tblproducto();
				TblproductoTblbodega prodBod = new TblproductoTblbodega();
				TblproductoTblbodegaHome prodBodHome = new TblproductoTblbodegaHome();
				TblproductoelaboradoHome tblproductoelaboradohome = new TblproductoelaboradoHome();

				Tblproducto prodelaborado = new Tblproducto();
				List<Tblproductoelaborado> listtblproductoelaborado = new ArrayList<Tblproductoelaborado>(0);
				Double stock =0.0;
				Double stockB = 0.0;
				String clave="";
				HashMap<String,Tblproducto> mapTbldtocomercialdetalle = new HashMap<String,Tblproducto>();
				Tblproducto productoaux;
				List<Integer> prodGuardados= new ArrayList<Integer>();
				while (iterDetalles.hasNext()) {
					detalleDTO = (DtoComDetalleDTO) iterDetalles.next();
					Tbldtocomercialdetalle det = new Tbldtocomercialdetalle(detalleDTO);
					Set<TbldtocomercialdetalleTblmultiImpuesto> detalleMultImp=crearSetDtoComDetalleMultiImpuestoDTO(detalleDTO.getTblimpuestos());
//					Set<TbldtocomercialdetalleTblmultiImpuesto> 
//					detalleMultImpDTO= new HashSet<TbldtocomercialdetalleTblmultiImpuesto>(0);
					for (TbldtocomercialdetalleTblmultiImpuesto detalleImp:detalleMultImp){
						detalleImp.setTbldtocomercialdetalle(det);
						detalleMultImpDTO.add(detalleImp);
					}
//					det.setTblimpuestos(detalleMultImpDTO);
					System.out.println("ID PRODUCTO DETALLE "+det.getTblproducto().getIdProducto()+" "+det.getTblproducto().getCodigoBarras());
					ProductoDTO prodDTO=this.buscarProducto(String.valueOf(det.getTblproducto().getIdProducto()),"idProducto");
					System.out.println("COSTO PRODUCTO "+prodDTO.getPromedio());
					det.setCostoProducto(prodDTO.getPromedio());
					System.out.println("COSTO PRODUCTO DETALLE "+det.getCostoProducto());
					det.setTbldtocomercial(documento);
					detalles.add(det);
					//documento.getTbldtocomercialdetalles().add(det);
					//System.out.println("Producto ID: "+ detalleDTO.getIdProducto());
					System.out.println("anular es:"+anular);
					System.out.println("Estado:"+documento.getEstado()+"");
					Double cantProd = det.getCantidad();
					if(anular < 0){
						documento.setEstado('2');
					}
					System.out.println("Estado:"+documento.getEstado()+"");
					if(documento.getEstado() == '1' || documento.getEstado() == '2' || documento.getTipoTransaccion()==0 || documento.getTipoTransaccion()==2 || documento.getTipoTransaccion()==26)//ESTADO 2 ES ANULAR DOCUMENTO 0 ES FACTURA
					{
						prod = accesoProd.findByNombre(detalleDTO.getProducto().getIdProducto()+"","idProducto");//accesoProd.load(detalleDTO.getIdProducto());
						//prod.setIdProducto(detalleDTO.getProducto().getIdProducto());
						//prod.setStock(detalleDTO.getProducto().getStock());
						System.out.println("Tipo:"+documentoDTO.getTipoTransaccion()+"");
						if(documentoDTO.getTipoTransaccion()==1||documentoDTO.getTipoTransaccion()==10 || documentoDTO.getTipoTransaccion()==27)//Si es una compra, el nuevo precio de compra
							//se debe agregar al producto y actualizarlo
						{
							System.out.println("Entro en cambio de promedio:"+det.getPrecioUnitario()+"");
							Double precioActual=0.0;
							Double descuento = 0.0;
							descuento = det.getDepartamento();
							descuento = descuento/100;
							descuento = 1-descuento;
							precioActual = det.getPrecioUnitario();
							precioActual = precioActual*descuento;
							precioActual=CValidarDato.getDecimal(banderaNumeroDecimales,precioActual);
//							if(documentoDTO.getTipoTransaccion()==10)
//							{
//								precioActual = precioActual/(1+(banderaIVA/100));
//								precioActual=CValidarDato.getDecimal(banderaNumeroDecimales,precioActual);
//							}
							System.out.println("Entro en cambio de promedio:"+precioActual+"");
							System.out.println("Entro en cambio de promedio:"+prod.getLifo()+"");
							System.out.println("Entro en cambio de promedio:"+prod.getFifo()+"");
							prod.setFifo(prod.getLifo());
							prod.setLifo(precioActual);
							if(prod.getFifo()==0.0)//Analizamos si hay un precio anterior

							{//Si no hay un precio anterior, los tres precios van a tomar el valor del costo de la fact de compra
								System.out.println("FIFo en 0:"+prod.getLifo()+"");
								prod.setPromedio(precioActual);
								prod.setFifo(precioActual);
							}
							else
							{//En caso de haber un precio anterior, se hara un promedio, multiplicando el num. de items que hay
								//if ((prod.getStock()+det.getCantidad())>0){
								if (prod.getStock()>0){
									System.out.println("FIFo con valor:"+prod.getLifo()+"");
									prod.setPromedio(CValidarDato.getDecimal(banderaNumeroDecimales,
											((CValidarDato.getDecimal(banderaNumeroDecimales,(prod.getStock()*prod.getPromedio()))+
													CValidarDato.getDecimal(banderaNumeroDecimales,(det.getCantidad()*precioActual.doubleValue())))
													/CValidarDato.getDecimal(banderaNumeroDecimales,(prod.getStock()+det.getCantidad())))));
								}else{
									System.out.println("Cantidad 0"+prod.getLifo()+"");
									prod.setPromedio(precioActual);
								}
							}

						}
						
						System.out.println("Buscar bodega");
						productos.add(prod);
						TblproductoTblbodega tblprodbod;
						try{
						tblprodbod= prodBodHome.loadBodega(prod.getIdProducto().intValue(),
								detalleDTO.getBodega().getIdBodega().intValue());
						}catch (Exception e){
							ProductoBodegaDTO pbdto=new ProductoBodegaDTO(detalleDTO.getBodega(),
									crearProductoDTO(prod),0.0);
							tblprodbod= new TblproductoTblbodega(pbdto);
							try {
								prodBodHome.grabar(new TblproductoTblbodega(pbdto));
							} catch (ErrorAlGrabar e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace(System.out);
								return "e: "+e1.getMessage();
							}
						}
						prodBods.add(tblprodbod);
						prodGuardados.add(prod.getIdProducto());
						Boolean afeccionInventario =false;

						System.out.println("TIPO TRANSACCION ----"+documento.getTipoTransaccion());
						if(documento.getTipoTransaccion() ==7 ||documento.getTipoTransaccion() ==12)
						{
							afeccionInventario = false;

							//Aqui debemos analizar si es una anulacion del gasto
							switch(anular){
							case -10:{//Anulacion 
								;break;}
							}
						}else{
							System.out.println("Entra a swi "+anular+" "+ind);
							switch(anular){
							case 0:{//No es una anulacion
								if(ind == 1 )//Se debe aumentar el stock
									cantProd = cantProd*-1;
								afeccionInventario = true;
								;break;}
							case -10:{//Anulacion sin devolucion
								cantProd =0.0;
								;break;}
							case -20:{//Anulacion con devolucion
								afeccionInventario = true;
								if(ind == 0 )//Se trata de un documento de venta, puesto q es una devolucion se deben sumar los items que se devuleven al inventario
									cantProd = cantProd*-1;
								;break;}
							}
						}
						System.out.println("CANTIDAD DE + O - INVENTARIO: "+cantProd);
						System.out.println("AQUI LA BODEGA LLEGA "+detalleDTO.getBodega().getIdBodega());

						clave=detalleDTO.getProducto().getIdProducto()+"";
						if(mapTbldtocomercialdetalle.containsKey(clave)){
							productoaux=mapTbldtocomercialdetalle.get(clave);
							productoaux.setStock(productoaux.getStock()-cantProd);
						}else{

							listtblproductoelaborado=tblproductoelaboradohome.findByQuery("where u.tblproductoByIdProductoPadre.idProducto='"+detalleDTO.getProducto().getIdProducto()+"'");
							if (listtblproductoelaborado!=null){
								for (Tblproductoelaborado tblproductoelaborado: listtblproductoelaborado ){
									//prod = accesoProd.findByNombre(tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto()+"","idProducto");
									if (prodGuardados.isEmpty() || !prodGuardados.contains(tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto().intValue())){
										prod = accesoProd.findByNombre(tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto().intValue()+"","idProducto");
										prodGuardados.add(tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto().intValue());
										stock = prod.getStock();
										//prodBod.load puede dar nulo error de indece cuando la consulta retorna nulo
										//Corregir cuando prodBod.getCantidad() = 0
										//Capturar la excepcion

										stock -= tblproductoelaborado.getCantidad()*cantProd;//det.getCantidad();
										System.out.println(detalleDTO.getProducto().getIdProducto());
										System.out.println(detalleDTO.getBodega().getIdBodega());
										/*
						prodBod = prodBodHome.load(detalleDTO.getProducto().getIdProducto()+"",
								detalleDTO.getBodega().getIdBodega()+"");//det.getIdProducto()+"",det.getIdBodega()+"");
										 */
										prodBod = prodBodHome.loadBodega(prod.getIdProducto().intValue(),
												detalleDTO.getBodega().getIdBodega().intValue());//det.getIdProducto()+"",det.getIdBodega()+"");
										stockB = prodBod.getCantidad();
										stockB -= tblproductoelaborado.getCantidad()*cantProd;//det.getCantidad();
										System.out.println("stock -= cantProd>> " + stock + "-= "+ cantProd);
										System.out.println("stockB -= cantProd>> " + stockB + "-= "+ cantProd);
										System.out.println("bodega >> " + prodBod.getTblbodega().getIdBodega());
										//Ahora vamos a modificar los registros del producto en la bodega correspondiente
										prod.setStock(stock);
										prodBod.setCantidad(stockB);
										productos.add(prod);
										prodBods.add(prodBod);
									}else{
										for(Tblproducto prodc:productos){
											if(prodc.getIdProducto().intValue()==tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto().intValue()){
												stock = prodc.getStock();
												stock -= tblproductoelaborado.getCantidad()*cantProd;//det.getCantidad();
												prodc.setStock(stock);
												break;
											}
										}
										for(TblproductoTblbodega prodcb:prodBods){
											if(prodcb.getTblproducto().getIdProducto().intValue()==tblproductoelaborado.getTblproductoByIdProductoHijo().getIdProducto().intValue()){
												stockB = prodcb.getCantidad();
												stockB -= tblproductoelaborado.getCantidad()*cantProd;//det.getCantidad();
												prodcb.setCantidad(stockB);
												break;
											}
										}
									}

								}
							}
							mapTbldtocomercialdetalle.put(clave, prodelaborado);

						}

						prod=accesoProd.findByNombre(String.valueOf(detalleDTO.getProducto().getIdProducto())+"","idProducto");
						if (prodGuardados.isEmpty() || !prodGuardados.contains(prod.getIdProducto().intValue())){
							prodGuardados.add(prod.getIdProducto());
							System.out.println("Mapa de productos > Clave producto: " + clave
									+", Cantidad: "+mapTbldtocomercialdetalle.get(clave).getStock());
							stock = prod.getStock();
							try{
								System.out.println("DATOS PRODUCTO-BODEGA 1: "+
										detalleDTO.getProducto().getIdProducto()+"- "+
										detalleDTO.getBodega().getIdBodega());
								prodBod = prodBodHome.loadBodega(detalleDTO.getProducto().getIdProducto(),detalleDTO.getBodega().getIdBodega());//det.getIdProducto()+"",det.getIdBodega()+"");
								System.out.println("DATOS PRODUCTO-BODEGA 2: "+prodBod.getCantidad()+" -"+
										prodBod.getId().getIdBodega()+"- "+
										prodBod.getId().getIdProducto());
							}catch(Exception e){
								System.out.println("XAVIER ZEAS ENTRO EN CATCH");
								TblproductoTblbodega tblproductotblbodega = new TblproductoTblbodega();
								Tblbodega tblbodega = new Tblbodega();
								Tblproducto tblproducto= new Tblproducto();
								tblbodega.setIdBodega(detalleDTO.getBodega().getIdBodega());
								tblproducto.setIdProducto(detalleDTO.getProducto().getIdProducto());
								TblproductoTblbodegaId tblproductotblbodegaid = new TblproductoTblbodegaId();
								tblproductotblbodegaid.setIdBodega(detalleDTO.getBodega().getIdBodega());
								tblproductotblbodegaid.setIdProducto(detalleDTO.getProducto().getIdProducto());
								tblproductotblbodega.setCantidad(0);
								tblproductotblbodega.setTblbodega(tblbodega);
								tblproductotblbodega.setTblproducto(tblproducto);
								tblproductotblbodega.setId(tblproductotblbodegaid);
								try {
									System.out.println("XAVIER ZEAS GRABO TblproductoTblbodega");
									prodBodHome.grabar(tblproductotblbodega);
									prodBod=tblproductotblbodega;
								} catch (ErrorAlGrabar e1) {
									// TODO Auto-generated catch block
									System.out.println("XAVIER ZEAS NO GRABO");
									e1.printStackTrace(System.out);
									return "e: "+e1.getMessage();
								}
							}
							if(afeccionInventario && String.valueOf(prod.getTipo()).equals("1"))
							{
								stock -= cantProd;//det.getCantidad();
								stockB = prodBod.getCantidad();
								stockB -= cantProd;//det.getCantidad();

							}
							//Ahora vamos a modificar los registros del producto en la bodega correspondiente


							prod.setStock(stock);
							prodBod.setCantidad(stockB);
							productos.add(prod);
							//System.out.println("AQUI LA BODEGA ES"+prodBod.getTblbodega().getNombre()+"  "+prodBod.getTblbodega().getIdBodega());
							prodBods.add(prodBod);
							System.out.println("Cantidades: "+String.valueOf(stock)+" - "+String.valueOf(stockB));
						}else{
							if(afeccionInventario && String.valueOf(prod.getTipo()).equals("1"))
							{
								for(Tblproducto prodc:productos){
									if(prodc.getIdProducto().intValue()==prod.getIdProducto().intValue()){
										stock = prodc.getStock();
										stock -= cantProd;//det.getCantidad();
										prodc.setStock(stock);
										break;
									}
								}
								for(TblproductoTblbodega prodcb:prodBods){
									System.out.println("Stock de bodegas");
									System.out.println(prodcb.getTblproducto().getIdProducto().intValue()+" = "+prod.getIdProducto().intValue());
									if(prodcb.getTblproducto().getIdProducto().intValue()==prod.getIdProducto().intValue()){
										stockB = prodcb.getCantidad();
										stockB -= cantProd;//det.getCantidad();
										prodcb.setCantidad(stockB);
										break;
									}
								}
							}
						}
					}
					detal+=1;
				}
				System.out.println("TOTAL DETALLES: " + detal + " -- "+ documento.getNumPagos());
				System.out.println("SIZE DE LAS BODEGAS "+prodBods.size());
				//A continuacion enlazamos todos los pa;gos al documento para que se graben
				//automaticamente en la base al momento de grabar el documento
			}//FIN DEL IF DE GASTO

			if(documentoDTO.getTblpagos()!=null){
				System.out.println("Si hay pagos: "+documentoDTO.getTblpagos().size());
				Set<TblpagoDTO> pagosDTO = documentoDTO.getTblpagos();
				iterDetalles = pagosDTO.iterator();
				//documento.setTblpagos(new HashSet());
				while (iterDetalles.hasNext()) {
					pagoDTO = (TblpagoDTO) iterDetalles.next();
					Tblpago pago = new Tblpago(pagoDTO);
					
					System.out.println("Hay pagos: "+pago.getFechaVencimiento()+", "+pago.getValor()+", "+pago.getFechaRealPago());
					pago.setTbldtocomercial(documento);
					if(anular<0)
						pago.setEstado('2');//Se eliminan tambien los pagos
					if (pago.getEstado()!='0'){
						System.out.println("Hay pagos concepto: "+pagoDTO.getConcepto()!=null);
						if (pagoDTO.getConcepto()!=null) pago.setConcepto(pagoDTO.getConcepto());
						System.out.println("Hay pagos forma pago: "+pagoDTO.getConcepto()!=null);
						if (pagoDTO.getFormaPago()!=null) pago.setFormaPago(pagoDTO.getFormaPago());
					}
					pago.setTbldtocomercial(documento);
					pagos.add(pago);
					//
					//documento.getTblpagos().add(pago);//.getTbldtocomercialdetalles().add(det);	
				}
			}	
			if(documentoDTO.getTbldtocomercialTbltipopagos()!=null){
				System.out.println("Si hay pagos2");

				Set<DtocomercialTbltipopagoDTO> tipospagosDTO = documentoDTO.getTbldtocomercialTbltipopagos();
				iterDetalles = tipospagosDTO.iterator();
				//documento.setTblpagos(new HashSet());
				Tbltipopago tipo = new Tbltipopago();
				TbltipopagoHome tpagoP = new TbltipopagoHome();
				while (iterDetalles.hasNext()) {

					tipopagoDTO = (DtocomercialTbltipopagoDTO) iterDetalles.next();

					TbldtocomercialTbltipopago pago = new TbldtocomercialTbltipopago(tipopagoDTO);
					pago.setTbldtocomercial(documento);
					tipo = tpagoP.load(tipopagoDTO.getTipoPago());
					pago.setTbltipopago(tipo);
					if(tipopagoDTO.getTipoPago()==4) documento.setIdAnticipo(pago.getReferenciaDto());
					System.out.println("TIPO DE PAGO SERVICESIMP: "+tipo.getIdTipoPago());
					pagosAsociados.add(pago);	
				}
			}		

			//documento.setTblretencions(retenciones);
			documento.setTblretencionsForIdFactura(retenciones);
			documento.setTbldtocomercialdetalles(detalles);
			documento.setTblpagos(pagos);
			//documento.setTbldtocomercialTbltipopagos(pagosAsociados);
			documento.setTbldtocomercialTbltipopagos(null);
			TbldtocomercialHome acceso = new TbldtocomercialHome();
			System.out.println("Documento: "+documento.getIdDtoComercial());
			System.out.println(documento.getEstado() == '1' || documento.getEstado() == '2');
			System.out.println("ESTADO DEL DOCUMENTO: " + documento.getEstado());
			int id=0;
			
			try {
				if(documento.getEstado() == '1' || documento.getEstado() == '2')
				{//DOCUMENTO CONFIRMADO
					documento.setIdDtoComercial(null);				
					System.out.println("Responnsable: "+documento.getObservacion()+" estado 1 o 2 "+documentoDTO.getIdDtoComercial());
					id=acceso.grabarConf(documento, documentoDTO.getIdDtoComercial(), productos, prodBods, pagosAsociados, ind, iva, anular, cuentaGastoIngreso);
					if (detalleMultImpDTO!=null && anular>=0) grabarDetalleMultImp(detalleMultImpDTO, id);
				}


				else if(ConfiguracionTipoFacturacion==0 && documento.getEstado() == '0'){
					System.out.println("Responnsable: "+documento.getObservacion()+" estado 0 configuraciontipofacturacion=0");
					//System.out.println("ENTRO A GRABAR Y CONFIRMAR: "+documento.getTipoTransaccion());
					/*
					System.out.println("EMPEZO DOC: ");
					System.out.println(documento.getIdDtoComercial());
					System.out.println(documento.getTblpersonaByIdPersona().getIdPersona());
					System.out.println(documento.getTblpersonaByIdVendedor().getIdPersona());
				    System.out.println(documento.getNumRealTransaccion());
				    System.out.println(documento.getTipoTransaccion());
				    System.out.println(documento.getFecha());
				    System.out.println(documento.getExpiracion());
				    System.out.println(documento.getAutorizacion());
				    System.out.println(documento.getNumPagos());
				    System.out.println(documento.getEstado());
				    System.out.println(documento.getSubtotal());
				    System.out.println(documento.getCosto());
				    System.out.println(documento.getNumCompra());
				    System.out.println(documento.getDescuento());
				    System.out.println(documento.getObservacion());
				    System.out.println(documento.getTbldtocomercialdetalles().size());
					 */
					//System.out.println("retenciones: "+documento.getTblretencions().size());
					//System.out.println("movimientos: "+documento.getTblmovimientos().size());
					//System.out.println("pagos: "+documento.getTblpagos().size());
					//System.out.println("FINALIZO DOC: ");
					if(documento.getEstado() == '0' && documento.getTipoTransaccion()==0)
					{
						System.out.println("Estado = 0, tipotransaccion=0");
						/*
						Tbldtocomercial tbldtocomercial = new Tbldtocomercial();
						acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);
						tbldtocomercial=acceso.getLast("", "idDtoComercial");		
						Set<Tbldtocomercialdetalle> tbldtocomercialdetalles = new HashSet<Tbldtocomercialdetalle>(0);
						Tbldtocomercial tbldtocomercialrecorrer = new Tbldtocomercial();
						tbldtocomercialrecorrer.setIdDtoComercial(tbldtocomercial.getIdDtoComercial());
						Tbldtocomercialdetalle tbldtocomercialdetalle = new Tbldtocomercialdetalle();
							for(Tbldtocomercialdetalle tbldtocomercialdetallerecorer:documento.getTbldtocomercialdetalles()){
								tbldtocomercialdetalle=tbldtocomercialdetallerecorer;
								tbldtocomercialdetalle.setTbldtocomercial(tbldtocomercialrecorrer);
								tbldtocomercialdetalles.add(tbldtocomercialdetalle);
							}
						documento.setTbldtocomercialdetalles(tbldtocomercialdetalles);
						acceso.grabarConf(documento, tbldtocomercial.getIdDtoComercial(), productos, prodBods, pagosAsociados, ind, iva, anular, cuentaGastoIngreso);
						 */
						id=acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);	
						documento.setIdDtoComercial(id);
						if (detalleMultImpDTO!=null) grabarDetalleMultImp(detalleMultImpDTO, id);
						grabarMovimiento(documento,ind,anular, iva, planCuentaID);
					}
					else if(documento.getEstado() == '0' && documento.getTipoTransaccion()==2)
					{
						System.out.println("Estado = 0, tipotransaccion=2");
						id=acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);
						documento.setIdDtoComercial(id);
						if (detalleMultImpDTO!=null) grabarDetalleMultImp(detalleMultImpDTO, id);
						grabarMovimiento(documento,ind,anular, iva, planCuentaID);
					}
					else if(documento.getEstado() == '0' && documento.getTipoTransaccion()==26)
					{
						System.out.println("Estado = 0, tipotransaccion=26");
						id=acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);
						documento.setIdDtoComercial(id);
						if (detalleMultImpDTO!=null) grabarDetalleMultImp(detalleMultImpDTO, id);
						grabarMovimiento(documento,ind,anular, iva, planCuentaID);
					}/*else if(documento.getEstado() == '0' && documento.getTipoTransaccion()==3)
					{
						id=acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);
						documento.setIdDtoComercial(id);
						grabarMovimiento(documento,ind,anular, iva, planCuentaID);
					}*/
					else{
						System.out.println("Else otro");
						//acceso.grabaryConfirmar(documento,pagosAsociados,iva,productos, prodBods);
						id=acceso.grabarP(documento,pagosAsociados,iva);
						if (detalleMultImpDTO!=null) grabarDetalleMultImp(detalleMultImpDTO, id);
					}
				}

				else	
				{//DOCUMENTO POR CONFIRMAR
					System.out.println("ESTADO a grabarse "+pagosAsociados);
					id=acceso.grabarP(documento,pagosAsociados,iva);
					if (detalleMultImpDTO!=null) grabarDetalleMultImp(detalleMultImpDTO, id);
					System.out.println("Se supone q grabo "+pagosAsociados);
				}

				TbldtocomercialHome tbldtocomercialhome = new TbldtocomercialHome();

				Tbldtocomercial tbldtocomercial = tbldtocomercialhome.findById(id);
				
				System.out.println("Grabado con exito-"+id+"-"+tbldtocomercial.getNumRealTransaccion());
				mensaje+="Grabado con exito-"+id+"-"+tbldtocomercial.getNumRealTransaccion();
			} catch (ErrorAlGrabar e) {
				System.out.println("ERROR "+e.getMessage());
				
				e.printStackTrace(System.out);
				System.out.println("ERROR "+e.getLocalizedMessage());
//				System.out.println("ERROR "+e.getCause().getMessage());
//				e.getCause().printStackTrace(System.out);
				return "e: "+e.getMessage();
				
			}
			/*}catch(Exception e){
			mensaje+="   ----JOHNY Error: "+e+" ------   ";
		}*/
			//Ahora tengo que enlazar los pagos, retenciones, movimientos contables y detalles al dtocomercial
		}
		System.out.println(mensaje);
		return mensaje;

	}
	
	public String grabarDtomovimiento(DtocomercialDTO documentoDTO, List<MovimientoCuentaPlanCuentaDTO> MovimientoCuentaPlanCuenta){
		String resultado="";
		Integer idDtocomercial=0;
		Integer idMovimiento=0;
		TblpersonaHome accesotblpersonahome = new TblpersonaHome();
		TbldtocomercialHome accesotbldtocomercialhome = new TbldtocomercialHome();
		TblmovimientoHome accesotblmovimientohome = new TblmovimientoHome();
		Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
		TblmovimientoTblcuentaTblplancuentaHome tblmovimientotblcuentatblplancuentahome = new TblmovimientoTblcuentaTblplancuentaHome();
		TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
		
		TblcuentaPlancuenta cuentaPlan = new TblcuentaPlancuenta();
		Tbldtocomercial tbldtocomercial= new Tbldtocomercial();
		Tblmovimiento tblmovimiento = new Tblmovimiento();
		TblmovimientoTblcuentaTblplancuenta tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta();
		TblmovimientoTblcuentaTblplancuentaId tblmovimientotblcuentatblplancuentaid = new TblmovimientoTblcuentaTblplancuentaId();
		List<TblcuentaPlancuenta> lstcuentaPlan ;
		System.out.println("ENTRO A ALMACENAR MOVIMIENTO");		
		System.out.println("ID VENDEDOR: "+documentoDTO.getTblpersonaByIdVendedor().getIdPersona());
		Tblpersona vendedor = accesotblpersonahome.load(documentoDTO.getTblpersonaByIdVendedor().getIdPersona());	
		List<Tbldtocomercial> listtbldtocomercial= new ArrayList<Tbldtocomercial>(); 
		tbldtocomercial = new Tbldtocomercial(documentoDTO, vendedor, vendedor, vendedor);
		try {
			idDtocomercial=(Integer) accesotbldtocomercialhome.grabaryretornar(tbldtocomercial);
			System.out.println("IDDTOCOMERCIAL: "+idDtocomercial);
			tbldtocomercial.setIdDtoComercial(idDtocomercial);
			tblmovimiento = new Tblmovimiento(tbldtocomercial, tbldtocomercial.getAutorizacion(), tbldtocomercial.getFecha());
			idMovimiento=(Integer) accesotblmovimientohome.grabaryretornar(tblmovimiento);
			System.out.println("IDMOVIMIENTO: "+idMovimiento);
			tblmovimiento.setIdMovimiento(idMovimiento);
			
				for(MovimientoCuentaPlanCuentaDTO movimientocuentaplancuentaDTO :MovimientoCuentaPlanCuenta){
					lstcuentaPlan = tblcuentaplancuentahome.findByQuery( "where "  +
							"u.tblplancuenta.idPlan = " +"'"+1+"' and u.tblcuenta.idCuenta='"+movimientocuentaplancuentaDTO.getTblcuentaPlancuenta().getTblcuenta().getIdCuenta()+"'");//BUSCAMOS EL PLAN DE CUENTA

					cuentaPlan=lstcuentaPlan.get(0);
					
					tblmovimientotblcuentatblplancuentaid =new TblmovimientoTblcuentaTblplancuentaId(idMovimiento, 1, movimientocuentaplancuentaDTO.getTblcuentaPlancuenta().getTblcuenta().getIdCuenta());
					
					tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta(tblmovimientotblcuentatblplancuentaid,cuentaPlan,tblmovimiento, movimientocuentaplancuentaDTO.getValor(), movimientocuentaplancuentaDTO.getSigno());
					detallesMovimiento.add(tblmovimientotblcuentatblplancuenta);
				}
				tblmovimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
				System.out.println("TAMAÑO MOVIMIENTO: "+detallesMovimiento.size());
				System.out.println("ID DTO MOVIMIENTO: "+tblmovimiento.getTbldtocomercial().getIdDtoComercial());
				accesotblmovimientohome.modificar(tblmovimiento);
		} catch (ErrorAlGrabar e) {
			e.printStackTrace();
			return e.getMessage();
		}
		
		
		
		return resultado;
	}
	
	public void grabarMovimiento(Tbldtocomercial dto,Integer ind, Integer anular, double iva, String planCuentaID){
 		System.out.println("ENTRO A GRABAR MOVIMIENTO");
 		try {		
			Integer planCuenta = Integer.valueOf(planCuentaID);//ESTE VALOR DEBE SER EXTRAIDO DE LA VARIABLE DE SESION
			String concepto="";
			String anulacion="";
			List<TbldtocomercialTbltipopago> listtbldtocomercialtbltipopago;
	
			if(anular<0){
				anulacion = " anulacion de ";
			}
			//---------------------------	VAMOS A GRABAR LA CABECERA DE MOVIMIENTO ----------------------------
			switch(dto.getTipoTransaccion()){
				case 0:{concepto="Por concepto de"+anulacion+" una venta con Factura ";break;}
				case 1:{concepto="Por concepto de"+anulacion+" una compra con Factura";break;}
				case 2:{concepto="Por concepto de"+anulacion+" una nota de entrega";break;}
				case 3:{concepto="Por concepto de"+anulacion+" una nota de credito";break;}
				case 4:{concepto="Por concepto de"+anulacion+" un anticipo";break;}
				case 5:{concepto="Por concepto de"+anulacion+" una retencion";break;}
				case 6:{concepto="Por concepto de"+anulacion+" un ajuste de inventario";break;}
				case 7:{concepto="Por concepto de"+anulacion+" un gasto";break;}
				case 12:{concepto="Por concepto de"+anulacion+" un ingreso";break;}
				case 13:{concepto="Por concepto de"+anulacion+" una venta con Factura Grande";break;}
			}
			//-------------------------------------------------------------------------------
			//1. CREAMOS EL MOVIMIENTO
			Tblmovimiento movimiento;
			movimiento = new Tblmovimiento(dto, concepto,dto.getFecha());
			TblmovimientoHome tblmovimientohome = new TblmovimientoHome();
			Integer idMov = tblmovimientohome.grabaryretornar(movimiento);
			
			String tipoIva = "IVA COBRADO";
			char signo='0';
			if(ind == 0 ){//ENTRAN LAS CUENTAS AL HABER
				signo = 'h';
				if(anular<0)
					signo='d';
			}else{//ENTRA A LAS CUENTAS DEL DEBE
				signo = 'd';
				if(anular<0)
					signo = 'h';
				tipoIva = "IVA PAGADO";
			}
			
	
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoHaber = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoDebe = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			TblmovimientoTblcuentaTblplancuentaId tblmovimientotblcuentatblplancuentaid;
			TblmovimientoTblcuentaTblplancuenta tblmovimientotblcuentatblplancuenta;
			TblcuentaPlancuenta cuentaPlan;
			List<TblcuentaPlancuenta> lstcuentaPlan ;
			Tbltipodocumento tipoDoc;
			List<Tbltipodocumento> lsttipoDoc;
			TbltipodocumentoHome tbltipodocumentohome = new TbltipodocumentoHome();
			TblcuentaPlancuentaHome tblcuentaplancuentahome = new TblcuentaPlancuentaHome();
			TbltipopagoHome  tbltipopagohome=new TbltipopagoHome();
			Tbltipopago tbltipopago=new Tbltipopago();
			TbldtocomercialTbltipopagoHome  tbldtocomercialtbltipopagohome=new TbldtocomercialTbltipopagoHome(); 
			
			
			System.out.println("TIPO TRANS: "+dto.getTipoTransaccion()); 
			//-------------------------------------------------------------------------------
			//2. AGREGAMOS EL MOVIMIENTO DEL DOCUMENTO RELACIONADO CON LA CUENTA 
			lsttipoDoc =  tbltipodocumentohome.findByQuery("where u.idTipo ='"+dto.getTipoTransaccion()+"'");//BUSCAMOS EL TIPO DE DOCUMENTO
			tipoDoc= lsttipoDoc.get(0);
			
			Integer IdCuentaAsiento = tipoDoc.getIdCuenta();//OBTENEMOS EL ID DE LA CUENTA
			
			tblmovimientotblcuentatblplancuentaid =new TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); //INICIALIZAMOS tblmovimientotblcuentatblplancuentaid	
			
			lstcuentaPlan = tblcuentaplancuentahome.findByQuery( "where "  +
					"u.tblplancuenta.idPlan = " +"'"+planCuenta.toString()+"' and u.tblcuenta.idCuenta='"+IdCuentaAsiento+"'");//BUSCAMOS EL PLAN DE CUENTA
			cuentaPlan = lstcuentaPlan.get(0);
			
			tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta(tblmovimientotblcuentatblplancuentaid,cuentaPlan,movimiento, dto.getSubtotal(), signo);//INICIALIAMOS tblmovimientotblcuentatblplancuenta
				if(signo=='h'){
					detallesMovimientoHaber.add(tblmovimientotblcuentatblplancuenta);
				}else{
					detallesMovimientoDebe.add(tblmovimientotblcuentatblplancuenta);
				}
			//-------------------------------------------------------------------------------
			//3. AGREGAMOS EL MOVIMIENTO DEL IVA RELACIONADO CON LA CUENTA
			lsttipoDoc =  tbltipodocumentohome.findByQuery("where u.tipoDoc ='"+tipoIva+"'");//BUSCAMOS EL TIPO DE DOCUMENTO
			tipoDoc= lsttipoDoc.get(0);
			
			lstcuentaPlan = tblcuentaplancuentahome.findByQuery( "where "  +
					"u.tblplancuenta.idPlan = " +"'"+planCuenta.toString()+"' and u.tblcuenta.idCuenta='"+IdCuentaAsiento+"'");//BUSCAMOS EL PLAN DE CUENTA
			cuentaPlan = lstcuentaPlan.get(0);
			
			tblmovimientotblcuentatblplancuentaid =new TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tipoDoc.getIdCuenta());
			
			tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta(tblmovimientotblcuentatblplancuentaid,cuentaPlan,movimiento, iva, signo);
				if(signo=='h'){
					detallesMovimientoHaber.add(tblmovimientotblcuentatblplancuenta);
				}else{
					detallesMovimientoDebe.add(tblmovimientotblcuentatblplancuenta);
				}
				//-------------------------------------------------------------------------------
				//--INICIO SELECCIONAMOS EL CONTRA ASIENTO	
					if(signo == 'h'){
						signo = 'd';
					}else if(signo == 'd'){
						signo = 'h';
					}
				//-------------------------------------------------------------------------------
				//--FIN SELECCIONAR EL CONTRA ASIENTO
			//-------------------------------------------------------------------------------
			//4. ANALISIMOS EL MOVIIENTO EN CASO DE PAGOS
			Integer tipoPago=0;
			listtbldtocomercialtbltipopago=tbldtocomercialtbltipopagohome.findByQuery(" where u.tbldtocomercial.idDtoComercial='"+dto.getIdDtoComercial()+"'");
			for (TbldtocomercialTbltipopago tbldtocomercialtbltipopago: listtbldtocomercialtbltipopago) {
				tbltipopago= tbltipopagohome.findById(tbldtocomercialtbltipopago.getTbltipopago().getIdTipoPago());
				tbldtocomercialtbltipopago.setTbltipopago(tbltipopago);
				if(tbldtocomercialtbltipopago.getTipo()=='1'){//SE TRATA DE UNA COMPRA O INGRESO
					switch(tipoPago)
					{
					case 2 :{tipoPago=8;break;}
					case 4 :{tipoPago=9;break;}
					case 5 :{tipoPago=10;break;}
					}
				}
				
				lstcuentaPlan = tblcuentaplancuentahome.findByQuery( "where "  +
						"u.tblplancuenta.idPlan = " +"'"+planCuenta.toString()+"' and u.tblcuenta.idCuenta='"+tbltipopago.getIdCuenta()+"'");//BUSCAMOS EL PLAN DE CUENTA
				cuentaPlan = lstcuentaPlan.get(0);
				
				tblmovimientotblcuentatblplancuentaid = new TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tbltipopago.getIdCuenta());
				tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta(tblmovimientotblcuentatblplancuentaid,cuentaPlan,movimiento, tbldtocomercialtbltipopago.getCantidad(), signo);
				if(signo=='h'){
					detallesMovimientoHaber.add(tblmovimientotblcuentatblplancuenta);
				}else{
					detallesMovimientoDebe.add(tblmovimientotblcuentatblplancuenta);
				}				
			}
			//-------------------------------------------------------------------------------
			//5. ANALISAMOS MOVIMIENTOS EN CASO DE RETENCIONES
			if (dto.getTblretencionsForIdFactura() != null){
				Iterator iterRetenciones = dto.getTblretencionsForIdFactura().iterator();
				while(iterRetenciones.hasNext()){
					Tblretencion retencion = (Tblretencion) iterRetenciones.next();
					//cuentaPlan = tblcuentaplancuentahome.findByIdPlanIdCuenta2(retencion.getTblimpuesto().getIdPlan()+"",retencion.getTblimpuesto().getIdCuenta()+"");
					lstcuentaPlan = tblcuentaplancuentahome.findByQuery( "where "  +
							"u.tblplancuenta.idPlan = " +"'"+retencion.getTblimpuesto().getIdPlan()+"' and u.tblcuenta.idCuenta='"+retencion.getTblimpuesto().getIdCuenta()+"'");//BUSCAMOS EL PLAN DE CUENTA
					cuentaPlan = lstcuentaPlan.get(0);
					tblmovimientotblcuentatblplancuentaid =new TblmovimientoTblcuentaTblplancuentaId(idMov, retencion.getTblimpuesto().getIdPlan(), retencion.getTblimpuesto().getIdCuenta());
					Double cantidad = 0.0;
					double porcentaje = retencion.getTblimpuesto().getRetencion();
					double base = retencion.getBaseImponible();
					cantidad = (base*porcentaje)/100;
					tblmovimientotblcuentatblplancuenta = new TblmovimientoTblcuentaTblplancuenta(tblmovimientotblcuentatblplancuentaid,cuentaPlan,movimiento, cantidad, signo);
					if(signo=='h'){	
						detallesMovimientoHaber.add(tblmovimientotblcuentatblplancuenta);
					}else{	
						detallesMovimientoDebe.add(tblmovimientotblcuentatblplancuenta);
					}
				}
			}
			//-------------------------------------------------------------------------------
			//6. UNIFICAMOS LOS ASIENTOS			
			Iterator detalles = detallesMovimientoHaber.iterator();
			detallesMovimiento = detallesMovimientoDebe;
				for (int k=0; k<detallesMovimientoHaber.size();k++){
					detallesMovimiento.add((TblmovimientoTblcuentaTblplancuenta) detalles.next());
				}
			//   Para arreglar lo de los impuestos Edu
			LinkedList<TblmovimientoTblcuentaTblplancuenta> listDetallesAuxMovimiento = new LinkedList<TblmovimientoTblcuentaTblplancuenta>();
			//   Para arreglar lo de los impuestos Edu
			LinkedList<TblmovimientoTblcuentaTblplancuenta> listDetallesAuxMovimientoTotal = new LinkedList<TblmovimientoTblcuentaTblplancuenta>();
			Iterator detallesTotales = detallesMovimiento.iterator();
			while (detallesTotales.hasNext())
			{
				TblmovimientoTblcuentaTblplancuenta aux = (TblmovimientoTblcuentaTblplancuenta)detallesTotales.next();
				//system.out.println("Inicial Mov "+aux.getId().getIdMovimiento()+" Plan "+aux.getId().getIdPlan()+" Cuenta "+aux.getId().getIdCuenta()+" Signo "+aux.getSigno()+" Valor "+aux.getValor());
				listDetallesAuxMovimiento.add(aux);
			}	
			
			//+++++++   Pasamos a una lista ligada auxiliar todos los elementos   ++++++++++++++
			int repetido=0;
			
			for (int a=0; a<listDetallesAuxMovimiento.size()-1;a++)
			{
					Double valorRetenidoTotal=0.0;
					valorRetenidoTotal=listDetallesAuxMovimiento.get(a).getValor();
					for (int b=a+1; b<listDetallesAuxMovimiento.size();b++){	
						if(listDetallesAuxMovimiento.get(a).getId().equals(listDetallesAuxMovimiento.get(b).getId()))
						{
							valorRetenidoTotal=valorRetenidoTotal+listDetallesAuxMovimiento.get(b).getValor();
						//	listAuxRepetidos.add(b);
						}
					}
					
					listDetallesAuxMovimiento.get(a).setValor(valorRetenidoTotal);
					if(listDetallesAuxMovimientoTotal.size()==0){
						listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(a));
					}
					else
					{
						repetido=0;
						for(int d=0;d<listDetallesAuxMovimientoTotal.size();d++){	
							if(listDetallesAuxMovimientoTotal.get(d).getId().equals(listDetallesAuxMovimiento.get(a).getId())){
								repetido=1;
								break;
							}
						}
						if (repetido==0){
							listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(a));
						}	
					}	
					//borramos todos los elementos que estan repetidos incluyendo la cabecera
				}
			repetido=0;
			for(int d=0;d<listDetallesAuxMovimientoTotal.size();d++)
			{	
				if(listDetallesAuxMovimientoTotal.get(d).getId().equals(listDetallesAuxMovimiento.get(listDetallesAuxMovimiento.size()-1).getId())){
					repetido=1;
					break;
					
				}
			}
			if (repetido==0)
			{
				listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(listDetallesAuxMovimiento.size()-1));
			}
			
			detallesMovimiento.clear();
			for (int k=0; k<listDetallesAuxMovimientoTotal.size();k++)
			{
				detallesMovimiento.add(listDetallesAuxMovimientoTotal.get(k));
			}
			
			
			movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
			//system.out.println("TAMAÑO MOVIMIENTO: "+detallesMovimiento.size());
			//system.out.println("ID DTO MOVIMIENTO: "+movimiento.getTbldtocomercial().getIdDtoComercial());
			tblmovimientohome.modificar(movimiento);
			//AQUI VAMOS A PROCEDER A GRABAR LOS ASIENTOS CONTABLES 
			//system.out.println("SE GRABO LOS MOVIMIENTOS: ");
			//session.flush();
			//session.clear();
	 		//session.getTransaction().commit();
	 		//session.close();			
		
 		} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	///FUNCION PARA ELIMINAR EL DOCUMENTO QUE NO HA SIDO CONFIRMADO
	public String eliminarDocSinConfirmar(DtocomercialDTO documentoDTO){
		String mensaje = "";
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		if(documentoDTO.getEstado() != '1' && documentoDTO.getEstado() != '2')
		{//SI NO ESTA CONFIRMADO O ELIMINADO LOGICAMENTE SE PUEDE ELIMINAR
			try{
				
				//Extraigo cliente y vendedor
				TblpersonaHome accesoP = new TblpersonaHome();
				System.out.println("En eliminar documento idpersona: "+
						documentoDTO.getTblpersonaByIdPersona().getIdPersona());
			    //Long id = (long) documentoDTO.getTblpersonaByIdPersona().getIdPersona();
				Tblpersona cliente = accesoP.load(documentoDTO.getTblpersonaByIdPersona().getIdPersona());
				System.out.println("En eliminar doc cliente ");
				//+cliente.getCedulaRuc()+" "+cliente.getRazonSocial());
				//id = (long) documentoDTO.getTblpersonaByIdVendedor().getIdPersona();//.getTblpersonaByIdPersona().getIdPersona();
				Tblpersona vendedor = accesoP.load(documentoDTO.getTblpersonaByIdVendedor().getIdPersona());
				System.out.println("En eliminar doc vendedor ");
				//+vendedor.getCedulaRuc()+" "+vendedor.getRazonSocial());
				
				Tblpersona facturaPor = accesoP.load(documentoDTO.getTblpersonaByIdFacturaPor().getIdPersona());
				System.out.println("En eliminar doc facturaPor ");
				//+facturaPor.getCedulaRuc()+" "+facturaPor.getRazonSocial());
						
				Tbldtocomercial documento = new Tbldtocomercial(documentoDTO, cliente, vendedor, facturaPor);
				mensaje = acceso.eliminarDocumento(documento);
			}catch(Exception e)
			{
				e.printStackTrace(System.out);
				mensaje="Error al acceder al documento";
				System.out.println(e.getMessage());
			}
		}
		
		return mensaje;
	
	}
	
	
	
	public Date ConvertirFecha(String fecha){
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("MM/dd/yyyy");
		Date fechaaux = null;
	    try{
	    	fechaaux = formatoDelTexto.parse(fecha);
	    }catch(Exception e)
	    {
	    	fechaaux = new Date();
	    	}
	    return fechaaux;
	}
	
	//Funcion para listar los documentos comerciales segun la fecha
	@Override
	public List<DtocomercialDTO> listarDocFecha(String fecha) {
		List<DtocomercialDTO> docs = null;
		List<Tbldtocomercial> listado = null;
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		try {Integer id=0;
			listado=acceso.CuadreCaja(fecha, fecha);
			docs = new ArrayList<DtocomercialDTO>();
            if (listado != null) {
                for (Tbldtocomercial doc : listado) {
                	if(doc.getTbldtocomercialTbltipopagos().size()>0)
                		{docs.add(crearDtocomercialDTO(doc));
                		System.out.println("Dto: "+id);
                		id++;
                		}
                	if(doc.getTipoTransaccion()==5)
                	{
                		System.out.println("Entre al tipo de transaccion = 5");
                		docs.add(crearDtocomercialDTO(doc));
                	}	
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return docs;
	}
	
	
	
	//Funcion para listar los documentos comerciales segun el tipo
	@Override
	public List<DtocomercialDTO> listarDocTipo(Integer tipo) {
		List<DtocomercialDTO> docs = null;
		List<Tbldtocomercial> listado = null;
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		try {
			listado=acceso.DocumentoTipo(tipo);
			docs = new ArrayList<DtocomercialDTO>();
            if (listado != null) {
                for (Tbldtocomercial doc : listado) {
                    docs.add(crearDtocomercialDTO(doc));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return docs;
	}
	
	public List<DtocomercialDTO> listarDocTipoPersona(Integer tipo, Integer persona) {
		List<DtocomercialDTO> docs = null;
		List<Tbldtocomercial> listado = null;
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		try {
			listado=acceso.DocumentoTipoPersona(tipo,persona);
			docs = new ArrayList<DtocomercialDTO>();
            if (listado != null) {
                for (Tbldtocomercial doc : listado) {
                    docs.add(crearDtocomercialDTO(doc));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		return docs;
	}
	//METODOS REPORTES PAGOS
	public TblpagoDTO BuscarPago(String idPago) {
		TblpagoHome acceso = new TblpagoHome();
		Tblpago tblpago= new Tblpago();
		TblpagoDTO pago= new TblpagoDTO();
		pago=null;
		String mensaje = "";
		try {
			try {
				System.out.println("idPago="+idPago);
				tblpago=acceso.findById(Integer.valueOf(idPago));
				System.out.println("dia="+tblpago.getValor());
				pago=new TblpagoDTO();
				pago.setIdEmpresa(tblpago.getIdEmpresa());
				pago.setEstablecimiento(tblpago.getEstablecimiento());
				pago.setIdPago(tblpago.getIdPago());
				pago.setEstado(tblpago.getEstado());
				pago.setFechaRealPago(tblpago.getFechaRealPago());
				pago.setFechaVencimiento(tblpago.getFechaVencimiento());
				DtocomercialDTO doc=new DtocomercialDTO();
				doc.setIdEmpresa(tblpago.getTbldtocomercial().getIdEmpresa());
				doc.setEstablecimiento(tblpago.getTbldtocomercial().getEstablecimiento());
				doc.setPuntoEmision(tblpago.getTbldtocomercial().getPuntoEmision());
				doc.setIdDtoComercial(tblpago.getTbldtocomercial().getIdDtoComercial());
				pago.setTbldtocomercial(doc);
				pago.setValor(tblpago.getValor());
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
		} catch (RuntimeException re) {
			System.out.println(re.getMessage());
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return pago;
	}
	public String ReprotePagosVencidosCliente(String idCliente){
		String mensaje="";
		try{
			TblpagoHome acceso = new TblpagoHome();
			double totalVencido=0.0;
			totalVencido=acceso.ReprotePagosVencidosCliente(idCliente);
			totalVencido = getDecimal(2,totalVencido);
			if (totalVencido>0)
			{	
				mensaje=" con un total de "+totalVencido;
			}	
			else
				{mensaje="";}
			System.out.println(mensaje);
			return mensaje;
		}catch(Exception e){
			return mensaje;
		}
	}
	
	public List<TblpagoDTO> listarPagosVencidos(){
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReprotePagosVencidos();
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO(); 
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	@Override
	public List<TblpagoDTO> listarPagos(String FechaI, String FechaF,
			String estado, String Tipo,String idfactura,String idCliente,int tipoConsulta) {
		
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReportePagos(FechaI,FechaF,estado, Tipo,idfactura,idCliente,tipoConsulta);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	
	public TblpagoDTO buscarPagoId(int idPago){
		TblpagoDTO pagoD=new TblpagoDTO();
		try{
			TblpagoHome acceso = new TblpagoHome();
			Iterator resultado = acceso.BuscarPagoID(idPago);
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblpago pago = (Tblpago) pair[0];
				Tbldtocomercial doc = (Tbldtocomercial) pair[1];
				Tblpersona persona=(Tblpersona) pair[2];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				DtocomercialDTO docDTO =new DtocomercialDTO();
				docDTO=this.crearDtocomercialDTO(doc);
				docDTO.setTblpersonaByIdPersona(personaD);
				docDTO.setTblpersonaByIdVendedor(personaD);
				pagoD.setIdEmpresa(pago.getIdEmpresa());
				pagoD.setEstablecimiento(pago.getEstablecimiento());
				pagoD.setNumEgreso(pago.getNumEgreso());
				pagoD.setConcepto(pago.getConcepto());
				pagoD.setFormaPago(pago.getFormaPago());
				pagoD.setIdPago(pago.getIdPago());
				pagoD.setEstado(pago.getEstado());
				pagoD.setFechaRealPago(pago.getFechaRealPago());
				pagoD.setFechaVencimiento(pago.getFechaVencimiento());
				pagoD.setTbldtocomercial(docDTO);
				pagoD.setValor(pago.getValor());
			}
		}catch(Exception e){
			System.out.println("Error buscarpagoID "+e.getMessage());
		}
		return pagoD;
	}
	
	
	
	// listar todos los pagos que tengan el mismo idDtcoComercial
	
	public TblpagoDTO buscarPagosIdComercial(int idDtoComercial){
		TblpagoDTO pagoD=new TblpagoDTO();
		try{
			TblpagoHome acceso = new TblpagoHome();
			Iterator resultado = acceso.BuscarPagoIdComercial(idDtoComercial);
			while ( resultado.hasNext() ) {
				Object[] pair = (Object[])resultado.next();
				Tblpago pago = (Tblpago) pair[0];
				Tbldtocomercial doc = (Tbldtocomercial) pair[1];
				Tblpersona persona=(Tblpersona) pair[2];
				PersonaDTO personaD = new PersonaDTO();
				personaD = crearPersonaDTO(persona);
				personaD.setIdPersona(persona.getIdPersona());
				DtocomercialDTO docDTO =new DtocomercialDTO();
				docDTO=this.crearDtocomercialDTO(doc);
				docDTO.setTblpersonaByIdPersona(personaD);
				docDTO.setTblpersonaByIdVendedor(personaD);
				pagoD.setIdEmpresa(pago.getIdEmpresa());
				pagoD.setEstablecimiento(pago.getEstablecimiento());
				pagoD.setNumEgreso(pago.getNumEgreso());
				pagoD.setConcepto(pago.getConcepto());
				pagoD.setFormaPago(pago.getFormaPago());
				pagoD.setIdPago(pago.getIdPago());
				pagoD.setEstado(pago.getEstado());
				pagoD.setFechaRealPago(pago.getFechaRealPago());
				pagoD.setFechaVencimiento(pago.getFechaVencimiento());
				pagoD.setTbldtocomercial(docDTO);
				pagoD.setValor(pago.getValor());
			}
		}catch(Exception e){
			System.out.println("Error buscarpagoID "+e.getMessage());
		}
		return pagoD;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//++++++++++++++++++++++   PARA MOSTRAR LOS PAGOS DEL FRMPAGOS2   ++++++++++++++++++++
	public List<TblpagoDTO> listarPagos2(String FechaI, String FechaF,
			String estado, String Tipo,String numRealTransaccion,String idCliente,int tipoConsulta) {
		
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReportePagos2(FechaI,FechaF,estado, Tipo,numRealTransaccion,idCliente,tipoConsulta);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
//			docDTO.setTblpersonaByIdPersona(personaD);
//			docDTO.setTblpersonaByIdVendedor(personaD);
			System.out.println("Retenciones cantidad: "+docDTO.getTblretencionsForIdFactura().size());
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	public List<TblpagoDTO> listarPagosFechaPago(String FechaI, String FechaF,
			String estado, String Tipo,String numRealTransaccion,String idCliente,int tipoConsulta) {
		
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReportePagosFechaPago(FechaI,FechaF,estado, Tipo,numRealTransaccion,idCliente,tipoConsulta);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	public int ultimoEgreso(){
		int num=0;
		TblpagoHome acceso = new TblpagoHome();
		num=acceso.ultimoEgreso();
		return num;
	}
	public int ultimoIngreso(){
		int num=0;
		TblpagoHome acceso = new TblpagoHome();
		num=acceso.ultimoIngreso();
		return num;
	}
	public int ultimoProducto(){
		int num=0;
		TblproductoHome acceso = new TblproductoHome();
		num=acceso.ultimoProducto();
		return num;
	}
	public List<TblpagoDTO> listarPagosEgreso(String Tipo,String num) {
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReportePagosEgreso(Tipo, num);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	
	public List<TblpagoDTO> listarPagosFacRealCompra(String FechaI, String FechaF,
			String estado, String Tipo,String idfactura,String idCliente, String numRealFactura) {
		
		List<TblpagoDTO> list = new ArrayList<TblpagoDTO>();
		
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.ReportePagosFacRealCompra(FechaI,FechaF,estado, Tipo,idfactura,idCliente,numRealFactura);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			TblpagoDTO pagoD=new TblpagoDTO();
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
			list.add(pagoD);
		}
		return list;
	}
	
	public String grabarPago(TblpagoDTO entidad) throws IllegalArgumentException {
		TblpagoHome acces = new TblpagoHome();
		Tblpago tblpago= new Tblpago();
		String mensaje = "";
		int ban=0;
		try {
			acces.grabar(new Tblpago(entidad));
			mensaje="Ingreso realizado con \u00e9xito";
		} catch (ErrorAlGrabar e) { 
		} catch (RuntimeException re) {
			mensaje="No se pudo grabar el dato";
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	public String grabarEgreso(LinkedList<TblpagoDTO> list, User usuario){
		String mensaje="Ingreso realizado con \u00e9xito";
		System.out.println("NUMERO DE PAGOS: "+list.size());
		for(int i=0;i<list.size();i++){
			try {
				Date fechaau = null;
				SimpleDateFormat formatoDelText = new SimpleDateFormat("yyyy-MM-dd");
				fechaau = formatoDelText.parse(list.get(i).getFechaRealPago().toString());
				list.get(i).setFechaRealPago(fechaau);
				if(list.get(i).getValorPagado()<list.get(i).getValor()){
					System.out.println("iddoc "+list.get(i).getTbldtocomercial().getIdDtoComercial());
					mensaje=modificarcrearPago(list.get(i),list.get(i).getValorPagado(), usuario);
				}else if(list.get(i).getValorPagado()==list.get(i).getValor()){
					System.out.println("iddoc "+list.get(i).getTbldtocomercial().getIdDtoComercial());
					mensaje=modificarPago(list.get(i), usuario);
				}else{
					mensaje="EL del pago es mayor";
				}
			} catch (ParseException e1) {
				mensaje="Error al convertir fecha grabar egreso: "+e1;
				System.out.println("Error al convertir fecha grabar egreso: "+e1);
			}
			
			
		}
		return mensaje;
		
	}
	
	public String grabarPagos (LinkedList<TblpagoDTO> list, User usuario){
		
		String mensaje = null;
		for(int i=0;i<list.size();i++){
			
			if(list.get(i).getValorPagado()<list.get(i).getValor()){
				System.out.println("iddoc "+list.get(i).getTbldtocomercial().getIdDtoComercial());
				mensaje=modificarcrearPago(list.get(i),list.get(i).getValorPagado(), usuario);
				
			}else if(list.get(i).getValorPagado()==list.get(i).getValor()){
				System.out.println("iddoc "+list.get(i).getTbldtocomercial().getIdDtoComercial());
				mensaje=modificarPago(list.get(i), usuario);
			}
		}
		return mensaje;
	}
	
	
	@Override
	public String modificarcrearPago(TblpagoDTO entidad, double pagado, User usuario) throws IllegalArgumentException{
		TblpagoHome acces = new TblpagoHome();
		Tblpago tblpago= new Tblpago();
		String mensaje = "";
		int ban=0;
		try { 
			Tblpago p=new Tblpago();
			p.setEstado(entidad.getEstado());
			p.setFechaRealPago(entidad.getFechaRealPago());
			p.setFechaVencimiento(entidad.getFechaVencimiento());
			p.setIdPago(entidad.getIdPago());
			Tbldtocomercial tbldoc=new Tbldtocomercial(entidad.getTbldtocomercial());
			p.setTbldtocomercial(tbldoc);
			p.setValor(CValidarDato.getDecimal(2,entidad.getValor()));
			p.setFormaPago(entidad.getFormaPago());
			p.setConcepto(entidad.getConcepto());
			p.setIdEmpresa(entidad.getIdEmpresa());
			p.setEstablecimiento(entidad.getEstablecimiento());
			DtocomercialDTO doc=new DtocomercialDTO();
			doc.setIdEmpresa(entidad.getTbldtocomercial().getIdEmpresa());
			doc.setEstablecimiento(entidad.getTbldtocomercial().getEstablecimiento());
			doc.setPuntoEmision(entidad.getTbldtocomercial().getPuntoEmision());
			System.out.println("fecha entidad= "+String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setFecha(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setSubtotal(CValidarDato.getDecimal(2,entidad.getValor()));
			//doc.setTbldtocomercialTbltipopagos(entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos());
			doc.setTblpersonaByIdPersona(entidad.getTbldtocomercial().getTblpersonaByIdPersona());
			if (tbldoc.getTipoTransaccion()==1 || tbldoc.getTipoTransaccion()==10) doc.setTipoTransaccion(11);
			else doc.setTipoTransaccion(6);
				
			doc.setTblpersonaByIdVendedor(entidad.getTbldtocomercial().getTblpersonaByIdVendedor());
			doc.setTblpersonaByIdFacturaPor(entidad.getTbldtocomercial().getTblpersonaByIdFacturaPor());
	 		doc.setExpiracion(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setAutorizacion(entidad.getTbldtocomercial().getAutorizacion());
			doc.setNumPagos(0);
			doc.setEstado('1');
			//Llamamos a la funcion para obtener el ultimo ID del documento del mismo tipo
			try{
				DtocomercialDTO docBase = ultimoDocumento("where TipoTransaccion = '6' ", "numRealTransaccion");
				doc.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
			}catch(Exception e){
				doc.setNumRealTransaccion(0);
			}
			
			System.out.println("fechadto= "+doc.getFecha());
			doc.setObservacion(usuario.getUserName());
			Tbldtocomercial tbl=new Tbldtocomercial(doc);
			
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy/MM/dd");
		    Date fechaaux = null;
		    try {
				fechaaux = formatoDelTexto.parse(doc.getFecha());
			} catch (ParseException e1) {
				System.out.println("Error al convertir fecha modificarcrearPago: "+e1);
			}
			tbl.setExpiracion(entidad.getFechaRealPago());
			tbl.setFecha(entidad.getFechaRealPago());
			System.out.println("fechatbl= "+tbl.getFecha());
			
			Set<TbldtocomercialTbltipopago> tipoPagos = new HashSet<TbldtocomercialTbltipopago>(0);
			Iterator iterTP= entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
			
			DtocomercialTbltipopagoDTO tPago = new DtocomercialTbltipopagoDTO();
			while (iterTP.hasNext()) {
				tPago = (DtocomercialTbltipopagoDTO) iterTP.next();
				TbldtocomercialTbltipopago tpagoDTO = new TbldtocomercialTbltipopago(tPago);		
				tpagoDTO.setTbldtocomercial(tbl);
				Tbltipopago tipopago=new Tbltipopago();
				tipopago.setIdTipoPago(tPago.getTipoPago());
				tipopago.setIdCuenta(1);
				tipopago.setIdPlan(1);
				switch (tPago.getTipoPago()){
					case 1:{
						tipopago.setTipoPago("CONTADO");
						break;
					}
					case 2:{
						tipopago.setTipoPago("CREDITO");
						break;
					}
					case 3:{
						tipopago.setTipoPago("RETENCION");
						break;
					}
					case 4:{
						tipopago.setTipoPago("ANTICIPO");
						break;
					}
					case 5:{
						tipopago.setTipoPago("NOTA");
						break;
					}
					case 6:{
						tipopago.setTipoPago("TARJETA");
						break;
					}
					case 7:{
						tipopago.setTipoPago("BANCO");
						break;
					}
				
				}
				tpagoDTO.setTbltipopago(tipopago);
				if(entidad.getTbldtocomercial().getTipoTransaccion()==1||entidad.getTbldtocomercial().getTipoTransaccion()==10){
					tpagoDTO.setTipo('1');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
					tipoPagos.add(tpagoDTO);
				}else{
					tpagoDTO.setTipo('0');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
					tipoPagos.add(tpagoDTO);
				}
				//this.tbldtocomercial = documento;
				//this.tbltipopago = Tipopago;
				//tipoPagos.add(tpagoDTO);	esto cambie el 13/09/2012 12pm
	        }
			System.out.println("A punto de grabar pago");
			acces.modificarcrearPago(p,pagado,tbl,tipoPagos);
			mensaje="Pago Ingresado. Se generar� un nuevo pago con el valor restante ";
		} catch (ErrorAlGrabar e) {
		} catch (RuntimeException re) {
			System.out.println("No se pudo grabar el dato");
			mensaje="No se pudo grabar el dato";
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		return mensaje;
	}
	
	public String modificarPago(TblpagoDTO entidad , User usuario) throws IllegalArgumentException {
		TblpagoHome acces = new TblpagoHome();
		Tblpago tblpago= new Tblpago();
		String mensaje = "";
		int ban=0;
		//try {
			System.out.println("fechad de entidad TblpagoDTO= "+String.valueOf(entidad.getFechaRealPago()));
			Tblpago p=new Tblpago(entidad);
			p.setEstado(entidad.getEstado());
			p.setFechaRealPago(entidad.getFechaRealPago());
			p.setFechaVencimiento(entidad.getFechaVencimiento());
			p.setIdPago(entidad.getIdPago());
			p.setEstado('1');
			p.setConcepto(entidad.getConcepto());
			p.setNumEgreso(entidad.getNumEgreso());
			p.setFormaPago(entidad.getFormaPago());
			DtocomercialDTO doc=new DtocomercialDTO();
			System.out.println("fecha entidad= "+String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setIdEmpresa(entidad.getTbldtocomercial().getIdEmpresa());
			doc.setEstablecimiento(entidad.getTbldtocomercial().getEstablecimiento());
			doc.setPuntoEmision(entidad.getTbldtocomercial().getPuntoEmision());
			doc.setFecha(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setSubtotal(CValidarDato.getDecimal(2,entidad.getValor()));
			//doc.setTbldtocomercialTbltipopagos(entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos());
			doc.setTblpersonaByIdPersona(entidad.getTbldtocomercial().getTblpersonaByIdPersona());
			if(entidad.getTbldtocomercial().getTipoTransaccion()==0  ||entidad.getTbldtocomercial().getTipoTransaccion()==2 )
			{
				doc.setTipoTransaccion(6);
				try{
					DtocomercialDTO docBase = ultimoDocumento("where TipoTransaccion = '6' ", "numRealTransaccion");
					doc.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
				}catch(Exception e){
					doc.setNumRealTransaccion(0);
				}
			}
			else
			{
				doc.setTipoTransaccion(11);
				try{
					DtocomercialDTO docBase = ultimoDocumento("where TipoTransaccion = '11' ", "numRealTransaccion");
					doc.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
				}catch(Exception e){
					doc.setNumRealTransaccion(0);
				}
			}	
			doc.setTblpersonaByIdVendedor(entidad.getTbldtocomercial().getTblpersonaByIdVendedor());
			doc.setTblpersonaByIdFacturaPor(entidad.getTbldtocomercial().getTblpersonaByIdFacturaPor());
	 		doc.setExpiracion(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setAutorizacion(entidad.getTbldtocomercial().getAutorizacion());
//			doc.setAutorizacion(String.valueOf(entidad.getTbldtocomercial().getIdDtoComercial()));
			doc.setNumPagos(0);
			doc.setEstado('1');
			//Llamamos a la funcion para obtener el ultimo ID del documento del mismo tipo
			
//			try{
//				DtocomercialDTO docBase = ultimoDocumento("where TipoTransaccion = '6' ", "numRealTransaccion");
//				doc.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
//			}catch(Exception e){
//				doc.setNumRealTransaccion(0);
//			}
			//System.out.println("fechadto= "+doc.getFecha());
			doc.setObservacion(usuario.getUserName());
			Tbldtocomercial tbl=new Tbldtocomercial(doc);
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy/MM/dd");
		    Date fechaaux = null;
		    try {
				fechaaux = formatoDelTexto.parse(doc.getFecha());
			} catch (ParseException e1) {
				System.out.println("Error al convertir fecha modificarPago: "+e1);
			}
			tbl.setExpiracion(entidad.getFechaRealPago());
			tbl.setFecha(entidad.getFechaRealPago());
			System.out.println("fechatbl= "+tbl.getFecha());
			
			Set<TbldtocomercialTbltipopago> tipoPagos = new HashSet<TbldtocomercialTbltipopago>(0);
			Iterator iterTP= entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
			
			DtocomercialTbltipopagoDTO tPago = new DtocomercialTbltipopagoDTO();
			while (iterTP.hasNext()) {
				tPago = (DtocomercialTbltipopagoDTO) iterTP.next();
				TbldtocomercialTbltipopago tpagoDTO = new TbldtocomercialTbltipopago(tPago);
				
				tpagoDTO.setTbldtocomercial(tbl);
				Tbltipopago tipopago=new Tbltipopago();
				tipopago.setIdTipoPago(tPago.getTipoPago());
				tipopago.setIdCuenta(1);
				tipopago.setIdPlan(1);
				switch (tPago.getTipoPago()){
					case 1:{
						tipopago.setTipoPago("CONTADO");
						break;
					}
					case 2:{
						tipopago.setTipoPago("CREDITO");
						break;
					}
					case 3:{
						tipopago.setTipoPago("RETENCION");
						break;
					}
					case 4:{
						tipopago.setTipoPago("ANTICIPO");
						break;
					}
					case 5:{
						tipopago.setTipoPago("NOTA");
						break;
					}
					case 6:{
						tipopago.setTipoPago("TARJETA");
						break;
					}
					case 7:{
						tipopago.setTipoPago("BANCO");
						break;
					}
				
				}
				tpagoDTO.setTbltipopago(tipopago);
				if(entidad.getTbldtocomercial().getTipoTransaccion()==1||entidad.getTbldtocomercial().getTipoTransaccion()==10){
					tpagoDTO.setTipo('1');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
					System.out.println("TIPO PAGO "+tipopago.getTipoPago());
					if(tipopago.getTipoPago().equals("CONTADO")){
						System.out.println("TIPO PAGO ADENTRO "+tipopago.getTipoPago());
						tpagoDTO.setTipo('0');
					}
				}else{
					tpagoDTO.setTipo('0');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
					
				}
				//this.tbldtocomercial = documento;
				//this.tbltipopago = Tipopago;
				tipoPagos.add(tpagoDTO);	
	        }
			
			//tbl.setTbldtocomercialTbltipopagos(tipoPagos);
			p.setValor(CValidarDato.getDecimal(2,entidad.getValor()));
			p.setTbldtocomercial(new Tbldtocomercial(entidad.getTbldtocomercial()));
			try {
				acces.modificarpago(p,tbl,tipoPagos);
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				System.out.println("ERROR INTERNO: "+e.getLocalizedMessage());
				e.printStackTrace(System.out);
				
			}
			mensaje="Modificaci\u00F3n exitosa";
		/*} catch (ErrorAlGrabar e) {
			System.out.println("error en pago greetingServiceImplements "+e.getMessage());
			
		} catch (RuntimeException re) {
			System.out.println("error en pago greetingServiceImplements "+re.getMessage());
			mensaje="No se pudo grabar el dato";
			mensaje = re.getMessage();
			throw re;	
		} */
		System.out.println(mensaje);
		return mensaje;
	
	}
	
	
	
	
	@Override
	/**
	 * Funcion ProdBodega extrae un listado de las bodegas con el stock de determinado producto
	 * @param idProducto ide Del Porducto a buscar
	 * @param tabla tabla con la que vamos a hacer el join, puede ser bodega o producto
	 * @return un listado tipo BodegaProdDTO que contiene las bodegas con el stock de determinado producto
	 */
	
	public List<BodegaProdDTO> ProdBodega(String idProducto,String tabla){
		List<BodegaProdDTO> list = new ArrayList<BodegaProdDTO>();
		TblproductoTblbodegaHome acceso = new TblproductoTblbodegaHome();
		Iterator resultado = acceso.findJoinGeneric(idProducto, tabla, "tblproducto.idProducto");
		while(resultado.hasNext()){
			Object[] pair = (Object[])resultado.next();
			TblproductoTblbodega pb = (TblproductoTblbodega) pair[0];
				pb=(TblproductoTblbodega) pair[0];
				Tblbodega bod=(Tblbodega) pair[1];
				BodegaProdDTO prodB = new BodegaProdDTO();
				prodB.setIdBodega(bod.getIdBodega());
				prodB.setBodega(bod.getNombre());
				prodB.setUbicacion(bod.getUbicacion());
				prodB.setCantidad(pb.getCantidad());	
				list.add(prodB);	
		}
		return list;
	}
	
	/**
	 * 	FUNCION PARA OBTENER UN DOCUMENTO SEGUN EL ID
	 * @param ID Tipo INTEGER, es el ID del documento a extraer
	 * @return documento de tipo DtocomercialDTO, es el dto que se busca
	 */
	public DtocomercialDTO getDtoID(Integer ID){
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		Tbldtocomercial dto = (Tbldtocomercial) acceso.findById(ID);
		DtocomercialDTO documento = new DtocomercialDTO();
		documento = crearDtocomercialDTO(dto);
		return documento;
	}
	public DtocomercialDTO getListDtoID(int inicio, int fin){
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		List<Tbldtocomercial> dtolist = acceso.getList(inicio,fin);
		DtocomercialDTO documento = new DtocomercialDTO();
		//documento = crearDtocomercialDTO(dto);
		return documento;
	}


	

	public DtocomercialDTO ultimoDocumento(String filtro, String campoOrden){
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		return crearDtocomercialDTO(acceso.getLast(filtro, campoOrden));
	}
	public RetencionDTO ultimaRetencion(String filtro, int tipo, String campoOrden){
		RetencionDTO retencion=new RetencionDTO();
		System.out.println("Consulta "+filtro+" "+tipo+" "+campoOrden);
		DtocomercialDTO documentoDTO= new DtocomercialDTO();
//		DtocomercialDTO documentoDTO=ultimoDocumento("where tipoTransaccion = "
////				+ "'"+19+"' "
//				+ "'"+tipo+"' ", "idDtoComercial");
//		Tbldtocomercial documento= new Tbldtocomercial(documentoDTO);
		TblretencionHome acceso = new TblretencionHome();
		TbldtocomercialHome accesoDto = new TbldtocomercialHome();
		retencion=crearRetencionDTO(acceso.getLast(filtro+"'"+tipo+"' ", campoOrden));
		if (retencion!=null){
			documentoDTO=getDtoID(accesoDto.DTOComercialPorIdRetencion(retencion.getIdRetencion()));
			retencion.setTbldtocomercialByIdDtoComercial(documentoDTO);
		}
		return retencion;
	}
	
	public ImpuestoDTO crearImpuestoReg(Tblimpuesto marc){
		ImpuestoDTO impuesto=new ImpuestoDTO();
		TblimpuestoHome accesoImp = new TblimpuestoHome();
		try{
			marc.getCodigo();
		}catch(Exception e){
			marc = accesoImp.findById(marc.getIdImpuesto());
		}
		impuesto.setIdImpuesto(marc.getIdImpuesto());
		impuesto.setCodigo(marc.getCodigo());
		impuesto.setIdCuenta(marc.getIdCuenta());
		impuesto.setIdPlan(marc.getIdPlan());
		impuesto.setNombre(marc.getNombre());
		impuesto.setRetencion(marc.getRetencion());
		impuesto.setTipo(marc.getTipo());
		impuesto.setCodigoElectronico(marc.getCodigoElectronico());
		return impuesto;
	}
	
	public PlanCuentaDTO crearPlanCuentaReg(Tblplancuenta cat){
		EmpresaDTO empresadto=new EmpresaDTO(cat.getTblempresa().getIdEmpresa(),cat.getTblempresa().getNombre());
		return new PlanCuentaDTO(empresadto,cat.getIdPlan(),String.valueOf(cat.getFechaCreacion()),cat.getPeriodo());
	}
	
	//Integer idBodega,String Bodega,String ubicacion,String telefono
	public BodegaDTO crearBodega(Tblbodega bod){
		return new BodegaDTO(bod.getIdBodega(),bod.getNombre(),bod.getUbicacion(),bod.getTelefono(),bod.getIdEmpresa(),bod.getEstablecimiento());
	}
	
	
	public DtocomercialTbltipopagoDTO crearDtoTipoPago(TbldtocomercialTbltipopago dtotipoPago){
		return new DtocomercialTbltipopagoDTO(dtotipoPago.getId().getIdTipoPago(),dtotipoPago.getCantidad(),
				dtotipoPago.getReferenciaDto(),dtotipoPago.getObservaciones(),dtotipoPago.getTipo());
		/*Integer tipoPago, double cantidad, 
			Integer referenciaDto, String observaciones)*/
	}
	
	public DtocomercialDTO crearDtocomercialDTO(Tbldtocomercial dto){
		System.out.println("En dtocomercial ");
		//System.out.println("	d: "+dto.toString());
		if(dto==null){
			System.out.println("dto es null");
			return null;
		}else { 
			System.out.println("no es null");
			/*return new DtocomercialDTO(dto.getIdDtoComercial(),dto.getNumRealTransaccion(), dto.getTipoTransaccion(),
					dto.getFecha().toString(),dto.getExpiracion().toString(),dto.getAutorizacion(),
					dto.getNumPagos(),dto.getEstado(),dto.getSubtotal()
					);*/
			PersonaDTO per = crearPersonaDTO(dto.getTblpersonaByIdPersona());
			System.out.println("	d2: "+dto.getTblpersonaByIdPersona().getCedulaRuc());
			PersonaDTO vendedor = crearPersonaDTO(dto.getTblpersonaByIdVendedor());
			System.out.println("	d3: "+dto.getTblpersonaByIdVendedor().getCedulaRuc());
			PersonaDTO facturaPor = crearPersonaDTO(dto.getTblpersonaByIdFacturaPor());
			System.out.println("	d4: "+dto.getTblpersonaByIdFacturaPor().getCedulaRuc());
			try{
				vendedor.setIdPersona(dto.getTblpersonaByIdVendedor().getIdPersona());
			}catch(Exception e){}
			try{
				per.setIdPersona(dto.getTblpersonaByIdPersona().getIdPersona());
			}catch(Exception e){}
			try{
				facturaPor.setIdPersona(dto.getTblpersonaByIdFacturaPor().getIdPersona());
			}catch(Exception e){}
			//System.out.println("vendedor: "+vendedor.getApellidos());
			System.out.println(String.valueOf(dto.getIdDtoComercial())+", "+ per.getCedulaRuc()+", "+
					vendedor.getCedulaRuc()+", "+facturaPor.getCedulaRuc()+", "+ String.valueOf(dto.getNumRealTransaccion())+", "+
					String.valueOf(dto.getTipoTransaccion())+", "+ dto.getFecha().toString()+", "+ dto.getExpiracion().toString()+", "+
					dto.getAutorizacion()+", "+ String.valueOf(dto.getNumPagos())+", "+ String.valueOf(dto.getEstado())+", "+ String.valueOf(dto.getSubtotal())+", "+
					String.valueOf(dto.getIdAnticipo())+", "+ String.valueOf(dto.getIdNotaCredito())+", "+ String.valueOf(dto.getIdRetencion())+", "+
					String.valueOf(dto.getCosto())+", "+dto.getNumCompra()+", "+String.valueOf(dto.getDescuento())+", "+ dto.getObservacion());
			if (dto.getTipoTransaccion()==1 || dto.getTipoTransaccion()==0){
				Set<RetencionDTO> retencionesPorIdFactura=retenciones(dto.getTblretencionsForIdFactura());
				TbldtocomercialHome acceso = new TbldtocomercialHome();
				Set<RetencionDTO> retencionesPorFacturaConDto= new HashSet<RetencionDTO>();
				Map<Integer,DtocomercialDTO> dtos = new HashMap<Integer,DtocomercialDTO> ();
				DtocomercialDTO dtoRet;
				System.out.println("Documentos de retencion: "+retencionesPorIdFactura.size());
				for (RetencionDTO ret:retencionesPorIdFactura){
					int iddto=acceso.DTOComercialPorIdRetencion(ret.getIdRetencion());
					System.out.println("Documento de retencion: "+iddto);
					//Agregar lineas de siystem out para comprobar a cual de los dos entra porq en getdto id se hace el bucle
					if(dtos.containsKey(iddto)){
						dtoRet=dtos.get(iddto);
					}else{
						System.out.println("Buscar documento de retencion "+iddto);
						dtoRet=getDtoID(iddto);
						
						dtos.put(iddto, dtoRet);
					}
					ret.setTbldtocomercialByIdDtoComercial(dtoRet);
					retencionesPorFacturaConDto.add(ret);
				}
				Set<RetencionDTO> retencionesPorIdDtoComercial=retenciones(dto.getTblretencionsForIdDtoComercial());
				return new DtocomercialDTO(
						dto.getIdEmpresa(),dto.getEstablecimiento(),dto.getPuntoEmision(),
						dto.getIdDtoComercial(), per,
						vendedor,facturaPor, dto.getNumRealTransaccion(),
						dto.getTipoTransaccion(), dto.getFecha().toString(), dto.getExpiracion().toString(),
						dto.getAutorizacion(), dto.getNumPagos(), dto.getEstado(), dto.getSubtotal(),
				/*********/listaDetallesDTO(dto.getTbldtocomercialdetalles()), retencionesPorFacturaConDto, retencionesPorIdDtoComercial,
						numPagos(dto.getTblpagos()),
						null/*movimientos*/, dto.getIdAnticipo(), dto.getIdNotaCredito(), dto.getIdRetencion(),
						tipoPago(dto.getTbldtocomercialTbltipopagos()),dto.getCosto(),dto.getNumCompra(),dto.getDescuento(), dto.getObservacion(), 0);	
			}
			return new DtocomercialDTO(
					dto.getIdEmpresa(),dto.getEstablecimiento(),dto.getPuntoEmision(),
					dto.getIdDtoComercial(), per,
			vendedor,facturaPor, dto.getNumRealTransaccion(),
			dto.getTipoTransaccion(), dto.getFecha().toString(), dto.getExpiracion().toString(),
			dto.getAutorizacion(), dto.getNumPagos(), dto.getEstado(), dto.getSubtotal(),
	/*********/listaDetallesDTO(dto.getTbldtocomercialdetalles()), retenciones(dto.getTblretencionsForIdFactura()),
			retenciones(dto.getTblretencionsForIdDtoComercial()),
			numPagos(dto.getTblpagos()),
			null/*movimientos*/, dto.getIdAnticipo(), dto.getIdNotaCredito(), dto.getIdRetencion(),
			tipoPago(dto.getTbldtocomercialTbltipopagos()),dto.getCosto(),dto.getNumCompra(),dto.getDescuento(), dto.getObservacion(), 0);	
		}
	}
	
	public DtoelectronicoDTO crearDtoelectronicoDTO(Tbldtoelectronico dto){
		if(dto==null)
			return null;
		else { 					
			return new DtoelectronicoDTO( dto.getIdEmpresa(),dto.getEstablecimiento(),dto.getPuntoEmision(),
					dto.getFechaautorizacion(), dto.getClave(), dto.getAutorizacion(), dto.getNombreXml(), dto.getNombreRide(), dto.getEstado(),dto.getAmbiente());					
		}
	}
	
	public Set<DtocomercialTbltipopagoDTO> tipoPago(Set<TbldtocomercialTbltipopago> dtotipoPagos){
		Set<DtocomercialTbltipopagoDTO> tipoPagosDTO = new HashSet<DtocomercialTbltipopagoDTO>(0);
		Iterator iterTP= dtotipoPagos.iterator();
		TbldtocomercialTbltipopago tPago = new TbldtocomercialTbltipopago();
		while (iterTP.hasNext()) {
			tPago = (TbldtocomercialTbltipopago) iterTP.next();
			DtocomercialTbltipopagoDTO tpagoDTO = crearDtoTipoPago(tPago);		
			tipoPagosDTO.add(tpagoDTO);	
            }
		return tipoPagosDTO;
	}
	
	public TblpagoDTO crearPagoDTO(Tblpago pago){
		Integer idPago=null;
		try{
			idPago=pago.getIdPago();
		}catch(Exception e){
			idPago=null;
		}
		System.out.println("Pago "+pago.getIdPago()+" estado "+pago.getEstado());
		TblpagoDTO pagoDTO=new TblpagoDTO(pago.getIdEmpresa(),pago.getEstablecimiento(),idPago,null, pago.getFechaVencimiento(),
				pago.getFechaRealPago(),pago.getValor(),pago.getEstado());
		if(pago.getEstado()!='0'){
			System.out.println("Pago "+pago.getIdPago()+" formapago "+pago.getFormaPago());
			pagoDTO=new TblpagoDTO(pago.getIdEmpresa(),pago.getEstablecimiento(),idPago,null, pago.getFechaVencimiento(),
					pago.getFechaRealPago(),pago.getValor(),pago.getEstado(),pago.getConcepto(),pago.getFormaPago());
			System.out.println("Pago "+pagoDTO.getIdPago()+" formapago "+pagoDTO.getFormaPago());
			
		}
		
//		TblpagoDTO pagoDTO=(pago.getEstado()=='0')?new TblpagoDTO(idPago,null, pago.getFechaVencimiento(),
//				pago.getFechaRealPago(),pago.getValor(),pago.getEstado()):
//					new TblpagoDTO(idPago,null, pago.getFechaVencimiento(),
//							pago.getFechaRealPago(),pago.getValor(),pago.getEstado(),pago.getFormaPago());
		return pagoDTO;
	}
	
	public RetencionDTO crearRetencionDTO(Tblretencion retencion){
		if(retencion==null){
			System.out.println("dto es null");
			return null;
		}else{
			System.out.println("REVISAR ID RETENCION: ");
		Integer idRetencion=null;
		try{
			idRetencion=retencion.getIdRetencion();
		}catch(Exception e){
			idRetencion=null;
		}
		System.out.println("REVISAR ID RETENCION: "+idRetencion);
		System.out.println("VALOR RETENIDO REVISAR: "+retencion.getValorRetenido());
//		DtocomercialDTO factura;
//		DtocomercialDTO documento;
//		factura=(retencion.getTbldtocomercialByIdFactura()==null)?null:crearDtocomercialDTO(retencion.getTbldtocomercialByIdFactura());
//		documento=(retencion.getTbldtocomercialByIdDtoComercial()==null)?null:crearDtocomercialDTO(retencion.getTbldtocomercialByIdDtoComercial());
		return new RetencionDTO(idRetencion,null,crearImpuestoReg(retencion.getTblimpuesto()),
				retencion.getBaseImponible(),retencion.getNumRealRetencion(), retencion.getAutorizacionSri(),
				retencion.getEstado(),retencion.getValorRetenido(),retencion.getNumero());
		}
	}
	
	
	public Set<TblpagoDTO> numPagos(Set<Tblpago> pagos){
		Iterator iterDetalles= pagos.iterator();
		Set<TblpagoDTO> listado = new HashSet<TblpagoDTO>(0);
		Tblpago pago = new Tblpago();
		while (iterDetalles.hasNext()) {
			pago = (Tblpago) iterDetalles.next();
			TblpagoDTO pagoDTO = crearPagoDTO(pago);		
			listado.add(pagoDTO);	
            }
		return listado;
	}
	
	public Set<RetencionDTO> retenciones(Set<Tblretencion> retenciones){
		Iterator iterDetalles= retenciones.iterator();
		Set<RetencionDTO> listado = new HashSet<RetencionDTO>(0);
		Tblretencion retencion = new Tblretencion();
		while (iterDetalles.hasNext()) {
			retencion = (Tblretencion) iterDetalles.next();
			RetencionDTO retencionDTO = crearRetencionDTO(retencion);	
			listado.add(retencionDTO);	
            }
		return listado;
	}
	
	
	/*DtocomercialDTO dtocomercial,
	ImpuestoDTO impuesto, double baseImponible,
	int numRealRetencion, String autorizacionSri, 
	char estado, String numero
	
	*/
	
	
	public DtoComDetalleDTO crearDtoComDetalleDTO(Tbldtocomercialdetalle det){
/*********/DtoComDetalleDTO detDTO = new DtoComDetalleDTO(det.getIdDtoComercialDetalle(),
				crearBodega(det.getTblbodega()),crearProductoReg(det.getTblproducto(), -1),
				det.getCantidad(),det.getPrecioUnitario(),det.getDepartamento(),
//				det.getImpuesto(),
				det.getTotal(),det.getDesProducto());		
			detDTO.setCostoProducto(det.getCostoProducto());
		return detDTO;
	}
	public Set<TbldtocomercialdetalleTblmultiImpuesto> crearSetDtoComDetalleMultiImpuestoDTO(Set<DtoComDetalleMultiImpuestosDTO> productosMultiImpuesto){
		Set<TbldtocomercialdetalleTblmultiImpuesto> detMul =new HashSet(0);
		for (DtoComDetalleMultiImpuestosDTO detalleMultiImpuesto:productosMultiImpuesto){
			detMul.add(new TbldtocomercialdetalleTblmultiImpuesto(detalleMultiImpuesto));
		}
		return detMul;
	}
	public DtoComDetalleMultiImpuestosDTO crearDtoComDetalleMultiImpuestoDTO(TbldtocomercialdetalleTblmultiImpuesto det){
		DtoComDetalleMultiImpuestosDTO detImpDTO= new DtoComDetalleMultiImpuestosDTO(det.getIdtbldtocomercialdetalle_tblmulti_impuesto(),
				det.getPorcentaje(),det.getTipo(),det.getNombre());
		return detImpDTO;
	}
	public Set<DtoComDetalleDTO> listaDetallesDTO(Set<Tbldtocomercialdetalle> detalles){
		Iterator iterDetalles= detalles.iterator();
		Set<DtoComDetalleDTO> listado = new HashSet<DtoComDetalleDTO>(0);
		Tbldtocomercialdetalle det = new Tbldtocomercialdetalle();
		while (iterDetalles.hasNext()) {
			det = (Tbldtocomercialdetalle) iterDetalles.next();
/*********/	DtoComDetalleDTO detDTO = crearDtoComDetalleDTO(det);
			Set<DtoComDetalleMultiImpuestosDTO>  multImps=new HashSet<DtoComDetalleMultiImpuestosDTO>(0);
			for (TbldtocomercialdetalleTblmultiImpuesto mult:det.getTblimpuestos()){
				multImps.add(crearDtoComDetalleMultiImpuestoDTO(mult));
			}
						detDTO.setTblimpuestos(multImps);
			listado.add(detDTO);	
            }
		return listado;
	}
	
	//POSICIONES
	
	@Override
	public String grabarPosiciones(LinkedList<PosicionDTO> pos) {
		TblposicionHome acceso=new TblposicionHome();
		String mensaje="";
		for(int i=0;i<pos.size();i++){
			Tblposicion posicion=new Tblposicion(pos.get(i));
			try {
				System.out.println("campo= "+posicion.getCampo()+" i="+i);
				if(acceso.BuscarPosicion(posicion.getNombreDocumento(),posicion.getCampo()).size()>0){
					Tblposicion posaux=acceso.BuscarPosicion(posicion.getNombreDocumento(),posicion.getCampo()).get(0);
					posicion.setId(posaux.getId());
					acceso.modificar(posicion);
					mensaje="Configuraci�n de Impresi�n Guardada";
					
				}else{
					acceso.grabar(posicion);
					mensaje="Configuraci�n de Impresi�n Guardada grabar";
				}
			}catch (ErrorAlGrabar e) {
				mensaje="Datos no Guardados";
				System.out.println(e.getMessage() +"pos: "+i);
			}
		}
		return mensaje;
	}

	@Override
	public List<PosicionDTO> listarPosiciones(String NombreDocuemto) {
		TblposicionHome acceso=new TblposicionHome();
		List<Tblposicion> list=acceso.ListarPosiciones(NombreDocuemto);
		LinkedList<PosicionDTO> listpos = new LinkedList<PosicionDTO>();
		if(list!=null){
			for(int i=0;i<list.size();i++){
				PosicionDTO pos=new PosicionDTO();
				pos.setCampo(list.get(i).getCampo());
				pos.setId(list.get(i).getId());
				pos.setLargo(list.get(i).getLargo());
				pos.setNombreDocumento(list.get(i).getNombreDocumento());
				pos.setPosicion(list.get(i).getPosicion());
				listpos.add(pos);
				
			}
		}
		return listpos;
	}
	
	
	
	@Override
	public String grabarDetalle(LinkedList<RetencionDTO> detalle) {
		String mensaje=" ";
		//try{
			LinkedList<Tblretencion> det=new LinkedList<Tblretencion>();
			Tblretencion ret=new Tblretencion();
			TblretencionHome acceso=new TblretencionHome();
			if(detalle!=null){
				try {
					for(int i=0;i<detalle.size();i++){
						ret=new Tblretencion(detalle.get(i));
						ret.setEstado('1');
						System.out.println(mensaje);
						det.add(ret);
					}
					acceso.grabarListRet(det);
					mensaje="Datos grabados";
				} catch (ErrorAlGrabar e) {
					//System.out.println(e.getMessage());
					mensaje="Error datos no Grabados "+e.getMessage();
					e.printStackTrace();
				}
				
		}
		return mensaje;
	}

	@Override
	public String TransferirBodega(LinkedList<TraspasoBodegaProductoDTO> listTransferir) {
		LinkedList<TblTraspasoBodegaProducto> list=new LinkedList<TblTraspasoBodegaProducto>();
		
		for(int i=0;i<listTransferir.size();i++){
			System.out.println(listTransferir.get(i).getProductoBodega().getTblbodega().getIdBodega());
			list.add(new TblTraspasoBodegaProducto(listTransferir.get(i)));
		}
		TblproductoTblbodegaHome acceso=new TblproductoTblbodegaHome();
		return acceso.TransferirBodega(list);
		
		
	}
	
	//SERIES

	@Override
	public String grabarSeries(LinkedList<SerieDTO> dto) {
		String mensaje="";
		TblserieHome acceso=new TblserieHome();
		
		LinkedList<Tblserie> series=new LinkedList<Tblserie>();
		try {
			for(int i=0;i<dto.size();i++){
				Tblserie serie=new Tblserie();
				serie.setFechaExpiracion(dto.get(i).getFechaExpiracion());
				System.out.println(dto.get(i).getSerie());
				TbldtocomercialHome accesodoc = new TbldtocomercialHome();
				Tbldtocomercial documento = new Tbldtocomercial();
				System.out.println(dto.get(i).getTblproductos().getIdProducto());
				try{
					//if(dto.get(i).getIdDtoComercialC().getIdDtoComercial())){
					documento=(Tbldtocomercial) accesodoc.findById(dto.get(i).getIdDtoComercialC().getIdDtoComercial());
					serie.setTbldtocomercialByIdDtoComercialC(documento);
					System.out.println(serie.getTbldtocomercialByIdDtoComercialC().getIdDtoComercial());
					//}
				}catch(Exception e1){
					
				}
				try{
					documento = new Tbldtocomercial();
					//if(!(dto.get(i).getIdDtoComercialV().getIdDtoComercial()==0)){
					documento=(Tbldtocomercial) accesodoc.findById(dto.get(i).getIdDtoComercialV().getIdDtoComercial());
					serie.setTbldtocomercialByIdDtoComercialV(documento);
					System.out.println(serie.getTbldtocomercialByIdDtoComercialV().getIdDtoComercial());
					//}
				}catch(Exception e2){
					
				}
				serie.setSerie(dto.get(i).getSerie());
				TblproductoHome producto=new TblproductoHome();
				serie.setTblproducto(producto.findById(dto.get(i).getTblproductos().getIdProducto()));
				series.add(serie);
			}
			if(dto.size()>0){
				System.out.println("series "+ series.size());
				acceso.grabarSeries(series);
				mensaje="Series Grabadas";
			}else{
				mensaje="No existen Series "; 
			}
		    
		} catch (ErrorAlGrabar e) {
			mensaje="Error Series NO Grabadas "+e.getMessage();
			e.printStackTrace();
		} 
		return mensaje;
	}
	public SerieDTO buscarSerie(String serie) {
		TblserieHome acceso=new TblserieHome();
		Tblserie tblserie=new Tblserie();
		tblserie=acceso.findByNombre(serie, "serie");
		SerieDTO seriedto=new SerieDTO();
		System.out.println("Serie tbl: "+tblserie.getSerie());
		seriedto.setIdSerie(tblserie.getIdSerie());
		seriedto.setSerie(tblserie.getSerie());
		System.out.println("Serie dto: "+seriedto.getSerie());
		DtocomercialDTO dto =new DtocomercialDTO();
		TbldtocomercialHome accesoDto=new TbldtocomercialHome();
		try{
			dto =new DtocomercialDTO();
			System.out.println("idC "+tblserie.getTbldtocomercialByIdDtoComercialC().getIdDtoComercial());
			tblserie.setTbldtocomercialByIdDtoComercialC(accesoDto.findById(tblserie.getTbldtocomercialByIdDtoComercialC().getIdDtoComercial()));
			dto=crearDtocomercialDTO(tblserie.getTbldtocomercialByIdDtoComercialC());
			System.out.println("num real= "+dto.getNumRealTransaccion());
			seriedto.setIdDtoComercialC(dto);
		}catch(Exception e){
			seriedto.setIdDtoComercialC(dto =new DtocomercialDTO());
		}
		
		try{
			dto =new DtocomercialDTO();
			System.out.println("idV "+tblserie.getTbldtocomercialByIdDtoComercialV().getIdDtoComercial());
			tblserie.setTbldtocomercialByIdDtoComercialC(accesoDto.findById(tblserie.getTbldtocomercialByIdDtoComercialC().getIdDtoComercial()));
			dto=crearDtocomercialDTO(tblserie.getTbldtocomercialByIdDtoComercialV());
			System.out.println("num real= "+dto.getNumRealTransaccion());
			seriedto.setIdDtoComercialV(dto);
		}catch(Exception e){
			seriedto.setIdDtoComercialV(dto =new DtocomercialDTO());
		}
		ProductoDTO producto=new ProductoDTO();
		TblproductoHome accesosprod=new TblproductoHome();
		Tblproducto prod=new Tblproducto();
		prod=accesosprod.findById(tblserie.getTblproducto().getIdProducto());
		producto.setIdEmpresa(prod.getIdEmpresa());
		producto.setEstablecimiento(prod.getEstablecimiento());
		producto.setCodigoBarras(prod.getCodigoBarras());
		producto.setIdProducto(prod.getIdProducto());
		producto.setDescripcion(prod.getDescripcion());
		seriedto.setTblproductos(producto);
		return seriedto;
	}
	
	public List<SerieDTO> listarSeries(int idproducto) {
		System.out.println("idproducto= "+idproducto);
		TblserieHome acceso=new TblserieHome();
		List<Tblserie> list=acceso.ListarSeriesProducto(idproducto);
		LinkedList<SerieDTO> listseries = new LinkedList<SerieDTO>();
		
		if(list!=null){
			for(int i=0;i<list.size();i++){
				SerieDTO serie=new SerieDTO();
				System.out.println(list.get(i).getSerie());
				serie.setIdSerie(list.get(i).getIdSerie());
				DtocomercialDTO dto =new DtocomercialDTO();
				try{
					dto =new DtocomercialDTO();
					System.out.println("idC "+list.get(i).getTbldtocomercialByIdDtoComercialC().getIdDtoComercial());
					dto=crearDtocomercialDTO(list.get(i).getTbldtocomercialByIdDtoComercialC());
					System.out.println("num real= "+dto.getNumRealTransaccion());
					serie.setIdDtoComercialC(dto);
				}catch(Exception e){
					serie.setIdDtoComercialC(dto =new DtocomercialDTO());
				}
				
				try{
					dto =new DtocomercialDTO();
					System.out.println("idV "+list.get(i).getTbldtocomercialByIdDtoComercialV().getIdDtoComercial());
					dto=crearDtocomercialDTO(list.get(i).getTbldtocomercialByIdDtoComercialV());
					System.out.println("num real= "+dto.getNumRealTransaccion());
					serie.setIdDtoComercialV(dto);
				}catch(Exception e){
					serie.setIdDtoComercialV(dto =new DtocomercialDTO());
				}
				
				ProductoDTO producto=new ProductoDTO();
				TblproductoHome accesosprod=new TblproductoHome();
				Tblproducto prod=new Tblproducto();
				prod=accesosprod.findById(idproducto);
				producto.setIdEmpresa(prod.getIdEmpresa());
				producto.setEstablecimiento(prod.getEstablecimiento());
				producto.setCodigoBarras(prod.getCodigoBarras());
				producto.setIdProducto(idproducto);
				producto.setDescripcion(prod.getDescripcion());
				serie.setTblproductos(producto);
				serie.setSerie(list.get(i).getSerie());
				listseries.add(serie);
			}
		}
		return listseries;
	}
	
	/*public PosicionDTO buscarPosicion(String NombreDocumento,String Campo){
		TblposicionHome acceso=new TblposicionHome();
		Tblposicion posicion=new Tblposicion();
		posicion=acceso.BuscarPosicion(posicion.getNombreDocumento(),posicion.getCampo()).get(0);
		PosicionDTO pos=new PosicionDTO();
		pos.setCampo(posicion.getCampo());
		pos.setId(posicion.getId());
		pos.setLargo(posicion.getLargo());
		pos.set
	}*/
	

	public ProductoTipoPrecioDTO obtenerPrecio(int idProducto, int idTipoPrecio) {
		TblproductoTipoprecioHome acceso = new TblproductoTipoprecioHome();
		TbltipoprecioHome tbltipopreciohome = new TbltipoprecioHome();
		TblproductoTipoprecio tblprotip= new TblproductoTipoprecio();
		ProductoTipoPrecioDTO productotipopreciodto =null;
		Double porcentaje = 0.0;
		System.out.println("Id Producto: "+ idProducto + " idtipoprecio :"+ idTipoPrecio);

		tblprotip = acceso.cargarTipoPrecio(idProducto, idTipoPrecio);
		if (tblprotip!=null){
			productotipopreciodto = new ProductoTipoPrecioDTO();
					Tbltipoprecio tbltipoprecio = tbltipopreciohome.findById(idTipoPrecio);
					if(tbltipoprecio!=null){
					TipoPrecioDTO tipopreciodto = new TipoPrecioDTO();
					tipopreciodto.setIdEmpresa(tbltipoprecio.getIdEmpresa());
					tipopreciodto.setIdTipoPrecio(idTipoPrecio);
					tipopreciodto.setTipoPrecio(tbltipoprecio.getTipoPrecio());
					productotipopreciodto.setPorcentaje(tblprotip.getPorcentaje());
					productotipopreciodto.setTbltipoprecio(tipopreciodto);
					System.out.println("TipoPrecio: "+ productotipopreciodto.getTbltipoprecio().getTipoPrecio());
					}else{
						System.out.println("ERROR AL OBTENER EL PRECIO");
					}
		}
		return productotipopreciodto;
	
	}	
	
	public String actualizarAso(List<TbltipodocumentoDTO> tipoDocumentos){
		String mensaje="";
		Tbltipodocumento documentos = null;
		List<Tbltipodocumento> detalles = new ArrayList<Tbltipodocumento>();
		Iterator iter = tipoDocumentos.iterator();
		while(iter.hasNext()){
			TbltipodocumentoDTO tipoDoc = new TbltipodocumentoDTO();
			tipoDoc = (TbltipodocumentoDTO) iter.next();
			detalles.add(new Tbltipodocumento(tipoDoc));
			System.out.println("Cuenta: "+tipoDoc.getIdCuenta());
		}
		TbltipodocumentoHome acceso = new TbltipodocumentoHome();
		//try {
			acceso.modificar(detalles);
		//} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			//mensaje = e.toString();
		//}
		return mensaje;
	}
	
	
	public String actualizarAsoPagos(List<TipoPagoDTO> tipoPagos){
		String mensaje="";
		Tbltipopago pagos = null;
		List<Tbltipopago> detalles = new ArrayList<Tbltipopago>();
		Iterator iter = tipoPagos.iterator();
		while(iter.hasNext()){
			TipoPagoDTO tipoPago = new TipoPagoDTO();
			tipoPago = (TipoPagoDTO) iter.next();
			detalles.add(new Tbltipopago(tipoPago));
			//System.out.println("Cuenta: "+tipoDoc.getIdCuenta());
		}
		TbltipopagoHome acceso = new TbltipopagoHome();
		//try {
			acceso.modificar(detalles);
		//} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			//mensaje = e.toString();
		//}
		return mensaje;
	}
	
	public String actualizarAsoImpuestos(List<ImpuestoDTO> tipoImpuestos){ 
		String mensaje="";
		Tbltipopago impuestos = null;
		List<Tblimpuesto> detalles = new ArrayList<Tblimpuesto>();
		Iterator iter = tipoImpuestos.iterator();
		while(iter.hasNext()){
			ImpuestoDTO impuesto = new ImpuestoDTO();
			impuesto = (ImpuestoDTO) iter.next();
			detalles.add(new Tblimpuesto(impuesto));
			//System.out.println("Cuenta: "+tipoDoc.getIdCuenta());
		}
		TblimpuestoHome acceso = new TblimpuestoHome();
		//try {
			acceso.modificar(detalles);
		//} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			//mensaje = e.toString();
		//}
		return mensaje;
	}
	//REPORTES
//	public double ReproteTotalDocumentos(String fechaI, String fechaf,String tipo){
//		double total=0;
//		TbldtocomercialdetalleHome acceso=new TbldtocomercialdetalleHome();
//		total=acceso.ReproteTotalDocumentos(fechaI, fechaf, tipo);
//		total=Float.parseFloat(String.valueOf(Math.rint(total*100)/100));
//		return total;
//	}
//	public double ReproteIVADocumentos(String fechaI, String fechaf,String tipo){
//		double Iva=0;
//		TbldtocomercialdetalleHome acceso=new TbldtocomercialdetalleHome();
//		Iva=acceso.ReproteIVADocumentos(fechaI, fechaf, tipo);
//		Iva=Float.parseFloat(String.valueOf(Math.rint(Iva*100)/100));
//		return Iva;
//	}
//	public double ReproteSubtIVA0Documentos(String fechaI, String fechaf,String tipo){
//		double SubtIva0=0;
//		TbldtocomercialdetalleHome acceso=new TbldtocomercialdetalleHome();
//		SubtIva0=acceso.ReproteSubtIVA0Documentos(fechaI, fechaf, tipo);
//		SubtIva0=Float.parseFloat(String.valueOf(Math.rint(SubtIva0*100)/100));
//		return SubtIva0;
//	}
//	public double ReproteSubTotalNetoDocumentos(String fechaI, String fechaf,String tipo){
//		double SubNeto=0;
//		TbldtocomercialdetalleHome acceso=new TbldtocomercialdetalleHome();
//		SubNeto=acceso.ReproteSubTotalNetoDocumentos(fechaI, fechaf, tipo);
//		SubNeto=Float.parseFloat(String.valueOf(Math.rint(SubNeto*100)/100));
//		return SubNeto; 
//	}
//	public ReportesDTO Libro(String fechaI, String fechaf,String tipo){
//		ReportesDTO reporte=new ReportesDTO();
//		reporte.setIVA(ReproteIVADocumentos(fechaI,fechaf,tipo));
//		reporte.setSubtIVA0(ReproteSubtIVA0Documentos(fechaI,fechaf,tipo));
//		reporte.setSubtNeto(ReproteSubTotalNetoDocumentos(fechaI,fechaf,tipo));
//		reporte.setTOTAL(ReproteTotalDocumentos(fechaI,fechaf,tipo));
//		return reporte;
//	} 
/*	public String listGridToExel(LinkedList<String[]> listado){
		String mensaje="";
		CreateExel test =new CreateExel();
		mensaje=test.ExportarArchivo(listado, "","");
		
		return mensaje;
	}
	*/
	public String listGridToExel(LinkedList<String[]> listado,String nombre){
		String mensaje="";
		String nombre1="";
		nombre1=nombre;
		String pathpdf = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/reportes/");
		nombre=pathpdf+File.separatorChar+nombre;
		System.out.println("NOMBRE DEL ARCHIVO "+ nombre);
		CreateExel test =new CreateExel();
		mensaje=test.ExportarArchivo(listado, nombre,nombre1);
		return mensaje;
	}
	
	public String exportarexcel(LinkedList<String[]> listado,String nombre){
		LinkedList<String[]> prodprec = new LinkedList () ;
		prodprec.add(listado.get(0));
		//prodprec.add(listado.get(1));
		String nombre1="";
		nombre1=nombre;
		
		String mensaje="";
		String pathpdf = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/reportes/");
		nombre=pathpdf+File.separatorChar+nombre;
		System.out.println("NOMBRE DEL ARCHIVO "+ nombre);
		CreateExel test =new CreateExel();
		for (int i=1 ;i< listado.size()-2 ;i++)
		{   String [] producto = listado.get(i);
			ProductoDTO  nprod= buscarProducto (producto[0],"codigoBarras");
			//int j = 0;
			List<ProductoTipoPrecio> lt = listarTipoPrecio(nprod.getIdProducto());
			String [] listaprecios = new String [lt.size()+4];
			listaprecios[0] = producto[0];
			listaprecios[1] = producto[1];
			listaprecios[2] = producto[2];
			for (ProductoTipoPrecio l : lt)
			{    
				listaprecios [l.getIdTipo()+2] = l.getPorcentaje()+"";
			   	
			 //  	 j++;
			}
			prodprec.add(listaprecios);
			listaprecios[listaprecios.length -1] = nprod.getIdProducto()+"";
			
		}
		prodprec.add(listado.get(listado.size()-2));
		prodprec.add(listado.get(listado.size()-1));
		
		mensaje=test.ExportarArchivo(prodprec, nombre,nombre1);
		return mensaje;
	}
	
	public LinkedList importarExcel (String ruta){
		//Obtiene el nombre del archivo
        LinkedList datostotales = new LinkedList();
        LinkedList <String[]> datosfact = new LinkedList ();
	    System.out.println("RUTA1"+ruta);
	    String [] rut = ruta.split("fakepath");  // Todas las rutas poseen la palabra fakepath
	    System.out.println("Tama�o"+rut.length);
	    CreateExel test =new CreateExel();
	    System.out.println(rut[1].substring(1)); // QUita el caracter especial "\"
		test.setInputFile(rut[1].substring(1));
		try {
			datosfact=test.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//ProductoBodegaDTO baux = null ;
		String [] cant = new String [datosfact.size()-2];
		String[] cab =  datosfact.get(datosfact.size()-1);
		BodegaDTO bd =   buscarBodegaSegunNombre (cab[2]);
		for (int i=1;i<datosfact.size()-2;i++){
			  String[] pro =  datosfact.get(i);
		    ProductoDTO  nprod= buscarProducto (pro[0],"codigoBarras");
		    datostotales.add (nprod);
		    cant [i-1]=  pro [1];
		    if (bd==null)
		     { 
		    	    	
		    	String nombre  = ProdBodega(nprod.getIdProducto()+"", "tblbodega").get(0).getBodega();
		    	 bd =   buscarBodegaSegunNombre (nombre);
		    	
		     }	    
		}
	     datostotales.add( cant);	 
	     datostotales.add(bd);
		 datostotales.add(cab[3]);
		return datostotales;
		
	}

	public LinkedList<MovimientoCuentaPlanCuentaDTO> listarLibro(String fechai, String fechaf, String planCuentaID) {
		TblmovimientoTblcuentaTblplancuentaHome tblmovimientotblcuentatblplancuentahome = new TblmovimientoTblcuentaTblplancuentaHome();
		TblcuentaHome tblcuentahome = new TblcuentaHome();
		List<TblmovimientoTblcuentaTblplancuenta> listado = null;
		LinkedList<MovimientoCuentaPlanCuentaDTO> listadoDTO = new LinkedList<MovimientoCuentaPlanCuentaDTO>();
		//try {
		listado=tblmovimientotblcuentatblplancuentahome.getListLibro(fechai,fechaf, planCuentaID); 
		if(listado.size()>0){
			for(int i=0;i<listado.size();i++){
				MovimientoCuentaPlanCuentaDTO mov=new MovimientoCuentaPlanCuentaDTO();
				MovimientoCuentaPlanCuentaId id=new MovimientoCuentaPlanCuentaId();
				id.setIdCuenta(listado.get(i).getId().getIdCuenta());
				id.setIdMovimiento(listado.get(i).getId().getIdMovimiento());
				id.setIdPlan(listado.get(i).getId().getIdPlan());
				mov.setId(id);
				mov.setSigno(listado.get(i).getSigno());
				
				CuentaPlanCuentaDTO cuent=new CuentaPlanCuentaDTO();				
				Tblcuenta tblcuenta=new Tblcuenta();
				tblcuenta=tblcuentahome.findById(listado.get(i).getTblcuentaPlancuenta().getId().getIdCuenta());
				CuentaDTO cuentaDTO=new CuentaDTO();
				cuentaDTO.setIdEmpresa(tblcuenta.getIdEmpresa());
				cuentaDTO.setCodigo(tblcuenta.getCodigo());
				cuentaDTO.setIdCuenta(tblcuenta.getIdCuenta());
				cuentaDTO.setNivel(tblcuenta.getNivel());
				cuentaDTO.setNombreCuenta(tblcuenta.getNombreCuenta());
				cuentaDTO.setPadre(tblcuenta.getPadre());

				
				cuent.setCuenta(cuentaDTO);
				CuentaPlanCuentaIdDTO  cpid=new CuentaPlanCuentaIdDTO();
				cpid.setIdCuenta(listado.get(i).getTblcuentaPlancuenta().getId().getIdCuenta());
				//System.out.println("setIdCuenta: "+listado.get(i).getTblcuentaPlancuenta().getId().getIdCuenta());
				cpid.setIdPlan(listado.get(i).getTblcuentaPlancuenta().getId().getIdPlan());
				cuent.setId(cpid);
				mov.setTblcuentaPlancuenta(cuent);
				MovimientoDTO movimiento =new MovimientoDTO();
//				movimiento.setConcepto(listado.get(i).getTblmovimiento().getConcepto());
	//			movimiento.setFecha(listado.get(i).getTblmovimiento().getFecha());
		//		movimiento.setIdMovimiento(listado.get(i).getTblmovimiento().getIdMovimiento());
				mov.setTblmovimiento(movimiento);
				mov.setValor(listado.get(i).getValor());
				/*System.out.println("valor "+mov.getValor());
				System.out.println("Signo "+mov.getSigno());
				System.out.println("movi "+mov.getId().getIdMovimiento());
				System.out.println("plan "+mov.getId().getIdPlan());
				System.out.println("cuenta "+mov.getId().getIdCuenta());*/
				listadoDTO.add(mov);
				
//				System.out.println("nombre cuenta "+listado.get(i).getTblcuentaPlancuenta().getTblcuenta().getNombreCuenta());
			}
		System.out.println("listado"+listadoDTO.size());
		System.out.println("valor"+listadoDTO.get(0).getValor());
		}
	
		return listadoDTO;
	}
	
	
	//LIBRO DIARIO
	

	public static Double getDecimal(int numeroDecimales,double decimal){
		decimal = decimal*(java.lang.Math.pow(10, numeroDecimales));
		decimal = java.lang.Math.round(decimal);
		decimal = decimal/java.lang.Math.pow(10, numeroDecimales);
		return decimal;
	}
	
	
	public String pagoConRetencion(LinkedList<RetencionDTO> detalle,DtocomercialDTO doc,TblpagoDTO pagodto){
		String mensaje="";
		System.out.println("A punto de grabar la retencion");
		LinkedList<Tblretencion> det=new LinkedList<Tblretencion>();
		Tblretencion ret=new Tblretencion();
		TblretencionHome acceso=new TblretencionHome();
		if(detalle!=null){
			//try {
				for(int i=0;i<detalle.size();i++){
					Tblretencion tblret= new Tblretencion();
					ret=tblret.Tblret(detalle.get(i));
					ret.setEstado('1');
//					Tbldtocomercial dtoTemp=new Tbldtocomercial();
//					dtoTemp.setIdDtoComercial(detalle.get(i).getTbldtocomercialByIdDtoComercial());
//					ret.setTbldtocomercialByIdFactura(dtoTemp);
					//-----------------------Pasar a tcldtocomercial el DTOtbldtocomercial
					Tbldtocomercial dtoTemp=new Tbldtocomercial();
					dtoTemp.setIdDtoComercial(detalle.get(i).getTbldtocomercialByIdFactura().getIdDtoComercial());
					ret.setTbldtocomercialByIdFactura(dtoTemp);
//					ret.setTbldtocomercialByIdFactura(new Tbldtocomercial(detalle.get(i).getTbldtocomercialByIdFactura()));
//					ret.setIdFactura(detalle.get(i).getIdFactura());
					System.out.println(mensaje);
					det.add(ret);
				}
				Tbldtocomercial dto=new Tbldtocomercial(doc);
				dto.setEstado('1');
				RetencionPago retpago=new RetencionPago();
				retpago.setDetret(det);
				retpago.setDocRetencion(dto);
				//Crear el Tblpago
				
				if(pagodto.getValorPagado()<pagodto.getValor()){
					///modificarcrerpago
					//mensaje=modificarcrearPago(list.get(i),list.get(i).getValorPagado());
					Tblpago p=new Tblpago();
					p.setEstado(pagodto.getEstado());
					p.setFechaRealPago(pagodto.getFechaRealPago());
					p.setFechaVencimiento(pagodto.getFechaVencimiento());
					p.setIdPago(pagodto.getIdPago());
					Tbldtocomercial tbldoc=new Tbldtocomercial(pagodto.getTbldtocomercial());
					p.setTbldtocomercial(tbldoc);
					p.setValor(pagodto.getValor());
					p.setConcepto(pagodto.getConcepto());
					p.setFormaPago("Retencion");
					p.setEstablecimiento(pagodto.getEstablecimiento());
					p.setIdEmpresa(pagodto.getIdEmpresa());
					
					DtocomercialDTO docP=new DtocomercialDTO();
					System.out.println("fecha entidad= "+String.valueOf(pagodto.getFechaRealPago()).replace('-','/'));
					docP.setIdEmpresa(pagodto.getTbldtocomercial().getIdEmpresa());
					docP.setEstablecimiento(pagodto.getTbldtocomercial().getEstablecimiento());
					docP.setPuntoEmision(pagodto.getTbldtocomercial().getPuntoEmision());
					docP.setFecha(String.valueOf(pagodto.getFechaRealPago()).replace('-','/'));
					docP.setSubtotal(pagodto.getValor());
					//doc.setTbldtocomercialTbltipopagos(entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos());
					docP.setTblpersonaByIdPersona(pagodto.getTbldtocomercial().getTblpersonaByIdPersona());
					docP.setTipoTransaccion(6);
					docP.setTblpersonaByIdVendedor(pagodto.getTbldtocomercial().getTblpersonaByIdVendedor());
					docP.setTblpersonaByIdFacturaPor(pagodto.getTbldtocomercial().getTblpersonaByIdFacturaPor());
			 		docP.setExpiracion(String.valueOf(pagodto.getFechaRealPago()).replace('-','/'));
					docP.setAutorizacion(pagodto.getTbldtocomercial().getAutorizacion());
					docP.setNumPagos(0);
					docP.setEstado('1');
					//Llamamos a la funcion para obtener el ultimo ID del documento del mismo tipo
					try{
						DtocomercialDTO docBase = ultimoDocumento("where TipoTransaccion = '6' ", "numRealTransaccion");
						docP.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
					}catch(Exception e){
						docP.setNumRealTransaccion(0);
					}
					
					System.out.println("fechadto= "+docP.getFecha());
					docP.setObservacion(dto.getObservacion());
					Tbldtocomercial tbl=new Tbldtocomercial(docP);
					
					SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy/MM/dd");
				    Date fechaaux = null;
				    try {
						fechaaux = formatoDelTexto.parse(docP.getFecha());
					} catch (ParseException e1) {
						System.out.println("Error al convertir fecha pagoConRetencion: "+e1);
					}
					tbl.setExpiracion(pagodto.getFechaRealPago());
					tbl.setFecha(pagodto.getFechaRealPago());
					System.out.println("fechatbl= "+tbl.getFecha());
					
					Set<TbldtocomercialTbltipopago> tipoPagos = new HashSet<TbldtocomercialTbltipopago>(0);
					Iterator iterTP= pagodto.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
					
					DtocomercialTbltipopagoDTO tPago = new DtocomercialTbltipopagoDTO();
					while (iterTP.hasNext()) {
						tPago = (DtocomercialTbltipopagoDTO) iterTP.next();
						tPago.setObservaciones("Pago con Retencion");
						TbldtocomercialTbltipopago tpagoDTO = new TbldtocomercialTbltipopago(tPago);		
						tpagoDTO.setTbldtocomercial(tbl);
						Tbltipopago tipopago=new Tbltipopago();
						tipopago.setIdTipoPago(tPago.getTipoPago());
						tipopago.setIdCuenta(1);
						tipopago.setIdPlan(1);
						switch (tPago.getTipoPago()){
							case 1:{
								tipopago.setTipoPago("CONTADO");
								break;
							}
							case 2:{
								tipopago.setTipoPago("CREDITO");
								break;
							}
							case 3:{
								tipopago.setTipoPago("RETENCION");
								break;
							}
							case 4:{
								tipopago.setTipoPago("ANTICIPO");
								break;
							}
							case 5:{
								tipopago.setTipoPago("NOTA");
								break;
							}
							case 6:{
								tipopago.setTipoPago("TARJETA");
								break;
							}
							case 7:{
								tipopago.setTipoPago("BANCO");
								break;
							}
						
						}
						tpagoDTO.setTbltipopago(tipopago);
						if(pagodto.getTbldtocomercial().getTipoTransaccion()==1||pagodto.getTbldtocomercial().getTipoTransaccion()==10){
							tpagoDTO.setTipo('1');
							System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
							tipoPagos.add(tpagoDTO);
						}else{
							tpagoDTO.setTipo('0');
							System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
							tipoPagos.add(tpagoDTO);
						}
						//this.tbldtocomercial = documento;
						//this.tbltipopago = Tipopago;
						//tipoPagos.add(tpagoDTO);	esto cambie el 13/09/2012 12pm
			        }
					
					//acces.modificarcrearPago(p,pagado,tbl,tipoPagos);
					retpago.setT(p);
					retpago.setTbl(tbl);
					retpago.setTipoPagos(tipoPagos);
					System.out.println("A punto de grabar pago y retencion");
					acceso.grabarRetencionPago(retpago);
					mensaje="Pago realizado..."; 
					//FIN MODIFICARCREAPAGO
				}else if(pagodto.getValorPagado()==pagodto.getValor()){
					//modificar pago
					//System.out.println("iddoc "+list.get(i).getTbldtocomercial().getIdDtoComercial());
					//mensaje=modificarPago(list.get(i));
				}
				
				
				
				
				
				
				
				
				
				
				
				
				//acceso.grabarRetencionPago(det,dto,pagodto);
				//mensaje="Datos grabados";
			/*} catch (Exception e) {
				//System.out.println(e.getMessage());
				mensaje="Error datos no Grabados "+e.getMessage();
				e.printStackTrace();
			}*/
		}
		return mensaje;
	}
/*	public String grabarRetencion(LinkedList<RetencionDTO> detalle,DtocomercialDTO doc) {
		String mensaje=" ";
		//try{
		System.out.println("A punto de grabar la retencion "+ detalle.size());
		LinkedList<Tblretencion> det=new LinkedList<Tblretencion>();
		Tblretencion ret=new Tblretencion();
		TblretencionHome acceso=new TblretencionHome();
		if(detalle!=null){
			try {
				for(int i=0;i<detalle.size();i++){
					//detalle.get(i).getImpuesto().setIdImpuesto(1);
					System.out.println("impuesto "+detalle.get(i).getImpuesto().getIdImpuesto());
					Tblretencion tblret= new Tblretencion();
					ret=tblret.Tblret(detalle.get(i));
					ret.setEstado('1');
					ret.setIdFactura(detalle.get(i).getIdFactura());
					System.out.println(mensaje);
					det.add(ret);
				}
				Tbldtocomercial dto=new Tbldtocomercial(doc);
				dto.setEstado('1');
				acceso.grabarRetencion(det,dto);
				mensaje="Datos grabados";
			} catch (ErrorAlGrabar e) {
				//System.out.println(e.getMessage());
				mensaje="Error datos no Grabados "+e.getMessage();
				e.printStackTrace();
			}
			
		}
		return mensaje; 
	}*/
	
	public String grabarRetencion(LinkedList<RetencionDTO> detalle,DtocomercialDTO doc) {
		String mensaje=" ";
		//try{
		System.out.println("A punto de grabar la retencion "+ detalle.size());
		LinkedList<Tblretencion> det=new LinkedList<Tblretencion>();
		Tblretencion ret=new Tblretencion();
		TblretencionHome acceso=new TblretencionHome();
		if(detalle!=null){
			try {
				for(int i=0;i<detalle.size();i++){
					//detalle.get(i).getImpuesto().setIdImpuesto(1);
					System.out.println("impuesto "+detalle.get(i).getImpuesto().getIdImpuesto());
					Tblretencion tblret= new Tblretencion();
					ret=tblret.Tblret(detalle.get(i));
					ret.setEstado('1');
//					Tbldtocomercial dtoTemp=new Tbldtocomercial();
//					dtoTemp.setIdDtoComercial(detalle.get(i).getTbldtocomercialByIdDtoComercial());
//					ret.setTbldtocomercialByIdFactura(dtoTemp);
					Tbldtocomercial dtoTemp=new Tbldtocomercial();
					dtoTemp.setIdDtoComercial(detalle.get(i).getTbldtocomercialByIdFactura().getIdDtoComercial());
					ret.setTbldtocomercialByIdFactura(dtoTemp);
					//ret.setTbldtocomercialByIdFactura(new Tbldtocomercial(detalle.get(i).getTbldtocomercialByIdFactura()));
//					ret.setIdFactura(detalle.get(i).getIdFactura());
					System.out.println("Mensaje"+mensaje);
					System.out.println("ret"+ret.getTblimpuesto().getIdCuenta());
					det.add(ret);
				}
				Tbldtocomercial dto=new Tbldtocomercial(doc);
				dto.setEstado('1');
				System.out.println("Se va a grabar la retencion");
				acceso.grabarRetencion(det,dto);
				mensaje="Datos grabados";
			} catch (ErrorAlGrabar e) {
				//System.out.println(e.getMessage());
				mensaje="Error datos no Grabados "+e.getMessage();
				e.printStackTrace();
			}
			
		}
		return mensaje; 
	}
	
	public LinkedList<ReporteVentasEmpleadoDTO> ReporteVentasEmpleado(String fechai,String fechaf,double IVA){
		LinkedList<ReporteVentasEmpleadoDTO> list=new LinkedList<ReporteVentasEmpleadoDTO>();
		ReporteVentasEmpleadoDTO rep=new ReporteVentasEmpleadoDTO();
		List<PersonaDTO> personas=this.ListEmpleados(0,100);
		TblproductoHome tblproductohome=new TblproductoHome();
		for(int i=0;i<personas.size();i++){
			rep=new ReporteVentasEmpleadoDTO();
			rep.setEmpleado(personas.get(i));
			rep.setVentas(tblproductohome.ReporteVentasEmpleado(personas.get(i).getIdPersona(),fechai,fechaf,IVA));
			rep.setUtilidad(tblproductohome.UtilidadPorEmpleado(personas.get(i).getIdPersona(),fechai,fechaf));
			rep.setUtilidadDetalle(tblproductohome.UtilidadPorEmpleadoDetalle(personas.get(i).getIdPersona(),fechai,fechaf));
			list.add(rep);
		}
		return  list;
	}

	@Override
	public List<DtocomercialDTO> kardex(String FechaI, String FechaF, String idProducto) {
		List<DtocomercialDTO> list = new ArrayList<DtocomercialDTO>();
		TbldtocomercialHome acceso = new TbldtocomercialHome(); 
		try {
	    	String fechaI=FechaI;
			String fechaF=FechaF;
			Iterator resultado = acceso.Kardex(fechaI, fechaF, idProducto);
			while ( resultado.hasNext() ) { 
				Object[] pair = (Object[])resultado.next();
				Tbldtocomercialdetalle detalle = (Tbldtocomercialdetalle) pair[1];
				Tbldtocomercial doc = (Tbldtocomercial) pair[0];
				if(doc!=null){
					DtocomercialDTO docdto = new DtocomercialDTO();
					docdto = crearDtocomercialDTO(doc);
					list.add(docdto);
				}
			} 
	    } catch (Exception e1) {
			System.out.println("Error al convertir fecha kardex: "+e1);
		}
		return list;
	}
	
	public Date fechaServidor(){
		Date fechaSistema = new Date();
		return fechaSistema;
	}
	
	
	//+++++++++++++++++++++   LISTADO PRODUCTOS POR PROVEEDOR   implements    +++++++++++++++++++
	public  List<ReporteRetencion> listarRetenciones(String FechaI, String FechaF,int tipoTransaccion, String ruc, String nombres, String razonSocial)
		{
			List<ReporteRetencion> list = new ArrayList<ReporteRetencion>();
			TbldtocomercialHome accesoDis = new TbldtocomercialHome();
			TbldtocomercialHome accesoDis2 = new TbldtocomercialHome();//para las retenciones individuales
			Iterator resultado = accesoDis.listarRetenciones(FechaI,FechaF,tipoTransaccion,ruc,nombres, razonSocial);
			Iterator resultado2 = accesoDis.listarRetencionesIndividuales(FechaI,FechaF,tipoTransaccion,ruc,nombres, razonSocial);
			//int i = 0;
			while (resultado.hasNext() ) {
				Object[] obj = (Object[])resultado.next();
//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,date(tc.fecha),tc.idDtoComercial,
//tc.tipoTransaccion,"+tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido) "+
  
				int idPers = (Integer)obj[0];
				String nomPersona = (String)obj[1];
				String apePersona = (String)obj[2];
				String dirPersona = (String)obj[3];
				Date fecha = (Date)obj[4];
				int idDto = (Integer)obj[5];
				int tipo = (Integer)obj[6];
				int idRet = (Integer)obj[7];
				String autSri = (String)obj[8];
				//String numero = (String)obj[9];
				int numero = (Integer)obj[9];
				double total = (Double)obj[10];
				total=Math.rint(total*100)/100;
				String cedPersona = (String)obj[11];
				ReporteRetencion repRet = new ReporteRetencion();
				repRet.setIdPersona(idPers);
				repRet.setNomPersona(nomPersona);
				repRet.setApePersona(apePersona);
				repRet.setDirPersona(dirPersona);
				repRet.setFecha(fecha.toString());
				repRet.setIdDtoComercial(idDto);
				repRet.setIdRetencion(idRet);
				repRet.setAutorizacionSri(autSri);
				repRet.setNumero(numero);
				repRet.setTotalRetencion(total);
				repRet.setCedPersona(cedPersona);
				repRet.setTipoTransaccion(tipo);
				list.add(repRet);
			}
			
			while (resultado2.hasNext() ) {
				Object[] obj = (Object[])resultado2.next();
//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,date(tc.fecha),tc.idDtoComercial,
//tc.tipoTransaccion,"+tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido) "+
  
				int idPers = (Integer)obj[0];
				String nomPersona = (String)obj[1];
				String apePersona = (String)obj[2];
				String dirPersona = (String)obj[3];
				Date fecha = (Date)obj[4];
				int idDto = (Integer)obj[5];
				int tipo = (Integer)obj[6];
				int idRet = (Integer)obj[7];
				String autSri = (String)obj[8];
				//String numero = (String)obj[9];
				int numero = (Integer)obj[9];
				double total = (Double)obj[10];
				total=Math.rint(total*100)/100;
				String cedPersona = (String)obj[11];
				ReporteRetencion repRet = new ReporteRetencion();
				repRet.setIdPersona(idPers);
				repRet.setNomPersona(nomPersona);
				repRet.setApePersona(apePersona);
				repRet.setDirPersona(dirPersona);
				repRet.setFecha(fecha.toString());
				repRet.setIdDtoComercial(idDto);
				repRet.setIdRetencion(idRet);
				repRet.setAutorizacionSri(autSri);
				repRet.setNumero(numero);
				repRet.setTotalRetencion(total);
				repRet.setCedPersona(cedPersona);
				repRet.setTipoTransaccion(tipo);
				list.add(repRet);
			}
	
			
			return list;
		}
	
	public List<DtocomercialDTO> listarRetenciones(String FechaI, String FechaF,String Tipo,String impuesto,String Cedula,String codigoImpuesto) {    	
		String codigo=codigoImpuesto;
		String cedula=Cedula;
		String tipoTransaccion=Tipo;
		String tipoImpuesto=impuesto;
		TblretencionHome  accesoretencion = new TblretencionHome();
		Tblretencion tblretencioncargar = new Tblretencion();
		
		TblimpuestoHome accesoimpuesto= new TblimpuestoHome();
		Tblimpuesto tblimpuestocargar = new Tblimpuesto();
		
		TblpersonaHome accesopersona = new TblpersonaHome();
		Tblpersona tblpersonacargar= new Tblpersona();
		
		Tblbodega tblbodega= new Tblbodega();
		TblbodegaHome accesoBodega=new TblbodegaHome();
		
		Tblproducto tblproducto=new Tblproducto();
		TblproductoHome accesoproducto=new TblproductoHome();
		
		Tbldtocomercial accesocomercial=new Tbldtocomercial();
		TbldtocomercialHome acceso = new TbldtocomercialHome();
		
		//Tbldtocomercialdetalle tbldtocomercialdetalle= new Tbldtocomercialdetalle();
		TbldtocomercialdetalleHome accesodetalle=new TbldtocomercialdetalleHome();
		
		
		
		DtocomercialDTO dtocomercial= new DtocomercialDTO();
		RetencionDTO retenciondto = new RetencionDTO();
		DtoComDetalleDTO dtocomdetalle=new DtoComDetalleDTO();
		PersonaDTO personadto = new PersonaDTO();
		ImpuestoDTO impuestodto= new ImpuestoDTO();
		BodegaDTO bodegadto= new BodegaDTO();
		ProductoDTO productodto= new ProductoDTO();
		
		List<DtocomercialDTO> docs = new ArrayList<DtocomercialDTO>();
	    List<Tbldtocomercial> listado = new ArrayList<Tbldtocomercial>();
	    List<Tblretencion> listtblretencion = new ArrayList<Tblretencion>();
	    List<Tbldtocomercialdetalle>listtbldtocomercialdetalle=new ArrayList<Tbldtocomercialdetalle>();;
	    List<Tblbodega>listbodega= new ArrayList<Tblbodega>();
	    List<Tblproducto>listproducto= new ArrayList<Tblproducto>();
	  
	    
	    String fechaI=FechaI;
	    String fechaF=FechaF;
	    
	    	if(tipoImpuesto.equals("0")||tipoImpuesto.equals("1"))
	    	{
		    	if(codigo.equals("")&&cedula.equals(""))
			    {
		    		
		    		//iterador para poder recuperar la tabla retenciones y la tabla dominio
		    			//Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
		    			Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
		    	    	 		"and r.tblimpuesto.tipo ='"+tipoImpuesto+"'");
		    	    	docs = new ArrayList<DtocomercialDTO>();
		    	    	System.out.println(tipoImpuesto);
		    	    	Tbldtocomercial tbldtocomercial;
		    	    	if(iterator.hasNext()){
		    	    	while (iterator.hasNext()) {
		    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
		    	    		DtocomercialDTO doc = new DtocomercialDTO();
			    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
			    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
			    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
			    				
				    		System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
							listtblretencion = new ArrayList<Tblretencion>();
							listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
							
							tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
							
							
							personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
							
							Set<DtoComDetalleDTO> dtocomdetalledto =  new HashSet<DtoComDetalleDTO>();
							
							System.out.println("TAMAÑO LISTA RETENCIONES: "+listtblretencion.size());
							
							listtbldtocomercialdetalle=accesodetalle.findByQuery("where u.tbldtocomercial.idDtoComercial='"+tbldtocomercial.getIdDtoComercial()+"'");
							for(Tbldtocomercialdetalle tbldtocomercialdetalle: listtbldtocomercialdetalle){
								bodegadto=new BodegaDTO();
								bodegadto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
								bodegadto.setIdBodega(tbldtocomercialdetalle.getTblbodega().getIdBodega());
								productodto= new ProductoDTO();
								productodto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
								productodto.setEstablecimiento(tbldtocomercial.getEstablecimiento());
								productodto.setIdProducto(tbldtocomercialdetalle.getTblproducto().getIdProducto());
								dtocomdetalle= new DtoComDetalleDTO(tbldtocomercialdetalle.getIdDtoComercialDetalle(), bodegadto, productodto, tbldtocomercialdetalle.getCantidad(), tbldtocomercialdetalle.getPrecioUnitario(), tbldtocomercialdetalle.getDepartamento(), 
//										tbldtocomercialdetalle.getImpuesto(), 
										tbldtocomercialdetalle.getTotal(), tbldtocomercialdetalle.getDesProducto());				
								dtocomdetalledto.add(dtocomdetalle);
							}
								
								tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
					
								System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
								
								impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
								retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
								retenciondto.setImpuesto(impuestodto);
								retenciones.add(retenciondto);
								
								System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());
								
								System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
								doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
								doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
								doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
								doc.setTbldtocomercialdetalles(dtocomdetalledto);
								doc.setTblpersonaByIdPersona(personadto);
								doc.setTblretencionsForIdFactura(retenciones);
								doc.setFecha(tbldtocomercial.getFecha().toString());
								doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
								doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
								doc.setNumCompra(tbldtocomercial.getNumCompra());
								docs.add(doc);					
				    	}
				    	
				    }
			    }
			    else if(codigo.equals("")){
			    		Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//				    	Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
			    	    	 		"and x.tblpersonaByIdPersona.cedulaRuc like'"+cedula+"' and r.tblimpuesto.tipo ='"+tipoImpuesto+"'");
			    	    	docs = new ArrayList<DtocomercialDTO>();
			    	    	System.out.println(tipoImpuesto);
			    	    	Tbldtocomercial tbldtocomercial;
			    	    	if(iterator.hasNext()){
			    	    	while (iterator.hasNext()) {
			    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
			    	    		DtocomercialDTO doc = new DtocomercialDTO();
				    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
				    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
				    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
				    				listtblretencion = new ArrayList<Tblretencion>();
									
				    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
									tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
									personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
									
									System.out.println("TAMAÑO LISTA RETENCIONES: "+listtblretencion.size());
										
										tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
										System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
										
										impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
										retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
										retenciondto.setImpuesto(impuestodto);
										retenciones.add(retenciondto);
										System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());
									
									System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
									doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
									doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
									doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
									doc.setTblpersonaByIdPersona(personadto);
									doc.setTblretencionsForIdFactura(retenciones);
									
									doc.setFecha(tbldtocomercial.getFecha().toString());
									doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
									doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
									doc.setNumCompra(tbldtocomercial.getNumCompra());
									docs.add(doc);
				    	 
						    	}
						    }
				    	}
				    	else if(cedula.equals("")){
				    	
				    		
				    		//iterador para poder recuperar la tabla retenciones y la tabla dominio
				    Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//				    			Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
				    	    	 		"and r.tblimpuesto.codigo ='"+codigo+"' and r.tblimpuesto.tipo ='"+tipoImpuesto+"'");
				    	    	docs = new ArrayList<DtocomercialDTO>();
				    	    	System.out.println(tipoImpuesto);
				    	    	Tbldtocomercial tbldtocomercial;
				    	    	if(iterator.hasNext()){
				    	    	while (iterator.hasNext()) {
				    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
				    	    		DtocomercialDTO doc = new DtocomercialDTO();
					    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
					    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
					    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
					    				
					    				System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
		    		    				listtblretencion = new ArrayList<Tblretencion>();
		    		    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
		    		    				tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
		    		    				personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
		    					
					    				tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
	    		    					System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
	    						
	    		    					impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
	    		    					retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
	    		    					retenciondto.setImpuesto(impuestodto);
	    		    					retenciones.add(retenciondto);
	    		    					System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());

		    		    				System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
		    							doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
		    							doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
		    		    				doc.setTblpersonaByIdPersona(personadto);
		    		    				doc.setTblretencionsForIdFactura(retenciones);
		    					
		    		    				doc.setFecha(tbldtocomercial.getFecha().toString());
		    		    				doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
		    		    				doc.setNumCompra(tbldtocomercial.getNumCompra());
		    		    				docs.add(doc);
				    	    		}
				    	    	}

				    		}	
				    		else
				    		{
				    			Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//				    			Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
				    					"and x.tblpersonaByIdPersona.cedulaRuc like'"+cedula+"' and r.tblimpuesto.codigo ='"+codigo+"' and r.tblimpuesto.tipo ='"+tipoImpuesto+"'");
				    	    	docs = new ArrayList<DtocomercialDTO>();
				    	    	
				    	    	Tbldtocomercial tbldtocomercial;
				    	    	if(iterator.hasNext()){
				    	    	while (iterator.hasNext()) {
				    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
				    	    		DtocomercialDTO doc = new DtocomercialDTO();
					    			Object[] pair = (Object[])iterator.next();
					    				tbldtocomercial = (Tbldtocomercial) pair[0];
					    				tblretencioncargar = (Tblretencion) pair[1];
					    				
					    				System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
		    		    				listtblretencion = new ArrayList<Tblretencion>();
		    		    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
		    		    				tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
		    		    				personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
		    					
					    				tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
	    		    					System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
	    						
	    		    					impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
	    		    					retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
	    		    					retenciondto.setImpuesto(impuestodto);
	    		    					retenciones.add(retenciondto);
	    		    					System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());

		    		    				System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
		    							doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
		    							doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
		    		    				doc.setTblpersonaByIdPersona(personadto);
		    		    				doc.setTblretencionsForIdFactura(retenciones);
		    					
		    		    				doc.setFecha(tbldtocomercial.getFecha().toString());
		    		    				doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
		    		    				doc.setNumCompra(tbldtocomercial.getNumCompra());
		    		    				docs.add(doc);
				    	    		}
				    	    	}

				    		}
	    	}
	    	else{
	    		
	    		if(codigo.equals("")&&cedula.equals(""))
			    {
		    		
		    		//iterador para poder recuperar la tabla retenciones y la tabla dominio
	        Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//		    			Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"'"
	        		"");
		    	    	docs = new ArrayList<DtocomercialDTO>();
		    	    	System.out.println(tipoImpuesto);
		    	    	Tbldtocomercial tbldtocomercial;
		    	    	if(iterator.hasNext()){
		    	    	while (iterator.hasNext()) {
		    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
		    	    		DtocomercialDTO doc = new DtocomercialDTO();
			    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
			    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
			    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
			    				
				    		System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
							listtblretencion = new ArrayList<Tblretencion>();
							listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
							
							tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
							
							
							personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
							
							Set<DtoComDetalleDTO> dtocomdetalledto =  new HashSet<DtoComDetalleDTO>();
							
							System.out.println("TAMAÑO LISTA RETENCIONES: "+listtblretencion.size());
							
							listtbldtocomercialdetalle=accesodetalle.findByQuery("where u.tbldtocomercial.idDtoComercial='"+tbldtocomercial.getIdDtoComercial()+"'");
							for(Tbldtocomercialdetalle tbldtocomercialdetalle: listtbldtocomercialdetalle){
								bodegadto=new BodegaDTO();
								bodegadto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
								bodegadto.setIdBodega(tbldtocomercialdetalle.getTblbodega().getIdBodega());
								productodto= new ProductoDTO();
								productodto.setIdProducto(tbldtocomercialdetalle.getTblproducto().getIdProducto());
								dtocomdetalle= new DtoComDetalleDTO(tbldtocomercialdetalle.getIdDtoComercialDetalle(), bodegadto, productodto, tbldtocomercialdetalle.getCantidad(), tbldtocomercialdetalle.getPrecioUnitario(), tbldtocomercialdetalle.getDepartamento(), 
//										tbldtocomercialdetalle.getImpuesto(),
										tbldtocomercialdetalle.getTotal(), tbldtocomercialdetalle.getDesProducto());				
								dtocomdetalledto.add(dtocomdetalle);
							}
								
								tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
					
								System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
								
								impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
								retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
								retenciondto.setImpuesto(impuestodto);
								retenciones.add(retenciondto);
								
								System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());
								
								System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
								doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
								doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
								doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
								doc.setTbldtocomercialdetalles(dtocomdetalledto);
								doc.setTblpersonaByIdPersona(personadto);
								doc.setTblretencionsForIdFactura(retenciones);
								doc.setFecha(tbldtocomercial.getFecha().toString());
								doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
								doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
								doc.setNumCompra(tbldtocomercial.getNumCompra());
								docs.add(doc);
								
							
						
				    	}
				    	
				    }
			    }
			    else if(codigo.equals("")){
			Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//				    	Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
			    	    	 		"and x.tblpersonaByIdPersona.cedulaRuc like'"+cedula+"'");
			    	    	docs = new ArrayList<DtocomercialDTO>();
			    	    	System.out.println(tipoImpuesto);
			    	    	Tbldtocomercial tbldtocomercial;
			    	    	if(iterator.hasNext()){
			    	    	while (iterator.hasNext()) {
			    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
			    	    		DtocomercialDTO doc = new DtocomercialDTO();
				    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
				    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
				    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
				    				listtblretencion = new ArrayList<Tblretencion>();
									
				    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
									tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
									personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
									
									System.out.println("TAMAÑO LISTA RETENCIONES: "+listtblretencion.size());
										
										tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
										System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
										
										impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
										retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
										retenciondto.setImpuesto(impuestodto);
										retenciones.add(retenciondto);
										System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());
									
									System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
									doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
									doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
									doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
									doc.setTblpersonaByIdPersona(personadto);
									doc.setTblretencionsForIdFactura(retenciones);
									
									doc.setFecha(tbldtocomercial.getFecha().toString());
									doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
									doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
									doc.setNumCompra(tbldtocomercial.getNumCompra());
									docs.add(doc);
				    	 
						    	}
						    }
				    	}
				    	else if(cedula.equals("")){
				    	
				    		
				    		//iterador para poder recuperar la tabla retenciones y la tabla dominio
				    Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +	
//				    			Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
				    	    	 		"and r.tblimpuesto.codigo ='"+codigo+"'");
				    	    	docs = new ArrayList<DtocomercialDTO>();
				    	    	System.out.println(tipoImpuesto);
				    	    	Tbldtocomercial tbldtocomercial;
				    	    	if(iterator.hasNext()){
				    	    	while (iterator.hasNext()) {
				    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
				    	    		DtocomercialDTO doc = new DtocomercialDTO();
					    			Object[] pair = (Object[])iterator.next();//recupera el iterador 
					    				tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
					    				tblretencioncargar = (Tblretencion) pair[1];//recupera tbldtocomercial en posicion 1
					    				
					    				System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
		    		    				listtblretencion = new ArrayList<Tblretencion>();
		    		    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
		    		    				tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
		    		    				personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
		    					
					    				tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
	    		    					System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
	    						
	    		    					impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
	    		    					retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
	    		    					retenciondto.setImpuesto(impuestodto);
	    		    					retenciones.add(retenciondto);
	    		    					System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());

		    		    				System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
		    							doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
		    							doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
		    		    				doc.setTblpersonaByIdPersona(personadto);
		    		    				doc.setTblretencionsForIdFactura(retenciones);
		    					
		    		    				doc.setFecha(tbldtocomercial.getFecha().toString());
		    		    				doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
		    		    				doc.setNumCompra(tbldtocomercial.getNumCompra());
		    		    				docs.add(doc);
				    	    		}
				    	    	}

				    		}	
				    		else
				    		{
				    Iterator iterator=acceso.getList("join x.tblretencionsForIdFactura r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
//				    			Iterator iterator=acceso.getList("join x.tblretencions r where x.estado='1' and x.fecha>='"+fechaI+"' and x.fecha<='"+fechaF+"' and x.tipoTransaccion='"+tipoTransaccion+"' " +
				    					"and x.tblpersonaByIdPersona.cedulaRuc like'"+cedula+"' and r.tblimpuesto.codigo ='"+codigo+"'");
				    	    	docs = new ArrayList<DtocomercialDTO>();
				    	    	
				    	    	Tbldtocomercial tbldtocomercial;
				    	    	if(iterator.hasNext()){
				    	    	while (iterator.hasNext()) {
				    	    		Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>();
				    	    		DtocomercialDTO doc = new DtocomercialDTO();
					    			Object[] pair = (Object[])iterator.next();
					    				tbldtocomercial = (Tbldtocomercial) pair[0];
					    				tblretencioncargar = (Tblretencion) pair[1];
					    				
					    				System.out.println("ID DEL DOCUMENTO: "+tbldtocomercial.getIdDtoComercial());
		    		    				listtblretencion = new ArrayList<Tblretencion>();
		    		    				listtblretencion=accesoretencion.getListRetencion(tbldtocomercial.getIdDtoComercial());
		    		    				tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
		    		    				personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
		    					
					    				tblimpuestocargar=accesoimpuesto.findById(tblretencioncargar.getTblimpuesto().getIdImpuesto());
	    		    					System.out.println("ID DE LA IMPUESTO: "+tblimpuestocargar.getIdImpuesto()+" "+tblimpuestocargar.getCodigo());
	    						
	    		    					impuestodto=new ImpuestoDTO(tblimpuestocargar.getNombre(), tblimpuestocargar.getCodigo(), tblimpuestocargar.getRetencion(), tblimpuestocargar.getIdPlan(), tblimpuestocargar.getIdCuenta(),tblimpuestocargar.getCodigoElectronico());
	    		    					retenciondto=new RetencionDTO(tblretencioncargar.getIdRetencion(), tblretencioncargar.getBaseImponible(), tblretencioncargar.getNumRealRetencion(), tblretencioncargar.getAutorizacionSri(), tblretencioncargar.getEstado(), tblretencioncargar.getValorRetenido(), tblretencioncargar.getNumero());
	    		    					retenciondto.setImpuesto(impuestodto);
	    		    					retenciones.add(retenciondto);
	    		    					System.out.println("codigo impuesto: "+retenciondto.getImpuesto().getCodigo());

		    		    				System.out.println("numero real tran: "+tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setIdEmpresa(tbldtocomercial.getIdEmpresa());
		    							doc.setEstablecimiento(tbldtocomercial.getEstablecimiento());
		    							doc.setPuntoEmision(tbldtocomercial.getPuntoEmision());
		    		    				doc.setTblpersonaByIdPersona(personadto);
		    		    				doc.setTblretencionsForIdFactura(retenciones);
		    					
		    		    				doc.setFecha(tbldtocomercial.getFecha().toString());
		    		    				doc.setNumRealTransaccion(tbldtocomercial.getNumRealTransaccion());
		    		    				doc.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
		    		    				doc.setNumCompra(tbldtocomercial.getNumCompra());
		    		    				docs.add(doc);
				    	    		}
				    	    	}

				    		}
	    		}
	    				
          
	    return docs;
	}

	public List<RetencionDetalle> listarRetTotales(int tipoTransaccion,LinkedList<Integer> idImpuesto, String ruc,String nombres, String razonSocial,String fechaI, String fechaF)
	{
		List<RetencionDetalle> list = new ArrayList<RetencionDetalle>();
		TbldtocomercialHome accesoDis = new TbldtocomercialHome();
		TbldtocomercialHome accesoDis2 = new TbldtocomercialHome();
	for(int asd=0;asd<idImpuesto.size();asd++)
	{	
		int num=0;//Para la primera vez que entra a cada tipo de impuesto
		int numEscogido=idImpuesto.get(asd);
		Iterator resultado = accesoDis.listarRetTotales(tipoTransaccion, numEscogido,ruc,nombres,razonSocial,fechaI,fechaF);
		Iterator resultado2 = accesoDis.listarRetTotalesIndividuales(tipoTransaccion, numEscogido,ruc,nombres,razonSocial,fechaI,fechaF);
		//int i = 0;
		double total=0.0;
		while (resultado.hasNext() ) 
		{
			Object[] obj = (Object[])resultado.next();
			int tipTra=0;
			int idImp=0;
			total=0.0;
//select tc.tipoTransaccion,tr.tblimpuesto.idImpuesto,sum(tr.valorRetenido)
			try
			{
				tipTra = (Integer)obj[0];
				idImp = (Integer)obj[1];
				total = (Double)obj[2];
			}
		 catch (Exception e) 
		 {
			 tipTra=0;
			 idImp=0;
			 total=0.0;
		 }
	//		RetencionDetalle repRet = new RetencionDetalle();
	//		repRet.setTipoTransaccion(tipTra);
	//		repRet.setIdImpuesto(idImp);
	//		repRet.setTotalRetencion(total);
	//		list.add(repRet);
	//		num++;
		}//fin while
		
		
		while (resultado2.hasNext() ) 
		{
			Object[] obj = (Object[])resultado2.next();
			int tipTra=0;
			int idImp=0;
			//double total=0.0;
			try
			{
				tipTra = (Integer)obj[0];
				idImp = (Integer)obj[1];
				total = total+(Double)obj[2];
			}
		 catch (Exception e) 
		 {
			 tipTra=0;
			 idImp=0;
			 //total=0.0;
		 }
			RetencionDetalle repRet = new RetencionDetalle();
			repRet.setTipoTransaccion(tipTra);
			repRet.setIdImpuesto(idImp);
			repRet.setTotalRetencion(total);
			list.add(repRet);
			num++;
		}
	}//fin For	
		return list;
	}
	
	public List<RetencionDTO> listarDetallesRetencion(int numDto)
	{
		TblretencionHome acceso = new TblretencionHome(); 
		List<Tblretencion> listado = null;
		List<RetencionDTO> listadoDTO;
		//try {
			listado=acceso.getListRetencion(numDto);
			listadoDTO = new ArrayList<RetencionDTO>();
            if (listado != null) {
                for (Tblretencion TipoRet : listado) {
                    listadoDTO.add(crearTipoRetencion(TipoRet));
                }
            }
		/*} catch (RuntimeException re) {
			throw re;
		}*/
		return listadoDTO;

	}
	
	public RetencionDTO crearTipoRetencion(Tblretencion tipoRet){
		RetencionDTO retDTO = new RetencionDTO(tipoRet.getIdRetencion(),tipoRet.getBaseImponible(), 
				tipoRet.getNumRealRetencion(), tipoRet.getAutorizacionSri(),tipoRet.getEstado(), 
				tipoRet.getValorRetenido(),tipoRet.getNumero());
		
		ImpuestoDTO impDTO = new ImpuestoDTO();
		impDTO.setIdImpuesto(tipoRet.getTblimpuesto().getIdImpuesto());
		retDTO.setImpuesto(impDTO);
		return retDTO;
	}
	
	
	

	public  List<ProductoProveedor> listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax,
			String idProveedor, String idProducto)
	{
		List<ProductoProveedor> list = new ArrayList<ProductoProveedor>();
		TbldtocomercialHome accesoDis = new TbldtocomercialHome();
		Iterator resultado = accesoDis.listarProductosProveedor(FechaI, FechaF, StockMin, StockMax, idProveedor, idProducto);
		//int i = 0;
		while (resultado.hasNext() ) {
			Object[] obj = (Object[])resultado.next();
			//td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, 
			//pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento			//int, string,double,double,double,double,int,string, string,timestamp
			int Producto = (Integer)obj[0];
			String nomProducto = (String)obj[1];
			double cantidad = (Double)obj[2];
			double stock= (Double)obj[3];
			double precioCompra = (Double)obj[4];
			double precioVenta = (Double)obj[5];
			int idPersona = (Integer)obj[6];
//			String proveedor  = (String)obj[7]+" "+(String)obj[8];
			String proveedor  = (String)obj[8];
			Date fechaCompra = (Date)obj[9];
//			double impuesto = (Double)obj[10];
			
			String[] ivas=((String)obj[10]).split(",");
			int i1=0;
			double impuestoPorc=1.0;
			double impuestoValor=0.0;
			//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
			for (String ivaS:ivas){
				i1=i1+1;
				impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
//				impuestoValor=impuestoValor+((Double.parseDouble(cargaProducto.getAttributeAsString("cantidad"))*cargaProducto.getAttributeAsDouble("Precio")
//						+impuestoValor)*(Double.parseDouble(ivaS)/100));
			}
			
			double descuento = (Double)obj[11];
			
			Double precioCompraIVA=precioCompra*(1-(descuento/100));
//			precioCompraIVA=precioCompraIVA*(1+(impuesto/100));
			precioCompraIVA=precioCompraIVA*(impuestoPorc);
			
			Double precioVentaIVA=precioVenta;//*(1+(impuesto/100));
			//precioVentaIVA=precioVentaIVA/(1+(descuento/100));
			
			
			precioCompraIVA=Math.rint(precioCompraIVA*100)/100;//redondear a 2 decimales
			
			precioVentaIVA=Math.rint(precioVentaIVA*100)/100;//redondear a 2 decimales
			
			ProductoProveedor proProveedor = new ProductoProveedor();
			
			proProveedor.setIdProducto(Producto);
			proProveedor.setNomProducto(nomProducto);
			proProveedor.setCantidad(cantidad);
			proProveedor.setStock(stock);
			proProveedor.setPrecioCompra(precioCompraIVA);
			proProveedor.setPrecioVenta(precioVentaIVA);
			proProveedor.setIdPersona(idPersona);
			proProveedor.setProveedor(proveedor);
			proProveedor.setFechaCompra(fechaCompra.toString());
			
			list.add(proProveedor);
		}
		return list;
	}

	public  List<ProductoCliente> listarProductosCliente(String FechaI, String FechaF,int StockMin, int StockMax,
			String idCliente, String idProducto)
	{
		List<ProductoCliente> list = new ArrayList<ProductoCliente>();
		TbldtocomercialHome accesoDis = new TbldtocomercialHome();
		Iterator resultado = accesoDis.listarProductosCliente(FechaI, FechaF, StockMin, StockMax, idCliente, idProducto);
		//int i = 0;
		while (resultado.hasNext() ) {
			Object[] obj = (Object[])resultado.next();
			//td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, 
			//pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento			//int, string,double,double,double,double,int,string, string,timestamp
			int Producto = (Integer)obj[0];
			String nomProducto = (String)obj[1];
			double cantidad = (Double)obj[2];
			double stock= (Double)obj[3];
			double precioCompra = (Double)obj[4];
			double precioVenta = (Double)obj[5];
			int idPersona = (Integer)obj[6];
//			String cliente  = (String)obj[7]+" "+(String)obj[8];
			String cliente  = (String)obj[8];
			Date fechaCompra = (Date)obj[9];
//			double impuesto = (Double)obj[10];
			
			String[] ivas=((String)obj[10]).split(",");
			int i1=0;
			double impuestoPorc=1.0;
			double impuestoValor=0.0;
			//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
			for (String ivaS:ivas){
				i1=i1+1;
				impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
//				impuestoValor=impuestoValor+((Double.parseDouble(cargaProducto.getAttributeAsString("cantidad"))*cargaProducto.getAttributeAsDouble("Precio")
//						+impuestoValor)*(Double.parseDouble(ivaS)/100));
			}
			
			double descuento = (Double)obj[11];
			
			Double precioCompraIVA=precioCompra*(1-(descuento/100));
//			precioCompraIVA=precioCompraIVA*(1+(impuesto/100));
			precioCompraIVA=precioCompraIVA*(impuestoPorc);
			
			Double precioVentaIVA=precioVenta;//*(1+(impuesto/100));
			//precioVentaIVA=precioVentaIVA/(1+(descuento/100));
			
			
			precioCompraIVA=Math.rint(precioCompraIVA*100)/100;//redondear a 2 decimales
			
			precioVentaIVA=Math.rint(precioVentaIVA*100)/100;//redondear a 2 decimales
			
			ProductoCliente proCliente = new ProductoCliente();
			
			proCliente.setIdProducto(Producto);
			proCliente.setNomProducto(nomProducto);
			proCliente.setCantidad(cantidad);
			proCliente.setStock(stock);
			proCliente.setPrecioCompra(precioCompraIVA);
			proCliente.setPrecioVenta(precioVentaIVA);
			proCliente.setIdPersona(idPersona);
			proCliente.setCliente(cliente);
			proCliente.setFechaCompra(fechaCompra.toString());
			
			list.add(proCliente);
		}
		return list;
	}

	public  List<ProductoTipoPrecio> listarTipoPrecio(int codProducto) {
				List<ProductoTipoPrecio> list = new ArrayList<ProductoTipoPrecio>();
				TblproductoTipoprecioHome acceso = new TblproductoTipoprecioHome();
				Iterator resultado = acceso.listarTipoprecio(codProducto);//ListEmpleados(ini, fin);
				//int i = 0;
				while (resultado.hasNext() ) {
					Object[] obj = (Object[])resultado.next();
					int idProducto = (Integer)obj[0];
					double porcentaje = (Double)obj[1];
					int idTipo = (Integer)obj[2];
					String tipo  = (String)obj[3];
					int orden = (Integer)obj[4];
					ProductoTipoPrecio proTip = new ProductoTipoPrecio();
					proTip.setIdProducto(idProducto);
					proTip.setPorcentaje(porcentaje);
					proTip.setIdTipo(idTipo);
					proTip.setTipo(tipo);
					proTip.setOrden(orden);
					//System.out.println(idProducto+" "+porcentaje+" "+idTipo+" "+tipo);
					list.add(proTip);
				}
				return list;
			}
	
	public  List<TblproductoMultiImpuestoDTO> listarProdMultiImp(int codProducto) {
		List<TblproductoMultiImpuestoDTO> list = new ArrayList<TblproductoMultiImpuestoDTO>();
		TblproductoMultiImpuestoHome acceso = new TblproductoMultiImpuestoHome();
		Iterator resultado = acceso.listarProdMultImp(codProducto);//ListEmpleados(ini, fin);
		//int i = 0;
		while (resultado.hasNext() ) {
			Object[] obj = (Object[])resultado.next();
			int idprodMultiImp = (Integer)obj[0];
			int idProducto = (Integer)obj[1];
			int idImpuesto = (Integer)obj[2];
			ProductoDTO prod= new ProductoDTO();
			prod.setIdProducto(idProducto);
			TblmultiImpuestoDTO multImp= new TblmultiImpuestoDTO();
			multImp.setIdmultiImpuesto(idImpuesto);
			TblproductoMultiImpuestoDTO proTip = new TblproductoMultiImpuestoDTO(idprodMultiImp,prod,multImp);

			//System.out.println(idProducto+" "+porcentaje+" "+idTipo+" "+tipo);
			list.add(proTip);
		}
		return list;
	}
	
	public  List<ProductoTipoPrecioDTO> listarTipoPrecioD(int codProducto) {
		List<ProductoTipoPrecioDTO> list = new ArrayList<ProductoTipoPrecioDTO>();
		TblproductoTipoprecioHome acceso = new TblproductoTipoprecioHome();
		TblproductoMultiImpuestoHome accesoMI = new TblproductoMultiImpuestoHome();
		List<TblproductoTipoprecio> resultado = acceso.listarTipoprecioD(codProducto);//ListEmpleados(ini, fin);
		//int i = 0;
		System.out.println(resultado.size()+" preciosD");
		Iterator res=resultado.iterator();
		while (res.hasNext() ) {
			TblproductoTipoprecio obj = (TblproductoTipoprecio)res.next();
			ProductoTipoPrecioDTO proTip = new ProductoTipoPrecioDTO();
			ProductoDTO prodDto=crearProductoDTO(obj.getTblproducto());
					List<TblproductoMultiImpuesto> tblmultiImp=accesoMI.getListProMultImp(codProducto);
			Set<TblproductoMultiImpuesto> multiImpuestos= new HashSet(0);
			for (TblproductoMultiImpuesto tblmi:tblmultiImp){
				multiImpuestos.add(tblmi);
			}
			prodDto.setTblmultiImpuesto(crearSetProductoMultiImpuestoDTO(multiImpuestos));
			
			proTip.setTblproducto(prodDto);
			proTip.setPorcentaje(obj.getPorcentaje());
			proTip.setTbltipoprecio(crearTipoprecioReg(obj.getTbltipoprecio()));
			//System.out.println(idProducto+" "+porcentaje+" "+idTipo+" "+tipo);
			list.add(proTip);
		}
		return list;
	}

	public String grabarProductoTipoPrecio(LinkedList <ProductoTipoPrecioDTO> proTip)
				throws IllegalArgumentException {
			TblproductoTipoprecioHome acceso = new TblproductoTipoprecioHome();
			LinkedList <TblproductoTipoprecio> tblproList= new LinkedList<TblproductoTipoprecio>();
			String mensaje = "";
			for(int i=0;i<proTip.size();i++)
			{	
				TblproductoTipoprecio tbtProTip= new TblproductoTipoprecio(proTip.get(i));
				tblproList.add(tbtProTip);
			}

			try
			{
				acceso.grabarProductoTipoPrecio(tblproList); 
				mensaje="PRECIOS MODIFICADOS EXITOSAMENTE";
				}catch (ErrorAlGrabar e) {
				e.printStackTrace();
				mensaje=e.getMessage();
			}
			
			return mensaje;	
		}
	public String asignarPuntos(ClienteDTO clientedto) {
		  String mensaje = "";
		  TblclienteHome accesoCliente= new TblclienteHome();
		  Tblcliente tblcliente= new Tblcliente();
		  //try{
		   try{
		    tblcliente=accesoCliente.findByNombrePuntos(String.valueOf(clientedto.getIdCliente()));
		    if(tblcliente!= null){
		     tblcliente.setPuntos(tblcliente.getPuntos()+clientedto.getPuntos());
		     ClienteDTO cli =new ClienteDTO();
		/*     cli.setCupoCredito(tblcliente.getCupoCredito());
		     cli.setMontoCredito(tblcliente.getMontoCredito());
		     cli.setIdCliente(tblcliente.getIdCliente());
		     cli.setPuntos(tblcliente.getPuntos()+clientedto.getPuntos());
		     cli.setCodigoTarjeta(tblcliente.getCodigoTarjeta());*/
		    }else{
		     //persona=null;
		    }
		   }catch(Exception e){
		  //  persona=null;
		   }
		   
		   try {
		    accesoCliente.modificar(tblcliente);
		    mensaje="Puntos asignados correctamente";
		   } catch (ErrorAlGrabar e) {
		    // TODO Auto-generated catch block
		    mensaje = e.getMessage();
		   }
		  
		  return mensaje;
	}
	//#######################################################################
	public String grabarAnticipo(LinkedList<DtocomercialDTO> listAnticiposDTO)
	{
		System.out.println("listAnticiposDTO.size() "+listAnticiposDTO.size());
		String mensaje = "";
		LinkedList<Tbldtocomercial> listAnticipos= new LinkedList<Tbldtocomercial>();
		 try 
	     {
			 int antEstado=listAnticiposDTO.size();//cuando tenga este valor pondremos como estado 1
			 //y no 5 a los anticipos
			for(int i=0; i<antEstado; i++)
			{
				//Extraigo cliente y vendedor
				//listAnticipos= new LinkedList<Tbldtocomercial>();
				TblpersonaHome accesoP = new TblpersonaHome();
				Tblpersona cliente = accesoP.load(listAnticiposDTO.get(i).getTblpersonaByIdPersona().getIdPersona());
				Tblpersona vendedor = accesoP.load(listAnticiposDTO.get(i).getTblpersonaByIdVendedor().getIdPersona());
				Tblpersona facturaPor = accesoP.load(listAnticiposDTO.get(i).getTblpersonaByIdFacturaPor().getIdPersona());
				Tbldtocomercial documento = new Tbldtocomercial(listAnticiposDTO.get(i), cliente, vendedor, facturaPor);		
				TblpagoDTO pagoDTO = new TblpagoDTO();	
				DtocomercialTbltipopagoDTO tipopagoDTO = new DtocomercialTbltipopagoDTO();
				Set<Tbldtocomercialdetalle> detalles = new HashSet<Tbldtocomercialdetalle>();
				Set<Tblretencion> retenciones = new HashSet<Tblretencion>();
				//Set<TbldtocomercialTbltipopago> pagosAsociados = new HashSet<TbldtocomercialTbltipopago>();
				Set<Tblpago> pagos = new HashSet<Tblpago>();
				Set<Tblproducto> productos = new HashSet<Tblproducto>();
				Set<TblproductoTblbodega> prodBods = new HashSet<TblproductoTblbodega>();
				Set<DtoComDetalleDTO> detallesDTO = listAnticiposDTO.get(i).getTbldtocomercialdetalles();
				Set<RetencionDTO> listaRetenciones = listAnticiposDTO.get(i).getTblretencionsForIdFactura();
				System.out.println("FechaLixta: "+listAnticiposDTO.get(i).getFecha()+" Exp "+listAnticiposDTO.get(i).getExpiracion());
				if(i<antEstado-1)
				{	
					documento.setEstado('5');
					documento.setNumRealTransaccion(listAnticiposDTO.get(i).getNumRealTransaccion());
					documento.setIdDtoComercial(listAnticiposDTO.get(i).getIdDtoComercial());
				}
				else
				{
					documento.setEstado('1');
				}	
//				documento.setTblretencions(retenciones);
				documento.setTblretencionsForIdFactura(retenciones);
				documento.setTbldtocomercialdetalles(detalles);
				documento.setTblpagos(pagos);
				documento.setTbldtocomercialTbltipopagos(null);
				
				listAnticipos.add(documento);
				
				System.out.println("No agrega verso listAnticipos.size()"+listAnticipos.size());
				//System.out.println("ESTADO DEL DOCUMENTO: " + listAnticipos);
				
			}	
			try 
			{
				System.out.println("Entrando al try primera parte");
				TbldtocomercialHome acceso = new TbldtocomercialHome();
				System.out.println("Entrando al try "+listAnticipos.size());
				acceso.grabarPAnticipos(listAnticipos);
				mensaje+="Grabado con exito";
			} catch (ErrorAlGrabar e){
					mensaje += e+"...";
					System.out.println("Entrando al catch");
			}
		}catch(Exception e)
		{
				mensaje+="Error: "+e+" ------   ";
		}
			return mensaje;
		}
	//#######################################################################	
	/**
	 * Funci�n permite modificar el costo en los  detalles de una factura 
	 * @param det DtoComDetalleDTO detalles a modificar
	 * @return String
	 */

	public List<TmpAuxresultadosDTO> ejecutarEstado (String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD)throws Exception{	
		Connection conn=null;
		ResultSet resultset = null;
		TmpAuxresultadosDTO estados = null;
		List<TmpAuxresultadosDTO> listaestado = new ArrayList<TmpAuxresultadosDTO>(0);
		CallableStatement proc= null;
		
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		Tblplancuenta tblplancuenta = new Tblplancuenta();
		tblplancuenta=tblplancuentahome.findById(Integer.valueOf(planCuenta));
		
		try {
		System.out.println("Nabucodonosor llego al procedimiento");
		Class.forName("com.mysql.jdbc.Driver");
		
		conn = DriverManager.getConnection ("jdbc:mysql://localhost:3306/"+NombreBD, UsuarioBD, PasswordBD);
		
		byte[] pdfbyte = null;

		//case 1: 
		//default: 2-comprobacion, 3-general, 4-estadoresultados
		System.out.println(param.get("desde")+"-"+param.get("hasta")+"-"+tblplancuenta.getPeriodo());
		
		proc = conn.prepareCall("{call "+fileName+" (?,?,?)}");//llamamos el procedeimietno alamcenado
					
				proc.setString(1, param.get("desde").toString());//Tipo String
				proc.setString(2, param.get("hasta").toString());//Tipo String
				
				proc.setString(3, tblplancuenta.getPeriodo());//Tipo String
				resultset=proc.executeQuery();

				while(resultset.next())
				{
					System.out.println("Entra ResultSet");
					String tmpcodigo =resultset.getString("tmpcodigo");
					String tmpcuenta =resultset.getString("tmpcuenta");
					BigDecimal resta=resultset.getBigDecimal("resta");
					estados= new TmpAuxresultadosDTO(tmpcodigo, tmpcuenta,resta);
					listaestado.add(estados);
					System.out.println("Codigo:"+tmpcodigo+" "+tmpcuenta+" "+resta);
				}
				return listaestado;
				
		} catch (SQLException e1) {
			conn.rollback();
			e1.printStackTrace();
		}finally{
			if(conn != null){
				conn.close();	
			}
			if(proc!=null){
				proc.close();				
			}		
		}
		return listaestado;

	}
	public List<TmpAuxgeneralDTO> ejecutarBalance (String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD )throws Exception{	
		Connection conn=null;
		ResultSet resultset = null;
		TmpAuxgeneralDTO balance = null;
		List<TmpAuxgeneralDTO> listabalance = new ArrayList<TmpAuxgeneralDTO>(0);
		CallableStatement proc= null;
		
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		Tblplancuenta tblplancuenta = new Tblplancuenta();
		tblplancuenta=tblplancuentahome.findById(Integer.valueOf(planCuenta));
		try {
		System.out.println("Nabucodonosor llego al procedimiento");
		Class.forName("com.mysql.jdbc.Driver");
		
		conn = DriverManager.getConnection ("jdbc:mysql://localhost:3306/"+NombreBD, UsuarioBD, PasswordBD);
		
		byte[] pdfbyte = null;

		//case 1: 
		//default: 2-comprobacion, 3-general, 4-estadoresultados
		System.out.println(param.get("desde")+"-"+param.get("hasta")+"-"+param.get("Periodo"));
		
		proc = conn.prepareCall("{call "+fileName+" (?,?,?)}");//llamamos el procedeimietno alamcenado
					
				proc.setString(1, param.get("desde").toString());//Tipo String
				proc.setString(2, param.get("hasta").toString());//Tipo String
				proc.setString(3, tblplancuenta.getPeriodo());//Tipo String
				resultset=proc.executeQuery();

				while(resultset.next())
				{
					System.out.println("Entra ResultSet");
					String tmpcodigo =resultset.getString("tmpcodigo");
					String tmpcuenta =resultset.getString("tmpcuenta");
					BigDecimal resta=resultset.getBigDecimal("resta");
					balance= new TmpAuxgeneralDTO(tmpcodigo, tmpcuenta,resta);
					listabalance.add(balance);
					System.out.println("Codigo:"+tmpcodigo+" "+tmpcuenta+" "+resta);
				}
				return listabalance;
				
		} catch (SQLException e1) {
			conn.rollback();
			e1.printStackTrace();
		}finally{
			if(conn != null){
				conn.close();	
			}
			if(proc!=null){
				proc.close();				
			}		
		}
		return listabalance;

	}
	public List<TmpComprobacionDTO> ejecutarComprobacion (String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD )throws Exception{	
		Connection conn=null;
		CallableStatement proc= null;
		
		ResultSet resultset = null;
		TmpComprobacionDTO comprobacion = null;
		List<TmpComprobacionDTO> listtmpcomprobaciondto = new ArrayList<TmpComprobacionDTO>(0);
		
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		Tblplancuenta tblplancuenta = new Tblplancuenta();
		tblplancuenta=tblplancuentahome.findById(Integer.valueOf(planCuenta));		
		try {
		System.out.println("Nabucodonosor llego al procedimiento");	
		Class.forName("com.mysql.jdbc.Driver");
		
		conn = DriverManager.getConnection ("jdbc:mysql://localhost:3306/"+NombreBD, UsuarioBD, PasswordBD);
		
		byte[] pdfbyte = null;

		//case 1: 
		//default: 2-comprobacion, 3-general, 4-estadoresultados
		System.out.println(param.get("desde")+"-"+param.get("hasta")+"-"+param.get("Periodo"));
		
		proc = conn.prepareCall("{call "+fileName+" (?,?,?)}");//llamamos el procedeimietno alamcenado
					
				proc.setString(1, param.get("desde").toString());//Tipo String
				proc.setString(2, param.get("hasta").toString());//Tipo String
				proc.setString(3, tblplancuenta.getPeriodo());//Tipo String
				resultset=proc.executeQuery();

				while(resultset.next())
				{
					System.out.println("Entra ResultSet");
					String tmpcodigo =resultset.getString("tmpcodigo");
					String tmpcuenta =resultset.getString("tmpcuenta");
					BigDecimal tmpdebe=resultset.getBigDecimal("tmpdebe");
					BigDecimal tmphaber=resultset.getBigDecimal("tmphaber");
					BigDecimal tmpdeudor=resultset.getBigDecimal("tmpdeudor");
					BigDecimal tmpacreedor=resultset.getBigDecimal("tmpacreedor");
					comprobacion= new TmpComprobacionDTO(tmpcodigo, tmpcuenta, tmpdebe, tmphaber, tmpdeudor, tmpacreedor);
					listtmpcomprobaciondto.add(comprobacion);
					System.out.println("Codigo:"+tmpcodigo+" "+tmpcuenta+" "+tmpdebe+" "+tmphaber+" "+tmpdeudor+" "+tmpacreedor);
				}
				return listtmpcomprobaciondto;
				
		} catch (SQLException e1) {
			conn.rollback();
			e1.printStackTrace();
		}finally{
			if(conn != null){
				conn.close();	
			}
			if(proc!=null){
				proc.close();				
			}		
		}
		return listtmpcomprobaciondto;

	}
	public String ejecutarProcedimiento (String fileName, HashMap <String,Object> param, boolean bandera, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD) throws Exception{
		Connection conn=null;
		CallableStatement proc=null;
		
		TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
		Tblplancuenta tblplancuenta = new Tblplancuenta();
		tblplancuenta=tblplancuentahome.findById(Integer.valueOf(planCuenta));
		
		try {
		System.out.println("Nabucodonosor llego al procedimiento");
		String nombreReporte="";
		String nombreProcedimiento="";	
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		conn = DriverManager.getConnection ("jdbc:mysql://localhost:3306/"+NombreBD, UsuarioBD,PasswordBD);
		
		byte[] pdfbyte = null;

		//case 1: 
		//default: 2-comprobacion, 3-general, 4-estadoresultados
		System.out.println(param.get("desde")+"-"+param.get("hasta")+"-"+param.get("Periodo"));
		if(fileName.equals("Mayorizacion")){
		}else{
			if(fileName.equals("Comprobacion")){
					nombreProcedimiento="comprobacion";
			}else if(fileName.equals("General")){
					nombreProcedimiento="Balgeneral";
			}else if(fileName.equals("EstadoResultados")){
					nombreProcedimiento="Estresultados";
			}

		
			try {
				proc = conn.prepareCall("{call "+nombreProcedimiento+" (?,?,?)}");//llamamos el procedeimietno alamcenado
				
				proc.setString(1, param.get("desde").toString());//Tipo String
				proc.setString(2, param.get("hasta").toString());//Tipo String
				proc.setString(3, tblplancuenta.getPeriodo());//Tipo String
				proc.execute();
				if (bandera){
					String filename=generarReporteJasper(fileName, param, conn);
					return filename;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		
		}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
		
		public String generarReporteJasper(String fileName,HashMap <String,Object> param, Connection conn){
			try{
				byte[] pdfbyte = null;
				String filepath=getServletConfig().getServletContext().getRealPath("reportes/");
				System.out.println("filepath: "+filepath);
				String jasperFileName = filepath+fileName+".jasper";
				System.out.println("reportJasper: "+jasperFileName);
				String jasperFileNameCabecera = filepath+"Cabecera"+fileName+".jasper";
				System.out.println("reportJasper: "+jasperFileName);		
				String jrxmlFileName = filepath+fileName+".jrxml";
				System.out.println("jrxmlFileName: "+jrxmlFileName);
				JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
				String jrxmlFileNameCabecera = filepath+"Cabecera"+fileName+".jrxml";
				System.out.println("jrxmlFileNameCabecera: "+jrxmlFileNameCabecera);
				JasperCompileManager.compileReportToFile(jrxmlFileNameCabecera, jasperFileNameCabecera);
				JasperPrint jasperprint= JasperFillManager.fillReport (jasperFileName, param, conn);
				String fileout=filepath+"jasperoutput"+File.separator;
				System.out.println("fileout: "+fileout);
				String newFileName = fileout+fileName+".pdf";
				System.out.println("newFileName: "+newFileName);
				JasperExportManager.exportReportToPdfFile(jasperprint, newFileName);
				File reportFile = new File(jasperFileName);
				pdfbyte = JasperRunManager.runReportToPdf ( reportFile.getPath (), new HashMap (), conn );
				return fileName+".pdf";
				}
			
			
			catch (JRException ex)
			{
			Logger.getLogger(GreetingServiceImpl.class.getName()).log(Level.FATAL, null, ex);
			System.out.println("JRException: " + ex.getMessage());
			}
			return null;
		}
		

	
	public String CostoUtilidadDetalle(LinkedList<DtoComDetalleDTO> listDetDTO){
		String mensaje="";
		try{
			LinkedList<Tbldtocomercialdetalle> lista=new  LinkedList<Tbldtocomercialdetalle>();
			TbldtocomercialdetalleHome acceso =new TbldtocomercialdetalleHome();
			for(int i=0;i<listDetDTO.size();i++){
				Tbldtocomercialdetalle det=new Tbldtocomercialdetalle();
				det=acceso.findById(listDetDTO.get(i).getIdDtoComercialDetalle());
				det.setCostoProducto(listDetDTO.get(i).getCostoProducto());
				lista.add(det);
				
			}
			acceso.modificar(lista);
			mensaje="Datos Grabados con exito..";
			
		}catch(Exception e){
			mensaje=e.getMessage();
		}
		return mensaje;
	}
	/*
	public LinkedList<detalleVentasATSDTO> ATSVentas(String fechai,String fechaf){
		TbldtocomercialHome acceso1 = new TbldtocomercialHome();
		LinkedList<detalleVentasATSDTO> ATSVentas=new LinkedList<detalleVentasATSDTO>();
		LinkedList<PersonaDTO> personas=new LinkedList<PersonaDTO>();
		List<DtocomercialDTO> retenciones=this.listarFacturas(fechai, fechaf,"5");
		List<DtocomercialDTO> facturas=this.listarFacturas(fechai, fechaf,"0");
		List<DtocomercialDTO> notasCredito=this.listarFacturas(fechai, fechaf, "3");
		
		if(facturas.size()>0){
			for(int i=0;i<facturas.size();i++){
				if(String.valueOf(facturas.get(i).getEstado()).equals("1")){
					boolean ban=true;
					PersonaDTO cliente=new PersonaDTO();
					cliente=facturas.get(i).getTblpersonaByIdPersona();
					for(int j=0;j<personas.size();j++){
						if(personas.get(j).getCedulaRuc().equals(cliente.getCedulaRuc())){
							ban=false;
						}
					}
					if(ban){
						personas.add(cliente);
						
					}
				}
				
			}
		}
		if(notasCredito.size()>0){
			for(int i=0;i<notasCredito.size();i++){
				if(String.valueOf(notasCredito.get(i).getEstado()).equals("1")){
					boolean ban=true;
					PersonaDTO cliente=new PersonaDTO();
					cliente=notasCredito.get(i).getTblpersonaByIdPersona();
					for(int j=0;j<personas.size();j++){
						if(personas.get(j).getCedulaRuc().equals(cliente.getCedulaRuc())){
							ban=false;
						}
					}
					if(ban){
						personas.add(cliente);
					}
				} 
				
			}
		}
		
		System.out.println("INICIO ATS");
		TblproductoHome acceso=new TblproductoHome();
		for(int i=0;i<personas.size();i++){
			List<DtocomercialDTO> fac=this.listarFacturasCliente(fechai, fechaf, String.valueOf(personas.get(i).getIdPersona()), "0");
			double ivaRet=0;
			double rentaRet=0;
			//System.out.println("Persona "+personas.get(i).getCedulaRuc());
			for(int j=0;j<fac.size();j++){
				if(String.valueOf(fac.get(j).getEstado()).equals("1")){
					ivaRet=ivaRet+acceso1.RetencionPorFactura(String.valueOf(fac.get(j).getIdDtoComercial()),"0");
					rentaRet=rentaRet+acceso1.RetencionPorFactura(String.valueOf(fac.get(j).getIdDtoComercial()),"1");
					ivaRet=ivaRet+acceso1.RetencionPorTblFactura(String.valueOf(fac.get(j).getIdDtoComercial()),"0");
					rentaRet=rentaRet+acceso1.RetencionPorTblFactura(String.valueOf(fac.get(j).getIdDtoComercial()),"1");
		//			System.out.println("     ivaRet "+ivaRet);
			//		System.out.println("     rentaRet "+rentaRet);
				}
				
			} 
			detalleVentasATSDTO ats=new detalleVentasATSDTO();
			ats.setPersona(personas.get(i));
			ats.setNumeroComprobantes(acceso.ATSVentasNumComprobantes(personas.get(i).getIdPersona(), fechai, fechaf));
			ats.setBaseImponible(acceso.ATSVentasClienteBaseImponible(personas.get(i).getIdPersona(), fechai, fechaf));
			ats.setBaseImpGrav(acceso.ATSVentasClienteBaseImpGrav(personas.get(i).getIdPersona(), fechai, fechaf));
			ats.setBaseNoGraIva(0);
			ats.setMontoIva();
			ats.setValorRetIva(ivaRet);
			ats.setValorRetRenta(rentaRet);
			if(ats.getPersona().getCedulaRuc().equals("9999999999999")){
				ats.setTpIdCliente("07"); 
			}else{
				ats.setTpIdCliente("04"); 
			}
			
			ats.setTipoComprobante(18);
			ATSVentas.add(ats);
		}
		System.out.println("FIN    ATS VENTAS");
		ATSVentasXML(ATSVentas);
		return ATSVentas;
	}
	*/
	public void ATSVentasXML(LinkedList<detalleVentasATSDTO> atsVentas){
		System.out.println("ENTRO EN ATSVENTAS");
		CreateXML create=new CreateXML();
		create.ATSVentasXML(atsVentas,"iva");
	}
	//#######################################################################

//	@Override
//	public String actualizarIva() {
//		// TODO Auto-generated method stub
//		TblproductoHome acceso = new TblproductoHome();
//		String respuesta="";
//		try {
//				respuesta=acceso.actualizarIva();
//		} catch (RuntimeException re) {
//			respuesta=re.getLocalizedMessage();
//			throw re;	
//		} finally {
//		}
//		return respuesta;
//	}
	
	//#######################################################################
	
	public List<EquipoOrdenDTO> listarEquipoOrdenFiltroID(Integer id){
		
		System.out.println("Llego a Listar orden");
		List<EquipoOrdenDTO> list = new ArrayList<EquipoOrdenDTO>();
		TblequipoordenHome acceso = new TblequipoordenHome();
		TblordenHome accesoorden = new TblordenHome();
		TblequipoHome accesoequipo = new TblequipoHome();
		TblclienteHome accesocliente = new TblclienteHome();
		TblempleadoHome accesoempleado = new TblempleadoHome();
		TblpersonaHome accesopersona = new TblpersonaHome();
		//TblmarcaHome accesomarca = new TblmarcaHome();
		//TbltipoequipoHome accesotipoquipo = new TbltipoequipoHome();
		Iterator resultado = acceso.ListJoinOrdenID(id);
		while (resultado.hasNext()) {
			Object[] pair = (Object[])resultado.next();
				TblequipoTblorden equipoorden = (TblequipoTblorden) pair[0];
				
				Tblequipo equipodto=accesoequipo.findById(equipoorden.getTblequipo().getIdequipo());
				
				
				Tblorden ordendto=accesoorden.findById(equipoorden.getTblorden().getIdorden());
				System.out.println("Orden: "+ordendto.getIdorden()+"-"+ordendto.getObservaciones()+"-"+ordendto.getDescripcionaccesorios());
				Tblcliente clientedto=accesocliente.findById(ordendto.getTblcliente().getIdCliente());
				Tblpersona personaclientedto=accesopersona.findById(clientedto.getTblpersona().getIdPersona());
				System.out.print("persona cliente: "+personaclientedto.getIdPersona());
				clientedto.setTblpersona(personaclientedto);
				clientedto.setIdCliente(ordendto.getTblcliente().getIdCliente());
				ordendto.setTblcliente(clientedto);
				
				Tblempleado empleadodto=accesoempleado.findById(ordendto.getTblempleado().getIdEmpleado());
				Tblpersona personaempleadodto=accesopersona.findById(empleadodto.getTblpersona().getIdPersona());
				System.out.print("persona empleado: "+personaempleadodto.getIdPersona());
				empleadodto.setTblpersona(personaempleadodto);
				empleadodto.setIdEmpleado(ordendto.getTblempleado().getIdEmpleado());
				ordendto.setTblempleado(empleadodto);
				
				//Tblorden ord=ordendto;
				
				EquipoOrdenDTO equipoordenD = new EquipoOrdenDTO(equipoorden.getIdequipoorde(), crearEquipoReg(equipodto), crearOrdenReg(ordendto));
				list.add(equipoordenD);
		}
		return list;
	}
	//#######################################################################
	public EquipoDTO crearEquipoReg(Tblequipo equ){
		System.out.println("inicializar los datos de los equipos");
		EquipoDTO newe = new EquipoDTO(equ.getIdEmpresa(),equ.getEstablecimiento(),equ.getIdequipo(), equ.getIdtipoequipo(), equ.getIdmarca(), equ.getModeloequipo(), equ.getNumeroserie());
		newe.setIdtipo(equ.getIdtipo());
		return newe;	
	}
	//#######################################################################
	public OrdenDTO crearOrdenReg(Tblorden ord){
		System.out.println("inicializar los datos de orden");
		System.out.println(ord.getTblcliente().getIdCliente());
		//System.out.println(ord.getTblempleado().getTblcargo().getIdCargo()+"-"+ord.getTblempleado().getTblcargo().getDescripcion());
		System.out.println(ord.getTblcliente().getIdCliente());
		System.out.println(ord.getTblcliente().getTblpersona().getIdPersona()+"-"+ord.getTblcliente().getTblpersona().getCedulaRuc()+"-"+ord.getTblcliente().getTblpersona().getNombreComercial()+"-"+ord.getTblcliente().getTblpersona().getRazonSocial()+"-"+ord.getTblcliente().getTblpersona().getDireccion()+"-"+ord.getTblcliente().getTblpersona().getTelefono1()+"-"+ord.getTblcliente().getTblpersona().getTelefono2()+"-"+ord.getTblcliente().getTblpersona().getObservaciones()+"-"+ord.getTblcliente().getTblpersona().getMail()+"-"+ord.getTblcliente().getTblpersona().getEstado());
		System.out.println(ord.getTblcliente().getMontoCredito()+"-"+ord.getTblcliente().getCupoCredito()+"-"+ord.getTblcliente().getCodigoTarjeta()+"-"+ord.getTblcliente().getPuntos());
		ClienteDTO tblcliente = crearClienteReg(ord.getTblcliente());
		System.out.println(ord.getTblempleado().getTblpersona().getIdPersona()+"-"+ ord.getTblempleado().getTblpersona().getCedulaRuc()+"-"+ ord.getTblempleado().getTblpersona().getNombreComercial()+"-"+  ord.getTblempleado().getTblpersona().getRazonSocial()+"-"+ ord.getTblempleado().getTblpersona().getDireccion()+"-"+ord.getTblempleado().getTblpersona().getTelefono1());

		EmpleadoDTO tblempleado= new EmpleadoDTO(
				//new CargoDTO(ord.getTblempleado().getTblcargo().getIdCargo(), ord.getTblempleado().getTblcargo().getDescripcion()),	
				//new TblrolDTO(),
				ord.getTblempleado().getIdEmpresa(),
				ord.getTblempleado().getEstablecimiento(),
				new CargoDTO(),
				new PersonaDTO(ord.getTblempleado().getTblpersona().getIdPersona(), ord.getTblempleado().getTblpersona().getCedulaRuc(), ord.getTblempleado().getTblpersona().getNombreComercial(),  ord.getTblempleado().getTblpersona().getRazonSocial(), ord.getTblempleado().getTblpersona().getDireccion(),ord.getTblempleado().getTblpersona().getTelefono1()),
				ord.getTblempleado().getPassword(), 
				ord.getTblempleado().getIdRol(),
				ord.getTblempleado().getNivelAcceso(), 
				ord.getTblempleado().getSueldo(), 
				ord.getTblempleado().getHorasExtras(),
				ord.getTblempleado().getIngresosExtras(), 
				ord.getTblempleado().getFechaContratacion(),
				ord.getTblempleado().getEstado()
		);
		
		tblempleado.setIdEmpleado(ord.getTblempleado().getIdEmpleado());
		tblcliente.setIdCliente(ord.getTblcliente().getIdCliente());
		
		OrdenDTO newo = new OrdenDTO(ord.getIdEmpresa(),ord.getEstablecimiento(),tblempleado, tblcliente, ord.getIdequipoorden(), ord.getTipoorden(), ord.getFechadiagnostico(), ord.getFechaentrega(), ord.getDescripcionfalla(), ord.getDescripcionaccesorios(), ord.getObservaciones(), ord.getReporte());
		newo.setFechaingreso(ord.getFechaingreso());
		newo.setIdorden(ord.getIdorden());
		return newo;	
	}
	//#######################################################################
	public ClienteDTO crearClienteReg(Tblcliente cli){
		System.out.println("inicializar los datos del cliente");
		return new ClienteDTO(
				cli.getIdEmpresa(),
				cli.getIdCliente(),
				crearPersonaDTO(cli.getTblpersona()), 
				cli.getMontoCredito(),
				cli.getCupoCredito(), 
				cli.getCodigoTarjeta(), 
				cli.getPuntos(),
				cli.getContribuyenteEspecial(),
				cli.getObligadoContabilidad()
				);	
	}
	
	//#######################################################################
	public ClienteDTO buscarCliente(int id){
		TblclienteHome accesoCliente = new TblclienteHome();
		Tblcliente tblcliente= new Tblcliente();
		ClienteDTO cliente=null;
		String mensaje = "";
		try {
			try {
				tblcliente=accesoCliente.findById(Integer.valueOf(id));
				PersonaDTO per = buscarPersona(tblcliente.getTblpersona().getIdPersona());
				cliente=new ClienteDTO(tblcliente.getIdEmpresa(),tblcliente.getIdCliente(),per,tblcliente.getMontoCredito(),tblcliente.getCupoCredito(),tblcliente.getCodigoTarjeta(),tblcliente.getPuntos(),tblcliente.getContribuyenteEspecial(), tblcliente.getObligadoContabilidad());
				mensaje = "Busqueda realizada con \u00e9xito";
				return cliente;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
			
		}
		return cliente;
	}
	//#######################################################################
	public PersonaDTO buscarPersona(int id){
		TblpersonaHome accesoPersona = new TblpersonaHome();
		Tblpersona tblpersona= new Tblpersona();
		PersonaDTO persona=null;
		String mensaje = "";
		try {
			try {
				tblpersona=accesoPersona.findById(Integer.valueOf(id));
				persona=new PersonaDTO(tblpersona.getIdPersona(),tblpersona.getCedulaRuc(),tblpersona.getNombreComercial(),tblpersona.getRazonSocial(),tblpersona.getDireccion(),tblpersona.getTelefono1());
				mensaje = "Busqueda realizada con \u00e9xito";
				return persona;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
			
		}
		return persona;
	}
	//#######################################################################
	/*public  void cambiaValor(int banderaSO) {
	        try {
	            Field field = Factum.class.getDeclaredField("banderaSO");
	            field.setAccessible(true);
	            field.setInt(null, banderaSO);
	        } catch (Exception e) {
	            System.out.println("No se pudo cambiar el valor :(");
	            e.printStackTrace(System.out);
	        }
	    }*/
	public String backupDB(boolean validadorAlmacenamiento,String NombreBD,String PasswordBD,String UsuarioBD,String SO,String PasswordComprimido, String destinoBackup  ){
	String dbtime;
	String dbUrl = "jdbc:mysql://localhost:3306/"+NombreBD;
	String dbClass = "com.mysql.jdbc.Driver";
	ResultSet res;
	String result="";
	String pathBACKUP=getServletConfig().getServletContext().getRealPath("backup/");
	pathBACKUP=pathBACKUP+File.separator;
	System.out.println("jdbc:mysql://localhost:3306/"+NombreBD);
	System.out.println("PATH DEL BACKUP: "+ pathBACKUP);
	Process runtimeProcess;
			try {
				
				//METODO PARA TOMAR EL DIRECTORIO DE INSTALACIO MYSQL
		            Class.forName(dbClass).newInstance();
		            System.out.println(NombreBD+"con"+PasswordBD);
		            Connection con = DriverManager.getConnection (dbUrl, UsuarioBD, PasswordBD);
		            
		            java.sql.Statement stmt = con.createStatement();
		            res = stmt.executeQuery("SHOW VARIABLES LIKE 'basedir'");
		            String Mysqlpath = "";

		            while(res.next()){
		                Mysqlpath=res.getString("Value") ;
		            }

		            Mysqlpath=Mysqlpath+"bin"+File.separator;
		            System.out.println("Mysql path is :"+Mysqlpath);
				
				//METODO PARA REALIZR AL BACKUP DE LA BASE DE DATOS
			    String line="";
	            String[] commandwindows=null;
	            String commandlinux=null;
	            Process process=null;
			    if(SO.equals("windows")){
			    	 commandwindows = new String[] {"cmd.exe", "/c", "\""+Mysqlpath+"mysqldump.exe\" --quick --lock-tables --user=\""+UsuarioBD+"\" --password=\""+PasswordBD+"\" "+NombreBD};
		             process = Runtime.getRuntime().exec(commandwindows);
			    	
			    }else if(SO.equals("linux")){
			    	commandlinux = "mysqldump  -u " + UsuarioBD +" -p " + PasswordBD+" --database "+NombreBD;
		            process = Runtime.getRuntime().exec(commandlinux);
			    }
			    //Process process = Runtime.getRuntime().exec(command);
			    BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
			    BufferedWriter bw = new BufferedWriter(new FileWriter(new File(pathBACKUP+NombreBD+".sql")));
			    while ((line = input.readLine()) != null) {
			        bw.write(line);
			        bw.write("\n");
			        System.out.println(line); //there you can write file 
			    }
			    input.close();
			    bw.close();
			    ComprimirDocumento(pathBACKUP, validadorAlmacenamiento, NombreBD, PasswordComprimido, destinoBackup);
				return result;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ZipException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;		
		
	}
	/*public String backupDB(boolean validadorAlmacenamiento){
		String dbtime;
		String dbUrl = "jdbc:mysql://localhost:3306/"+Factum.banderaNombreBD;
		String dbClass = "com.mysql.jdbc.Driver";
		ResultSet res;
		String result="";
		String pathBACKUP=getServletConfig().getServletContext().getRealPath("backup/");
		pathBACKUP=pathBACKUP+File.separator;
		System.out.println("jdbc:mysql://localhost:3306/"+Factum.banderaNombreBD);
		System.out.println(Factum.banderaUsuarioBD+"con"+Factum.banderaPasswordBD);
		System.out.println("PATH DEL BACKUP: "+ pathBACKUP);
		Process runtimeProcess;
				try {
					
					//METODO PARA TOMAR EL DIRECTORIO DE INSTALACIO MYSQL
			            Class.forName(dbClass).newInstance();
			            System.out.println(Factum.banderaUsuarioBD+"con"+Factum.banderaPasswordBD);
			            Connection con = DriverManager.getConnection (dbUrl, Factum.banderaUsuarioBD, Factum.banderaPasswordBD);
			            
			            java.sql.Statement stmt = con.createStatement();
			            res = stmt.executeQuery("SHOW VARIABLES LIKE 'basedir'");
			            String Mysqlpath = "";

			            while(res.next()){
			                Mysqlpath=res.getString("Value") ;
			            }

			            Mysqlpath=Mysqlpath+"bin"+File.separator;
			            System.out.println("Mysql path is :"+Mysqlpath);
					
					//METODO PARA REALIZR AL BACKUP DE LA BASE DE DATOS
				    String line="";
		            String[] commandwindows=null;
		            String commandlinux=null;
		            Process process=null;
				    if(Factum.banderaSO.equals("windows")){
				    	 commandwindows = new String[] {"cmd.exe", "/c", "\""+Mysqlpath+"mysqldump.exe\" --quick --lock-tables --user=\""+Factum.banderaUsuarioBD+"\" --password=\""+Factum.banderaPasswordBD+"\" "+Factum.banderaNombreBD};
			             process = Runtime.getRuntime().exec(commandwindows);
				    	
				    }else if(Factum.banderaSO.equals("linux")){
				    	commandlinux = "mysqldump  -u " + Factum.banderaUsuarioBD +" -p " + Factum.banderaPasswordBD+" --database "+Factum.banderaNombreBD;
			            process = Runtime.getRuntime().exec(commandlinux);
				    }
				    //Process process = Runtime.getRuntime().exec(command);
				    BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
				    BufferedWriter bw = new BufferedWriter(new FileWriter(new File(pathBACKUP+Factum.banderaNombreBD+".sql")));
				    while ((line = input.readLine()) != null) {
				        bw.write(line);
				        bw.write("\n");
				        System.out.println(line); //there you can write file 
				    }
				    input.close();
				    bw.close();
				    ComprimirDocumento(pathBACKUP, validadorAlmacenamiento);
					return result;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ZipException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return null;		
			
		}*/
	//#######################################################################
	public String grabarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproh) {
		String mensaje="";
		if(this.buscarProducto(pro.getCodigoBarras(),"codigoBarras")==null){
			Set<Tblbodega> bodegas = new HashSet<Tblbodega>();
			Tblbodega bodega=new Tblbodega();
			Set<Tbltipoprecio> precios = new HashSet<Tbltipoprecio>();
			Tbltipoprecio tipo=new Tbltipoprecio();
			ProductoTipoPrecioDTO[] preciosL = new ProductoTipoPrecioDTO[pro.getProTip().size()];
			List<Tblproducto> productos= new ArrayList<Tblproducto>();
				try {
					Tblcategoria cat=new Tblcategoria();
					cat.setIdCategoria(pro.getTblcategoria());
					Tblunidad unidad=new Tblunidad();
					unidad.setIdUnidad(pro.getTblunidad());
					Tblmarca marca =new Tblmarca();
					marca.setIdMarca(pro.getTblmarca());
					Tblproducto productopadre = new Tblproducto(pro);
					productopadre.setTblcategoria(cat);
					productopadre.setTblmarca(marca);
					productopadre.setTblunidad(unidad);
					productopadre.setJerarquia(pro.getJerarquia());
					for(int i=0;i<pro.getTblbodega().size();i++){
						bodega =new Tblbodega(pro.getTblbodega().get(i));
						bodegas.add(bodega);
					}
					for(int i=0;i<pro.getProTip().size();i++){
						//System.out.println("Tipo precio "+pro.getProTip().get(i).getTbltipoprecio().getIdTipoPrecio());
						tipo=new Tbltipoprecio(pro.getTbltipoprecio().get(i));
						precios.add(tipo);
						preciosL[i]=pro.getProTip().get(i);
					}
					
					productopadre.setTblbodegas(bodegas);
					productopadre.setTbltipoprecios(precios);
					TblproductoHome prod = new TblproductoHome();
					TblproductoelaboradoHome prodelab= new TblproductoelaboradoHome();
					
					
					//mensaje="bodega encontrada "+bodega.getNombre()+" "+tipo.getIdTipoPrecio()+ " "+tipo.getTipoPrecio();
					int id=prod.grabaryretornar(productopadre);
					System.out.println("CODIGO PADRE NUEVO: "+id +"TAMA�O HIJOS: "+listproh.size());
					productopadre.setIdProducto(id);
						for(ProductoDTO productodto : listproh){
							Tblproducto productohijo= new Tblproducto();
							productohijo.setIdProducto(productodto.getIdProducto());
							System.out.println("Codigo de hijo producto elaborado: "+productodto.getIdProducto());
							
							Tblproductoelaborado productoelaborado = new Tblproductoelaborado();
							productoelaborado.setTblproductoByIdProductoPadre(productopadre);
							productoelaborado.setTblproductoByIdProductoHijo(productohijo);
							productoelaborado.setCantidad(productodto.getCantidad());
							
							// cambio elaborado
							productoelaborado.setIdEmpresa(productodto.getIdEmpresa());
							productoelaborado.setEstablecimiento(productodto.getEstablecimiento());
							
							System.out.println("PADRE: "+productoelaborado.getTblproductoByIdProductoPadre().getIdProducto()+" HIJO "+productoelaborado.getTblproductoByIdProductoHijo().getIdProducto()+"CANTIDAD "+productoelaborado.getCantidad());
							prodelab.grabar(productoelaborado);
						}
						System.out.println(this.grabarProductoTipoPrecio(preciosL,preciosL.length));
					mensaje="Producto Grabado";
				} catch (ErrorAlGrabar e) {
					mensaje = e.toString();
				}
			
		}
		//si existe un producto dado de baja
		else if(this.buscarProductoEliminado(pro.getCodigoBarras(),"codigoBarras")!=null){
			mensaje="baja";			 	
		}
		else{
			
			mensaje="El producto ya existe";
		}
			return mensaje;
	}
	
	public List<ProductoElaboradoDTO> listarHijosProductoElaborado(String idProductoElaborado){
		System.out.println("ID DEL PADRE: "+idProductoElaborado);
		List<ProductoElaboradoDTO> listaproductosdto = new ArrayList<ProductoElaboradoDTO>();
		List<Tblproductoelaborado> listatblproductos = new ArrayList<Tblproductoelaborado>();
		Tblproductoelaborado productoelaborado= new Tblproductoelaborado();
		TblproductoelaboradoHome tblproductoelaboradohome = new TblproductoelaboradoHome();
		listatblproductos= tblproductoelaboradohome.findByIdProductoHijos(idProductoElaborado);
		ProductoElaboradoDTO productoelaboradodto;
		System.out.println("TAMA�O LISTA PRODUCTOS HIJOS: "+listatblproductos.size());
		for(Tblproductoelaborado prod : listatblproductos){
			ProductoDTO prodDTOpadre=this.buscarProducto(String.valueOf(prod.getTblproductoByIdProductoPadre().getIdProducto()),"idProducto");	
			ProductoDTO prodDTOhijo=this.buscarProducto(String.valueOf(prod.getTblproductoByIdProductoHijo().getIdProducto()),"idProducto");
			productoelaboradodto= new ProductoElaboradoDTO(prod.getIdEmpresa(),prod.getEstablecimiento(),prodDTOpadre, prodDTOhijo, prod.getCantidad());
			productoelaboradodto.setIdproductoelaborado(prod.getIdProductoelaborado());
			System.out.println("ID PRODUCTO ELBORADO: "+prod.getIdProductoelaborado()+" ID HIJO: "+String.valueOf(prod.getTblproductoByIdProductoHijo().getIdProducto())+" ID PADRE: "+String.valueOf(prod.getTblproductoByIdProductoPadre().getIdProducto())+" CANTIDAD: "+prod.getCantidad());
			listaproductosdto.add(productoelaboradodto);
		}	
		return listaproductosdto;
	} 
	
	public String  modificarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproh){
		System.out.println("-------------------------------------------------------");
		System.out.println("ENTRO A MODIFICAR PRODUCTO ELABORADO: "+listproh.size());
		String mensaje="";
		Set<Tblbodega> bodegas = new HashSet<Tblbodega>();
		Tblbodega bodega=new Tblbodega();
		Set<Tbltipoprecio> precios = new HashSet<Tbltipoprecio>();
		Tbltipoprecio tipo=new Tbltipoprecio();
		boolean ban=false;
		TblproductoHome prod = new TblproductoHome();
		System.out.println("bodega: " + bodega);
		System.out.println("tipo: " + tipo);

		TblproductoelaboradoHome prodelab = new TblproductoelaboradoHome();
		ProductoElaboradoDTO productoelaboradodto = new ProductoElaboradoDTO();
		System.out.println("producto elaborado home: " + prodelab);
		System.out.println("producto elaborado dto: " + productoelaboradodto);

		
		List<ProductoElaboradoDTO> listaproductoseleborados=listarHijosProductoElaborado(String.valueOf(pro.getIdProducto()));
			try {
				Tblproducto producto = new Tblproducto(pro);		
				producto=prod.findById(pro.getIdProducto());
				System.out.println("producto: " + producto);
				Tblcategoria cat=new Tblcategoria();
				cat.setIdCategoria(pro.getTblcategoria());
				System.out.println("categoria: " + cat);
				Tblunidad unidad=new Tblunidad();
				unidad.setIdUnidad(pro.getTblunidad());
				System.out.println("unidad: " + unidad);
				Tblmarca marca =new Tblmarca();
				marca.setIdMarca(pro.getTblmarca());
				System.out.println("marca: " + marca);
				producto.setTblcategoria(cat);
				producto.setTblmarca(marca);
				producto.setTblunidad(unidad);
				producto.setStock(pro.getStock());
				producto.setCodigoBarras(pro.getCodigoBarras());
				producto.setDescripcion(pro.getDescripcion());
				
				
				
				Set<TblproductoMultiImpuesto> prodMultiImp=multiImpuestoDTOtoMultiImp(pro.getTblmultiImpuesto());
				Set<TblproductoMultiImpuesto> prodMultiImpN=new HashSet(0);
				Set<TblmultiImpuesto> multiImpSet=new HashSet(0);
				for (TblproductoMultiImpuesto multImp:prodMultiImp) {
					multImp.setTblproducto(producto);
					prodMultiImpN.add(multImp);
					multiImpSet.add(multImp.getTblmultiImpuesto());
					System.out.println("Multi impuesto set: " + multiImpSet);
				}
				producto.setTblmultiimpuestos(multiImpSet);
//				producto.setTblProdMultiImpuesto(prodMultiImpN);
				
//				producto.setTblmultiImpuesto(multiImpuestoDTOtoMultiImp(pro.getTblmultiImpuesto()));
//				producto.setImpuesto(pro.getImpuesto());
				producto.setPromedio(pro.getPromedio()); //cambiar luego
				producto.setFifo(pro.getFifo());
				producto.setLifo(pro.getLifo());
				producto.setTipo(pro.getTipo());
				producto.setDescuento(pro.getDescuento());
				producto.setImagen(pro.getImagen());
				producto.setJerarquia(pro.getJerarquia());
				producto.setCantidadunidad(pro.getCantidadunidad());
				prod.modificar(producto);

				Boolean bandera=false;
				HashMap<Integer, Boolean> hijosMap = new HashMap<Integer, Boolean>();
				
					for(int j=0;j<listproh.size();j++){							
						hijosMap.put(listproh.get(j).getIdProducto(), false);
					}
					for(ProductoElaboradoDTO prodelabdto: listaproductoseleborados){
						hijosMap.put(prodelabdto.getTblproductoByIdproductohijo().getIdProducto(), false);
					}
					
					for(Integer key :hijosMap.keySet()){
						System.out.println("LISTA DE PRODUCTOS NUEVA: "+key);
					}
					
					System.out.println("MODIFICADOS:");
					for(int i=0;i<listproh.size();i++){
						bandera=false;
						for(ProductoElaboradoDTO prodelabdto: listaproductoseleborados){
							//System.out.println("PRODUCTO HIJO RECIBIDO: "+listproh.get(i).getIdProducto()+" PRODUCTO HIJO INGRESADO: "+prodelabdto.getTblproductoByIdproductohijo().getIdProducto());
							if(listproh.get(i).getIdProducto().toString().equals(prodelabdto.getTblproductoByIdproductohijo().getIdProducto().toString())){
								System.out.println("PRODUCTO ELABORADO MODIFICADOS: "+listproh.get(i).getIdProducto());
								Tblproductoelaborado tblproductoelaborado =new Tblproductoelaborado();
								tblproductoelaborado.setTblproductoByIdProductoHijo(new Tblproducto(listproh.get(i)));
								tblproductoelaborado.setTblproductoByIdProductoPadre(producto);
								tblproductoelaborado.setCantidad(listproh.get(i).getCantidad());
								tblproductoelaborado.setIdProductoelaborado(prodelabdto.getIdproductoelaborado());
								
								// cambio
								tblproductoelaborado.setEstablecimiento(producto.getEstablecimiento());
								
//								System.out.println("PRODUCTO ELABORADO modificados: "+tblproductoelaborado.getTblproductoByIdProductoHijo());
//								System.out.println(" PADRE: "+tblproductoelaborado.getTblproductoByIdProductoPadre());
//								System.out.println(" HIJO: "+tblproductoelaborado.getIdProductoelaborado());
//								System.out.println(" CANTIDDAD: "+tblproductoelaborado.getCantidad());
//								System.out.println(" Establecimiento: "+producto.getEstablecimiento());
								
								prodelab.modificar(tblproductoelaborado);
								bandera=true;
								System.out.println("Prod elaborado modificar: " + prodelab);

							}
						}
						hijosMap.put(listproh.get(i).getIdProducto(), bandera);
					}
					System.out.println("ELIMINADOS:");
					for(Integer key :hijosMap.keySet()){
					bandera=false;
					//System.out.println("PRODUCTO ELABORADO ELIMINADO: "+key+" ESTADO ATRIBUTO: "+hijosMap.get(key));
						if(!hijosMap.get(key)){
							//System.out.println("PRODUCTO ELABORADO ELIMINADO: "+key+" ESTADO ATRIBUTO: "+hijosMap.get(key));
							for(ProductoElaboradoDTO prodelabdto: listaproductoseleborados){
								//System.out.println("PRODUCTO ELABORADO ELIMINADO: "+key+" PRODUCTO ELABORADO GRABDO ATRIBUTO: "+prodelabdto.getTblproductoByIdproductohijo().getIdProducto().toString());
								if(prodelabdto.getTblproductoByIdproductohijo().getIdProducto().toString().equals(key.toString())){
									System.out.println("PRODUCTO ELABORADO ELIMINADO: "+key+" ESTADO ATRIBUTO: "+hijosMap.get(key));
									Tblproductoelaborado tblproductoelaborado= new Tblproductoelaborado(prodelabdto);
									tblproductoelaborado.setIdProductoelaborado(prodelabdto.getIdproductoelaborado());
									prodelab.eliminacionFisica(tblproductoelaborado);
									bandera=true;	
								}
							}
							hijosMap.put(key, bandera);
						}	
					}
					System.out.println("AGREGADOS:");	
					for(Integer key :hijosMap.keySet()){
						bandera=false;
						//System.out.println("PRODUCTO ELABORADO AGREGADO: "+key+" ESTADO ATRIBUTO: "+hijosMap.get(key));
						if(!hijosMap.get(key)){
							System.out.println("PRODUCTO ELABORADO AGREGADO: "+key+" ESTADO ATRIBUTO: "+hijosMap.get(key));
							Tblproductoelaborado tblproductoelaborado =new Tblproductoelaborado();
							Tblproducto tblproducto=new Tblproducto();
							tblproducto.setIdProducto(key);
							tblproductoelaborado.setTblproductoByIdProductoHijo(tblproducto);
							tblproductoelaborado.setTblproductoByIdProductoPadre(producto);
							// cambiooo
							tblproductoelaborado.setIdEmpresa(producto.getIdEmpresa());
							tblproductoelaborado.setEstablecimiento(producto.getEstablecimiento());
							
							
//							System.out.println("PRODUCTO ELABORADO creados: "+tblproductoelaborado.getTblproductoByIdProductoHijo());
//							System.out.println(" PADRE: "+tblproductoelaborado.getTblproductoByIdProductoPadre());
//							System.out.println(" HIJO: "+tblproductoelaborado.getIdProductoelaborado());
//							System.out.println(" CANTIDDAD: "+tblproductoelaborado.getCantidad());
//							System.out.println(" establecimiento: "+producto.getEstablecimiento());

							//AQUI SETTEAMOS LA CANTIDAD
								for(ProductoDTO productodto : listproh){
									System.out.println("producto dto" +productodto.getIdProducto().toString() );
									if(productodto.getIdProducto().toString().equals(key.toString())){
										tblproductoelaborado.setCantidad(productodto.getCantidad());
									}
								}
							prodelab.grabar(tblproductoelaborado);
							System.out.println("Prod elaborado agregados: " + prodelab);

							bandera=true;	
						}							
					}
				mensaje="Producto Modificado";
			} catch (ErrorAlGrabar e) {
				mensaje = e.toString();
			}
		/*
		}else{
			mensaje="Error C�digo de Barras Repetido";
		}
		*/
		return mensaje;
	}
	//####################################################################3
	public void ComprimirDocumento(String path, boolean validadorAlmacenamiento, String NombreBD, String PasswordComprimido,String destinoBackup) throws IOException, ZipException {
        try {
         ZipFile zipFile = new ZipFile(path+"backup"+NombreBD+".zip");
         ArrayList filesToAdd = new ArrayList();
         //filesToAdd.add(new File(path+Factum.banderaNameBD+".sql"));
         filesToAdd.add(new File(path+NombreBD+".sql"));
         ZipParameters parameters = new ZipParameters();
         parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); // set compression method to deflate compression
         parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL); 
         parameters.setEncryptFiles(true);
         parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
         parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
         parameters.setPassword(PasswordComprimido);
         zipFile.addFiles(filesToAdd, parameters);
         if(validadorAlmacenamiento){
       		String destinoUSB=destinoBackup+File.separator+NombreBD+".zip";
      		ZipFile zipFileUSB = new ZipFile(destinoUSB);
      		System.out.println("DESTINO USB: "+destinoUSB);
      		zipFileUSB.addFiles(filesToAdd, parameters);
       	}
        } catch (ZipException e) {
         e.printStackTrace();
        }
  }
	//###################################################	
	public String obtenerIPServidor(){
		String ip ="";
		InetAddress ipaddress;
		try {
			ipaddress = InetAddress.getLocalHost();
			ip=ipaddress.getHostAddress();
			return ip;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ip;
		
	} 
	//###################################################
	public void comprimirImagen(String imagen) throws IOException{
		String path = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("images/catalogo/");
		File imageFile = new File(path+imagen);
		File compressedImageFile = new File(path+imagen);
		InputStream is = new FileInputStream(imageFile);
		OutputStream os = new FileOutputStream(compressedImageFile);

		float quality = 0.5f;

		// create a BufferedImage as the result of decoding the supplied InputStream
		BufferedImage image = ImageIO.read(is);

		// get all image writers for JPG format
		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");

		if (!writers.hasNext())
			throw new IllegalStateException("No writers found");

		ImageWriter writer = (ImageWriter) writers.next();
		ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		writer.setOutput(ios);

		ImageWriteParam param = writer.getDefaultWriteParam();

		// compress to a given quality
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(quality);

		// appends a complete image stream containing a single image and
	    //associated stream and image metadata and thumbnails to the output
		writer.write(null, new IIOImage(image, null, null), param);

		// close all streams
		is.close();
		os.close();
		ios.close();
		writer.dispose();

		
	}
	//###################################################
	public List<String> listarImagenes(){
	String path = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("images/catalogo/");
	System.out.println(path);
	File folder = new File(path);
	System.out.println("Tamanio del archivo: "+folder.length());
	File[] listOfFiles = folder.listFiles();
	List<String> listaimagenes = new ArrayList<String>();
	for (int i = 0; i < listOfFiles.length; i++) {
	    if (listOfFiles[i].isFile()) {
	    	System.out.println("File " + listOfFiles[i].getName());
	    	String nombre=listOfFiles[i].getName().toString();
	    	System.out.println("Nombre " + nombre);
	    	String[] nombres = nombre.split("\\.");	    	
	    	int longitud =nombres.length;
	    	System.out.println("Extension " +longitud);
	    	System.out.println("Extension "+nombres[longitud-1]);
	    	String extension=nombres[longitud-1];
	        if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg")) {
	            System.out.println("File " + nombre);
	            listaimagenes.add(nombre);
	        }
	    }
	}
	return listaimagenes;
	}	
		 
	//###################################################	 	
 	public String obtenernombreHost(){  
		String path=this.getThreadLocalRequest().getSession().getServletContext().getContextPath();
		System.out.println(path);
		return path;
	}		
	//###################################################	
	public String documentoElectronico(DtocomercialDTO documentodto, String NombrePlugFacturacionElectronica,String RutaCertificado, 
//			String AmbienteFacturacionElectronica, 
			String SO){

		//System.out.println("SE ESTA MODIFICANDO EL DTOCOMERCIAL: "+documentoDTO.getIdDtoComercial());
		List<Tbldtoelectronico> lsttbldtoelectronico = new ArrayList<Tbldtoelectronico>(0);
		TbldtoelectronicoHome tbldtoelectronicohome = new TbldtoelectronicoHome();
		String estado="";
		String id=String.valueOf(documentodto.getIdDtoComercial());
		lsttbldtoelectronico=tbldtoelectronicohome.findByQuery("where u.tbldtocomercial.idDtoComercial='"+documentodto.getIdDtoComercial()+"'");
		System.out.println("TAMAÑO LISTA: "+lsttbldtoelectronico.size());
		if(lsttbldtoelectronico.size()>0){
      	  for(Tbldtoelectronico tbldtoelectronico: lsttbldtoelectronico){
      	   estado= String.valueOf(tbldtoelectronico.getEstado());
      	  }
        }else{
      	   estado="0";
        }
		int tipotransaccion=documentodto.getTipoTransaccion();
		String documento="";
		try {
			
			//System.out.println("SE GENERARA LA RETENCION ELECTRONICA DEL: "+id);
			try {
			ProcessBuilder pb=null;
			switch(tipotransaccion){
			case 1:
				documento="Retencion";
				//pb = new ProcessBuilder("java"+" "+"-jar"+" "+Factum.banderaNombrePlugFacturacionElectronica+" "+id+" "+"07"+" "+Factum.banderaRutaCertificado+" "+Factum.banderaAmbienteFacturacionElectronica+" "+estado);
				pb = new ProcessBuilder("java","-jar",NombrePlugFacturacionElectronica,id,"07",RutaCertificado,
//						AmbienteFacturacionElectronica,
						estado);
			break;
			case 0:
				documento="Factura";
				//pb = new ProcessBuilder("java"+" "+"-jar"+" "+Factum.banderaNombrePlugFacturacionElectronica+" "+id+" "+"01"+" "+Factum.banderaRutaCertificado+" "+Factum.banderaAmbienteFacturacionElectronica+" "+estado);
				pb = new ProcessBuilder("java","-jar",NombrePlugFacturacionElectronica,id,"01",RutaCertificado,
//						AmbienteFacturacionElectronica,
						estado);
			break;
			case 3:
				documento="Nota de Credito";
				//pb = new ProcessBuilder("java"+" "+"-jar"+" "+Factum.banderaNombrePlugFacturacionElectronica+" "+id+" "+"01"+" "+Factum.banderaRutaCertificado+" "+Factum.banderaAmbienteFacturacionElectronica+" "+estado);
				pb = new ProcessBuilder("java","-jar",NombrePlugFacturacionElectronica,id,"04",RutaCertificado,
//						AmbienteFacturacionElectronica,
						estado);
			break;
			}
			System.out.println("SE GENERARA LA "+documento+" ELECTRONICA DEL: "+id);
			pb.redirectErrorStream(true);
			if(SO.equals("windows")){
				pb.directory(new File("C:/FirmaDigital"));
		    }else if(SO.equals("linux")){
		    	pb.directory(new File("/FirmaDigital"));
		    }
			//pb.directory("/FirmaDigital/");
	    	System.out.println("PROCESO: " + pb.toString());
	    	System.out.println("DIRECTORIO: " + pb.directory().getAbsolutePath());
	    	Process p = pb.start();
	    	InputStream is = p.getInputStream();
	    	BufferedReader br = new BufferedReader(new InputStreamReader(is));
	    	for (String line = br.readLine(); line != null; line = br.readLine()) {
	            System.out.println( line ); // Or just ignore it
	    	}	    	
				p.waitFor();				
			return "Se ha generado su "+documento+ " electronica"; 				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Error al generar su "+documento+" electronica";
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Error al generar su "+documento+" electronica";
		}
	//return "No se ha generado su "+documento+" electronica";			
	}		
	//###################################################
	public String listGridToPDF(LinkedList<String[]> listado, String[] atributos,String NombreEmpresa,String TelefonoCelularEmpresa,String TelefonoConvencionalEmpresa, String DireccionEmpresa, String Logo){
		System.out.println("Llego a crear el implements");
		String pathimagen = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/images/catalogo/");
		String pathlogo = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/images/");
		String pathpdf = this.getThreadLocalRequest().getSession().getServletContext().getRealPath("/reportes/");
		System.out.println("path pdf: "+pathpdf);
		System.out.println("path imagen: "+pathimagen);
		ReportePDF reportepdf = new ReportePDF();
		String mensaje;

			try {
				reportepdf.crearReporte(listado, pathpdf, pathimagen, pathlogo,atributos,NombreEmpresa,TelefonoCelularEmpresa,TelefonoConvencionalEmpresa,DireccionEmpresa,Logo);
				mensaje="Generado con exitor";
				return mensaje;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		return "No Generado con exitor";				
	}		
	
	/**
	 * METODO PARA LISTAR LOS PEDIDOS DEL RESTAURANT
	 * 
	 */
		
	public List<PedidoProductoelaboradoDTO> listarPedidos(String busqueda, String fechainicial, String fechafinal, int banderafiltro){
		List<PedidoProductoelaboradoDTO> listpedidoprodcutoelaboradodto = new ArrayList<PedidoProductoelaboradoDTO>();
		TblpedidoTblproductoelaboradoHome tblpedidotebltblproductoelaboradohome = new TblpedidoTblproductoelaboradoHome();
		TblempleadoHome tblempleadohome = new TblempleadoHome();
		TblpersonaHome tblpersonahome = new TblpersonaHome();
		TblmesaHome tblmesahome = new TblmesaHome();
		TblareaHome tblareahome = new TblareaHome();
		TblpedidoHome tblpedidohome = new TblpedidoHome();
		TblproductoHome tblproductohome = new TblproductoHome();
		TblproductoelaboradoHome tblproductoelaborado = new TblproductoelaboradoHome();

		Iterator iteratortblpedidotblproductoelaborado = null;
		List<TblpedidoTblproductoelaborado> listtblpedidotblproductoelaborado = new ArrayList<TblpedidoTblproductoelaborado>();
		
		Tblpedido tblpedido;
		Tblproducto tblproducto;
		Tblmesa tblmesa;
		Tblarea tblarea;
		Tblempleado tblempleado;
		Tblpersona tblpersona;
		
		switch(banderafiltro){
		case 0://Mesero
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal
//					+" 23:59:59' and p.tblempleado.tblpersona.nombres like '%"+busqueda+"%' or p.tblempleado.tblpersona.razonSocial like '%"+busqueda+"%'" );
					+" 23:59:59' and p.tblempleado.tblpersona.nombreComercial like '%"+busqueda+"%' or p.tblempleado.tblpersona.razonSocial like '%"+busqueda+"%'" );
			break;
		case 1://Num mesa
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal
					+" 23:59:59' and p.tblmesa.nombreMesa='"+busqueda+"'");
			break;
		case 2://Num pedido
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal
					+" 23:59:59' and x.idtblpedidoTblproductoelaborado='"+busqueda+"'");
			break;
		case 3://Todo
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal
					+" 23:59:59'");
			break;
		case 4://Producto
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p join x.tblproducto r where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal+" 23:59:59' and r.descripcion like '%"+busqueda+"%'");						
			break;
		case 5://Producto
			iteratortblpedidotblproductoelaborado=tblpedidotebltblproductoelaboradohome.getList("join x.tblpedido p join x.tblproducto r where p.fecha>'"+fechainicial+" 00:00:00' and p.fecha<'"+fechafinal+" 23:59:59' and r.descripcion like '%"+busqueda+"%'");						
			break;	
		}
		
		while(iteratortblpedidotblproductoelaborado.hasNext()){
				Object [] pair = (Object[]) iteratortblpedidotblproductoelaborado.next();
				TblpedidoTblproductoelaborado tblpedidotblproductoelaborado= new TblpedidoTblproductoelaborado();
				tblpedidotblproductoelaborado=(TblpedidoTblproductoelaborado)pair[0];
				tblpedido= new Tblpedido();
				tblpedido=tblpedidohome.findById(tblpedidotblproductoelaborado.getTblpedido().getIdPedido());
				
				tblmesa = tblmesahome.findById(tblpedido.getTblmesa().getIdMesa());
				tblarea = tblareahome.findById(tblmesa.getTblarea().getIdArea());
				tblmesa.setTblarea(tblarea);
				
				tblempleado= tblempleadohome.findById(tblpedido.getTblempleado().getIdEmpleado());
				tblpersona = tblpersonahome.findById(tblempleado.getTblpersona().getIdPersona());
				tblempleado.setTblpersona(tblpersona);
				tblpedido.setTblmesa(tblmesa);
				tblpedido.setTblempleado(tblempleado);
				
				tblproducto= new Tblproducto();
				tblproducto=tblproductohome.findById(tblpedidotblproductoelaborado.getTblproducto().getIdProducto());
				
				System.out.println("PASAR PEDIDO_PRODUCTOELABORAD: "+tblpedidotblproductoelaborado.getIdtblpedidoTblproductoelaborado()+"-"+ tblpedidotblproductoelaborado.getTblpedido().getIdPedido()+"-"+ tblpedidotblproductoelaborado.getTblproducto().getIdProducto()+"-"+ tblpedidotblproductoelaborado.getObservacion()+"-"+ tblpedidotblproductoelaborado.getCantidad()+"-"+ tblpedidotblproductoelaborado.getEstado());
				listpedidoprodcutoelaboradodto.add(new PedidoProductoelaboradoDTO(tblpedidotblproductoelaborado.getIdtblpedidoTblproductoelaborado(), pasarPedido(tblpedido), pasarProducto(tblproducto), tblpedidotblproductoelaborado.getObservacion(), tblpedidotblproductoelaborado.getCantidad(), tblpedidotblproductoelaborado.getEstado()));
							
		}
		
		return listpedidoprodcutoelaboradodto;
	}
	///METODOS PARA CORE////////////////////////////////////////////////////
	
	@Override
	public String grabarCore(CoreDTO coredto)
			throws IllegalArgumentException {
		String mensaje = "";
		
		TblcoreHome accesocore = new TblcoreHome();
		Tblcore tblcore= new Tblcore();
		
		
		int ban=0;
			try {
				tblcore=accesocore.findByNombre(coredto.getNombreCore(),"nombrecore");
				ban=1;
				mensaje="core ya ingresado";
			} catch (Exception e) {
				ban=0;
			}
			if(ban==0){
				try {
					tblcore=accesocore.findByNombre(coredto.getCodigoCore(),"codigocore");
					ban=1;
					mensaje="core ya ingresado";
				} catch (Exception e) {
					ban=0;
				}
			}
			if(ban==0){
					try {
						//mensaje="entro en el try";
						
						tblcore.setNombreCore(coredto.getNombreCore());
						tblcore.setTipoCore(coredto.getTipoCore());
						tblcore.setPadreCore(coredto.getPadreCore());
						tblcore.setNivelCore(coredto.getNivelCore());
						tblcore.setCodigoCore(coredto.getCodigoCore());
						accesocore.grabar(tblcore);
						mensaje="core Grabado";
					} catch (ErrorAlGrabar e) {
						mensaje=e.getMessage();
					}
					catch(Exception e){
						mensaje="error "+e.getMessage();
					}
			}
		
		return mensaje;
	}
	
	public CoreDTO crearcoreReg(Tblcore tblcore){		
		return new CoreDTO(tblcore.getIdCore(), tblcore.getNombreCore(), tblcore.getTipoCore(), tblcore.getPadreCore(), tblcore.getNivelCore(), tblcore.getCodigoCore());
	}
	
	@Override
	public List<CoreDTO> listarCore(int ini, int fin) {
		List<CoreDTO> coreReg = null;
		
		List<Tblcore> listado = null;
		TblcoreHome accesocore = new TblcoreHome();
		try {
			listado=accesocore.getList(ini,fin);
			coreReg = new ArrayList<CoreDTO>();
            if (listado != null) {
                for (Tblcore tblcore : listado) {
                	coreReg.add(crearcoreReg(tblcore));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		
		return coreReg;
	}
	public int numeroRegistrosCore(String tabla) {
		int registros=0;
		
		TblcoreHome accesocore = new TblcoreHome();
		try {
				registros=accesocore.count(tabla);
		} catch (RuntimeException re) {
			throw re;	
		} finally {
		}
		
		return registros;
	}
	@Override
	public String eliminarCore(CoreDTO coredto) {
		String mensaje = "";
		TblcoreHome accescore = new TblcoreHome();
		
		try {
			try {
				accescore.eliminacionFisica(new Tblcore(coredto));
				mensaje = "Objeto eliminador con �xito realizado con \u00e9xito";
			} catch (ErrorAlGrabar e) {
				mensaje="Elemento no Eliminado";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		
		return mensaje;
	}
	@Override
	public String modificarCore(CoreDTO coredto) {
		String mensaje = "";
		
		TblcoreHome accesocore = new TblcoreHome();
		
		try {
			try {
				accesocore.modificar(new Tblcore(coredto));
				mensaje = "Cuenta Modificada";
			} catch (ErrorAlGrabar e) {
				mensaje="No se pudo grabar el dato";
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		
		return mensaje;
	}
	@Override
	public CoreDTO buscarCore(String idcore) {
		CoreDTO coredto= new CoreDTO();
		
		TblcoreHome accesocore = new TblcoreHome();
		Tblcore tblcore= new Tblcore();
		
		coredto=null;
		String mensaje = "";
		try {
			try {
				tblcore=accesocore.findById(Integer.valueOf(idcore));
				
				coredto=new CoreDTO();
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		
		return coredto;
	}
	@Override
	public List<CoreDTO> listarCoreLike(String busqueda, String campo) {
		List<CoreDTO> coreReg = null;
		
		List<Tblcore> listado = null;
		TblcoreHome acceso = new 	TblcoreHome();
		try {
			listado=acceso.findLike(busqueda, campo);
			coreReg = new ArrayList<CoreDTO>();
            if (listado != null) {
                for (Tblcore tblcore : listado) {
                	coreReg.add(crearcoreReg(tblcore));
                }
            }
		} catch (RuntimeException re) {
			throw re;
		} 
		
		return coreReg;
	}
	@Override
	public CoreDTO buscarCoreNombre(String idCuenta, String campo) {
		CoreDTO coredto= new CoreDTO();
		
		TblcoreHome accesocore = new TblcoreHome();
		Tblcore tblcore= new Tblcore();
		
		tblcore=null;
		String mensaje = "";
		try {
			try {
				tblcore=accesocore.findByNombre(idCuenta, campo);
				coredto=crearcoreReg(tblcore);
				mensaje = "Busqueda realizada con \u00e9xito";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				mensaje="No se pudo recuperar";
				
			}
			
		} catch (RuntimeException re) {
			mensaje = re.getMessage();
			throw re;	
		} finally {
		}
		
		return coredto;
	}
	
	
	
	public PedidoDTO pasarPedido(Tblpedido tblpedido){
		System.out.println("PASAR Pedido: "+tblpedido.getTblmesa().getIdMesa()+"-"+ tblpedido.getFechaInicio()+"-"+ tblpedido.getTblempleado().getIdEmpleado());
		PedidoDTO  pedidodto = new PedidoDTO();
		//System.out.println("ID: "+tblpedido.getIdPedido());		
		pedidodto.setIdPedido(tblpedido.getIdPedido());
		//System.out.println("MESA: "+tblpedido.getTblmesa().getIdMesa()+"-"+tblpedido.getTblmesa().getNombreMesa()+"-"+tblpedido.getTblmesa().getEstado()+"-"+tblpedido.getTblmesa().getImagen()+"-"+tblpedido.getTblmesa().getImagenOcupado()+"-"+tblpedido.getTblmesa().getX()+"-"+tblpedido.getTblmesa().getY());
		pedidodto.setTblmesa(pasarMesa(tblpedido.getTblmesa()));
		//System.out.println("EMPLEADO: "+tblpedido.getTblempleado().getIdEmpleado());
		pedidodto.setTblempleado(pasarEmpleado(tblpedido.getTblempleado()));
		//System.out.println("FECHA: "+tblpedido.getFecha());
		pedidodto.setFecha(tblpedido.getFechaInicio());
		return pedidodto;
	}
	
	public MesaDTO pasarMesa(Tblmesa tblmesa){
		System.out.println("PASAR MESA:"+"aREA"+"-"+ tblmesa.getNombreMesa()+"-"+ tblmesa.getEstado()+"-"+ tblmesa.getImagen()+"-"+ tblmesa.getImagenOcupado()+"-"+ tblmesa.getX()+"-"+ tblmesa.getY());
		MesaDTO mesadto= new MesaDTO();
		mesadto.setIdEmpresa(tblmesa.getIdEmpresa());
		//System.out.println("AREA: "+tblmesa.getTblarea().getIdArea()+"-"+tblmesa.getTblarea().getNombre()+"-"+ tblmesa.getTblarea().getImagen()+"-"+ tblmesa.getTblarea().getImagenOcupado());
		mesadto.setTblarea(pasarArea(tblmesa.getTblarea()));
		//System.out.println("ID: "+tblmesa.getIdMesa());
		mesadto.setIdMesa(tblmesa.getIdMesa());		
		//System.out.println("NOMBRE: "+tblmesa.getNombreMesa());
		mesadto.setNombreMesa(tblmesa.getNombreMesa());
		//System.out.println("ESTADO: "+tblmesa.getEstado());
		mesadto.setEstado(String.valueOf(tblmesa.getEstado()));
		//System.out.println("IMAGEN: "+tblmesa.getImagen());
		mesadto.setImagen(tblmesa.getImagen());
		//System.out.println("IMAGEN O: "+tblmesa.getImagenOcupado());
		mesadto.setImagenOcupado(tblmesa.getImagenOcupado());
		//System.out.println("X: "+tblmesa.getX());
		mesadto.setX(tblmesa.getX());
		//System.out.println("Y: "+tblmesa.getY());
		mesadto.setY(tblmesa.getY());
		return mesadto;
	}
	public AreaDTO pasarArea(Tblarea tblarea){
		System.out.println("PASAR AREA:"+tblarea.getNombre()+"-"+ tblarea.getImagen()+"-"+ tblarea.getImagenOcupado());
		AreaDTO areadto= new AreaDTO();
		areadto.setIdEmpresa(tblarea.getIdEmpresa());
		areadto.setEstablecimiento(tblarea.getEstablecimiento());
		System.out.println("ID: "+tblarea.getIdArea());
		areadto.setIdArea(tblarea.getIdArea());
		System.out.println("NOMBRE: "+tblarea.getNombre());
		areadto.setNombre(tblarea.getNombre());
		System.out.println("IMAGEN: "+tblarea.getImagen());
		areadto.setImagen(tblarea.getImagen());
		System.out.println("IMAGEN O: " +tblarea.getImagenOcupado());
		areadto.setImagenOcupado(tblarea.getImagenOcupado());
		return areadto;
	}
	
	public PersonaDTO pasarPersona(Tblpersona tblpersona){
		System.out.println("PASAR PERSONA:"+ tblpersona.getIdPersona()+"-"+ tblpersona.getCedulaRuc()+"-"+ tblpersona.getNombreComercial()+"-"+  tblpersona.getRazonSocial()+"-"+ tblpersona.getDireccion()+"-"+tblpersona.getTelefono1());
		PersonaDTO personadto = new PersonaDTO();
		//System.out.println("ID: "+tblpersona.getIdPersona());
		personadto.setIdPersona(tblpersona.getIdPersona());
		//System.out.println("CEDULA_RUC: "+tblpersona.getIdPersona());
		personadto.setCedulaRuc(tblpersona.getCedulaRuc());
		//System.out.println("NOMBRES: "+tblpersona.getNombres());
		personadto.setNombreComercial(tblpersona.getNombreComercial());		
		//System.out.println("APELLIDO: "+tblpersona.getApellidos());
		personadto.setRazonSocial(tblpersona.getRazonSocial());		
		//System.out.println("DIRECCION: "+tblpersona.getDireccion());
		personadto.setDireccion(tblpersona.getDireccion());	
		//System.out.println("TELEFONO: "+tblpersona.getTelefono1());
		personadto.setTelefono1(tblpersona.getTelefono1());			
		return personadto;
	}
	
	private PedidoProductoelaboradoDTO pasarPedidoproductoelaborado(TblpedidoTblproductoelaborado tblpedidoTblproductoelaborado, Tblproducto tblproducto, double pvp, Integer idPedido){
		PedidoProductoelaboradoDTO pedidoProductoelaboradoDTO = new PedidoProductoelaboradoDTO();
		pedidoProductoelaboradoDTO.setIdtblpedidoTblproductoelaborado(tblpedidoTblproductoelaborado.getIdtblpedidoTblproductoelaborado());
		pedidoProductoelaboradoDTO.setCantidad(tblpedidoTblproductoelaborado.getCantidad());
		pedidoProductoelaboradoDTO.setEstado(tblpedidoTblproductoelaborado.getEstado());
		pedidoProductoelaboradoDTO.setIdtblpedidoTblproductoelaborado(tblpedidoTblproductoelaborado.getIdtblpedidoTblproductoelaborado());
		pedidoProductoelaboradoDTO.setObservacion(tblpedidoTblproductoelaborado.getObservacion());
		/*
		TblcategoriaHome tblcategoriahome = new TblcategoriaHome();
		Tblcategoria tblcategoria = new Tblcategoria();
		CategoriaDTO  categoriadto = new CategoriaDTO();
		TblunidadHome tblunidadhome = new TblunidadHome();
		Tblunidad tblunidad = new Tblunidad();
		UnidadDTO  unidaddto = new UnidadDTO();
		*/
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdEmpresa(tblproducto.getIdEmpresa());
		productoDTO.setEstablecimiento(tblproducto.getEstablecimiento());
		productoDTO.setIdProducto(tblproducto.getIdProducto());
		System.out.println("ID: "+tblproducto.getIdProducto());
		productoDTO.setTblcategoria(tblproducto.getTblcategoria().getIdCategoria());
		System.out.println("CATEGORIA: "+tblproducto.getTblcategoria().getIdCategoria());
		productoDTO.setTblunidad(tblproducto.getTblunidad().getIdUnidad());
		System.out.println("UNIDAD: "+tblproducto.getTblunidad().getIdUnidad());
		productoDTO.setTblunidad(tblproducto.getTblmarca().getIdMarca());
		System.out.println("MARCA: "+tblproducto.getTblmarca().getIdMarca());
		productoDTO.setCodigoBarras(tblproducto.getCodigoBarras());
		System.out.println("BARRAS: "+tblproducto.getCodigoBarras());
		productoDTO.setDescripcion(tblproducto.getDescripcion());
		System.out.println("DESCRIPCION: "+tblproducto.getDescripcion());
		productoDTO.setStock(0.0);
		//System.out.println("ID: "+);
		productoDTO.setTblmultiImpuesto(crearSetProductoMultiImpuestoDTO(tblproducto.getTblProdMultiImpuesto()));
//		productoDTO.setImpuesto(tblproducto.getImpuesto());
//		System.out.println("IMPUESTO: "+tblproducto.getImpuesto());
		System.out.println("IMPUESTOS: "+tblproducto.getTblProdMultiImpuesto().size());
		for (TblproductoMultiImpuesto imp: tblproducto.getTblProdMultiImpuesto()){
			System.out.println("IMPUESTO: "+imp.getTblmultiImpuesto().getDescripcion());
		}
//		tblproducto.getTblmultiImpuesto().forEach(aImpuesto->System.out.println(aImpuesto.getTblmultiImpuesto().getDescripcion()));
		productoDTO.setLifo(tblproducto.getLifo());
		System.out.println("LIFO: "+tblproducto.getLifo());
		productoDTO.setFifo(tblproducto.getFifo());
		System.out.println("FIFO: "+tblproducto.getFifo());
		productoDTO.setPromedio(tblproducto.getPromedio());
		System.out.println("PROMEDIO: "+tblproducto.getPromedio());
		productoDTO.setEstado(tblproducto.getEstado());
		System.out.println("ESTADO: "+tblproducto.getEstado());
		productoDTO.setDescuento(tblproducto.getDescuento());
		System.out.println("DESCUENTO: "+tblproducto.getDescuento());
		productoDTO.setBono(tblproducto.getBono());
		System.out.println("BONO: "+tblproducto.getBono());
		productoDTO.setImagen(tblproducto.getImagen());
		System.out.println("IMAGEN: "+tblproducto.getImagen());
		productoDTO.setJerarquia(tblproducto.getJerarquia());
		System.out.println("JERARQUIA: "+tblproducto.getJerarquia());
		productoDTO.setCantidadunidad(tblproducto.getCantidadunidad());
		System.out.println("CANTIDAD UNIDAD: "+tblproducto.getCantidadunidad());		
		//productoDTO.setPrecio(10.0);
		ProductoTipoPrecioDTO productotipopreciodto= new ProductoTipoPrecioDTO();
		List<ProductoTipoPrecioDTO> listproductotipoprecio = new ArrayList<ProductoTipoPrecioDTO>();
		productotipopreciodto= new ProductoTipoPrecioDTO();
		productotipopreciodto.setPorcentaje(0.0);
		listproductotipoprecio.add(productotipopreciodto);
		productotipopreciodto= new ProductoTipoPrecioDTO();
		productotipopreciodto.setPorcentaje(pvp);
		listproductotipoprecio.add(productotipopreciodto);
		productotipopreciodto= new ProductoTipoPrecioDTO();
		productotipopreciodto.setPorcentaje(0.0);
		listproductotipoprecio.add(productotipopreciodto);
		productoDTO.setProTip(listproductotipoprecio);
		
		
		pedidoProductoelaboradoDTO.setProductodto(productoDTO);
		 
		PedidoDTO pedidodto = new PedidoDTO();
		MesaDTO mesadto = new MesaDTO();
		mesadto.setIdEmpresa(tblpedidoTblproductoelaborado.getTblpedido().getTblmesa().getIdEmpresa());
		mesadto.setIdMesa(tblpedidoTblproductoelaborado.getTblpedido().getTblmesa().getIdMesa());
		
		pedidodto.setTblmesa(mesadto);
		pedidodto.setIdPedido(idPedido);
		
		pedidoProductoelaboradoDTO.setTblpedido(pedidodto);	
		
		return pedidoProductoelaboradoDTO;
	}
	//################################################################
	public EmpleadoDTO pasarEmpleado(Tblempleado tblempleado){
		System.out.println("PASAR EMPLEADO: "+"-"+"pERSONA"+"-"+tblempleado.getPassword()+"-"+tblempleado.getNivelAcceso()+"-"+tblempleado.getSueldo()+"-"+ tblempleado.getHorasExtras()+"-"+tblempleado.getIngresosExtras()+"-"+ tblempleado.getFechaContratacion());
		EmpleadoDTO empleadodto = new EmpleadoDTO(tblempleado.getIdEmpresa(),tblempleado.getEstablecimiento(),pasarPersona(tblempleado.getTblpersona()),tblempleado.getPassword(), tblempleado.getNivelAcceso(), tblempleado.getIdRol(),tblempleado.getSueldo(), tblempleado.getHorasExtras(),tblempleado.getIngresosExtras(), tblempleado.getFechaContratacion(), tblempleado.getEstado());
		//EmpleadoDTO empleadodto = new EmpleadoDTO(pasarPersona(tblempleado.getTblpersona()),tblempleado.getPassword(), tblempleado.getNivelAcceso(), tblempleado.getSueldo(), tblempleado.getHorasExtras(),tblempleado.getIngresosExtras(), tblempleado.getFechaContratacion());
				
		return empleadodto;
	}
	//##################################################################
	public ProductoDTO pasarProducto(Tblproducto  tblproducto){
		System.out.println("PASAR PRODUCTO: "+tblproducto.getIdProducto()+"-"+tblproducto.getCodigoBarras()+"-"+tblproducto.getDescripcion()+"-"+ tblproducto.getStock()+
//				"-"+tblproducto.getImpuesto()+
				"-"+tblproducto.getLifo()+"-"+tblproducto.getFifo()+"-"+tblproducto.getPromedio() +"-"+tblproducto.getTblunidad().getIdUnidad()+"-"+tblproducto.getTblcategoria().getIdCategoria()+"-"+tblproducto.getTblmarca().getIdMarca()+"-"+tblproducto.getEstado()+"-"+tblproducto.getCantidadunidad());
//		ProductoDTO productodto = new ProductoDTO(tblproducto.getIdProducto(), tblproducto.getCodigoBarras(), tblproducto.getDescripcion(), tblproducto.getStock(), tblproducto.getImpuesto(), tblproducto.getLifo(), tblproducto.getFifo(), tblproducto.getPromedio(), tblproducto.getTblunidad().getIdUnidad(), tblproducto.getTblcategoria().getIdCategoria(),tblproducto.getTblmarca().getIdMarca(), tblproducto.getEstado(), tblproducto.getTipo(),tblproducto.getCantidadunidad());
		ProductoDTO productodto = new ProductoDTO(tblproducto.getIdEmpresa(),tblproducto.getEstablecimiento(),tblproducto.getIdProducto(), 
				tblproducto.getCodigoBarras(), tblproducto.getDescripcion(), tblproducto.getStock(), 
				crearSetProductoMultiImpuestoDTO(tblproducto.getTblProdMultiImpuesto()), tblproducto.getLifo(), tblproducto.getFifo(), 
				tblproducto.getPromedio(), tblproducto.getTblunidad().getIdUnidad(), tblproducto.getTblcategoria().getIdCategoria(),
				tblproducto.getTblmarca().getIdMarca(), tblproducto.getEstado(), tblproducto.getTipo(),tblproducto.getCantidadunidad(),
				tblproducto.getStockMinimo(),tblproducto.getStockMax());
		return productodto;
	}
	//#################################################################3
	public ArrayList<PedidoProductoelaboradoDTO> obtenerPedido(Integer idMesa) {
		ArrayList<PedidoProductoelaboradoDTO> listpedidoprodcutoelaboradodto = new ArrayList<PedidoProductoelaboradoDTO>();
		System.out.println("Entra a metodo obtenerPedido()");
		
		Tblpedido tblpedido = new Tblpedido();
		
		//XAVIER
		Tblmesa tblmesa = new Tblmesa();
		Tblarea tblarea = new Tblarea();
		Tblempleado tblempleado = new Tblempleado();
		Tblpersona tblpersona = new Tblpersona();
		Tblproducto tblproducto = new Tblproducto();
		TblmesaHome tblmesaHome = new TblmesaHome();
		TblareaHome tblareaHome = new TblareaHome();
		TblempleadoHome tblempleadoHome = new TblempleadoHome();
		TblpersonaHome tblpersonaHome = new TblpersonaHome();
		TblproductoHome tblproductoHome = new TblproductoHome();
		//XAVIER
		
		TblpedidoHome tblpedidoHome = new TblpedidoHome();
		
		PedidoProductoelaboradoDTO PedidoProductoelaboradoDTO;
		TblpedidoTblproductoelaboradoHome tblpedidoTblproductoelaboradoHome = new TblpedidoTblproductoelaboradoHome();
		TblpedidoTblproductoelaborado tblpedidoTblproductoelaborado;
		
		//Iterator iteratorpedido = tblpedidoHome.findByQuery("join u.tblmesa m");
		//Iterator iteratorpedido = tblpedidoHome.findByQuery("join u.tblmesa m where m.idMesa='"+idMesa+"'");
		Iterator iteratorpedido = tblpedidoHome.getList("join x.tblmesa m where m.idMesa='"+idMesa+"' and x.tblmesa.idMesa=m.idMesa order by x.idPedido desc");
		if(iteratorpedido.hasNext()){
				Object[] pair = (Object[])iteratorpedido.next();
				tblpedido = (Tblpedido) pair[0];
		}
		
		//tblpedido = tblpedidoHome.getLast("u join u.tblmesa m where m.idMesa='"+idMesa+"'", "u.fecha");
		Integer idPedido = tblpedido.getIdPedido();
		
		System.out.println("idPedido: "+idPedido);
		
		Iterator iterador = tblpedidoTblproductoelaboradoHome.getList("join x.tblpedido p where p.idPedido ='"+idPedido+"' and x.tblpedido.idPedido=p.idPedido");
		
		if(iterador.hasNext()){
			while(iterador.hasNext()){
				Object[] pair = (Object[])iterador.next();
				
				tblpedidoTblproductoelaborado =(TblpedidoTblproductoelaborado) pair[0];
				System.out.println("idPedidoProducto: "+tblpedidoTblproductoelaborado.getIdtblpedidoTblproductoelaborado());			
				//tblproducto = tblpedidoTblproductoelaborado.getTblproducto();
				tblproducto = tblproductoHome.findById(tblpedidoTblproductoelaborado.getTblproducto().getIdProducto());
				System.out.println("Informacion de producto. Descripcion: "+tblproducto.getDescripcion()+ " Id: "+tblproducto.getIdProducto()+" Imagen: "+tblproducto.getImagen()
//				+" Impuesto: "+tblproducto.getImpuesto()
				);
				System.out.println("                 IMPUESTOS: "+tblproducto.getTblProdMultiImpuesto().size());
				for (TblproductoMultiImpuesto imp: tblproducto.getTblProdMultiImpuesto()){
					System.out.println("                IMPUESTO: "+imp.getTblmultiImpuesto().getDescripcion());
				}
				//System.out.println("idProducto: "+tblpedidoTblproductoelaborado.getTblproducto().getIdProducto());
				double pvp=0;
				 
				Iterator iteratorproducto= tblproductoHome.getList("join x.tblcategoria c, com.giga.factum.server.base.TblproductoTipoprecio t  where x.jerarquia='1' and t.tbltipoprecio.idTipoPrecio='2' and t.tblproducto.idProducto="+tblpedidoTblproductoelaborado.getTblproducto().getIdProducto()+" order by c.idCategoria ");
				
				if(iteratorproducto.hasNext()){
	                while(iteratorproducto.hasNext()){
	                        Object[] pairProducto = (Object[])iteratorproducto.next();
	                        TblproductoTipoprecio tblproductotipoprecio = (TblproductoTipoprecio) pairProducto[2]; 
	                        
	                        pvp = tblproductotipoprecio.getPorcentaje();
	                        System.out.println("PVP: "+pvp);
	               }
				}
				
				PedidoProductoelaboradoDTO = pasarPedidoproductoelaborado(tblpedidoTblproductoelaborado, tblproducto, pvp, idPedido);
				//PedidoProductoelaboradoDTO.setProductodto(productodto);
				listpedidoprodcutoelaboradodto.add(PedidoProductoelaboradoDTO);
			}
		}
		
		//System.out.println("Antes de salir de metodo obtenerPedido(), listaSize: "+listpedidoprodcutoelaboradodto.size());
		return listpedidoprodcutoelaboradodto;
	}	
	//##################################################################
	/**
	 * Funcion para modificar los platos del pedido de una mesa
	 * Se cambia el estado de los platos del pedido anterior a 4 (anulado)
	 * y se carga el nuevo pedido
	 */
	public String modificarPedido(ArrayList<PedidoProductoelaboradoDTO> listapedidoproductoelaboradodto){
		if(!listapedidoproductoelaboradodto.isEmpty()){
		TblpedidoTblproductoelaboradoHome tblpedidoTblproductoelaboradoHomeAModificar = new TblpedidoTblproductoelaboradoHome();
		TblpedidoTblproductoelaborado tblpedidoTblproductoelaboradoAModificar;
		
		Integer idPedidoAModificar=listapedidoproductoelaboradodto.get(0).getTblpedido().getIdPedido();
		Integer IdPedido=0;
		
		Iterator iteratorPedidoProductoelaborado;
		iteratorPedidoProductoelaborado = tblpedidoTblproductoelaboradoHomeAModificar.getList("where x.tblpedido.idPedido="+idPedidoAModificar);
		
		int contador=0;
		
		for(PedidoProductoelaboradoDTO productoelaboradodto: listapedidoproductoelaboradodto){
			Tblpedido tblpedido = new  Tblpedido();
			tblpedido.setIdPedido(productoelaboradodto.getTblpedido().getIdPedido());
			Tblproducto tblproducto = new Tblproducto();
			tblproducto.setIdProducto(productoelaboradodto.getTblproducto().getIdProducto());
			tblpedidoTblproductoelaboradoAModificar= new TblpedidoTblproductoelaborado(productoelaboradodto.getIdtblpedidoTblproductoelaborado(), tblpedido, tblproducto);
			tblpedidoTblproductoelaboradoAModificar.setCantidadFacturada(productoelaboradodto.getCantidadFacturada());
			tblpedidoTblproductoelaboradoAModificar.setCantidad(productoelaboradodto.getCantidad());
			tblpedidoTblproductoelaboradoAModificar.setObservacion(productoelaboradodto.getObservacion());
				if(productoelaboradodto.getCantidad()==productoelaboradodto.getCantidadFacturada()){
					tblpedidoTblproductoelaboradoAModificar.setEstado('1');
				}else{
					tblpedidoTblproductoelaboradoAModificar.setEstado('0');
					contador = contador+1;
				}
			System.out.println("IdPedidoAModificar: "+idPedidoAModificar+" iterador");
			try {
				tblpedidoTblproductoelaboradoHomeAModificar.modificar(tblpedidoTblproductoelaboradoAModificar);
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		if(contador==0){
		Tblmesa tblmesarecuperada;
		TblmesaHome tblmesaHome = new TblmesaHome();
		Tblproducto tblproducto;
		
		tblmesarecuperada = tblmesaHome.findById(listapedidoproductoelaboradodto.get(0).getTblpedido().getTblmesa().getIdMesa());
		tblmesarecuperada.setEstado('0');
		
			try {
				tblmesaHome.modificar(tblmesarecuperada);
				return "Pedido guardado";
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "Error a guardar Pedido";
	}else{
		return "Noy pedido que modificar";
	}
	}
	//##################################################################
	public ArrayList<FacturaProduccionDTO> listarFacturasProduccion(String fecha){
		System.out.println(fecha);
		String[] fechas = fecha.split("/");
		String dia=fechas[0];
		String mes=fechas[1];
		String anio=fechas[2];
		//fecha=+"-"++"-"+;
		ArrayList<FacturaProduccionDTO> listvwfacturasproduccionid = new ArrayList<FacturaProduccionDTO>();
		VwFacturasProduccionIdHome vwfacturasproduccionidhome = new VwFacturasProduccionIdHome();
		String consulta="select fp.id.categoria as categoria, sum(fp.id.total) as total "+
		"from VwFacturasProduccion fp "+
		"where fp.id.fecha between '"+fecha+" 00:00:00' and '"+fecha+" 23:59:59' "+
		"group by fp.id.categoria";
		Iterator iterator=vwfacturasproduccionidhome.listByQuery(consulta);
		String categoria="";
		Double total;
		while (iterator.hasNext()){
		Object[] obj = (Object[])iterator.next();
		categoria="";
		total=0.0;
			try{
				categoria = (String)obj[0];
				total = (Double)obj[1];
			}
			catch (Exception e) {
			 categoria="";
			 total=0.0;
			 //total=0.0;
			}
		FacturaProduccionDTO refacturaproducciondto = new FacturaProduccionDTO();
		refacturaproducciondto.setCategoria(categoria);
		refacturaproducciondto.setTotal(total);
		listvwfacturasproduccionid.add(refacturaproducciondto);
			
		}
		return listvwfacturasproduccionid;
	}
public List<DtocomercialDTO> obtenerCuentasPorCobrar() {
		
		List<DtocomercialDTO> cuentascob = new ArrayList<DtocomercialDTO>();
		TbldtocomercialHome accesodtocomercial = new TbldtocomercialHome();
		
		TblpersonaHome accesopersona = new TblpersonaHome();
		Tblpersona tblpersonacargar= new Tblpersona();
		DtocomercialDTO documento=new DtocomercialDTO();
		PersonaDTO personadto = new PersonaDTO();
		Tblbodega tblbodega= new Tblbodega();
		TblbodegaHome accesoBodega=new TblbodegaHome();
		
		Tblproducto tblproducto=new Tblproducto();
		TblproductoHome accesoproducto=new TblproductoHome();
		TbldtocomercialdetalleHome accesodetalle=new TbldtocomercialdetalleHome();
		
		BodegaDTO bodegadto= new BodegaDTO();
		ProductoDTO productodto= new ProductoDTO();
		DtoComDetalleDTO dtocomdetalle=new DtoComDetalleDTO();
		
		TblpagoHome accesopago=new TblpagoHome();
		List<Tblpago> tblpagolist= new ArrayList<Tblpago>();
		TblpagoDTO tblpagodto=new TblpagoDTO();
		Tblpago tblpago=new Tblpago();
		List<Tbldtocomercialdetalle>listtbldtocomercialdetalle=new ArrayList<Tbldtocomercialdetalle>();;
	    List<Tblbodega>listbodega= new ArrayList<Tblbodega>();
	    List<Tblproducto>listproducto= new ArrayList<Tblproducto>();
	  
		
	    Iterator iterator=accesodtocomercial.getList("join x.tblpagos p where p.estado = '0' or p.estado = '1' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
    	Tbldtocomercial tbldtocomercial;
    	if(iterator.hasNext()){
    	while (iterator.hasNext()) {
    		Object[] pair = (Object[])iterator.next();//recupera el iterador 
    			Set<TblpagoDTO> listpagodto=new HashSet<TblpagoDTO>();
    			tbldtocomercial = (Tbldtocomercial) pair[0];//recupera tbldtocomercial en posicion 0
				tblpago = (Tblpago) pair[1];//recupera tbldtocomercial en posicion 1
		
				documento = new DtocomercialDTO();
				tblpersonacargar=accesopersona.findById(tbldtocomercial.getTblpersonaByIdPersona().getIdPersona());
				personadto = new PersonaDTO(tblpersonacargar.getIdPersona(), tblpersonacargar.getCedulaRuc(), tblpersonacargar.getNombreComercial(), tblpersonacargar.getRazonSocial(), tblpersonacargar.getDireccion(), tblpersonacargar.getTelefono1());
				//tblpagolist=accesopago.getList();
				
				tblpagodto = new TblpagoDTO(tblpago.getIdEmpresa(),tblpago.getEstablecimiento(),documento, tblpago.getFechaVencimiento(), tblpago.getFechaRealPago(), tblpago.getValor(), tblpago.getEstado(),tblpago.getFormaPago());
				listpagodto.add(tblpagodto);

				Set<DtoComDetalleDTO> dtocomdetalledto =  new HashSet<DtoComDetalleDTO>();
				listtbldtocomercialdetalle=accesodetalle.findByQuery("where u.tbldtocomercial.idDtoComercial='"+tbldtocomercial.getIdDtoComercial()+"'");
				for(Tbldtocomercialdetalle tbldtocomercialdetalle: listtbldtocomercialdetalle){
					bodegadto=new BodegaDTO();
					bodegadto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
					bodegadto.setEstablecimiento(tbldtocomercial.getEstablecimiento());
					bodegadto.setIdBodega(tbldtocomercialdetalle.getTblbodega().getIdBodega());
					productodto= new ProductoDTO();
					productodto.setIdEmpresa(tbldtocomercial.getIdEmpresa());
					productodto.setEstablecimiento(tbldtocomercial.getEstablecimiento());
					productodto.setIdProducto(tbldtocomercialdetalle.getTblproducto().getIdProducto());
					dtocomdetalle= new DtoComDetalleDTO(tbldtocomercialdetalle.getIdDtoComercialDetalle(), bodegadto, productodto, tbldtocomercialdetalle.getCantidad(), tbldtocomercialdetalle.getPrecioUnitario(), tbldtocomercialdetalle.getDepartamento(), 
//							tbldtocomercialdetalle.getImpuesto(),
							tbldtocomercialdetalle.getTotal(), tbldtocomercialdetalle.getDesProducto());				
					dtocomdetalledto.add(dtocomdetalle);
				}
				documento.setIdEmpresa(tbldtocomercial.getIdEmpresa());
				documento.setEstablecimiento(tbldtocomercial.getEstablecimiento());
				documento.setPuntoEmision(tbldtocomercial.getPuntoEmision());
				documento.setTblpagos(listpagodto);
				documento.setTbldtocomercialdetalles(dtocomdetalledto);
				documento.setTblpersonaByIdPersona(personadto);
				documento.setIdDtoComercial(tbldtocomercial.getIdDtoComercial());
				documento.setTipoTransaccion(tbldtocomercial.getTipoTransaccion());
				documento.setFecha(tbldtocomercial.getFecha().toString());
				documento.setSubtotal(tbldtocomercial.getSubtotal());
				cuentascob.add(documento);
    		}
    	}			
		return cuentascob;			
	}
/*
 * METODO PARA LISTAR ELECTRONICOS
 * 
 */
public List<DtoelectronicoDTO> listarElectronicoVendedorCliente(String FechaI, String FechaF,String idCliente,String idVendedor,String Tipo) {				
	List<DtoelectronicoDTO> list = new ArrayList<DtoelectronicoDTO>();
    try {
    	String fechaI=FechaI;
		String fechaF=FechaF;
		TbldtoelectronicoHome acceso = new TbldtoelectronicoHome();
		TblpersonaHome accesopersona = new TblpersonaHome();
		TbldtocomercialHome tbldtocomercialhome = new TbldtocomercialHome();
		TbldtocomercialdetalleHome tbldtocomercialdetallehome = new TbldtocomercialdetalleHome();  
		Iterator resultado = acceso.ReproteCajaVendedorCliente(fechaI, fechaF,idVendedor,idCliente,Tipo);
		
		while ( resultado.hasNext() ) { 
			Object[] pairdoc = (Object[])resultado.next();				
			Tbldtoelectronico tblele = (Tbldtoelectronico) pairdoc[0];
			if(tblele!=null){
				Tblpersona tblpersona= new Tblpersona();
				DtoelectronicoDTO docele = new DtoelectronicoDTO();
				docele.setIdEmpresa(tblele.getIdEmpresa());
				docele.setEstablecimiento(tblele.getEstablecimiento());
				docele.setPuntoEmision(tblele.getPuntoEmision());
				docele.setAmbiente(tblele.getAmbiente());
				docele.setAutorizacion(tblele.getAutorizacion());
				docele.setClave(tblele.getClave());
				docele.setEstado(tblele.getEstado());				
				docele.setFechaautorizacion(tblele.getFechaautorizacion());
				docele.setIddtoelectronico(tblele.getIddtoelectronico());
				docele.setNombreRide(tblele.getNombreRide());
				docele.setNombreXml(tblele.getNombreXml());
				Tbldtocomercial tblcom = tbldtocomercialhome.findById(tblele.getTbldtocomercial().getIdDtoComercial());
				
				DtocomercialDTO docdto = new DtocomercialDTO();
				docdto.setIdEmpresa(tblcom.getIdEmpresa());
				docdto.setEstablecimiento(tblcom.getEstablecimiento());
				docdto.setPuntoEmision(tblcom.getPuntoEmision());
				docdto.setAutorizacion(tblcom.getAutorizacion());
				docdto.setEstado(tblcom.getEstado());
				docdto.setExpiracion(String.valueOf(tblcom.getExpiracion()));
				docdto.setFecha(String.valueOf(tblcom.getFecha()));
				docdto.setIdDtoComercial(tblcom.getIdDtoComercial());
				docdto.setNumPagos(tblcom.getNumPagos());
				docdto.setNumRealTransaccion(tblcom.getNumRealTransaccion());
				docdto.setSubtotal(tblcom.getSubtotal());
				docdto.setTipoTransaccion(tblcom.getTipoTransaccion());
				tblpersona= new Tblpersona();
				tblpersona=accesopersona.findById(tblcom.getTblpersonaByIdPersona().getIdPersona());
				PersonaDTO perC=new PersonaDTO();
				perC.setIdPersona(tblpersona.getIdPersona());
				perC.setRazonSocial(tblpersona.getRazonSocial());
				perC.setNombreComercial(tblpersona.getNombreComercial());
				docdto.setTblpersonaByIdPersona(perC);
				tblpersona= new Tblpersona();
				tblpersona=accesopersona.findById(tblcom.getTblpersonaByIdVendedor().getIdPersona());
				PersonaDTO perV=new PersonaDTO();
				perV.setIdPersona(tblpersona.getIdPersona());
				perV.setRazonSocial(tblpersona.getRazonSocial());
				perV.setNombreComercial(tblpersona.getNombreComercial());
				docdto.setTblpersonaByIdVendedor(perV);	
				
				DtoComDetalleDTO dtocomdetalledto =new DtoComDetalleDTO();
				Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>();
				System.out.println("Detalles Documento: "+ tblcom.getTbldtocomercialdetalles().size());
				Iterator resultadodetalles = tblcom.getTbldtocomercialdetalles().iterator();					
				while(resultadodetalles.hasNext()){						
					Tbldtocomercialdetalle docdet = (Tbldtocomercialdetalle) resultadodetalles.next();
					if (docdet!=null){
						
						Tbldtocomercialdetalle tbldtocomercialdetalle  = new Tbldtocomercialdetalle();
						tbldtocomercialdetalle=tbldtocomercialdetallehome.findById(docdet.getIdDtoComercialDetalle());
						dtocomdetalledto =new DtoComDetalleDTO(tbldtocomercialdetalle.getDepartamento(), 
//								tbldtocomercialdetalle.getImpuesto(),
								tbldtocomercialdetalle.getTotal()); 
								
						tbldtocomercialdetalles.add(dtocomdetalledto);							
					}
				}
				docdto.setTbldtocomercialdetalles(tbldtocomercialdetalles);
				docele.setDtocomercialDTO(docdto);
				list.add(docele);
				
			}
		}							
    } catch (Exception e1) {
		System.out.println("Error al convertir fecha listarElectronicoVendedorCliente: "+e1);
	}
	return list;
}


@Override
public String modificarCuenta(CuentaDTO cuenta, String planCuentaID) {
	TblcuentaHome accesocuenta = new TblcuentaHome();
	TblplancuentaHome tblplancuentahome = new TblplancuentaHome();
	Tblplancuenta tblplancuenta = new Tblplancuenta();
	String mensaje = "";
	try {
		try {
			Tblcuenta tblcuenta = new Tblcuenta(cuenta);
			Set<Tblplancuenta> tblplancuentas = new HashSet<Tblplancuenta>(0);				
			tblplancuenta=tblplancuentahome.findById(Integer.parseInt(planCuentaID));
			tblplancuentas.add(tblplancuenta);
			tblcuenta.setTblplancuentas(tblplancuentas);
			accesocuenta.modificar(tblcuenta);
			mensaje = "Cuenta Modificada";
		} catch (ErrorAlGrabar e) {
			mensaje="No se pudo grabar el dato";
		}
		
	} catch (RuntimeException re) {
		mensaje = re.getMessage();
		throw re;	
	} finally {
	}
	return mensaje;
}


@Override
public int ingresar() {
	// TODO Auto-generated method stub
	ingresos+=1;
	return ingresos;
}


@Override
public String[] grabarDocumentosDiv(List<DocumentoSaveDTO> documentos, String userName, String planCuenta,
		int banderaConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales) {
	System.out.println("Factura en division " +documentos.size());
	String[] resultados=new String[documentos.size()];
	int i=0;
	for (DocumentoSaveDTO documento: documentos){
		System.out.println("Factura en division antes de grabar " +documento.getDocumento().getNumRealTransaccion());
		resultados[i]=grabarDocumento(documento.getDocumento(), documento.getAfeccion(), documento.getTotalIva(), documento.getAnulacion(), userName, planCuenta, banderaConfiguracionTipoFacturacion, banderaIVA, banderaNumeroDecimales);
		System.out.println("Factura en division resultado " +resultados[i]);
		i++;
	}
	
	return resultados;
}

/*@Override
public String buscarXML(FileItem arch){
	
	String respuestaRet=null;
	
	final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
	final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

	DiskFileItemFactory factory = new DiskFileItemFactory();
	factory.setSizeThreshold(THRESHOLD_SIZE);
	factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

	ServletFileUpload upload = new ServletFileUpload(factory);
	upload.setFileSizeMax(MAX_FILE_SIZE);
	upload.setSizeMax(MAX_REQUEST_SIZE);

	// constructs the directory path to store upload file
	String uploadPath = getServletContext().getRealPath("")
			//+ File.separator
	// + UPLOAD_DIRECTORY
	;
	
	System.out.println("ContextRealPath " + uploadPath);
	//List formItems;
	try {
		//formItems = upload.parseRequest(request);

		//FileItem path = (FileItem) formItems.get(0);
		//uploadPath += path.getFieldName();
		String separador=Matcher.quoteReplacement(File.separator);
		
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		//FileItem imagn = (FileItem) formItems.get(1);
		//FileItem arch=(FileItem)dynXML.getItem("CARGAR XML");
		
		if (!arch.isFormField()) {
			System.out.println("FileItem "+arch.getName());

			String fileName = new File(arch.getName()).getName();
			String filePath = uploadPath + separador + fileName;
			File storeFile = new File(filePath);
			
			// saves the file on disk
			arch.write(storeFile);
		}
		// creates the directory if it does not exist
		System.out.println(uploadPath);
		
		respuestaRet= uploadPath;
		//http://localhost:9192/readxml?file=C:/Users/Usuario/Downloads/Factura.xml
		
		
		//out.println("Subido correctamente");
	} catch (FileUploadException e) {
		// TODO Auto-generated catch block
		//out.println("Error: "+e.getMessage());
		e.printStackTrace();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//out.println("Error: "+e.getMessage());
		e.printStackTrace();
		
	}finally{
		return respuestaRet;
	}
	//out.close();
}*/

@Override
public String eliminarFile(String url){
	
	//File storeFile = new File(url);
	String resultado="";
	// saves the file on disk
	
		//String[] divs=GWT.getModuleBaseURL().split("\\/");
	String[] divs=url.split("\\/");
		String ppath=divs[divs.length-2]+"\\"+divs[divs.length-1];
		String uploadPath = getServletContext().getRealPath("");
		System.out.println(uploadPath);
		String separador=Matcher.quoteReplacement(File.separator);
		//String[] pathParte=uploadPath.split(separador);
		System.out.println(ppath);
		/*pathParte[pathParte.length-1]=ppath.replaceAll("\\/",separador);
		uploadPath=String.join(separador, pathParte);*/
		//String filePath = uploadPath +/*+ url*/ppath.replaceAll("\\/",separador);
		//String filePath = (separador.equals("\\/"))?uploadPath +/*+ url*/ppath.replaceAll("\\\\",separador):uploadPath +/*+ url*/ppath.replaceAll("\\/",separador);
		String filePath =uploadPath+FilenameUtils.separatorsToSystem(ppath);
		File storeFile = new File(filePath);
		System.out.println("En servidor eliminando archivo El archivo si existe "+uploadPath + " "+storeFile.exists());
		
	while (storeFile.exists()) {
		resultado="El archivo si existe";
		storeFile.delete();
		System.out.println("Se elimino aun existe: "+storeFile.exists());
		resultado=String.valueOf("Se elimino aun existe: "+storeFile.exists());
	}
	
	return resultado;
}


@Override
public TblpagoDTO BuscarPagoIdComercial(int idDtoComercial) {
	TblpagoDTO pagoD=new TblpagoDTO();
	try{
		TblpagoHome acceso = new TblpagoHome();
		Iterator resultado = acceso.BuscarPagoIdComercial(idDtoComercial);
		while ( resultado.hasNext() ) {
			Object[] pair = (Object[])resultado.next();
			Tblpago pago = (Tblpago) pair[0];
			Tbldtocomercial doc = (Tbldtocomercial) pair[1];
			Tblpersona persona=(Tblpersona) pair[2];
			PersonaDTO personaD = new PersonaDTO();
			personaD = crearPersonaDTO(persona);
			personaD.setIdPersona(persona.getIdPersona());
			DtocomercialDTO docDTO =new DtocomercialDTO();
			docDTO=this.crearDtocomercialDTO(doc);
			docDTO.setTblpersonaByIdPersona(personaD);
			docDTO.setTblpersonaByIdVendedor(personaD);
			pagoD.setIdEmpresa(pago.getIdEmpresa());
			pagoD.setEstablecimiento(pago.getEstablecimiento());
			pagoD.setNumEgreso(pago.getNumEgreso());
			pagoD.setConcepto(pago.getConcepto());
			pagoD.setFormaPago(pago.getFormaPago());
			pagoD.setIdPago(pago.getIdPago());
			pagoD.setEstado(pago.getEstado());
			pagoD.setFechaRealPago(pago.getFechaRealPago());
			pagoD.setFechaVencimiento(pago.getFechaVencimiento());
			pagoD.setTbldtocomercial(docDTO);
			pagoD.setValor(pago.getValor());
		}
	}catch(Exception e){
		System.out.println("Error buscarpagoID "+e.getMessage());
	}
	return pagoD;
}

}
	
