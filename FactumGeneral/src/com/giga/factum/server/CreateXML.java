package com.giga.factum.server;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.giga.factum.client.DTO.detalleVentasATSDTO;

public class CreateXML {
	public  CreateXML() {
		
	}
    
    public void ATSVentasXML(LinkedList<detalleVentasATSDTO> atsVentas,String name){
    	try {
    		System.out.println("ENTRO EN LA FUNCION");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
		    DOMImplementation implementation = builder.getDOMImplementation();
	        Document document = implementation.createDocument(null, name, null);
	        document.setXmlVersion("1.0");
	      //Main Node
	        System.out.println("INICIO CABECERA");
            Element raiz = document.getDocumentElement();
            
            Element tipoIDInformante = document.createElement("TipoIDInformante"); 
            Text tipoIDInformanteText = document.createTextNode("R");
            tipoIDInformante.appendChild(tipoIDInformanteText);
            raiz.appendChild(tipoIDInformante);
            
            Element idInformante = document.createElement("IdInformante"); 
            Text idInformanteText = document.createTextNode("0105594394001");
            idInformante.appendChild(idInformanteText);
            raiz.appendChild(idInformante);
            
            Element razonSocial = document.createElement("razonSocial"); 
            Text razonSocialText = document.createTextNode("ISRAEL PESANTEZ");
            razonSocial.appendChild(razonSocialText);
            raiz.appendChild(razonSocial);
            
            Element Anio = document.createElement("Anio"); 
            Text AnioText = document.createTextNode("2014");
            Anio.appendChild(AnioText);
            raiz.appendChild(Anio);
            
            Element Mes = document.createElement("Mes"); 
            Text MesText = document.createTextNode("08");
            Mes.appendChild(MesText);
            raiz.appendChild(Mes);
            
            Element numEstabRuc = document.createElement("numEstabRuc"); 
            Text numEstabRucText = document.createTextNode("001");
            numEstabRuc.appendChild(numEstabRucText);
            raiz.appendChild(numEstabRuc);
            double totalV=0;
            DecimalFormat df = new DecimalFormat("0.00"); 
            for(int i=0;i<atsVentas.size();i++){
            	totalV=totalV+atsVentas.get(i).getBaseImponible()+atsVentas.get(i).getBaseImpGrav();
            }
            String tot=String.valueOf(df.format(totalV)).replace(",", ".");
            
            
            Element totalVentas = document.createElement("totalVentas"); 
            Text totalVentasText = document.createTextNode(tot);
            totalVentas.appendChild(totalVentasText);
            raiz.appendChild(totalVentas);
            
            Element codigoOperativo = document.createElement("codigoOperativo"); 
            Text codigoOperativoText = document.createTextNode("IVA");
            codigoOperativo.appendChild(codigoOperativoText);
            raiz.appendChild(codigoOperativo);
            System.out.println("FIN CABECERA");
            System.out.println("INICIO DETALLE");
            Element ventas = document.createElement("ventas"); 
            
            for(int i=0;i<atsVentas.size();i++){
            	System.out.println(); 
            	System.out.println(atsVentas.get(i).getPersona().getCedulaRuc());
            	System.out.println(atsVentas.get(i).getTpIdCliente()+ " "+ String.valueOf(atsVentas.get(i).getNumeroComprobantes()));
            	System.out.println(df.format(atsVentas.get(i).getValorRetRenta()));
            	Element detalleVentas = document.createElement("detalleVentas");
            	
            		Element tpIdCliente = document.createElement("tpIdCliente"); 
            		Text tpIdClienteText = document.createTextNode(atsVentas.get(i).getTpIdCliente());
	                tpIdCliente.appendChild(tpIdClienteText);
	                detalleVentas.appendChild(tpIdCliente);
	                
	                Element idCliente = document.createElement("idCliente");
	                Text idClienteText = document.createTextNode(atsVentas.get(i).getPersona().getCedulaRuc());
	                idCliente.appendChild(idClienteText);
	                detalleVentas.appendChild(idCliente);
	                
	                Element tipoComprobante = document.createElement("tipoComprobante"); 
	                Text tipoComprobanteText = document.createTextNode(String.valueOf(atsVentas.get(i).getTipoComprobante()));
	                tipoComprobante.appendChild(tipoComprobanteText);
	                detalleVentas.appendChild(tipoComprobante);
	                
	                Element numeroComprobantes = document.createElement("numeroComprobantes"); 
	                Text numeroComprobantesText = document.createTextNode(String.valueOf(atsVentas.get(i).getNumeroComprobantes()));
	                numeroComprobantes.appendChild(numeroComprobantesText);
	                detalleVentas.appendChild(numeroComprobantes);
	                
	                Element baseNoGraIva = document.createElement("baseNoGraIva"); 
	                Text baseNoGraIvaText = document.createTextNode("0.00");
	                baseNoGraIva.appendChild(baseNoGraIvaText);
	                detalleVentas.appendChild(baseNoGraIva);
	                
	                Element baseImponible = document.createElement("baseImponible"); 
	                Text baseImponibleText = document.createTextNode(String.valueOf(df.format(atsVentas.get(i).getBaseImponible())).replace(",", "."));
	                baseImponible.appendChild(baseImponibleText);
	                detalleVentas.appendChild(baseImponible);
	                
	                
	                Element baseImpGrav = document.createElement("baseImpGrav"); 
	                Text baseImpGravText = document.createTextNode(String.valueOf(df.format(atsVentas.get(i).getBaseImpGrav())).replace(",", "."));
	                baseImpGrav.appendChild(baseImpGravText);
	                detalleVentas.appendChild(baseImpGrav);
	                
	                
	                Element montoIva = document.createElement("montoIva"); 
	                Text montoIvaText = document.createTextNode(String.valueOf(df.format(atsVentas.get(i).getMontoIva())).replace(",", "."));
	                montoIva.appendChild(montoIvaText);
	                detalleVentas.appendChild(montoIva);
	                
	                
	                Element valorRetIva = document.createElement("valorRetIva"); 
	                Text valorRetIvaText = document.createTextNode(String.valueOf(df.format(atsVentas.get(i).getValorRetIva())).replace(",", "."));
	                valorRetIva.appendChild(valorRetIvaText);
	                detalleVentas.appendChild(valorRetIva);
	                
	                Element valorRetRenta = document.createElement("valorRetRenta"); 
	                Text valorRetRentaText = document.createTextNode(String.valueOf(df.format(atsVentas.get(i).getValorRetRenta())).replace(",", "."));
	                valorRetRenta.appendChild(valorRetRentaText);
	                detalleVentas.appendChild(valorRetRenta);
                
                ventas.appendChild(detalleVentas);
            }
            raiz.appendChild(ventas);
            System.out.println("FIN DETALLE");
            System.out.println("INICIO PIE");
            Element ventasEstablecimiento = document.createElement("ventasEstablecimiento"); 
            	Element ventaEst = document.createElement("ventaEst");  
            		
            		Element codEstab = document.createElement("codEstab"); 
            		Text codEstabRentaText = document.createTextNode("001");
            		codEstab.appendChild(codEstabRentaText);
            		ventaEst.appendChild(codEstab);
            		
            		Element ventasEstab = document.createElement("ventasEstab"); 
            		Text ventasEstabText = document.createTextNode(tot);
            		ventasEstab.appendChild(ventasEstabText);
            		ventaEst.appendChild(ventasEstab);
            		
        		ventasEstablecimiento.appendChild(ventaEst);
        		
        	raiz.appendChild(ventasEstablecimiento);
        	System.out.println("FIN PIE");
            Source source = new DOMSource(document);
            //Indicamos donde lo queremos almacenar
            Result result = new StreamResult(new java.io.File(name+".xml")); //nombre del archivo
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
            System.out.println("FIN FUNCION");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
    }
    public static void generate(String name, ArrayList<String> key,ArrayList<String> value) throws Exception{
    	System.out.println("GNERAR XML");
        if(key.isEmpty() || value.isEmpty() || key.size()!=value.size()){
            System.out.println("ERROR empty ArrayList");
            return;
        }else{

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null, name, null);
            document.setXmlVersion("1.0");
            //Main Node
            Element raiz = document.getDocumentElement();
            //Por cada key creamos un item que contendr� la key y el value 
            for(int i=0; i<key.size();i++){
                //Item Node
                Element itemNode = document.createElement("ITEM"); 
                //Key Node
                Element keyNode = document.createElement("KEY"); 
                Text nodeKeyValue = document.createTextNode(key.get(i));
                keyNode.appendChild(nodeKeyValue);      
                //Value Node
                Element valueNode = document.createElement("VALUE"); 
                Text nodeValueValue = document.createTextNode(value.get(i));                
                valueNode.appendChild(nodeValueValue);
                //append keyNode and valueNode to itemNode
                itemNode.appendChild(keyNode);
                itemNode.appendChild(valueNode);
                //append itemNode to raiz
                raiz.appendChild(itemNode); //pegamos el elemento a la raiz "Documento"
            }                
            //Generate XML
            Source source = new DOMSource(document);
            //Indicamos donde lo queremos almacenar
            Result result = new StreamResult(new java.io.File(name+".xml")); //nombre del archivo
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        }
    }

}
