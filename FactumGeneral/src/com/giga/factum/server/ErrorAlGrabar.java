package com.giga.factum.server;


public class ErrorAlGrabar extends Exception {
	private static final long serialVersionUID = 1L;

	public ErrorAlGrabar(String msg) {       
		super(msg);
    }
}