package com.giga.factum.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
   
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
   
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ImageServlet extends HttpServlet {
    private final String repository = ""; 
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String filename = request.getParameter("file");

        // Security: '..' in the filename will let sneaky users access files
        // not in your repository.
        filename = filename.replaceAll("..", "");

        File file = new File(repository + filename);
        if (!file.exists())
            throw new FileNotFoundException(file.getAbsolutePath());

        response.setHeader("Content-Type", "image/jpeg");
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-disposition", "attachment;filename=\"" + filename + "\"");

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
        byte[] buf = new byte[1024];
        while (true) {
            int length = bis.read(buf);
            if (length == -1)
                break;

            bos.write(buf, 0, length);
        }
        bos.flush();
        bos.close();
        bis.close();
    }

    /*public void doGet(HttpServletRequest request, 
    	    HttpServletResponse response)
    	      throws ServletException, IOException
    	   {
    	    OutputStream out = null;
    	    try
    	    {

    	     response.setContentType("application/vnd.ms-excel");

    	     response.setHeader("Content-Disposition", 
    	    "attachment; filename=sampleName.xls");

    	     WritableWorkbook w = Workbook.createWorkbook(response.getOutputStream());
    	     WritableSheet s = w.createSheet("Demo", 0);

    	     s.addCell(new Label(0, 0, "Hello World"));
    	     w.write();
    	     w.close();
    	     //processRequest(request, response);
    	    } catch (Exception e)
    	    {
    	     throw new ServletException("Exception in Excel Sample Servlet", e);
    	    } finally
    	    {
    	     if (out != null)
    	      out.close();
    	    }
    	   }*/
}
