package com.giga.factum.server;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.giga.factum.server.base.Tbldtocomercial;

public class mainPrueba {

	
	private static Session session;
	
	public mainPrueba() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Hola Johny");
		//eliminarID("23");
		session = HibernateUtil.getSessionFactory().openSession();
		
	}

	
	public static String eliminarID(String id){
		Tbldtocomercial obj=load(Integer.parseInt(id));
		try {
			eliminacionFisica(obj);
			return "Objeto eliminado";
		} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			return e.getMessage();
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	public static Tbldtocomercial load(Integer id) {
		session = HibernateUtil.getSessionFactory().openSession();
		Tbldtocomercial returnValue = (Tbldtocomercial) session.load(
				"com.giga.factum.server.base.Tbldtocomercial", id);
		//session.getTransaction().commit();
		session.close();
		return returnValue;
	}
	
	private static Session getHibernateTemplate() {
		session = HibernateUtil.getSessionFactory().openSession();
 			//.getCurrentSession();
		session.beginTransaction();
		return session;
	}
	
	public  static void eliminacionFisica(Tbldtocomercial t) throws ErrorAlGrabar {
		try{
	 	getHibernateTemplate().delete(t);
	 	session.getTransaction().commit();
		} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		session.close();
	 	}
	}
}
