package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

/**
 * TblproductoTblbodegaId generated by hbm2java
 */
public class TblproductoTblbodegaId implements java.io.Serializable {

	private int idProducto;
	private int idBodega;

	public TblproductoTblbodegaId() {
	}

	public TblproductoTblbodegaId(int idProducto, int idBodega) {
		this.idProducto = idProducto;
		this.idBodega = idBodega;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdBodega() {
		return this.idBodega;
	}

	public void setIdBodega(int idBodega) {
		this.idBodega = idBodega;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TblproductoTblbodegaId))
			return false;
		TblproductoTblbodegaId castOther = (TblproductoTblbodegaId) other;

		return (this.getIdProducto() == castOther.getIdProducto())
				&& (this.getIdBodega() == castOther.getIdBodega());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdProducto();
		result = 37 * result + this.getIdBodega();
		return result;
	}

}
