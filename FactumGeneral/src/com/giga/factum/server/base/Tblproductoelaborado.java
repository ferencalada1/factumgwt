package com.giga.factum.server.base;

import com.giga.factum.client.DTO.ProductoElaboradoDTO;

/**
 * Tblproductoelaborado generated by hbm2java
 */
public class Tblproductoelaborado implements java.io.Serializable {

	private Integer idProductoelaborado;
	private Tblproducto tblproductoByIdProductoPadre;
	private Tblproducto tblproductoByIdProductoHijo;
	private Double cantidad;
	private int idEmpresa;
	private String establecimiento;

	public Tblproductoelaborado() {
	}

	public Tblproductoelaborado(Integer idEmpresa, String establecimiento,Tblproducto tblproductoByIdProductoPadre, Tblproducto tblproductoByIdProductoHijo) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tblproductoByIdProductoPadre = tblproductoByIdProductoPadre;
		this.tblproductoByIdProductoHijo = tblproductoByIdProductoHijo;
	}

	public Tblproductoelaborado(Integer idEmpresa, String establecimiento,Tblproducto tblproductoByIdProductoPadre, Tblproducto tblproductoByIdProductoHijo,
			Double cantidad) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tblproductoByIdProductoPadre = tblproductoByIdProductoPadre;
		this.tblproductoByIdProductoHijo = tblproductoByIdProductoHijo;
		this.cantidad = cantidad;
	}
//	public Tblproductoelaborado(Integer idEmpresa, String establecimiento,ProductoElaboradoDTO productoelaboradodto) {
	public Tblproductoelaborado(ProductoElaboradoDTO productoelaboradodto) {
		this.idEmpresa=productoelaboradodto.getIdEmpresa();
		this.establecimiento=productoelaboradodto.getEstablecimiento();
		this.tblproductoByIdProductoPadre = new Tblproducto(productoelaboradodto.getTblproductoByIdproductopadre());
		this.tblproductoByIdProductoHijo = new Tblproducto(productoelaboradodto.getTblproductoByIdproductohijo());
		this.cantidad = productoelaboradodto.getCantidad();
	}

	public Tblproductoelaborado(Integer idProductoelaborado, Tblproducto tblproductoByIdProductoPadre,
			Tblproducto tblproductoByIdProductoHijo, Double cantidad, int idEmpresa, String establecimiento) {
		this.idProductoelaborado = idProductoelaborado;
		this.tblproductoByIdProductoPadre = tblproductoByIdProductoPadre;
		this.tblproductoByIdProductoHijo = tblproductoByIdProductoHijo;
		this.cantidad = cantidad;
		this.idEmpresa = idEmpresa;
		this.establecimiento = establecimiento;
	}

	public Integer getIdProductoelaborado() {
		return this.idProductoelaborado;
	}

	public void setIdProductoelaborado(Integer idProductoelaborado) {
		this.idProductoelaborado = idProductoelaborado;
	}

	public Tblproducto getTblproductoByIdProductoPadre() {
		return this.tblproductoByIdProductoPadre;
	}

	public void setTblproductoByIdProductoPadre(Tblproducto tblproductoByIdProductoPadre) {
		this.tblproductoByIdProductoPadre = tblproductoByIdProductoPadre;
	}

	public Tblproducto getTblproductoByIdProductoHijo() {
		return this.tblproductoByIdProductoHijo;
	}

	public void setTblproductoByIdProductoHijo(Tblproducto tblproductoByIdProductoHijo) {
		this.tblproductoByIdProductoHijo = tblproductoByIdProductoHijo;
	}

	public Double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

}
