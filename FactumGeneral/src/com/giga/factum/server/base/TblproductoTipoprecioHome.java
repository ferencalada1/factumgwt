package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1


import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import java.util.Iterator;

import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

/**
 * Home object for domain model class TblproductoTipoprecio.
 * @see com.giga.factum.server.base.TblproductoTipoprecio
 * @author Hibernate Tools
 */
public class TblproductoTipoprecioHome{ 
	private static final Log log = LogFactory.getLog(TblproductoTipoprecioHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public void grabarList(TblproductoTipoprecio[] t,int length) throws ErrorAlGrabar {
	 	org.hibernate.classic.Session session = null;
		try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		for(int i=0;i<length;i++){
	 			session.update(t[i]); 
	 		}
	 		session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	} 
	}

	public TblproductoTipoprecio cargarTipoPrecio(int IdProducto, int IdTipoPrecio){
		org.hibernate.classic.Session session = null;
		TblproductoTipoprecioId prodTipoprecioID = new TblproductoTipoprecioId (IdProducto, IdTipoPrecio);  
		session = HibernateUtil.getSessionFactory().openSession();
		session.flush();
		TblproductoTipoprecio precio = (TblproductoTipoprecio) session.get(TblproductoTipoprecio.class, prodTipoprecioID);
		System.out.println("PRECIO: "+precio);
		session.close();
		return precio;
	}

	
	public List<TblproductoTipoprecio> listarTipoprecioD(int codPro) {	
		org.hibernate.classic.Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from TblproductoTipoprecio t where t.id.idProducto="+codPro);
		System.out.println(consulta);
		List<TblproductoTipoprecio> returnList = consulta.list();
		session.close();
		return returnList;
	}
	

	public Iterator listarTipoprecio(int codPro){
			org.hibernate.classic.Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("select t.id.idProducto, t.porcentaje, tp.idTipoPrecio, tp.tipoPrecio, tp.orden from TblproductoTipoprecio t,Tbltipoprecio tp where t.tbltipoprecio.idTipoPrecio=tp.idTipoPrecio and t.id.idProducto =" +codPro);			
			System.out.println(consulta);
			Iterator itemBidMaps = consulta.list().iterator();
			session.close();
			return itemBidMaps;
		}
	
	
	public void grabarProductoTipoPrecio(LinkedList <TblproductoTipoprecio> mod) throws ErrorAlGrabar 
	{org.hibernate.classic.Session session = null;
	 	try 
	 	{	 		
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		System.out.println("tama�o prodtipprecio"+mod.size());
	 		for(int k = 0; k<mod.size(); k++)
	 		{
	 			session.update(mod.get(k));
	 		}
	 		session.getTransaction().commit();
		 }
	 	catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}

}
