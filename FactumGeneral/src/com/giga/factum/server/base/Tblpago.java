package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import com.giga.factum.client.DTO.TblpagoDTO;

/**
 * Tblpago generated by hbm2java
 */
public class Tblpago implements java.io.Serializable {

	private Integer idPago;
	private Tbldtocomercial tbldtocomercial;
	private Date fechaVencimiento;
	private Date fechaRealPago;
	private double valor;
	private char estado;
	private int numEgreso;
	private String Concepto;
	private String FormaPago;
	private int idEmpresa;
	private String establecimiento;
//	private int docPago;

	public Tblpago() {
	}

	public Tblpago(Tbldtocomercial tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado) {
		this.idEmpresa=tbldtocomercial.getIdEmpresa();
		this.establecimiento=tbldtocomercial.getEstablecimiento();
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
	}
	
	public Tblpago(Tbldtocomercial tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado, int idEmpresa, String establecimiento, int docPago) {
		this.idEmpresa=tbldtocomercial.getIdEmpresa();
		this.establecimiento=tbldtocomercial.getEstablecimiento();
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
	}

	public Tblpago(TblpagoDTO pagoDTO) {
		this.idEmpresa=pagoDTO.getIdEmpresa();
		this.establecimiento=pagoDTO.getEstablecimiento();
		this.estado=pagoDTO.getEstado();
		this.fechaRealPago=pagoDTO.getFechaRealPago();
		this.fechaVencimiento=pagoDTO.getFechaVencimiento();
		this.idPago=pagoDTO.getIdPago();
		this.valor=pagoDTO.getValor();
		
	}

	public Integer getIdPago() {
		return this.idPago;
	}

	public void setIdPago(Integer idPago) {
		this.idPago = idPago;
	}

	public Tbldtocomercial getTbldtocomercial() {
		return this.tbldtocomercial;
	}

	public void setTbldtocomercial(Tbldtocomercial tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaRealPago() {
		return this.fechaRealPago;
	}

	public void setFechaRealPago(Date fechaRealPago) {
		this.fechaRealPago = fechaRealPago;
	}

	public double getValor() {
		return this.valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public void setNumEgreso(int numEgreso) {
		this.numEgreso = numEgreso;
	}

	public int getNumEgreso() {
		return numEgreso;
	}

	public void setConcepto(String concepto) {
		Concepto = concepto;
	}

	public String getConcepto() {
		return Concepto;
	}

	public void setFormaPago(String formaPago) {
		FormaPago = formaPago;
	}

	public String getFormaPago() {
		return FormaPago;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

//	public int getDocPago() {
//		return docPago;
//	}
//
//	public void setDocPago(int docPago) {
//		this.docPago = docPago;
//	}

}
