package com.giga.factum.server.base;

import com.giga.factum.client.DTO.TblmultiImpuestoDTO;

public class TblmultiImpuesto implements java.io.Serializable{
	private Integer idmultiImpuesto;
	private int porcentaje;
	private String descripcion;
	private char tipo;//Por porcentaje o valor fijo
	
	public TblmultiImpuesto(Integer idmultiImpuesto, int porcentaje, String descripcion, char tipo) {
		this.idmultiImpuesto = idmultiImpuesto;
		this.porcentaje = porcentaje;
		this.descripcion = descripcion;
		this.tipo=tipo;
	}
	
	
	
	public TblmultiImpuesto() {
	}

	public TblmultiImpuesto(TblmultiImpuestoDTO mult) {
		this.idmultiImpuesto=mult.getIdmultiImpuesto();
		this.descripcion=mult.getDescripcion();
		this.porcentaje=mult.getPorcentaje();
		this.tipo=mult.getTipo();
	}

	public Integer getIdmultiImpuesto() {
		return idmultiImpuesto;
	}
	public void setIdmultiImpuesto(Integer idmultiImpuesto) {
		this.idmultiImpuesto = idmultiImpuesto;
	}
	public int getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public char getTipo() {
		return tipo;
	}



	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	
}
