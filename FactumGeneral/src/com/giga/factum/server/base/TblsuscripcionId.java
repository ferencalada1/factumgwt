package com.giga.factum.server.base;

public class TblsuscripcionId implements java.io.Serializable{

	private Integer idSuscripcion;
	private Integer idEmpresa;
	private Integer idServicio;
	
	public TblsuscripcionId(){
		
	}
	
	public TblsuscripcionId(Integer idSuscripcion, Integer idEmpresa, Integer idServicio) {
		this.idSuscripcion = idSuscripcion;
		this.idEmpresa = idEmpresa;
		this.idServicio = idServicio;
	}
	public Integer getIdSuscripcion() {
		return idSuscripcion;
	}
	public void setIdSuscripcion(int idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Integer getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TblsuscripcionId))
			return false;
		TblsuscripcionId castOther = (TblsuscripcionId) other;

		return (this.getIdEmpresa() == castOther.getIdEmpresa())
				&& ((this.getIdServicio() == castOther.getIdServicio())
						|| (this.getIdServicio() != null && castOther.getIdServicio() != null
								&& this.getIdServicio().equals(castOther.getIdServicio())))
				&& ((this.getIdSuscripcion() == castOther.getIdSuscripcion())
						|| (this.getIdSuscripcion() != null && castOther.getIdSuscripcion() != null
								&& this.getIdSuscripcion().equals(castOther.getIdSuscripcion())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdEmpresa();
		result = 37 * result + (getIdServicio() == null ? 0 : this.getIdServicio().hashCode());
		result = 37 * result + (getIdSuscripcion() == null ? 0 : this.getIdSuscripcion().hashCode());
		return result;
	}
}
