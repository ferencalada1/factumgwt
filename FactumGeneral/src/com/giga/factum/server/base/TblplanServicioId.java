package com.giga.factum.server.base;

public class TblplanServicioId implements java.io.Serializable {
	private Integer idPlan;
	private Integer idServicio;
	
	
	public TblplanServicioId() {
	}
	public TblplanServicioId(Integer idPlan, Integer idServicio) {
		this.idPlan = idPlan;
		this.idServicio = idServicio;
	}
	public Integer getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}
	public Integer getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TblplanServicioId))
			return false;
		TblplanServicioId castOther = (TblplanServicioId) other;

		return ((this.getIdPlan() == castOther.getIdPlan())
				|| (this.getIdPlan() != null && castOther.getIdPlan() != null
				&& this.getIdPlan().equals(castOther.getIdPlan())))
				&& ((this.getIdServicio() == castOther.getIdServicio())
						|| (this.getIdServicio() != null && castOther.getIdServicio() != null
								&& this.getIdServicio().equals(castOther.getIdServicio())));
	}

	public int hashCode() {
		int result = 17;

//		result = 37 * result + this.getIdEmpresa();
		result = 37 * result + (getIdPlan() == null ? 0 : this.getIdPlan().hashCode());
		result = 37 * result + (getIdServicio() == null ? 0 : this.getIdServicio().hashCode());
		return result;
	}
}
