package com.giga.factum.server.base;

public class Tblplan implements java.io.Serializable{
	private Integer idPlan;
	private double costo;
	private String descripcion;
	private int numUsuario;
	
	
	public Tblplan(Integer idPlan, double costo, String descripcion, int numUsuario) {
		this.idPlan = idPlan;
		this.costo = costo;
		this.descripcion = descripcion;
		this.numUsuario = numUsuario;
	}
	public Integer getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}
	public double getCosto() {
		return costo;
	}
	public void setCosto(double costo) {
		this.costo = costo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getNumUsuario() {
		return numUsuario;
	}
	public void setNumUsuario(int numUsuario) {
		this.numUsuario = numUsuario;
	}
	
	
}
