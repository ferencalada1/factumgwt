package com.giga.factum.server.base;

import java.util.Date;

public class TblplanServicio implements java.io.Serializable {

	private TblplanServicioId id;
	private Tblplan tblplan;
	private Tblservicio tblservicio;
	private Date fecha;
	
	public TblplanServicio() {
	}
	
	public TblplanServicio(TblplanServicioId tblplanServicioId, Tblplan tblplan, Tblservicio tblservicio, Date fecha) {
		this.id = tblplanServicioId;
		this.tblplan = tblplan;
		this.tblservicio = tblservicio;
		this.fecha = fecha;
	}

	public TblplanServicio(Tblplan tblplan, Tblservicio tblservicio, Date fecha) {
		this.tblplan = tblplan;
		this.tblservicio = tblservicio;
		this.id=new TblplanServicioId(tblplan.getIdPlan(),tblservicio.getIdServicio());
		this.fecha = fecha;
	}
	public Tblplan getTblplan() {
		return tblplan;
	}
	public void setTblplan(Tblplan tblplan) {
		this.tblplan = tblplan;
	}
	public Tblservicio getTblservicio() {
		return tblservicio;
	}
	public void setTblservicio(Tblservicio tblservicio) {
		this.tblservicio = tblservicio;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public TblplanServicioId getId() {
		return id;
	}

	public void setId(TblplanServicioId id) {
		this.id = id;
	}
	
	
}
