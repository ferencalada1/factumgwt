package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.GreetingServiceImpl;
import com.giga.factum.server.HibernateUtil;

/**
 * Home object for domain model class Tblretencion.
 * @see com.giga.factum.server.base.Tblretencion
 * @author Hibernate Tools
 */
public class TblretencionHome extends DAOGenerico<Tblretencion>{
	private Session session;
	Integer planCuenta = 1;//DEBE SER EXTRAIDO DE LA VARIABLE DE SESION
	private static final Log log = LogFactory.getLog(TblretencionHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public void grabarListRet(LinkedList<Tblretencion> det) throws ErrorAlGrabar {
		
		
		org.hibernate.classic.Session session = null;
		//try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		for(int i=0;i<det.size();i++){
	 			String mensaje="id "+String.valueOf(det.get(i).getIdRetencion())+ " idImpuesto "+String.valueOf(det.get(i).getTblimpuesto().getIdImpuesto())+" base "+det.get(i).getBaseImponible()+
				" num real "+det.get(i).getNumRealRetencion()+"  idDocCom "+det.get(i).getTbldtocomercialByIdFactura().getIdDtoComercial()+" autor "+det.get(i).getAutorizacionSri()+" retenido "+det.get(i).getValorRetenido();
				System.out.println(mensaje);
	 			session.save(det.get(i));
		 	}
	 	 	session.getTransaction().commit();
	 	 	session.close();
		/*} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		System.out.println(e.getMessage());
	 		//throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}*/
	}
	
	
public void grabarRetencionPago(RetencionPago retpago){
	//try {
	
 		session = HibernateUtil.getSessionFactory().openSession();
 		session.beginTransaction();
 		Integer id=(Integer) session.save(retpago.getDocRetencion());
 		double total=0;
 		for(int i=0;i<retpago.getDetret().size();i++){
			System.out.println("id doc= "+id);
			retpago.getDocRetencion().setIdDtoComercial(id);
//			retpago.getDetret().get(i).setTbldtocomercialByIdFactura(retpago.getDocRetencion());
			System.out.println("id doc= "+id);
			retpago.getDetret().get(i).setTbldtocomercialByIdDtoComercial(retpago.getDocRetencion());
			System.out.println("id doc puesto= "+retpago.getDetret().get(i).getTbldtocomercialByIdDtoComercial().getIdDtoComercial());
 			session.save(retpago.getDetret().get(i));
 			System.out.println("Ya grabado ");
 			total=total+retpago.getDetret().get(i).getValorRetenido();
 			System.out.println("grabado total: "+total);
	 	}
 		
 		retpago.getT().setFechaRealPago(retpago.getDocRetencion().getFecha());
 		double valor=0.0;
 		System.out.println("valor de la retencion "+total);
 		retpago.setPagado(total);
 		valor=retpago.getT().getValor();
 		retpago.getT().setValor(retpago.getPagado());
 		retpago.getTbl().setFecha(retpago.getT().getFechaRealPago());
 		retpago.getTbl().setExpiracion(retpago.getT().getFechaRealPago());
 		
 		retpago.getTbl().setSubtotal(retpago.getPagado());
 		id=(Integer) session.save(retpago.getTbl()); //grabar nuevo documento
		Iterator iterTP= retpago.getTipoPagos().iterator();
		TbldtocomercialTbltipopago pago;
		String concepto="";
		char signo = 'd';
		Integer tipoT = retpago.getTbl().getTipoTransaccion();
		while (iterTP.hasNext()) {
			pago = (TbldtocomercialTbltipopago) iterTP.next();
			pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
			pago.setCantidad(retpago.getT().getValor());
			pago.setReferenciaDto(retpago.getT().getIdPago());
			System.out.println("tipo documento "+retpago.getT().getTbldtocomercial().getTipoTransaccion());
			if(retpago.getT().getTbldtocomercial().getTipoTransaccion()==1 || retpago.getT().getTbldtocomercial().getTipoTransaccion()==10){
				pago.setTipo('1');
				concepto="Por concepto de pago de Dto:"+retpago.getTbl().getNumRealTransaccion();
				signo = 'd';
				tipoT=11;
			}else{
				pago.setTipo('0');
				concepto="Por concepto de cobro de Dto:"+retpago.getTbl().getNumRealTransaccion();
				signo = 'h';
			}
			System.out.println("pago "+pago.getCantidad());
			session.save(pago);
			//AQUI VAMOS A GENERAR LOS ASIENTOS CONTABLES DE LOS PAGOS
			//Primero generamos la cabecera del movimiento
			Tblmovimiento movimiento;
			//retpago.getTbl().setTbldtocomercialTbltipopagos(null);
			//System.out.println(retpago.getT().getTbldtocomercial().gett)
			
			movimiento = new Tblmovimiento(retpago.getTbl(), concepto,retpago.getT().getFechaRealPago());
			Integer idMov = (Integer) session.save(movimiento);
			//Ahora vamos a generar los detalles del movimiento
			
			//LISTADO DE LOS DETALLES DEL MOVIMIENTO CONTABLE
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			
			TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento;
			TblmovimientoTblcuentaTblplancuenta detalleMovimiento;
			TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento2;
			TblmovimientoTblcuentaTblplancuenta detalleMovimiento2;
			TblcuentaPlancuenta cuentaPlan;
			System.out.println("TIPO TRANS: "+tipoT); 
			Tbltipodocumento tipoDoc =  findByNombreT(tipoT+"", "idTipo");
			Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
			idDetalleMovimiento2 =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); 		
			//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
			//T findByIdPlanIdCuenta(String idplan,String idcuenta)
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
					IdCuentaAsiento+"");//idcuenta)
			System.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
			detalleMovimiento2 = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento2,
					cuentaPlan,
					movimiento, retpago.getT().getValor(), signo);
			
			if(signo=='d')
			{
				detallesMovimiento.add(detalleMovimiento2);
			}
				//detalleMovimiento.setTblmovimiento(movimiento);
			//VAMOS EXTRER LA CUENTA Y EL PLAN DE CUENTA ASOCIADO AL PAGO

			//-------------------------------------------------------------------------------
			//CREAMOS EL DETALLE PARA EL IVA, SEA COBRADO O PAGADO
			//tipoDoc =  (Tbltipodocumento) findByNombre(tipoIva, "TipoDoc");
			//pago = 
			Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipopago " +
					"u where u.idTipoPago ="+ "'"+pago.getTbltipopago().getIdTipoPago()+"'");	
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tbltipopago> returnList = consulta.list();
			//session.close();
			Tbltipopago tipopago =  returnList.get(0);	
			System.out.println("ID Cuenta: "+tipopago.getIdCuenta()+"");
			System.out.println("PLAN Cuenta: "+planCuenta.toString());
			
			
			//AQUI VAMOS A GRABAR LOS MOVIMIENTOS DE LAS RETENCIONES 
			if(signo=='d')
				signo='h';
			else
				signo='d';
			//****************************************************************************
			if (retpago.getDetret() != null)
			{
				
				//Es un cobro a cliente
					Iterator iterRetenciones = retpago.getDetret().iterator();
					while(iterRetenciones.hasNext()){
						Tblretencion retencion = (Tblretencion) iterRetenciones.next();
						
						cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(retencion.getTblimpuesto().getIdPlan()+"",
								retencion.getTblimpuesto().getIdCuenta()+"");
						idDetalleMovimiento =new 
						TblmovimientoTblcuentaTblplancuentaId(idMov, retencion.getTblimpuesto().getIdPlan(), 
								retencion.getTblimpuesto().getIdCuenta());
						Double cantidad = 0.0;
						double porcentaje = retencion.getTblimpuesto().getRetencion();
						double base = retencion.getBaseImponible();
						cantidad = (base*porcentaje)/100;
						
						cantidad = CValidarDato.getDecimal(2,cantidad);
						detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
								idDetalleMovimiento,
								cuentaPlan,
								movimiento, cantidad, signo);
						detallesMovimiento.add(detalleMovimiento);
				}
			}
			else{
				System.out.println("Retenciones en nulo");
			}
			//****************************************************************************
			if(signo == 'd')
			{
				detallesMovimiento.add(detalleMovimiento2);
			}
			movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
							
			//movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
			//movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detallesMovimiento);
			System.out.println("TAMANIO MOVIMIENTO: "+detallesMovimiento.size());
			System.out.println("ID DTO MOVIMIENTO: "+movimiento.getTbldtocomercial().getIdDtoComercial());
			session.update(movimiento);
			
			//session.flush();
			//*************************************************
			//*************************************************
		}
		
 		session.update(retpago.getT());
 		System.out.println("modifico pago y creo dtco");
 		Tblpago pagonuevo=new Tblpago();
 		if(valor-retpago.getPagado()>0){
 			pagonuevo.setEstado('0');
 	 		pagonuevo.setFechaRealPago(null);
 	 		pagonuevo.setFechaVencimiento(retpago.getT().getFechaVencimiento());
 	 		pagonuevo.setValor(valor-retpago.getPagado());
 	 		pagonuevo.setTbldtocomercial(retpago.getT().getTbldtocomercial());
 	 		pagonuevo.setIdEmpresa(retpago.getT().getIdEmpresa());
 	 		pagonuevo.setEstablecimiento(retpago.getT().getEstablecimiento());
 			session.save(pagonuevo);
 		}
 		
		System.out.println("SE INICIO GRABO LOS MOVIMIENTOS: ");
		
		session.flush();
		session.clear();
		session.getTransaction().commit();

		System.out.println("SE FINALIZO GRABO LOS MOVIMIENTOS: ");
	
}

public void grabarRetencion1(LinkedList<Tblretencion> det,Tbldtocomercial doc) throws ErrorAlGrabar {
	
	
	org.hibernate.classic.Session session = null;
	//try {
 		session = HibernateUtil.getSessionFactory().openSession();
 		session.beginTransaction();
 		Integer id=(Integer) session.save(doc);
 	
 		
 		for(int i=0;i<det.size();i++){
			System.out.println("id doc= "+id);
			doc.setIdDtoComercial(id);
			det.get(i).setTbldtocomercialByIdFactura(doc);
 			session.save(det.get(i));
	 	}
 	 	session.getTransaction().commit();
 	 	session.close();
	/*} catch (HibernateException e) {
 		session.getTransaction().rollback();
 		System.out.println(e.getMessage());
 		//throw new ErrorAlGrabar(e.getMessage());
 	} finally {
 		if(session!=null)
 		session.close();
 	}*/
}


public void grabarRetencion(LinkedList<Tblretencion> det,Tbldtocomercial doc) throws ErrorAlGrabar {
	
	
	//org.hibernate.classic.Session session = null;
	//try {
	System.out.println("------------------ EN GRABAR RETENCION ----------------------");
		Boolean unico=false;
		Integer numReal = doc.getNumRealTransaccion();
 		session = HibernateUtil.getSessionFactory().openSession();
 		session.beginTransaction();
 		System.out.println("from "+doc.getClass().getName()+" u where u.numRealTransaccion = " +"'"+numReal+"'"+
 					"and u.tipoTransaccion ="+"'"+doc.getTipoTransaccion()+"'");
 		while(!unico)
 		{
 			Query consulta = session.createQuery("from "+doc.getClass().getName()+" u where u.numRealTransaccion = " +"'"+numReal+"'"+
 					"and u.tipoTransaccion ="+"'"+doc.getTipoTransaccion()+"'");
	 		if(!consulta.list().isEmpty())
	 		{
	 			numReal++;
	 		}else{
	 			unico=true;
	 			break;
	 		}
 		}
 		System.out.println("iddto: "+doc.getIdDtoComercial()+" numreal: "+doc.getNumRealTransaccion());
 		doc.setNumRealTransaccion(numReal);
 		Integer id=(Integer) session.save(doc);
 		doc.setIdDtoComercial(id);
 		System.out.println("iddto: "+doc.getIdDtoComercial()+" numreal: "+doc.getNumRealTransaccion());
 		/******************************************/
 		String concepto="Por concepto de una Retencion";
 		Tblmovimiento movimiento;
		movimiento = new Tblmovimiento(doc, concepto,doc.getFecha());
		Integer idMov = (Integer) session.save(movimiento);
		
		Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new LinkedHashSet<TblmovimientoTblcuentaTblplancuenta>(0);
		//Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoHaber = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
		//Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoDebe = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
		
		TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento;
		TblmovimientoTblcuentaTblplancuenta detalleMovimiento;
		TblcuentaPlancuenta cuentaPlan;
		
		char signo='h';
		//if(!doc.getTblpersonaByIdPersona().getCedulaRuc().equals("0190359956001")||doc.getTipoTransaccion()==0)
		if(doc.getTipoTransaccion()==0)
		{//Es una retencion de un cliente a nuestro favor, debemos devolver dinero
			//System.out.println("entre cuando la cedula es diferente de gIGA");
			Tbltipodocumento tipoDoc =  findByNombreT(doc.getTipoTransaccion()+"", "idTipo");
			//(Tbltipodocumento) findByNombre(ndto.getTipoTransaccion()+"", "idTipo");
			Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento);	
			//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
			//T findByIdPlanIdCuenta(String idplan,String idcuenta)
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
					IdCuentaAsiento+"");//idcuenta)
			
			System.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, doc.getSubtotal(), signo);
			detallesMovimiento.add(detalleMovimiento);
			signo ='d';
		}		
 		
 		for(int i=0;i<det.size();i++){
 			System.out.println("el det.size "+det.size());
			System.out.println("id doc= "+id);
//			det.get(i).setTbldtocomercialByIdFactura(doc);
			det.get(i).setTbldtocomercialByIdDtoComercial(doc);
 			session.save(det.get(i));
 			System.out.println("Codigo= "+det.get(i).getTblimpuesto().getCodigo());
 			Tblretencion retencion = det.get(i);
			
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(retencion.getTblimpuesto().getIdPlan()+"",
					retencion.getTblimpuesto().getIdCuenta()+"");
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, retencion.getTblimpuesto().getIdPlan(), 
					retencion.getTblimpuesto().getIdCuenta());
			Double cantidad = 0.0;
			double porcentaje = retencion.getTblimpuesto().getRetencion();
			double base = retencion.getBaseImponible();
			cantidad = (base*porcentaje)/100;
			cantidad = getDecimal(2,cantidad);
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, cantidad, signo);
			//detallesMovimiento.add(detalleMovimiento);
			detallesMovimiento.add(detalleMovimiento);
	 	}
 		
 		if(signo=='h')
 		{
 			signo = 'd';
 			Tbltipodocumento tipoDoc =  findByNombreT(doc.getTipoTransaccion()+"", "idTipo");
			//(Tbltipodocumento) findByNombre(ndto.getTipoTransaccion()+"", "idTipo");
			Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); 	
			System.out.println("el idDetalleMovimiento es :"+idDetalleMovimiento.getIdMovimiento());
			//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
			//T findByIdPlanIdCuenta(String idplan,String idcuenta)
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
					IdCuentaAsiento+"");//idcuenta)
			System.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, doc.getSubtotal(), signo);
			detallesMovimiento.add(detalleMovimiento);
 		}
 		System.out.println("a 3 lineas de acabar");
 		movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
 		System.out.println("el detalles movimiento es "+detallesMovimiento.size());
 		session.update(movimiento);
 	 	session.getTransaction().commit();
 	 	session.close();
	/*} catch (HibernateException e) {
 		session.getTransaction().rollback();
 		System.out.println(e.getMessage());
 		//throw new ErrorAlGrabar(e.getMessage());
 	} finally {
 		if(session!=null)
 		session.close();
 	}*/
}
public void grabarListRetencion(LinkedList<Tblretencion> det) throws ErrorAlGrabar {
		org.hibernate.classic.Session session = null;
		try{
			
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		for(int i=0;i<det.size();i++){
	 			String mensaje="id "+String.valueOf(det.get(i).getIdRetencion())+ " idImpuesto "+String.valueOf(det.get(i).getTblimpuesto().getIdImpuesto())+" base "+det.get(i).getBaseImponible()+
				" num real "+det.get(i).getNumRealRetencion()+"  idDocCom "+det.get(i).getTbldtocomercialByIdFactura().getIdDtoComercial()+" autor "+det.get(i).getAutorizacionSri()+" retenido "+det.get(i).getValorRetenido();
				System.out.println(mensaje);
	 			session.save(det.get(i)); 
	 		}
	 		session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	 	
	}
	
	public String modificarPago(TblpagoDTO entidad) throws IllegalArgumentException {
		TblpagoHome acces = new TblpagoHome();
		Tblpago tblpago= new Tblpago();
		String mensaje = "";
		int ban=0;
		//try {
			System.out.println("fechad de entidad TblpagoDTO= "+String.valueOf(entidad.getFechaRealPago()));
			Tblpago p=new Tblpago(entidad);
			p.setEstado(entidad.getEstado());
			p.setFechaRealPago(entidad.getFechaRealPago());
			p.setFechaVencimiento(entidad.getFechaVencimiento());
			p.setIdPago(entidad.getIdPago());
			p.setEstado('1');
			DtocomercialDTO doc=new DtocomercialDTO();
			System.out.println("fecha entidad= "+String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setIdEmpresa(entidad.getTbldtocomercial().getIdEmpresa());
			doc.setEstablecimiento(entidad.getTbldtocomercial().getEstablecimiento());
			doc.setPuntoEmision(entidad.getTbldtocomercial().getPuntoEmision());
			doc.setFecha(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setSubtotal(entidad.getValor());
			//doc.setTbldtocomercialTbltipopagos(entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos());
			doc.setTblpersonaByIdPersona(entidad.getTbldtocomercial().getTblpersonaByIdPersona());
			doc.setTipoTransaccion(6);
			doc.setTblpersonaByIdVendedor(entidad.getTbldtocomercial().getTblpersonaByIdVendedor());
	 		doc.setExpiracion(String.valueOf(entidad.getFechaRealPago()).replace('-','/'));
			doc.setAutorizacion(entidad.getTbldtocomercial().getAutorizacion());
			doc.setNumPagos(0);
			doc.setEstado('1');
			//Llamamos a la funcion para obtener el ultimo ID del documento del mismo tipo
			
			try{
				GreetingServiceImpl imp=new GreetingServiceImpl();
				DtocomercialDTO docBase = imp.ultimoDocumento("where TipoTransaccion = '6' ", "numRealTransaccion");
				doc.setNumRealTransaccion(docBase.getNumRealTransaccion()+1);
			}catch(Exception e){
				doc.setNumRealTransaccion(0);
			}
			System.out.println("fechadto= "+doc.getFecha());
			Tbldtocomercial tbl=new Tbldtocomercial(doc);
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy/MM/dd");
		    Date fechaaux = null;
		    try {
				fechaaux = formatoDelTexto.parse(doc.getFecha());
			} catch (ParseException e1) {
				System.out.println("Error al convertir fecha en retencion: "+e1);
			}
			tbl.setExpiracion(entidad.getFechaRealPago());
			tbl.setFecha(entidad.getFechaRealPago());
			System.out.println("fechatbl= "+tbl.getFecha());
			
			Set<TbldtocomercialTbltipopago> tipoPagos = new HashSet<TbldtocomercialTbltipopago>(0);
			Iterator iterTP= entidad.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
			
			DtocomercialTbltipopagoDTO tPago = new DtocomercialTbltipopagoDTO();
			while (iterTP.hasNext()) {
				tPago = (DtocomercialTbltipopagoDTO) iterTP.next();
				TbldtocomercialTbltipopago tpagoDTO = new TbldtocomercialTbltipopago(tPago);
				
				tpagoDTO.setTbldtocomercial(tbl);
				Tbltipopago tipopago=new Tbltipopago();
				tipopago.setIdTipoPago(tPago.getTipoPago());
				tipopago.setIdCuenta(1);
				tipopago.setIdPlan(1);
				switch (tPago.getTipoPago()){
					case 1:{
						tipopago.setTipoPago("CONTADO");
						break;
					}
					case 2:{
						tipopago.setTipoPago("CREDITO");
						break;
					}
					case 3:{
						tipopago.setTipoPago("RETENCION");
						break;
					}
					case 4:{
						tipopago.setTipoPago("ANTICIPO");
						break;
					}
					case 5:{
						tipopago.setTipoPago("NOTA");
						break;
					}
					case 6:{
						tipopago.setTipoPago("TARJETA");
						break;
					}
					case 7:{
						tipopago.setTipoPago("BANCO");
						break;
					}
				
				}
				tpagoDTO.setTbltipopago(tipopago);
				if(entidad.getTbldtocomercial().getTipoTransaccion()==1||entidad.getTbldtocomercial().getTipoTransaccion()==10){
					tpagoDTO.setTipo('1');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
				}else{
					tpagoDTO.setTipo('0');
					System.out.println("Tipo de tbldottipopago "+tpagoDTO.getTipo());
				}
				//this.tbldtocomercial = documento;
				//this.tbltipopago = Tipopago;
				tipoPagos.add(tpagoDTO);	
	        }
			
			//tbl.setTbldtocomercialTbltipopagos(tipoPagos);
			p.setValor(entidad.getValor());
			p.setTbldtocomercial(new Tbldtocomercial(entidad.getTbldtocomercial()));
			try {
				acces.modificarpago(p,tbl,tipoPagos);
			} catch (ErrorAlGrabar e) {
				// TODO Auto-generated catch block
				System.out.println("ERROR INTERNO: "+e.getLocalizedMessage());
			}
			mensaje="Ingreso realizado con \u00e9xito";
		/*} catch (ErrorAlGrabar e) {
			System.out.println("error en pago greetingServiceImplements "+e.getMessage());
			
		} catch (RuntimeException re) {
			System.out.println("error en pago greetingServiceImplements "+re.getMessage());
			mensaje="No se pudo grabar el dato";
			mensaje = re.getMessage();
			throw re;	
		} */
		System.out.println(mensaje);
		return mensaje;
	
	}
	public Tbltipodocumento findByNombreT(String nombre,String campo) {
		//session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("RETENCION:.......from com.giga.factum.server.base.Tbltipodocumento u where u."+campo+" = " +"'"+nombre+"'");
		Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipodocumento u where u."+campo+" = " +"'"+nombre+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<Tbltipodocumento> returnList = consulta.list();
		//session.close();
		return returnList.get(0);
	}
	public TblcuentaPlancuenta findByIdPlanIdCuenta2(String idplan,String idcuenta) {
		//session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("from com.giga.factum.server.base.TblcuentaPlancuenta u where " +
				"u.tblplancuenta.idPlan = " +"'"+idplan+"' and u.tblcuenta.idCuenta='"+idcuenta+"'");
		Query consulta = session.createQuery("from com.giga.factum.server.base.TblcuentaPlancuenta u where " +
				"u.tblplancuenta.idPlan = " +"'"+idplan+"' and u.tblcuenta.idCuenta='"+idcuenta+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<TblcuentaPlancuenta> returnList = consulta.list();
		//session.close();
		return returnList.get(0);
	}
	
	public static Double getDecimal(int numeroDecimales,double decimal){
		decimal = decimal*(java.lang.Math.pow(10, numeroDecimales));
		decimal = java.lang.Math.round(decimal);
		decimal = decimal/java.lang.Math.pow(10, numeroDecimales);
		return decimal;
	}
	
	public List<Tblretencion> getListRetencion(int numDto) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from Tblretencion t where t.tbldtocomercialByIdFactura.idDtoComercial="+numDto);
		//Le pasamos el registro desde el cual va a extraer
		//consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(NumReg);
		List<Tblretencion> returnList = consulta.list();
		session.close();
		return returnList;
	}
}
