package com.giga.factum.server.base;

import com.giga.factum.server.DAOGenerico;

/**
 * Home object for domain model class Tblservicio.
 * @see com.giga.factum.server.base.Tblservicio
 * @author Hibernate Tools
 */
public class TblservicioHome extends DAOGenerico<Tblservicio>{

}
