package com.giga.factum.server.base;

import java.util.HashSet;
import java.util.Set;

import com.giga.factum.client.DTO.BodegaDTO;

public class TblbodegaProd {

	
	private Integer idBodega;
	private String Bodega;
	private String ubicacion;
	private Double cantidad;
	

	public TblbodegaProd() {
	}
	/*public TblbodegaProd(BodegaDTO bod) {
		this.idBodega=bod.getIdBodega();
		this.nombre=bod.getBodega();
		this.telefono=bod.getTelefono();
		this.ubicacion=bod.getUbicacion();
	}*/
	public TblbodegaProd(Integer idBodega, String Bodega, String ubicacion, Double cantidad) {
		this.idBodega = idBodega;
		this.Bodega = Bodega;
		this.ubicacion = ubicacion;
		this.cantidad = cantidad;
	}


	public Integer getIdBodega() {
		return this.idBodega;
	}

	public void setIdBodega(Integer idBodega) {
		this.idBodega = idBodega;
	}

	public String getBodega() {
		return this.Bodega;
	}

	public void setBodega(String bodega) {
		this.Bodega = bodega;
	}

	public String getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Double getCantidad() {
		return this.cantidad;
	}

	public void setTCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}


}
