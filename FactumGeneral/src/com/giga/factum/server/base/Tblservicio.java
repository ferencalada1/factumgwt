package com.giga.factum.server.base;

public class Tblservicio implements java.io.Serializable{

	private int idServicio;
	private  String nombre;
	private String descripcion;
	private double costo;
	private int diasGracia;
	
	public Tblservicio(){
		
	}
	public Tblservicio(int idServicio, String nombre, String descripcion, double costo, int diasGracia) {
		this.idServicio = idServicio;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.costo = costo;
		this.diasGracia = diasGracia;
	}
	public int getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getCosto() {
		return costo;
	}
	public void setCosto(double costo) {
		this.costo = costo;
	}
	public int getDiasGracia() {
		return diasGracia;
	}
	public void setDiasGracia(int diasGracia) {
		this.diasGracia = diasGracia;
	}
	
	
}
