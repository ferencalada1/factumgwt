package com.giga.factum.server.base;

import java.util.LinkedList;
import java.util.Set;

public class RetencionPago {
	
	private Tblpago t=new Tblpago();
	private double pagado=0.0;
	private Tbldtocomercial tbl=new Tbldtocomercial();
	private Set<TbldtocomercialTbltipopago> tipoPagos;
	private LinkedList<Tblretencion> detret=new LinkedList<Tblretencion>();
	private Tbldtocomercial docRetencion=new Tbldtocomercial();
	
	public RetencionPago(){
		
	}
	
	public void setT(Tblpago t) {
		this.t = t;
	}
	public Tblpago getT() {
		return t;
	}
	public void setPagado(double pagado) {
		this.pagado = pagado;
	}
	public double getPagado() {
		return pagado;
	}
	public void setTbl(Tbldtocomercial tbl) {
		this.tbl = tbl;
	}
	public Tbldtocomercial getTbl() {
		return tbl;
	}
	public void setTipoPagos(Set<TbldtocomercialTbltipopago> tipoPagos) {
		this.tipoPagos = tipoPagos;
	}
	public Set<TbldtocomercialTbltipopago> getTipoPagos() {
		return tipoPagos;
	}

	public void setDetret(LinkedList<Tblretencion> detret) {
		this.detret = detret;
	}

	public LinkedList<Tblretencion> getDetret() {
		return detret;
	}

	public void setDocRetencion(Tbldtocomercial docRetencion) {
		this.docRetencion = docRetencion;
	}

	public Tbldtocomercial getDocRetencion() {
		return docRetencion;
	}
	

}
