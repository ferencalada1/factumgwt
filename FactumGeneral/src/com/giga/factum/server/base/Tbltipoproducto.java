package com.giga.factum.server.base;

public class Tbltipoproducto implements java.io.Serializable{

	private int idTipoProducto;
	private String nombreTipoProducto;
	private int idEmpresa;
	
	
	public Tbltipoproducto(int idTipoProducto, String nombreTipoProducto, int idEmpresa) {
		this.idTipoProducto = idTipoProducto;
		this.nombreTipoProducto = nombreTipoProducto;
		this.idEmpresa = idEmpresa;
	}
	public int getIdTipoProducto() {
		return idTipoProducto;
	}
	public void setIdTipoProducto(int idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}
	
	public String getNombreTipoProducto() {
		return nombreTipoProducto;
	}
	public void setNombreTipoProducto(String nombreTipoProducto) {
		this.nombreTipoProducto = nombreTipoProducto;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
}
