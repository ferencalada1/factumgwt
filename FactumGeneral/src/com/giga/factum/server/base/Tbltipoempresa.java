package com.giga.factum.server.base;

import com.giga.factum.client.DTO.TbltipoempresaDTO;

public class Tbltipoempresa {
	private Integer id_tipo_empresa;
	private String tipo_empresa;
	
	
	public Tbltipoempresa() {
	}
	
	public Tbltipoempresa(TbltipoempresaDTO tipoempresa) {
		this.id_tipo_empresa=tipoempresa.getId_tipo_empresa();
		this.tipo_empresa=tipoempresa.getTipo_empresa();
	}

	public Tbltipoempresa(Integer id_tipo_empresa, String tipo_empresa) {
		this.id_tipo_empresa = id_tipo_empresa;
		this.tipo_empresa = tipo_empresa;
	}

	public Integer getId_tipo_empresa() {
		return id_tipo_empresa;
	}

	public void setId_tipo_empresa(Integer id_tipo_empresa) {
		this.id_tipo_empresa = id_tipo_empresa;
	}

	public String getTipo_empresa() {
		return tipo_empresa;
	}

	public void setTipo_empresa(String tipo_empresa) {
		this.tipo_empresa = tipo_empresa;
	}
	
	
}
