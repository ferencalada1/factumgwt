package com.giga.factum.server.base;

import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

/**
 * TblproductoTipoprecio generated by hbm2java
 */
public class TblproductoTipoprecio implements java.io.Serializable {

	private TblproductoTipoprecioId id;
	private Tbltipoprecio tbltipoprecio;
	private Tblproducto tblproducto;
	private double porcentaje;
	private Integer idTipoPrecio;
	private Integer idProducto;

	public Integer getIdTipoPrecio() {
		return idTipoPrecio;
	}

	public void setIdTipoPrecio(Integer idTipoPrecio) {
		this.idTipoPrecio = idTipoPrecio;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public TblproductoTipoprecio() {
	}
	
	public TblproductoTipoprecio(ProductoTipoPrecioDTO ptDTO ) {
		TblproductoTipoprecioId idT=new TblproductoTipoprecioId();
		idT.setIdProducto(ptDTO.getTblproducto().getIdProducto());
		idT.setIdTipoPrecio(ptDTO.getTbltipoprecio().getIdTipoPrecio());
		id=idT;
		tbltipoprecio=new Tbltipoprecio(ptDTO.getTbltipoprecio());
		tbltipoprecio.setIdTipoPrecio(ptDTO.getTbltipoprecio().getIdTipoPrecio());
		tblproducto=new Tblproducto(ptDTO.getTblproducto());
		tblproducto.setIdProducto(ptDTO.getTblproducto().getIdProducto());
		porcentaje=ptDTO.getPorcentaje();
	}

	public TblproductoTipoprecio(TblproductoTipoprecioId id,
			Tbltipoprecio tbltipoprecio, Tblproducto tblproducto, int porcentaje) {
		this.id = id;
		this.tbltipoprecio = tbltipoprecio;
		this.tblproducto = tblproducto;
		this.porcentaje = porcentaje;
	}

	public TblproductoTipoprecioId getId() {
		return this.id;
	}

	public void setId(TblproductoTipoprecioId id) {
		this.id = id;
	}

	public Tbltipoprecio getTbltipoprecio() {
		return this.tbltipoprecio;
	}

	public void setTbltipoprecio(Tbltipoprecio tbltipoprecio) {
		this.tbltipoprecio = tbltipoprecio;
	}

	public Tblproducto getTblproducto() {
		return this.tblproducto;
	}

	public void setTblproducto(Tblproducto tblproducto) {
		this.tblproducto = tblproducto;
	}

	public double getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

}
