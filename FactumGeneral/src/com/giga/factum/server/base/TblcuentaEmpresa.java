package com.giga.factum.server.base;

public class TblcuentaEmpresa implements java.io.Serializable{
	private Tblcuenta tblcuenta;
	private Tblempresa tblempresa;
	private TblcuentaEmpresaId id;
	public TblcuentaEmpresa() {
	}
	public TblcuentaEmpresa(Tblcuenta tblcuenta) {
		this.tblcuenta = tblcuenta;
	}
	public TblcuentaEmpresa(Tblcuenta tblcuenta, Tblempresa tblempresa) {
		this.tblcuenta = tblcuenta;
		this.tblempresa = tblempresa;
		this.id= new TblcuentaEmpresaId(tblempresa.getIdEmpresa(),tblcuenta.getIdCuenta()); 
	}
	public TblcuentaEmpresa(int idEmpresa, int idCuenta) {
		this.id= new TblcuentaEmpresaId(idEmpresa,idCuenta); 
	}
	public Tblcuenta getTblcuenta() {
		return tblcuenta;
	}
	public void setTblcuenta(Tblcuenta tblcuenta) {
		this.tblcuenta = tblcuenta;
	}
	public Tblempresa getTblempresa() {
		return tblempresa;
	}
	public void setTblempresa(Tblempresa tblempresa) {
		this.tblempresa = tblempresa;
	}
	public TblcuentaEmpresaId getId() {
		return id;
	}
	public void setId(TblcuentaEmpresaId id) {
		this.id = id;
	}
	
}
