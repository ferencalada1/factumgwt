package com.giga.factum.server.base;

import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;

public class TblproductoMultiImpuesto implements java.io.Serializable{

	private Integer idtblproducto_tblmulti_impuesto;
	private Tblproducto tblproducto;
	private TblmultiImpuesto tblmultiImpuesto;
	
	
	public TblproductoMultiImpuesto() {
	}
	public TblproductoMultiImpuesto(TblproductoMultiImpuestoDTO multImp) {
		this.idtblproducto_tblmulti_impuesto=multImp.getIdtblproducto_tblmulti_impuesto();
		this.tblproducto=new Tblproducto(multImp.getTblproducto());
		this.tblmultiImpuesto=new TblmultiImpuesto(multImp.getTblmultiImpuesto());
	}
	public TblproductoMultiImpuesto(Integer idtblproducto_tblmulti_impuesto, Tblproducto tblproducto,
			TblmultiImpuesto tblmultiImpuesto) {
		this.idtblproducto_tblmulti_impuesto = idtblproducto_tblmulti_impuesto;
		this.tblproducto = tblproducto;
		this.tblmultiImpuesto = tblmultiImpuesto;
	}
	public Integer getIdtblproducto_tblmulti_impuesto() {
		return idtblproducto_tblmulti_impuesto;
	}
	public void setIdtblproducto_tblmulti_impuesto(Integer idtblproducto_tblmulti_impuesto) {
		this.idtblproducto_tblmulti_impuesto = idtblproducto_tblmulti_impuesto;
	}
	public Tblproducto getTblproducto() {
		return tblproducto;
	}
	public void setTblproducto(Tblproducto tblproducto) {
		this.tblproducto = tblproducto;
	}
	public TblmultiImpuesto getTblmultiImpuesto() {
		return tblmultiImpuesto;
	}
	public void setTblmultiImpuesto(TblmultiImpuesto tblmultiImpuesto) {
		this.tblmultiImpuesto = tblmultiImpuesto;
	}
	
	
}
