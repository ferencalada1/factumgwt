package com.giga.factum.server.base;

// Generated 22/07/2011 10:55:01 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.server.DAOGenerico;

/**
 * Home object for domain model class Tbldtocomercial.
 * @see com.giga.factum.server.base.Tbldtocomercial
 * @author Hibernate Tools
 */
public class TbldtocomercialHome extends DAOGenerico<Tbldtocomercial>{
	/*

	private static final Log log = LogFactory.getLog(TbldtocomercialHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public void persist(Tbldtocomercial transientInstance) {
		log.debug("persisting Tbldtocomercial instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Tbldtocomercial instance) {
		log.debug("attaching dirty Tbldtocomercial instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Tbldtocomercial instance) {
		log.debug("attaching clean Tbldtocomercial instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Tbldtocomercial persistentInstance) {
		log.debug("deleting Tbldtocomercial instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Tbldtocomercial merge(Tbldtocomercial detachedInstance) {
		log.debug("merging Tbldtocomercial instance");
		try {
			Tbldtocomercial result = (Tbldtocomercial) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Tbldtocomercial findById(java.lang.Integer id) {
		log.debug("getting Tbldtocomercial instance with id: " + id);
		try {
			Tbldtocomercial instance = (Tbldtocomercial) sessionFactory
					.getCurrentSession().get(
							"com.giga.factum.server.base.Tbldtocomercial", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Tbldtocomercial instance) {
		log.debug("finding Tbldtocomercial instance by example");
		try {
			List results = sessionFactory
					.getCurrentSession()
					.createCriteria("com.giga.factum.server.base.Tbldtocomercial")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}*/
}
