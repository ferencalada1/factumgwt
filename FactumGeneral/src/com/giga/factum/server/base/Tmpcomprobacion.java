package com.giga.factum.server.base;

public class Tmpcomprobacion implements java.io.Serializable {

	private TmpcomprobacionId id;

	public Tmpcomprobacion() {
	}

	public Tmpcomprobacion(TmpcomprobacionId id) {
		this.id = id;
	}

	public TmpcomprobacionId getId() {
		return this.id;
	}

	public void setId(TmpcomprobacionId id) {
		this.id = id;
	}

}
