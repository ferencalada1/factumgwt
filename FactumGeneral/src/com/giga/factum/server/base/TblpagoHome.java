package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

/**
 * Home object for domain model class Tblpago.
 * @see com.giga.factum.server.base.Tblpago
 * @author Hibernate Tools
 */
public class TblpagoHome extends DAOGenerico<Tblpago>{
	private Session session;
	/*
	 * Da de baja a un pago y crea un nuevo con el valor de la diferencia
	 * @param pagado valor pagado debe ser menor que el valor del pago original
	 */
	
	
	Integer planCuenta = 1;//DEBE SER EXTRAIDO DE LA VARIABLE DE SESION
	
	public Tbltipodocumento findByNombreT(String nombre,String campo) {
		//session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipodocumento u where u."+campo+" = " +"'"+nombre+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<Tbltipodocumento> returnList = consulta.list();
		//session.close();
		return returnList.get(0);
	}
	public TblcuentaPlancuenta findByIdPlanIdCuenta2(String idplan,String idcuenta) {
		//session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from com.giga.factum.server.base.TblcuentaPlancuenta u where " +
				"u.tblplancuenta.idPlan = " +"'"+idplan+"' and u.tblcuenta.idCuenta='"+idcuenta+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<TblcuentaPlancuenta> returnList = consulta.list();
		//session.close();
		return returnList.get(0);
	}
	
	public void modificarpago(Tblpago t, Tbldtocomercial tbl, Set<TbldtocomercialTbltipopago> tipoPagos) throws ErrorAlGrabar {
		try {
			System.out.println("Modificar pago home ");
			System.out.println("Modificar pago home "+t.getTbldtocomercial().getTipoTransaccion());
			System.out.println("Modificar pago home "+tbl.getTipoTransaccion());
			System.out.println("Modificar pago home "+tbl.getNumRealTransaccion());
			session = HibernateUtil.getSessionFactory().openSession();
			System.out.println("Modificar pago home session");
	 		session.beginTransaction();
	 		System.out.println("Modificar pago home transaction");
	 		t.setEstado('1');
	 		System.out.println("Modificar pago home estado");
	 		//if(tbl.getTbldtocomercialTbltipopagos().size()>0){
				Integer id=(Integer) session.save(tbl); //grabar nuevo documento
				Iterator iterTP= tipoPagos.iterator();
				System.out.println("Modificar pago home "+tipoPagos.size());
				TbldtocomercialTbltipopago pago;
				String concepto="";
				char signo = 'd';
				Integer tipoT = tbl.getTipoTransaccion();
				Tblmovimiento movimiento;
				System.out.println("Modificar pago home antes iter");
				while (iterTP.hasNext()) {
					pago = (TbldtocomercialTbltipopago) iterTP.next();
					pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
					pago.setCantidad(CValidarDato.getDecimal(2,t.getValor()));
					System.out.println("tipo documento "+t.getTbldtocomercial().getTipoTransaccion());
					if(t.getTbldtocomercial().getTipoTransaccion()==1 || t.getTbldtocomercial().getTipoTransaccion()==10){
						pago.setTipo('1');
						System.out.println("ANTES DE");
						if(pago.getTbltipopago().getTipoPago().equals("CONTADO")){
							System.out.println("ADENTRO");
							pago.setTipo('0'); ///preguntar al jhonnt
						}
						concepto="Por concepto de pago de Dto:"+tbl.getNumRealTransaccion();
						signo = 'd';
						tipoT=11;
					}else{
						pago.setTipo('0');
						concepto="Por concepto de cobro de Dto:"+tbl.getNumRealTransaccion();
						signo = 'h';
					}
					System.out.println("pago "+pago.getCantidad());
					System.out.println(signo);
					session.save(pago);
					//AQUI VAMOS A GENERAR LOS ASIENTOS CONTABLES DE LOS PAGOS
					//Primero generamos la cabecera del movimiento
					
					movimiento = new Tblmovimiento(tbl, concepto,t.getFechaRealPago());
					Integer idMov = (Integer) session.save(movimiento);
					//Ahora vamos a generar los detalles del movimiento
					
					//LISTADO DE LOS DETALLES DEL MOVIMIENTO CONTABLE
					Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
					
					TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento;
					TblmovimientoTblcuentaTblplancuenta detalleMovimiento;
					TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento2;
					TblmovimientoTblcuentaTblplancuenta detalleMovimiento2;
					TblcuentaPlancuenta cuentaPlan;
					System.out.println("TIPO TRANS: "+tipoT); 
					Tbltipodocumento tipoDoc =  findByNombreT(tipoT+"", "idTipo");
					Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
					System.out.println("ID: "+IdCuentaAsiento);
					idDetalleMovimiento =new 
					TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); 		
					System.out.println("IDMOV: "+idMov);
					//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
					//T findByIdPlanIdCuenta(String idplan,String idcuenta)
					cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
							IdCuentaAsiento+"");//idcuenta)
					System.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
					detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
							idDetalleMovimiento,
							cuentaPlan,
							movimiento, CValidarDato.getDecimal(2,t.getValor()), signo);
					
					
					//VAMOS EXTRER LA CUENTA Y EL PLAN DE CUENTA ASOCIADO AL PAGO

					//-------------------------------------------------------------------------------
					//CREAMOS EL DETALLE PARA EL IVA, SEA COBRADO O PAGADO
					//tipoDoc =  (Tbltipodocumento) findByNombre(tipoIva, "TipoDoc");
					//pago = 
					
					Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipopago " +
							"u where u.idTipoPago ="+ "'"+pago.getTbltipopago().getIdTipoPago()+"'");	
					//Le pasamos el registro desde el cual va a extraer
					consulta.setFirstResult(0);
					//Le pasamos el numero maximo de registros, paginacion.
					consulta.setMaxResults(1);
					List<Tbltipopago> returnList = consulta.list();
					//session.close();
					Tbltipopago tipopago =  returnList.get(0);	
					System.out.println("ID Cuenta: "+tipopago.getIdCuenta()+" - "+tipopago.getIdTipoPago());
					System.out.println("PLAN Cuenta: "+planCuenta.toString());
					
					cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
							tipopago.getIdCuenta()+"");//idcuenta)	
					System.out.println("CUENTAPlan "+planCuenta.toString()+": "+
							tipopago.getIdCuenta()+"");
					idDetalleMovimiento2 =new 
					TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tipopago.getIdCuenta());
					System.out.println("CUENTAPlanID "+idMov+" "+planCuenta.toString()+": "+
							tipopago.getIdCuenta()+"");
					if(signo=='d')
						signo='h';
					else
						signo='d';
					System.out.println("Signo "+signo);
					detalleMovimiento2 = new TblmovimientoTblcuentaTblplancuenta(
							idDetalleMovimiento2,
							cuentaPlan,
							movimiento, CValidarDato.getDecimal(2,pago.getCantidad()), signo);
					System.out.println("detalle "+idDetalleMovimiento2+" "+cuentaPlan.getId().getIdCuenta()+
							" "+cuentaPlan.getId().getIdPlan()+" "+movimiento.getIdMovimiento()
							+CValidarDato.getDecimal(2,pago.getCantidad()));
					//Agregamos el detalle del coumento al array
					//considerando primero agregar el que va al debe
					
					if(signo=='d')
					{
						session.save(detalleMovimiento);
						System.out.println("Signod2 "+signo);
//						if (dto)
						if (!tipopago.getIdTipoPago().equals(5)) session.save(detalleMovimiento2);
						System.out.println("Signod1 "+signo);
						
					}
					else
					{
						System.out.println("Signoe1 "+signo);
						session.save(detalleMovimiento);
						System.out.println("Signoe2 "+signo);
						session.save(detalleMovimiento2);
						System.out.println("Signoe3 "+signo);
						
					}
									
					//movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
					System.out.println("TAMA�O MOVIMIENTO: "+detallesMovimiento.size());
					System.out.println("ID DTO MOVIMIENTO: "+movimiento.getTbldtocomercial().getIdDtoComercial());
					//session.update(movimiento);
					
				}	
			/*}else{
				session.getTransaction().rollback();
				System.out.println("error en modificarcrearpago no existen tipos de pago asignados al documento comercial");
			}*/
			session.update(t);
	 		session.getTransaction().commit();
	 		//session.close();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		System.out.println("error en modificarpago "+e.getMessage());
	 		throw new ErrorAlGrabar(e.getMessage());
		}finally{
			session.close();
		}
	}
	
	public void modificarcrearPago(Tblpago t,double pagado,Tbldtocomercial tbl, Set<TbldtocomercialTbltipopago> tipoPagos) throws ErrorAlGrabar {
		
		try {
			double valor=0.0;
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		valor=CValidarDato.getDecimal(2,t.getValor());
	 		pagado=CValidarDato.getDecimal(2,pagado);
	 		t.setValor(pagado);
	 		tbl.setSubtotal(pagado);
	 		Integer id=(Integer) session.save(tbl); //grabar nuevo documento
			Iterator iterTP= tipoPagos.iterator();
			TbldtocomercialTbltipopago pago;
			String concepto="";
			char signo = 'd';
			Integer tipoT = tbl.getTipoTransaccion();
			while (iterTP.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterTP.next();
				pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
				pago.setCantidad(CValidarDato.getDecimal(2,t.getValor()));
				System.out.println("tipo documento "+t.getTbldtocomercial().getTipoTransaccion());
				if(t.getTbldtocomercial().getTipoTransaccion()==1 || t.getTbldtocomercial().getTipoTransaccion()==10){
					pago.setTipo('1');
//					if(pago.getTbltipopago().getTipoPago().equals("CONTADO")){
//						System.out.println("ADENTRO");
//						pago.setTipo('0'); ///preguntar al jhonnt
//					}
					concepto="Por concepto de pago de Dto:"+tbl.getNumRealTransaccion();
					signo = 'd';
					tipoT=11;
				}else{
					pago.setTipo('0');
					concepto="Por concepto de cobro de Dto:"+tbl.getNumRealTransaccion();
					signo = 'h';
				}
				System.out.println("pago "+pago.getCantidad());
				session.save(pago);
				//AQUI VAMOS A GENERAR LOS ASIENTOS CONTABLES DE LOS PAGOS
				//Primero generamos la cabecera del movimiento
				Tblmovimiento movimiento;
				movimiento = new Tblmovimiento(tbl, concepto,t.getFechaRealPago());
				Integer idMov = (Integer) session.save(movimiento);
				//Ahora vamos a generar los detalles del movimiento
				
				//LISTADO DE LOS DETALLES DEL MOVIMIENTO CONTABLE
				Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
				
				TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento;
				TblmovimientoTblcuentaTblplancuenta detalleMovimiento;
				TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento2;
				TblmovimientoTblcuentaTblplancuenta detalleMovimiento2;
				TblcuentaPlancuenta cuentaPlan;
				System.out.println("TIPO TRANS: "+tipoT); 
				Tbltipodocumento tipoDoc =  findByNombreT(tipoT+"", "idTipo");
				Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
				idDetalleMovimiento =new 
				TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); 		
				//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
				//T findByIdPlanIdCuenta(String idplan,String idcuenta)
				cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
						IdCuentaAsiento+"");//idcuenta)
				System.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
				detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
						idDetalleMovimiento,
						cuentaPlan,
						movimiento, CValidarDato.getDecimal(2,t.getValor()), signo);
				
				detalleMovimiento.setTblmovimiento(movimiento);
				//VAMOS EXTRER LA CUENTA Y EL PLAN DE CUENTA ASOCIADO AL PAGO

				//-------------------------------------------------------------------------------
				//CREAMOS EL DETALLE PARA EL IVA, SEA COBRADO O PAGADO
				//tipoDoc =  (Tbltipodocumento) findByNombre(tipoIva, "TipoDoc");
				//pago = 
				Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipopago " +
						"u where u.idTipoPago ="+ "'"+pago.getTbltipopago().getIdTipoPago()+"'");	
				//Le pasamos el registro desde el cual va a extraer
				consulta.setFirstResult(0);
				//Le pasamos el numero maximo de registros, paginacion.
				consulta.setMaxResults(1);
				List<Tbltipopago> returnList = consulta.list();
				//session.close();
				Tbltipopago tipopago =  returnList.get(0);	
				System.out.println("ID Cuenta: "+tipopago.getIdCuenta()+"");
				System.out.println("PLAN Cuenta: "+planCuenta.toString());
				
				cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
						tipopago.getIdCuenta()+"");//idcuenta)	
				idDetalleMovimiento2 =new 
				TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tipopago.getIdCuenta());
				if(signo=='d')
					signo='h';
				else
					signo='d';
				
				detalleMovimiento2 = new TblmovimientoTblcuentaTblplancuenta(
						idDetalleMovimiento2,
						cuentaPlan,
						movimiento, CValidarDato.getDecimal(2,pago.getCantidad()), signo);
				detalleMovimiento2.setTblmovimiento(movimiento);
				//Agregamos el detalle del coumento al array
				//considerando primero agregar el que va al debe
				
				if(signo=='d')
				{
					movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detalleMovimiento2);
					movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detalleMovimiento);
				}
				else
				{
					movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detalleMovimiento);
					movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detalleMovimiento2);
				}
								
				//movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
				//movimiento.getTblmovimientoTblcuentaTblplancuentas().add(detallesMovimiento);
				System.out.println("TAMA�O MOVIMIENTO: "+detallesMovimiento.size());
				System.out.println("ID DTO MOVIMIENTO: "+movimiento.getTbldtocomercial().getIdDtoComercial());
				session.update(movimiento);
				
				//session.flush();
				//*************************************************
				//*************************************************
			}
			
	 		session.update(t);
	 		System.out.println("modifico pago y creo dtco");
	 		Tblpago pagonuevo=new Tblpago();
	 		pagonuevo.setEstado('0');
	 		pagonuevo.setFechaRealPago(null);
	 		pagonuevo.setFechaVencimiento(t.getFechaVencimiento());
	 		double val=valor-pagado;
	 		val=CValidarDato.getDecimal(2,val);
	 		//pagonuevo.setValor(Math.rint(val*100)/100);
	 		pagonuevo.setValor(val);
	 		pagonuevo.setTbldtocomercial(t.getTbldtocomercial());
	 		pagonuevo.setIdEmpresa(t.getIdEmpresa());
	 		pagonuevo.setEstablecimiento(t.getEstablecimiento());
			session.save(pagonuevo);
			System.out.println("SE GRABO LOS MOVIMIENTOS: ");
			
			session.flush();
			session.clear();
			session.getTransaction().commit();
		} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		System.out.println("error en funcion modificarcrearPago adeeeenntro "+e.getMessage());
	 		throw new ErrorAlGrabar(e.getMessage()+"adentro");
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}

}
