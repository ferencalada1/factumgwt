package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

/**
 * Home object for domain model class Tblpago.
 * @see com.giga.factum.server.base.Tblpago
 * @author Hibernate Tools
 */
public class TblpedidoHome extends DAOGenerico<Tblpedido>{
	
}
