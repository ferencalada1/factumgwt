package com.giga.factum.server.base;

import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;

public class TbldtocomercialdetalleTblmultiImpuesto implements java.io.Serializable{
	private Integer idtbldtocomercialdetalle_tblmulti_impuesto;
	private Tbldtocomercialdetalle tbldtocomercialdetalle;
	private Double porcentaje;
	private char tipo;
	private String nombre;
	public TbldtocomercialdetalleTblmultiImpuesto() {
	}
	public TbldtocomercialdetalleTblmultiImpuesto(DtoComDetalleMultiImpuestosDTO detalleMultiImpuestos){
		this.nombre=detalleMultiImpuestos.getNombre();
		this.tbldtocomercialdetalle=(detalleMultiImpuestos.getTbldtocomercialdetalle()!=null)?
				new Tbldtocomercialdetalle(detalleMultiImpuestos.getTbldtocomercialdetalle()):null;
		this.porcentaje=detalleMultiImpuestos.getPorcentaje();
		this.tipo=detalleMultiImpuestos.getTipo();
	}
	public TbldtocomercialdetalleTblmultiImpuesto(Integer idtbldtocomercialdetalle_tblmulti_impuesto,
			Tbldtocomercialdetalle tbldtocomercialdetalle, Double porcentaje, char tipo, String nombre) {
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
		this.tbldtocomercialdetalle = tbldtocomercialdetalle;
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}

	public TbldtocomercialdetalleTblmultiImpuesto(Double porcentaje, char tipo, String nombre) {
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}

	public TbldtocomercialdetalleTblmultiImpuesto(Integer idtbldtocomercialdetalle_tblmulti_impuesto, Double porcentaje,
			char tipo, String nombre) {
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}

	public Integer getIdtbldtocomercialdetalle_tblmulti_impuesto() {
		return idtbldtocomercialdetalle_tblmulti_impuesto;
	}
	public void setIdtbldtocomercialdetalle_tblmulti_impuesto(Integer idtbldtocomercialdetalle_tblmulti_impuesto) {
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
	}
	public Tbldtocomercialdetalle getTbldtocomercialdetalle() {
		return tbldtocomercialdetalle;
	}
	public void setTbldtocomercialdetalle(Tbldtocomercialdetalle tbldtocomercialdetalle) {
		this.tbldtocomercialdetalle = tbldtocomercialdetalle;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
