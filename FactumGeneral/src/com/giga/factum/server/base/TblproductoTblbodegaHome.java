package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.util.LinkedList;
import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;
import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
/**
 * Home object for domain model class TblproductoTblbodega.
 * @see com.giga.factum.server.base.TblproductoTblbodega
 * @author Hibernate Tools
 */
public class TblproductoTblbodegaHome  extends DAOGenerico<TblproductoTblbodega>{
	
	private static final Log log = LogFactory.getLog(TblproductoTblbodegaHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	private org.hibernate.classic.Session session;
	
	/**
	 * Metodo para buscar una tupla 
	 * @param idBodega id de la Bodega
	 * @param idProducto id del Producto
	 * @return entidad
	 */
	public double findStock(String idProducto,String idBodega) {
		org.hibernate.classic.Session session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from TblproductoTblbodega u where u.tblbodega.idBodega="+"'"+idBodega
				+"' and u.tblproducto.idProducto="+"'"+idProducto+"'");
		//from TblproductoTblbodega u where u.tblbodega.idBodega='3' and u.tblproducto.idProducto='10'
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<TblproductoTblbodega> returnList = consulta.list();
		session.close();
		return returnList.get(0).getCantidad();
	}
	
	public boolean existe(String idProducto,String idBodega){
		org.hibernate.classic.Session session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from TblproductoTblbodega u where u.tblbodega.idBodega="+"'"+idBodega
				+"' and u.tblproducto.idProducto="+"'"+idProducto+"'");
		List<TblproductoTblbodega> returnList = consulta.list();
		session.close();
		if(returnList!=null){
			return true;
		}else{
			return false;
		}
		
	} 
	public void grabarLista(TblproductoTblbodega[] b) throws ErrorAlGrabar {
		org.hibernate.classic.Session session = null;
		try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		for(int i=0;i<b.length;i++){
	 			session.update(b[i]);
	 			//session.saveOrUpdate(b[i]);
		 	}
	 	 		
	 		session.getTransaction().commit();
		} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage()+"adentro");
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}
	public void grabar(TblproductoTblbodega t) throws ErrorAlGrabar {
	 	try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		session.merge(t);
	 		session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}


	public TblproductoTblbodega loadBodega(int idProducto,int idBodega) {
		session = HibernateUtil.getSessionFactory().openSession();
		TblproductoTblbodega tblproductotblbodega = new TblproductoTblbodega();
		Query consulta = session.createQuery("from TblproductoTblbodega u where u.tblbodega.idBodega="+"'"+idBodega
				+"' and u.tblproducto.idProducto="+"'"+idProducto+"'");
		//from TblproductoTblbodega u where u.tblbodega.idBodega='3' and u.tblproducto.idProducto='10'
		System.out.println("Consulta BODEGAS: "+consulta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<TblproductoTblbodega> returnList = consulta.list();
		System.out.println("NUMERO Consulta BODEGAS: "+returnList.size());
		tblproductotblbodega=returnList.get(0);
		//System.out.println("AQUI LA BODEGA ES"+tblproductotblbodega.getTblbodega().getNombre()+"  "+tblproductotblbodega.getTblbodega().getIdBodega());
		session.close();
		return tblproductotblbodega;
	}
	
	public String TransferirBodega(LinkedList<TblTraspasoBodegaProducto> probod){
		String mensaje="";
		System.out.println("Iniciando transaccion");
		try {
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		for(int i=0;i<probod.size();i++){
	 			Query consulta = session.createQuery("from TblproductoTblbodega u where u.tblproducto.idProducto= '"+probod.get(i).getProductoBodega().getTblproducto().getIdProducto()+"' " +
	 					"and  u.tblbodega.idBodega= '"+probod.get(i).getIdBodega()+"'");
	 			consulta.setFirstResult(0);
	 			consulta.setMaxResults(1);
	 			List<TblproductoTblbodega> returnList = consulta.list();
	 			if(returnList.get(0).getCantidad()>=probod.get(i).getProductoBodega().getCantidad()){
	 				double stock=0;
	 				TblproductoTblbodega nuevo=new TblproductoTblbodega();
	 				TblproductoTblbodega anterior=new TblproductoTblbodega();
	 				stock=probod.get(i).getProductoBodega().getCantidad();
	 				anterior=returnList.get(0);
	 				anterior.setCantidad(anterior.getCantidad()-stock);
	 				session.update(anterior);
	 				consulta = session.createQuery("from TblproductoTblbodega u where u.tblproducto.idProducto= '"+probod.get(i).getProductoBodega().getTblproducto().getIdProducto()+"' " +
		 					"and  u.tblbodega.idBodega= '"+probod.get(i).getProductoBodega().getTblbodega().getIdBodega()+"'");
		 			consulta.setFirstResult(0);
		 			consulta.setMaxResults(1);
		 			returnList = consulta.list();
		 			if(returnList.isEmpty()){
		 				session.save(probod.get(i).getProductoBodega());
		 			}else{
		 				anterior=returnList.get(0);
		 				anterior.setCantidad(anterior.getCantidad()+probod.get(i).getProductoBodega().getCantidad());
		 				session.update(anterior);
		 			}
		 			mensaje="Transacci�n Exitosa";
		 			
	 			}else{
	 				mensaje="No existe suficiente stock";
	 				System.out.print("No existe suficiente stock");
	 				session.getTransaction().rollback();
	 				break;
	 			}
		 	}
	 		session.getTransaction().commit();
		}catch (HibernateException e) {
			if(!mensaje.equals("No existe suficiente stock")){
				System.out.println(e.getMessage());
		 		session.getTransaction().rollback();
		 		mensaje=e.getMessage();
			}
			
	 		//throw new ErrorAlGrabar(e.getMessage()+"adentro");
	 	} finally {
	 		if(session!=null)
	 			session.close();
	 	}
		return mensaje;
	}
	
	public Double SumaStockBodegas(Integer idProducto){
		org.hibernate.classic.Session session = HibernateUtil.getSessionFactory().openSession();
		Double stock=0.0;
		Object count =  session.createQuery("SELECT sum(pb.cantidad) FROM TblproductoTblbodega pb where pb.tblproducto.idProducto="+idProducto).uniqueResult();;
			System.out.println("SELECT sum(pb.cantidad) FROM TblproductoTblbodega pb where pb.tblproducto.idProducto="+idProducto);
			//session.getTransaction().commit();
			session.close();
			try{
				//system.out.println("VALORRETENCION= "+String.valueOf(count));
				//system.out.println("VALORRETENCIONDOUBLE= "+Double.parseDouble(String.valueOf(count)));
				System.out.println("COUNT: "+count);
				stock=Double.parseDouble(String.valueOf(count));
				System.out.println("STOCK: "+stock);
				return stock;
			}catch(Exception e){
				System.out.println("ENTRO AL CATCH");
				return 0.0;
			}		
	}

	
}
