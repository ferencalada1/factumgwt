package com.giga.factum.server.base;

public class TbltipoproductoTblProducto implements java.io.Serializable{

	private int idtbltipoproductoTblproducto;
	private Tblproducto tblproducto;
	private Tbltipoproducto tbltipoproducto;
	
	
	public TbltipoproductoTblProducto(int idtbltipoproductoTblproducto, Tblproducto tblproducto,
			Tbltipoproducto tbltipoproducto) {
		this.idtbltipoproductoTblproducto = idtbltipoproductoTblproducto;
		this.tblproducto = tblproducto;
		this.tbltipoproducto = tbltipoproducto;
	}
	public int getIdtbltipoproductoTblproducto() {
		return idtbltipoproductoTblproducto;
	}
	public void setIdtbltipoproductoTblproducto(int idtbltipoproductoTblproducto) {
		this.idtbltipoproductoTblproducto = idtbltipoproductoTblproducto;
	}
	public Tblproducto getTblproducto() {
		return tblproducto;
	}
	public void setTblproducto(Tblproducto tblproducto) {
		this.tblproducto = tblproducto;
	}
	public Tbltipoproducto getTbltipoproducto() {
		return tbltipoproducto;
	}
	public void setTbltipoproducto(Tbltipoproducto tbltipoproducto) {
		this.tbltipoproducto = tbltipoproducto;
	}
	
}
