package com.giga.factum.server.base;

import com.giga.factum.client.DTO.PosicionDTO;

public class Tblposicion implements java.io.Serializable{
	
	private Integer id;
	private String NombreDocumento;
	private Integer Posicion;
	private Integer largo;
	private String Campo;
	
	
	
	public Tblposicion(){
		
	}
	public Tblposicion(PosicionDTO pos){
		this.Campo=pos.getCampo();
		this.largo=pos.getLargo();
		this.NombreDocumento=pos.getNombreDocumento();
		this.Posicion=pos.getPosicion();
	}

	public void setPosicion(Integer posicion) {
		Posicion = posicion;
	}

	public Integer getPosicion() {
		return Posicion;
	}

	
	public void setNombreDocumento(String nombreDocumento) {
		NombreDocumento = nombreDocumento;
	}

	public String getNombreDocumento() {
		return NombreDocumento;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setLargo(Integer largo) {
		this.largo = largo;
	}

	public Integer getLargo() {
		return largo;
	}

	public void setCampo(String campo) {
		Campo = campo;
	}

	public String getCampo() {
		return Campo;
	}

}
