package com.giga.factum.server.base;

import java.util.Date;

public class Tblguiaremision implements java.io.Serializable{
	private Integer idGuiaRemision;
	private Date fechaDevolucion;
	private Tbldtocomercial tbldtocomercial;
	private Tbldtocomercial dtoguiaremision;
	private Tblpersona transportista;
	private Tblunidad unidad;
	private String motivo;
	private String nAutorizacion;
	private Integer numRealDocumento;
	private String observacion;
	private Double peso;
	private Integer piezas;
	private String puntoDestino;
	private String puntoPartida;
	private String tipoDocumento;
	private String placa;
	public Tblguiaremision(Integer idGuiaRemision, Date fechaDevolucion, Tbldtocomercial tbldtocomercial,
			Tbldtocomercial dtoguiaremision, Tblpersona transportista, Tblunidad unidad, String motivo,
			String nAutorizacion, Integer numRealDocumento, String observacion, Double peso, Integer piezas,
			String puntoDestino, String puntoPartida, String tipoDocumento, String placa) {
		this.idGuiaRemision = idGuiaRemision;
		this.fechaDevolucion = fechaDevolucion;
		this.tbldtocomercial = tbldtocomercial;
		this.dtoguiaremision = dtoguiaremision;
		this.transportista = transportista;
		this.unidad = unidad;
		this.motivo = motivo;
		this.nAutorizacion = nAutorizacion;
		this.numRealDocumento = numRealDocumento;
		this.observacion = observacion;
		this.peso = peso;
		this.piezas = piezas;
		this.puntoDestino = puntoDestino;
		this.puntoPartida = puntoPartida;
		this.tipoDocumento = tipoDocumento;
		this.placa = placa;
	}
	public Integer getIdGuiaRemision() {
		return idGuiaRemision;
	}
	public void setIdGuiaRemision(Integer idGuiaRemision) {
		this.idGuiaRemision = idGuiaRemision;
	}
	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public Tbldtocomercial getTbldtocomercial() {
		return tbldtocomercial;
	}
	public void setTbldtocomercial(Tbldtocomercial tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}
	public Tbldtocomercial getDtoguiaremision() {
		return dtoguiaremision;
	}
	public void setDtoguiaremision(Tbldtocomercial dtoguiaremision) {
		this.dtoguiaremision = dtoguiaremision;
	}
	public Tblpersona getTransportista() {
		return transportista;
	}
	public void setTransportista(Tblpersona transportista) {
		this.transportista = transportista;
	}
	public Tblunidad getUnidad() {
		return unidad;
	}
	public void setUnidad(Tblunidad unidad) {
		this.unidad = unidad;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getnAutorizacion() {
		return nAutorizacion;
	}
	public void setnAutorizacion(String nAutorizacion) {
		this.nAutorizacion = nAutorizacion;
	}
	public Integer getNumRealDocumento() {
		return numRealDocumento;
	}
	public void setNumRealDocumento(Integer numRealDocumento) {
		this.numRealDocumento = numRealDocumento;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Integer getPiezas() {
		return piezas;
	}
	public void setPiezas(Integer piezas) {
		this.piezas = piezas;
	}
	public String getPuntoDestino() {
		return puntoDestino;
	}
	public void setPuntoDestino(String puntoDestino) {
		this.puntoDestino = puntoDestino;
	}
	public String getPuntoPartida() {
		return puntoPartida;
	}
	public void setPuntoPartida(String puntoPartida) {
		this.puntoPartida = puntoPartida;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
}
