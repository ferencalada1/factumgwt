package com.giga.factum.server.base;

// Generated 22/07/2011 10:55:01 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.server.DAOGenerico;

/**
 * Home object for domain model class TbldtocomercialTbltipopago.
 * @see com.giga.factum.server.base.TbldtocomercialTbltipopago
 * @author Hibernate Tools
 */
public class TbldtocomercialTbltipopagoHome  extends DAOGenerico<TbldtocomercialTbltipopago>{

	private static final Log log = LogFactory
			.getLog(TbldtocomercialTbltipopagoHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public void persist(TbldtocomercialTbltipopago transientInstance) {
		log.debug("persisting TbldtocomercialTbltipopago instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TbldtocomercialTbltipopago instance) {
		log.debug("attaching dirty TbldtocomercialTbltipopago instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TbldtocomercialTbltipopago instance) {
		log.debug("attaching clean TbldtocomercialTbltipopago instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TbldtocomercialTbltipopago persistentInstance) {
		log.debug("deleting TbldtocomercialTbltipopago instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TbldtocomercialTbltipopago merge(
			TbldtocomercialTbltipopago detachedInstance) {
		log.debug("merging TbldtocomercialTbltipopago instance");
		try {
			TbldtocomercialTbltipopago result = (TbldtocomercialTbltipopago) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TbldtocomercialTbltipopago findById(
			com.giga.factum.server.base.TbldtocomercialTbltipopagoId id) {
		log.debug("getting TbldtocomercialTbltipopago instance with id: " + id);
		try {
			TbldtocomercialTbltipopago instance = (TbldtocomercialTbltipopago) sessionFactory
					.getCurrentSession()
					.get("com.giga.factum.server.base.TbldtocomercialTbltipopago",
							id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TbldtocomercialTbltipopago instance) {
		log.debug("finding TbldtocomercialTbltipopago instance by example");
		try {
			List results = sessionFactory
					.getCurrentSession()
					.createCriteria(
							"com.giga.factum.server.base.TbldtocomercialTbltipopago")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
