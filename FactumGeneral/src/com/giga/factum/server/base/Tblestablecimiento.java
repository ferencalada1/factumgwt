package com.giga.factum.server.base;

import java.util.Set;

import com.giga.factum.client.DTO.EstablecimientoDTO;

public class Tblestablecimiento implements java.io.Serializable{
	
	private TblestablecimientoTblempresaId id;
	private int idEmpresa;
	private String direccion;
	private String establecimiento;
	private String nick;
	private String mail;
	private Set<Tblcorreorespaldo> tblcorreosForEstablecimiento;
	private char activo;
	
	public Tblestablecimiento() {
		
	}
	public Tblestablecimiento(int idEmpresa, String direccion, String establecimiento, String nick, String mail, char activo) {
		this.id=new TblestablecimientoTblempresaId(idEmpresa,establecimiento);
		this.idEmpresa = idEmpresa;
		this.direccion = direccion;
		this.establecimiento = establecimiento;
		this.nick = nick;
		this.mail = mail;
		this.activo = activo;
	}
	
	public Tblestablecimiento(int idEmpresa, String direccion, String establecimiento, String nick, String mail, char activo,
			Set<Tblcorreorespaldo> tblcorreorespaldo) {
		this.id=new TblestablecimientoTblempresaId(idEmpresa,establecimiento);
		this.idEmpresa = idEmpresa;
		this.direccion = direccion;
		this.establecimiento = establecimiento;
		this.nick = nick;
		this.mail = mail;
		this.activo = activo;
		this.tblcorreosForEstablecimiento = tblcorreorespaldo;
	}
	
	public Tblestablecimiento(EstablecimientoDTO establecimiento) {
		this.id=new TblestablecimientoTblempresaId(establecimiento.getIdEmpresa(),establecimiento.getEstablecimiento());
		this.idEmpresa=establecimiento.getIdEmpresa();
		this.direccion=establecimiento.getDireccion();
		this.establecimiento=establecimiento.getEstablecimiento();
		this.nick=establecimiento.getNick();
		this.mail=establecimiento.getMail();
		this.activo=establecimiento.getActivo();
	}
	

	/**
	 * @return the idEmpresa
	 */
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	/**
	 * @param idEmpresa the idEmpresa to set
	 */
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the establecimiento
	 */
	public String getEstablecimiento() {
		return establecimiento;
	}
	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	/**
	 * @return the id
	 */
	public TblestablecimientoTblempresaId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(TblestablecimientoTblempresaId id) {
		this.id = id;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public char getActivo() {
		return activo;
	}
	public void setActivo(char activo) {
		this.activo = activo;
	}
	public Set<Tblcorreorespaldo> getTblcorreosForEstablecimiento() {
		return tblcorreosForEstablecimiento;
	}
	public void setTblcorreosForEstablecimiento(Set<Tblcorreorespaldo> tblcorreosForEstablecimiento) {
		this.tblcorreosForEstablecimiento = tblcorreosForEstablecimiento;
	}
	
	
}
