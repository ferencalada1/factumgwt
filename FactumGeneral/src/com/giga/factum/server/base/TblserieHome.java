package com.giga.factum.server.base;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

public class TblserieHome extends DAOGenerico<Tblserie>{
	
	public void grabarSeries(LinkedList<Tblserie> b) throws ErrorAlGrabar {
		org.hibernate.classic.Session session = null;
		try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		
	 		for(int i=0;i<b.size();i++){
	 			System.out.println(i);
	 			System.out.println(b.get(i).getSerie());
	 			session.save(b.get(i));
	 		}
	 	 	session.getTransaction().commit();
		} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage()+" adentro");
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}
	
	public List ListarSeriesProducto(int idProducto) {
		org.hibernate.classic.Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("from Tblserie u where u.tblproducto.idProducto='"+String.valueOf(idProducto)+"'");
		Query consulta = session.createQuery("from Tblserie u where u.tblproducto.idProducto='"+String.valueOf(idProducto)+"'");
		List<Tblserie> returnList = consulta.list();
		session.close();
		return returnList;
	}

}
