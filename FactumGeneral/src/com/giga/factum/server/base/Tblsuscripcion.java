package com.giga.factum.server.base;

import java.util.Date;

public class Tblsuscripcion implements java.io.Serializable{

	private TblsuscripcionId id;
	private int idEmpresa;
	private int idSuscripcion;
	private Tblservicio tblservicio;
	private int idServicio;
	private String estado;
	private Date fechaFin;
	private Date fechaInicio;
	
	public Tblsuscripcion(){
		
	}
	
	public Tblsuscripcion(TblsuscripcionId id, int idEmpresa, int idSuscripcion, int idServicio, String estado,
			Date fechaFin, Date fechaInicio) {
		this.id = id;
		this.idEmpresa = idEmpresa;
		this.idSuscripcion = idSuscripcion;
		this.idServicio = idServicio;
		this.estado = estado;
		this.fechaFin = fechaFin;
		this.fechaInicio = fechaInicio;
	}
	public TblsuscripcionId getId() {
		return id;
	}
	public void setId(TblsuscripcionId id) {
		this.id = id;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public int getIdSuscripcion() {
		return idSuscripcion;
	}
	public void setIdSuscripcion(int idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}
	public int getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Tblservicio getTblservicio() {
		return tblservicio;
	}
	public void setTblservicio(Tblservicio tblservicio) {
		this.tblservicio = tblservicio;
	}
	
	
}
