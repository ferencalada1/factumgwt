package com.giga.factum.server.base;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

public class TblproductoMultiImpuestoHome  extends DAOGenerico<TblproductoMultiImpuesto>{
	private static final Log log = LogFactory.getLog(TblproductoTipoprecioHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	public Iterator listarProdMultImp(int codPro){
		org.hibernate.classic.Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("select t.idtblproducto_tblmulti_impuesto ,t.tblproducto.idProducto, t.tblmultiImpuesto.idmultiImpuesto from TblproductoMultiImpuesto t where t.tblproducto.idProducto =" +codPro);			
		System.out.println(consulta);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public void grabarList(TblproductoMultiImpuesto[] t) throws ErrorAlGrabar {
	 	org.hibernate.classic.Session session = null;
		try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		String hqlDelete = "delete TblproductoMultiImpuesto as c where c.tblproducto.idProducto = :idDto";
			// or String hqlDelete = "delete Customer where name = :oldName";
			session.createQuery( hqlDelete )
			        .setString( "idDto", t[0].getTblproducto().getIdProducto()+"" )
			        .executeUpdate();
	 		for(int i=0;i<t.length;i++){
	 			System.out.println("En grabarlist multiimpuesto prodMult en for "+t[i].getIdtblproducto_tblmulti_impuesto()+", "+
	 					t[i].getTblproducto().getIdProducto()+", "+t[i].getTblmultiImpuesto().getIdmultiImpuesto()+" "+t[i].getTblmultiImpuesto().toString());
	 			session.saveOrUpdate(t[i]); 
	 		}
	 		session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		System.out.println(e.getLocalizedMessage());
			System.out.println("Error "+e.getMessage());
			e.printStackTrace(System.out);
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	} 
	}
}
