package com.giga.factum.server.base;

public class Tblcorreorespaldo implements java.io.Serializable {
	private int idtblcorreo_respaldo;
	private String mail;
	private Tblestablecimiento establecimiento;
	public Tblcorreorespaldo(int idtblcorreo_respaldo, String mail, Tblestablecimiento establecimiento) {
		this.idtblcorreo_respaldo = idtblcorreo_respaldo;
		this.mail = mail;
		this.establecimiento = establecimiento;
	}
	public Tblcorreorespaldo() {
	}
	public Tblcorreorespaldo(String mail, Tblestablecimiento establecimiento) {
		this.mail = mail;
		this.establecimiento = establecimiento;
	}
	/**
	 * @return the idtblcorreo_respaldo
	 */
	public int getIdtblcorreo_respaldo() {
		return idtblcorreo_respaldo;
	}
	/**
	 * @param idtblcorreo_respaldo the idtblcorreo_respaldo to set
	 */
	public void setIdtblcorreo_respaldo(int idtblcorreo_respaldo) {
		this.idtblcorreo_respaldo = idtblcorreo_respaldo;
	}
	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return the establecimiento
	 */
	public Tblestablecimiento getEstablecimiento() {
		return establecimiento;
	}
	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(Tblestablecimiento establecimiento) {
		this.establecimiento = establecimiento;
	}

}
