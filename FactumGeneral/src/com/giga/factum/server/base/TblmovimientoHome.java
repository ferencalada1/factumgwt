package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.server.DAOGenerico;

/**
 * Home object for domain model class Tblmovimiento.
 * @see com.giga.factum.server.base.Tblmovimiento
 * @author Hibernate Tools
 */
public class TblmovimientoHome extends DAOGenerico<Tblmovimiento>{

	private static final Log log = LogFactory.getLog(TblmovimientoHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public void persist(Tblmovimiento transientInstance) {
		log.debug("persisting Tblmovimiento instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Tblmovimiento instance) {
		log.debug("attaching dirty Tblmovimiento instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Tblmovimiento instance) {
		log.debug("attaching clean Tblmovimiento instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Tblmovimiento persistentInstance) {
		log.debug("deleting Tblmovimiento instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Tblmovimiento merge(Tblmovimiento detachedInstance) {
		log.debug("merging Tblmovimiento instance");
		try {
			Tblmovimiento result = (Tblmovimiento) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Tblmovimiento findById(java.lang.Integer id) {
		log.debug("getting Tblmovimiento instance with id: " + id);
		try {
			Tblmovimiento instance = (Tblmovimiento) sessionFactory
					.getCurrentSession().get(
							"com.giga.factum.server.base.Tblmovimiento", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Tblmovimiento instance) {
		log.debug("finding Tblmovimiento instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.giga.factum.server.base.Tblmovimiento")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
