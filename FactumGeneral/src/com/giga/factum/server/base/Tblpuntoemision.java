package com.giga.factum.server.base;

import com.giga.factum.client.DTO.PuntoEmisionDTO;

public class Tblpuntoemision implements java.io.Serializable{
	
	private TblestablecimientoTblempresaTblpuntoemisionId id;
	private int idEmpresa;
	private String establecimiento;
	private String puntoemision;
	private String descripcion;
	private String nick;
	private char activo;
	
	
	public Tblpuntoemision() {

	}



	public Tblpuntoemision(int idEmpresa, String establecimiento, String puntoemision, String descripcion,
			String nick, char activo) {
		this.id=new TblestablecimientoTblempresaTblpuntoemisionId(idEmpresa
				,establecimiento,puntoemision);
		this.idEmpresa = idEmpresa;
		this.establecimiento = establecimiento;
		this.puntoemision = puntoemision;
		this.descripcion = descripcion;
		this.nick = nick;
		this.activo = activo;
	}

	public Tblpuntoemision(PuntoEmisionDTO puntoemision) {
		this.id=new TblestablecimientoTblempresaTblpuntoemisionId(puntoemision.getIdEmpresa()
				,puntoemision.getEstablecimiento(),puntoemision.getPuntoemision());
		this.idEmpresa=puntoemision.getIdEmpresa();
		this.establecimiento=puntoemision.getEstablecimiento();
		this.puntoemision=puntoemision.getPuntoemision();
		this.descripcion=puntoemision.getDescripcion();
		this.nick=puntoemision.getNick();
		this.activo=puntoemision.getActivo();
	}
	
	

	/**
	 * @return the idEmpresa
	 */
	public Integer getIdEmpresa() {
		return idEmpresa;
	}



	/**
	 * @param idEmpresa the idEmpresa to set
	 */
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}



	/**
	 * @return the establecimiento
	 */
	public String getEstablecimiento() {
		return establecimiento;
	}



	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}



	/**
	 * @return the puntoemision
	 */
	public String getPuntoemision() {
		return puntoemision;
	}



	/**
	 * @param puntoemision the puntoemision to set
	 */
	public void setPuntoemision(String puntoemision) {
		this.puntoemision = puntoemision;
	}



	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	/**
	 * @return the correo
	 */
	public String getNick() {
		return nick;
	}



	/**
	 * @param correo the correo to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}



	/**
	 * @return the id
	 */
	public TblestablecimientoTblempresaTblpuntoemisionId getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(TblestablecimientoTblempresaTblpuntoemisionId id) {
		this.id = id;
	}



	/**
	 * @return the activo
	 */
	public char getActivo() {
		return activo;
	}



	/**
	 * @param activo the activo to set
	 */
	public void setActivo(char activo) {
		this.activo = activo;
	}
	
	
}
