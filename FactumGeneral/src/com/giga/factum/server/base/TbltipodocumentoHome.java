package com.giga.factum.server.base;

// Generated 26/09/2012 06:34:20 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import com.giga.factum.server.DAOGenerico;

/**
 * Home object for domain model class Tbltipodocumento.
 * @see com.base.dos.server.Tbltipodocumento
 * @author Hibernate Tools
 */
public class TbltipodocumentoHome extends DAOGenerico<Tbltipodocumento>{}
/*
	private static final Log log = LogFactory
			.getLog(TbltipodocumentoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Tbltipodocumento transientInstance) {
		log.debug("persisting Tbltipodocumento instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Tbltipodocumento instance) {
		log.debug("attaching dirty Tbltipodocumento instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Tbltipodocumento instance) {
		log.debug("attaching clean Tbltipodocumento instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Tbltipodocumento persistentInstance) {
		log.debug("deleting Tbltipodocumento instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Tbltipodocumento merge(Tbltipodocumento detachedInstance) {
		log.debug("merging Tbltipodocumento instance");
		try {
			Tbltipodocumento result = (Tbltipodocumento) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Tbltipodocumento findById(java.lang.Integer id) {
		log.debug("getting Tbltipodocumento instance with id: " + id);
		try {
			Tbltipodocumento instance = (Tbltipodocumento) sessionFactory
					.getCurrentSession().get(
							"com.base.dos.server.Tbltipodocumento", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Tbltipodocumento instance) {
		log.debug("finding Tbltipodocumento instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.base.dos.server.Tbltipodocumento")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
*/