package com.giga.factum.server.base;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.giga.factum.server.DAOGenerico;
import com.giga.factum.server.ErrorAlGrabar;
import com.giga.factum.server.HibernateUtil;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1


/**
 * Home object for domain model class Tblcuenta.
 * @see com.giga.factum.server.base.Tblcuenta
 * @author Hibernate Tools
 */
public class TblcuentaHome extends DAOGenerico<Tblcuenta>{
	private Session session;
//Cambiar por consulta directa
	public void modificarCuentaNo(Tblcuenta t) throws ErrorAlGrabar {
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx=session.beginTransaction();
			//getHibernateTemplate().update(t);
			Tblcuenta cuenta=(Tblcuenta) session.load(Tblcuenta.class, t.getIdCuenta());
			if(!cuenta.getCodigo().equals(t.getCodigo())) cuenta.setCodigo(t.getCodigo());
			if(!cuenta.getNombreCuenta().equals(t.getNombreCuenta())) cuenta.setNombreCuenta(t.getNombreCuenta());
			if(cuenta.getNivel()!=t.getNivel()) cuenta.setNivel(t.getNivel());
			if(cuenta.getPadre()!=t.getPadre()) cuenta.setPadre(t.getPadre());;
			if(cuenta.getTbltipocuenta().getIdTipoCuenta().intValue()!=  t.getTbltipocuenta().getIdTipoCuenta().intValue()){
				Tbltipocuenta tc=(Tbltipocuenta) session.load(Tbltipocuenta.class, t.getTbltipocuenta().getIdTipoCuenta());
				cuenta.setTbltipocuenta(tc);
			}
			session.update(cuenta);
			tx.commit();
			session.close();
	 		//session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		e.printStackTrace(System.out);
	 		System.out.println(e.getMessage());
	 		System.out.println(e.getCause());
	 		throw new ErrorAlGrabar(e.getMessage()+"error de modific");
		}finally{
			session.close();
		}
	}
public void modificarCuenta(Tblcuenta t) throws ErrorAlGrabar {
	try {
		
		
		
		session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		
		Query query = session.createQuery("update Tblcuenta set codigo = :codigo, nombreCuenta = :nombreCuenta, nivel = :nivel, padre = :padre, idTipoCuenta = :idTipoCuenta" +
				" where idCuenta = :idCuenta");
		query.setParameter("codigo", t.getCodigo());
		query.setParameter("nombreCuenta", t.getNombreCuenta());
		query.setParameter("nivel", t.getNivel());
		query.setParameter("padre", t.getPadre());
		query.setParameter("idTipoCuenta", t.getTbltipocuenta().getIdTipoCuenta());
		
		query.setParameter("idCuenta", t.getIdCuenta());
		
		System.out.println(query.getQueryString());
		int result = query.executeUpdate();
		
		//getHibernateTemplate().update(t);
		/*Tblcuenta cuenta=(Tblcuenta) session.load(Tblcuenta.class, t.getIdCuenta());
		if(!cuenta.getCodigo().equals(t.getCodigo())) cuenta.setCodigo(t.getCodigo());
		if(!cuenta.getNombreCuenta().equals(t.getNombreCuenta())) cuenta.setNombreCuenta(t.getNombreCuenta());
		if(cuenta.getNivel()!=t.getNivel()) cuenta.setNivel(t.getNivel());
		if(cuenta.getPadre()!=t.getPadre()) cuenta.setPadre(t.getPadre());;
		if(cuenta.getTbltipocuenta().getIdTipoCuenta().intValue()!=  t.getTbltipocuenta().getIdTipoCuenta().intValue()){
			Tbltipocuenta tc=(Tbltipocuenta) session.load(Tbltipocuenta.class, t.getTbltipocuenta().getIdTipoCuenta());
			cuenta.setTbltipocuenta(tc);
		}
		session.update(cuenta);*/
		System.out.println("Actualizando cuenta da: "+result);
		tx.commit();
		
		//session.getTransaction().commit();
	} catch (HibernateException e) {
		session.getTransaction().rollback();
		e.printStackTrace(System.out);
		System.out.println(e.getMessage());
		System.out.println(e.getCause());
		throw new ErrorAlGrabar(e.getMessage()+"error de modific");
	}finally{
		session.close();
	}
}
}
