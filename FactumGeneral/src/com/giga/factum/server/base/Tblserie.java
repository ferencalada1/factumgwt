package com.giga.factum.server.base;

import java.util.Date;

public class Tblserie implements java.io.Serializable {

	private Integer idSerie;
	private Tbldtocomercial tbldtocomercialByIdDtoComercialV;
	private Tblproducto tblproducto;
	private Tbldtocomercial tbldtocomercialByIdDtoComercialC;
	private String serie;
	private Date fechaExpiracion;

	public Tblserie() {
	}

	public Tblserie(Tblproducto tblproducto, String serie) {
		this.tblproducto = tblproducto;
		this.serie = serie;
	}

	public Tblserie(Tbldtocomercial tbldtocomercialByIdDtoComercialV,
			Tblproducto tblproducto,
			Tbldtocomercial tbldtocomercialByIdDtoComercialC, String serie) {
		this.tbldtocomercialByIdDtoComercialV = tbldtocomercialByIdDtoComercialV;
		this.tblproducto = tblproducto;
		this.tbldtocomercialByIdDtoComercialC = tbldtocomercialByIdDtoComercialC;
		this.serie = serie;
	}
	
	public Tblserie(Tbldtocomercial tbldtocomercialByIdDtoComercialV,
			Tblproducto tblproducto,
			Tbldtocomercial tbldtocomercialByIdDtoComercialC, String serie, Date fecha) {
		this.tbldtocomercialByIdDtoComercialV = tbldtocomercialByIdDtoComercialV;
		this.tblproducto = tblproducto;
		this.tbldtocomercialByIdDtoComercialC = tbldtocomercialByIdDtoComercialC;
		this.serie = serie;
		this.setFechaExpiracion(fecha);
	}

	public Integer getIdSerie() {
		return this.idSerie;
	}

	public void setIdSerie(Integer idSerie) {
		this.idSerie = idSerie;
	}

	public Tbldtocomercial getTbldtocomercialByIdDtoComercialV() {
		return this.tbldtocomercialByIdDtoComercialV;
	}

	public void setTbldtocomercialByIdDtoComercialV(
			Tbldtocomercial tbldtocomercialByIdDtoComercialV) {
		this.tbldtocomercialByIdDtoComercialV = tbldtocomercialByIdDtoComercialV;
	}

	public Tblproducto getTblproducto() {
		return this.tblproducto;
	}

	public void setTblproducto(Tblproducto tblproducto) {
		this.tblproducto = tblproducto;
	}

	public Tbldtocomercial getTbldtocomercialByIdDtoComercialC() {
		return this.tbldtocomercialByIdDtoComercialC;
	}

	public void setTbldtocomercialByIdDtoComercialC(
			Tbldtocomercial tbldtocomercialByIdDtoComercialC) {
		this.tbldtocomercialByIdDtoComercialC = tbldtocomercialByIdDtoComercialC;
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Date getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

}
