package com.giga.factum.server.base;

// Generated 02-ago-2011 16:46:36 by Hibernate Tools 3.4.0.CR1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.giga.factum.client.DTO.PlanCuentaDTO;

/**
 * Tblplancuenta generated by hbm2java
 */
public class Tblplancuenta implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idPlan;
	private Tblempresa tblempresa;
	private Date fechaCreacion;
	private String periodo;
	//private Set tblcuentaPlancuentas = new HashSet(0);
	private Set<Tblcuenta> tblcuentas = new HashSet<Tblcuenta>(0);
	
	public Tblplancuenta() {
	}

	public Tblplancuenta(PlanCuentaDTO plan) {
		Date f=new Date();
		f.parse(plan.getFechaCreacion());
		this.fechaCreacion = f;
		this.tblempresa = new Tblempresa(plan.getTblempresa());
		this.periodo = plan.getPeriodo();
		
		
	}
	public Tblplancuenta(Tblempresa tblempresa, Date fechaCreacion,
			String periodo) {
		this.tblempresa = tblempresa;
		this.fechaCreacion = fechaCreacion;
		this.periodo = periodo;
	}

	public Tblplancuenta(Tblempresa tblempresa, Date fechaCreacion,
			String periodo, Set<Tblcuenta> tblcuentaPlancuentas) {
		this.tblempresa = tblempresa;
		this.fechaCreacion = fechaCreacion;
		this.periodo = periodo;
		this.tblcuentas = tblcuentaPlancuentas;
	}

	public Integer getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

	public Tblempresa getTblempresa() {
		return this.tblempresa;
	}

	public void setTblempresa(Tblempresa tblempresa) {
		this.tblempresa = tblempresa;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Set<Tblcuenta> getTblcuentas() {
		return this.tblcuentas;
	}

	public void setTblcuentas(Set<Tblcuenta> tblcuentaPlancuentas) {
		this.tblcuentas = tblcuentaPlancuentas;
	}
	
	/*public Set getTblcuentaPlancuentas() {
		return this.tblcuentaPlancuentas;
	}

	public void setTblcuentaPlancuentas(Set tblcuentaPlancuentas) {
		this.tblcuentaPlancuentas = tblcuentaPlancuentas;
	}*/

}
