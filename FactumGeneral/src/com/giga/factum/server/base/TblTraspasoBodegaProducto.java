package com.giga.factum.server.base;

import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;



public class TblTraspasoBodegaProducto {
	private TblproductoTblbodega productoBodega;
	private int idBodega;
	
	public TblTraspasoBodegaProducto(){
		
	}
	public TblTraspasoBodegaProducto(TraspasoBodegaProductoDTO dto){
		this.setProductoBodega(new TblproductoTblbodega(dto.getProductoBodega()));
		this.idBodega=dto.getIdBodega();
	}
	
	public void setIdBodega(int idBodega) {
		this.idBodega = idBodega;
	}

	public int getIdBodega() {
		return idBodega;
	}
	public void setProductoBodega(TblproductoTblbodega productoBodega) {
		this.productoBodega = productoBodega;
	}
	public TblproductoTblbodega getProductoBodega() {
		return productoBodega;
	}

}
