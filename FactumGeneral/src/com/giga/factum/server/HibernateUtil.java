package com.giga.factum.server;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StringType;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;
    static {
        try {
//            sessionFactory = new AnnotationConfiguration().configure("hibernate.cfg.xml").buildSessionFactory();
        	AnnotationConfiguration conf=new AnnotationConfiguration().configure("hibernate.cfg.xml");
        	conf.addSqlFunction("group_concat", new StandardSQLFunction("group_concat", new StringType()));
            sessionFactory =  conf.buildSessionFactory();
            //sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
