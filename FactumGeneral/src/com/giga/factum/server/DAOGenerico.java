package com.giga.factum.server;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Example;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.HibernateUtil;
import com.giga.factum.server.base.TblcuentaPlancuenta;
import com.giga.factum.server.base.Tbldtocomercial;
import com.giga.factum.server.base.TbldtocomercialHome;
import com.giga.factum.server.base.TbldtocomercialTbltipopago;
import com.giga.factum.server.base.TbldtocomercialTbltipopagoHome;
import com.giga.factum.server.base.TbldtocomercialTbltipopagoId;
import com.giga.factum.server.base.Tblmovimiento;
import com.giga.factum.server.base.TblmovimientoHome;
import com.giga.factum.server.base.TblmovimientoTblcuentaTblplancuenta;
import com.giga.factum.server.base.TblmovimientoTblcuentaTblplancuentaId;
import com.giga.factum.server.base.Tblpago;
import com.giga.factum.server.base.Tblproducto;
import com.giga.factum.server.base.TblproductoTblbodega;
import com.giga.factum.server.base.Tblretencion;
import com.giga.factum.server.base.Tblservicio;
import com.giga.factum.server.base.Tbltipodocumento;
import com.giga.factum.server.base.Tbltipopago;



public abstract class DAOGenerico<T> {
	 
	public Class<T> domainClass = getDomainClass();
 
	private Session session;
 	/**
	  * Method to return the class of the domain object
	  */
 
		protected Class getDomainClass() {
	 	if (domainClass == null) {
			ParameterizedType thisType = (ParameterizedType) getClass()
 				.getGenericSuperclass();
			domainClass = (Class) thisType.getActualTypeArguments()[0];
	 	}
		return domainClass;
	}
 
	public String eliminarID(String id){
		T obj=load(Integer.parseInt(id));
		try {
			eliminacionFisica(obj);
			return "Objeto eliminado";
		} catch (ErrorAlGrabar e) {
			// TODO Auto-generated catch block
			return e.getMessage();
		}
		
		
	}
	@SuppressWarnings("unchecked")
	public T load(Integer id) {
		session = HibernateUtil.getSessionFactory().openSession();
		T returnValue = (T) session.load(domainClass, id);
		//session.getTransaction().commit();
		session.close();
		return returnValue;
	}
	
	public void modificar(T t) throws ErrorAlGrabar {
		try {
			getHibernateTemplate().update(t);
	 		session.getTransaction().commit();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		e.printStackTrace(System.out);
	 		System.out.println(e.getMessage());
	 		System.out.println(e.getCause());
	 		throw new ErrorAlGrabar(e.getMessage()+"error de modific");
		}finally{
			session.close();
		}
	}
	public Tbltipodocumento findByNombreT(String nombre,String campo) {
		//session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("en findbynombret");
		Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipodocumento u where u."+campo+" = '"+nombre+"'");
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		System.out.println("First result 0 en findbynombret");
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		System.out.println("First result 1 en findbynombret");
		List<Tbltipodocumento> returnList = consulta.list();
		System.out.println("Return list");
		//session.close();
		return returnList.get(0);
	}
	
	public Iterator ListJoinOrdenID(Integer id) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tblequipo join u.tblorden where u.tblorden.idorden="+id);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	public TblcuentaPlancuenta findByIdPlanIdCuenta2(String idplan,String idcuenta) {
		//session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from com.giga.factum.server.base.TblcuentaPlancuenta u where " +
				"u.tblplancuenta.idPlan = " +"'"+idplan+"' and u.tblcuenta.idCuenta='"+idcuenta+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<TblcuentaPlancuenta> returnList = consulta.list();
		//session.close();
	//	System.out.println("sise de la lista del query: "+returnList.size()); 
		return returnList.get(0);
	}
	
	//FUNCION PARA ELIMINAR UN DOCUMENTO SIN HABER SIDO CONFIRMADO
	public String eliminarDocumento(Tbldtocomercial dto){
		String mensaje = "";
		try{
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
			String hqlDelete = "delete TbldtocomercialTbltipopago as c where c.id.idDtoComercial = :idDto";
			System.out.println("Query eliminar: "+ hqlDelete +" iddto "+dto.getIdDtoComercial());
			// or String hqlDelete = "delete Customer where name = :oldName";
			int resQuery=session.createQuery( hqlDelete )
			        .setString( "idDto", dto.getIdDtoComercial()+"" )
			        .executeUpdate();
			System.out.println("Eliminado de pagos: "+ dto.getIdDtoComercial() +" r: "+resQuery);
			
			TblmovimientoHome movacces=new TblmovimientoHome();
			Tblmovimiento mov=movacces.findByCampo(dto.getIdDtoComercial().toString(), "tbldtocomercial.idDtoComercial");
			
			if (mov!=null){
			String hqlDeleteMov = "delete TblmovimientoTblcuentaTblplancuenta as c where c.id.idMovimiento = :idMov";
			System.out.println("Query eliminar: "+ hqlDeleteMov +" idMov "+mov.getIdMovimiento());
			// or String hqlDelete = "delete Customer where name = :oldName";
			int resQueryMov=session.createQuery( hqlDeleteMov )
			        .setString( "idMov", mov.getIdMovimiento()+"" )
			        .executeUpdate();
			System.out.println("Eliminado de movimientocuenta: "+ mov.getIdMovimiento() +" r: "+resQueryMov);
			
			Tblmovimiento movOld = new Tblmovimiento();
			movOld = (Tblmovimiento) session.load(Tblmovimiento.class, mov.getIdMovimiento());//load(integer);
			System.out.println("Mov a ELIMINAR: "+ mov.getIdMovimiento() );
			session.delete(movOld);
			//session.getTransaction().commit();
			}
			Tbldtocomercial docOld = new Tbldtocomercial();
			docOld = (Tbldtocomercial) session.load(domainClass, dto.getIdDtoComercial());//load(integer);
			System.out.println("Doc a ELIMINAR: "+ dto.getIdDtoComercial() );
			System.out.println("Doc a ELIMINAR ss: "+  dto.getIdDtoComercial()+" y tb "+docOld.getNumRealTransaccion()+" estado "+docOld.getEstado() );
			
			session.delete(docOld);
			session.getTransaction().commit();
			if(session != null)
				session.close();
			
			mensaje="Documento eliminado con exito";
		}catch(Exception e)
		{
			e.printStackTrace(System.out);
			mensaje="Error durante la eliminacion.";
			System.out.println(e.getMessage());
		}
		
		return mensaje;
	}
	
	//FUNCION PARA ELIMINAR RETENCIONES ANTES DE CONFIRMAR
		public String eliminarRetenciones(Integer iddtocomercial){
			String mensaje = "";
			try{
				session = HibernateUtil.getSessionFactory().openSession();
		 		session.beginTransaction();
				String hqlDelete = "delete Tblretencion as c where c.tbldtocomercial.idDtoComercial = :idDto";
				// or String hqlDelete = "delete Customer where name = :oldName";
				session.createQuery( hqlDelete )
				        .setString( "idDto", iddtocomercial+"" )
				        .executeUpdate();
				
//				Tbldtocomercial docOld = new Tbldtocomercial();
//				docOld = (Tbldtocomercial) session.load(domainClass, dto.getIdDtoComercial());//load(integer);
//				//System.out.println("Doc a ELIMINAR: "+ dto.getIdDtoComercial() );
//				//system.out.println("Doc a ELIMINAR ss: "+  dto.getIdDtoComercial()+"y tb"+docOld.getNumRealTransaccion()+"estado"+docOld.getEstado() );
//				
//				session.delete(docOld);
				session.getTransaction().commit();
				if(session != null)
					session.close();
				
				mensaje="Documento eliminado con exito";
			}catch(Exception e)
			{
				mensaje="Error durante la eliminacion.";
			}
			
			return mensaje;
		}
	
	
	public Integer grabarConf(Tbldtocomercial t, Integer integer, Set<Tblproducto> productos,Set<TblproductoTblbodega> prodBods,
			Set<TbldtocomercialTbltipopago> pagosAsociados, Integer ind, Double iva, Integer anular, Integer cuentaGastoIngreso) throws ErrorAlGrabar {
		Iterator iterProductos;
		Iterator iterProductoBods;
		Iterator iterPagos;
		Tbldtocomercial ndto = new Tbldtocomercial();
		Tbldtocomercial Anticipodto = new Tbldtocomercial();
		Integer id=0;
		System.out.println("Entro a grabarConfInteg "+t.getIdDtoComercial()+" "+integer);
		try {
			//System.out.println("ID: "+integer);
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		id = integer;
	 		ndto = t;
	 		//eliminarRetenciones(t.getIdDtoComercial());
	 		
			/*session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();*/
//	 		String hqlDeleteDtoRet = "delete Tbldtocomercial as d where d.idDtoComercial in "
//					+ "(select c.tbldtocomercialByIdDtoComercial.idDtoComercial from "
//					+ "Tblretencion as c where c.tbldtocomercialByIdFactura.idDtoComercial = :idDto)";
	 		
	 		
			
	 		String hqlDelete;

			iterProductos = productos.iterator();
			System.out.println("productos iterator");
			iterProductoBods = prodBods.iterator();
			System.out.println("productos bodega iterator");
			iterPagos = pagosAsociados.iterator();
			System.out.println("pagos asociados iterator");
			Tblproducto producto;
			TbldtocomercialTbltipopago pago;
			TblproductoTblbodega productoB;
			//While para recorrer los productos 
			if(t.getTipoTransaccion() != 7 && t.getTipoTransaccion()!=4&& t.getTipoTransaccion()!=12)
			{
				Integer idProdGasto=null;
				while (iterProductos.hasNext()) {
					producto = (Tblproducto) iterProductos.next();
					if(!producto.getCodigoBarras().equals("PRODGASTOS")) session.update(producto);
					else idProdGasto=producto.getIdProducto();
				}
				//While para recorrer los items ProductoBodega
				while (iterProductoBods.hasNext()) {
					productoB = (TblproductoTblbodega) iterProductoBods.next();
					if(idProdGasto!=null)
						if (!(productoB.getTblproducto().getIdProducto().intValue()==idProdGasto.intValue())) session.update(productoB);
				}
			}
			System.out.println("Anular: "+ anular+" real "+t.getNumRealTransaccion());
			//VAMOS A EXTRAER LAS RETENCIONES PARA GRABARLAS EXTERIORMENTE 
			/*Set<Tblretencion> listaRetenciones = new HashSet<Tblretencion>(0);
			listaRetenciones = ndto.getTblretencions();
			ndto.setTblretencions(null);*/
			Integer num=null;
			Set<Tblretencion> retenciones = ndto.getTblretencionsForIdFactura();
			if (t.getTblretencionsForIdFactura() != null)
			{
				if (t.getTblretencionsForIdFactura().size()>0){
					String hqlDeleteDtoRet = "Select d.idDtoComercial from Tbldtocomercial as d where d.idDtoComercial in "
							+ "(select c.tbldtocomercialByIdDtoComercial.idDtoComercial from "
							+ "Tblretencion as c where c.tbldtocomercialByIdFactura.idDtoComercial = :idDto)";
					System.out.println(hqlDeleteDtoRet+(" "+id+""));
					Object resultadoDtoRet=session.createQuery( hqlDeleteDtoRet )
							.setString( "idDto", id+"" )
							.uniqueResult();
					
					if (resultadoDtoRet!=null){ num=Integer.parseInt(String.valueOf(resultadoDtoRet));
					System.out.println(hqlDeleteDtoRet+(" "+num+""));}

					hqlDelete = "delete Tblretencion as c where c.tbldtocomercialByIdFactura.idDtoComercial = :idDto";
					// or String hqlDelete = "delete Customer where name = :oldName";
					System.out.println(hqlDelete+(" "+id+""));
					int resultado=session.createQuery( hqlDelete )
							.setString( "idDto", id+"" )
							.executeUpdate();
					System.out.println(hqlDelete+(" "+resultado+" "));

//					if (id.intValue()!=num){
//						hqlDelete = "delete Tbldtocomercial as c where c.idDtoComercial = :idDto";
//						// or String hqlDelete = "delete Customer where name = :oldName";
//						System.out.println(hqlDelete+(" "+num+""));
//						resultado=session.createQuery( hqlDelete )
//								.setString( "idDto", num+"" )
//								.executeUpdate();
//						System.out.println(hqlDelete+(" "+resultado+" "));
//					}

				}
			}
			System.out.println("After get retenciones: "+ retenciones.size());
	 		ndto.setTblretencionsForIdFactura(null);
	 		System.out.println("Retenciones de dto null: ");
			if(anular>=0)
			{
				System.out.println("Anular mayor o igual: "+ anular);
				//Vamos a proceder a eliminar el documento confirmado eliminando primero los elementos asociados		
				hqlDelete = "delete TbldtocomercialTbltipopago as c where c.id.idDtoComercial = :idDto";
				System.out.println("After hqlDelete 1: ");
				// or String hqlDelete = "delete Customer where name = :oldName";
				session.createQuery( hqlDelete )
				        .setString( "idDto", integer+"" )
				        .executeUpdate();
				System.out.println("hqlDelete: "+ hqlDelete+" :"+integer);
				
				String hqlDeleteDetMult = "delete TbldtocomercialdetalleTblmultiImpuesto as c where c.tbldtocomercialdetalle.idDtoComercialDetalle in (select d.idDtoComercialDetalle from Tbldtocomercialdetalle as d where d.tbldtocomercial.idDtoComercial= :idDto)";
				// or String hqlDelete = "delete Customer where name = :oldName";
				System.out.println("After hqlDelete 1.9: ");
				session.createQuery( hqlDeleteDetMult )
				        .setString( "idDto", integer+"" )
				        .executeUpdate();
				System.out.println("hqlDelete: "+ hqlDeleteDetMult+" :"+integer);
				
				String hqlDeleteDet = "delete Tbldtocomercialdetalle as c where c.tbldtocomercial.idDtoComercial = :idDto";
				// or String hqlDelete = "delete Customer where name = :oldName";
				System.out.println("After hqlDelete 2: ");
				session.createQuery( hqlDeleteDet )
				        .setString( "idDto", integer+"" )
				        .executeUpdate();
				System.out.println("hqlDelete: "+ hqlDeleteDet+" :"+integer);
				
				String hqlDeletePag = "delete Tblpago as c where c.tbldtocomercial.idDtoComercial = :idDto";
				System.out.println("After hqlDelete 3: ");
				// or String hqlDelete = "delete Customer where name = :oldName";
				session.createQuery( hqlDeletePag )
				        .setString( "idDto", integer+"" )
				        .executeUpdate();
				System.out.println("hqlDelete: "+ hqlDeletePag+" :"+integer);
				
				Tbldtocomercial docOld = new Tbldtocomercial();
				docOld = (Tbldtocomercial) session.load(domainClass, integer);//load(integer);
				System.out.println("Doc a ELIMINAR: "+ integer );
				//system.out.println("Doc a ELIMINAR pp: "+ integer+"numero real de transaccion"+docOld.getNumRealTransaccion()+"estado"+docOld.getEstado() );
				//anular=docOld.getNumRealTransaccion();
				
	
					// session.delete(docOld);
	//				---------------------------------------------------------------       ndto = (Tbldtocomercial) t;
					System.out.println("ndto reciencreado: " + ndto.getIdDtoComercial());
					ndto.setIdDtoComercial(integer);
					System.out.println("ndto cambiado id: " + ndto.getIdDtoComercial());
					// id=(Integer) session.save(ndto);
					System.out.println("ndto after save: " + ndto.getIdDtoComercial());
	
					// ndto.setTbldtocomercialdetalles(null);
					// ndto.setTblretencions(null);
					// ndto.setTblpagos(null);
					
					ndto.setIdDtoComercial(integer);
					session.update(ndto);
	
					id = integer;
					System.out.println("ndto after save: " + ndto.getIdDtoComercial());
					// En caso de tener anticipos debemos cambiar su estado a 5 QUE
					// SIGNIFICA QUE YA SE HA USADO
					if (ndto.getIdAnticipo() != null) {
						System.out.println("dto numanticipo: " + ndto.getIdAnticipo());
						Anticipodto = (Tbldtocomercial) session.get("com.giga.factum.server.base.Tbldtocomercial",
								ndto.getIdAnticipo());
						System.out.println("dto anticipodto: " + Anticipodto.getIdDtoComercial());
						// Anticipodto.setIdDtoComercial();
						Anticipodto.setEstado('5');
						session.update(Anticipodto);
					}
			} else {
				System.out.println("En anular"); 
				System.out.println("NUM. DETALLES:"
						 +ndto.getTbldtocomercialdetalles().size());
				ndto.setTbldtocomercialdetalles(null);
				// ndto.setTblretencions(null);
				// ndto.setTblpagos(null);
				/*
				 * Iterator iir = ndto.getTblpagos().iterator(); Tblpago pagg =
				 * (Tblpago) iir.next(); system.out.println("Id del pago: -- "+
				 * pagg.getIdPago());
				 */
				ndto.setIdDtoComercial(integer);
				session.update(ndto);
				// ndto = (Tbldtocomercial) session.get(domainClass.getName(),
				// integer);//(Tbldtocomercial) acceso.findById(integer);
				// En caso de tener anticipos y como se va a anular, se debe
				// poner el estado del ANTICIPO a 1
				// para que pueda ser utilizado nuevamente
				if (ndto.getIdAnticipo() != null) {
					// Anticipodto.setIdDtoComercial(ndto.getIdAnticipo());
					Anticipodto = (Tbldtocomercial) session.get("com.giga.factum.server.base.Tbldtocomercial",
							ndto.getIdAnticipo());
					Anticipodto.setEstado('1');
					session.update(Anticipodto);
				}
			}
			ndto.setTblretencionsForIdFactura(retenciones);
			t.setTblretencionsForIdFactura(retenciones);
	 		if (ndto.getTipoTransaccion()==0 || ndto.getTipoTransaccion()==1){
		 		if (retenciones!=null){
		 			if (retenciones.size()>0){
		 				System.out.println("Retenciones en grabarp");
				 		Tbldtocomercial documentoRetencion= null;
				 		for (Tblretencion ret:retenciones){
				 			if (documentoRetencion==null && ret.getTbldtocomercialByIdDtoComercial()!=null){
				 				System.out.println("RRRRRRR ------------------- Cargando documento de retencion ");
				 				documentoRetencion=ret.getTbldtocomercialByIdDtoComercial();
//				 				int idDtoRet=(Integer)session.save(documentoRetencion);
				 				System.out.println("Despues cargar: "+(documentoRetencion.getIdDtoComercial()!=null));
//				 				if (documentoRetencion.getIdDtoComercial()!=null) session.update(documentoRetencion);
//				 				else 
				 				if (id.intValue()!=num && num!=null) session.update(documentoRetencion);
				 				else session.save(documentoRetencion);
				 				System.out.println("Despues actualizar");
//				 				documentoRetencion.setIdDtoComercial(idDtoRet);
				 			}else if (ret.getTbldtocomercialByIdDtoComercial()==null){
				 				documentoRetencion=new Tbldtocomercial();
				 				documentoRetencion.setIdDtoComercial(integer);
				 			}
				 			System.out.println("guardadndo retencion "+ret.getIdRetencion());
				 			ret.setTbldtocomercialByIdDtoComercial(documentoRetencion);
				 			ret.setTbldtocomercialByIdFactura(ndto);
//				 			int idr=(Integer)session.save(ret);
//				 			System.out.println("retencion guaradada "+idr);
//				 			if(ret.getIdRetencion()!=null) session.update(ret);
//				 			else 
				 				session.save(ret);
				 			System.out.println("retencion actualizada "+ret.getIdRetencion());
				 			//				 			retencionesC.add(ret);
				 		}
		 			}
		 		}
	 		}
			
			
			System.out.println("Despues de anular: ");
			Iterator iterDetalles;
			Tblpago pagoDTO = new Tblpago();
			System.out.println("Despues de anular: "+(t.getTblpagos() != null));
				if (t.getTblpagos() != null) {
					System.out.println("Si hay pagos en DaoGenerico: " + t.getTblpagos().size());
					Set<Tblpago> pagosDTO = t.getTblpagos();
					iterDetalles = pagosDTO.iterator();
					// documento.setTblpagos(new HashSet());
					int sigAnt=0;
					while (iterDetalles.hasNext()) {
						pagoDTO = (Tblpago) iterDetalles.next();
						if (pagoDTO.getEstado() == '2') {
							TbldtocomercialTbltipopagoHome homepagodto = new TbldtocomercialTbltipopagoHome();
							TbldtocomercialTbltipopago pagodto=homepagodto.buscarPago(pagoDTO.getIdPago());
							if (pagodto!=null){
								TbldtocomercialHome homedto = new TbldtocomercialHome();
	//							Tbldtocomercial dtocom = (Tbldtocomercial) homedto.buscarDtoPago(pagoDTO.getIdPago());
								Tbldtocomercial dtocom = homedto.findById(pagodto.getId().getIdDtoComercial());
							
								dtocom.setEstado('2');
								session.update(dtocom);
								
								//---------------------------------------Agregar comprobacion de que si es pago con retencion no crear anticipo
								if (pagodto.getTbltipopago().getIdTipoPago().intValue()!=3){
							
									Tbldtocomercial docToAnticipo = new Tbldtocomercial();
									
									String tipoT=(dtocom.getTipoTransaccion()==6)?"4":"15";
									if (sigAnt==0){
										Tbldtocomercial docBase = homedto.getLast("where TipoTransaccion = '"+tipoT+"' ", "numRealTransaccion");
										try{
											//									sigAnt=(sigAnt>0)?sigAnt+1:docBase.getNumRealTransaccion()+1;
											//									docToAnticipo.setNumRealTransaccion(sigAnt);
											sigAnt=docBase.getNumRealTransaccion()+1;
										}catch(Exception e){
											sigAnt=0;
										}
	
									}else{
										sigAnt=sigAnt+1;
									}
									docToAnticipo.setNumCompra("");
									docToAnticipo.setNumRealTransaccion(sigAnt);
									docToAnticipo.setTipoTransaccion(Integer.valueOf(tipoT));
									docToAnticipo.setTblpersonaByIdPersona(dtocom.getTblpersonaByIdPersona());
									docToAnticipo.setFecha(dtocom.getFecha());
									docToAnticipo.setExpiracion(dtocom.getExpiracion());
									docToAnticipo.setAutorizacion("Anticipo de pago anulado "+pagoDTO.getIdPago()+" documento tipo "+t.getTipoTransaccion()+" num "+t.getNumRealTransaccion());
									docToAnticipo.setNumPagos(0);
									docToAnticipo.setEstado('1');
									docToAnticipo.setSubtotal(dtocom.getSubtotal());
									docToAnticipo.setTblpersonaByIdVendedor(dtocom.getTblpersonaByIdVendedor());
									docToAnticipo.setCosto(0);
									docToAnticipo.setDescuento(0);
									docToAnticipo.setObservacion(dtocom.getObservacion());
									docToAnticipo.setTblpersonaByIdFacturaPor(dtocom.getTblpersonaByIdFacturaPor());
									docToAnticipo.setEstablecimiento(dtocom.getEstablecimiento());
									docToAnticipo.setIdEmpresa(dtocom.getIdEmpresa());
									docToAnticipo.setPuntoEmision(dtocom.getPuntoEmision());
									
									Integer iddtoAnt=(Integer)session.save(docToAnticipo);
									Set<TbldtocomercialTbltipopago> tipopagos=dtocom.getTbldtocomercialTbltipopagos();
	//								Set<TbldtocomercialTbltipopago> tipopagosSet= new HashSet<TbldtocomercialTbltipopago>(); 
									TbldtocomercialTbltipopago tipopago=new TbldtocomercialTbltipopago();
									TbldtocomercialTbltipopago tipopagoNuevo=new TbldtocomercialTbltipopago();
									Iterator iterPagos2=tipopagos.iterator();
									while (iterPagos2.hasNext()) {
										tipopago= (TbldtocomercialTbltipopago) iterPagos2.next();
										tipopagoNuevo.setTbltipopago(tipopago.getTbltipopago());
										tipopagoNuevo.setCantidad(tipopago.getCantidad());
										tipopagoNuevo.setObservaciones(tipopago.getObservaciones());
										tipopagoNuevo.setTipo(tipopago.getTipo());
										tipopagoNuevo.setReferenciaDto(0);
										tipopagoNuevo.setId(new TbldtocomercialTbltipopagoId(iddtoAnt,tipopago.getTbltipopago().getIdTipoPago()));
										session.save(tipopagoNuevo);
									}
								}
//								for (TbldtocomercialTbltipopago tp:tipopagos){
//									tipopago=new TbldtocomercialTbltipopago();
//									tipopago.setTbltipopago(tp.getTbltipopago());
//									tipopago.setId(new TbldtocomercialTbltipopagoId(iddtoAnt,tp.getTbltipopago().getIdTipoPago()));
//									
//									tipopagosSet.add(tipopago;
//								}
								
//								TbldtocomercialTbltipopago tipopago=new TbldtocomercialTbltipopago();
//								tipopago.set
//								docToAnticipo.setTbldtocomercialTbltipopagos();
							}
						}
					}
				}
			Integer planCuenta = 1;//ESTE VALOR DEBE SER EXTRAIDO DE LA VARIABLE DE SESION
			String concepto="";
			String anulacion="";
			if(anular<0)
				anulacion = " anulacion de ";
			//---------------------------	VAMOS A GRABAR LA CABECERA DE MOVIMIENTO ----------------------------
			switch(ndto.getTipoTransaccion()){
				case 0:{concepto="Por concepto de"+anulacion+" una venta con Factura ";break;}
				case 1:{concepto="Por concepto de"+anulacion+" una compra con Factura";break;}
				case 2:{concepto="Por concepto de"+anulacion+" una nota de entrega";break;}
				case 3:{concepto="Por concepto de"+anulacion+" una nota de credito";break;}
				case 4:{concepto="Por concepto de"+anulacion+" un anticipo";break;}
				case 5:{concepto="Por concepto de"+anulacion+" una retencion";break;}
				case 6:{concepto="Por concepto de"+anulacion+" un ajuste de inventario";break;}
				case 7:{concepto="Por concepto de"+anulacion+" un gasto";break;}
				case 12:{concepto="Por concepto de"+anulacion+" un ingreso";break;}
				case 13:{concepto="Por concepto de"+anulacion+" una venta con Factura Grande";break;}
			}
					
			//Aqui guardamos la cabecera del movimiento contable 
			Tblmovimiento movimiento;
			movimiento = new Tblmovimiento(ndto, concepto,ndto.getFecha());
			Integer idMov = (Integer) session.save(movimiento);
			
			//AQUI SE DEBE CARGAR EL PLAN DE CUENTAS QUE SE ESTE USANDO EN UNA VARIABLE DE SESION
			String tipoIva = "IVA COBRADO";
			char signo='0';
			if(ind == 0 ){//ENTRAN LAS CUENTAS AL HABER
				signo = 'h';
				if(anular<0)
					signo='d';
			}else{//ENTRA A LAS CUENTAS DEL DEBE
				signo = 'd';
				if(anular<0)
					signo = 'h';
				tipoIva = "IVA PAGADO";
			}
			
			//LISTADO DE LOS DETALLES DEL MOVIMIENTO CONTABLE
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimiento = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoHaber = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			Set<TblmovimientoTblcuentaTblplancuenta> detallesMovimientoDebe = new HashSet<TblmovimientoTblcuentaTblplancuenta>();
			
			TblmovimientoTblcuentaTblplancuentaId idDetalleMovimiento;
			TblmovimientoTblcuentaTblplancuenta detalleMovimiento;
			TblcuentaPlancuenta cuentaPlan;
			System.out.println("TIPO TRANS: "+ndto.getTipoTransaccion()); 
			Tbltipodocumento tipoDoc =  findByNombreT(ndto.getTipoTransaccion()+"", "idTipo");
				//(Tbltipodocumento) findByNombre(ndto.getTipoTransaccion()+"", "idTipo");
			System.out.println("Despues tipo trans...");
			Integer IdCuentaAsiento = tipoDoc.getIdCuenta();
			System.out.println("IdCuentaAsiento");
			if(ndto.getTipoTransaccion()==7 || ndto.getTipoTransaccion()==12)//En caso de ser un gasto o ingreso
				IdCuentaAsiento = cuentaGastoIngreso;
			System.out.println("Antes movimiento cuenta ");
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, IdCuentaAsiento); 		
			//VAMOS A EXTRAER EL OBJETO TBLCUENTAPLANCUENTA
			//T findByIdPlanIdCuenta(String idplan,String idcuenta)
			//system.out.println("plan cuenta: "+planCuenta+" y el id de cuenta"+IdCuentaAsiento);
			System.out.println("Antes cuenta plan ");
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
					IdCuentaAsiento+"");//idcuenta)
			//system.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+IdCuentaAsiento);
			System.out.println("Antes detalle movimiento ");
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, ndto.getSubtotal(), signo);
			//Agregamos el detalle del coumento al array
			System.out.println("Antes debe o haber ");
			if(signo=='h')
				detallesMovimientoHaber.add(detalleMovimiento);
			else
				detallesMovimientoDebe.add(detalleMovimiento);
			//detallesMovimiento.add(detalleMovimiento);
			
			
			//-------------------------------------------------------------------------------
			//CREAMOS EL DETALLE PARA EL IVA, SEA COBRADO O PAGADO
			System.out.println("Antes tipo iva ");
			tipoDoc =  findByNombreT(tipoIva, "tipoDoc");
			System.out.println("despues de tipo documento ");
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(),
					tipoDoc.getIdCuenta()+"");//idcuenta)
			//system.out.println("Plan de cuenta: "+planCuenta.toString()+" - "+"CTA: "+tipoDoc.getIdCuenta());
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tipoDoc.getIdCuenta());
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, iva, signo);
			
			if(signo=='h')
				detallesMovimientoHaber.add(detalleMovimiento);
			else
				detallesMovimientoDebe.add(detalleMovimiento);
			System.out.println("Movimiento detalle documento ");
			//detallesMovimiento.add(detalleMovimiento);
 		//Vamos a proceder a guardar las formas de pago
 		//While para recorrer los productos 
			//system.out.println("detalleMovimiento1 "+detalleMovimiento.getId().getIdMovimiento()+" paln "+detalleMovimiento.getId().getIdPlan()+" cuenta "+detalleMovimiento.getId().getIdCuenta()+" ");
			if(signo == 'h')
			{signo = 'd';}
			else if(signo == 'd')
			{signo = 'h';}
			System.out.println("Signos ");
			Integer tipoPago=0;
			while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				tipoPago = pago.getTbltipopago().getIdTipoPago();
				pago.setId(new TbldtocomercialTbltipopagoId(id,tipoPago));
				
				if(anular>=0)
					session.save(pago);
				
				//VAMOS EXTRER LA CUENTA Y EL PLAN DE CUENTA ASOCIADO AL PAGO

				//-------------------------------------------------------------------------------
				//CREAMOS EL DETALLE PARA EL IVA, SEA COBRADO O PAGADO
				//tipoDoc =  (Tbltipodocumento) findByNombre(tipoIva, "TipoDoc");
				//pago = 
				
				//if(fpago.getTbltipopago().getTipoPago().equals("1"))
				if(pago.getTipo()=='1')
				{//SE TRATA DE UNA COMPRA O INGRESO
					switch(tipoPago)
					{
					case 2 :{tipoPago=8;break;}
					case 4 :{tipoPago=9;break;}
					case 5 :{tipoPago=10;break;}
					}
				}
				Query consulta = session.createQuery("from com.giga.factum.server.base.Tbltipopago " +
						"u where u.idTipoPago ="+ "'"+tipoPago+"'");	
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tbltipopago> returnList = consulta.list();
			//session.close();
			Tbltipopago tipopago =  returnList.get(0);	
			//system.out.println("ID Cuenta: "+tipopago.getIdCuenta()+"");
			//system.out.println("PLAN Cuenta: "+planCuenta.toString());
			
			cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(planCuenta.toString(), 
					tipopago.getIdCuenta()+"");//idcuenta)	
			idDetalleMovimiento =new 
			TblmovimientoTblcuentaTblplancuentaId(idMov, planCuenta, tipopago.getIdCuenta());
			detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
					idDetalleMovimiento,
					cuentaPlan,
					movimiento, pago.getCantidad(), signo);
			if(signo=='h')
				detallesMovimientoHaber.add(detalleMovimiento);
			else
				detallesMovimientoDebe.add(detalleMovimiento);
			//detallesMovimiento.add(detalleMovimiento);
			//system.out.println("detalleMovimiento2 "+detalleMovimiento.getId().getIdMovimiento()+" paln "+detalleMovimiento.getId().getIdPlan()+" cuenta "+detalleMovimiento.getId().getIdCuenta()+" ");					
		}
			System.out.println("Despues de procesar movimientos de pagos Habler:"+detallesMovimientoHaber.size()+" debe:"+detallesMovimientoDebe.size());
		//AQUI VAMOS A PROCEDER A GUARDAR LOS DETALLES DE LA RETENCION
			if (t.getTblretencionsForIdFactura() != null)
			{
				
				Iterator iterRetenciones = t.getTblretencionsForIdFactura().iterator();
				while(iterRetenciones.hasNext()){
					Tblretencion retencion = (Tblretencion) iterRetenciones.next();

					cuentaPlan = (TblcuentaPlancuenta) findByIdPlanIdCuenta2(retencion.getTblimpuesto().getIdPlan()+"",
							retencion.getTblimpuesto().getIdCuenta()+"");
					idDetalleMovimiento =new 
							TblmovimientoTblcuentaTblplancuentaId(idMov, retencion.getTblimpuesto().getIdPlan(), 
									retencion.getTblimpuesto().getIdCuenta());
					Double cantidad = 0.0;
					double porcentaje = retencion.getTblimpuesto().getRetencion();
					double base = retencion.getBaseImponible();
					cantidad = (base*porcentaje)/100;
					detalleMovimiento = new TblmovimientoTblcuentaTblplancuenta(
							idDetalleMovimiento,
							cuentaPlan,
							movimiento, cantidad, signo);
					//detallesMovimiento.add(detalleMovimiento);

					if(signo=='h')
					{	
						//	listDetallesAuxHaber.add(detalleMovimiento);
						detallesMovimientoHaber.add(detalleMovimiento);
					}
					else
					{	
						//	listDetallesAuxDebe.add(detalleMovimiento);
						detallesMovimientoDebe.add(detalleMovimiento);
					}
					//VAMOS A PROCEDER A GRABAR LAS RETENCION 
					/*TblretencionHome ret = new TblretencionHome();

			ret.grabarList();*/
				}


			}
		else{
			System.out.println("Retenciones en nulo");
		}
			System.out.println("Despues de procesar movimientos de retenciones Habler:"+detallesMovimientoHaber.size()+" debe:"+detallesMovimientoDebe.size());
		Iterator detalles = detallesMovimientoHaber.iterator();
		detallesMovimiento = detallesMovimientoDebe;
		
		for (int k=0; k<detallesMovimientoHaber.size();k++)
		{
			detallesMovimiento.add((TblmovimientoTblcuentaTblplancuenta) detalles.next());
		}
		System.out.println("UN solo movimiento de detalle "+detallesMovimiento.size());
		//   Para arreglar lo de los impuestos Edu
		LinkedList<TblmovimientoTblcuentaTblplancuenta> listDetallesAuxMovimiento = new LinkedList<TblmovimientoTblcuentaTblplancuenta>();
	//   Para arreglar lo de los impuestos Edu
		LinkedList<TblmovimientoTblcuentaTblplancuenta> listDetallesAuxMovimientoTotal = new LinkedList<TblmovimientoTblcuentaTblplancuenta>();
		Iterator detallesTotales = detallesMovimiento.iterator();
		while (detallesTotales.hasNext())
		{
			TblmovimientoTblcuentaTblplancuenta aux = (TblmovimientoTblcuentaTblplancuenta)detallesTotales.next();
			//system.out.println("Inicial Mov "+aux.getId().getIdMovimiento()+" Plan "+aux.getId().getIdPlan()+" Cuenta "+aux.getId().getIdCuenta()+" Signo "+aux.getSigno()+" Valor "+aux.getValor());
			listDetallesAuxMovimiento.add(aux);
		}	
		System.out.println("listado detalles movimientos "+listDetallesAuxMovimiento.size());
		
		//+++++++   Pasamos a una lista ligada auxiliar todos los elementos   ++++++++++++++
		int repetido=0;
		
		for (int a=0; a<listDetallesAuxMovimiento.size()-1;a++)
		{
	//			LinkedList<Integer> listAuxRepetidos = new LinkedList<Integer>();
				Double valorRetenidoTotal=0.0;
				valorRetenidoTotal=listDetallesAuxMovimiento.get(a).getValor();
	//			listAuxRepetidos.clear();
		//		//system.out.println("Valor "+listDetallesAuxMovimiento.get(a).getValor()+" Mov "+listDetallesAuxMovimiento.get(a).getId().getIdMovimiento()+" Plan "+listDetallesAuxMovimiento.get(a).getId().getIdPlan()+" Cuenta "+listDetallesAuxMovimiento.get(a).getId().getIdCuenta());
				for (int b=a+1; b<listDetallesAuxMovimiento.size();b++)
				{	
					if(listDetallesAuxMovimiento.get(a).getId().equals(listDetallesAuxMovimiento.get(b).getId()))
					{
						valorRetenidoTotal=valorRetenidoTotal+listDetallesAuxMovimiento.get(b).getValor();
					//	listAuxRepetidos.add(b);
					}
				}
				//++pongo el valor total y le asigno al detalles movimiento haber ese primer elemento
				listDetallesAuxMovimiento.get(a).setValor(valorRetenidoTotal);
				if(listDetallesAuxMovimientoTotal.size()==0)
				{
					listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(a));
				}
				else
				{
					repetido=0;
					for(int d=0;d<listDetallesAuxMovimientoTotal.size();d++)
					{	
						if(listDetallesAuxMovimientoTotal.get(d).getId().equals(listDetallesAuxMovimiento.get(a).getId()))
						{
							repetido=1;
							break;
						}
					}
					if (repetido==0)
					{
						listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(a));
					}	
				}	
				//borramos todos los elementos que estan repetidos incluyendo la cabecera
	/*			System.out.println("tamanioAuxRep"+a+" "+listAuxRepetidos.size());
				for (int c=0; c<listAuxRepetidos.size();c++)
				{
					listDetallesAuxMovimiento.remove(listAuxRepetidos.get(c));
					
					System.out.println("elemento a eliminar +"+listAuxRepetidos.get(c)+" size "+listDetallesAuxMovimiento.size());
				}
				*/
			}//for principal
		
		System.out.println("listado detalles movimientos total"+listDetallesAuxMovimiento.size());
		
		repetido=0;
		for(int d=0;d<listDetallesAuxMovimientoTotal.size();d++)
		{	
			if(listDetallesAuxMovimientoTotal.get(d).getId().equals(listDetallesAuxMovimiento.get(listDetallesAuxMovimiento.size()-1).getId()))
			{
				repetido=1;
				//system.out.println("verso estoy repetido");
				break;
				
			}
		}
		if (repetido==0)
		{
			listDetallesAuxMovimientoTotal.add(listDetallesAuxMovimiento.get(listDetallesAuxMovimiento.size()-1));
			//system.out.println("no estoy repetido"+listDetallesAuxMovimiento.get(listDetallesAuxMovimiento.size()-1).getValor());
		}
		System.out.println("Despues repetido ");
		detallesMovimiento.clear();//borramos todos los elementos del detalles Movimiento
		for (int k=0; k<listDetallesAuxMovimientoTotal.size();k++)
		{
			detallesMovimiento.add(listDetallesAuxMovimientoTotal.get(k));
			//system.out.println("Final Mov "+listDetallesAuxMovimientoTotal.get(k).getId().getIdMovimiento()+" Plan "+listDetallesAuxMovimientoTotal.get(k).getId().getIdPlan()+" Cuenta "+listDetallesAuxMovimiento.get(k).getId().getIdCuenta()+" Signo "+listDetallesAuxMovimiento.get(k).getSigno()+" Valor "+listDetallesAuxMovimientoTotal.get(k).getValor());
		}
		
		System.out.println("Despues repetido "+detallesMovimiento.size());
		movimiento.setTblmovimientoTblcuentaTblplancuentas(detallesMovimiento);
		System.out.println("Set movimiento cuentas");
		//system.out.println("TAMAÑO MOVIMIENTO: "+detallesMovimiento.size());
		//system.out.println("ID DTO MOVIMIENTO: "+movimiento.getTbldtocomercial().getIdDtoComercial());
		session.update(movimiento);
		//AQUI VAMOS A PROCEDER A GRABAR LOS ASIENTOS CONTABLES 
		System.out.println("SE GRABO LOS MOVIMIENTOS: ");
		session.flush();
		session.clear();
 		session.getTransaction().commit();
 		//session.close();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		e.printStackTrace(System.out);
	 		throw new ErrorAlGrabar(e.getLocalizedMessage()+"---"+e.toString()+"---"+e.getCause());
	 	} finally {
	 		//if(session!=null)
	 		session.close();
	 	}
	 return id;
	}
	
	public T buscarPago(int idpago){
	
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.referenciaDto = " +idpago 
				+" and (u.tbldtocomercial.tipoTransaccion=6 or u.tbldtocomercial.tipoTransaccion=11)");
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		if(consulta.list().isEmpty()){
			//system.out.println("lista vacia");
			session.close();
			return null;
		}else{
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<T> returnList = consulta.list();
			session.close();
			return returnList.get(0);
		}
		//return dtocomercial;
	}
	
	public T buscarDtoPago(int idpago){
//		Tbldtocomercial dtocomercial= new Tbldtocomercial();
//		TbldtocomercialTbltipopagoHome dtocomercialtipoHome = new TbldtocomercialTbltipopagoHome();
//		TbldtocomercialHome dtoHome=new TbldtocomercialHome();
		
		session = HibernateUtil.getSessionFactory().openSession();
//		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.tbldtocomercialTbltipopagos.referenciaDto = " +idpago);
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercialTbltipopagos t where t.referenciaDto = " +idpago);
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		if(consulta.list().isEmpty()){
			//system.out.println("lista vacia");
			session.close();
			return null;
		}else{
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<T> returnList = consulta.list();
			session.close();
			return returnList.get(0);
		}
		//return dtocomercial;
	}
	
	

	public void modificar(List<T> t) {
		
		session = HibernateUtil.getSessionFactory().openSession();	
		session.getTransaction().begin();
		for(int i = 0; i<t.size();i++)
		{
			session.update(t.get(i));
			
		}	
		session.flush();
		session.clear();
		session.getTransaction().commit();
 
		session.close();
	}
	public List<T> DocumentoTipo(Integer tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.tipoTransaccion='"+tipo+"' and u.estado='1'" );//1 ESTADO CONFIRMADO
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	public List<T> DocumentoTipoPersona(Integer tipo, Integer persona) {
		session = HibernateUtil.getSessionFactory().openSession();
		//system.out.println("JOHNY ESTE ES ANTICIPO: from "+domainClass.getName()+" u where u.tipoTransaccion='"+tipo+"' and u.estado='1' and u.tblpersonaByIdPersona.idPersona='"+persona+"'");
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.tipoTransaccion='"+tipo+"' and u.estado='1' and u.tblpersonaByIdPersona.idPersona='"+persona+"'");//1 ESTADO CONFIRMADO
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}

	public void grabar(T t) throws ErrorAlGrabar {
	 	try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		session.save(t);
	 		session.getTransaction().commit(); 		
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		e.printStackTrace(System.out);
	 		System.out.print(e.getStackTrace());
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}
	public List<T> getList() {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x");
		//Le pasamos el registro desde el cual va a extraer
		//consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(NumReg);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
 
	public void eliminacionFisica(T t) throws ErrorAlGrabar {
		try{
	 	getHibernateTemplate().delete(t);
	 	session.getTransaction().commit();
		} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		session.close();
	 	}
	}
	
	public void eliminacionLogica(T t) {
	 	/*
	 	 * AQUI DEBEMOS CREAR UN QUERY PARA MODIFICAR EL ESTADO
	 	 * getHibernateTemplate().delete(t);
	 	session.getTransaction().commit();*/
	}
 
	@SuppressWarnings("unchecked")
	
 
	/*public void deleteById(Long id) {
	 	Object obj = this.load(id);
	 	getHibernateTemplate().delete(obj);
	}
 */
	public int deleteAll(boolean isSure) {
		int countDeleted = getHibernateTemplate().createQuery(
 			"delete " + domainClass.getName()).executeUpdate();
		if (isSure)
	 		session.getTransaction().commit();
	 	else
	 		session.getTransaction().rollback();
		return countDeleted;
	}
	/**
	 * Metodo retorna el total de la retención por factura
	 * @param idFactura
	 * @return Double
	 */
	public double RetencionPorFactura(String idFactura) {
		double retencion=0.0;
		session = HibernateUtil.getSessionFactory().openSession();
		//system.out.println("select sum(valorRetenido) from Tblretencion r where r.idFactura='"+idFactura+"'");
//		Object count =  session.createQuery(
// 			"select sum(valorRetenido) from Tblretencion r where r.idFactura='"+idFactura+"'").uniqueResult();
		
		Object count =  session.createQuery(
	 			"select sum(valorRetenido) from Tblretencion r where r.tbldtocomercialByIdFactura.idDtoComercial='"+idFactura+"'").uniqueResult();
		
		//session.getTransaction().commit();
		session.close();
		try{
			//system.out.println("VALORRETENCION= "+String.valueOf(count));
			//system.out.println("VALORRETENCIONDOUBLE= "+Double.parseDouble(String.valueOf(count)));
			retencion=Double.parseDouble(String.valueOf(count));
			return retencion;
		}catch(Exception e){
			retencion=0.0;
			return retencion;
		}		
	}
	/**
	 * Metodo retorna iddtocomercial correspondiente a una factura
	 * @param idRetencion
	 * @return DtoComercialDTO
	 */
	public int DTOComercialPorIdRetencion(Integer idRetencion) {
		int retencion=-1;
		session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("select r.idDtoComercial from "+domainClass.getName()+" r join r.tblretencionsForIdDtoComercial d "
					+ "where (r.tipoTransaccion='5' or r.tipoTransaccion='19') and d.idRetencion='"+idRetencion+"'");
		Object count =  session.createQuery(
	 			"select r.idDtoComercial from "+domainClass.getName()+" r join r.tblretencionsForIdDtoComercial d "
	 					+ "where (r.tipoTransaccion='5' or r.tipoTransaccion='19') and d.idRetencion='"+idRetencion+"'").uniqueResult();
//	 					+ "where d.idRetencion='"+idRetencion+"'").uniqueResult();
		
		//session.getTransaction().commit();
		session.close();
		try{
			//system.out.println("VALORRETENCION= "+String.valueOf(count));
			//system.out.println("VALORRETENCIONDOUBLE= "+Double.parseDouble(String.valueOf(count)));
			retencion=Integer.parseInt(String.valueOf(count));
			return retencion;
		}catch(Exception e){
			retencion=-1;
			return retencion;
		}		
	}
	
	public int countProd(String tabla) {
		session = HibernateUtil.getSessionFactory().openSession();
		Object count =  session.createQuery(
 			"select count(*) from " + domainClass.getName() + " x where estado='1'")
 			.uniqueResult();
		//session.getTransaction().commit();
		session.close();
		int num=Integer.parseInt(String.valueOf(count));
		return num;
	}
	
	public String actualizarIva() {
		String resultado="";
		session = HibernateUtil.getSessionFactory().openSession();
		try{
		String hqlUpdate = "update Tblproducto c set c.impuesto= :nuevoimpuesto where c.impuesto = :valor";
		// or String hqlDelete = "delete Customer where name = :oldName";
		session.createQuery( hqlUpdate )
		        .setString( "nuevoimpuesto", "14" )
		        .setString( "valor", "12" )
		        .executeUpdate();
		
		//session.getTransaction().commit();
		session.close();
		resultado="Actualizacion exitosa";
		}
		catch(Exception e)
		{
			resultado="Error: "+e.getMessage();	
		}
		return resultado;
	}
	
	/**
	 * Metodo para buscar en una tabla por un campo
	 * @param cedula nocedulambre a buscar
	 * @return entidad
	 */
	public T findByCedula(String cedula) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.cedulaRuc = " +"'"+cedula+"'");
		//Le pasamos el registro desde el cual va a extraer
		if(consulta.list().isEmpty()){
			//system.out.println("lista vacia");
			session.close();
			return null;
		}else{
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<T> returnList = consulta.list();
			session.close();
			return returnList.get(0);
		}
	} 
	
	public int count(String tabla) {
		session = HibernateUtil.getSessionFactory().openSession();
		Object count =  session.createQuery(
 			"select count(*) from " + domainClass.getName() + " x")
 			.uniqueResult();
		//session.getTransaction().commit();
		session.close();
		int num=Integer.parseInt(String.valueOf(count));
		return num;
	}
	
	public int countWithWhere(String tabla, String query) {
		session = HibernateUtil.getSessionFactory().openSession();
		Object count =  session.createQuery(
 			"select count(*) from " + domainClass.getName() + " u "+ query)
 			.uniqueResult();
		//session.getTransaction().commit();
		session.close();
		int num=Integer.parseInt(String.valueOf(count));
		return num;
	}
	
	 /**
	  * Método para contar los registros de la tabla personas con su respectivo join 
	  * @param tabla tabla para hacer el join
	  * @return int numero de registros
	  */
	 public int countPersona(String tabla) {
			session = HibernateUtil.getSessionFactory().openSession();
			Object count =  session.createQuery(
	 			"select count(*) from " + domainClass.getName()+" u join u."+tabla)
	 			.uniqueResult();
			//session.getTransaction().commit();
			session.close();
			int num=Integer.parseInt(String.valueOf(count));
			return num;
	 }
	@SuppressWarnings("unchecked")
		public T findById(Integer id) {
		T instance=null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			instance = (T) session.get(domainClass.getName(), id);
			if (instance == null) {
				//log.debug("get successful, no instance found");
			} else {
				//log.debug("get successful, instance found");
			}
			session.close();
			return instance;
		} catch (RuntimeException re) {
			//log.error("get failed", re);
			throw re;
		}
		
	}
	
	

	public List<T> findByQuery(String query) {
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" u "+ query);
			System.out.println(consulta);
			List<T> returnList =consulta.list();
			session.close();
			return returnList;
		} catch (RuntimeException re) {
			//log.error("get failed", re);
			throw re;
		}
		
	}
	
	public T findByQueryUnique(String query) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u "+query);
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		if(returnList.size()>0){
			return returnList.get(0);
		}else{
			return null;
		}
		
	}

	public List<T> findByQueryWithLenght(String query,int inicio, int NumReg) {
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" u "+ query);
			System.out.println(consulta);
			consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(NumReg);
			List<T> returnList =consulta.list();
			session.close();
			return returnList;
		} catch (RuntimeException re) {
			//log.error("get failed", re);
			throw re;
		}
		
	}
	/**
	 * Metodo para buscar en una la tabla Tipo Pago
	 * @param idPlan 
	 * @param idCuenta
	 * @return entidad Tbltipopago
	 */
	public T findByIdPlanIdCuenta(String idplan,String idcuenta) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.idPlan = " +"'"+idplan+"' and u.idCuenta='"+idcuenta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		return returnList.get(0);
	}
	/**
	 * Metodo para buscar en una tabla por un campo
	 * @param nombre nombre a buscar
	 * @param campo columna en la tabla
	 * @return entidad
	 */

	public T findByNombre(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" = " +"'"+nombre+"'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		return returnList.get(0);
	}
	
	//para buscar productos dados de baja
    public T findByNombreEliminado(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" = " +"'"+nombre+"' and estado='0'");
		 
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		return returnList.get(0);
	}
	
	
	
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public T findByNombre2(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" = " +"'"+nombre+"'");
		//Le pasamos el registro desde el cual va a extraer
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		return returnList.get(0);
	}

	/**
	 * Muestra las facturas Documentos comerciales de cierta fecha
	 * @param fechaI
	 * @return
	 */
	public List<T> CuadreCaja(String fechaI, String fechaF) {
		session = HibernateUtil.getSessionFactory().openSession();
		//System.out.println("from "+domainClass.getName()+" u where u.fecha='"+fechaI+"' and NOT u.tipoTransaccion='1'");
		//Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.fecha='"+fechaI+"' and NOT u.tipoTransaccion='1'"+
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.fecha>='"+fechaI+" 00:00:00' and  u.fecha<='"+fechaF+" 23:59:59' "+
				"and NOT u.estado='0'");
		System.out.println(consulta.getQueryString());
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Mètodo para generar un reporte de facturas
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param Tipo String Tipo de documento 
	 * @return Lista con de Tbldtocomercial
	 */
	public List<T> ReproteCaja(String fechaI,String fechaF,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
	//	System.out.println("from "+domainClass.getName()+" u where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' and TipoTransaccion='"+Tipo+"'" );
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' order by fecha" );
		System.out.println(consulta);	//"by u.tblpersonaByIdPersona.apellidos, u.tblpersonaByIdPersona.nombres, u.tblpersonaByIdPersona.cedulaRuc" );
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	//+++++++++  Para los reportes de un determinado cliente +++++++++++++++++++++++++
	public List<T> ReporteFacCliente(String fechaI,String fechaF,String Tipo, String ruc, String nombres, String razonSocial) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' "+
//				"and u.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%' and u.tblpersonaByIdPersona.nombres like '"+nombres+"%' "+
//				"and u.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' order by u.tblpersonaByIdPersona.razonSocial, u.tblpersonaByIdPersona.nombres, u.tblpersonaByIdPersona.cedulaRuc");
		"and u.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%' and u.tblpersonaByIdPersona.nombreComercial like '"+nombres+"%' "+
		"and u.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' order by u.tblpersonaByIdPersona.razonSocial, u.tblpersonaByIdPersona.nombreComercial, u.tblpersonaByIdPersona.cedulaRuc");
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Mètodo para buscar un pago mediante su id
	 * @param idpago
	 * @return Iterator de tblpago y tbldocumentoComercial
	 */
	public Iterator BuscarPago(String idpago){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial where u.idPago='"+idpago+"'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * Método para listar los pagos vencidos
	 * @param Tipo tipo de documento comercial 
	 * @return List<Tblpago>
	 */
	public Iterator ReprotePagosVencidos() {
		Calendar c2 = new GregorianCalendar();
        Integer.toString(c2.get(Calendar.DATE));
        String fechaactual=(String.valueOf(c2.get(Calendar.YEAR))+"-"+
                String.valueOf(c2.get(Calendar.MONTH)+1)+"-"+String.valueOf(c2.get(Calendar.DATE)));
        session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d  join d.tblpersonaByIdPersona p where u.estado='0' "+"and u.fechaVencimiento<='"+fechaactual+" 23:59:59' " +
				"and NOT d.tipoTransaccion='1' and NOT d.tipoTransaccion='2' and d.estado='1'"); 
		Iterator returnList = consulta.list().iterator();  
		session.close();
		return returnList;
	}
	
	public Float ValorDelInventario(){
		session = HibernateUtil.getSessionFactory().openSession();
        Object count =  session.createQuery("Select sum(stock*promedio) from "+domainClass.getName()+" p where p.stock>0 and p.estado='1'").uniqueResult();
		session.close();
		Float num=Float.parseFloat(String.valueOf(count));
		return num;
	}
	
	
	/**
	 * Método para listar los pagos vencidos por cliente
	 * @param idPersona id de la Persona  
	 * @return Iterator
	 */
	public Float ReprotePagosVencidosCliente(String idPersona) {
		Calendar c2 = new GregorianCalendar();
        Integer.toString(c2.get(Calendar.DATE));
        String fechaactual=(String.valueOf(c2.get(Calendar.YEAR))+"-"+
                String.valueOf(c2.get(Calendar.MONTH)+1)+"-"+String.valueOf(c2.get(Calendar.DATE)));
        session = HibernateUtil.getSessionFactory().openSession();
        Object count =  session.createQuery("Select sum(valor) from "+domainClass.getName()+" u where u.tbldtocomercial.tblpersonaByIdPersona.idPersona='"+idPersona+"' and u.estado='0' and u.tbldtocomercial.estado='1' " +
				"and u.fechaVencimiento<'"+fechaactual+" 23:59:59'")
	 			.uniqueResult();
		//session.getTransaction().commit();
		session.close();
		Float num=Float.parseFloat(String.valueOf(count));
		return num;
    } 
	
	/**
	 * Método para buscar los pagos en un rango de fechas 
	 * Busca segun la fecha de vencimiento 
	 * @param fechaI String Fecha Inicial
	 * @param fechaF String Fecha Final
	 * @param Tipo String Tipo de transaccion (factura de compra, venta, etc)
	 * @param TipoConsulta Tipo de Consulta (0 por fecha, tipo de documento) (1 0 y factura) (2 0 y cliente) (3 1 y 2)
	 * @return retorna un Iterator con Tblpago Tbldoccomercial y TblPersona
	 */
	public Iterator ReportePagos(String fechaI, String fechaF,String estado,String Tipo,String idfactura,String idCliente ,int TipoConsulta){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		if(TipoConsulta==0){
			/*System.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+"' and" 
					+" u.fechaVencimiento<='"+fechaF+"' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.estado='1'");*/
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and  d.estado='1'");
		}else if(TipoConsulta==1){
			/*System.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+"' and" 
					+" u.fechaVencimiento<='"+fechaF+"' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.idDtoComercial='"+idfactura+"' and d.estado='1'");*/
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.idDtoComercial='"+idfactura+"' and d.estado='1'");
		}else if(TipoConsulta==2){
			/*System.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+"' and" 
					+" u.fechaVencimiento<='"+fechaF+"' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.estado='1'");*/
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.estado='1'");
		}else if(TipoConsulta==3){
			/*System.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+"' and" 
					+" u.fechaVencimiento<='"+fechaF+"' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.idDtoComercial='"+idfactura+"' and d.estado='1'");*/
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.idDtoComercial='"+idfactura+"' and d.estado='1'");
		}
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public Iterator BuscarPagoID(int id){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		
		// query para buscar los pagos
		System.out.println("*********************Buscaaaaarrrr pagooooosss********************************");
		System.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.idPago='"+String.valueOf(id)+"'");
		consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.idPago='"+String.valueOf(id)+"'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	// Buscar todos los pagos por idDtoComercial
	public Iterator BuscarPagoIdComercial(int id){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		System.out.println("*********************Buscaaaaarrrr pagooooosss agrupados********************************");
		//system.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.idPago='"+String.valueOf(id)+"'");
		consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.idDtoComercial='"+String.valueOf(id)+"'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	
	
	
	
	
	
	public int ultimoEgreso(){
		int num=0;
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		consulta = session.createQuery("select max(numEgreso) from Tblpago p where p.tbldtocomercial.tipoTransaccion='1' or p.tbldtocomercial.tipoTransaccion='2' or p.tbldtocomercial.tipoTransaccion='27'");
		num=Integer.parseInt(String.valueOf(consulta.uniqueResult()));
		return num;
	}
	public int ultimoIngreso(){
		int num=0;
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		consulta = session.createQuery("select max(numEgreso) from Tblpago p where not p.tbldtocomercial.tipoTransaccion='1' and not p.tbldtocomercial.tipoTransaccion='2' or p.tbldtocomercial.tipoTransaccion='26'");
		num=Integer.parseInt(String.valueOf(consulta.uniqueResult()));
		return num;
	}
	public int ultimoProducto(){
		int num=0;
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		consulta = session.createQuery("select max(idProducto) from Tblproducto");
		num=Integer.parseInt(String.valueOf(consulta.uniqueResult()));
		return num;
	}
	public T ultimoPlanCuenta(){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		//consulta = session.createQuery("select max(idPlan) from Tblplancuenta");
		consulta = session.createQuery("from "+domainClass.getName()+" u order by u.idPlan desc");
		System.out.println(consulta);
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		System.out.println("TAMAÑO LISTA "+returnList.size());
		session.close();
		if(returnList.size()>0){
			return (T) returnList.get(0);
		}else
		{
			return null;
		}
	}
	public T ultimoPlanCuenta(String idEmpresa){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		//consulta = session.createQuery("select max(idPlan) from Tblplancuenta");
		consulta = session.createQuery("from "+domainClass.getName()+" u where idEmpresa="+ idEmpresa+ " order by u.idPlan desc");
		System.out.println(consulta);
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		System.out.println("TAMAÑO LISTA "+returnList.size());
		session.close();
		if(returnList.size()>0){
			return (T) returnList.get(0);
		}else
		{
			return null;
		}
	}
	public Iterator ReportePagosEgreso(String Tipo,String num){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
			//system.out.println("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where d.tipoTransaccion='"+Tipo+"' and  d.estado='1' u.numEgreso='"+num+"'");
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where d.tipoTransaccion='"+Tipo+"' and  d.estado='1' and u.numEgreso='"+num+"'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * Metodo permite buscar pagos por la fecha de pago
	 * @param fechaI
	 * @param fechaF
	 * @param estado
	 * @param Tipo
	 * @param numRealTransaccion
	 * @param idCliente
	 * @param TipoConsulta
	 * @return
	 */
	public Iterator ReportePagosFechaPago(String fechaI, String fechaF,String estado,String Tipo,String numRealTransaccion,String idCliente ,int TipoConsulta){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		if(TipoConsulta==0){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaRealPago>='"+fechaI+" 00:00:00' and" 
					+" u.fechaRealPago<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and  d.estado='1'");
		}else if(TipoConsulta==1){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaRealPago>='"+fechaI+" 00:00:00' and" 
					+" u.fechaRealPago<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.numRealTransaccion='"+numRealTransaccion+"' and d.estado='1'");
		}else if(TipoConsulta==2){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaRealPago>='"+fechaI+" 00:00:00' and" 
					+" u.fechaRealPago<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.estado='1'");
		}else if(TipoConsulta==3){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaRealPago>='"+fechaI+" 00:00:00' and" 
					+" u.fechaRealPago<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.numRealTransaccion='"+numRealTransaccion+"' and d.estado='1'");
		}
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public Iterator ReportePagos2(String fechaI, String fechaF,String estado,String Tipo,String numRealTransaccion,String idCliente ,int TipoConsulta){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		if(TipoConsulta==0){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and  d.estado='1'");
		}else if(TipoConsulta==1){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.numRealTransaccion='"+numRealTransaccion+"' and d.estado='1'");
		}else if(TipoConsulta==2){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.estado='1'");
		}else if(TipoConsulta==3){
			consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
					+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona='"+idCliente+"' and d.numRealTransaccion='"+numRealTransaccion+"' and d.estado='1'");
		}
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public Iterator ReportePagosFacRealCompra(String fechaI, String fechaF,String estado,String Tipo,String idFactura,String idCliente, String numRealFactura){
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=null;
		consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercial d join d.tblpersonaByIdPersona p where u.fechaVencimiento>='"+fechaI+" 00:00:00' and" 
			+" u.fechaVencimiento<='"+fechaF+" 23:59:59' and u.estado='"+estado+"' and d.tipoTransaccion='"+Tipo+"' and d.tblpersonaByIdPersona like '"+idCliente+"%' and d.idDtoComercial like'"+idFactura+"%' and d.estado='1' " +
					"and d.numCompra like '"+numRealFactura+"%'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	public Iterator Kardex(String fechaI,String fechaF,String idProducto) {
		session = HibernateUtil.getSessionFactory().openSession();
		/*System.out.println("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles a  " +
				"where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"'  and a.tblproducto.idProducto='"+idProducto+"'");*/
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles a  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59'  and a.tblproducto.idProducto='"+idProducto+"'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * Mètodo para generar un reporte de documentos por Producto
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param idProducto id del producto
	 * @return Lista de Tbldtocomercial
	 */
	public Iterator ReporteCajaProducto(String fechaI,String fechaF,String idProducto,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		/*System.out.println("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles a  " +
				"where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' and TipoTransaccion='"+Tipo+"' and a.tblproducto.idProducto='"+idProducto+"'");*/
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles a  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' and a.tblproducto.idProducto='"+idProducto+"'");
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * Mètodo para generar un reporte de documentos por Cliente
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param idCliente id del cliente
	 * @return Lista de Tbldtocomercial
	 */
	public Iterator ReproteCajaCliente(String fechaI,String fechaF,String idCliente,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("from "+domainClass.getName()+" u  join u.tblpersonaByIdPersona a  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' and a.idPersona='"+idCliente+"'");
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tblpersonaByIdPersona a  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' and a.idPersona='"+idCliente+"'");
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	
	
	/**
	 * Mètodo para generar un reporte de documentos por Vendedor
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param idCliente id del cliente
	 * @return Lista de Tbldtocomercial
	 */
	public Iterator ReproteCajaVendedor(String fechaI,String fechaF,String idVendedor,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tblpersonaByIdVendedor a  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and u.tipoTransaccion='"+Tipo+"' and a.idPersona='"+idVendedor+"'");
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	
	public List<T> ReporteCajaNumDtoVenta(String fechaI,String fechaF,String Tipo, String NumDto) {
		session = HibernateUtil.getSessionFactory().openSession();
		
		Query consulta= session.createQuery("from "+domainClass.getName()+" u " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and u.tipoTransaccion='"+Tipo+"' and cast(u.numRealTransaccion, string) like '"+NumDto+"%'");			
		
		System.out.println(consulta);
		consulta.setFirstResult(0);
		consulta.setMaxResults(20);
		List itemBidMaps = consulta.list();
		session.close();
		return itemBidMaps;
	}
	public List<T> ReporteCajaNumDtoCompra(String fechaI,String fechaF,String Tipo, String NumDto) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta= session.createQuery("from "+domainClass.getName()+" u " +
					"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and u.tipoTransaccion='"+Tipo+"' and u.numCompra like '%"+NumDto+"%'");				
		
		//Le pasamos el numero maximo de registros, paginacion.
		
		//Le pasamos el numero maximo de registros, paginacion.
		System.out.println(consulta);
		consulta.setFirstResult(0);
		consulta.setMaxResults(20);
		List itemBidMaps = consulta.list();
		session.close();
		return itemBidMaps;
	}
	
	public Iterator ReporteCajaVendedorProducto(String fechaI,String fechaF,String idVendedor,String idProducto,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercialdetalles b " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' and u.tblpersonaByIdVendedor.idPersona='"+idVendedor+"' " +
						"and b.tblproducto.idProducto='"+idProducto+"'");
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	public Iterator ReporteCajaClienteProducto(String fechaI,String fechaF,String idCliente,String idProducto,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		/*System.out.println("from "+domainClass.getName()+" u  join u.tblpersonaByIdPersona a  " +
				"where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' and TipoTransaccion='"+Tipo+"' and a.idPersona='"+idCliente+"'");*/
		
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles b  " +
				"where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' and TipoTransaccion='"+Tipo+"' and u.tblpersonaByIdPersona.idPersona='"+idCliente+"' " +
						"and b.tblproducto.idProducto='"+idProducto+"'");
		
		/*
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  join u.tbldtocomercialdetalles b  join u.tblpersonaByIdPersona a " +
				"where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' and TipoTransaccion='"+Tipo+"' a.idPersona='"+idCliente+"'" +
						"and b.tblproducto.idProducto='"+idProducto+"'");		
		*/
		//Le pasamos el numero maximo de registros, paginacion.
		 
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * Mètodo para generar un reporte de caja por Vendedor y Cliente
	 * @param fechaI fecha de inicio 	
	 * @param fechaF fecha final
	 * @param idCliente id del cliente
	 * @return Lista de Tbldtocomercial
	 */
	public Iterator ReproteCajaVendedorCliente(String fechaI,String fechaF,String idVendedor,String idCliente,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tblpersonaByIdPersona a join u.tblpersonaByIdVendedor e  where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<='"+fechaF+" 23:59:59' " +
				"and TipoTransaccion='"+Tipo+"' and a.idPersona='"+idCliente+"'" +
						"and e.idPersona='"+idVendedor+"'");
		System.out.println(consulta);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		//Le pasamos el numero maximo de registros, paginacion.
		//List<T> returnList = consulta.list();
		session.close();
		return itemBidMaps;
	}
	
	public Iterator ReporteCajaVendedorClienteProducto(String fechaI,String fechaF,String idVendedor,String idCliente,String idProducto,String Tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
		/*System.out.println("from "+domainClass.getName()+" u  where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' " +
				"and TipoTransaccion='"+Tipo+"' and u.tblpersonaByIdPersona.idPersona='"+idCliente+"'" +
						"and u.tblpersonaByIdVendedor.idPersona='"+idVendedor+"'");*/
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tbldtocomercialdetalles b join u.tblpersonaByIdPersona a join u.tblpersonaByIdVendedor e where u.fecha>='"+fechaI+" 00:00:00' and u.fecha<= '"+fechaF+" 23:59:59' " +
				"and TipoTransaccion='"+Tipo+"' and a.idPersona='"+idCliente+"' " +
						"and e.idPersona='"+idVendedor+"' and b.tblproducto.idProducto='"+idProducto+"'");
		System.out.println(consulta);
		//Le pasamos el numero maximo de registros, paginacion.
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * Metodo para buscar en una tabla por un campo  filtrando datos 
	 * @param nombre nombre a buscar
	 * @param campo columna en la tabla
	 * @return entidad
	 */
	public List<T> findLike(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%'");
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Metodo para buscar en una tabla por un campo  filtrando datos y con el estado =1
	 * @param nombre nombre a buscar
	 * @param campo columna en la tabla
	 * @return entidad
	 */
	public List<T> findLikePro(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and estado='1'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
//	++++++++++++++++++  PARA QUE BUSQUE TODOS LOS PRODUCTOS EN EL frmProducto ++++++++++++++++++++++++
	public List<T> findLikePro2(String nombre,String campo,int stock, int Tipo, int Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(stock==2)// todos los productos
		{	
			consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1' and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
		}
		else if(stock==0)//SIN STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1' and u.stock<=0 and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}	
			else if(stock==1)//CON STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1' and u.stock>0 and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}
			else if(stock==3)// ELABORADOS
			{	
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1' and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}		
		//Le pasamos el registro desde el cual va a extraer
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		System.out.println(consulta);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	public List<T> findLikeProductoMateriaPrima(String nombre,String campo,int stock) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(stock==2)// todos los productos
		{	
			consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and estado='1' and jerarquia='0'" );
		}
		else if(stock==0)//SIN STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and estado='1' and u.stock<=0 and jerarquia='0'");
			}	
			else if(stock==1)//CON STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and estado='1' and u.stock>0 and jerarquia='0'");
			}
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	
	public List<T> findLikeProEliminados(String nombre,String campo,int stock) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(stock==3){//ELIMINADOS, CUANDO ESTADO=0
			consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+" like " +"'%"+nombre+"%' and estado='0'");
		}
		
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	
	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++        METODO PARA BUSQUEDAS AVANZADAS   EDU     +++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	public List<T> findLikeProDescripcion(String nombre,String campo,int stock, int Tipo, int Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(stock==2)// todos
		{	
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+nombre+" and u.estado='1' and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
		}
		else if(stock==0)//SIN STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+nombre+" and u.estado='1' and u.stock<=0 and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}	
			else if(stock==1)//CON STOCK
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+nombre+" and u.estado='1' and u.stock>0 and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}
			else if(stock==3)//ELABORADOS
			{
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+nombre+" and u.estado='1' and u.tipo='"+Tipo+"' and u.jerarquia='"+Jerarquia+"'");
			}
		System.out.println(consulta);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}

	
	
	public List<T> findLikeProDescripcionEliminados(String nombre,String campo,int opcion) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
	
			if(opcion==3){//ELIMINADOS
				consulta=session.createQuery("from "+domainClass.getName()+" u where u."+campo+nombre+" and estado='0'");
			}
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	
	public List<T> findByExample(T exampleObject) {
	 	Example example = Example.create(exampleObject).excludeZeroes()
 			.enableLike().ignoreCase();
		List<T> list = getHibernateTemplate().createCriteria(domainClass).add(example).list();
		session.getTransaction().commit();
		return list;
	}
 
	private Session getHibernateTemplate() {
		session = HibernateUtil.getSessionFactory().openSession();
 			//.getCurrentSession();
		session.beginTransaction();
		return session;
	}
	
	private void cerrarSession(Boolean rollBack){
		if(rollBack){
			session.getTransaction().rollback();
		}
		session.close();
	}
	public List<T> getListProd(Integer inicio, Integer NumReg,Integer Tipo,Integer Jerarquia) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x where estado='1' and tipo='"+Tipo+"' and jerarquia='"+Jerarquia+"'");
		System.out.println(consulta);
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(/*NumReg*/20);
		List list = consulta.list();
		List<T> returnList = list;
		session.close();
		return returnList;
	}
	
	public List<T> getListProd() {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x where estado='1'" );
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	//METODO PARA LISTAR DIFERENTE NUMERO DE REGISTROS DEPENDIENDO A EL FILTRO DE STOCK, O SOLO ELIMINADOS
	public List<T> getListProdConFiltro(Integer inicio, Integer NumReg, int opcion) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(opcion==0){//sin stock
			consulta=session.createQuery("from "+domainClass.getName()+" u where estado='1' and u.stock<=0");
		}else if(opcion==1){//con sotck
			consulta=session.createQuery("from "+domainClass.getName()+" u where estado='1' and u.stock>0");
		}else if(opcion==2){//todos
			 consulta = session.createQuery("from "+domainClass.getName()+" x where estado='1'" );
		}else if(opcion==3){//elim inados
			 consulta=session.createQuery("from "+domainClass.getName()+" u where estado='0'");
		}else if(opcion==4){
			consulta=session.createQuery("from "+domainClass.getName()+" u where jerarquia='1'");
		}
		
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(/*NumReg*/20);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	public List<T> getList(Integer inicio, Integer NumReg) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(NumReg);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	
	public List<T> getListCombo(Integer inicio, Integer NumReg) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(NumReg);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Muestra una lista de objetos segun su estado 1 activo 0 inactivo
	 * @param inicio
	 * @param NumReg
	 * @return List
	 */
	public List<T> getListPro(Integer inicio, Integer NumReg) {		
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" x where estado='1'");
		//Le pasamos el registro desde el cual va a extraer
		consulta.setFirstResult(inicio);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(/*NumReg*/20);
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Lista los atributos idPlan, nombreCuenta,idPlan de la tabla TblcuentaPlancuenta
	 * @return Iterator
	 */
	public Iterator ListarTblcuentaPlancuenta(String id) { 
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta;
		
		//Object count =  session.createQuery("select length(codigo) from tblcuenta order by length(codigo) ").setMaxResults(1);
		
		consulta = session.createQuery("select tblcuentaplancuenta.tblcuenta.idCuenta,tblcuentaplancuenta.tblcuenta.nombreCuenta,tblcuentaplancuenta.id.idPlan from "+domainClass.getName()+" tblcuentaplancuenta join tblcuentaplancuenta.tblplancuenta tblplancuenta"+
				" where tblplancuenta.tblempresa.idEmpresa='"+id+"'");
		/*consulta = session.createQuery("select tblcuenta.idCuenta,tblcuenta.nombreCuenta,tblplancuenta.idPlan from TblcuentaPlancuenta" +
				" where tblplancuenta.tblempresa.idEmpresa='"+id+"'");*/
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * Funcion findJoin, permite realizar busquedas por una columna determinada, retornando como resultado un
	 * listado producto de la relacion JOIN entre las tablas especificadas
	 * @param nombre De tipo STRING es la variable por la que se va a realizar la busqueda LIKE
	 * @param tabla  De tipo STRING, es el nombre de la tabla con la cual se va a unir, ej: tblclientes
	 * @param campo  De tipo STRING, es la columna con la cual se va a comparar el parametro nombre para filtrar el resultado de la consulta JOIN
	 * @return retorna un listado de objetos de tipo ROOT, es decir de tipo del objeto principal
	 */
	public Iterator findJoinPerso(String nombre,String tabla, String campo) { 
		session = HibernateUtil.getSessionFactory().openSession();
		String cons="";
		if(campo.equals("razonSocial") || campo.equals("nombreComercial") || campo.equals("direccion"))
		{
			String busquedaAvanzada=getStringLikeOutSpace(nombre,campo);
//			nombre=nombre.replace('%', ' ');
//			boolean espBlanco=true;
//            while(espBlanco==true)
//            {
//                int longitud=nombre.length();
//                if(longitud>0){
//                	if(nombre.charAt(longitud-1)==' '){
//                       nombre=nombre.substring(0,longitud-1);
//                    }
//                    else
//                    {espBlanco=false;}
//                }
//                else
//                {espBlanco=false;}	
//            }
//			String busquedaAvanzada=" like '%";
//			
//			for(int i=0;i<nombre.length()-1;i++)
//			{
//				if(nombre.charAt(i)==(' '))
//				{
//					busquedaAvanzada=busquedaAvanzada+"%' and u.descripcion like '%";
//				}else	
//				{	
//						busquedaAvanzada=busquedaAvanzada+nombre.charAt(i);
//				}					
//			}
//			if(nombre.length()>0)
//			{	
//				busquedaAvanzada=busquedaAvanzada+nombre.charAt(nombre.length()-1)+"%'";
//			}
//			else
//			{
//				busquedaAvanzada=busquedaAvanzada+"%'";
//			}	
		//	System.out.println("busquedaAvanzada "+busquedaAvanzada);
			cons = "from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+busquedaAvanzada+" and u.estado='1'";
		}else
		{
		cons = "from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1'";
		}
		Query consulta = session.createQuery(cons);
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public Iterator findJoinPerso2(String nombre,String tabla, String campo) { 
		session = HibernateUtil.getSessionFactory().openSession();
		
		String cons="";
		if(campo.equals("razonSocial") || campo.equals("nombreComercial") || campo.equals("direccion"))
		{
			String busquedaAvanzada=getStringLikeOutSpace(nombre,campo);
			System.out.println("busquedaAvanzada "+busquedaAvanzada);
			cons = "from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+busquedaAvanzada+" and u.estado='1'";
		}else
//		{
//		cons = "from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" like " +"'%"+nombre+"%' and estado='1'";
//		}
		
		
//		Query consulta;
		if(campo.equals("codigoTarjeta")){
//			consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" c where c."+campo+"  like " +"'%"+nombre+"%' and estado='1'");
			cons = "from "+domainClass.getName()+" u join u."+tabla+" c where c."+campo+"  like " +"'%"+nombre+"%' and u.estado='1'";
		}else{
//			consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" like " +"'%"+nombre+"%' and estado='1'");
			cons = "from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" like " +"'%"+nombre+"%' and u.estado='1'";
		}
		
		Query consulta = session.createQuery(cons);
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * 
	 * @param joinText datos para el join
	 * @return retorna un listado de objetos de tipo coorespondiente
	 */
	public Iterator findJoinOtherTable(String joinText) {
//	public List<T> findJoinOtherTable(String joinText) {
		System.out.println("findJoinOtherTable "+joinText);
		session = HibernateUtil.getSessionFactory().openSession();
//		Query consulta = session.createSQLQuery("select u.* from "+domainClass.getName()+" u "+joinText);
		Query consulta = session.createSQLQuery("select u.* from "+domainClass.getSimpleName().toLowerCase()+" u "+joinText);
		System.out.println(consulta.getQueryString());
		System.out.println(consulta);
//		System.out.println(consulta.);
		List<T> returnList = consulta.list();
		Iterator iterator=returnList.iterator();
//		System.out.println(Arrays.toString(returnList.toArray()));
//		System.out.println("Tama�o resultado join other table "+returnList.size());
////		while (returnList.iterator().hasNext()){
////			Object o=returnList.iterator().next();
////			System.out.println(o.getClass());
////		}
//		int cont=0;
//		while (iterator.hasNext() && cont<10){
//			Object[] pair = (Object[])iterator.next();
//			for (Object o:pair){
//				System.out.println(o.getClass());
//			}
//			cont++;
//			System.out.println(cont);
//		}
		
		session.close();
//		return returnList;
		return iterator;
	}
	
	/**
	 * Funcion findJoin, permite realizar busquedas por una columna determinada, retornando como resultado un
	 * listado producto de la relacion JOIN entre las tablas especificadas
	 * @param nombre De tipo STRING es la variable por la que se va a realizar la busqueda LIKE
	 * @param tabla  De tipo STRING, es el nombre de la tabla con la cual se va a unir, ej: tblclientes
	 * @param campo  De tipo STRING, es la columna con la cual se va a comparar el parametro nombre para filtrar el resultado de la consulta JOIN
	 * @return retorna un listado de objetos de tipo ROOT, es decir de tipo del objeto principal
	 */
	public Iterator findJoin(String nombre,String tabla, String campo) { //CAMBIE LA ÚLTIMA VEZ
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" like " +"'%"+nombre+"%' and estado='1'");
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public Iterator findJoinPro(String nombre,String tabla, String campo, Integer Tipo, Integer Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where "+campo+" like " +"'%"+nombre+"%' and estado='1' and tipo='"+Tipo+"' and jerarquia='"+Jerarquia+"'");
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	//++++++++++++++++++  PARA QUE BUSQUE TODOS LOS PRODUCTOS EN EL frmProducto ++++++++++++++++++++++++
	public Iterator findJoinPro2(String nombre,String tabla, String campo,int stock, int Tipo, int Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = null;
		if(stock==2)//selecciono todos los productos
		{	
			consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where "+campo+" like " +"'%"+nombre+"%' and estado='1' and tipo='"+Tipo+"' and jerarquia='"+Jerarquia+"'" );
		}
		else
		{
			if (stock==0)//selecciono sin stock
			{
				consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where "+campo+" like " +"'%"+nombre+"%' and estado='1' and tipo='"+Tipo+"' and jerarquia='"+Jerarquia+"' and u.stock<=0");
			}
			else//selecciono stock mayor a cero
			{
				consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where "+campo+" like " +"'%"+nombre+"%' and estado='1' and tipo='"+Tipo+"' and jerarquia='"+Jerarquia+"' and u.stock>0");
			}	
		}
//		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
//		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * busca los productos de cierta bodega con Stock > 0 y estado del producto 1
	 * @param ini inicio de listado
	 * @param bod Nombre de la Bodega a Buscar
	 * @param fin fin del listado
	 * @return Iterator
	 */
	public Iterator ListJoinProBod(String bod,int ini,int fin, Integer Tipo, Integer Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
				"where u.cantidad >'0' and a.nombre='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"'");
		//Le pasamos el numero maximo de registros, paginacion.
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public String getStringLikeOutSpace(String descripc, String campo){
		descripc=descripc.replace('%', ' ');
		boolean espBlanco=true;
        while(espBlanco==true)
        {
            int longitud=descripc.length();
            if(longitud>0){
            	if(descripc.charAt(longitud-1)==' '){
            		descripc=descripc.substring(0,longitud-1);
                }
                else
                {espBlanco=false;}
            }
            else
            {espBlanco=false;}	
        }
		String busquedaAvanzada=" like '%";
		
		for(int i=0;i<descripc.length()-1;i++)
		{
			if(descripc.charAt(i)==(' '))
			{
				//busquedaAvanzada=busquedaAvanzada+"%' and p.descripcion like '%";
				busquedaAvanzada=busquedaAvanzada+"%' and "+campo+" like '%";
			}else	
			{	
					busquedaAvanzada=busquedaAvanzada+descripc.charAt(i);
			}					
		}
		if(descripc.length()>0)
		{	
			busquedaAvanzada=busquedaAvanzada+descripc.charAt(descripc.length()-1)+"%'";
		}
		else
		{
			busquedaAvanzada=busquedaAvanzada+"%'";
		}	
	//	System.out.println("busquedaAvanzada "+busquedaAvanzada);
		return busquedaAvanzada;
	}
	public Iterator ListJoinProBodUbic2(String bod,int ini,String descripc, int stock, int Tipo, int Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta;
		
		String busquedaAvanzada=getStringLikeOutSpace(descripc,"p.descripcion");
		
		if(stock==3){
			/*consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
					"where a.ubicacion='"+bod+"' and p.estado='1'and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion like  '%"+descripc+"%'");*/
			
			consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
					"where a.ubicacion='"+bod+"' and p.estado='1'and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"");
			
		}else{
			if(stock==2)//selecciono todos los productos
			{	
				/*consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
						"where a.ubicacion='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion like  '%"+descripc+"%'");*/
				
				consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
						"where a.ubicacion='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"");
			}
			else
			{
				if (stock==0)//selecciono sin stock
				{
					consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
							"where a.ubicacion='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"' and u.cantidad<=0");
				}
				else//selecciono stock mayor a cero
				{
					consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
							"where a.ubicacion='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"' and u.cantidad>0");
				}	
			}
		}
		//Le pasamos el numero maximo de registros, paginacion.
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	public Iterator ListJoinProBod2(String bod,int ini,String descripc, int stock, int Tipo, int Jerarquia) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta;
		String busquedaAvanzada=getStringLikeOutSpace(descripc,"p.descripcion");
		
		if(stock==3){
			consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
					"where a.nombre='"+bod+"' and p.estado='1'and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"");
		}else{
			if(stock==2)//selecciono todos los productos
			{	
				consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
						"where a.nombre='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+"");
			}
			else
			{
				if (stock==0)//selecciono sin stock
				{
					consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
							"where a.nombre='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+" and u.cantidad<=0");
				}
				else//selecciono stock mayor a cero
				{
					consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
							"where a.nombre='"+bod+"' and p.estado='1' and p.tipo='"+Tipo+"' and p.jerarquia='"+Jerarquia+"' and p.descripcion"+busquedaAvanzada+" and u.cantidad>0");
				}	
			}
		}
		System.out.println(consulta);
		//Le pasamos el numero maximo de registros, paginacion.
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	//++++++++++++++++++  PARA QUE BUSQUE TODOS LOS PRODUCTOS EN EL frmProducto ++++++++++++++++++++++++

	/**
	 * Metodo busca un producto en cierta bodega
	 * @param bod bodega en donde buscar
	 * @param descripcion descripcion del producto
	 * @return Iterato
	 */
	public Iterator BuscarJoinProBod(String bod,String descripcion) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from TblproductoTblbodega u join u.tblbodega a join u.tblproducto p " +
				"where u.cantidad!='0' and a.idBodega='"+bod+"' and p.estado='1' and p.descripcion like  '%"+descripcion+"%'");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * lista mediante un join solo la tabla producto
	 * @param tabla tabla para el join
	 * @param ini inicio de listado
	 * @param fin fin del listado
	 * @return Iterator
	 */
	public Iterator ListJoinPro(String tabla,int ini,int fin) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u.estado='1'");
		consulta.setFirstResult(ini);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(/*fin*/20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * lista mediante un join 
	 * @param tabla tabla para el join
	 * @param ini inicio de listado
	 * @param fin fin del listado
	 * @return Iterator
	 */
	public Iterator ListJoin(String tabla,int ini,int fin) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u.estado='1'");
		consulta.setFirstResult(ini);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(fin);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public Iterator ListJoin2(String tabla) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u.estado='1'");
		//consulta.setFirstResult(ini);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(fin);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * Metodo para hacer una consula de la tabla persona empreado y cargo
	 * @param tabla
	 * @param ini
	 * @param fin
	 * @return
	 */
	public Iterator ListEmpleadosLike(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		
		String cons="";
		if(campo.equals("razonSocial") || campo.equals("nombreComercial") || campo.equals("direccion"))
		{
			String busquedaAvanzada=getStringLikeOutSpace(nombre,campo);
			System.out.println("busquedaAvanzada "+busquedaAvanzada);
			cons = "from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where u."+campo+busquedaAvanzada+" and a.estado='1' and u.estado='1'";
		}else{
			cons="from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where "+campo+" like " 
					+"'%"+nombre+"%' and a.estado='1' and u.estado='1'";
		}
	Query consulta = session.createQuery(cons);
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	public Iterator ListEmpleadosLike2(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		String cons="";
		if(campo.equals("razonSocial") || campo.equals("nombreComercial") || campo.equals("direccion"))
		{
			String busquedaAvanzada=getStringLikeOutSpace(nombre,campo);
			System.out.println("busquedaAvanzada "+busquedaAvanzada);
			cons = "from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where u."+campo+busquedaAvanzada+" and a.estado='1' and u.estado='1'";
		}else{
			cons="from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where "+campo+" like " 
					+"'%"+nombre+"%' and a.estado='1' and u.estado='1'";
		}
	Query consulta = session.createQuery(cons);
//		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where "
//				+campo+" like " +"'%"+nombre+"%' and estado='1'");
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	//+++++++++++++     LIST PERSONAS CEDULAS INCORRECTAS    +++++++++++++++++++++++++++++++++
	public Iterator ListCedulasIncorrectas(String nombre,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u  where "+campo+" like " +"'%"+nombre+"%' and estado='1'");
		//consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		//consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	/**
	 * Metodo para hacer una consula de la tabla persona empreado y cargo
	 * @param tabla
	 * @param ini
	 * @param fin
	 * @return
	 */
	public Iterator ListEmpleados(int ini,int fin) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u.tblempleados a join a.tblcargo where a.estado='1' and u.estado='1' and u.estado='1'");
		consulta.setFirstResult(ini);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(/*fin*/20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	/**
	 * Funcion para obtener el último registro ingresado en una tabla, según un criterio de busqueda 
	 * y ordenado según un campo indicado
	 * @param filtro	De tipo String, es una parte de la sentencia, en caso de que se requiera algun filtro
	 * @param campoOrden	De tipo String, es el nombre del campo por el cual vamos a ordenar
	 * @return Retorna un objeto del tipo de la clase de la cual esta siendo llamada la función
	 */
	public T getLast(String filtro, String campoOrden){
		try{
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" "+ filtro + " order by "+campoOrden+" desc");
		//Le decimos que como maximo debe haber un solo registro.
		System.out.println("getlast: "+consulta.toString());
		consulta.setMaxResults(1);
		List<T> returnList = consulta.list();
		session.close();
		if(returnList.size()>0){
			System.out.println("getlast cantidad:  "+returnList.size());
			return (T) returnList.get(0);
		}else
		{
			return null;
		}
		}catch(Exception e){
			e.printStackTrace(System.out);
			System.out.println(e.getMessage());
			return null;
		}
	}
	/**
	 * Funcion findjoinGeneric es una funcion generica para unir dos tablas segun un campo en comun y un filtro
	 * @param filtro de tipo STRING
	 * @param tabla de tipo STRING
	 * @param campo de tipo STRING
	 * @return un ITERATOR que contiene un listaod de dos objetos, las dos tablas resultado del join
	 */
	public Iterator findJoinGeneric(String filtro, String tabla, String campo)
	{
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from "+domainClass.getName()+" u join u."+tabla+" where u."+campo+" = "+filtro);
		consulta.setFirstResult(0);
		//Le pasamos el numero maximo de registros, paginacion.
		consulta.setMaxResults(20);
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
	
	
//******
	
	public Integer grabarP(T t, Set<TbldtocomercialTbltipopago> pagosAsociados, Double iva) throws ErrorAlGrabar {
		Iterator iterPagos;
		GreetingServiceImpl objServidor=null;
		iterPagos = pagosAsociados.iterator();
		TbldtocomercialTbltipopago pago;
		System.out.println("Entro a grabarP");
		Integer id=0;
		try {
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		//Vamos a verificar que no hayan numeros duplicados
	 		Tbldtocomercial dto = (Tbldtocomercial) t;
	 		Integer numReal = dto.getNumRealTransaccion();
	 		Boolean unico = true;
	 		
	 		while(unico)
	 		{
	 			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.numRealTransaccion = " +"'"+numReal+"' "+
	 					"and u.tipoTransaccion ="+"'"+dto.getTipoTransaccion()+"'");
	 			System.out.println(consulta);
	 			if(!consulta.list().isEmpty())
		 		{
		 			numReal++;
		 		}else{
		 			unico=false;
		 			break;
		 		}
	 		}
	 		Set<Tblretencion> retenciones = dto.getTblretencionsForIdFactura();
//	 		Set<Tblretencion> retencionesC = new HashSet<Tblretencion>();
	 		dto.setTblretencionsForIdFactura(null);
	 		dto.setNumRealTransaccion(numReal);
	 		System.out.println("ANTES ALMACENAMOS DTO COMERCIAL");
	 		id  = (Integer) session.save(t); 
	 		dto.setIdDtoComercial(id);
	 		if (dto.getTipoTransaccion()==0 || dto.getTipoTransaccion()==1){
		 		if (retenciones!=null){
		 			if (retenciones.size()>0){
		 				System.out.println("Retenciones en grabarp");
				 		Tbldtocomercial documentoRetencion= null;
				 		for (Tblretencion ret:retenciones){
				 			if (documentoRetencion==null){
				 				documentoRetencion=ret.getTbldtocomercialByIdDtoComercial();
				 				int idDtoRet=(Integer)session.save(documentoRetencion);
				 				documentoRetencion.setIdDtoComercial(idDtoRet);
				 			}
				 			ret.setTbldtocomercialByIdDtoComercial(documentoRetencion);
				 			ret.setTbldtocomercialByIdFactura(dto);
				 			session.save(ret);
				 			//				 			retencionesC.add(ret);
				 		}
		 			}
		 		}
	 		}
	 		System.out.println("DESPUES ALMACENAMOS DTO COMERCIAL");
	 		while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
				System.out.println("ANTES ALMACENAMOS PAGO");
				session.save(pago);
				System.out.println("DESPUES ALMACENAMOS PAGO");
			}
	 		
	 		session.getTransaction().commit();
	 		//AQUI VAMOS A CONFIRMAR LA FACTURA SOLO DE VENTA
	 		
	 	} catch (HibernateException e) {
	 		System.out.println(e.toString());
	 		System.out.println(e.getMessage());
	 		e.printStackTrace(System.out);
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	 	return id;
	}
	public int grabaryConfirmar(T t, Set<TbldtocomercialTbltipopago> pagosAsociados, Double iva,
			Set<Tblproducto> productos,Set<TblproductoTblbodega> prodBods) throws ErrorAlGrabar {
		Iterator iterPagos;
		GreetingServiceImpl objServidor=null;
		iterPagos = pagosAsociados.iterator();
		TbldtocomercialTbltipopago pago;
		Tbldtocomercial Anticipodto = new Tbldtocomercial();
		Iterator iterProductos;
		Iterator iterProductoBods;	
		int idDtocomercial;
		System.out.println("ENTRO A GRABAR Y CONFIRMAR");
		try {
			
	 		session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		
	 		//Vamos a verificar que no hayan numeros duplicados
	 		Tbldtocomercial dto = (Tbldtocomercial) t;
	 		dto.setEstado('1');
	 		Integer numReal = dto.getNumRealTransaccion();
	 		Boolean unico = true;
	 		
			// or String hqlDelete = "delete Customer where name = :oldName";
	 		
			if (dto.getIdDtoComercial()!=null){
				String hqlDeleteDtoRet = "delete Tbldtocomercial as d where d.idDtoComercial in "
						+ "(select c.tbldtocomercialByIdDtoComercial.idDtoComercial from "
						+ "Tblretencion as c where c.tbldtocomercialByIdFactura.idDtoComercial = :idDto)";
				System.out.println(hqlDeleteDtoRet+(" "+dto.getIdDtoComercial()+""));
				int resultadoDtoRet=session.createQuery( hqlDeleteDtoRet )
				        .setString( "idDto", dto.getIdDtoComercial()+"" )
				        .executeUpdate();
				
				String hqlDelete = "delete Tblretencion as c where c.tbldtocomercialByIdFactura.idDtoComercial = :idDto";
				System.out.println(hqlDelete+(" "+dto.getIdDtoComercial()+""));
				int resultado=session.createQuery( hqlDelete )
				        .setString( "idDto", dto.getIdDtoComercial()+"" )
				        .executeUpdate();
				System.out.println(hqlDelete+(" "+dto.getIdDtoComercial()+" ")+"--"+resultado);
				
				
			}
	 		
			
	 		//ACTUALIZAMOS EL STOCK DE LOS PRODUCTOS
	 		iterProductos = productos.iterator();
			iterProductoBods = prodBods.iterator();
			Tblproducto producto;
			TblproductoTblbodega productoB;
			//While para recorrer los productos 
			System.out.println("1 ENTROR ACTUALIZAR STOCK");
			while (iterProductos.hasNext()) {
				producto = (Tblproducto) iterProductos.next();
				session.update(producto);
			}
			System.out.println("1 SALIO ACTUALIZAR STOCK");
			//While para recorrer los items ProductoBodega
			System.out.println("2 ENTRO WHILE BODEGAS STOCK");
			while (iterProductoBods.hasNext()) {
			System.out.println("2.1 ENTRO WHILE BODEGAS STOCK");
				productoB = (TblproductoTblbodega) iterProductoBods.next();
				//System.out.print("producto bodega"+productoB.getTblbodega().getNombre());
				session.update(productoB);
			}
			System.out.println("3 SALIO WHILE BODEGAS STOCK");
			System.out.println("4 ENTRO WHILE BODEGAS STOCK");
	 		while(unico)
	 		{
	 			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.numRealTransaccion = " +"'"+numReal+"'"+
	 					"and u.tipoTransaccion ="+"'"+dto.getTipoTransaccion()+"'");
		 		if(!consulta.list().isEmpty())
		 		{
		 			numReal++;
		 		}else{
		 			unico=false;
		 			break;
		 		}
	 		}
	 		System.out.println("4 SALIO WHILE BODEGAS STOCK");
	 		System.out.println("transacciones dto "+dto.getNumRealTransaccion()+" t "+((Tbldtocomercial)t).getNumRealTransaccion());
	 		Set<Tblretencion> retenciones = dto.getTblretencionsForIdFactura();
	 		dto.setTblretencionsForIdFactura(null);
	 		//dto.setNumRealTransaccion(numReal+1);
	 		dto.setNumRealTransaccion(numReal);
	 		System.out.println("transacciones dto "+dto.getNumRealTransaccion()+" t "+((Tbldtocomercial)t).getNumRealTransaccion());
	 		idDtocomercial  = (Integer) session.save(t);

	 		dto.setIdDtoComercial(idDtocomercial);
	 		if (dto.getTipoTransaccion()==0 || dto.getTipoTransaccion()==1){
		 		if (retenciones!=null){
		 			if (retenciones.size()>0){
		 				System.out.println("Retenciones en grabarp");
				 		Tbldtocomercial documentoRetencion= null;
				 		for (Tblretencion ret:retenciones){
				 			if (documentoRetencion==null){
				 				documentoRetencion=ret.getTbldtocomercialByIdDtoComercial();
				 				int idDtoRet=(Integer)session.save(documentoRetencion);
				 				documentoRetencion.setIdDtoComercial(idDtoRet);
				 			}
				 			ret.setTbldtocomercialByIdDtoComercial(documentoRetencion);
				 			ret.setTbldtocomercialByIdFactura(dto);
				 			session.save(ret);
				 			//				 			retencionesC.add(ret);
				 		}
		 			}
		 		}
	 		}
	 		
	 		if (dto.getIdAnticipo() != null) {
				System.out.println("dto numanticipo: " + dto.getIdAnticipo());
				Anticipodto = (Tbldtocomercial) session.get("com.giga.factum.server.base.Tbldtocomercial",
						dto.getIdAnticipo());
				System.out.println("dto anticipodto: " + Anticipodto.getIdDtoComercial());
				// Anticipodto.setIdDtoComercial();
				Anticipodto.setEstado('5');
				session.update(Anticipodto);
			}
	 		
	 		System.out.println("5 ENTRO WHILE BODEGAS STOCK");
	 		while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				pago.setId(new TbldtocomercialTbltipopagoId(idDtocomercial,pago.getTbltipopago().getIdTipoPago()));
				session.save(pago);
			}	
	 		System.out.println("5 SALIO WHILE BODEGAS STOCK");
	 		//AQUI VAMOS A CONFIRMAR LA FACTURA SOLO DE VENTA	 	
	 		session.getTransaction().commit();	
	 		return idDtocomercial;
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		e.printStackTrace(System.out);
	 		System.out.println(e.getMessage().toString());
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}
	
	

	public void grabarConf(T t, Integer integer, Set<Tblproducto> productos,Set<TblproductoTblbodega> prodBods,
			Set<TbldtocomercialTbltipopago> pagosAsociados) throws ErrorAlGrabar {
		Iterator iterProductos;
		Iterator iterProductoBods;
		Iterator iterPagos;
		System.out.println("Entro a grabarConf");
		//try {
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();
	 		
	 		
			iterProductos = productos.iterator();
			iterProductoBods = prodBods.iterator();
			iterPagos = pagosAsociados.iterator();
			Tblproducto producto;
			TbldtocomercialTbltipopago pago;
			TblproductoTblbodega productoB;
			//While para recorrer los productos 
			while (iterProductos.hasNext()) {
				producto = (Tblproducto) iterProductos.next();
				session.update(producto);
			}
			//While para recorrer los items ProductoBodega
			while (iterProductoBods.hasNext()) {
				productoB = (TblproductoTblbodega) iterProductoBods.next();
				session.update(productoB);
			}
			
			//Vamos a proceder a eliminar el documento confirmado eliminando primero los elementos asociados		
			String hqlDelete = "delete TbldtocomercialTbltipopago as c where c.id.idDtoComercial = :idDto";
			// or String hqlDelete = "delete Customer where name = :oldName";
			session.createQuery( hqlDelete )
			        .setString( "idDto", integer+"" )
			        .executeUpdate();
			
			Tbldtocomercial docOld = new Tbldtocomercial();
			docOld = (Tbldtocomercial) session.load(domainClass, integer);//load(integer);
			//System.out.println("Doc a ELIMINAR gg: "+ integer+"y tb"+docOld.getNumRealTransaccion()+"estado"+docOld.getEstado() );
			session.delete(docOld); 		
			//Vamos a eliminar las formas de pago asociadas al antiguo documento.
			//Integer id = integer;
		    
			//try{session.merge(t);//.update(t);
			//}catch(Exception e){
			//session.clear();
				//id = (Integer) session.save(new Tbldtocomercial());
				Tbldtocomercial ndto = new Tbldtocomercial();
				ndto = (Tbldtocomercial) t;
				//ndto.setIdDtoComercial(id);
				Integer id=(Integer) session.save(ndto);
			//}
			
	 		//Vamos a proceder a guardar las formas de pago
	 		//While para recorrer los productos 
			while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
				session.save(pago);
			}
	 		session.getTransaction().commit();
	 		//session.close();
	 	//} catch (HibernateException e) {
	 		//session.getTransaction().rollback();
	 		//throw new ErrorAlGrabar(e.getMessage());
	 	//} finally {
	 		if(session!=null)
	 		session.close();
	 	//}
	}

	/*public void grabarConf(T t, Integer integer, Set<Tblproducto> productos,Set<TblproductoTblbodega> prodBods,
			Set<TbldtocomercialTbltipopago> pagosAsociados) throws ErrorAlGrabar {
		Iterator iterProductos;
		Iterator iterProductoBods;
		Iterator iterPagos;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();		
			iterPagos = pagosAsociados.iterator();
			Tblproducto producto;
			TbldtocomercialTbltipopago pago;
			TblproductoTblbodega productoB;
			if(!productos.equals(null))
			{
				iterProductos = productos.iterator();
				iterProductoBods = prodBods.iterator();
				//While para recorrer los productos 
				while (iterProductos.hasNext()) {
					producto = (Tblproducto) iterProductos.next();
					session.update(producto);
				}
				//While para recorrer los items ProductoBodega
				while (iterProductoBods.hasNext()) {
					productoB = (TblproductoTblbodega) iterProductoBods.next();
					session.update(productoB);
				}
			}
			//Vamos a proceder a eliminar el documento confirmado eliminando primero los elementos asociados		
			String hqlDelete = "delete TbldtocomercialTbltipopago as c where c.id.idDtoComercial = :idDto";
			// or String hqlDelete = "delete Customer where name = :oldName";
			session.createQuery( hqlDelete )
			        .setString( "idDto", integer+"" )
			        .executeUpdate();
			
			Tbldtocomercial docOld = new Tbldtocomercial();
			docOld = (Tbldtocomercial) session.load(domainClass, integer);//load(integer);
			System.out.println("Doc a ELIMINAR: "+ integer );
			session.delete(docOld); 		
			//Vamos a eliminar las formas de pago asociadas al antiguo documento.
			Integer id = integer;
		    session.update(t);
			
			
	 		//Vamos a proceder a guardar las formas de pago
	 		//While para recorrer los productos 
			while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
				session.save(pago);
			}
	 		session.getTransaction().commit();
	 		//session.close();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	} finally {
	 		if(session!=null)
	 		session.close();
	 	}
	}*/
	
	
	/*public void grabarConf(T t, Integer integer, Set<Tblproducto> productos,Set<TblproductoTblbodega> prodBods,
			Set<TbldtocomercialTbltipopago> pagosAsociados) throws ErrorAlGrabar {
		Iterator iterProductos;
		Iterator iterProductoBods;
		Iterator iterPagos;
		
		//try {
			session = HibernateUtil.getSessionFactory().openSession();
	 		session.beginTransaction();		
			iterPagos = pagosAsociados.iterator();
			Tblproducto producto;
			TbldtocomercialTbltipopago pago;
			TblproductoTblbodega productoB;
			if(!productos.equals(null))
			{
				iterProductos = productos.iterator();
				iterProductoBods = prodBods.iterator();
				//While para recorrer los productos 
				while (iterProductos.hasNext()) {
					producto = (Tblproducto) iterProductos.next();
					session.update(producto);
				}
				//While para recorrer los items ProductoBodega
				while (iterProductoBods.hasNext()) {
					productoB = (TblproductoTblbodega) iterProductoBods.next();
					session.update(productoB);
				}
			}
			//Vamos a proceder a eliminar el documento a confirmar eliminando primero los elementos asociados		
			String hqlDelete = "delete TbldtocomercialTbltipopago as c where c.id.idDtoComercial = :idDto";
			// or String hqlDelete = "delete Customer where name = :oldName";
			session.createQuery( hqlDelete )
			        .setString( "idDto", integer+"" )
			        .executeUpdate();
			
			Tbldtocomercial docOld = new Tbldtocomercial();
			docOld = (Tbldtocomercial) session.load(domainClass, integer);//load(integer);
			System.out.println("Doc a ELIMINAR: "+ integer );
			session.delete(docOld); 		
			//Vamos a eliminar las formas de pago asociadas al antiguo documento.
			//Integer id = (Integer) session.save(t);//integer;
		
			Tbldtocomercial documento = (Tbldtocomercial) t;
			System.out.println("Supuesto nuevo ID: "+documento.getIdDtoComercial());
			//session.flush()
			Integer id = integer;
			session.update(t);
			
	 		//Vamos a proceder a guardar las formas de pago
	 		//While para recorrer los productos 
			while (iterPagos.hasNext()) {
				pago = (TbldtocomercialTbltipopago) iterPagos.next();
				pago.setId(new TbldtocomercialTbltipopagoId(id,pago.getTbltipopago().getIdTipoPago()));
				session.save(pago);
			}
	 		session.getTransaction().commit();
	 		//session.close();
	 	} catch (HibernateException e) {
	 		session.getTransaction().rollback();
	 		throw new ErrorAlGrabar(e.getMessage());
	 	//} finally {
	 		if(session!=null)
	 		session.close();
	 	//}
	}*/
	public List<T> ListarPosiciones(String NombreDocumento) {
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta = session.createQuery("from  Tblposicion  p where p.NombreDocumento='"+NombreDocumento+"'" );
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Metodo que busca una posicion por el nombre del documento y el campo
	 * @param NombreDocumento
	 * @param campo
	 * @return
	 */
	public List<T> BuscarPosicion(String NombreDocumento,String campo) {
		session = HibernateUtil.getSessionFactory().openSession();
		List<T> returnList=null;
		try{
			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.NombreDocumento="+"'"+NombreDocumento+
					"' and u.Campo='"+campo+"'");
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			returnList = consulta.list();
			session.close();
			return returnList;
		}catch(Exception e){
			//system.out.println("retorna null size= "+returnList.size());
			return returnList;
		}
		
	}
	//REPORTES
	public float ReproteTotalDocumentos(String fechaI, String fechaf, String tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
        Object count =  session.createQuery("SELECT sum((total*impuesto/100)+total-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+" 00:00:00' and d.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' " +
        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'")
	 			.uniqueResult();
      /*  System.out.println("SELECT sum((total*impuesto/100)+total-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+"' and d.tbldtocomercial.fecha<='"+fechaf+"' " +
        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'");*/
		//session.getTransaction().commit();
		session.close();
		float num=Float.parseFloat(String.valueOf(count));
		return num;
    }
	public float ReproteSubtIVA0Documentos(String fechaI, String fechaf,String tipo) {
		try{
			session = HibernateUtil.getSessionFactory().openSession();
	        Object count =  session.createQuery("SELECT sum(total-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+" 00:00:00' and d.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and d.impuesto='0' " +
	        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'")
		 			.uniqueResult();
			//session.getTransaction().commit();
	      /*  System.out.println("SELECT sum(total-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+"' and d.tbldtocomercial.fecha<='"+fechaf+" and d.impuesto='0'" +
	        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'");*/
			session.close();
			float num=Float.parseFloat(String.valueOf(count));
			//System.out.println(num);
			return num;
		}catch(Exception e){
			System.out.println("error "+e.getMessage());
			return 0;
		}
		
    }
	public float ReproteIVADocumentos(String fechaI, String fechaf,String tipo) {
		try{
			session = HibernateUtil.getSessionFactory().openSession();
	        Object count =  session.createQuery("SELECT sum((total*impuesto/100)-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+" 00:00:00' and d.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' " +
	        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'")
		 			.uniqueResult();
			/*System.out.println("SELECT sum((total*impuesto/100)-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+"' and d.tbldtocomercial.fecha<='"+fechaf+" " +
	        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and d.tbldtocomercial.estado='1'");*/
			session.close();
			float num=Float.parseFloat(String.valueOf(count));
			//System.out.println(num);
			return num;
		}catch(Exception e){ 
			System.out.println("error "+e.getMessage());
			return 0;
		}
		
    }
	public Float ReproteSubTotalNetoDocumentos(String fechaI, String fechaf,String tipo) {
		session = HibernateUtil.getSessionFactory().openSession();
        Object count =  session.createQuery("SELECT SUM((total)-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+" 00:00:00' and d.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' " +
        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and impuesto>'0' and d.tbldtocomercial.estado='1' ").uniqueResult();
		//session.getTransaction().commit();
      /*  System.out.println("SELECT SUM((total)-((total*departamento/100))) FROM Tbldtocomercialdetalle d where  d.tbldtocomercial.fecha>='"+fechaI+"' and d.tbldtocomercial.fecha<='"+fechaf+"' " +
        		"and d.tbldtocomercial.tipoTransaccion='"+tipo+"' and impuesto>'0' and d.tbldtocomercial.estado='1'"); */
		session.close();
		Float num=Float.parseFloat(String.valueOf(count));
		return num;
    }
	/*
	 * Mètodo para listar el libro diario en un rango de fechas
	 * @param String fechai
	 * @param String fechaf
	 */
	public List<T> getListLibro(String fechai, String fechaf, String planCuentaID) {		
		session = HibernateUtil.getSessionFactory().openSession();
	/*	System.out.println("from "+domainClass.getName()+" m where m.tblmovimiento.fecha>='"+fechai+"' and m.tblmovimiento.fecha<='"+fechaf+"'" +
				"and m.tblmovimiento.tbldtocomercial.estado!='0'");*/
		Query consulta = session.createQuery("from "+domainClass.getName()+" m where m.tblmovimiento.fecha>='"+fechai+" 00:00:00' and m.tblmovimiento.fecha<='"+fechaf+" 23:59:59'  " +
				"and m.tblmovimiento.tbldtocomercial.estado!='0' and m.tblcuentaPlancuenta.tblplancuenta.idPlan='"+planCuentaID+"' order by m.tblmovimiento.idMovimiento asc");
		List<T> returnList = consulta.list();
		session.close();
		return returnList;
	}
	/**
	 * Metodo que permite obtener un  reporte valorizado de ventas por empleado
	 * @param idPersona 
	 * @param fechaf 
	 * @param fechai 
	 * @return float
	 */ 

	public Float ReporteVentasEmpleado(Integer idPersona, String fechai, String fechaf, double IVA){
		  String consulta="";
		  float numF=0;
		  float numN=0;
		  try {
		   session = HibernateUtil.getSessionFactory().openSession();
//		         consulta="select sum(case impuesto when "+IVA+" then (total*"+(1+(IVA/100))+") else total end) " +
//		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
//		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
//		        "and  (u.tbldtocomercial.tipoTransaccion='0')";
		         
		         consulta="select sum(case u.porcentaje when "+IVA+" then (dt.total*"+(1+(IVA/100))+") else dt.total end) " +
		   		      "from"+
		   		      " TbldtocomercialdetalleTblmultiImpuesto u"+
		   		      " , Tbldtocomercial d, Tbldtocomercialdetalle dt "+
		   		      "where d.fecha>='"+fechai+" 00:00:00' " +
		   		      "and d.fecha<='"+fechaf+" 23:59:59' and d.tblpersonaByIdVendedor.idPersona='"+String.valueOf(idPersona)+
		   		      "' and d.estado='1' and dt.idDtoComercialDetalle=u.tbldtocomercialdetalle.idDtoComercialDetalle " +
		   		      " and dt.tbldtocomercial.idDtoComercial=d.idDtoComercial "+
		   		        "and  (d.tipoTransaccion='0') ";
		         //System.out.println(consulta);
		   Object count =  session.createQuery(consulta).uniqueResult();
		        
		         session.close();
		         
		         numF=(Float.valueOf(count.toString()));
		  }catch (Exception e){
		   numF=0;
		  } 
		  try {
		   session = HibernateUtil.getSessionFactory().openSession();
		         consulta="select sum((total)) " +
		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
		        "and  (u.tbldtocomercial.tipoTransaccion='2')";
		   Object count =  session.createQuery(consulta).uniqueResult();
		        // System.out.println(consulta);
		         session.close();
		        // System.out.println(count.toString());
		         numN=(Float.valueOf(count.toString())); 
		  }catch (Exception e){
		   numN= 0;
		  } 
		  return numN+numF;
		 }
	
	public Float UtilidadPorEmpleadoDetalle(Integer idPersona, String fechai, String fechaf){
		  String consulta="";
		  float numF=0;
		  float numN=0;
		  try {  
		   session = HibernateUtil.getSessionFactory().openSession();     
		   consulta="select sum((total)-(costoProducto*u.cantidad)) " +
		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
		        " and  (u.tbldtocomercial.tipoTransaccion='0')";
		         Object count =  session.createQuery(consulta).uniqueResult();
		        // System.out.println(consulta);
		         session.close();
		         
		         numF=(Float.valueOf(count.toString()));
		   
		  }catch (Exception e){
		   numF=0;
		  }
		  try {  
		   session = HibernateUtil.getSessionFactory().openSession();     
		   consulta="select sum((total)-(costoProducto*u.cantidad)) " +
		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
		        " and  (u.tbldtocomercial.tipoTransaccion='2')";
		         Object count =  session.createQuery(consulta).uniqueResult();
		       //  System.out.println(consulta);
		         session.close();
		         numN=(Float.valueOf(count.toString()));
		   
		  }catch (Exception e){
		   numN=0;
		  }
		  return numN+numF;
		 }
	
	public Float UtilidadPorEmpleado(Integer idPersona, String fechai, String fechaf){
		  String consulta="";
		  float numF=0;
		  float numN=0;
		  try {  
		   session = HibernateUtil.getSessionFactory().openSession();     
		   consulta="select sum((total)-(u.tblproducto.promedio*u.cantidad)) " +
		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
		        " and  (u.tbldtocomercial.tipoTransaccion='0')";
		         Object count =  session.createQuery(consulta).uniqueResult();
		         //system.out.println(consulta);
		         session.close();
		         
		         numF=(Float.valueOf(count.toString()));
		   
		  }catch (Exception e){
		   numF=0;
		  }
		  try {  
		   session = HibernateUtil.getSessionFactory().openSession();     
		   consulta="select sum((total)-(u.tblproducto.promedio*u.cantidad)) " +
		      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
		      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdVendedor='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
		        " and  (u.tbldtocomercial.tipoTransaccion='2')";
		         Object count =  session.createQuery(consulta).uniqueResult();
		         System.out.println(consulta);
		         session.close();
		         numN=(Float.valueOf(count.toString()));
		   
		  }catch (Exception e){
		   numN=0;
		  }
		  return numN+numF;
		 }
			
	//++++++++++++++++++++++++++++++++++++             dao                 ++++++++++++++++++++++++++++++++++++++++++++
/*	
	public Iterator listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax)
	{
		session = HibernateUtil.getSessionFactory().openSession();
//		select td.tblproducto.idProducto, td.desProducto,td.cantidad,td.precioUnitario, pro.stock, 
//		pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha) from 
//		Tbldtocomercialdetalle td,Tbldtocomercial t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.idDtoComercialDetalle
//		and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona
//		and t.fecha>='2013-03-19' and t.fecha<='2014/03/19' order by t.fecha

		Query consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
				"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto from "+
				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
				"and t.fecha>='"+FechaI+"' and t.fecha<='"+FechaF+"' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" order by t.fecha");
				Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
*/
	



	public Iterator listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax, String idProveedor, String idProducto)
		{
			session = HibernateUtil.getSessionFactory().openSession();
//			select td.tblproducto.idProducto, td.desProducto,td.cantidad,td.precioUnitario, pro.stock, 
//			pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha) from 
//			Tbldtocomercialdetalle td,Tbldtocomercial t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.idDtoComercialDetalle
//			and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona
//			and t.fecha>='2013-03-19' and t.fecha<='2014/03/19' 
			//and p.nombres like 'a%' and p.apellidos like '%' and td.desProducto like 'p%' order by t.fecha
			Query consulta=null;
			if(idProveedor.equals("") && idProducto.equals("")){
			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"/*pro.impuesto,*/
					
					+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
					+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
					+ " and pro.idProducto=pr.idProducto) as impuesto, "
					
					+" td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
							+ "where (t.tipoTransaccion='1' "
							+ "or t.tipoTransaccion='10')"
					+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					//esta liena de abajo se aumento
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"order by t.fecha");

			}else if(idProveedor.equals("") && !idProducto.equals("")){
				consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
						//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
						"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"/*pro.impuesto,*/
						
						+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
						+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
						+ " and pro.idProducto=pr.idProducto) as impuesto, "
												
						+"td.departamento from "+
						"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
								+ "where (t.tipoTransaccion='1' "
								+ "or t.tipoTransaccion='10')"
						+ "and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
						"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
						//esta liena de abajo se aumento
						"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
						"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
						"and pro.idProducto = '"+idProducto+"' order by t.fecha");
				
			}else if(!idProveedor.equals("") && idProducto.equals("")){
				consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
						//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
						"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"/*pro.impuesto,*/
						
						+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
						+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
						+ " and pro.idProducto=pr.idProducto) as impuesto, "
						
						+"td.departamento from "+
						"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
								+ "where (t.tipoTransaccion='1' "
								+ "or t.tipoTransaccion='10')"
						+ "and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
						"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
						//esta liena de abajo se aumento
						"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
						"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
						"and p.cedulaRuc = '"+idProveedor+"' order by t.fecha");
				
			}else if(!idProveedor.equals("") && !idProducto.equals("")){
			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"/*pro.impuesto,*/
					
					+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
					+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
					+ " and pro.idProducto=pr.idProducto) as impuesto, "
					
					+"td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
							+ "where (t.tipoTransaccion='1' "
							+ "or t.tipoTransaccion='10')"
					+ "and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					//esta liena de abajo se aumento
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"and p.cedulaRuc = '"+idProveedor+"' and pro.idProducto = '"+idProducto+"' order by t.fecha");
			}
			System.out.println(consulta);
			/*
			 Query consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"ttp.porcentaje, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					//esta liena de abajo se aumento
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"and p.nombres like '"+nombres+"%' and p.apellidos like '"+apellidos+"%' and pro.idProducto = '"+productos+"%' order by t.fecha")
			 */
			Iterator itemBidMaps = consulta.list().iterator();
			session.close();
			return itemBidMaps;
		}
	
	public Iterator listarProductosCliente(String FechaI, String FechaF,int StockMin, int StockMax, String idCliente, String idProducto)
	{
		session = HibernateUtil.getSessionFactory().openSession();
//		select td.tblproducto.idProducto, td.desProducto,td.cantidad,td.precioUnitario, pro.stock, 
//		pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha) from 
//		Tbldtocomercialdetalle td,Tbldtocomercial t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.idDtoComercialDetalle
//		and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona
//		and t.fecha>='2013-03-19' and t.fecha<='2014/03/19' 
		//and p.nombres like 'a%' and p.apellidos like '%' and td.desProducto like 'p%' order by t.fecha
		Query consulta=null;
		if(idCliente.equals("") && idProducto.equals("")){
//		consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
//				//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
//				"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),pro.impuesto,td.departamento from "+
//				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
//						+ "where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
//						+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
//				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
//				//esta liena de abajo se aumento
//				"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
//				"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
//				"order by t.fecha");
		
		consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
				//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
				"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"
//				+ "pro.impuesto,"
				+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
				+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
				+ " and pro.idProducto=pr.idProducto) as impuesto, "
				+ "td.departamento from "+
				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
				//+"TblproductoMultiImpuesto proMul"
						+ "where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
						+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
				//esta liena de abajo se aumento
				"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
				"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
				"order by t.fecha");

		}else if(idCliente.equals("") && !idProducto.equals("")){
//			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
//					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
//					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),pro.impuesto,td.departamento from "+
//					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
//							+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
//					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
//					//esta liena de abajo se aumento
//					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
//					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
//					"and pro.idProducto = '"+idProducto+"' order by t.fecha");
			
			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"
//					+ "pro.impuesto,"
					+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
					+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
					+ " and pro.idProducto=pr.idProducto) as impuesto, "
					+ "td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p "
							+ "where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
							+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"and pro.idProducto = '"+idProducto+"' order by t.fecha");
			
		}else if(!idCliente.equals("") && idProducto.equals("")){
//			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
//					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
//					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),pro.impuesto,td.departamento from "+
//					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
//							+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
//					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
//					//esta liena de abajo se aumento
//					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
//					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
//					"and p.cedulaRuc = '"+idCliente+"' order by t.fecha");
			
			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"
//					+ "pro.impuesto,"
					+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
					+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
					+ " and pro.idProducto=pr.idProducto) as impuesto, "
					+ "td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
							+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					//esta liena de abajo se aumento
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"and p.cedulaRuc = '"+idCliente+"' order by t.fecha");
			
		}else if(!idCliente.equals("") && !idProducto.equals("")){
//		consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
//				//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
//				"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),pro.impuesto,td.departamento from "+
//				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
//						+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
//				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
//				//esta liena de abajo se aumento
//				"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
//				"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
//				"and p.cedulaRuc = '"+idCliente+"' and pro.idProducto = '"+idProducto+"' order by t.fecha");
			
			
			consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
					//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
					"ttp.porcentaje, p.idPersona,p.nombreComercial, p.razonSocial, date(t.fecha),"
		//					+ "pro.impuesto,"
					+"(select GROUP_CONCAT(mi.porcentaje) from TblmultiImpuesto mi, TblproductoMultiImpuesto pmi, Tblproducto pr"
					+ " where mi.idmultiImpuesto=pmi.tblmultiImpuesto.idmultiImpuesto and pmi.tblproducto.idProducto=pr.idProducto"
					+ " and pro.idProducto=pr.idProducto) as impuesto, "
					+ "td.departamento from "+
					"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where (t.tipoTransaccion='0' or t.tipoTransaccion='2')"
							+ " and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
					"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
					//esta liena de abajo se aumento
					"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=2 "+
					"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
					"and p.cedulaRuc = '"+idCliente+"' and pro.idProducto = '"+idProducto+"' order by t.fecha");
		}
		System.out.println(consulta);
		/*
		 Query consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
				//"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
				"ttp.porcentaje, p.idPersona,p.nombres, p.apellidos, date(t.fecha),pro.impuesto,td.departamento from "+
				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, TblproductoTipoprecio ttp, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
				//esta liena de abajo se aumento
				"and ttp.id.idProducto=pro.idProducto and ttp.id.idTipoPrecio=1 "+
				"and t.fecha>='"+FechaI+" 00:00:00' and t.fecha<='"+FechaF+" 23:59:59' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" " +
				"and p.nombres like '"+nombres+"%' and p.apellidos like '"+apellidos+"%' and pro.idProducto = '"+productos+"%' order by t.fecha")
		 */
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}

	
	public Iterator listarRetenciones(String FechaI, String FechaF,int tipoTransaccion, String ruc, String nombres,String razonSocial)
	{
//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
//tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido) "+  

		session = HibernateUtil.getSessionFactory().openSession();
//		Query consulta= session.createQuery("select tp.idPersona,tp.nombres,tp.razonSocial,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
		Query consulta= session.createQuery("select tp.idPersona,tp.nombreComercial,tp.razonSocial,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
				"tr.idRetencion,tr.autorizacionSri,tr.numRealRetencion,sum(tr.valorRetenido),tp.cedulaRuc "+  
				"from Tblretencion tr, "+domainClass.getName()+" tc, Tblpersona tp "+
//				"where tr.tbldtocomercial.idDtoComercial=tc.idDtoComercial"
				"where tr.tbldtocomercialByIdFactura.idDtoComercial=tc.idDtoComercial"
				+" and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+ 
//				"and tp.cedulaRuc like '"+ruc+"%' and tp.nombres like '"+nombres+"%' and tp.razonSocial like '"+razonSocial+"%' and tc.fecha>='"+FechaI+" 00:00:00' and tc.fecha<='"+FechaF+" 23:59:59' and tc.tipoTransaccion = "+tipoTransaccion+" group by tc.idDtoComercial");
				"and tp.cedulaRuc like '"+ruc+"%' and tp.nombreComercial like '"+nombres+"%' and tp.razonSocial like '"+razonSocial+"%' and tc.fecha>='"+FechaI+" 00:00:00' and tc.fecha<='"+FechaF+" 23:59:59' and tc.tipoTransaccion = "+tipoTransaccion+" group by tc.idDtoComercial");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
//+++++++++++++   PARA LAS RETENCIONES Q SE HACEN POR SEPARADO DESDE EL MENU RETENCIONES   ++++++++++++++
	public Iterator listarRetencionesIndividuales(String FechaI, String FechaF,int tipoTransaccion, String ruc, String nombres,String razonSocial)
	{
//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
//tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido) "+  

		session = HibernateUtil.getSessionFactory().openSession();
//		Query consulta= session.createQuery("select tp.idPersona,tp.nombres,tp.razonSocial,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
		Query consulta= session.createQuery("select tp.idPersona,tp.nombreComercial,tp.razonSocial,tp.direccion,date(tc.fecha),tc.idDtoComercial,tc.tipoTransaccion,"+
				"tr.idRetencion,tr.autorizacionSri,tr.numRealRetencion,sum(tr.valorRetenido),tp.cedulaRuc "+  
				"from Tblretencion tr, "+domainClass.getName()+" tc, Tblpersona tp "+
//				"where tr.idFactura=tc.idDtoComercial and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+
				"where tr.tbldtocomercialByIdDtoComercial.idDtoComercial=tc.idDtoComercial"
				+ " and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+
				"and tp.cedulaRuc like '"+ruc+"%' and tp.nombreComercial like '"+nombres+"%' and tp.razonSocial like '"+razonSocial+"%' and tc.fecha>='"+FechaI+" 00:00:00' and tc.fecha<='"+FechaF+" 23:59:59' and tc.tipoTransaccion = "+tipoTransaccion+" group by tc.idDtoComercial");
//				"and tp.cedulaRuc like '"+ruc+"%' and tp.nombres like '"+nombres+"%' and tp.razonSocial like '"+razonSocial+"%' and tc.fecha>='"+FechaI+" 00:00:00' and tc.fecha<='"+FechaF+" 23:59:59' and tc.tipoTransaccion = "+tipoTransaccion+" group by tc.idDtoComercial");
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}

	
	public Iterator listarRetTotales(int tipoTransaccion, int idImpuesto, String ruc, String nombres, String razonSocial,String fechaI,String fechaF)
	{
		//select tc.tipoTransaccion,tr.tblimpuesto.idImpuesto,sum(tr.valorRetenido) 
		//from Tblretencion tr, Tbldtocomercial tc, Tblpersona tp 
		//where tr.tbldtocomercial.idDtoComercial=tc.idDtoComercial and 
		//tc.tblpersonaByIdPersona.idPersona=tp.idPersona  and tc.tipoTransaccion=0 
		//and tr.tblimpuesto.idImpuesto=3 and tc.fecha>='2012/03/19' and tc.fecha<='2014/03/19' 
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=session.createQuery("select tc.tipoTransaccion,tr.tblimpuesto.idImpuesto,sum(tr.valorRetenido) "+  
				"from Tblretencion tr, "+domainClass.getName()+" tc, Tblpersona tp "+
				"where tr.tbldtocomercialByIdFactura.idDtoComercial=tc.idDtoComercial and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+ 
//				"where tr.tbldtocomercial.idDtoComercial=tc.idDtoComercial and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+ 
				"and tc.tipoTransaccion="+tipoTransaccion+" and tr.tblimpuesto.idImpuesto="+idImpuesto+
//				" and tc.tblpersonaByIdPersona.nombres like '"+nombres+"%' and tc.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' and tc.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%'"+
				" and tc.tblpersonaByIdPersona.nombreComercial like '"+nombres+"%' and tc.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' and tc.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%'"+
				" and tc.fecha>='"+fechaI+" 00:00:00' and tc.fecha<='"+fechaF+" 23:59:59'");				
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
	
//++++++++++++++++++   PARA LAS RETENCIONES TOTALES INDIVIDUALES   +++++++++++++++++++++++++
	
	public Iterator listarRetTotalesIndividuales(int tipoTransaccion, int idImpuesto, String ruc, String nombres, String razonSocial,String fechaI,String fechaF)
	{
		session = HibernateUtil.getSessionFactory().openSession();
		Query consulta=session.createQuery("select tc.tipoTransaccion,tr.tblimpuesto.idImpuesto,sum(tr.valorRetenido) "+  
				"from Tblretencion tr, "+domainClass.getName()+" tc, Tblpersona tp "+
//				"where tr.idFactura=tc.idDtoComercial and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+ 
				"where tr.tbldtocomercialByIdDtoComercial.idDtoComercial=tc.idDtoComercial and tc.tblpersonaByIdPersona.idPersona=tp.idPersona and tc.estado='1' "+ 
				"and tc.tipoTransaccion="+tipoTransaccion+" and tr.tblimpuesto.idImpuesto="+idImpuesto+
				" and tc.tblpersonaByIdPersona.nombreComercial like '"+nombres+"%' and tc.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' and tc.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%'"+
//				" and tc.tblpersonaByIdPersona.nombres like '"+nombres+"%' and tc.tblpersonaByIdPersona.razonSocial like '"+razonSocial+"%' and tc.tblpersonaByIdPersona.cedulaRuc like '"+ruc+"%'"+
				" and tc.fecha>='"+fechaI+" 00:00:00' and tc.fecha<='"+fechaF+" 23:59:59'");				
		Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}

	
	/*public Iterator listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax)
	{
		session = HibernateUtil.getSessionFactory().openSession();
//		select td.tblproducto.idProducto, td.desProducto,td.cantidad,td.precioUnitario, pro.stock, 
//		pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha) from 
//		Tbldtocomercialdetalle td,Tbldtocomercial t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.idDtoComercialDetalle
//		and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona
//		and t.fecha>='2013-03-19' and t.fecha<='2014/03/19' order by t.fecha

		Query consulta = session.createQuery("select td.tblproducto.idProducto, td.desProducto,td.cantidad, pro.stock,td.precioUnitario, "+
				"pro.promedio, p.idPersona,p.nombres, p.apellidos, date(t.fecha) from "+
				"Tbldtocomercialdetalle td, "+domainClass.getName()+" t, Tblproducto pro, Tblpersona p where t.tipoTransaccion='1' and t.idDtoComercial=td.tbldtocomercial.idDtoComercial "+
				"and td.tblproducto.idProducto=pro.idProducto and t.tblpersonaByIdPersona.idPersona = p.idPersona "+
				"and t.fecha>='"+FechaI+"' and t.fecha<='"+FechaF+"' and pro.stock >="+StockMin+" and pro.stock<="+StockMax+" order by t.fecha");
				Iterator itemBidMaps = consulta.list().iterator();
		session.close();
		return itemBidMaps;
	}
*/ 
	/**
	  * Metodo retorna el total de la retención por factura
	  * @param idFactura
	  * @param TipoImpuesto 0 si el impuesto es IVA y 1 si es RENTA
	  * @return Double
	  */
	 public double RetencionPorFactura(String idFactura,String TipoImpuesto) {
	  double retencion=0.0;
	  session = HibernateUtil.getSessionFactory().openSession();
	  //system.out.println("select sum(valorRetenido) from Tblretencion r where r.idFactura='"+idFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'");
	  Object count =  session.createQuery(
//	    "select sum(valorRetenido) from Tblretencion r where r.idFactura='"+idFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'").uniqueResult();
		"select sum(valorRetenido) from Tblretencion r where r.tbldtocomercialByIdFactura.idDtoComercial='"+idFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'").uniqueResult();
	  //session.getTransaction().commit();
	  session.close();
	  try{
	   retencion=Double.parseDouble(String.valueOf(count));
	   return retencion;
	  }catch(Exception e){
	   retencion=0.0;
	   return retencion;
	  }
	 }
	 public double RetencionPorTblFactura(String TbldtoFactura,String TipoImpuesto) {
	  double retencion=0.0;
	  session = HibernateUtil.getSessionFactory().openSession();
	  //system.out.println("select sum(valorRetenido) from Tblretencion r where r.tbldtocomercial='"+TbldtoFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'");
	  Object count =  session.createQuery(
//	    "select sum(valorRetenido) from Tblretencion r where r.tbldtocomercial='"+TbldtoFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'").uniqueResult();
			  "select sum(valorRetenido) from Tblretencion r where r.tbldtocomercialByIdFactura='"+TbldtoFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'").uniqueResult();
	  //session.getTransaction().commit();
	  System.out.println( "select sum(valorRetenido) from Tblretencion r where r.tbldtocomercialByIdFactura='"+TbldtoFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'");
	  session.close();
	  try{
	   retencion=Double.parseDouble(String.valueOf(count));
	   return retencion;
	  }catch(Exception e){
	   retencion=0.0;
	   return retencion;
	  }
	 }
	 public String NumRetencionPorTblFactura(String TbldtoFactura) {
		  String numRetencion="";
		  session = HibernateUtil.getSessionFactory().openSession();
		  //system.out.println("select sum(valorRetenido) from Tblretencion r where r.tbldtocomercial='"+TbldtoFactura+"' and r.tblimpuesto.tipo='"+TipoImpuesto+"'");
		  Object count =  session.createQuery(
//		    "select numRealRetencion from Tblretencion r where r.tbldtocomercial='"+TbldtoFactura+"'").setMaxResults(1).uniqueResult();
			"select numRealRetencion from Tblretencion r where r.tbldtocomercialByIdFactura='"+TbldtoFactura+"'").setMaxResults(1).uniqueResult();
		  //session.getTransaction().commit();
		  session.close();
		  try{
			  numRetencion=String.valueOf(count);
		   return numRetencion;
		  }catch(Exception e){
			  numRetencion="";
		   return numRetencion;
		  }
		 }
	 public T findByNombrePuntos(String idCliente) {//Para buscar el cliente al cual asignarle los puntos
		  session = HibernateUtil.getSessionFactory().openSession();
		  Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.idCliente ="  +"'"+idCliente+"'");
		  //Le pasamos el registro desde el cual va a extraer
		  consulta.setFirstResult(0);
		  //Le pasamos el numero maximo de registros, paginacion.
		  consulta.setMaxResults(1);
		  List<T> returnList = consulta.list();
		  session.close();
		  return returnList.get(0);
	 }
	
	 public void grabarPAnticipos(LinkedList<T> t) throws ErrorAlGrabar {
			//system.out.println("madre entre al grabarPAnticipos "+t.size());	
			 Iterator iterDocumentos;
				iterDocumentos = t.iterator();
				TbldtocomercialTbltipopago pago;
				try {
			 		session = HibernateUtil.getSessionFactory().openSession();
			 		session.beginTransaction();
			 		for(int i=0;i<t.size();i++)
			 		{	
			 			//system.out.println("madre "+i);
			 			
			 			//Vamos a verificar que no hayan numeros duplicados
			 			Tbldtocomercial dto = (Tbldtocomercial) t.get(i);
			 			//system.out.println("madre "+i+" valor "+dto.getSubtotal());
				 		if(i==t.size()-1)
				 		{	
					 		Integer numReal = 1;
					 //		Boolean unico = true;
					 		
					 	//	while(unico)
					 	//	{
					 			//Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.numRealTransaccion = " +"'"+numReal+"'"+
					 				//	"and u.tipoTransaccion ="+"'"+dto.getTipoTransaccion()+"'");
					 			Query consulta = session.createQuery("SELECT max(t.numRealTransaccion) from Tbldtocomercial t where t.tipoTransaccion="+dto.getTipoTransaccion());
					 			consulta.setFirstResult(0);
				 				  //Le pasamos el numero maximo de registros, paginacion.
				 				  consulta.setMaxResults(1);
				 				  
					 			if(!consulta.list().isEmpty())
						 		{
					 				  numReal = Integer.valueOf(consulta.list().get(0).toString())+1;
					 			//	  session.close();
						 		}
					 	/*		else{
						 			unico=false;
						 			break;
						 		}*/
					 			dto.setNumRealTransaccion(numReal);
					 		//system.out.print("El numReal es"+numReal);
				 		}	
				 		
				 		session.saveOrUpdate(dto); 
			 		}	
				 		session.getTransaction().commit();
			 	} catch (HibernateException e) {
			 		session.getTransaction().rollback();
			 		throw new ErrorAlGrabar(e.getMessage());
			 	} finally {
			 		if(session!=null)
			 		session.close();
			 	}
			}
	 
	 public int ATSVentasNumComprobantes(Integer idPersona, String fechai, String fechaf){
			String consulta="";
			int num;
			try {
				session = HibernateUtil.getSessionFactory().openSession();
				consulta="select count(idDtoComercial) " +
			      "from Tbldtocomercial u where u.fecha>='"+fechai+" 00:00:00' " +
			      "and u.fecha<='"+fechaf+" 23:59:59' and u.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.estado='1' " +
			      "and  (u.tipoTransaccion='0' ) and u.estado=1";
				//System.out.println(consulta);
			    Object count =  session.createQuery(consulta).uniqueResult();
			   
			    session.close();
			    num=(Integer.valueOf(count.toString()));
		   }catch (Exception e){
			   num=0;
			   //System.out.println(e.getMessage());
		   }
		   return num;
			  
			
		}
		/**
		 * Metodo que permite obtener un  valor totalizado de  las retenciones IVA por cliente
		 * @param idPersona 
		 * @param fechaf 
		 * @param fechai 
		 * @return float BaseImponible
		 */ 
		public Float ATSVentasRetencionesIVA(Integer idPersona, String fechai, String fechaf){
			String consulta="";
			float retIva=0;
			try {
				session = HibernateUtil.getSessionFactory().openSession();
				consulta="select sum(valorRetenido) from Tblretencion r where r.tbldtocomercialByIdFactura.estado='1' and r.tblimpuesto.tipo='0' " +
//				consulta="select sum(valorRetenido) from Tblretencion r where r.tbldtocomercial.estado='1' and r.tblimpuesto.tipo='0' " +
					"and r.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
					"and r.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' " +
					"and r.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"'"+
					"and (r.tbldtocomercial.tipoTransaccion='0' or r.tbldtocomercial.tipoTransaccion='5')";
				Object count =  session.createQuery(consulta).uniqueResult();
			    //System.out.println(consulta);
			    session.close();
			    retIva=(Float.valueOf(count.toString()));
			}catch (Exception e){
				retIva=0;
				//System.out.println(e.getMessage());
			}
			return retIva;
		}
	
		public Float ATSVentasClienteBaseImponible(Integer idPersona, String fechai, String fechaf){
			  String consulta="";
			  float facturas=0;
			  float notas=0;
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				         consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='0') and u.impuesto=0 and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				         
				   facturas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  facturas=0;
				  //System.out.println(e.getMessage());
			  }
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				         consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='3') and u.impuesto=0 and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				         
				   notas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  notas=0;
				  //System.out.println(e.getMessage());
			  } 
			  return facturas-notas;
		 }
		/**
		 * Metodo que permite obtener un  valor totalizado de  vBaseImpGrav por cliente
		 * @param idPersona 
		 * @param fechaf 
		 * @param fechai 
		 * @return float BaseImpGrav
		 */ 
		public Float ATSVentasClienteBaseImpGrav(Integer idPersona, String fechai, String fechaf, double IVA){
			  String consulta="";
			  float facturas=0;
			  float notas=0;
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				   consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='0') and u.impuesto="+IVA+" and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				   facturas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  facturas=0;
				  //System.out.println(e.getMessage());
			  } 
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				   consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='3') and u.impuesto="+IVA+" and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				   notas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  notas=0;
				  //System.out.println(e.getMessage());
			  } 
			  return facturas-notas; 
		 }
		/**
		 * Metodo que permite obtener un  valor totalizado de  vBaseImpGrav por cliente
		 * @param idPersona 
		 * @param fechaf 
		 * @param fechai 
		 * @return float BaseImpGrav
		 */ 
		public Float ATSVentasClienteBaseImpNoGrav(Integer idPersona, String fechai, String fechaf){
			  String consulta="";
			  float facturas=0;
			  float notas=0;
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				   consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='0') and u.impuesto=0 and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				   facturas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  facturas=0;
				  //System.out.println(e.getMessage());
			  } 
			  try {
				   session = HibernateUtil.getSessionFactory().openSession();
				   consulta="select sum(total) " +
				      "from Tbldtocomercialdetalle u where u.tbldtocomercial.fecha>='"+fechai+" 00:00:00' " +
				      "and u.tbldtocomercial.fecha<='"+fechaf+" 23:59:59' and u.tbldtocomercial.tblpersonaByIdPersona='"+String.valueOf(idPersona)+"' and u.tbldtocomercial.estado='1' " +
				      "and  (u.tbldtocomercial.tipoTransaccion='3') and u.impuesto= and u.tbldtocomercial.estado=1";
				   Object count =  session.createQuery(consulta).uniqueResult();
				   //System.out.println(consulta);
				   session.close();
				   notas=(Float.valueOf(count.toString()));
			  }catch (Exception e){
				  notas=0;
				  //System.out.println(e.getMessage());
			  } 
			  return facturas-notas;
		 }

		public int grabaryretornar(T t) throws ErrorAlGrabar {
			int id;
		 	try {
		 		session = HibernateUtil.getSessionFactory().openSession();
		 		session.beginTransaction();
		 		id=(Integer)session.save(t);
		 		session.getTransaction().commit();
		 		return id;
		 	} catch (HibernateException e) {
		 		session.getTransaction().rollback();
		 		throw new ErrorAlGrabar(e.getMessage());
		 	} finally {
		 		if(session!=null)
			 	session.close();
		 		
		 	}
		 		
		 	
		}
		
		public Iterator getList(String query) {		
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x "+query);
			System.out.println(consulta);
			System.out.println("TAMAÑO DE LA LISTA: "+consulta.list());
			//Le pasamos el registro desde el cual va a extraer
			//consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			//consulta.setMaxResults(NumReg);
			Iterator returnList = consulta.list().iterator();
			session.close();
			return returnList;
		}
		public Iterator getList2(String query) {		
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x join x.tblpagos p where p.fechaVencimiento ='"+query+"' and x.idDtoComercial = p.tbldtocomercial.idDtoComercial");
			System.out.println(consulta);
			System.out.println("TAMAÑO DE LA LISTA: "+consulta.list());
			Iterator returnList = consulta.list().iterator();
			session.close();
			return returnList;
		
		}
		//##########################################################################
		public List<T> findByIdProductoHijos (String id) {
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.tblproductoByIdProductoPadre = " +id);
			//Le pasamos el registro desde el cual va a extraer
			//consulta.setFirstResult(0);//TOMAMOS EL PRIMER REGISTRO OBTENIDO
			//Le pasamos el numero maximo de registros, paginacion.
			//consulta.setMaxResults(1);//DAMOS EL MAXIMO DE 1 REGISTRO
			System.out.println(consulta);
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		public List<T> listarDtoComercial(String FechaI, String FechaF, String TipoTransaccion){
			session = HibernateUtil.getSessionFactory().openSession();
			System.out.println("PARAMETRSO DE ENTRADA CONSLUTA DOCUMENTOS:  "+FechaI +"-"+FechaF+"-"+TipoTransaccion);
			Query consulta=session.createQuery("from "+domainClass.getName()+" u where u.estado='1' and u.fecha>='"+FechaI+" 00:00:00' and u.fecha<='"+FechaF+" 23:59:59' and u.tipoTransaccion="+TipoTransaccion);
			System.out.println(consulta);
			List<T> returnList = consulta.list();
			System.out.println("TAMANIO DE LA LISTA DE DOCUMENTOS:  "+returnList.size());
			session.close();
			return returnList;
		}	

		public Iterator listByQuery(String query) {
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta=session.createQuery(query);		
			System.out.println(consulta);
			Iterator itemBidMaps = consulta.list().iterator();
			session.close();
			return itemBidMaps;
		}
			
		/*
		 * PARAMETROS PARA GENERAR REPORTE DE LISTADO DE DTOELECTRONICO
		 */
		public List<T> ReporteElectronico(String fechaI,String fechaF,String Tipo) {
			session = HibernateUtil.getSessionFactory().openSession();
		//	System.out.println("from "+domainClass.getName()+" u where u.fecha>='"+fechaI+"' and u.fecha<='"+fechaF+"' and TipoTransaccion='"+Tipo+"'" );
			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u.fechaautorizacion>='"+fechaI+" 00:00:00' and u.fechaautorizacion<='"+fechaF+" 23:59:59' and u.tbldtocomercial.tipoTransaccion='"+Tipo+"' order by fechaautorizacion" );
				//"by u.tblpersonaByIdPersona.apellidos, u.tblpersonaByIdPersona.nombres, u.tblpersonaByIdPersona.cedulaRuc" );
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		
		public List<T> getListProMultImp(Integer idProducto) {		
			session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x where x.tblproducto.idProducto='"+idProducto+"'");
			//Le pasamos el registro desde el cual va a extraer
			//consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			//consulta.setMaxResults(/*NumReg*/20);
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		
		public T findByCampo(String valor,String campo) {
			session = HibernateUtil.getSessionFactory().openSession();
			System.out.println("en findbycampo");
			Query consulta = session.createQuery("from "+domainClass.getName()+" u where u."+campo+" = '"+valor+"'");
			System.out.println(consulta);
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			System.out.println("First result 0 en findbynombret");
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			System.out.println("First result 1 en findbynombret");
			List<T> returnList = consulta.list();
			System.out.println("Return list");
			//session.close();
			if (!returnList.isEmpty())
				return returnList.get(0);
			else
				return null;
		}
}
