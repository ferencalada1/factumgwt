package com.giga.factum.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RIDEServlet extends HttpServlet{
	 private final String repository = "FirmaDigital";
	 private String path = "...";

	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
			//ServletContext servletContext = getServletContext();
			// the file data.xls is under web application folder
	    	//String contextPath = new File("C:/").getCanonicalPath();
	    	//String contextPath = new File("/").getCanonicalPath();
			//String[] contextPaths=contextPath.split(File.separator);
			
			//contextPath=contextPaths[0]+File.separator;
	    	String contextPath ="";
			
			//String contextPath = servletContext.getRealPath(File.separator);	    	
	        String fileName = request.getParameter("fileName");
	        String typeDoc = request.getParameter("typeDoc");
	        String SO = request.getParameter("SO");
	        
	        if(SO.equals("windows")){
	        	contextPath = new File("C:/").getCanonicalPath();
		    }else if(SO.equals("linux")){
		    	contextPath = new File("/").getCanonicalPath();
		    }
	        String tipoDoc="";
	        // Security: '..' in the filename will let sneaky users access files
	        // not in your repository.
	        //filename = filename.replace("..", "");

	        
	        if(typeDoc.equals("0")){//factura
	        	tipoDoc="Facturas";
	        }else if(typeDoc.equals("1")){
	        	tipoDoc="Retenciones";
	        }else if(typeDoc.equals("3")){
	        	tipoDoc="NotasCredito";
	        }
	        //File file = new File(contextPath+"PATH1"+contextPath2+"PATH2"+contextPath3+"PATH3"+contextPath4+"PATH4"+repository + fileName);
	        String path =contextPath+File.separator+repository+File.separator+"RIDE"+File.separator+tipoDoc+File.separator+ fileName;
	        File file = new File(path);
	        if (!file.exists()){
	        	System.out.println("DIRECCION DESCARGA DEL RIDE: "+repository+"FirmaDigital"+File.separator+"RIDE"+File.separator+tipoDoc+File.separator+ fileName);
		        PrintWriter out = response.getWriter();  
		        response.setContentType("text/html");
		        out.println("<b>"+"DIRECCION DESCARGA DEL RIDE: "+"</b>");
		        out.println("<b>"+ file.getAbsolutePath() +"</b>");
	            throw new FileNotFoundException(file.getAbsolutePath()+"-"+path);
	        }

	        response.setHeader("Content-Type","application/pdf");
	        response.setHeader("Content-Length", String.valueOf(file.length()));
	        response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");

	        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
	        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
	        byte[] buf = new byte[1024];
	        while (true) {
	            int length = bis.read(buf);
	            if (length == -1)
	                break;

	            bos.write(buf, 0, length);
	        }
	        bos.flush();
	        bos.close();
	        bis.close();
	    }

}
