package com.giga.factum.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.LinkedList;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ReportePDF {
	
	public ReportePDF(){
		
	}
	
	public void crearReporte(LinkedList<String[]> listado, String pathpdf, String pathimagen, String pathlogo,String[] atributos,String NombreEmpresa,String TelefonoCelularEmpresa,String TelefonoConvencionalEmpresa, String DireccionEmpresa, String Logo) throws DocumentException, MalformedURLException, IOException{
		//System.out.println("Llego a crear el PDF"+pathpdf+"-"+pathimagen);
		
		OutputStream file = new FileOutputStream(new File(pathpdf+File.separatorChar+"ReporteProductos.pdf"));

		com.itextpdf.text.Font myfontsimplepequeno= FontFactory.getFont("arial", 10, BaseColor.BLACK);
		com.itextpdf.text.Font myfontsimplepequeno2= FontFactory.getFont("arial", 10,Font.BOLD, BaseColor.BLACK);	    
	    //CREAMOS UN FUENTE CON ESTILO NEGRITA	   	
		
		com.itextpdf.text.Font myfontstyle= FontFactory.getFont("arial", 11, Font.BOLD, BaseColor.BLACK);
	    com.itextpdf.text.Font titlefontstyle= FontFactory.getFont("arial", 14, Font.ITALIC, BaseColor.BLACK);
	    com.itextpdf.text.Font subtitlefontstyle= FontFactory.getFont("arial", 12, Font.ITALIC, BaseColor.BLACK);
		Document document = new Document(PageSize.LETTER,5,5,20,20);
		PdfWriter.getInstance(document, file);
		document.open();
		
		 //tabla de columnas del producto
		//PdfPTable tablecuerpo = new PdfPTable(atributos.length);	//se quita +1	y se elimina el if del listado - linea 300
		PdfPTable tablecuerpo = new PdfPTable(2);//numero de columnas
		tablecuerpo.setWidthPercentage(100);
		tablecuerpo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
		tablecuerpo.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		
		PdfPTable tablecuerpo2 = new PdfPTable(1);		
		tablecuerpo2.setWidthPercentage(100);
		tablecuerpo2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
		tablecuerpo2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		PdfPTable tablecuerpo3 = new PdfPTable(2);		
		tablecuerpo3.setWidthPercentage(100);
		tablecuerpo3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
		tablecuerpo3.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		PdfPTable tableprincipal = new PdfPTable(1);
		tablecuerpo.getDefaultCell().setBorder(0);
		tableprincipal.setWidthPercentage(100);
		tableprincipal.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
		tableprincipal.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
				
		PdfPCell cell;
		PdfPTable tabletodo = new PdfPTable(2);
		Paragraph parrafo;
		Paragraph campo;
						
		PdfPTable tablecabecera = new PdfPTable(2);
		PdfPTable tabledatos = new PdfPTable(1);
		tabledatos.getDefaultCell().setBorder(0);
		
		parrafo = new Paragraph(new Paragraph(NombreEmpresa, titlefontstyle));         // color  
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);            
        cell = new PdfPCell(parrafo);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(atributos.length+1);
        cell.setBorder(Rectangle.NO_BORDER); 
        tabledatos.addCell(cell);
        
        parrafo = new Paragraph(new Paragraph("TELIFONO: "+TelefonoCelularEmpresa+"/"+TelefonoConvencionalEmpresa, subtitlefontstyle));         // color  
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);            
        cell = new PdfPCell(parrafo);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(atributos.length+1);
        cell.setBorderWidth(0);
        tabledatos.addCell(cell);
        
        parrafo = new Paragraph(new Paragraph("DIRECCION: "+DireccionEmpresa, subtitlefontstyle));         // color  
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);            
        cell = new PdfPCell(parrafo);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(atributos.length+1);
        cell.setBorderWidth(0);
        tabledatos.addCell(cell);
        
        cell = new PdfPCell(tabledatos);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM  );
        cell.setRowspan(2);
        cell.setBorder(Rectangle.NO_BORDER);
        tablecabecera.addCell(cell); 
        
        System.out.println("PATH IMAGEN: "+pathlogo+Logo);
        Image logo = Image.getInstance(pathlogo+Logo);//CARGAMOS EL LOGO DE LA EMPRESA    
        logo.scaleAbsolute(95, 95);
        cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.addElement(logo);    
        cell.setBorder(Rectangle.NO_BORDER);
        tablecabecera.addCell(cell);
                  
        cell = new PdfPCell(tablecabecera);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM  );
        cell.setColspan(atributos.length+1);
        cell.setBorder(Rectangle.NO_BORDER);
        tablecuerpo3.addCell(cell);
                			
        boolean banderaPVP=false;
        boolean banderaAfiliado=false;
        boolean banderaMayorista=false;
        boolean banderaPromedio=false;        
        int posicionPVP=0;
        int posicionAfiliado=0;
        int posicionMayorista=0;
        int posicionPromedio=0;        
        String signo="";
		 
        //__________________________________________________________________________________________________
               
        //__________________________________________________________________________________________________
        for(int i=0; i<listado.size(); i++){ //Cantidad de las filas de las filas
        	        	        	   	
        	tablecuerpo2 = new PdfPTable(1);		
    		tablecuerpo2.setWidthPercentage(100);
    		tablecuerpo2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
    		tablecuerpo2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
    		tablecuerpo2.getDefaultCell().setBorder(0);  
    		
    		 tablecuerpo = new PdfPTable(2);		
	    	 tablecuerpo.setWidthPercentage(100);
	    	 tablecuerpo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_MIDDLE);
	    	 tablecuerpo.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	    	 tablecuerpo.getDefaultCell().setBorder(0);
    		
        	if(i<listado.size()){        
            }        	
			String fila[]=listado.get(i);
			System.out.println(Arrays.toString(fila));
			for(int j=0; j<fila.length;j++){ //length numero de campos (descrip,pvp,imagen,etc)
				System.out.println(atributos[j]+"-"+fila[j]);
				//campo = new Paragraph(new Paragraph(String.valueOf(atributos[j]), titlefontstyle)); 
				//campo.setFont(FontFactory.getFont("arial", 14, Font.BOLD, BaseColor.RED));								
				if(atributos[j].equals("Imagen"))//if de imagen
				{										
					String nombreimagen=String.valueOf(fila[j]);
					System.out.println(nombreimagen);
					Image image;					
					try {
						System.out.println(pathimagen+nombreimagen);
						if(nombreimagen.equals("") || nombreimagen.equals("null") || nombreimagen.isEmpty()|| nombreimagen.equals(null))
						{												
						     if(new File(pathimagen+"notavailible.jpg").exists())
							    {								
								System.out.println("NO INGRESADA IMAGEN");
								System.out.println(pathimagen+"notavailible.jpg");
								image = Image.getInstance(pathimagen+"notavailible.jpg");								
								image.scaleAbsolute(80,80 );
								
								for(int y = fila.length;y<10;y++)//sirve para agregar como filas a los campos vacios
								{
									cell = new PdfPCell();
						            //cell.setBackgroundColor(BaseColor.BLUE);
						            //cell.setVerticalAlignment(Element.ALIGN_MIDDLE); //bloque quitado 
								 	 cell.setBorder(Rectangle.NO_BORDER);
								 	 cell.setBorderWidth(0);
						             tablecuerpo2.addCell(cell);
								}
								
								
								//image.scaleToFit(150, 300);
								cell = new PdfPCell(tablecuerpo2);
						        cell.setVerticalAlignment(Element.ALIGN_BOTTOM  );
						        //cell.setRowspan(fila.length+1);
						        cell.setBorder(Rectangle.NO_BORDER);
						        cell.setBorderWidth(0);
						        tablecuerpo.addCell(cell);																
								cell=new PdfPCell(image);
								//cell.setFixedHeight(90f);
								cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
								cell.setPadding(5);	
								cell.setBorderWidth(0);
								tablecuerpo.addCell(cell);	
								cell = new PdfPCell(tablecuerpo);
						        tablecuerpo3.addCell(cell);
							    }
							  
							  else 
							  {		
								  
								  for(int y = fila.length;y<10;y++)
									{
										cell = new PdfPCell();
							            //cell.setBackgroundColor(BaseColor.BLUE);
							            //cell.setVerticalAlignment(Element.ALIGN_MIDDLE); //bloque quitado 
									 	 cell.setBorder(Rectangle.NO_BORDER);
									 	 cell.setBorderWidth(0);
							             tablecuerpo2.addCell(cell);
									}
								  cell = new PdfPCell(tablecuerpo2);
							      cell.setVerticalAlignment(Element.ALIGN_BOTTOM  );
							      //cell.setRowspan(fila.length+1);
							      cell.setBorder(Rectangle.NO_BORDER);							       
							      tablecuerpo.addCell(cell);								  
								  parrafo = new Paragraph(new Paragraph("No existe la imagen", myfontstyle));         // color  
							      parrafo.setAlignment(Paragraph.ALIGN_CENTER);            
							      cell = new PdfPCell(parrafo);
							      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);	
							      cell.setBorderWidth(0);
						    	  tablecuerpo.addCell(cell);	
						    	  cell = new PdfPCell(tablecuerpo);
							      tablecuerpo3.addCell(cell);
							  }						     						     
							  
						  }//fin if de imagenes no existentes
						    else {		// se establece la imagen si existe
							     System.out.println("IMAGEN INGRESADA");
							     System.out.println(pathimagen+File.separatorChar+nombreimagen);
							     image = Image.getInstance(pathimagen+nombreimagen);							     					
							     cell = new PdfPCell(tablecuerpo2);
							     cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
							     //cell.setRowspan(fila.length+1);
							     cell.setBorder(Rectangle.NO_BORDER);							       
							     tablecuerpo.addCell(cell);							     
							     image.scaleAbsolute(80, 80);
							     
							     for(int y = fila.length;y<10;y++)
									{
										cell = new PdfPCell();
							            //cell.setBackgroundColor(BaseColor.BLUE);
							            //cell.setVerticalAlignment(Element.ALIGN_MIDDLE); //bloque quitado 
									 	 cell.setBorder(Rectangle.NO_BORDER);
									 	 cell.setBorderWidth(0);
							             tablecuerpo2.addCell(cell);
									}
							     
							     
							     
							    // image.scaleToFit(150, 300);
							     cell.setBorderWidth(0);
							     cell=new PdfPCell(image);
								 //cell.setFixedHeight(90f);
								 cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								 cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
								 cell.setPadding(5);
								 cell.setBorderWidth(0);
								 tablecuerpo.addCell(cell);								 
								 cell = new PdfPCell(tablecuerpo);
							     tablecuerpo3.addCell(cell);
								 
					           }// fin else imagen existente																			    				    			    			    				    						
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}//fin if de imagen				
				else{ //else if imagen
				    //Campos (descrip, pvp, etc)
					if(atributos[j].equals("codigoBarras"))
					{
				 	parrafo = new Paragraph(new Paragraph("CSDIGO: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }					
					if(atributos[j].equals("descripcion"))
					{
				 	parrafo = new Paragraph(new Paragraph(String.valueOf(fila[j]), myfontsimplepequeno2));         // color
				    }					
					if(atributos[j].equals("PVP"))
					{
				 	parrafo = new Paragraph(new Paragraph("PVP: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
					if(atributos[j].equals("stock"))
					{
				 	parrafo = new Paragraph(new Paragraph("STOCK: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
					if(atributos[j].equals("impuesto"))
					{
				 	parrafo = new Paragraph(new Paragraph("IMPUESTO: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }					
					if(atributos[j].equals("Afiliado"))
					{
				 	parrafo = new Paragraph(new Paragraph("AFILIADO: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
					if(atributos[j].equals("Mayorista"))
					{
				 	parrafo = new Paragraph(new Paragraph("MAYORISTA: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
					if(atributos[j].equals("promedio"))
					{
				 	parrafo = new Paragraph(new Paragraph("PROMEDIO: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
					if(atributos[j].equals("Unidad"))
					{
				 	parrafo = new Paragraph(new Paragraph("UNIDAD: "+String.valueOf(fila[j]), myfontsimplepequeno));         // color
				    }
				 	signo="";
				 	parrafo.setAlignment(Paragraph.ALIGN_CENTER); 				 	
		            cell = new PdfPCell(parrafo);
		            //cell.setBackgroundColor(BaseColor.BLUE);
		            //cell.setVerticalAlignment(Element.ALIGN_MIDDLE); //bloque quitado 
				 	 cell.setBorder(Rectangle.NO_BORDER);
				 	 cell.setBorderWidth(0);
		             tablecuerpo2.addCell(cell);
		             
				}//fin else campos												
			}//fin de bucle for columnas		
			
			    /*cell = new PdfPCell(tablecuerpo);
		        tablecuerpo3.addCell(cell);*/							       
		}//fin de for principal de filas         
       /* cell = new PdfPCell(tablecuerpo);
        tableprincipal.addCell(cell);*/	
        
        cell = new PdfPCell(); // Celda vacia para que visualice el ultimo item si es impar
        tablecuerpo3.addCell(cell);
		document.add(tablecuerpo3);		 
		document.close();
		try {
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}

}
