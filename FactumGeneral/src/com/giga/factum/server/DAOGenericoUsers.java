package com.giga.factum.server;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import com.giga.factum.server.baseusuarios.Tblpermiso;
import com.giga.factum.server.baseusuarios.Tblpermisorolpestana;
import com.giga.factum.server.baseusuarios.TblpermisorolpestanaHome;
import com.giga.factum.server.baseusuarios.Tblpestanas;
import com.giga.factum.server.baseusuarios.TblpestanasHome;
import com.giga.factum.server.baseusuarios.Tblrol;
import com.mysql.jdbc.PreparedStatement;

import bsh.TargetError;

	
public abstract class DAOGenericoUsers<T> {

	public Class<T> domainClass = getDomainClass();
	private Session session;
 	/**
	  * Method to return the class of the domain object
	  */
		protected Class getDomainClass() {
	 	if (domainClass == null) {
			ParameterizedType thisType = (ParameterizedType) getClass()
 				.getGenericSuperclass();
			domainClass = (Class) thisType.getActualTypeArguments()[0];
	 	}
		return domainClass;
	}
		public T load(Integer id) {
			session = HibernateUtil.getSessionFactory().openSession();
//			session = hibernateUtilUsuarios.getSessionFactory().openSession();
			T returnValue = (T) session.load(domainClass, id);
			//session.getTransaction().commit();
			session.close();
			return returnValue;
		}
		public List<T> getList() {		
			session = HibernateUtil.getSessionFactory().openSession();
//			session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x");
			//Le pasamos el registro desde el cual va a extraer
			//consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			//consulta.setMaxResults(NumReg);
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		
		public Tblpermiso findByIdpermiso(int id) {
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from com.giga.factum.server.baseusuarios u where " +
					"u.tblpermiso.idPermiso = " +"'"+id);
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tblpermiso> returnList = consulta.list();
			//session.close();
		//	System.out.println("sise de la lista del query: "+returnList.size()); 
			return returnList.get(0);
		}

		public Tblrol findByIdRol(int id) {
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from com.giga.factum.server.baseusuarios u where " +
					"u.tblrol.idRol = " +"'"+id);
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tblrol> returnList = consulta.list();
			//session.close();
		//	System.out.println("sise de la lista del query: "+returnList.size()); 
			return returnList.get(0);
		}
		public List<T> getListCombo(Integer inicio, Integer NumReg) {		
			session = HibernateUtil.getSessionFactory().openSession();
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x");
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(NumReg);
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		public List<T> getList(Integer inicio, Integer NumReg) {		
			//session = HibernateUtil.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" x");
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(inicio);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(NumReg);
			List<T> returnList = consulta.list();
			session.close();
			return returnList;
		}
		public Tblpestanas findByIdPestana(int id) {
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from com.giga.factum.server.baseusuarios u where " +
					"u.tblpestanas.idPestanas = " +"'"+id);
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tblpestanas> returnList = consulta.list();
			//session.close();
		//	System.out.println("sise de la lista del query: "+returnList.size()); 
			return returnList.get(0);
		}
		public Tblpermisorolpestana findByIdPrp(int idNivel) {
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from com.giga.factum.server.baseusuarios u where " +
					"u.tblpermisorolpestana.tblrol.idRol = " +"'"+idNivel);
			//Le pasamos el registro desde el cual va a extraer
			consulta.setFirstResult(0);
			//Le pasamos el numero maximo de registros, paginacion.
			consulta.setMaxResults(1);
			List<Tblpermisorolpestana> returnList = consulta.list();
			//session.close();
		//	System.out.println("sise de la lista del query: "+returnList.size()); 
			return returnList.get(0);
		}
		/**
		 * Metodo para buscar en una tabla por un campo  filtrando datos 
		 * @param nombre nombre a buscar
		 * @param campo columna en la tabla
		 * @return entidad
		 */
		///********Nivel y Pesta�a
		public T findIdPrpLectura(int nivel, String pestana) {
			System.out.println("Consulta de lectura ");
			session = HibernateUtil.getSessionFactory().openSession();
			//session = hibernateUtilUsuarios.getSessionFactory().openSession();
			Query consulta = session.createQuery("from "+domainClass.getName()+" u where "
			+ "u.tblrol.idRol='"+nivel+"' and u.tblpestanas.descripcion='"+pestana+"'");
			//System.out.println("Buscar "+consulta.getQueryString());
			consulta.setFirstResult(0);
			consulta.setMaxResults(1);
			List<T> returnList = consulta.list();
			System.out.println(consulta+", "+returnList.size());
			session.close();
			T resp=(returnList.size()==0)?null:returnList.get(0);
			return resp;
		}
}
