package com.giga.factum.client;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;

public class GenerarClaveAcceso {
//	public String Generar(DtocomercialDTO dtocomercial, String tipoemision, String tipotransaccion){
	public String Generar(String fecha, String numRealTransaccion, String tipoemision, String tipotransaccion){
		String clave=null;
		String fechaemision=ObtenerFecha(fecha);//obtenemos la fecha de emision
		//System.out.println(fechaemision);
		String tipocomprobante=tipotransaccion;//obtenemos el tipo de comprobante
		//System.out.println(tipocomprobante);
		String ruc=Factum.banderaRUCEmpresa;
		//String ruc= empresa.getRUC();//obtenemos el ruc de la empresa
		//System.out.println(ruc);
		//String serie="002100";//definir la serie identificar el documento
		//MULTIEMPRESA
		////com.google.gwt.user.client.Window.alert("Llamar generar clave "+Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0));
		////com.google.gwt.user.client.Window.alert("Llamar generar clave "+Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getEstablecimiento());
		////com.google.gwt.user.client.Window.alert("Llamar generar clave "+Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision());
		PuntoEmisionDTO punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
		String serie=punto.getEstablecimiento()+punto.getPuntoemision();
		////com.google.gwt.user.client.Window.alert(serie);
		//System.out.println(serie);
		//String numerocomprobante= String.valueOf(dtocomercial.getIdDtoComercial());
		String numerocomprobante=ValidarSecuencial(String.valueOf(numRealTransaccion));//definir el numero de comprobante
		//System.out.println(numerocomprobante);
		String codigonumerico="12341234";//definir el codigo numerico
		//System.out.println(codigonumerico);
		String codigo=fechaemision+tipocomprobante+ruc+Factum.banderaAmbienteFacturacionElectronica+serie+numerocomprobante+codigonumerico+tipoemision;
		//String codigo="121220130109122382430011002003000000017123456781";
		//String codigo="250720140101021449530011001001000000018123456781";
		//String codigo="170220120517600132100011001003000100001123456781";
		//System.out.println(codigo);
		//String digitoverificador= calculateCD(codigo);
		String digitoverificador=ValidarDigitoVerificador(codigo);
		clave=codigo+digitoverificador;
		//System.out.println(clave);
		return clave;
	}
	public String ValidarSecuencial(String iddtocomercial){
		String secuencial=null;
		int longitud=iddtocomercial.length();
		int resta=9-longitud;
		for (int i=0; i<resta; i++){
			iddtocomercial="0"+iddtocomercial;
		}
		secuencial=iddtocomercial;
		return secuencial;

	}
	public String ObtenerFecha(String fecha){
		//System.out.println("FECHA ANTES: "+fecha);
//		String [] fechahora = fecha.split(" ");
//		String [] fechacompleta = fechahora[0].split("-");
		String [] fechacompleta = fecha.split("/");
		//System.out.println("FECHA MEDIO: "+fechacompleta);
		String anio=fechacompleta[0];
		String mes =(fechacompleta[1].length()==2)?fechacompleta[1]:"0"+fechacompleta[1];
		String dia=(fechacompleta[2].length()==2)?fechacompleta[2]:"0"+fechacompleta[2];
		String cadenafecha=null;
		cadenafecha=dia+mes+anio;	
		//System.out.println("FECHA FINAL: "+cadenafecha);
		return cadenafecha;
	}

	public String ValidarTipoTransaccion(int transaccion){
		String tipotransaccion=null;
		switch(transaccion){
		case 0:
			tipotransaccion="01";
			break;
		case 5:
			tipotransaccion="07";
			break;
		}
		return tipotransaccion;
	}	

	public String ValidarDigitoVerificador(String codigo){
		char[] arreglocodigo;
		String digitoverificador=null;
		int multiplicador=2;
		int digito=0;	
		int residuo=0;
		int resultado=0;
		arreglocodigo = codigo.toCharArray();
		arreglocodigo=InvierteRec(arreglocodigo, 0, arreglocodigo.length-1);
		for (int j=0; j<codigo.length(); j++){
			int resto=j%6;
			if (resto==0){				
				multiplicador=2;
			}
			//System.out.println(j);
			//System.out.println(String.valueOf(multiplicador)+"*"+String.valueOf(arreglocodigo[j]));
			//int valor=(Character.getNumericValue(arreglocodigo[j])*multiplicador);
			int valor=(Integer.parseInt(String.valueOf(arreglocodigo[j]))*multiplicador);
			digito=digito+valor;
			multiplicador=multiplicador+1;
			//System.out.println(valor);
		}
		residuo=digito%11;

		resultado=11-residuo;
		if(resultado==10){
			resultado=1;
		}else if(resultado==11){
			resultado=0;
		}
		digitoverificador=String.valueOf(resultado);	
		//System.out.println(digitoverificador);
		return digitoverificador;
	}
	public static String calculateCD(String number) {
		// We use string as leading 0s are significant
		String digitoverificador=null;
		if (!number.matches("^[0-9]+$")) 
			throw new IllegalArgumentException("Can only calculate MOD11 for number-only strings");
		
		String[] numArr = number.split("");
		int total = 0;
		int weightArr[] = {2,3,4,5,6,7};
		int weight = 0;
		for (int i = numArr.length-1; i>0; i--) {
			total += Integer.parseInt(numArr[i]) * weightArr[weight++];
			weight %= 6;
		}		
		System.out.println(total);
		int residuo=(total%11);

		int resultado=11-residuo;
		if(resultado==10){
			resultado=1;
		}else if(resultado==11){
			resultado=0;
		}
		digitoverificador=String.valueOf(resultado);	
		//System.out.println(digitoverificador);
		return digitoverificador;
//		int mod = (total%11);
//		System.out.println(mod);
//		if (mod == 1) {
//			return "-";
//		} else if (mod == 0) {
//			return "0";
//		} else {
//			return Integer.toString(11-mod);
//		}
	}

	public static char[] InvierteRec(char [] a,int i,int f){
		while (i<=f){
			char temp = a[f];
			a[f] = a[i];
			a[i] = temp;
			return InvierteRec(a,i+1,f-1);
		}
		return  a;

	}
}
