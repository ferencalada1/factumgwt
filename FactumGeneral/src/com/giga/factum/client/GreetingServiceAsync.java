package com.giga.factum.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

//import org.apache.commons.fileupload.FileItem;

//import javax.xml.bind.JAXBElement;
import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.BodegaProdDTO;
import com.giga.factum.client.DTO.CargoDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CoreDTO;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.DocumentoSaveDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtoelectronicoDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.EquipoOrdenDTO;
import com.giga.factum.client.DTO.EstablecimientoDTO;
import com.giga.factum.client.DTO.FacturaProduccionDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.MayorizacionDTO;
import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.PosicionDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoElaboradoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.ReporteVentasEmpleadoDTO;
import com.giga.factum.client.DTO.ReportesDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.SerieDTO;
import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.DTO.TblpermisorolpestanaDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.giga.factum.client.DTO.TblrolDTO;
import com.giga.factum.client.DTO.TbltipodocumentoDTO;
import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.TmpAuxgeneralDTO;
import com.giga.factum.client.DTO.TmpAuxresultadosDTO;
import com.giga.factum.client.DTO.TmpComprobacionDTO;
import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.DTO.detalleVentasATSDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback)
			throws IllegalArgumentException;  
	
	
	public void fechaServidor(AsyncCallback<Date> asyncCallback);
	public void verificar_sesion(AsyncCallback<User> asyncCallback);
	public void checkLogin(String userName,String password, String planCuentaID,AsyncCallback<User> asyncCallback);
	public void getUserFromSession(AsyncCallback<User> asyncCallback);
	public void cerrar_sesion(String NombreBD, String PasswordBD, String UsuarioBD, String SO,String PasswordComprimido, String destinoBackup,AsyncCallback<String> asyncCallback);
	public void LeerXML(AsyncCallback<List<String>> asyncCallback) throws IllegalArgumentException, IOException;
	
	//Carga de Menus
	void listarpermisosLecturap(int nivel, String pestana, AsyncCallback<TblpermisorolpestanaDTO> asyncCallback);
	void listarRol(int ini,int fin,AsyncCallback<List<TblrolDTO>> callback);
	void listarRolCombo(int ini, int fin,AsyncCallback<List<TblrolDTO>> Asyncallback);
		
		//actualizar IVA al 14%
//	void actualizarIva(AsyncCallback<String> asyncCallback);
	void numeroRegistros(String tabla,AsyncCallback<Integer> callback);
	
	void listaProductos(Integer inicio, Integer NumReg, Integer Tipo, Integer Jerarquia,Integer TipoPrecio,int StockNegativo, AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> objback);
	void listaProductos(AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> objback);
	void findJoin(String variable,String tabla, String campo, AsyncCallback<List<PersonaDTO>> objback);
	
	void listaTipoPago(AsyncCallback<List<TipoPagoDTO>> objback );
	void ultimoDocumento(String filtro, String campoOrden,AsyncCallback<DtocomercialDTO> objback);
	void ListJoinPersona(String tabla, int ini,int fin,AsyncCallback<List<PersonaDTO>> objback);
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	void ListJoinPersona2(String tabla, AsyncCallback<List<PersonaDTO>> objback);
	void findJoin2(String variable,String tabla, String campo, AsyncCallback<List<PersonaDTO>> objback);
	void buscarPersona2(PersonaDTO cedula,int tipo, AsyncCallback<PersonaDTO> callback);
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	void ListEmpleados(int ini,int fin,AsyncCallback<List<PersonaDTO>> objback);
	void grabarProductoBodega(ProductoBodegaDTO[] proBod,int length,AsyncCallback<String> callback);
	void grabarProductoTipoPrecio(ProductoTipoPrecioDTO[] proTip,int length,AsyncCallback<String> callback);
	void numeroRegistrosPersona(String tabla,AsyncCallback<Integer> callback);
	void listarDocFecha(String fecha,AsyncCallback<List<DtocomercialDTO>> objback);
	//Funci�n para listar el tipo de Documentos 
	void listarTiposDocumento(Integer ini, Integer fin, AsyncCallback<List<TbltipodocumentoDTO>> objback);
	
	// METODOS PARA CLIENTE
	void grabarPersona(PersonaDTO persona,AsyncCallback<String> callback)
	throws IllegalArgumentException;
	void grabarCliente(PersonaDTO cliDTO, AsyncCallback<String> objback);
	void modificarPersona(PersonaDTO persona,AsyncCallback<String> callback);
	void eliminarPersona(PersonaDTO persona,AsyncCallback<String> callback);
	void buscarPersona(PersonaDTO cedula,int tipo, AsyncCallback<PersonaDTO> callback);
	
	
	
	// METODOS PARA MARCA
	void grabarMarca (MarcaDTO marca,AsyncCallback<String> callback);
	void modificarMarca(MarcaDTO marca,AsyncCallback<String> callback);
	void eliminarMarca(MarcaDTO marca,AsyncCallback<String> callback);
	void buscarMarca (String idMarca,AsyncCallback<MarcaDTO> callback);
	void listarMarca (int ini,int fin,AsyncCallback<List<MarcaDTO>> callback);
	void listarMarcaLike(String busqueda,String campo,AsyncCallback<List<MarcaDTO>> callback);
	void listarMarcaCombo(int ini, int fin,AsyncCallback<List<MarcaDTO>> callback);
	void numeroRegistrosMarca(String tabla,AsyncCallback<Integer> callback);
	
	// METODOS PARA Unidad
	void grabarUnidad (UnidadDTO unidad,AsyncCallback<String> callback);
	void modificarUnidad(UnidadDTO unidad,AsyncCallback<String> callback);
	void eliminarUnidad(UnidadDTO unidad,AsyncCallback<String> callback);
	void buscarUnidad (String idUnidad,AsyncCallback<UnidadDTO> callback);
	void listarUnidad(int ini,int fin,AsyncCallback<List<UnidadDTO>> callback);
	void listarUnidadLike(String busqueda,String campo,AsyncCallback<List<UnidadDTO>> callback);
	void listarUnidadCombo(int ini, int fin,AsyncCallback<List<UnidadDTO>> callback);
	void numeroRegistrosUnidad(String tabla,AsyncCallback<Integer> callback);
	
	// METODOS PARA Categoria
	void grabarCategoria (CategoriaDTO marca,AsyncCallback<String> callback);
	void modificarCategoria(CategoriaDTO marca,AsyncCallback<String> callback);
	void eliminarCategoria(CategoriaDTO marca,AsyncCallback<String> callback);
	void buscarCategoria (String idCategoria,AsyncCallback<CategoriaDTO> callback);
	void listarCategoria(int ini,int fin,AsyncCallback<List<CategoriaDTO>> callback);
	void listarCategoriaLike(String busqueda,String campo,AsyncCallback<List<CategoriaDTO>> callback);
	void listarCategoriaCombo(int ini, int fin,AsyncCallback<List<CategoriaDTO>> callback);
	void numeroRegistrosCategoria(String tabla,AsyncCallback<Integer> callback);
	
	// METODOS PARA BODEGA
	void grabarBodega (BodegaDTO marca,AsyncCallback<String> callback);
	void modificarBodega(BodegaDTO marca,AsyncCallback<String> callback);
	void eliminarBodega(BodegaDTO marca,AsyncCallback<String> callback);
	void buscarBodega (String idBodega,AsyncCallback<BodegaDTO> callback);
	void listarBodega(int ini,int fin,AsyncCallback<List<BodegaDTO>> callback);
	void listarBodegaProducto(int ini,int fin,int idproducto, AsyncCallback<List<BodegaDTO>> callback);
	void listarBodegaLike(String busqueda,String campo,AsyncCallback<List<BodegaDTO>> callback);
	void numeroRegistrosBodega(String tabla,AsyncCallback<Integer> callback);
	
	 //METODOS PARA TIPO PRECIO
	void grabarTipoprecio (TipoPrecioDTO tipoprecio,AsyncCallback<String> callback);
	void modificarTipoprecio(TipoPrecioDTO tipoprecio,AsyncCallback<String> callback);
	void eliminarTipoprecio(TipoPrecioDTO tipoprecio,AsyncCallback<String> callback);
	void buscarTipoprecio (String idTipoPrecio,AsyncCallback<TipoPrecioDTO> callback);
	void listarTipoprecio(int ini,int fin,AsyncCallback<List<TipoPrecioDTO>> callback);
	void listarTipoprecioLike(String busqueda,String campo,AsyncCallback<List<TipoPrecioDTO>> callback);
	void numeroRegistrosTipoprecio(String tabla,AsyncCallback<Integer> callback);
	
	
	// METODOS PARA Producto
	void grabarProducto (ProductoDTO producto,AsyncCallback<String> callback);
	void modificarProducto(ProductoDTO producto,AsyncCallback<String> callback);
	void modificarProductoEliminado(ProductoDTO pro,AsyncCallback<String> callback);
	void eliminarProducto(ProductoDTO producto,AsyncCallback<String> callback);
	void buscarProducto (String nombre,String campo,AsyncCallback<ProductoDTO> callback);
	void buscarProductoCodBar (String nombre,String campo,Integer Tipo,int StockNegativo,AsyncCallback<ProductoDTO> callback);
	void buscarProductoEliminado (String nombre,String campo,AsyncCallback<ProductoDTO> callback);
	void listaProductosConFiltro(int ini, int fin, int op,AsyncCallback<LinkedHashMap<Integer, ProductoDTO>> listaCallback);
	void listarProducto(int ini,int fin,AsyncCallback<List<ProductoDTO>> callback);
	void listarProductoLike(String busqueda,String campo,AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> callback);
	void listarProductoJoin(String nombre,String tabla, String campo,Integer Tipo, Integer Jerarquia, Integer TipoPrecio,int StockNegativo, AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> callback);
	void numeroRegistrosProducto(String tabla, AsyncCallback<Integer> callback);
	void ProdBodega(String idProducto,String tabla, AsyncCallback<List<BodegaProdDTO>> callback);
	void BuscarProductoJoin(String bod,String des,AsyncCallback<LinkedHashMap<Integer, ProductoDTO>> listaCallback);
	void TransferirBodega(LinkedList<TraspasoBodegaProductoDTO> listTransferir,
			AsyncCallback<String> callback);
	void ValorDelInventario(AsyncCallback<Float> callback);
	void grabarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproh, AsyncCallback<String> callback);
	
	//METODOS PARA PRODUCTOS ELABORADOS
	void listarHijosProductoElaborado(String idproductoelaborado, AsyncCallback<List<ProductoElaboradoDTO>> funcioncallback);
	void modificarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproductodto,AsyncCallback<String>  objbackModificar);

	
	// METODOS PARA EMPRESA
	void grabarEmpresa (EmpresaDTO marca,AsyncCallback<String> callback);
	void modificarEmpresa(EmpresaDTO marca,AsyncCallback<String> callback);
	void eliminarEmpresa(EmpresaDTO marca,AsyncCallback<String> callback);
	void buscarEmpresa (String idEmpresa,AsyncCallback<EmpresaDTO> callback);
	void listarEmpresa(int ini,int fin,AsyncCallback<List<EmpresaDTO>> callback);
	void buscarEmpresaNombre (String idEmpresa,String campo,AsyncCallback<EmpresaDTO> callback);
	void listarEmpresaLike(String busqueda,String campo,AsyncCallback<List<EmpresaDTO>> callback);
	
	
	//METODOS PARA TIPO DE CUENTA
	void grabarTipoCuenta (TipoCuentaDTO tipocuenta,AsyncCallback<String> callback);
	void modificarTipoCuenta(TipoCuentaDTO tipocuenta,AsyncCallback<String> callback);
	void eliminarTipoCuenta(TipoCuentaDTO tipocuenta,AsyncCallback<String> callback); 
	void buscarTipoCuenta (String idTipoCuenta,AsyncCallback<TipoCuentaDTO> callback);
	void listarTipoCuenta(int ini,int fin,AsyncCallback<List<TipoCuentaDTO>> callback);
	void buscarTipoCuentaNombre (String idTipoCuenta,String campo,AsyncCallback<TipoCuentaDTO> callback);
	void listarTipoCuentaLike(String busqueda,String campo,AsyncCallback<List<TipoCuentaDTO>> callback);
	void numeroRegistrosTipocuenta(String tabla,AsyncCallback<Integer> callback);
	
	//METODOS PARA CUENTA
	void grabarCuenta (CuentaDTO cuenta,String planCuentaID,AsyncCallback<String> callback);
	void listarCuenta(int ini,int fin,String planCuentaID, AsyncCallback<List<CuentaDTO>> callback);
	void numeroRegistroscuenta(String tabla,String planCuentaID,AsyncCallback<Integer> callback);
	void eliminarCuenta(CuentaDTO cuenta,AsyncCallback<String> callback); 
	void modificarCuenta(CuentaDTO cuenta,AsyncCallback<String> callback);
	void buscarCuentaNombre (String idCuenta,String campo,AsyncCallback<CuentaDTO> callback);
	void listarCuentaLike(String busqueda,String campo,String planCuentaID,AsyncCallback<List<CuentaDTO>> callback);
	void listarCuentaQuery(String query,AsyncCallback<List<CuentaDTO>> callback);
	void listarCuentaPlanCuentas(String planCuentaID, AsyncCallback<List<CuentaDTO>> objbacklst);
	void buscarCuenta (String idCuenta,AsyncCallback<CuentaDTO> callback);
	
	//METODOS PARA PLANCUENTA
	void grabarPlanCuenta (PlanCuentaDTO PlanCuenta,AsyncCallback<String> callback);
	void listarPlanCuenta(int ini,int fin,AsyncCallback<List<PlanCuentaDTO>> callback);
	void numeroRegistrosPlanCuenta(String tabla,AsyncCallback<Integer> callback);
	void eliminarPlanCuenta(String id,AsyncCallback<String> callback); 
	void buscarPlanCuentaNombre (String idPlanCuenta,String campo,AsyncCallback<PlanCuentaDTO> callback);
	void listarPlanCuentaLike(String busqueda,String campo,AsyncCallback<List<PlanCuentaDTO>> callback);
	void buscarPlanCuenta (String idPlanCuenta,AsyncCallback<PlanCuentaDTO> callback);
	void ultimoPlanCuenta(AsyncCallback<PlanCuentaDTO> callbackUltimoPlanCuenta);
	
	// METODOS PARA Cargo
	void grabarCargo (CargoDTO cargo,AsyncCallback<String> callback);
	void modificarCargo(CargoDTO cargo,AsyncCallback<String> callback);
	void eliminarCargo(CargoDTO cargo,AsyncCallback<String> callback);
	void buscarCargo (String idCargo,AsyncCallback<CargoDTO> callback);
	void listarCargo (int ini,int fin,AsyncCallback<List<CargoDTO>> callback);
	void listarCargoLike(String busqueda,String campo,AsyncCallback<List<CargoDTO>> callback);
	void numeroRegistrosCargo(String tabla,AsyncCallback<Integer> callback);
	
	//METODOS PARA IMPUESTO
	void grabarImpuesto (ImpuestoDTO cargo,AsyncCallback<String> callback);
	void modificarImpuesto(ImpuestoDTO cargo,AsyncCallback<String> callback);
	void eliminarImpuesto(ImpuestoDTO cargo,AsyncCallback<String> callback);
	void buscarImpuesto (String idImpuesto,AsyncCallback<ImpuestoDTO> callback);
	void listarImpuesto (int ini,int fin,AsyncCallback<List<ImpuestoDTO>> callback);
	void listarImpuestoLike(String busqueda,String campo,AsyncCallback<List<ImpuestoDTO>> callback);
	void numeroRegistrosImpuesto(String tabla,AsyncCallback<Integer> callback);
	void listarCuentaPlanCuenta(String idEmpresa,AsyncCallback<LinkedList<String[]>> callback);
	
	//Metodos para Retenciones 
	void listarRetenciones(String FechaI, String FechaF, String TipoTransaccion,String tipoImpuesto,String Cedula,String codigoImpuesto,AsyncCallback<List<DtocomercialDTO>> callback);
	void grabarDetalle(LinkedList<RetencionDTO> det,AsyncCallback<String> callback);
	
	//METODOS PARA TIPO PAGO
	void grabarTipopago (TipoPagoDTO cargo,AsyncCallback<String> callback);
	void modificarTipopago(TipoPagoDTO cargo,AsyncCallback<String> callback); 
	void eliminarTipopago(TipoPagoDTO cargo,AsyncCallback<String> callback);
	void buscarTipopago (String idImpuesto,AsyncCallback<TipoPagoDTO> callback);
	void listarTipopago (int ini,int fin,AsyncCallback<List<TipoPagoDTO>> callback);
	void listarTipopagoLike(String busqueda,String campo,AsyncCallback<List<TipoPagoDTO>> callback);
	void numeroRegistrosTipopago(String tabla,AsyncCallback<Integer> callback);

	//CONTABILIDAD
	void listarMayorizacion(String FechaI,String FechaF,String nrt,String tipo,AsyncCallback<List<MayorizacionDTO>> listaCallback);
	void listarTipoDocumento(AsyncCallback<List<TbltipodocumentoDTO>> objbackbodega);
	
	//METODOS PARA REPORTES FACTURAS
	void listarFacturas(String FechaI, String FechaF,String Tipo,AsyncCallback<List<DtocomercialDTO>> callback);
	void listarReportesCliente(String FechaI, String FechaF,String Tipo,String ruc, String nombre, String razonSocial, AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasCliente(String FechaI, String FechaF,String idCliente,String Tipo,AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasVendedorCliente(String FechaI, String FechaF,String idCliente,String idVendedor,String Tipo,AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasVendedor(String FechaI, String FechaF,String idVendedor,String Tipo,AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasProducto(String FechaI, String FechaF,String idProducto,String Tipo,AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasClienteProducto(String FechaI, String FechaF, String idCliente, String idProducto, String Tipo, AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasVendedorProducto(String FechaI, String FechaF, String idVendedor, String idProducto, String Tipo, AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasVendedorClienteProducto(String FechaI, String FechaF,String idCliente, String idVendedor, String idProducto, String Tipo, AsyncCallback<List<DtocomercialDTO>> callback);
	void listarFacturasNumDto(String FechaI, String FechaF,String Tipo,String NumDto, AsyncCallback<List<DtocomercialDTO>> callback);
	
	// METODOS PARA AREA
	void grabarArea (AreaDTO unidad,AsyncCallback<String> callback);
	void modificarArea(AreaDTO unidad,AsyncCallback<String> callback);
	void eliminarArea(AreaDTO unidad,AsyncCallback<String> callback);
	void buscarArea (String idUnidad,AsyncCallback<AreaDTO> callback);
	void listarArea(int ini,int fin,AsyncCallback<List<AreaDTO>> callback);
	void listarAreaLike(String busqueda,String campo,AsyncCallback<List<AreaDTO>> callback);
	void listarMesasOcupadas(AsyncCallback<List<AreaDTO>> callback);
	void listarAreaCombo(int ini, int fin,AsyncCallback<List<AreaDTO>> callback);
	void numeroRegistrosArea(String tabla,AsyncCallback<Integer> callback);
	
	// METODOS PARA MESA
	void grabarMesa (MesaDTO unidad,AsyncCallback<String> callback);
	void modificarMesa(MesaDTO unidad,AsyncCallback<String> callback);
	void eliminarMesa(MesaDTO unidad,AsyncCallback<String> callback);
	void buscarMesa (String idUnidad,AsyncCallback<MesaDTO> callback);
	void listarMesa(int ini,int fin,AsyncCallback<List<MesaDTO>> callback);
	void listarMesaLike(String busqueda,String campo,AsyncCallback<List<MesaDTO>> callback);
	void listarMesaCombo(int ini, int fin,AsyncCallback<List<MesaDTO>> callback);
	void numeroRegistrosMesa(String tabla,AsyncCallback<Integer> callback);
	void obtenerPedido(Integer idMesa, AsyncCallback<ArrayList<PedidoProductoelaboradoDTO>> callback);
	
	//METODOS PARA SERVICIO
	void grabarCore(CoreDTO Core,AsyncCallback<String> callback);
	void listarCore(int ini,int fin,AsyncCallback<List<CoreDTO>> callback);
	void numeroRegistrosCore(String tabla,AsyncCallback<Integer> callback);
	void eliminarCore(CoreDTO Core,AsyncCallback<String> callback); 
	void modificarCore(CoreDTO Core,AsyncCallback<String> callback);
	void buscarCoreNombre (String idCore,String campo,AsyncCallback<CoreDTO> callback);
	void listarCoreLike(String busqueda,String campo,AsyncCallback<List<CoreDTO>> callback);
	void buscarCore (String idCore,AsyncCallback<CoreDTO> callback);
	
	//METODOS PARA REPORTES PAGOS
	void listarPagos(String FechaI, String FechaF,String estado,String Tipo,String idfactura,String idCliente,int tipoConsulta,AsyncCallback<List<TblpagoDTO>> callback);
	//++++++++++++++++++++++++   PARA QUE BUSQUE POR NUMERO DE FACTURA   ++++++++++++++++++++++++++++++++++++
	void listarPagos2(String FechaI, String FechaF,String estado,String Tipo,String numRealTransaccion,String idCliente,int tipoConsulta,AsyncCallback<List<TblpagoDTO>> callback);
	void BuscarPago(String idPago,AsyncCallback<TblpagoDTO> callback);
	void grabarPago(TblpagoDTO pagoDTO, AsyncCallback<String> callback);
	void modificarPago(TblpagoDTO pagoDTO,User usuario, AsyncCallback<String> callback);
	void modificarcrearPago(TblpagoDTO pagoDTO,double pagado,User usuario, AsyncCallback<String> callback);
	void listarPagosVencidos(AsyncCallback<List<TblpagoDTO>> callback);
	
	//DOCUMENTO COMERCIAL
	void grabarDocumento(DtocomercialDTO dto, Integer indicador, Double iva, Integer anular, String responsable, String planCuentaID, int ConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales, AsyncCallback<String> funcioncallback);
	void listarDocTipo(Integer tipo, AsyncCallback<List<DtocomercialDTO>> callback);
	void listarDocTipoPersona(Integer tipo, Integer persona,AsyncCallback<List<DtocomercialDTO>> callback);
	void getDtoID(Integer ID, AsyncCallback<DtocomercialDTO> funcioncallback);
	
	//POSICIONES
	void grabarPosiciones(LinkedList<PosicionDTO> pos, AsyncCallback<String> callback);
	void listarPosiciones(String NombreDocuemto,AsyncCallback<List<PosicionDTO>> callback);


	void grabarEmpleado(PersonaDTO cliDTO, AsyncCallback<String> callback);
	
	//SERIES
	void grabarSeries(LinkedList<SerieDTO> pos, AsyncCallback<String> callback);
	void listarSeries(int idproducto, AsyncCallback<List<SerieDTO>> callback);
	void buscarSerie(String serie, AsyncCallback<SerieDTO> callback);


	void grabarPagos(LinkedList<TblpagoDTO> pagoDTO, User Usuario,
			AsyncCallback<String> callback);


	void ReprotePagosVencidosCliente(String idPersona,
			AsyncCallback<String> callback);

	//REPORTES


	void actualizarAso(List<TbltipodocumentoDTO> tipoDocumentos,
			AsyncCallback<String> actualizarAsociaciones);


	void listarLibro(String fechai, String fechaf,String planCuentaID,AsyncCallback<LinkedList<MovimientoCuentaPlanCuentaDTO>> callback);


	void actualizarAsoPagos(List<TipoPagoDTO> tipoPagos,
			AsyncCallback<String> callback);


	void actualizarAsoImpuestos(List<ImpuestoDTO> Impuestos,
			AsyncCallback<String> callback);


	void grabarRetencion(LinkedList<RetencionDTO> detalle, DtocomercialDTO doc,
			AsyncCallback<String> callback);


	void GrabarCli(PersonaDTO personadto, AsyncCallback<String> callback);


	void GrabarProv(PersonaDTO personadto, AsyncCallback<String> callback);


	void GrabarEmp(PersonaDTO personadto, AsyncCallback<String> callback);


	void pagoConRetencion(LinkedList<RetencionDTO> detalle,
			DtocomercialDTO doc, TblpagoDTO pagodto,
			AsyncCallback<String> callback);

//	void ReproteTotalDocumentos(String fechaI, String fechaf, String tipo,
//			AsyncCallback<Double> callback);


//	void ReproteIVADocumentos(String fechaI, String fechaf, String tipo,
//			AsyncCallback<Double> callback);


//	void ReproteSubtIVA0Documentos(String fechaI, String fechaf, String tipo,
//			AsyncCallback<Double> callback);


//	void ReproteSubTotalNetoDocumentos(String fechaI, String fechaf,
//			String tipo, AsyncCallback<Double> callback);


	void ReporteVentasEmpleado(String fechaI, String fechaF,double IVA,
			AsyncCallback<LinkedList<ReporteVentasEmpleadoDTO>> callback);


//	void Libro(String fechaI, String fechaf, String tipo,
//			AsyncCallback<ReportesDTO> callbackImpresion);
	
	void impresion(String canvas, AsyncCallback<String> callbackImpresion);


	//void listGridToExel(LinkedList<String[]> listado, AsyncCallback<String> callback);
	void listGridToExel(LinkedList<String[]> listado,String nombre, AsyncCallback<String> callback);


	void kardex(String FechaI, String FechaF, String idProducto,
			AsyncCallback<List<DtocomercialDTO>> callback);


	/*void Kardex(String FechaI, String FechaF, String idProducto,
			AsyncCallback<List<DtocomercialDTO>> callback);*/


	 void eliminarDocSinConfirmar(DtocomercialDTO documentoDTO, AsyncCallback<String> callback);


	//void listarProductosProveedor(String FechaI, String FechaF, int StockMin,
		//	int StockMax, AsyncCallback<List<ProductoProveedor>> callback);


	void listarTipoPrecio(int codProducto,
			AsyncCallback<List<ProductoTipoPrecio>> callback);


	void grabarProductoTipoPrecio(LinkedList<ProductoTipoPrecioDTO> proTip,
			AsyncCallback<String> callback);


	void listarRetenciones(String FechaI, String FechaF, int tipoTransaccion,
			String ruc, String nombres, String razonSocial,
			AsyncCallback<List<ReporteRetencion>> callback);


	void listarDetallesRetencion(int numDto,
			AsyncCallback<List<RetencionDTO>> callback);


	void listarRetTotales(int tipoTransaccion, LinkedList<Integer> idImpuesto,String ruc, String nombres, String razonSocial, String fechaI, String fechaF,
			AsyncCallback<List<RetencionDetalle>> callback);


	void listarProductosProveedor(String FechaI, String FechaF, int StockMin,
			int StockMax,String idProveedor, String idProducto,
			AsyncCallback<List<ProductoProveedor>> callback);
	
	void listarProductosCliente(String FechaI, String FechaF, int StockMin,
			int StockMax,String idCliente, String idProducto,
			AsyncCallback<List<ProductoCliente>> callback);

	//void impresion(Canvas canvImpresionCallback<String> callbackImpresion);
	void listarProductoJoin2(String nombre, String tabla, String campo,
			int stock, int Tipo, int Jerarquia, int TipoPrecio, int StockNegativo,AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> callback);


	void listarProductoLike2(String busqueda, String campo, int stock, int Tipo, int Jerarquia, int TipoPrecio,int StockNegativo,
			AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> callback);
	void listarProductoMateriaPrimaLike(String busqueda, String campo, int stock,
			AsyncCallback<LinkedHashMap<Integer, ProductoDTO>> listaCallback);	
	
	void listarProductoLikeEliminados(String busqueda, String campo, int stock,
			AsyncCallback<LinkedHashMap<Integer, ProductoDTO>> listaCallback);

	void listarPagosFacRealCompra(String FechaI, String FechaF, String estado,
			String Tipo, String idfactura, String idCliente,
			String numRealFactura, AsyncCallback<List<TblpagoDTO>> callback);


	void grabarEgreso(LinkedList<TblpagoDTO> list, User usuario, 
			AsyncCallback<String> callback);


	void listarPagosEgreso(String Tipo,
			String num, AsyncCallback<List<TblpagoDTO>> callback);
	
	void ultimoEgreso(AsyncCallback<Integer> callback);
	
	void ultimoIngreso(AsyncCallback<Integer> callback);


	void ultimoProducto(AsyncCallback<Integer> callback);


	void findJoinCedulas(String variable, String campo,
			AsyncCallback<List<PersonaDTO>> callback);


	void modificarPersonaRUC(PersonaDTO persona, AsyncCallback<String> callback);


	


	void asignarPuntos(ClienteDTO cliente, AsyncCallback<String> callback);


	void grabarAnticipo(LinkedList<DtocomercialDTO> listAnticipos,
			AsyncCallback<String> callback);


	void CostoUtilidadDetalle(LinkedList<DtoComDetalleDTO> listDetDTO,
			AsyncCallback<String> callback);


	void listarPagosFechaPago(String FechaI, String FechaF, String estado,
			String Tipo, String numRealTransaccion, String idCliente,
			int tipoConsulta, AsyncCallback<List<TblpagoDTO>> callback);


	void buscarPagoId(int idPago, AsyncCallback<TblpagoDTO> callback);
	
	void BuscarPagoIdComercial(int idDtoComercial, AsyncCallback<TblpagoDTO> callback);


	void buscarBodegaSegunNombre(String nombre,
			AsyncCallback<BodegaDTO> callback);
	
	
	//ejecucion de Vistas
	void ejecutarProcedimiento(String fileName, HashMap <String,Object> param,boolean bandera, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD,AsyncCallback<String> callback);
	void ejecutarComprobacion(String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD,AsyncCallback<List<TmpComprobacionDTO>> callback);
	void ejecutarBalance(String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD, AsyncCallback<List<TmpAuxgeneralDTO>> callback);
	void ejecutarEstado(String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD,AsyncCallback<List<TmpAuxresultadosDTO>> callback);
	
	void importarExcel(String ruta, AsyncCallback<LinkedList> retornolist);


	void exportarexcel(LinkedList<String[]> ld, String nombre,
			AsyncCallback<String> objbackString);


	

	void listarEquipoOrdenFiltroID(Integer id,AsyncCallback<List<EquipoOrdenDTO>>  callback);


	void buscarCliente(int id, AsyncCallback<ClienteDTO> callback);
	//METODO SETEAR VARIABLES FACTUM
	//void cambiaValor(int banderaSO,AsyncCallback<String> callback);	
	//METODO PARA HACER BACKUP DE LA BASE DE DATOS
	void backupDB(boolean validadorAlmacenamiento,String NombreBD, String PasswordBD, String UsuarioBD,String SO,String PasswordComprimido, String DestinoBackup, AsyncCallback<String> callback);
	//void backupDB(boolean validadorAlmacenamiento, AsyncCallback<String> callback);
	//METODO PARA CONSULTAR LA IP DEL SERVIDOR
	void obtenerIPServidor(AsyncCallback<String> callback);
	//METODO PARA CONSULTAR LA IP DEL SERVIDOR
	void listGridToPDF(LinkedList<String[]> ld, String[] atributos,String NombreEmpresa,String TelefonoCelularEmpresa,String TelefonoConvencionalEmpresa, String DireccionEmpresa, String Logo, AsyncCallback<String> objbackString);
	//METODO PARA CONSULTAR LA LISTA DE LAS IMAGENES
	void listarImagenes(AsyncCallback<List<String>> objbackString);	
	//METODO PARA CONSULTAR LA LISTA DE LAS IMAGENES
	void obtenernombreHost(AsyncCallback<String> objbackString);	
	//METODO PARA LISTAR LOS PEDIDOS
	void listarPedidos(String busqueda, String fechainicial, String fechafinal, int banderafiltro,AsyncCallback<List<PedidoProductoelaboradoDTO>> objbackString);
	//METODO PARA MODIFICAR PEDIDO
	void modificarPedido(ArrayList<PedidoProductoelaboradoDTO> listapedidoproductoelaboradodto, AsyncCallback<String> objback);
	//METODO PARA DOCUMENTOS ELECTRONICOS
	void documentoElectronico(DtocomercialDTO documento, String NombrePlugFacturacionElectronica,String RutaCertificado, 
//			String AmbienteFacturacionElectronica, 
			String SO, AsyncCallback<String> objback);
	//METODO PARA LISTAR FACTURAS PRODUCCION
	void listarFacturasProduccion(String fecha,  AsyncCallback<ArrayList<FacturaProduccionDTO>> objback);
	//METODOS CUENTAS POR COBRAR
	void obtenerCuentasPorCobrar(AsyncCallback<List<DtocomercialDTO>> callback);
	//METODO PARA BUSCAR CUENTAS POR COBRAR
	void buscarCuentasCobrar(String nombre, String campo,AsyncCallback<List<DtocomercialDTO>> objCuetasPorCobrar);
	//METODOS PARA GRABAR MOVIMIENTOS-ASIENTOS CONTABLES
	void grabarDtomovimiento(DtocomercialDTO dtocomercialdto, List<MovimientoCuentaPlanCuentaDTO> MovimientoCuentaPlanCuenta, AsyncCallback<String> objbackDtoMovimiento);


	void modificarCuenta(CuentaDTO cuenta, String planCuentaID,AsyncCallback<String> callback);


	void listarTipoPrecioD(int parseInt, AsyncCallback<List<ProductoTipoPrecioDTO>> objbacklstTipo);


	void buscarEstablecimiento(Integer idEmpresa, AsyncCallback<EstablecimientoDTO> callback);


	void buscarPuntoemision(Integer idEmpresa, String establecimiento, AsyncCallback<PuntoEmisionDTO> callback);


	void ingresar( AsyncCallback<Integer> callback);
	void getSessionId(AsyncCallback<String> callback);

	void checkLogin(String userName, String planCuentaID, AsyncCallback<User> callback);
	void ultimaRetencion(String filtro, int tipo, String orden, AsyncCallback<RetencionDTO> objbackDto);


	void listarImpuesto(int tipo, AsyncCallback<List<ImpuestoDTO>> callback);


	void listarMultiImpuesto(int ini, int fin, AsyncCallback<List<TblmultiImpuestoDTO>> callback);


	void grabarProductoMultiImpuesto(TblproductoMultiImpuestoDTO[] proTip, AsyncCallback<String> callback);


	void listarProdMultiImp(int idProducto, AsyncCallback<List<TblproductoMultiImpuestoDTO>> objbacklstMult);


	void grabarDocumentosDiv(List<DocumentoSaveDTO> documentos, String userName, String planCuenta,
			int banderaConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales,
			AsyncCallback<String[]> factsDivCallback);


	void ultimoPlanCuenta(String idEmpresa, AsyncCallback<PlanCuentaDTO> callback);


	//void buscarXML(DynamicForm dynXML, AsyncCallback<String> callback);
	//void buscarXML(FileItem dynXML, AsyncCallback<String> callback);
	
	void eliminarFile(String url, AsyncCallback<String> callback);
	
	}
	
