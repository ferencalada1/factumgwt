package com.giga.factum.client.mappers;

import com.giga.factum.client.DTO.RetencionDTO;
import com.google.gwt.core.client.JavaScriptObject;

public class RetencionJSON extends JavaScriptObject {
	protected RetencionJSON(){}
//	 public RetencionJSON(RetencionDTO retencion){
//		setIdRetencion(retencion.getIdRetencion());
//		setIdImpuesto(retencion.getImpuesto().getIdImpuesto());
//		setBaseImponible(retencion.getBaseImponible());
//		setAutorizacionSri(retencion.getAutorizacionSri());
//		setEstado(retencion.getEstado());
//		setNumero(retencion.getNumero());
//		setNumRealRetencion(retencion.getNumRealRetencion());
//		setIdDtoComercial(retencion.getTbldtocomercialByIdDtoComercial().getIdDtoComercial());
//		setIdFactura(retencion.getTbldtocomercialByIdFactura().getIdDtoComercial());
//		setValorRetenido(retencion.getValorRetenido());
//	 }
	 
	 final public void setValues(RetencionDTO retencion){
//		 //com.google.gwt.user.client.Window.alert("setvaluesretencion");
			int idretencion=retencion.getIdRetencion();
//			//com.google.gwt.user.client.Window.alert("take retencion id");
		 	setIdRetencion(idretencion);
//		 	//com.google.gwt.user.client.Window.alert("set id retencion");
		 	int idimpuesto=retencion.getImpuesto().getIdImpuesto().intValue();
//		 	//com.google.gwt.user.client.Window.alert("take id impuesto");
			setIdImpuesto(idimpuesto);
//			//com.google.gwt.user.client.Window.alert("set id");
			setBaseImponible(retencion.getBaseImponible());
//			//com.google.gwt.user.client.Window.alert("set base imponible");
			setAutorizacionSri(retencion.getAutorizacionSri());
//			//com.google.gwt.user.client.Window.alert("set autorizacion");
			setEstado(retencion.getEstado());
//			//com.google.gwt.user.client.Window.alert("set estado");
			setNumero(retencion.getNumero());
//			//com.google.gwt.user.client.Window.alert("set numero");
			setNumRealRetencion(retencion.getNumRealRetencion());
//			//com.google.gwt.user.client.Window.alert("set num real retencion");
			int iddtocomercial=retencion.getTbldtocomercialByIdDtoComercial().getIdDtoComercial().intValue();
//			//com.google.gwt.user.client.Window.alert("get iddtocomercial");
			setIdDtoComercial(iddtocomercial);
//			//com.google.gwt.user.client.Window.alert("set iddtocomercial");
//			setIdFactura(retencion.getTbldtocomercialByIdFactura().getIdDtoComercial());
			setValorRetenido(retencion.getValorRetenido());
//			//com.google.gwt.user.client.Window.alert("set valor retenido");
		 }
	  
	  	public final native int getIdRetencion() /*-{ 
	  		return this.idRetencion; 
	  	}-*/;

	  	public final native void setIdRetencion(int idretencion) /*-{ 
	  		this.idRetencion = idretencion; 
	  	}-*/;

		public final native int getIdImpuesto() /*-{ 
			return this.idImpuesto; 
		}-*/;

		public final native void setIdImpuesto(int impuestodto) /*-{ 
			this.idImpuesto = impuestodto; 
		}-*/;

		public final native double getBaseImponible() /*-{ 
			return this.baseImponible; 
		}-*/;

		public final native void setBaseImponible(double baseImponible) /*-{
			this.baseImponible = baseImponible;
		}-*/;

		public final native String getNumRealRetencion() /*-{
			return this.numRealRetencion;
		}-*/;

		public final native void setNumRealRetencion(String numRealRetencion) /*-{
			this.numRealRetencion = numRealRetencion;
		}-*/;

		public final native String getAutorizacionSri() /*-{
			return this.autorizacionSri;
		}-*/;

		public final native void setAutorizacionSri(String autorizacionSri) /*-{
			this.autorizacionSri = autorizacionSri;
		}-*/;

		public final native char getEstado() /*-{
			return this.estado;
		}-*/;

		public final native void setEstado(char estado) /*-{
			this.estado = estado;
		}-*/;

		public final native void setValorRetenido(double ValorRetenido) /*-{
			this.valorRetenido=ValorRetenido;
		}-*/;
		public final native double getValorRetenido() /*-{
			return this.valorRetenido;
		}-*/;
		
		public final native String getNumero() /*-{
			return this.numero;
		}-*/;

		public final native void setNumero(String numero) /*-{
			this.numero = numero;
		}-*/;

		public final native void setIdDtoComercial(int idDtoComercial) /*-{
			this.idDtoComercial = idDtoComercial;
		}-*/;

		public final native int getIdDtoComercial() /*-{
			return this.idDtoComercial;
		}-*/;

		public final native int getIdFactura() /*-{
			return this.idFactura;
		}-*/;

		public final native void setIdFactura(int idFactura) /*-{
			this.idFactura = idFactura;
		}-*/;
}
