package com.giga.factum.client.mappers;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsonUtils;




public class DtocomercialJSON extends JavaScriptObject {
	
	protected DtocomercialJSON() {
	}

	
//	private double descuento;
//	private String observacion;
//	private String NumeroRetencion;
//	public DtocomercialJSON(DtocomercialDTO dtocomercial) {
//		setIdDtoComercial(dtocomercial.getIdDtoComercial());
//		setIdPersona (dtocomercial.getTblpersonaByIdPersona().getIdPersona());
//		setIdVendedor (dtocomercial.getTblpersonaByIdVendedor().getIdPersona());
//		setNumRealTransaccion (dtocomercial.getNumRealTransaccion());
//		setTipoTransaccion (dtocomercial.getTipoTransaccion());
//		setFecha (dtocomercial.getFecha());
//		setExpiracion (dtocomercial.getExpiracion());
//		setAutorizacion (dtocomercial.getAutorizacion());
//		setNumPagos (dtocomercial.getNumPagos());
//		setEstado( dtocomercial.getEstado());
//		setSubtotal (dtocomercial.getSubtotal());
//		setIdAnticipo (dtocomercial.getIdAnticipo());
//		setIdNotaCredito (dtocomercial.getIdNotaCredito());
//        setIdRetencion (dtocomercial.getIdRetencion());
//        setCosto (dtocomercial.getCosto());
//        setNumCompra(dtocomercial.getNumCompra());
//        setDescuento(dtocomercial.getDescuento());
//        setObservacion(dtocomercial.getObservacion());
//	}
	final public String escapeValue(String val) {
//		val.replace('/', ' ');
//		//com.google.gwt.user.client.Window.alert("despues sustituir "+val);
//		val = JsonUtils.escapeJsonForEval(val);
//		//com.google.gwt.user.client.Window.alert("despues sustituir "+val);
		for (int i = 0; i < val.length(); i++) {
//			//com.google.gwt.user.client.Window.alert("valores char "+i+":"+val.charAt(i));
	        if (val.charAt(i) == '/' || val.charAt(i) == '"') {
//	        	//com.google.gwt.user.client.Window.alert("antes sustituir "+val);
//	        	val=val.substring(0, i)+'\\'+val.charAt(i)+val.substring(i+1);
	        	val=val.substring(0, i)+' '+val.substring(i+1);
//	        	//com.google.gwt.user.client.Window.alert("despues sustituir "+val);
//	        	i++;
//	        	//com.google.gwt.user.client.Window.alert("valores char "+i+":"+val.charAt(i));
	        }
	    }
		return val;
	}
	final public void setValues(DtocomercialDTO dtocomercial) {
//			//com.google.gwt.user.client.Window.alert("seteando json");
		int iddtocomercial=dtocomercial.getIdDtoComercial().intValue();
		setIdDtoComercial(iddtocomercial);
//			//com.google.gwt.user.client.Window.alert("seteando json iddtocomercial"+iddtocomercial);
		int idpersona=(dtocomercial.getTblpersonaByIdPersona()!=null)?dtocomercial.getTblpersonaByIdPersona().getIdPersona().intValue():0;
		setIdPersona (idpersona);
//			//com.google.gwt.user.client.Window.alert("seteando json idpersona"+idpersona);
		int idvendedor=(dtocomercial.getTblpersonaByIdVendedor()!=null)?dtocomercial.getTblpersonaByIdVendedor().getIdPersona().intValue():0;
		setIdVendedor (idvendedor);
//			//com.google.gwt.user.client.Window.alert("seteando json idvendedor"+idvendedor);
		setNumRealTransaccion (dtocomercial.getNumRealTransaccion());
//			//com.google.gwt.user.client.Window.alert("seteando json numreal"+dtocomercial.getNumRealTransaccion());
		setTipoTransaccion (dtocomercial.getTipoTransaccion());
//			//com.google.gwt.user.client.Window.alert("seteando json tipo"+dtocomercial.getTipoTransaccion());
		setFecha (dtocomercial.getFecha());
//			//com.google.gwt.user.client.Window.alert("seteando json fecha"+dtocomercial.getFecha());
		setExpiracion (dtocomercial.getExpiracion());
//			//com.google.gwt.user.client.Window.alert("seteando json expiracion"+dtocomercial.getExpiracion());
		setAutorizacion (dtocomercial.getAutorizacion());
		setAutorizacion (escapeValue(dtocomercial.getAutorizacion()));
		//setAutorizacion (JsonUtils.escapeJsonForEval(dtocomercial.getAutorizacion()));
//			//com.google.gwt.user.client.Window.alert("seteando json autorizxacion"+dtocomercial.getAutorizacion());
		setNumPagos (dtocomercial.getNumPagos());
//			//com.google.gwt.user.client.Window.alert("seteando json numpagos"+dtocomercial.getNumPagos());
		setEstado( dtocomercial.getEstado());
//			//com.google.gwt.user.client.Window.alert("seteando json estado"+dtocomercial.getEstado());
		setSubtotal (dtocomercial.getSubtotal());
//			//com.google.gwt.user.client.Window.alert("seteando json subtotal"+dtocomercial.getSubtotal());
		int idanticipo=(dtocomercial.getIdAnticipo()!=null)?dtocomercial.getIdAnticipo().intValue():0;
		setIdAnticipo (idanticipo);
//			//com.google.gwt.user.client.Window.alert("seteando json idanticipo"+idanticipo);
		int idnotacredito=(dtocomercial.getIdNotaCredito()!=null)?dtocomercial.getIdNotaCredito().intValue():0;
		setIdNotaCredito (idnotacredito);
//			//com.google.gwt.user.client.Window.alert("seteando json notacredito"+idnotacredito);
        int idretencion=(dtocomercial.getIdRetencion()!=null)?dtocomercial.getIdRetencion().intValue():0;
		setIdRetencion (idretencion);
//			//com.google.gwt.user.client.Window.alert("seteando json idretencion"+idretencion);
        setCosto (dtocomercial.getCosto());
//        	//com.google.gwt.user.client.Window.alert("seteando json costo"+dtocomercial.getCosto());
        String numcompra=(dtocomercial.getNumCompra()!=null)?dtocomercial.getNumCompra():"000";
        setNumCompra(numcompra);
//        	//com.google.gwt.user.client.Window.alert("seteando json numcompra"+numcompra);
        setDescuento(dtocomercial.getDescuento());
//        	//com.google.gwt.user.client.Window.alert("seteando json descuento"+dtocomercial.getDescuento());
        setObservacion(dtocomercial.getObservacion());
//        	//com.google.gwt.user.client.Window.alert("seteando json observacion"+dtocomercial.getObservacion());
	}

	public final native int getIdDtoComercial() /*-{
		return this.idDtoComercial;
	}-*/;

	public final native void setIdDtoComercial(int idDtoComercial) /*-{
		this.idDtoComercial = idDtoComercial;
	}-*/;

	public final native int getIdPersona() /*-{
		return this.idPersona;
	}-*/;

	public final native void setIdPersona(int idPersona) /*-{
		this.idPersona = idPersona;
	}-*/;

	public final native int getIdVendedor() /*-{
		return this.idVendedor;
	}-*/;

	public final native void setIdVendedor(int idVendedor) /*-{
		this.idVendedor = idVendedor;
	}-*/;

	public final native int getNumRealTransaccion() /*-{
		return this.NumRealTransaccion;
	}-*/;

	public final native void setNumRealTransaccion(int numRealTransaccion) /*-{
		this.NumRealTransaccion = numRealTransaccion;
		this.numRealTransaccion = numRealTransaccion;
	}-*/;

	public final native int getTipoTransaccion() /*-{
		return this.TipoTransaccion;
	}-*/;

	public final native void setTipoTransaccion(int tipoTransaccion) /*-{
		this.TipoTransaccion = tipoTransaccion;
		this.tipoTransaccion = tipoTransaccion;
	}-*/;

	public final native String getFecha() /*-{
		return this.fecha;
	}-*/;

	public final native void setFecha(String fecha) /*-{
		this.fecha = fecha;
	}-*/;

	public final native String getExpiracion() /*-{
		return this.expiracion;
	}-*/;

	public final native void setExpiracion(String expiracion) /*-{
		this.expiracion = expiracion;
	}-*/;

	public final native String getAutorizacion() /*-{
		return this.autorizacion;
	}-*/;

	public final native void setAutorizacion(String autorizacion) /*-{
//		this.autorizacion = JSON.parse(autorizacion);
		this.autorizacion = autorizacion;
	}-*/;

	public final native int getNumPagos() /*-{
		return this.NumPagos;
	}-*/;

	public final native void setNumPagos(int numPagos) /*-{
		this.NumPagos = numPagos;
		this.numPagos = numPagos;
	}-*/;

	public final native char getEstado() /*-{
		return this.estado;
	}-*/;

	public final native void setEstado(char estado) /*-{
		this.estado = estado;
	}-*/;

	public final native double getSubtotal() /*-{
		return this.subtotal;
	}-*/;

	public final native void setSubtotal(double subtotal) /*-{
		this.subtotal = subtotal;
	}-*/;
	
	public final native double getCosto() /*-{
		return this.costo;
	}-*/;

	public final native void setCosto(double costo) /*-{
		this.costo = costo;
	}-*/;
	
	public final native int getIdAnticipo() /*-{
		return this.idAnticipo;
	}-*/;

	public final native void setIdAnticipo(int idAnticipo) /*-{
		this.idAnticipo = idAnticipo;
	}-*/;
	
	public final native int getIdNotaCredito() /*-{
		return this.idNotaCredito;
	}-*/;

	public final native void setIdNotaCredito(int idNotaCredito) /*-{
		this.idNotaCredito = idNotaCredito;
	}-*/;
	
	public final native int getIdRetencion() /*-{
		return this.idRetencion;
	}-*/;

	public final native void setIdRetencion(int idRetencion) /*-{
		this.idRetencion = idRetencion;
	}-*/;

	public final native void setNumCompra(String numCompra) /*-{
		this.numCompra = numCompra;
	}-*/;

	public final native String getNumCompra() /*-{
		return this.numCompra;
	}-*/;
	
	public final native double getDescuento() /*-{
		return this.descuento;
	}-*/;

	public final native void setDescuento(double descuento) /*-{
		this.descuento = descuento;
	}-*/;
	
	public final native String getObservacion() /*-{
		return this.observacion;
	}-*/;

	public final native void setObservacion(String observacion) /*-{
		this.observacion = observacion;
	}-*/;	
	
}
