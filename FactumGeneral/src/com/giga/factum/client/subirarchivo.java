package com.giga.factum.client;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.ClickEvent; 

public class subirarchivo {
	final Window winExportar = new Window();
	DynamicForm d  = new DynamicForm();
	public  subirarchivo (Factura fact){
		final Factura fac = fact;
		d.setAction(GWT.getModuleBaseURL()+"upload");
		 
		final UploadItem btnUpload = new UploadItem("Archivo");
		
		Button bimport = new Button("importar");		
		bimport.addClickHandler( new com.smartgwt.client.widgets.events.ClickHandler  ()
       
		{
            @Override
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                d.submitForm();
            }
        });
		
		winExportar.setWidth(400);  
		winExportar.setHeight(100);  
		winExportar.setTitle("Importar datos");  
		winExportar.setShowMinimizeButton(false);  
		winExportar.setIsModal(true);  
		winExportar.setShowModalMask(true);  
		winExportar.centerInPage();  
		winExportar.addCloseClickHandler(new CloseClickHandler() {  
		@Override
			public void onCloseClick(CloseClientEvent event) {
			 DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			 CreateExelDTO exel=new CreateExelDTO(fac, btnUpload.getDisplayValue());
			 winExportar.destroy(); 
			}    
		});
			
		VLayout vf = new VLayout();  
		vf.setSize("100%","100%"); 
	    vf.setPadding(5);  
	    vf.setLayoutAlign(VerticalAlignment.BOTTOM);
		vf.addMember(bimport);
	   
	 	d.setSize("100%", "100%");
		d.setFields(btnUpload );
		winExportar.addItem(d);
		winExportar.addItem(vf);
		winExportar.show();
	}
	
	
}
