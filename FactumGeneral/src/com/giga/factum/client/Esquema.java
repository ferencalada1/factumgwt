package com.giga.factum.client;

import com.smartgwt.client.widgets.Canvas;

public interface Esquema {
	Canvas create();
	String getID();
	String getDescription();
}