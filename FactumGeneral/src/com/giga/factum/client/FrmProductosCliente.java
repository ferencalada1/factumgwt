package com.giga.factum.client;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.regGrillas.TipoPrecioRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;

public class FrmProductosCliente extends VLayout
{
	ListGrid lstProductoCliente = new ListGrid();//+++ LISTADO GRANDE
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtStockMinimo =new TextItem("txtStockMinimo", "<b>Stock Min</b>");
	TextItem txtStockMaximo =new TextItem("txtStockMaximo", "<b>Stock Max</b>");
	TextItem txtProducto =new TextItem("txtProducto", "<b>Producto</b>");
	TextItem txtidCli =new TextItem("txtidCli", "<b>Cedula/RUC Cliente</b>");
	TextItem txtnomCli =new TextItem("txtnomCli", "<b>Nombre Comercial Cliente</b>");
	TextItem txtapeCli =new TextItem("txtapeCli", "<b>Razon Social Cliente</b>");
	//TextItem txtMarca =new TextItem("txtMarca", "<b>Marca</b>");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnGrabar = new IButton("Grabar");
	LinkedList <ProductoTipoPrecioDTO> disProTipPrecio = new LinkedList <ProductoTipoPrecioDTO>();//para grabar el dist
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	ListGrid lstPrecios = new ListGrid();//+++++ LISTADO PEQUE�O
	double PrecioCompraSeleccionado =0;
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	Date fechaFactura;// Para la fecha del sistema
	int contador=20;
	int registros=0;
	Label lblRegisros = new Label("# Registros");
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Window winClientes = new Window();
	frmListClientes frmlisCli;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Window winProducto;  
	ListProductos listProductos;
	String idProductoSeleccionado="";
	String idClienteSeleccionado="";
	
	 NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	public FrmProductosCliente() {
		getService().fechaServidor(callbackFecha);
	//++++++++++++++++++++++  Tipo Precio ++++++++++++++++++++++++++++++++++++++++
		try{
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
			ListGridField idProducto = new ListGridField("idProducto", "IdProducto");
			//idProducto.setHidden(true);
			ListGridField idTipoPrecio = new ListGridField("idTipo", "IdTipo");
			idTipoPrecio.setHidden(true);
			ListGridField TipoPrecio = new ListGridField("tipo", "Tipo");
			//TipoPrecio.setHidden(true);
			ListGridField precioC = new ListGridField("precioC", "Precio Compra");
			ListGridField precioV = new ListGridField("precioV", "Precio Venta");
			
			precioV.addChangeHandler(new ManejadorBotones(""));
			precioV.setCanEdit(true);
			ListGridField porcentaje = new ListGridField("porcentaje", "% Utilidad");
			lstPrecios.setFields(new ListGridField[] {idProducto,idTipoPrecio,TipoPrecio,precioC,precioV,porcentaje});
			lstPrecios.setSize("40%", "100%");
		dynamicForm.setSize("20%", "20%");
		dynamicForm2.setSize("20%", "20%");
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		  
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, txtStockMinimo,txtStockMaximo});
		//dynamicForm2.setFields(new FormItem[] { txtnomCli, txtapeCli, txtProducto,txtMarca});
		dynamicForm2.setFields(new FormItem[] { txtidCli, txtnomCli, txtapeCli, txtProducto});
		HStack hStackEspacio = new HStack();
		hStackEspacio.setSize("4%", "20%");
		HStack hStackEspacio1 = new HStack();
		hStackEspacio1.setSize("4%", "20%");
		
		btnGrabar.setSize("10%", "20%");
		btnGrabar.addClickHandler(new ClickHandler()
		{
	       public void onClick(ClickEvent event)
	       {		
	    	   int numRegTipo=lstPrecios.getRecords().length;
	    	   disProTipPrecio = new LinkedList <ProductoTipoPrecioDTO>();	       			
	    	   for (int i = 0; i<numRegTipo; i++)
	    	   {
	    		   ProductoDTO proDTO = new ProductoDTO();
	    		   proDTO.setIdProducto(Integer.valueOf(lstPrecios.getRecord(i).getAttribute("idProducto")));
	    		   proDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
	    		   proDTO.setEstablecimiento(Factum.user.getEstablecimiento());
	    		   TipoPrecioDTO tipDTO = new TipoPrecioDTO();
	    		   tipDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
	    		   tipDTO.setIdTipoPrecio(Integer.valueOf(lstPrecios.getRecord(i).getAttribute("idTipo")));
	    		   tipDTO.setTipoPrecio(lstPrecios.getRecord(i).getAttribute("tipo"));	    		   				
				   
	    		   ProductoTipoPrecioDTO proTipPre = new ProductoTipoPrecioDTO();	
	    		   proTipPre.setTblproducto(proDTO);
	    		   proTipPre.setTbltipoprecio(tipDTO);
	    		   proTipPre.setPorcentaje(Double.valueOf(lstPrecios.getRecord(i).getAttribute("precioV")));
	    		   disProTipPrecio.add(proTipPre);
			}//fin for
   			//		getService().grabarDistributivo2(disList, codigoProfesor,anioSistema,cicloSistema,objback);
	    	   //DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	    	   		getService().grabarProductoTipoPrecio(disProTipPrecio, objback);
   					
		           }
		       });
		HStack hStackSuperior = new HStack();
		hStackSuperior.setSize("100%", "20%");
		hStackSuperior.addMember(dynamicForm);
		hStackSuperior.addMember(dynamicForm2);
		hStackSuperior.addMember(hStackEspacio);
		hStackSuperior.addMember(lstPrecios);
		hStackSuperior.addMember(hStackEspacio1);
		//hStackSuperior.addMember(btnGrabar);
		
		VStack vStackFinal = new VStack();
		vStackFinal.setSize("10%", "100%");
		HStack hStackSup1 = new HStack();
		hStackSup1.setSize("100%", "35%");
	//	hStackSup1.setBackgroundColor("blue");
		HStack hStackSup2 = new HStack();
		hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
		btnGrabar.setSize("100%", "30%");
		
		
		vStackFinal.addMember(hStackSup1);
		vStackFinal.addMember(btnGrabar);
		vStackFinal.addMember(hStackSup2);
		
		hStackSuperior.addMember(vStackFinal);
		addMember(hStackSuperior);
		
		lstProductoCliente.setSize("100%", "60%");
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		//NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
		
		ListGridField Id =new ListGridField("idProducto", "<b>id</b>");
		Id.setWidth("1%");
		Id.setHidden(true);
		ListGridField Descripcion =new ListGridField("nomProducto", "<b>Descripci&oacute;n</b>");
		Descripcion.setWidth("36%");
		ListGridField Cantidad =new ListGridField("cantidad", "<b>Cantidad</b>");
		Cantidad.setWidth("5%");
		Cantidad.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		Cantidad.setAlign(Alignment.RIGHT);
//		Cantidad.setSummaryFunction(SummaryFunctionType.SUM);
		Cantidad.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("cantidad"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
                }
            }
        });
		Cantidad.setShowGridSummary(true);
		ListGridField Stock= new ListGridField("stock", "<b>Stock</b>");
		Stock.setWidth("5%");
		Stock.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		Stock.setAlign(Alignment.RIGHT);
		ListGridField PrecioCompra =new ListGridField("precioCompra", "<b>Precio Compra</b>");
		PrecioCompra.setWidth("7%");
		PrecioCompra.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		PrecioCompra.setAlign(Alignment.RIGHT);
		PrecioCompra.setShowGridSummary(true);
//		PrecioCompra.setSummaryFunction(SummaryFunctionType.SUM);
		
		PrecioCompra.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("precioCompra"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
                }
            }
        });
		ListGridField PrecioVenta =new ListGridField("precioVenta", "<b>Precio Venta</b>");
		PrecioVenta.setWidth("7%");
		PrecioVenta.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		PrecioVenta.setAlign(Alignment.RIGHT);
		PrecioVenta.setShowGridSummary(true);
//		PrecioVenta.setSummaryFunction(SummaryFunctionType.SUM);
		PrecioVenta.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("precioVenta"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
                }
            }
        });
		
		ListGridField IdPersona =new ListGridField("idPersona", "<b>idPersona</b>");
		IdPersona.setWidth("1%");
		IdPersona.setHidden(true);
		ListGridField cliente =new ListGridField("Cliente", "<b>Cliente</b>");
		cliente.setWidth("30%");
		ListGridField FechaCompra=new ListGridField("fechaCompra", "<b>Fecha de Compra</b>");
		FechaCompra.setWidth("8%");
		FechaCompra.setAlign(Alignment.CENTER);
		
		ListGridField vacio =new ListGridField("vacio", "&nbsp;&nbsp; ");
		vacio.setWidth("2%");
		
		lstProductoCliente.setFields(new ListGridField[] { Id,Descripcion, Cantidad,Stock,PrecioCompra, 
				PrecioVenta,vacio,IdPersona,cliente,FechaCompra});
		lstProductoCliente.setShowRowNumbers(true);
		lstProductoCliente.setShowGridSummary(true);
		lstProductoCliente.addSelectionChangedHandler(new SelectionChangedHandler() {  
            public void onSelectionChanged(SelectionEvent event) 
            {  
            	PrecioCompraSeleccionado=Double.valueOf(lstProductoCliente.getSelectedRecord().getAttribute("precioCompra"));
            	Integer codPro = Integer.valueOf(lstProductoCliente.getSelectedRecord().getAttribute("idProducto"));
				getService().listarTipoPrecio(codPro, objbacklstTipoPrecio);
        }  
    });
		addMember(lstProductoCliente);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		hStack.addMember(btnGenerarReporte);
		
		IButton btnLimpiar = new IButton("Limpiar");
		//IButton btnVerDocumento = new IButton("Ver Documento");
		txtStockMinimo.setValue("0");
		txtStockMaximo.setValue("10000");
		
		PickerIcon pckLimpiarProducto = new PickerIcon(PickerIcon.CLEAR);
		txtProducto.setIcons(pckLimpiarProducto);
		pckLimpiarProducto.addFormItemClickHandler(new ManejadorBotones("borrarProducto"));
		
		txtProducto.setValue("");
		txtProducto.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				listProductos = new ListProductos();
				listProductos.lstProductos.addDoubleClickHandler( new ManejadorBotones("producto"));
				winProducto=new Window();
				winProducto.setWidth(930);  
				winProducto.setHeight(610);  
				winProducto.setTitle("Seleccione un producto");  
				winProducto.setShowMinimizeButton(false);  
				winProducto.setIsModal(true);  
				winProducto.setShowModalMask(true);  
				winProducto.centerInPage();  
				winProducto.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winProducto.destroy();
                    }  
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                
                listProductos.setSize("100%","100%"); 
                form.addMember(listProductos);
                winProducto.addItem(form);
                winProducto.show();
			}
		});
		
		PickerIcon pckLimpiarCliente = new PickerIcon(PickerIcon.CLEAR);
		txtidCli.setIcons(pckLimpiarCliente);
		pckLimpiarCliente.addFormItemClickHandler(new ManejadorBotones("borrarCliente"));
		
		txtidCli.setValue("");
		txtidCli.setDisabled(false);
		txtidCli.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {				
				winClientes = new Window();
				winClientes.setWidth(930);
				winClientes.setHeight(610);
				winClientes.setShowMinimizeButton(false);
				winClientes.setIsModal(true);
				winClientes.setShowModalMask(true);
				winClientes.setKeepInParentRect(true);
				winClientes.centerInPage();
				winClientes.addCloseClickHandler(new CloseClickHandler() {
					@Override
					public void onCloseClick(CloseClientEvent event) {
						winClientes.destroy();

					}
				});
				VLayout form = new VLayout();
				form.setSize("100%", "100%");
				form.setPadding(5);
				form.setLayoutAlign(VerticalAlignment.BOTTOM);
				
				frmlisCli = new frmListClientes("tblclientes");
				frmlisCli.lstCliente.addDoubleClickHandler(new ManejadorBotones("cliente"));
				frmlisCli.setSize("100%", "100%");
				winClientes.setTitle("Buscar Cliente");
				form.addMember(frmlisCli);
			
				frmlisCli.txtBuscarLst.setSelectOnFocus(true);
				winClientes.addItem(form);
				winClientes.show();				
			}
		});
		txtnomCli.setValue("");
		txtnomCli.setDisabled(true);
		txtapeCli.setValue("");
		txtapeCli.setDisabled(true);
		//txtMarca.setValue("");
		btnGenerarReporte.setDisabled(false);
		btnGenerarReporte.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event){     	  
	        	getService().listarProductosCliente(txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(), Integer.valueOf(txtStockMinimo.getValue().toString()), Integer.valueOf(txtStockMaximo.getValue().toString()),
	        	idClienteSeleccionado,idProductoSeleccionado,objProductoCliente);
	//        	SC.say("proveeN: "+txtProveedorN.getDisplayValue().toString()+"proveeA: "+txtProveedorA.getDisplayValue().toString()+"producto: "+txtProducto.getDisplayValue().toString());
	   			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	           }
	       });
		btnLimpiar.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event)
	           {
	        	txtStockMinimo.setValue("0");
	   			txtStockMaximo.setValue("0");
	   			txtProducto.setValue("");
	   			txtidCli.setValue("");
	   			txtnomCli.setValue("");
	   			txtapeCli.setValue("");
	   			//txtMarca.setValue("");
	   			btnGenerarReporte.setDisabled(false);
	           }
	       });
		
	/*	btnVerDocumento.addClickHandler(new ClickHandler(){
	           @Override
	           public void onClick(ClickEvent event){
	           	
	          
	           }
	       });
*/
		hStack.addMember(btnLimpiar);
	//	hStack.addMember(btnVerDocumento);
		
		addMember(hStack);
		}catch(Exception e ){SC.say("Error: "+e);}
	}
	
	
	final AsyncCallback<List<ProductoCliente>>  objProductoCliente = new AsyncCallback<List<ProductoCliente>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			txtStockMinimo.setValue("0");
			txtStockMaximo.setValue("0");
		}
		public void onSuccess(List<ProductoCliente> resultProd) {
			ListGridRecord[] listadoProd = new ListGridRecord[resultProd.size()];
			int contadorProdPro=0;
			if (resultProd.size()>0)
			{	
				for(int i=0;i<resultProd.size();i++) 
				{		
					String[] descripciones =resultProd.get(i).getNomProducto().split(";");
					listadoProd[contadorProdPro]=new ListGridRecord();
					listadoProd[contadorProdPro].setAttribute("idProducto", resultProd.get(i).getIdProducto());
					listadoProd[contadorProdPro].setAttribute("nomProducto", descripciones[1]);
					listadoProd[contadorProdPro].setAttribute("cantidad", resultProd.get(i).getCantidad());
					listadoProd[contadorProdPro].setAttribute("stock", resultProd.get(i).getStock());
					listadoProd[contadorProdPro].setAttribute("precioCompra", resultProd.get(i).getPrecioCompra());
					listadoProd[contadorProdPro].setAttribute("precioVenta", resultProd.get(i).getPrecioVenta());
					listadoProd[contadorProdPro].setAttribute("idPersona", resultProd.get(i).getIdPersona());
					listadoProd[contadorProdPro].setAttribute("Cliente", "  "+resultProd.get(i).getCliente());
					listadoProd[contadorProdPro].setAttribute("fechaCompra", resultProd.get(i).getFechaCompra());
					contadorProdPro++;	
				}
			}
			else
			{
				SC.say("No hay productos que cumplan los parametros establecidos");
			}
			lstProductoCliente.setData(listadoProd);
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        lstProductoCliente.show();
	        lstProductoCliente.redraw();
		}
	};

 	 	
 	
 	final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
			
		}
		public void onSuccess(Date result) {
			fechaFactura=result;
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
		}
	};
	
	
//+++++++++++++++++++++++++++++++++++++++   TIPO DE PRECIOS    +++++++++++++++++++++++++++++++++++++++++++
	final AsyncCallback<List<ProductoTipoPrecio>>  objbacklstTipoPrecio = new AsyncCallback<List<ProductoTipoPrecio>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
			}
			public void onSuccess(List<ProductoTipoPrecio> resultTip) {
				ListGridRecord[] listadoTip = new ListGridRecord[resultTip.size()];
				int contadorTip=0;
				if (resultTip.size()>0){	
					int res = resultTip.size();
					for(int i=0; i<res; i++) 
					{
						Double pCompra = Double.valueOf(PrecioCompraSeleccionado);
						Double pVenta = Double.valueOf(resultTip.get(i).getPorcentaje());
						Double porcentaje = pVenta-pCompra;
						porcentaje = (porcentaje*100)/pCompra;
						porcentaje=Math.rint(porcentaje*100)/100;//redondear a 2 decimales
						listadoTip[contadorTip]=new ListGridRecord();
						listadoTip[contadorTip].setAttribute("idProducto", resultTip.get(i).getIdProducto());
						listadoTip[contadorTip].setAttribute("idTipo", resultTip.get(i).getIdTipo());
						listadoTip[contadorTip].setAttribute("tipo", resultTip.get(i).getTipo());
						listadoTip[contadorTip].setAttribute("precioC", String.valueOf(PrecioCompraSeleccionado));
						listadoTip[contadorTip].setAttribute("precioV", resultTip.get(i).getPorcentaje());
						listadoTip[contadorTip].setAttribute("porcentaje", String.valueOf(porcentaje));
						contadorTip++;
					}
					lstPrecios.show();
					lstPrecios.setData(listadoTip);
					lstPrecios.redraw();
				}
				else{
					SC.say("No existen tipos de precios");
					lstPrecios.hide();
				}		
			}
		};
		
		private class ManejadorBotones implements ChangeHandler, DoubleClickHandler, FormItemClickHandler
		{
		String indicador="";
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		@Override
		public void onChange(ChangeEvent event) {
			try{
				Double pCompra = Double.valueOf(PrecioCompraSeleccionado);
				Double pVenta = Double.valueOf(String.valueOf(event.getValue()));
				Double porcentaje = pVenta-pCompra;
				porcentaje = (porcentaje*100)/pCompra;
				porcentaje=Math.rint(porcentaje*100)/100;//redondear a 2 decimales
				lstPrecios.getSelectedRecord().setAttribute("porcentaje", String.valueOf(porcentaje));
				lstPrecios.refreshRow(lstPrecios.getEditRow());
			}catch(Exception e){
			//	SC.say(e.getMessage());
			}
		}
		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equals("producto")){
				idProductoSeleccionado=listProductos.lstProductos.getSelectedRecord().getAttribute("idProducto");
				txtProducto.setValue(listProductos.lstProductos.getSelectedRecord().getAttribute("descripcion"));
				winProducto.destroy();
			}else if(indicador.equals("cliente")){
				txtidCli.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("Cedula"));
				txtnomCli.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("NombreComercial"));
				txtapeCli.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("RazonSocial"));
				idClienteSeleccionado=frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("Cedula");
				winClientes.destroy();
			}
			
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equals("borrarCliente")){				
				idClienteSeleccionado="";
				txtidCli.setValue("");
				txtnomCli.setValue("");
				txtapeCli.setValue("");
			}else if(indicador.equals("borrarProducto")){
				idProductoSeleccionado="";
				txtProducto.setValue("");
			}
			
		} 
		}
		
		final AsyncCallback<String>  objback=new AsyncCallback<String>(){
				public void onFailure(Throwable caught) {
					SC.say("Error dado:" + caught.toString());
				}
				public void onSuccess(String result) {
					SC.say(result);
				}
			};
}
