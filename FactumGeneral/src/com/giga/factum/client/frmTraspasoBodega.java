package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.gwtext.client.widgets.form.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.events.CloseClickHandler;  
import com.smartgwt.client.widgets.events.CloseClientEvent;  
import com.smartgwt.client.widgets.layout.HStack;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.Producto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.types.Alignment;

public class frmTraspasoBodega extends HLayout {
	ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Bodega Origen");
	ComboBoxItem cbmBuscarLst2 = new ComboBoxItem("cbmBuscarLst2", "Bodega Destino");
	TextItem txtBuscarTxt;
	ListGridField cbmBodegalist = new ListGridField("cbmBodegalist", "Bodega",60);
	LinkedList<BodegaDTO> listBodegas=new LinkedList<BodegaDTO>();
	LinkedList<TraspasoBodegaProductoDTO> prodBodega=new LinkedList<TraspasoBodegaProductoDTO>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	ListGrid lstProductos = new ListGrid();
	ListGrid lstTraspaso = new ListGrid();
	
	int idBodega = 0;
	public frmTraspasoBodega() {
		setSize("900", "700");
		
		VLayout layout = new VLayout();
		layout.setSize("45%", "100%");
		SearchForm searchForm = new SearchForm();
		searchForm.setSize("100%", "5%");
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		cbmBuscarLst.setLeft(162);
	    cbmBuscarLst.setTop(20);
	    cbmBuscarLst.setHint("Seleccione una Bodega");
	    cbmBuscarLst.setShowHintInField(true);
	    
	    txtBuscarTxt=new TextItem("txtBuscarTxt","");
	    txtBuscarTxt.setLeft(6);
	    txtBuscarTxt.setTop(20);
	    txtBuscarTxt.setHint("Buscar");
	    txtBuscarTxt.setShowHintInField(true);
	    txtBuscarTxt.setIcons(buscarPicker);
	    
	    cbmBuscarLst2.setLeft(300);
	    cbmBuscarLst2.setTop(20);
	    cbmBuscarLst2.setHint("Seleccione una Bodega");
	    cbmBuscarLst2.setShowHintInField(true);
	    
	    buscarPicker.addFormItemClickHandler(new ManejadorBotones("buscar"));
	    txtBuscarTxt.addKeyPressHandler(new KeyPressHandler() {
			
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter"))
				{
					String mens=String.valueOf(listBodegas.size());
					int idbodega = 0;
					/*if(txtBuscarTxt.equals(""))
					{
						for(int i=0;i<listBodegas.size();i++){
							mens=mens+" "+listBodegas.get(i).getBodega();
							if(cbmBuscarLst.getDisplayValue().equals(listBodegas.get(i).getBodega())){
								idbodega=listBodegas.get(i).getIdBodega();
								break;
							}
						}
						getService().BuscarProductoJoin(String.valueOf(idbodega),String.valueOf(txtBuscarTxt.getValue()), listaCallback);
					
					}
					else
					{
						for(int i=0;i<listBodegas.size();i++){
							mens=mens+" "+listBodegas.get(i).getBodega();
							if(cbmBuscarLst.getDisplayValue().equals(listBodegas.get(i).getBodega())){
								idbodega=listBodegas.get(i).getIdBodega();
								break;
							}
						}
						getService().BuscarProductoJoin(String.valueOf(idbodega),String.valueOf(txtBuscarTxt.getValue()), listaCallback);
					}*/
					getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas",txtBuscarTxt.getDisplayValue(),2,1,0,-1,Factum.banderaStockNegativo, listaCallback);
					
				}
			}
		});
	    searchForm.setFields(new FormItem[] { cbmBuscarLst, txtBuscarTxt});
		searchForm.setNumCols(4);
		
		layout.addMember(searchForm);
		lstProductos.setSize("105%", "100%");
		ListGridField lstid = new ListGridField("idProducto", "idProducto",50);
		lstid.setAlign(Alignment.CENTER);
        ListGridField lstcodbarras = new ListGridField("codigoBarras", "C\u00F3digo de Barras",100);
        lstcodbarras.setCellAlign(Alignment.CENTER);
        ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",325); 
        ListGridField listStock = new ListGridField("stock", "Stock",50);
        ListGridField listCantidad = new ListGridField("cantidad", "Cantidad",50);
        listCantidad.setAlign(Alignment.CENTER);
        listStock.setCellAlign(Alignment.CENTER);
        lstProductos.setFields(new ListGridField[] { 
        		lstid,
        		lstcodbarras, 
    			lstdescripcion,
    			listStock
    			});
        
		layout.addMember(lstProductos);
		HStack hStack = new HStack();
		
		IButton btnGuardar = new IButton("Guardar");
		hStack.addMember(btnGuardar);
		
		layout.addMember(hStack);
		addMember(layout);
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("4%", "10%");
		layout_1.setDefaultLayoutAlign(Alignment.CENTER);
		
		VStack vStack = new VStack();
		vStack.setSize("100%", "45%");
		
		Canvas canvas = new Canvas();
		canvas.setHeight("270px");
		vStack.addMember(canvas);
		layout_1.addMember(vStack);
		
		IButton cmdAgregar = new IButton(">");
		cmdAgregar.addClickHandler(new ManejadorBotones("add"));
		layout_1.addMember(cmdAgregar);
		
		IButton cmdQuitar = new IButton("<");
		layout_1.addMember(cmdQuitar);
		cmdQuitar.addClickHandler(new ManejadorBotones("remove"));
		
		IButton cmdAnadirTodos = new IButton(">>");
		layout_1.addMember(cmdAnadirTodos);
		cmdAnadirTodos.addClickHandler(new ManejadorBotones("addall"));
		
		IButton cmdQuitaTodos = new IButton("<<");
		layout_1.addMember(cmdQuitaTodos);
		cmdQuitaTodos.addClickHandler(new ManejadorBotones("removeall"));
		
		addMember(layout_1);
		
		VLayout layout_2 = new VLayout();
		layout_2.setSize("48%", "100%");
		
		SearchForm searchTraspaso = new SearchForm();
		searchTraspaso.setFields(new FormItem[] {cbmBuscarLst2});
		layout_2.addMember(searchTraspaso);
		
		lstTraspaso.addRecordDoubleClickHandler(new ManejadorBotones("remove"));
		listCantidad.setCanEdit(true);
		cbmBodegalist.setCanEdit(true);
		lstTraspaso.setSize("110%", "70%");
		lstTraspaso.setAlign(Alignment.CENTER);
		lstTraspaso.setFields(new ListGridField[] { 
        		lstid,
        		lstcodbarras, 
    			lstdescripcion,
    			listCantidad,
    			cbmBodegalist,
    			});
		layout_2.addMember(lstTraspaso);
		addMember(layout_2);
		getService().listarBodega(0, 50, objbackbodega);
		cbmBuscarLst.addChangedHandler(new ChangedHandler() {  
			@Override
			public void onChanged(ChangedEvent event) {	
				//getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas", "nombre",2, 1, 0, -1,Factum.banderaStockNegativo,listaCallback);
				getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas",txtBuscarTxt.getDisplayValue(),2,1,0,-1,Factum.banderaStockNegativo, listaCallback);
			}  
        });
		cbmBuscarLst2.addChangedHandler(new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				lstTraspaso.setData(new ListGridRecord[]{});
				String nombre="";
				String nombreOrigen=cbmBuscarLst.getDisplayValue();
				String nombreDestino=cbmBuscarLst2.getDisplayValue();
				nombre=cbmBuscarLst.getValue().toString();
				if(nombreDestino.equals(nombreOrigen))
				{
				    cbmBuscarLst2.isDisabled().equals(nombre);
					SC.say("No puede trasladar ala misma Bodega de Origen");
				}
				
			}
		});
		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("add"));
		txtBuscarTxt.addKeyPressHandler(new ManejadorBotones("buscar"));
		btnGuardar.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
			        	String mensaje="";
			        	String dos=cbmBuscarLst2.getDisplayValue();
			        	prodBodega=new LinkedList<TraspasoBodegaProductoDTO>();
			        	try{
			        		for(int i=0;i<lstTraspaso.getRecords().length;i++){
				        		ProductoBodegaDTO probod=new ProductoBodegaDTO();
				        		TraspasoBodegaProductoDTO traspaso=new TraspasoBodegaProductoDTO();
				        		BodegaDTO bod=new BodegaDTO();
					        	if(lstTraspaso.getRecord(i).getAttribute("cbmBodegalist").equals(dos))
					        		{
					        			idBodega = Integer.valueOf(cbmBuscarLst2.getValue().toString());
					        			bod.setIdBodega(idBodega);
					        			SC.say("valor cbmbodega "+lstTraspaso.getRecord(i).getAttribute("cbmBodegalist"));
					        			
					        		}
					        	else
					        		{
					        		
					        			if(lstTraspaso.getRecord(i).getAttribute("cbmBodegalist").equals(dos))
					        			{
					        				idBodega = Integer.valueOf(cbmBuscarLst2.getValue().toString());
						        			bod.setIdBodega(idBodega);
					        			}
					        			else
					        			{
					        				bod.setIdBodega(Integer.parseInt(lstTraspaso.getRecord(i).getAttribute("cbmBodegalist")));
				        					
					        			}
					        		}
					        	bod.setIdEmpresa(Factum.empresa.getIdEmpresa());
								bod.setEstablecimiento(Factum.getEstablecimientoCero());
					        	probod.setTblbodega(bod);
					        	mensaje=mensaje+"bodegalist  "+String.valueOf(bod.getIdBodega());		
				        		probod.setCantidad(Integer.parseInt(lstTraspaso.getRecord(i).getAttribute("cantidad")));
				        		mensaje=mensaje+"  cantidad= "+probod.getCantidad();
				        		ProductoDTO pro=new ProductoDTO();
				        		pro.setIdEmpresa(Factum.empresa.getIdEmpresa());
								pro.setEstablecimiento(Factum.getEstablecimientoCero());
				        		pro.setIdProducto(Integer.parseInt(lstTraspaso.getRecord(i).getAttribute("idProducto")));
				        		mensaje=mensaje+" id producto"+pro.getIdProducto();
				        		probod.setTblproducto(pro);
				        		traspaso.setProductoBodega(probod);
				        		mensaje=mensaje+"cbmbodega   "+String.valueOf(cbmBuscarLst.getValue());
				        		traspaso.setIdBodega(Integer.parseInt((String) cbmBuscarLst.getValue()));
				        		prodBodega.add(traspaso);
				        	}
				        	getService().TransferirBodega(prodBodega, objback);
			        	}catch(Exception e){
			        		SC.say("error en pantalla .........."+ e.getMessage());
			        	}
			       
			        }
		});
		
	}

	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(cbmBuscarLst2.getValue().toString().equals("0"))
			{
				SC.say("Selecciones una Bodega Origen");
			}
			else{
				
				if(indicador.equals("add")){
					String id="";
					idBodega = Integer.valueOf(cbmBuscarLst2.getValue().toString());
					id=cbmBuscarLst2.getValue().toString();
					String nombreOrigen=cbmBuscarLst.getDisplayValue();
					String nombreDestino=cbmBuscarLst2.getDisplayValue();
					if(nombreDestino.equals(nombreOrigen))
					{
						SC.say("No se puede traspasar ala misma Bodega");
					}
					else
					{
						if(idBodega!=0){	
		
							ListGridRecord nuevo =new ListGridRecord();
							nuevo.setAttribute("idProducto", (lstProductos.getSelectedRecord().getAttributeAsString("idProducto")));
							nuevo.setAttribute("codigoBarras", lstProductos.getSelectedRecord().getAttribute("codigoBarras"));
							nuevo.setAttribute("descripcion", lstProductos.getSelectedRecord().getAttribute("descripcion"));
							nuevo.setAttribute("cbmBodegalist", MapBodega.get(id));
							lstTraspaso.addData(nuevo);
							lstTraspaso.selectRecord(lstTraspaso.getRecords().length);
							SC.say("Selecciones una Bodega Origen"+cbmBuscarLst2.getValue().toString());
						}
						
						else SC.say("Seleccione una Bodega Destino para Continuar");
					}
				}
			}
		}

		public void onFormItemClick(FormItemIconClickEvent event) {
			String mens=String.valueOf(listBodegas.size());
			int idbodega = 0;
			for(int i=0;i<listBodegas.size();i++){
				mens=mens+" "+listBodegas.get(i).getBodega();
				if(cbmBuscarLst.getDisplayValue().equals(listBodegas.get(i).getBodega())){
					idbodega=listBodegas.get(i).getIdBodega();
					break;
				}
			}
			//getService().BuscarProductoJoin(String.valueOf(idbodega),String.valueOf(txtBuscarTxt.getValue()), listaCallback);
			getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas",txtBuscarTxt.getDisplayValue(),2,1,0,-1,Factum.banderaStockNegativo, listaCallback);
		}
		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equals("enter")){
				String mens=String.valueOf(listBodegas.size());
				int idbodega = 0;
				for(int i=0;i<listBodegas.size();i++){
					mens=mens+" "+listBodegas.get(i).getBodega();
					if(cbmBuscarLst.getDisplayValue().equals(listBodegas.get(i).getBodega())){
						idbodega=listBodegas.get(i).getIdBodega();
						break;
					}
				}
				//getService().BuscarProductoJoin(String.valueOf(idbodega),String.valueOf(txtBuscarTxt.getValue()), listaCallback);
				getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas",txtBuscarTxt.getDisplayValue(),2,1,0,-1,Factum.banderaStockNegativo, listaCallback);
			}
		}

		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equals("add")){
				String id="";
				idBodega = Integer.valueOf(cbmBuscarLst2.getValue().toString());
				id=cbmBuscarLst2.getValue().toString();
				String nombreOrigen=cbmBuscarLst.getDisplayValue();
				String nombreDestino=cbmBuscarLst2.getDisplayValue();
				if(nombreDestino.equals(nombreOrigen))
				{
					SC.say("No se puede traspasar ala misma Bodega");
				}
				else
				{
					if(idBodega!=0){
						
						ListGridRecord nuevo =new ListGridRecord();
						nuevo.setAttribute("idProducto", (lstProductos.getSelectedRecord().getAttributeAsString("idProducto")));
						nuevo.setAttribute("codigoBarras", lstProductos.getSelectedRecord().getAttribute("codigoBarras"));
						nuevo.setAttribute("descripcion", lstProductos.getSelectedRecord().getAttribute("descripcion"));
						nuevo.setAttribute("cbmBodegalist", MapBodega.get(id));
						lstTraspaso.addData(nuevo);
						lstTraspaso.selectRecord(lstTraspaso.getRecords().length);
					}
					else if (cbmBuscarLst2.equals(""))SC.say("Seleccione una Bodega Destino para Continuar");
				}
					
	        }  else if(indicador.equals("remove")){
	        	lstTraspaso.removeSelectedData();
	        }if(indicador.equals("addall")){
	        	String id="";
	        	id=cbmBuscarLst2.getValue().toString();
	        	
	        	for(int i=0;i<lstProductos.getRecords().length;i++){
		        	ListGridRecord nuevo =new ListGridRecord();
					nuevo.setAttribute("idProducto", (lstProductos.getRecord(i).getAttribute("idProducto")));
					nuevo.setAttribute("codigoBarras", (lstProductos.getRecord(i).getAttribute("codigoBarras")));
					nuevo.setAttribute("descripcion", (lstProductos.getRecord(i).getAttribute("descripcion")));
					nuevo.setAttribute("cbmBodegalist", MapBodega.get(id));
					lstTraspaso.addData(nuevo);
					lstTraspaso.selectRecord(lstTraspaso.getRecords().length);
	        	}
//	        	lstProductos.getRecords();
//	        	lstTraspaso.setRecords(lstProductos.getRecords());
// 
	        }else if(indicador.equals("removeall")){
	        	limpiar();
	        }
	    }
	}
	public void limpiar(){
		int filas=lstTraspaso.getRecords().length;
		for(int i=0;i<filas;i++){
			lstTraspaso.removeData(lstTraspaso.getRecord(0));
		}
		lstTraspaso.redraw();
		
		
	}
	final AsyncCallback<List<BodegaDTO>>  objbackbodega=new AsyncCallback<List<BodegaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}
		public void onSuccess(List<BodegaDTO> result) {
			MapBodega.clear();
			MapBodega.put("","");
            for(int i=0;i<result.size();i++) {
            	listBodegas.add(result.get(i));
            	MapBodega.put(String.valueOf(result.get(i).getIdBodega()), 
						result.get(i).getBodega());	
			}
            cbmBuscarLst.setValueMap(MapBodega);
            cbmBuscarLst2.setValueMap(MapBodega);
            cbmBodegalist.setValueMap(MapBodega);
            
       }
	};
	final AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(LinkedHashMap<Integer,ProductoDTO> result) {
			RecordList listado = new RecordList();
			Iterator iter = result.values().iterator();
            //Size = equiposenTaller.size();
            //ListGridRecord[] allrecords = new ListGridRecord[Size];
            while (iter.hasNext()) {
            	listado.add(new Producto((ProductoDTO)iter.next()));
            }
            
            lstProductos.setData(listado);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
            lstProductos.redraw();
		}
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("No se conecta con la base "+caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas",txtBuscarTxt.getDisplayValue(),2,1,0,-1,Factum.banderaStockNegativo, listaCallback);
			//getService().listarProductoJoin2(cbmBuscarLst.getDisplayValue(), "tblbodegas", "nombre",2,1, 0, -1,Factum.banderaStockNegativo,listaCallback);
			limpiar();
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
