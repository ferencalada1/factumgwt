package com.giga.factum.client;

import java.io.IOException;

import java.util.List;
//import javax.xml.bind.JAXBElement;
//import java.util.Properties;

import com.gargoylesoftware.htmlunit.util.Cookie;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Factum implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	int i=0;
	Integer intentos=0;
	
	//**********************************++**
	//VARIABLES DE TALLER
	public static User user;
	public static String Usuario;
	public static String UserName;
	public static String Password;
	public static String idusuario;
	public static String planCuenta;
	public static Integer nivelAcceso;
	public static String Menu;
	public static String MontoFact;
	public static boolean loggeado;
	public static String idorden;
	public static String idcliente;
	
	//***************************************
	
	//--CONFIGURACION GENERAL
	public static String loginResponse;
	
	public static String banderaSO;//VARIABLE ENCARGADA DE DIFERENCIAR EL SO SOBE EL CUAL ESTA MONTADO EL SISTEMA linux-windows
	public static String banderaIpServidor;
	public static String banderaIpImpresion;
	public static String banderaIpImpresionFactura;
	public static String banderaIpImpresionProforma;
	public static String banderaIpImpresionNota;
	public static String banderaIpImpresionNotaVentas;
	public static String banderaIpImpresionNotaCredito;
	public static String banderaPuertoServidor;
	public static String banderaPuertoZuul;
	public static String banderaNombreProyecto;
	public static String banderaNombreProyectoTaller;
	public static String banderaImagenes;
	public static String banderaNombreBD;
	public static String banderaPasswordBD;
	public static String banderaUsuarioBD;
	public static int banderaDividirFactura; // Dividir la factura cuando los items son mayores al numero de items permitidos en la factura
	public static String banderaPasswordComprimido;
	public static String banderaNombreEmpresa;
	public static String banderaRUCEmpresa;
	public static String banderaDireccionEmpresa;
	public static String banderaTelefonoCelularEmpresa;
	public static String banderaTelefonoConvencionalEmpresa;
	public static String banderaNombreLogo;
	public static String banderaNombreCuerpo;
	public static String banderaLogo;
	public static int banderaClienteFactura;
	public static String banderaServidorFactum;
	//*--CONFIGURACION DE FUNCIONALIDADES
	
	//CONFIGURACION DE MENU
	public static String destinoBackup; //COLOCAMOS EL NOMBRE DE LA UNIDAD SOBRE LA CUAL SE VA A ALMACENAR EL ARCHIVO DE BACKUP
	
	public static int banderaMenuTaller=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS TALLER Y CUANDO COLOCAMOS EL 1 HABILITAMOS TALLER
	public static int banderaMenuBackup=1; //(Todos)CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS BACKUP Y CUANDO COLOCAMOS EL 1 HABILITAMOS BACKUP
	//public static int banderaMenuRestaurant=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS RESTAURANT Y CUANDO COLOCAMOS EL 1 HABILITAMOS RESTAURANT
	public static int banderaMenuCore=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS SERVICIOS Y CUANDO COLOCAMOS EL 1 HABILITAMOS SERVICIOS
	
	
	public static int banderaMenuServicio=1;//(Todos)CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS SERVICIOS Y CUANDO COLOCAMOS EL 1 HABILITAMOS SERVICIOS
	public static int banderaMenuBodega=1;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS BODEGAS Y CUANDO COLOCAMOS EL 1 HABILITAMOS BODEGAS
	public static int banderaMenuContabilidad=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS CONTABILIDAD Y CUANDO COLOCAMOS EL 1 HABILITAMOS CONTABILIDAD
	public static int banderaMenuProforma=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS PROFORMA Y CUANDO COLOCAMOS EL 1 HABILITAMOS PROFORMA
	public static int banderaMenuCuentasPorCobrar=1;//(Todos)CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS CUENTAS POR COBRAR Y CUANDO COLOCAMOS EL 1 HABILITAMOS CUENTAS POR COBRAR
	public static int banderaMenuLibroDiario=0;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS LIBRO DIARIO Y CUANDO COLOCAMOS EL 1 HABILITAMOS LIBRO DIARIO
	public static int banderaMenuFacturacionElectronica=1;//CUANDO COLOCAMOS EL NUMERO CERO NO HABILITAMOS MENU DE FACTURACION ELECTRONNICA Y CUANDO COLOCAMOS EL 1 HABILITAMOS LA FACTURACION ELECTRONICA
	//--CONFIGURACION DE PRODUCTOS
	public static int banderaCatalogoProductos=0;	//CUANDO SE REQUIERE AGREGAR UN IMAGENES A LOS PRODUCTOS 0-NO CATALOGO DE PRODUCTO, 1-AGREGAMOS CATALOGO DE PRODUCTOS
	public static int banderaProduccionProductos=1;	//CUANDO SE REQUIERE AGREGAR UN IMAGENES A LOS PRODUCTOS 0-NO PRODUCCION PRODUCTO, 1-AGREGAMOS PRODUCCION PRODUCTOS
	
	//--CONFIGURCION DE FACTURACION
	public static int banderaConfiguracionImpresion; //CUANDO SE REQUIERE SEPARAR ENTRE IMPRESION DIRECTA E IMPRESION PARA SELECCIONAR IMPRESORAS 0-IMPRESION PRINTVIEW, 1-IMPRESION DIRECTA, 3-MIXTA  
	public static int banderaConfiguracionTipoFacturacion; //EN FUNCION DEL TIPO DE FACTURACION SE INGRESA EL DOCUMENTO COMO CONFIRMADO 0-FACTURACION RAPIDA, 1-FACTURACION POR CONFIRMACION
	public static int banderaNumeroItemsFactura;//NUMERO DE ITEMS QUE PUEDEN INGRESAR EN UN FACTURA
	public static int banderaConfiguracionImpresionFactura;//EN CASO DE banderaConfiguracionImpresion=3 -> 0-IMPRESION PRINTVIEW 1-IMPRESION DIRECTA
	public static int banderaNumeroItemsNotaEntrega;//NUMERO DE ITEMS QUE PUEDE INGRESAR EN UNA NOTA DE VENTA
	public static int banderaConfiguracionImpresionNotaCredito;////EN CASO DE banderaConfiguracionImpresion=3 -> 0-IMPRESION PRINTVIEW 1-IMPRESION DIRECTA
	public static int banderaConfiguracionImpresionNota;////EN CASO DE banderaConfiguracionImpresion=3 -> 0-IMPRESION PRINTVIEW 1-IMPRESION DIRECTA
	public static int banderaNumeroItemsProforma;//NUMERO DE ITEMS QUE PUEDE INGRESAR EN UNA NOTA DE VENTA
	public static int banderaNumeroItemsNotaCredito;//NUMERO DE ITEMS QUE PUEDE INGRESAR EN UNA NOTA DE VENTA
	public static int banderaNumeroItemsRetencion;//NUMERO DE ITEMS QUE PUEDE INGRESAR EN UNA NOTA DE VENTA
	public static int banderaNumeroItemsNotaVentas;
	public static int banderaConfiguracionImpresionRetencion;////EN CASO DE banderaConfiguracionImpresion=3 -> 0-IMPRESION PRINTVIEW 1-IMPRESION DIRECTA
	public static int banderaNumeroDecimales;//NUMERO DE DECIMALES A UTILIZAR
	public static int banderaConfiguracionImpresionPago;
	
	public static int banderaConfiguracionImpresionNotaVentas;
	
	public static String banderaImprecionPHPFactura;//ARCHIVO PHP PARA FACTURA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPNota;//ARCHIVO PHP PARA NOTA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPNotaVentas;//ARCHIVO PHP PARA NOTA DE VENTA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPNotaCredito;//ARCHIVO PHP PARA NOTA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPRetencion;//ARCHIVO PHP PARA NOTA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPProforma;//ARCHIVO PHP PARA NOTA DE IMPRESION DIRECTA
	public static String banderaImprecionPHPAjuste;//ARCHIVO PHP PARA NOTA DE IMPRESION DIRECTA
	public static int banderaElegirVendedor;//CUANDO SE REQUIERE ASIGNAR LA FACTURA A UN VENDEDOR EN ESPECIFICO 0-NO PUEDE ELEGIR, 1-PUEDE ELEGIR
	public static int banderaCambiarPVP;//CUANDO SE REQUIERE MODIFICAR EL PVP DE UN PRODUCTO EN ESPECIFICO 0-NO PUEDE ELEGIR, 1-PUEDE ELEGIR
	public static int banderaCambiarDescuento;//CUANDO SE REQUIERE MOFIICAR EL DESCUENTO EN ESPECIFICO 0-NO PUEDE ELEGIR, 1-PUEDE ELEGIR
	public static int banderaFacturarMayorista;//CUANDO SE REQUIERE PERMITIR QUE SE PUEDA FACTURAR A MAYORISTA 0-NO PUEDE ELEGIR, 1-PUEDE ELEGIR	
	public static int banderaImportar;
	public static int banderaExportar;
	
	
	
	public static int banderaImpuestoAdicional;//CUANDO SE REQUIERE APLICAR UN IMPUESTO ADICIONAL AL MOMENTO DE FACTURAR 0-APLICAR IMPUESTO, 1-NO APLICAR IMPUESTO
	public static int banderaPorcentajeimpuestoAdicional;//EL PROCENTAJE QUE SE APLICA AL NUEVO IMPUESTO
	public static int banderaMicroServicios; //0 Acceso a microservicios desde dashboard, 1 acceso a microservicios local
	public static String banderaNombreimpuestoAdicional;//EL NOMBRE DEL IMPUESTO A IMPONER
	public static int banderaCalculoimpuestoAdicional;//BANDERA PARA IDENTIFICAR SOBRE QUE SE CALCULA EL IMPUESTO 0-IMPUESTO INCLUIDO EN EL PRECIO PRODUCTO(DESGLOSAR DEL PRECIO) 1-CALCULAR DEL SUBTOTAL

	//--CONFIGURACION DE FACTURACION ELECTRONICA
	public static String banderaNombrePlugFacturacionElectronica;//NOMBRE DEL PLUG DE FACTURACION ELECTRONICA
	public static String banderaRutaCertificado;
	//public static String banderaRutaCertificado="C:/Program Files/Java/jdk1.8.0_92/jre/lib/security/cacerts";
	public static String banderaAmbienteFacturacionElectronica;//AMBIENTE DE ENVIO DE FACTURA 1-PRUEBAS, 2-PRODUCCION
	
	
	public static EmpresaDTO empresa;
	
//--CONFIGURACION TALLER
	public static String banderaDeclaracionEmpresa="1. El cliente manifiesta que el equipo que consta en esta orden de trabajo es de su propiedad y que posee los documentos de respaldo, por lo que asume total responsabilidad sobre dicho equipo ingresado a servicio tecnico." +"<br/>"+
	"2. Garant&iacute;a de 60 d&iacute;as, excepto en equipos sometidos a humedad." +"<br/>"+
	"3. La garant&iacute;a no ser&1acute; v&aacute;lida se el sello de seguridad ha sido retirado o tiene indicios de maniplaci&iocute;an."+"<br/>"+
	"4. La informaci&oacute;n de datos o contactos es de exclusiva responsabilidad del propietario, por lo que se recomienda respaldar la misma."+"<br/>"+
	"5. El cliente acepta que el equipo ingresado a este servicio t&eacute;cnico tiene el riesgo de apagarse, definitivamente, cuando ha sido manipulado o sometido a humedad, software no autorizado, impactos fuertes, etc."+"<br/>"+
	"6. Equipos no retirados dentro de 45 d&iacute;as posteriores a la notificaci&oacute;n, se cargar&aacute; un valor de US$5,00 por concepto de bodegaje, adicional a los costos de reparaci&oacute;n."+"<br/>"+
	"7. El plazo máximo de retiro de equipos es de 60 d&iacute;as, pasado este tiempo, el equipo ser&aacute; considerado an abandono, el mismo que se entiende cedido como compensaci&oacute;n por costos de reparaci&oacute;n y bodegaje."+"<br/>"+
	"8. El cliente acepta expresamente todas las condiciones de servicio t&eacute;cnico al suscribir esta orden de trabajo."+"<br/>";

			
	//--CONFIGURACION DE IVA ECUADOR
	public static double banderaIVA;

	public static int banderaStockNegativo; //Si es 1 se permite facturar con stock en 0 o negativo, si es 0 solo se permite facturar con stock positivo
	public static int banderaBodegaUbicacion; //Si es uno se coloca la ubicacion de la bodega previo al nombre de la misma en la ventana factura
	public static int banderaBodegaDefecto; // Si es uno los productos se guardan por defecto en la primera bodega si es 0 se debe asignar en una bodega
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	public static int separarValoresInicio; //Indica si se debe separar numeros de producto colocados al principio de una descripcion
	
	public static int banderaModificarNumFactura; //En uno se puede modificar el numero de factura
	public static int banderaModificarNumNota; //En uno se puede modificar el numero de nota
	public static int banderaModificarNumNotaC; //En uno se puede modificar el numero de nota de credito
	public static int banderaModificarNumNotaV; //En uno se puede modificar el numero de nota de venta
	//Retencion
	public static int banderaModificarNumeroRetencion; // 0 PARA NO PODER MODIFICAR EL NUMERO DE RETENECI�N, 1 PARA PODER MODIFICAR EL NUMERO DE RETENCI�N
		
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	/**
	 * This is the entry point method.
	 */
	
	public void onModuleLoad() {		
		//try{
		try {
//			Properties props= new Properties();
//			props.load(getClass().getResourceAsStream("factum.properties"));
//			SC.say(props.getProperty("port"));
			FactumProp props = (FactumProp) GWT.create(FactumProp.class);
			//SC.say(props.port());
			getService().LeerXML(asyncCallbackXML);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//}
		/*catch(Exception e){
			SC.warn(e.getMessage());
		}*/
		
	}	
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
		}
	final AsyncCallback<String> asyncCallback=new AsyncCallback<String>() {
		
		@Override
		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}
	};
	final AsyncCallback<List<String>> asyncCallbackXML=new AsyncCallback<List<String>>() {
		
		@Override
		public void onSuccess(List <String> result) {
			// TODO Auto-generated method stub
			//Factum.banderaSO=result.get(0);
			//SC.say(banderaSO);
			//if (result!=null){
				Factum.banderaSO=result.get(0);
				Factum.banderaIpServidor=result.get(1);
				Factum.banderaIpImpresion=result.get(2);
				Factum.banderaIpImpresionFactura=result.get(3);
				Factum.banderaIpImpresionProforma=result.get(4);
				Factum.banderaIpImpresionNota=result.get(5);
				Factum.banderaIpImpresionNotaCredito=result.get(6);
				Factum.banderaPuertoServidor=result.get(7);
				Factum.banderaNombreProyecto=result.get(8);
				Factum.banderaNombreProyectoTaller=result.get(9);
				Factum.banderaImagenes=result.get(10);
				Factum.banderaConfiguracionImpresionPago=Integer.valueOf(result.get(11));
				Factum.banderaPuertoZuul=result.get(12);
				
				Factum.banderaNombreBD="bdfactumcompleto31";
				Factum.banderaPasswordBD="3tk98M17";
				Factum.banderaUsuarioBD="factumadmin";
				Factum.banderaDividirFactura=Integer.valueOf(result.get(13));
				//SC.say(Factum.banderaUsuarioBD);
				
				Factum.banderaPasswordComprimido="backup365.smart:gigacomputers";
				Factum.banderaNombreEmpresa=result.get(14);
				Factum.banderaRUCEmpresa=result.get(15);
				Factum.banderaDireccionEmpresa=result.get(16);
				Factum.banderaTelefonoCelularEmpresa=result.get(17);
				Factum.banderaTelefonoConvencionalEmpresa=result.get(18);
				Factum.banderaNombreLogo=result.get(19);
				//SC.say(banderaNombreLogo);
				Factum.banderaNombreCuerpo=result.get(20);
				Factum.banderaLogo=result.get(21);
				Factum.banderaClienteFactura=Integer.valueOf(result.get(22));
				Factum.banderaServidorFactum="http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor;
				Factum.destinoBackup=result.get(23);
				Factum.banderaConfiguracionImpresion=Integer.valueOf(result.get(24));
				Factum.banderaConfiguracionTipoFacturacion=Integer.valueOf(result.get(25));
				Factum.banderaNumeroItemsFactura=Integer.valueOf(result.get(26));
				Factum.banderaConfiguracionImpresionFactura=Integer.valueOf(result.get(27));
				Factum.banderaNumeroItemsNotaEntrega=Integer.valueOf(result.get(28));
				Factum.banderaConfiguracionImpresionNotaCredito=Integer.valueOf(result.get(29));
				Factum.banderaConfiguracionImpresionNota=Integer.valueOf(result.get(30));
				Factum.banderaNumeroItemsProforma=Integer.valueOf(result.get(31));
				Factum.banderaNumeroItemsNotaCredito=Integer.valueOf(result.get(32));
				Factum.banderaNumeroItemsRetencion=Integer.valueOf(result.get(33));
				Factum.banderaConfiguracionImpresionRetencion=Integer.valueOf(result.get(34));
				Factum.banderaNumeroDecimales=Integer.valueOf(result.get(35));
				//SC.say(String.valueOf(banderaNumeroDecimales));
				Factum.banderaImprecionPHPFactura=result.get(36);
				Factum.banderaImprecionPHPNota=result.get(37);
				Factum.banderaImprecionPHPNotaCredito=result.get(38);
				Factum.banderaImprecionPHPRetencion=result.get(39);
				Factum.banderaImprecionPHPProforma=result.get(40);
				Factum.banderaImprecionPHPAjuste=result.get(41);
				Factum.banderaElegirVendedor=Integer.valueOf(result.get(42));
				Factum.banderaCambiarPVP=Integer.valueOf(result.get(43));
				Factum.banderaCambiarDescuento=Integer.valueOf(result.get(44));
				Factum.banderaFacturarMayorista=Integer.valueOf(result.get(45));
				Factum.banderaImportar=Integer.valueOf(result.get(46));
				Factum.banderaExportar=Integer.valueOf(result.get(47));
				Factum.banderaImpuestoAdicional=Integer.valueOf(result.get(48));
				Factum.banderaPorcentajeimpuestoAdicional=Integer.valueOf(result.get(49));
				Factum.banderaMicroServicios=Integer.valueOf(result.get(50));
				Factum.banderaNombreimpuestoAdicional="SERVICIO "+banderaPorcentajeimpuestoAdicional+"%";
				Factum.banderaCalculoimpuestoAdicional=Integer.valueOf(result.get(51));
				Factum.banderaNombrePlugFacturacionElectronica=result.get(52);
				Factum.banderaRutaCertificado=result.get(53);
				Factum.banderaAmbienteFacturacionElectronica=result.get(54);
				Factum.banderaIVA=Integer.valueOf(result.get(55));
				Factum.banderaStockNegativo=Integer.valueOf(result.get(56));
				Factum.banderaBodegaUbicacion=Integer.valueOf(result.get(57));
				Factum.banderaBodegaDefecto=Integer.valueOf(result.get(58));
				Factum.separarValoresInicio=Integer.valueOf(result.get(59));
				Factum.banderaModificarNumFactura=Integer.valueOf(result.get(60));
				Factum.banderaModificarNumNota=Integer.valueOf(result.get(61));
				Factum.banderaModificarNumNotaC=Integer.valueOf(result.get(62));
				Factum.banderaConfiguracionImpresionNotaVentas=Integer.valueOf(result.get(63));
				Factum.banderaImprecionPHPNotaVentas=result.get(64);
				Factum.banderaNumeroItemsNotaVentas=Integer.valueOf(result.get(65));
				Factum.banderaIpImpresionNotaVentas=result.get(66);
				Factum.banderaModificarNumNotaV=Integer.valueOf(result.get(67));
				Factum.banderaModificarNumeroRetencion=Integer.valueOf(result.get(68));
				//com.google.gwt.user.client.Window.alert("Parametros leidos");
				getUserStatus();
			/*}else{
				com.google.gwt.user.client.Window.alert("El servicio no esta registrado, se enceuntra fuera de servicio o fuera de fecha de suscripcion");
				Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
			}*/
			
		}

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.warn("Error: "+caught.getMessage(),null);
		}
	};

			
	public static String getEstablecimientoCero(){
		return empresa.getEstablecimientos().get(0).getEstablecimiento();
	}
	public static String getPuntoEmisionCero(){
		return empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision();
	}		
//	public String getEstablecimientoCero(){
//		return empresa.getEstablecimientos().get(0).getEstablecimiento();
//	}
//	public String getPuntoEmisionCero(){
//		return empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision();
//	}		
		
		
	private void getUserStatus(){
		final AsyncCallback<String> sessionCallback=new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				if (result!=null){
					String[] sessions = result.split(";");
					String url="http://"+Factum.banderaIpServidor+":9999/login/get_username?cookie=";
	//				String 
	//				url="http://"+Factum.banderaIpServidor+":9999/login/get_username_json";
					String 
					cookiesD="cookie:[{";
					String 
					jsession="";
					for (int i=0; i<sessions.length;i++){
						String[] cookie=sessions[i].split("=");
	//					Cookies.setCookie(cookie[0], cookie[1]);
						url+=cookie[0]+":"+ cookie[1];
						cookiesD+='"'+cookie[0]+'"'+':'+'"'+ cookie[1]+'"';
						if (cookie[1].length()==32) {
							jsession=cookie[1];
	//						Cookies.setCookie("JSESSIONID=", cookie[1]);
						}
						if (i<sessions.length-1) {
							url+="-";
							cookiesD+=",";
						}
					}
					cookiesD+="}]";
					
					RequestBuilder 
					rb = new RequestBuilder(RequestBuilder.GET,url);
					//rb.setRequestData("Cookie:{JSESSIONID="+jsession+"}");
					//rb.setHeader("Cookie", "JSESSIONID=" +jsession);
	
	
						//			//com.google.gwt.user.client.Window.alert("SessionId "+result);
	
						try {
	//						rb.setHeader("Access-Control-Allow-Origin", "*");
	//					    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
	//					    rb.setHeader("Access-Control-Max-Age", "3600");
	//					    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
							rb.sendRequest(null, new RequestCallback() {
								public void onError(
										final com.google.gwt.http.client.Request request,
										final Throwable exception) {
									SC.say("Error no hay usuario "
											+ exception+ "request "+request.toString());
								}
	
								public void onResponseReceived(
										final com.google.gwt.http.client.Request request,
										final Response response) {
	
									//com.google.gwt.user.client.Window.alert("Response: "+response.getText());
															
									loginResponse= response.getText();
									boolean enabled=false;
	//								//com.google.gwt.user.client.Window.alert("Response enabled0: "+enabled);
									//com.google.gwt.user.client.Window.alert("Response enabled0: "+JSONParser.parseLenient(loginResponse).isObject());
									JSONObject respuesta=JSONParser.parseLenient(loginResponse).isObject();
									
									//com.google.gwt.user.client.Window.alert("Response respuesta: "+respuesta);
									enabled=respuesta.get("enabled").isBoolean().booleanValue();
									//com.google.gwt.user.client.Window.alert("Response enabled: "+enabled);
									if (enabled) {
										//						//com.google.gwt.user.client.Window.alert("enabled "+String.valueOf(enabled));
										//com.google.gwt.user.client.Window.alert("Response: "+respuesta.get("idUsuario").isString().stringValue());
										Factum.loggeado=true;
										Factum.Usuario=respuesta.get("idUsuario").isString().stringValue();
										//com.google.gwt.user.client.Window.alert(Factum.Usuario);
	//								String xuser = Cookies.getCookie("user");
	//								String xpassword = Cookies.getCookie("password");
	//								String xmenu = Cookies.getCookie("menu");
	//								String xmonto = Cookies.getCookie("monto");
	//								String xidusuario = Cookies.getCookie("idUsuario");
	//								String xidcliente = Cookies.getCookie("idCliente");
	//								String xidorden = Cookies.getCookie("idOrden");
	//								String xplanCuenta = Cookies.getCookie("planCuenta");
	
	
										////com.google.gwt.user.client.Window.alert(loginResponse);
	//									Factum.loggeado=true;
	//									Factum.Usuario = xuser;
	//									Factum.Password = xpassword;
	//									Factum.idusuario = xidusuario;
	//									Factum.idorden = xidorden;
	//									Factum.idcliente = xidcliente;
	//									Factum.planCuenta=xplanCuenta;
	//									if(xmenu!=null){
	//										Factum.Menu = xmenu;
	//										Factum.MontoFact = xmonto;
	//									}			
	
										//SC.say("planCuenta: "+Factum.planCuenta);
										RootPanel rootPanel = RootPanel.get();
										rootPanel.setSize("100%", "100%");
										Contenedor cont=new Contenedor();
										cont.setSize("100%", "100%");
										rootPanel.add(cont, 0, 0);
	
	
										Timer timer = new Timer() {
											@Override
											public void run() {
	//										if (i == 30) {
													updateStatus();
	//											i = 0;
	//										}
	//
	//										i++;
											}
										};
										timer.scheduleRepeating(30000);
									}else{
										//com.google.gwt.user.client.Window.alert("No enabled ");
										Factum.loggeado = false;
										Factum.Usuario = null;
										Factum.Password = null;
										Factum.Menu = null;
										Factum.MontoFact = null;
										Factum.planCuenta=null;
										Cookies.removeCookieNative("user", "/"+Factum.banderaNombreProyecto+"/");
										 Cookies.removeCookieNative("password", "/"+Factum.banderaNombreProyecto+"/");
										 Cookies.removeCookieNative("idUsuario", "/"+Factum.banderaNombreProyecto+"/");
										 Cookies.removeCookieNative("menu", "/"+Factum.banderaNombreProyecto+"/");
										 Cookies.removeCookieNative("username", "/"+Factum.banderaNombreProyecto+"/");
										 Cookies.removeCookieNative("planCuenta", "/"+Factum.banderaNombreProyecto+"/");
	//									Window.Location.replace("http://"+banderaIpServidor+":9999/login");
										Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
									}
								}
	
							});
						} catch (RequestException e) {
							// TODO Auto-generated catch block
	//						//com.google.gwt.user.client.Window.alert("Error ");
							Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
							SC.say("Error "+e);
							e.printStackTrace();
						}
				}else{
					com.google.gwt.user.client.Window.alert("El servicio no esta registrado, se encuentra fuera de servicio o fuera de fecha de suscripcion");
					Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/");
				}
				
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				//com.google.gwt.user.client.Window.alert("Error failure");
				Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
				SC.warn("Error: "+caught.getMessage(),null);
			}
		};
		getService().getSessionId(sessionCallback);
	}
	
	
	private void updateStatus(){
		final AsyncCallback<String> sessionCallback=new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				String[] sessions = result.split(";");
				String url="http://"+Factum.banderaIpServidor+":9999/login/get_username?cookie=";
				for (int i=0; i<sessions.length;i++){
					String[] cookie=sessions[i].split("=");
//					Cookies.setCookie(cookie[0], cookie[1]);
					url+=cookie[0]+":"+ cookie[1];
					if (i<sessions.length-1) url+="-";
				}
				RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,url);

				try {

					//			//com.google.gwt.user.client.Window.alert("SessionId "+result);
//					rb.setHeader("Access-Control-Allow-Origin", "*");
//				    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
//				    rb.setHeader("Access-Control-Max-Age", "3600");
//				    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
					rb.sendRequest(null, new RequestCallback() {
						public void onError(
								final com.google.gwt.http.client.Request request,
								final Throwable exception) {
							SC.say("Error no hay usuario "
									+ exception+ "request "+request.toString());
						}

						public void onResponseReceived(
								final com.google.gwt.http.client.Request request,
								final Response response) {

							//						//com.google.gwt.user.client.Window.alert("Response: "+response.getText());

							loginResponse= response.getText();
							JSONObject respuesta=JSONParser.parseLenient(loginResponse).isObject();
							boolean enabled=respuesta.get("enabled").isBoolean().booleanValue();
							if (!enabled) {
								Factum.loggeado = false;
								Factum.Usuario = null;
								Factum.Password = null;
								Factum.Menu = null;
								Factum.MontoFact = null;
								Factum.planCuenta=null;
								 
								 Cookies.removeCookieNative("user", "/"+Factum.banderaNombreProyecto+"/");
								 Cookies.removeCookieNative("password", "/"+Factum.banderaNombreProyecto+"/");
								 Cookies.removeCookieNative("idUsuario", "/"+Factum.banderaNombreProyecto+"/");
								 Cookies.removeCookieNative("menu", "/"+Factum.banderaNombreProyecto+"/");
								 Cookies.removeCookieNative("username", "/"+Factum.banderaNombreProyecto+"/");
								 Cookies.removeCookieNative("planCuenta", "/"+Factum.banderaNombreProyecto+"/");
								 
								Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
							}
						}

					});
				} catch (final Exception e) {
					Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
					SC.say("Error "+e);
				}
				
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.Location.replace("http://"+banderaIpServidor+":"+banderaPuertoZuul+"/login");
				SC.warn("Error: "+caught.getMessage(),null);
			}
		};
		getService().getSessionId(sessionCallback);
	}
	
		private void obtener_sesion(){
					
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro
        		
                SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

                    public void execute(Boolean value) {
                        if (value) {
//                            Window.Location.reload();
                            com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
                            //getService().LeerXML(asyncCallbackXML);
                        }
                    }
                });
            }
            intentos = 0; 
        }

        public void onFailure(Throwable caught) {
        	intentos += 1;
            if (intentos <= 5) {
                obtener_sesion();
            } else {
                SC.say("Error al comprobar el usuario, por favor actulice la pagina y revise su conexion de Red");
            }
        }
    };
    
    getService().getUserFromSession(callbackUser);
    
	} 
}
