package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.regGrillas.DetallesRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;

public class frmUtilidadCosto extends VLayout{
	String Tipo="0";
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	//TextItem txtNumFactura =new TextItem("txtNumFactura", "Num Factura");
	private final ListGrid listGrid = new ListGrid();
	NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public frmUtilidadCosto() {
		
//		formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
		txtFechaInicial.setValue(new Date());
		txtFechaFinal.setValue(new Date());
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		
		DynamicForm dynamicForm = new DynamicForm();
		cmbDocumento.setRequired(true);
		cmbDocumento.setValueMap("Facturas de Venta","Notas de Entrega");
		cmbDocumento.setDefaultToFirstOption(true);
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, cmbDocumento });
		ListGridField lstNumFactura =new ListGridField("lstNumFactura", "Num Factura",70);
		ListGridField lstIdDetalle =new ListGridField("lstIdDetalle", "IdDetalle",70);
		ListGridField lstDescripcion =new ListGridField("lstDescripcion", "Descripcion",600);
		ListGridField lstCantidad =new ListGridField("lstCantidad", "Cantidad",70);
		ListGridField lstPrecio =new ListGridField("lstPrecio", "Precio Unitario",70);
		lstPrecio.setAlign(Alignment.RIGHT);
		ListGridField lstTotal =new ListGridField("lstTotal", "Total",70);
		ListGridField lstCosto =new ListGridField("lstCosto", "Costo",70);
		ListGridField lstUtilidad =new ListGridField("lstUtilidad", "Utilidad",70);
		ListGridField lstUtilidadP =new ListGridField("lstUtilidadP", "Utilidad%",70);
		ListGridField Ban = new ListGridField("Ban", "Activo",60);
		
		lstTotal.setAlign(Alignment.RIGHT);
		
		
		lstPrecio.setType(ListGridFieldType.FLOAT);
		lstPrecio.setShowGridSummary(true);
		lstPrecio.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstPrecio"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
//                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		
		lstCosto.setType(ListGridFieldType.FLOAT);
		lstCosto.setShowGridSummary(true);
		
		lstCosto.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstCosto"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
//                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		
		lstTotal.setType(ListGridFieldType.FLOAT);
		lstTotal.setShowGridSummary(true);
		lstTotal.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstTotal"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
//                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		
//		lstUtilidadP.setSummaryFunction(SummaryFunctionType.AVG);
//		lstUtilidad.setSummaryFunction(SummaryFunctionType.SUM);
		lstUtilidad.setType(ListGridFieldType.FLOAT);
		lstUtilidad.setShowGridSummary(true);
		lstUtilidad.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstUtilidad"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
//                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		lstUtilidadP.setType(ListGridFieldType.FLOAT);
		lstUtilidadP.setShowGridSummary(true);
		lstUtilidadP.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;double sumC=0;
                for (int i = 0; i < records.length; i++) {
                	sum +=  Double.valueOf(records[i].getAttribute("lstTotal"));
                	sumC +=  Double.valueOf(records[i].getAttribute("lstCosto")) * Double.valueOf(records[i].getAttribute("lstCantidad"));
//                	String porcV=records[i].getAttribute("lstUtilidadP");
//                    sum +=  Double.valueOf(porcV.substring(0,porcV.length()-1));
                }
                double utilidadPor=(100*(sum-sumC))/(sumC);
                if (utilidadPor < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(utilidadPor)+ "</span>";
//                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(utilidadPor) + "</span>";
//                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		addMember(dynamicForm); 
		listGrid.setFields(new ListGridField[] {lstIdDetalle,lstNumFactura,lstCantidad,lstDescripcion,lstPrecio,lstTotal,lstCosto,lstUtilidad,lstUtilidadP,Ban });
		listGrid.setShowRowNumbers(true);
		listGrid.setShowGridSummary(true); 
		listGrid.setHeight100();
		lstCosto.setCanEdit(true);
		lstCosto.setAlign(Alignment.RIGHT);
//		lstCosto.setShowGridSummary(false);
		Ban.setType(ListGridFieldType.BOOLEAN); 
		Ban.setCanEdit(true);
		addMember(listGrid);
		
		HStack hStack = new HStack();
		
		IButton btnGenerar = new IButton("Generar");
		btnGenerar.addClickHandler(new ManejadorBotones("generar"));
		hStack.addMember(btnGenerar);
		
		IButton btnGrabar = new IButton("Grabar");
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		hStack.addMember(btnGrabar);
		addMember(hStack);
	}
	private class ManejadorBotones implements  com.smartgwt.client.widgets.events.ClickHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		public ManejadorBotones(String s){
			indicador=s;
			
		}
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario")){
					Tipo="5";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
					Tipo="10";
				}else if(cmbDocumento.getDisplayValue().equals("Gastos")){
					Tipo="7";
				}else if(cmbDocumento.getDisplayValue().equals("Ingresos")){
					Tipo="12";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Venta Grandes")){
					Tipo="13";
				}else if(cmbDocumento.getDisplayValue().equals("Nota de Credito Proveedores")){
					Tipo="14";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Proveedores")){
					Tipo="15";
				}else if(cmbDocumento.getDisplayValue().equals("Proforma")){
					Tipo="20";
				}
				
				
				//SC.say(idCliente+" vendedor="+idVendedor+" Tipo "+Tipo);
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}

		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				datosfrm();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().listarFacturas(fechaI, fechaF,Tipo, listaCallback);
				
			}else if(indicador.equalsIgnoreCase("grabar")){
				try{
					
					
					LinkedList<DtoComDetalleDTO> listaDetalles =new LinkedList<DtoComDetalleDTO>();
					for(int i=0;i<listGrid.getRecordList().getLength();i++){
						if(listGrid.getRecord(i).getAttributeAsBoolean("Ban")){
//							SC.say("grabar.....");
							DtoComDetalleDTO det=new DtoComDetalleDTO();
							det.setIdDtoComercialDetalle(listGrid.getRecord(i).getAttributeAsInt("lstIdDetalle"));
							det.setCostoProducto(listGrid.getRecord(i).getAttributeAsDouble("lstCosto"));
							listaDetalles.add(det);
						}
					}
					
					if(!listaDetalles.isEmpty()){
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().CostoUtilidadDetalle(listaDetalles,objback);
					}else{
						SC.say("No existen datos a modificar");
					}
					
				}catch(Exception e){
					SC.say(e.getMessage());
				}
				
			}
		}
		final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

			public void onFailure(Throwable caught) {
				SC.say(caught.toString()+ "error");
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
			}
			public void onSuccess(List<DtocomercialDTO> result) {
				LinkedList<ListGridRecord> listado = new LinkedList<ListGridRecord>();		
				for(int i=0;i<result.size();i++){
					if(String.valueOf(result.get(i).getEstado()).equals("1")){
						Iterator resultado = result.get(i).getTbldtocomercialdetalles().iterator();
				    	   while ( resultado.hasNext() ) {
				    		   DtoComDetalleDTO det=(DtoComDetalleDTO) resultado.next();
				    		   DetallesRecords doc =new DetallesRecords(det,result.get(i).getNumRealTransaccion(), result.get(i).getTipoTransaccion());
				    		   listado.add(doc);
				    	   }
					}
				}
				ListGridRecord[] listadof = new ListGridRecord[listado.size()];
				for(int i=0;i<listado.size();i++){
					listadof[i] = new ListGridRecord();
					listadof[i]=listado.get(i);
				}
				listGrid.setData(listadof);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		        //btnGenerarReporte.setDisabled(true);
		    }
		};
		final AsyncCallback<String>  objback=new AsyncCallback<String>(){
			public void onFailure(Throwable caught) {
				SC.say("Error dado:" + caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(String result) {
				SC.say(result);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
			}
		};
		
		
	}
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
