package com.giga.factum.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

//import org.apache.commons.fileupload.FileItem;

//import javax.xml.bind.JAXBElement;
import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.BodegaProdDTO;
import com.giga.factum.client.DTO.CargoDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CoreDTO;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.DocumentoSaveDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtoelectronicoDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.EquipoOrdenDTO;
import com.giga.factum.client.DTO.EstablecimientoDTO;
import com.giga.factum.client.DTO.FacturaProduccionDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.MayorizacionDTO;
import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.PosicionDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoElaboradoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.ReporteVentasEmpleadoDTO;
import com.giga.factum.client.DTO.ReportesDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.SerieDTO;
import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.DTO.TblpermisorolpestanaDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.giga.factum.client.DTO.TblrolDTO;
import com.giga.factum.client.DTO.TbltipodocumentoDTO;
import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.TmpAuxgeneralDTO;
import com.giga.factum.client.DTO.TmpAuxresultadosDTO;
import com.giga.factum.client.DTO.TmpComprobacionDTO;
import com.giga.factum.client.DTO.TraspasoBodegaProductoDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.DTO.detalleVentasATSDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
//	Integer ingresar(Factura factura);
	public Date fechaServidor();
	//METODOS PARA SESIONES
	public User verificar_sesion();
	public User checkLogin(String userName, String password, String planCuentaID);
	public User checkLogin(String userName, String planCuentaID);
	public User getUserFromSession();
	public String getSessionId();
	public String cerrar_sesion(String NombreBD, String PasswordBD, String UsuarioBD, String SO,String PasswordComprimido, String destinoBackup);
	public List<String> LeerXML() throws IllegalArgumentException, IOException;
	
//	String actualizarIva();
	
	//METODOS PARA PERMISOS
	TblpermisorolpestanaDTO listarpermisosLecturap(int nivel, String pestana);
	List<TblrolDTO> listarRol(int ini,int fin);
	List<TblrolDTO> listarRolCombo(int ini, int fin);	
		
	//METODOS PARA PERSONA
	
	String greetServer(String name) throws IllegalArgumentException;
	String grabarCliente(PersonaDTO cliDTO) throws IllegalArgumentException;
	LinkedHashMap<Integer, ProductoDTO> listaProductos(Integer inicio, Integer NumReg, Integer Tipo, Integer Jerarquia, Integer TipoPrecio, int StockNegativo);
	LinkedHashMap<Integer, ProductoDTO> listaProductos();
	List<PersonaDTO> findJoin(String variable,String tabla, String campo);
	List<TipoPagoDTO> listaTipoPago();
	DtocomercialDTO ultimoDocumento(String filtro, String campoOrden); 
	List<PersonaDTO> ListJoinPersona(String tabla, int ini, int fin);
	List<PersonaDTO> ListEmpleados(int ini, int fin);
	//String grabarProductoBodega(ProductoBodegaDTO[] proBod, int length,ProductoTipoPrecioDTO[] proTip, int l)throws IllegalArgumentException;
	String grabarProductoBodega(ProductoBodegaDTO[] proBod, int length)throws IllegalArgumentException;
	String grabarProductoTipoPrecio(ProductoTipoPrecioDTO[] proTip, int length)throws IllegalArgumentException;
	int numeroRegistrosPersona(String tabla);
	String grabarEmpleado (PersonaDTO cliDTO) throws IllegalArgumentException;
	
	int numeroRegistros(String tabla);
	// METODOS PARA CLIENTE
	String grabarPersona(PersonaDTO persona) throws IllegalArgumentException;
	String modificarPersona(PersonaDTO persona);
	String modificarPersonaRUC(PersonaDTO persona);//cedula invalida
	String eliminarPersona(PersonaDTO persona);
	PersonaDTO buscarPersona (PersonaDTO cedula, int tipo);
	
	String GrabarCli(PersonaDTO personadto);
	String GrabarProv(PersonaDTO personadto);
	String GrabarEmp(PersonaDTO personadto);
	
	// METODOS FIN PARA CLIENTE
	//METODOS PARA MARCA
	String grabarMarca(MarcaDTO marca) throws IllegalArgumentException;
	String modificarMarca(MarcaDTO marca);
	String eliminarMarca(MarcaDTO marca);
	MarcaDTO buscarMarca (String idMarca);
	List<MarcaDTO> listarMarca(int ini,int fin);
	List<MarcaDTO> listarMarcaCombo(int ini, int fin);
	List<MarcaDTO> listarMarcaLike(String busqueda,String campo);
	int numeroRegistrosMarca(String tabla);
	List<TbltipodocumentoDTO> listarTiposDocumento(Integer ini, Integer fin);
	
	//METODOS PARA Unidad
	String grabarUnidad(UnidadDTO marca) throws IllegalArgumentException;
	String modificarUnidad(UnidadDTO marca);
	String eliminarUnidad(UnidadDTO marca);
	UnidadDTO buscarUnidad (String idMarca);
	List<UnidadDTO> listarUnidad(int ini,int fin);
	List<UnidadDTO> listarUnidadCombo(int ini, int fin);
	List<UnidadDTO> listarUnidadLike(String busqueda,String campo);
	int numeroRegistrosUnidad(String tabla);
	
	//METODOS PARA Categoria
	String grabarCategoria(CategoriaDTO categoria) throws IllegalArgumentException;
	String modificarCategoria(CategoriaDTO categoria);
	String eliminarCategoria(CategoriaDTO categoria);
	CategoriaDTO buscarCategoria (String idCategoria);
	List<CategoriaDTO> listarCategoria(int ini,int fin);
	List<CategoriaDTO> listarCategoriaLike(String busqueda,String campo);
	List<CategoriaDTO> listarCategoriaCombo(int ini, int fin);
	int numeroRegistrosCategoria(String tabla);
	
	//METODOS PARA BODEGA
	String grabarBodega(BodegaDTO bodega) throws IllegalArgumentException;
	String modificarBodega(BodegaDTO bodega);
	String eliminarBodega(BodegaDTO bodega);
	BodegaDTO buscarBodega (String idBodega);
	List<BodegaDTO> listarBodega(int ini,int fin);
	List<BodegaDTO> listarBodegaProducto(int ini,int fin, int idproducto);
	List<BodegaDTO> listarBodegaLike(String busqueda,String campo);
	int numeroRegistrosBodega(String tabla);
	
	//METODOS PARA TIPO PRECIO
	String grabarTipoprecio(TipoPrecioDTO tipoprecio) throws IllegalArgumentException;
	String modificarTipoprecio(TipoPrecioDTO tipoprecio);
	String eliminarTipoprecio(TipoPrecioDTO tipoprecio);
	TipoPrecioDTO buscarTipoprecio (String idTipoPrecio);
	List<TipoPrecioDTO> listarTipoprecio(int ini,int fin);
	List<TipoPrecioDTO> listarTipoprecioLike(String busqueda,String campo);
	int numeroRegistrosTipoprecio(String tabla);
	
	
	//METODOS PARA PRODUCTO
	String grabarProducto(ProductoDTO producto) throws IllegalArgumentException;
	String modificarProducto(ProductoDTO producto);
	String modificarProductoEliminado(ProductoDTO pro);
	String eliminarProducto(ProductoDTO producto);
	String impresion(String canvas);
	ProductoDTO buscarProducto (String nombre,String campo);
	ProductoDTO buscarProductoCodBar (String nombre,String campo,Integer Tipo,int StockNegativo);
	ProductoDTO buscarProductoEliminado(String nombre, String campo);
	LinkedHashMap<Integer, ProductoDTO> listaProductosConFiltro(int ini, int fin, int op);
	List<ProductoDTO> listarProducto(int ini,int fin);
	LinkedHashMap<Integer, ProductoDTO> listarProductoLike(String busqueda,String campo);
	LinkedHashMap<Integer, ProductoDTO> listarProductoLike2(String busqueda,String campo,int stock , int Tipo, int Jerarquia, int TipoPrecio, int StockNegativo);
	LinkedHashMap<Integer, ProductoDTO> listarProductoMateriaPrimaLike(String busqueda,String campo,int stock);
	LinkedHashMap<Integer, ProductoDTO> listarProductoJoin(String nombre,String tabla, String campo, Integer Tipo, Integer Jerarquia, Integer TipoPrecio,int StockNegativo);
	//String impresion(Canvas canvas);
	LinkedHashMap<Integer, ProductoDTO> listarProductoJoin2(String nombre,String tabla, String campo,int stock, int Tipo, int Jerarquia, int TipoPrecio, int StockNegativo);
	LinkedHashMap<Integer, ProductoDTO> listarProductoLikeEliminados(String busqueda,	String campo, int stock);
	int numeroRegistrosProducto(String tabla);
	List<BodegaProdDTO> ProdBodega(String idProducto,String tabla);
	String  TransferirBodega(LinkedList<TraspasoBodegaProductoDTO> listTransferir);
	float  ValorDelInventario();
	String grabarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproh);
	

	//METODOS PARA PROCUTOS ELBORADOS
	List<ProductoElaboradoDTO> listarHijosProductoElaborado(String idproductoelaborado);	
	String modificarProductoElaborado(ProductoDTO pro, List<ProductoDTO> listproductodto);

	
	//METODOS PARA EMPRESA
	String grabarEmpresa(EmpresaDTO empresa) throws IllegalArgumentException;
	String modificarEmpresa(EmpresaDTO empresa);
	String eliminarEmpresa(EmpresaDTO empresa);
	EmpresaDTO buscarEmpresa (String idempresa);
	List<EmpresaDTO> listarEmpresa(int ini,int fin);
	EmpresaDTO buscarEmpresaNombre (String idempresa,String campo);
	List<EmpresaDTO> listarEmpresaLike(String busqueda,String campo);
	
	//METODOS PARA TIPO DE CUENTA
	String grabarTipoCuenta(TipoCuentaDTO tipocuenta) throws IllegalArgumentException;
	String modificarTipoCuenta(TipoCuentaDTO tipocuenta);
	String eliminarTipoCuenta(TipoCuentaDTO tipocuenta);
	TipoCuentaDTO buscarTipoCuenta (String idtipocuenta);
	List<TipoCuentaDTO> listarTipoCuenta(int ini,int fin);
	TipoCuentaDTO buscarTipoCuentaNombre (String idTipoCuenta,String campo);
	List<TipoCuentaDTO> listarTipoCuentaLike(String busqueda,String campo);
	int numeroRegistrosTipocuenta(String tabla);
	
	//METODOS PARA CUENTA
	String grabarCuenta(CuentaDTO cuenta, String planCuentaID) throws IllegalArgumentException;
	String modificarCuenta(CuentaDTO cuenta);
	CuentaDTO buscarCuenta (String idcuenta);
	List<CuentaDTO> listarCuenta(int ini,int fin, String planCuentaID);
	int numeroRegistroscuenta(String tabla,String planCuentaID);
	String eliminarCuenta(CuentaDTO cuenta);
	List<CuentaDTO> listarCuentaLike(String busqueda,String campo,String planCuentaID);
	List<CuentaDTO> listarCuentaQuery(String query);
	List<CuentaDTO> listarCuentaPlanCuentas(String planCuentaID);
	CuentaDTO buscarCuentaNombre (String idCuenta,String campo);
	
	//METODOS PARA PLANCUENTA
	String grabarPlanCuenta(PlanCuentaDTO PlanCuenta) throws IllegalArgumentException;
	PlanCuentaDTO buscarPlanCuenta (String idPlanCuenta);
	List<PlanCuentaDTO> listarPlanCuenta(int ini,int fin);
	int numeroRegistrosPlanCuenta(String tabla);
	String eliminarPlanCuenta(String id);
	List<PlanCuentaDTO> listarPlanCuentaLike(String busqueda,String campo);
	PlanCuentaDTO buscarPlanCuentaNombre (String idPlanCuenta,String campo);
	PlanCuentaDTO ultimoPlanCuenta();
	//METODOS PARA CARGO
	CargoDTO buscarCargo(String idCargo);
	String eliminarCargo(CargoDTO cargo);
	String grabarCargo(CargoDTO cargo);
	List<CargoDTO> listarCargo(int ini, int fin);
	List<CargoDTO> listarCargoLike(String busqueda, String campo);
	String modificarCargo(CargoDTO cargo);
	int numeroRegistrosCargo(String tabla);
	
	//METODOS PARA IMPUESTO
	ImpuestoDTO buscarImpuesto(String idImpuesto);
	String eliminarImpuesto(ImpuestoDTO impuesto);
	String grabarImpuesto(ImpuestoDTO impuesto);
	List<ImpuestoDTO> listarImpuesto(int ini, int fin);
	List<ImpuestoDTO> listarImpuestoLike(String busqueda, String campo);
	String modificarImpuesto(ImpuestoDTO impuesto);
	int numeroRegistrosImpuesto(String tabla);
	LinkedList<String[]> listarCuentaPlanCuenta(String idEmpresa);
	
	
	//METODOS PARA RETENCION
	String grabarDetalle(LinkedList<RetencionDTO> det) throws IllegalArgumentException;
	
	
	//METODOS PARA TIPO PAGO
	TipoPagoDTO buscarTipopago(String Tipopago);
	String eliminarTipopago(TipoPagoDTO tipopago);
	String grabarTipopago(TipoPagoDTO tipopago);
	List<TipoPagoDTO> listarTipopago(int ini, int fin);
	List<TipoPagoDTO> listarTipopagoLike(String busqueda, String campo);
	String modificarTipopago(TipoPagoDTO Tipopago);
	int numeroRegistrosTipopago(String tabla);
	
	//METODOS PPARA REPORTES CONTABILIDAD
	List<MayorizacionDTO> listarMayorizacion(String FechaI, String FechaF,String nrt,String tipo);
	List<TbltipodocumentoDTO> listarTipoDocumento();
	//METODOS PARA REPORTES FACTURAS
	
	//METODOS PARA REPORTES FACTURAS
	List<DtocomercialDTO> listarFacturas(String FechaI, String FechaF,String Tipo);
	List<DtocomercialDTO> listarFacturasCliente(String FechaI, String FechaF,
			String idCliente,String Tipo);
	List<DtocomercialDTO> listarFacturasVendedor(String FechaI, String FechaF,
			String idVendedor,String Tipo);
	List<DtocomercialDTO> listarFacturasVendedorCliente(String FechaI, String FechaF,
			String idCliente,String idVendedor,String Tipo);
	List<DtocomercialDTO> listarFacturasProducto(String FechaI, String FechaF,
			String idProducto,String Tipo);
	String grabarDocumento(DtocomercialDTO dto, Integer indicador, Double iva, Integer anular, String responsable,
			String planCuentaID, int ConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales);
	List<DtocomercialDTO> listarDocTipo(Integer tipo);
	List<DtocomercialDTO> listarDocTipoPersona(Integer tipo, Integer persona);
	List<DtocomercialDTO> listarDocFecha(String fecha);
	DtocomercialDTO getDtoID(Integer ID);
	List<DtocomercialDTO> listarFacturasClienteProducto(String FechaI, String FechaF,
			String idCliente, String idProducto, String Tipo);
	List<DtocomercialDTO> listarFacturasVendedorProducto(String FechaI, String FechaF,
			String idVendedor, String idProducto, String Tipo);
	List<DtocomercialDTO> listarFacturasVendedorClienteProducto(String FechaI, String FechaF,String idCliente,
			String idVendedor, String idProducto, String Tipo);
	List<DtocomercialDTO> listarFacturasNumDto(String FechaI, String FechaF,String Tipo, String NumDto);
	
	//METODOS PARA AREA
	String grabarArea(AreaDTO marca) throws IllegalArgumentException;
	String modificarArea(AreaDTO marca);
	String eliminarArea(AreaDTO marca);
	AreaDTO buscarArea (String idMarca);
	List<AreaDTO> listarArea(int ini,int fin);
	List<AreaDTO> listarMesasOcupadas();
	List<AreaDTO> listarAreaCombo(int ini, int fin);
	List<AreaDTO> listarAreaLike(String busqueda,String campo);
	int numeroRegistrosArea(String tabla);
	ArrayList<PedidoProductoelaboradoDTO> obtenerPedido(Integer idMesa);
	
	//METODOS PARA  MESA
	String grabarMesa(MesaDTO marca) throws IllegalArgumentException;
	String modificarMesa(MesaDTO marca);
	String eliminarMesa(MesaDTO marca);
	MesaDTO buscarMesa (String idMarca);
	List<MesaDTO> listarMesa(int ini,int fin);
	List<MesaDTO> listarMesaCombo(int ini, int fin);
	List<MesaDTO> listarMesaLike(String busqueda,String campo);
	int numeroRegistrosMesa(String tabla);
	
	//METODOS PARA  SERVICIO
	String grabarCore(CoreDTO Coredto) throws IllegalArgumentException;
	String modificarCore(CoreDTO Coredto);
	CoreDTO buscarCore (String idCore);
	List<CoreDTO> listarCore(int ini,int fin);
	int numeroRegistrosCore(String tabla);
	String eliminarCore(CoreDTO Coredto);
	List<CoreDTO> listarCoreLike(String busqueda,String campo);
	CoreDTO buscarCoreNombre (String idCore,String campo);
	
	//METODOS PARA PAGOS
	TblpagoDTO BuscarPago(String idPago);
	List<TblpagoDTO> listarPagos(String FechaI, String FechaF,String estado,String Tipo,String idfactura,String idCliente,int tipoConsulta);
	List<TblpagoDTO> listarPagosFacRealCompra(String FechaI, String FechaF,String estado,String Tipo,String idfactura,String idCliente, String numRealFactura);
	String grabarPago(TblpagoDTO pagoDTO) throws IllegalArgumentException;
	String modificarPago(TblpagoDTO pagoDTO, User usuario) throws IllegalArgumentException;
	String modificarcrearPago(TblpagoDTO pagoDTO,double pagado, User usuario) throws IllegalArgumentException;
	List<TblpagoDTO> listarPagosVencidos();
	String grabarPagos(LinkedList<TblpagoDTO> pagoDTO, User usuario) throws IllegalArgumentException;
	String ReprotePagosVencidosCliente(String idPersona);
	
	
	//POSICIONES
	String grabarPosiciones(LinkedList<PosicionDTO> pos);
	List<PosicionDTO> listarPosiciones(String NombreDocuemto);
	LinkedHashMap<Integer, ProductoDTO> BuscarProductoJoin(String bod, String des);
	
	//SERIES
	String grabarSeries(LinkedList<SerieDTO> pos);
	List<SerieDTO> listarSeries(int idproducto);
	SerieDTO buscarSerie(String serie);
	
	//REPORTES
//	double ReproteTotalDocumentos(String fechaI, String fechaf,String tipo);
//	double ReproteIVADocumentos(String fechaI, String fechaf,String tipo);
//	double ReproteSubtIVA0Documentos(String fechaI, String fechaf,String tipo);
//	double ReproteSubTotalNetoDocumentos(String fechaI, String fechaf,String tipo);
	LinkedList<ReporteVentasEmpleadoDTO> ReporteVentasEmpleado(String fechaI, String fechaF, double IVA);
//	ReportesDTO Libro(String fechaI, String fechaf,String tipo);
//	String listGridToExel(LinkedList<String[]> listado);
	
	//METODO para obtner el porcentaje del tipo de precio
	
	String actualizarAso(List<TbltipodocumentoDTO> tipoDocumentos);
	String actualizarAsoPagos(List<TipoPagoDTO> tipoPagos);
	String actualizarAsoImpuestos(List<ImpuestoDTO> Impuestos);
	
	
	LinkedList<MovimientoCuentaPlanCuentaDTO> listarLibro(String fechai, String fechaf, String planCuentaID);	
	
	String grabarRetencion(LinkedList<RetencionDTO> detalle,DtocomercialDTO doc);
	String pagoConRetencion(LinkedList<RetencionDTO> detalle,DtocomercialDTO doc,TblpagoDTO pagodto);
	List<DtocomercialDTO> kardex(String FechaI, String FechaF,String idProducto);
	
	
	String eliminarDocSinConfirmar(DtocomercialDTO documentoDTO); 
	
	//+++++++++++++++++++++   LISTADO PRODUCTOS POR PROVEEDOR   +++++++++++++++++++
	//List<ProductoProveedor> listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax);
	List<ProductoProveedor> listarProductosProveedor(String FechaI, String FechaF,int StockMin, int StockMax,String idProveedor, String idProducto);
	List<ProductoCliente> listarProductosCliente(String FechaI, String FechaF,int StockMin, int StockMax,String idCliente, String idProducto);
	List<ProductoTipoPrecio> listarTipoPrecio(int codProducto);
	String grabarProductoTipoPrecio(LinkedList <ProductoTipoPrecioDTO> proTip);
	//++++++++++++++++++++++    REPORTE DE RETENCIONES   +++++++++++++++++++++++++++++++++++++++++++++++++++
	List<DtocomercialDTO> listarRetenciones(String FechaI, String FechaF, String TipoTransaccion,String tipoImpuesto,String Cedula,String codigoImpuesto);	
	List<ReporteRetencion> listarRetenciones(String FechaI, String FechaF,int tipoTransaccion, String ruc, String nombres, String razonSocial);
	//++++++++++++++++ PARA SACAR EL TOTAL DE CADA TIPO DE RETENCION EN EL FrmReporteRetencion +++++++
	List<RetencionDetalle> listarRetTotales(int tipoTransaccion,LinkedList<Integer> idImpuesto,String ruc, String nombres, String razonSocial,String fechaI, String fechaF);
	List<RetencionDTO> listarDetallesRetencion(int numDto);
	/*List<DtocomercialDTO> Kardex(String FechaI, String FechaF,String idProducto);*/
	List<DtocomercialDTO> listarReportesCliente(String FechaI, String FechaF,
			String Tipo, String ruc, String nombre, String razonSocial);
	//+++++++++   PARA EL LISTADO DE TOOOOODOS LOS CLIENTES (frmClientes2)   ++++++++++++++++
	List<PersonaDTO> ListJoinPersona2(String tabla);
	List<PersonaDTO> findJoin2(String variable, String tabla, String campo);
	List<PersonaDTO> findJoinCedulas(String variable, String campo);
	PersonaDTO buscarPersona2(PersonaDTO cedula, int tipo);
	String listGridToExel(LinkedList<String[]> listado, String nombre);
	List<TblpagoDTO> listarPagos2(String FechaI, String FechaF, String estado,
			String Tipo, String numRealTransaccion, String idCliente,
			int tipoConsulta);
	String grabarEgreso(LinkedList<TblpagoDTO> list, User usuario);
	public List<TblpagoDTO> listarPagosEgreso(String Tipo,String num);
	int ultimoEgreso();
	int ultimoIngreso();
	int ultimoProducto();
	 String asignarPuntos(ClienteDTO cliente);
	 
	 String grabarAnticipo(LinkedList<DtocomercialDTO> listAnticipos);
	 String CostoUtilidadDetalle(LinkedList<DtoComDetalleDTO> listDetDTO);
	 List<TblpagoDTO> listarPagosFechaPago(String FechaI, String FechaF,
				String estado, String Tipo,String numRealTransaccion,String idCliente,int tipoConsulta);
	  TblpagoDTO buscarPagoId(int idPago);
	BodegaDTO buscarBodegaSegunNombre(String nombre);
	
	//LinkedList<detalleVentasATSDTO> ATSVentas(String fechai,String fechaf, double IVA);
	
	LinkedList importarExcel(String ruta);
	String exportarexcel(LinkedList<String[]> ld, String nombre);
	
	  
	
	// METODOS PARA CARGAR FACTURA DESDE MODULO TALLER
	
	List<EquipoOrdenDTO> listarEquipoOrdenFiltroID(Integer id);
	
	ClienteDTO buscarCliente(int id);
	
	//METODO SETTEAR VARIABLES DE FACTUM
	//void cambiaValor(int banderaSO);	
	
	//METODO PARA EJECUTAR BACKUP DE LA BASE DE DATOS
	//String backupDB(boolean validadorAlmacenamiento);
	String backupDB(boolean validadorAlmacenamiento, String NombreBD, String PasswordBD, String UsuarioBD,String SO, String PasswordComprimido, String DestinoBackup);
	
	
	//PROCEDIMIENTOS ALMACENADOS 
	String ejecutarProcedimiento(String fileName, HashMap <String,Object> param, boolean bandera, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD) throws Exception;
	List<TmpComprobacionDTO> ejecutarComprobacion(String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD) throws Exception;
	List<TmpAuxgeneralDTO> ejecutarBalance(String fileName, HashMap <String,Object> param, String planCuenta, String NombreBD, String UsuarioBD, String PasswordBD ) throws Exception;
	List<TmpAuxresultadosDTO> ejecutarEstado(String fileName, HashMap <String,Object> param, String planCuenta,String NombreBD, String UsuarioBD, String PasswordBD) throws Exception;
	
	//METODO PARA OBTENER EL IPDE SERVIDOR
	String obtenerIPServidor();
	//OBTEENR LA LISTA DE LAS IMAGENES DNTRO DE UN DIRECOTROIO
	String listGridToPDF(LinkedList<String[]> listado, String[] atributos,String NombreEmpresa,String TelefonoCelularEmpresa,String TelefonoConvencionalEmpresa, String DireccionEmpresa, String Logo);
	//OBTEENR LA LISTA DE LAS IMAGENES DNTRO DE UN DIRECOTROIO
	List<String> listarImagenes();
	//OBTEENR EL NOMBRE DE HOST
	String obtenernombreHost();	
	//METODO PARA LISTAR LOS PEDIDOS
	List<PedidoProductoelaboradoDTO> listarPedidos(String busqueda, String fechainicial, String fechafinal, int banderafiltro);
	//METODO PARA MODIFICAR LOS PEDIDOS
	String modificarPedido(ArrayList<PedidoProductoelaboradoDTO> listapedidoproductoelaboradodto);
	//METODO PARA DOCUMENTOS ELECTRONICOS	
	String documentoElectronico(DtocomercialDTO documento,String NombrePlugFacturacionElectronica,String RutaCertificado, 
//			String AmbienteFacturacionElectronica, 
			String SO);
	//METODO PARA LISTAR FACTURAS PRODUCCION
	ArrayList<FacturaProduccionDTO> listarFacturasProduccion(String fecha);
	//METODO PARA CUENTAS POR COBRAR
	List<DtocomercialDTO> obtenerCuentasPorCobrar();
	//METODO PARA BUSCAR CUENTAS POR COBRAR
	List<DtocomercialDTO> buscarCuentasCobrar(String nombre, String campo);
	//METODO PARA GRABA RMOVIMIENTOS-ASIENTOS CONTABLES
	String grabarDtomovimiento(DtocomercialDTO dtocomercialdto, List<MovimientoCuentaPlanCuentaDTO> MovimientoCuentaPlanCuenta);
	String modificarCuenta(CuentaDTO cuenta, String planCuentaID);
	List<ProductoTipoPrecioDTO> listarTipoPrecioD(int parseInt);
	EstablecimientoDTO buscarEstablecimiento(Integer idEmpresa);
	PuntoEmisionDTO buscarPuntoemision(Integer idEmpresa, String establecimiento);
	int ingresar();
	RetencionDTO ultimaRetencion(String string,int tipo, String string2);
	List<ImpuestoDTO> listarImpuesto(int tipo);
	List<TblmultiImpuestoDTO> listarMultiImpuesto(int ini, int fin);
	String grabarProductoMultiImpuesto(TblproductoMultiImpuestoDTO[] proTip) throws IllegalArgumentException;
	List<TblproductoMultiImpuestoDTO> listarProdMultiImp(int idProducto);
	String[] grabarDocumentosDiv(List<DocumentoSaveDTO> documentos, String userName, String planCuenta,
			int banderaConfiguracionTipoFacturacion, double banderaIVA, int banderaNumeroDecimales);
	PlanCuentaDTO ultimoPlanCuenta(String idEmpresa);
	//String buscarXML(DynamicForm dynXML);
	//String buscarXML(FileItem arch);
	String eliminarFile(String url);
	TblpagoDTO BuscarPagoIdComercial(int idDtoComercial);

}
