package com.giga.factum.client;

import java.io.Serializable;

public class User implements Serializable{

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	  private String userName;
	  private String SessionID;
	  private String aviso;
	  private Integer userID;
	  private Integer nivelAcceso;
	  private String cedulaRUC;
	  private String planCuenta;
	  private int idEmpresa;
	  private String establecimiento;

	  private int orden=0;

	  public String getUserName() {
	    return userName;
	  }

	  public void setUserName(String userName) {
	    this.userName = userName;
	  }
	  public String getSessionID() {
	 	return SessionID;
	  }
	  public void setSessionID(String SessionID) {
		this.SessionID = SessionID;
	 }

	  //FUNCIONES PARA LOS MENSAJES
	  public String getAdvice(){
	      return aviso;
	  }

	  public void setAdvice(String aviso){
	      this.aviso = aviso;
	  }

	  //FUNCIONES PARA OBTENER LA ORDEN
	  public int getOrden(){
	      return orden;
	  }

	  public void setOrden(int orden){
	      this.orden = orden;
	  }

	  //METODOS SET Y GET PARA COLOCAR Y OBTENER EL TIPO DE USUARIO
	  public void setNivelAcceso(Integer nivelAcceso){
	      this.nivelAcceso = nivelAcceso;
	  }

	  public Integer getNivelAcceso(){
	      return nivelAcceso;
	  }
	  
	  public void setIdUsuario(Integer userID){
	      this.userID = userID;
	  }

	  public Integer getIdUsuario(){
	      return userID;
	  }
	
	  public void setCedulaRuc(String ci){
	      this.cedulaRUC = ci;
	  }

	  public String getCedulaRuc(){
	      return cedulaRUC;
	  }

	public String getPlanCuenta() {
		return planCuenta;
	}

	public void setPlanCuenta(String planCuenta) {
		this.planCuenta = planCuenta;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	  
	
}
