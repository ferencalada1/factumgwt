package com.giga.factum.client;

import java.util.List;



import com.giga.factum.client.regGrillas.ImagenRecord;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.Page;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.ItemChangeEvent;
import com.smartgwt.client.widgets.form.events.ItemChangeHandler;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.PortalLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tile.TileGrid;

public class frmCatalogo extends TabSet{
	TabSet tabset = new TabSet();
	Tab tabbuscarimagen = new Tab("Buscar Imagen");
	Tab tabsubirimagen = new Tab("Subir Imagen");
	
	DynamicForm dynamicForm;
	HLayout hlayoutizquierda;
	HLayout hlayoutderecha;
	VLayout vLayoutIzquierda = new VLayout();
	HLayout vLayoutIzquierdaH = new HLayout();
	VLayout vLayoutDerecha = new VLayout();
	HLayout hLayoutDerechaSuperior = new HLayout();
	HLayout hLayoutDerechaInferior = new HLayout();  
	
	ListGrid listGrid = new ListGrid();
	RecordList listado = new RecordList();
	//ListGridField lstidimagen = new ListGridField("id", "#");
    ListGridField lstnombreimagen = new ListGridField("nombre", "Nombre");
    TextItem txtBuscar = new TextItem();
    Img img = new Img();
    DynamicForm dynamicFormDerecha = new DynamicForm(); 
    DynamicForm dynamicFormIzquierda = new DynamicForm(); 
    PickerIcon buscarPickerLst = new PickerIcon(PickerIcon.SEARCH);
    String nombreHost;
    String nombreImagen="";
    TileGrid tileGrid;
    frmSubirArchivo frmsubirarchivo;
    
	public frmCatalogo(){			
		hlayoutizquierda= new HLayout();
		vLayoutIzquierda = new VLayout();
		vLayoutDerecha= new VLayout();
		
        //******************************************              
        listGrid = new ListGrid();
        listGrid.setFields(lstnombreimagen);        
        listGrid.setWidth("100%");  
        listGrid.setHeight("100%");               
        listGrid.addRecordClickHandler(new RecordClickHandler() {			
			@Override
			public void onRecordClick(RecordClickEvent event) {
				Record record = new Record();
				record=event.getRecord();
				cambiarImagen(record.getAttribute("nombre"));
				nombreImagen=record.getAttribute("nombre");
			}
		});
        
        listGrid.setAlternateRecordStyles(true);
        listGrid.setHeight100();
        listGrid.setShowResizeBar(false);
        txtBuscar.setTitle("Buscar");
        txtBuscar.addKeyPressHandler(new KeyPressHandler() {
			
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					buscarNombreImagen();
				}else if(event.getKeyName().equalsIgnoreCase("backspace")){
					if(txtBuscar.getDisplayValue().equals("") || txtBuscar.getDisplayValue().isEmpty()){
						listGrid.setData(listado);	
						listGrid.redraw();	
					}
				}
				
			}
		});
        dynamicFormIzquierda = new DynamicForm();
        dynamicFormIzquierda.setFields(new FormItem[] { txtBuscar});
        dynamicFormIzquierda.setAlign(Alignment.CENTER);	
        
        lstnombreimagen.setCellAlign(Alignment.LEFT); 
        

        img.setWidth(298);
        img.setAppImgDir("/");
        img.setHeight(298);

        
        vLayoutDerecha.addMember(img);        
        vLayoutDerecha.setAlign(Alignment.RIGHT);
        vLayoutDerecha.setAlign(VerticalAlignment.CENTER);
        
        vLayoutIzquierda.setWidth("45%");  
        vLayoutIzquierda.setHeight100();  
        vLayoutIzquierda.setLayoutMargin(10);  
        vLayoutIzquierda.setMembersMargin(10);           
        vLayoutIzquierda.setAlign(Alignment.LEFT);
        vLayoutIzquierda.addMember(dynamicFormIzquierda); 
        vLayoutIzquierda.addMember(listGrid);        
        
        
        hlayoutizquierda.addMember(vLayoutIzquierda);
        hlayoutizquierda.addMember(vLayoutDerecha);
        
        tabbuscarimagen.setPane(hlayoutizquierda);
        
        hlayoutderecha = new HLayout();
        frmsubirarchivo= new frmSubirArchivo("Imagen");
        /*
        frmsubirarchivo.uploadButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				frmsubirarchivo.dynamicformsubirarchivo.submitForm();
				
			}
		});
		*/
        hlayoutderecha.addMember(frmsubirarchivo);
        
        tabsubirimagen.setPane(hlayoutderecha);
        
        setSize("100%", "100%");
        addTab(tabbuscarimagen);
        addTab(tabsubirimagen);
        getService().listarImagenes(callbackImagen);
        getService().obtenernombreHost(callbackString);
	}
	//**************************************************
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	//**************************************************		
	//Funcion para listar las imagenes disponibles en el catalogo
	final AsyncCallback<List<String>> callbackImagen = new AsyncCallback<List<String>>() {

        public void onSuccess(List<String> result) {
        	List<String> listaimagenes;
        	listaimagenes= result;	         
        	listado= new RecordList();
			for(int i=0;i<listaimagenes.size();i++){
				ImagenRecord imagen= new ImagenRecord(listaimagenes.get(i));
				listado.add(imagen);				
			}			
			listGrid.setData(listado);	
			listGrid.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
        }

        public void onFailure(Throwable caught) {          
        	SC.say("No se puede obtener la lista las imagenes.");
        }
    };
  //**************************************************    
    public void buscarNombreImagen(){
    	RecordList listaimagenes = new RecordList();
    	RecordList listabusqueda= new RecordList();
    	listaimagenes=listado;
    	String nombreImagen=txtBuscar.getDisplayValue();
    	int contador=0;
    	for(int i=0; i<listaimagenes.getLength(); i++){
    		String[] nombreImagenConExtension=listaimagenes.get(i).getAttribute("nombre").split(".");
    		String nombreImagenSinExtension=nombreImagenConExtension[0];
			if(listaimagenes.get(i).getAttribute("nombre").equals(nombreImagen) || listaimagenes.get(i).getAttribute("nombre").equals(nombreImagenSinExtension)){
    		ImagenRecord imagen= new ImagenRecord(listaimagenes.get(i).getAttribute("nombre"));
    		listabusqueda.add(imagen);
			contador=contador+1;
			}
		}
    	listGrid.setData(listabusqueda);	
		listGrid.redraw();
    }    
  //**************************************************	
	final AsyncCallback<String> callbackString = new AsyncCallback<String>() {

        public void onSuccess(String result){
        	//SC.say(result);
        	nombreHost=result;
        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
        }
        public void onFailure(Throwable caught) {          
        	SC.say("No se puede obtener la lista las imagenes;");
        }
    }; 
  //**************************************************
    //Funcion para cambiar la imagen la dar click sobre un record de la lista de imagenes
    public void cambiarImagen(String nombreImagen){			
    	String nombre=nombreHost+"/images/catalogo/"+nombreImagen;
    	img.setSrc(nombre); 
    	
    }
  
}
