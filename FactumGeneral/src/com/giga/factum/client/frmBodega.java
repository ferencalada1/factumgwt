package com.giga.factum.client;
import java.util.List;

import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.regGrillas.BodegaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.widgets.form.Label;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;

public class frmBodega extends VLayout{
	DynamicForm dynamicForm;
	SearchForm searchForm = new SearchForm();
	ListGrid lstBodegas = new ListGrid();
	TabSet tabBodega = new TabSet();
	Label lblRegisros = new Label("# Registros");
	TextItem txtBodega = new TextItem("txtBodega", "Bodega");
	TextItem txtUbicacion = new TextItem("txtUbicacion", "Ubicaci\u00F3n");
	TextItem txtTelefono = new TextItem("txtTelefono", "Tel\u00E9fono");
	Boolean ban=false;
	int contador=20;
	int registros=0;
	IButton btnModificar;
	IButton btnEliminar;
	IButton btnGrabar;
	
	public frmBodega() {
		
		getService().numeroRegistros("Tblbodeba", objbackI);
		setSize("450", "300");
		  
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.setName("buscarPicker");
		tabBodega.setSize("100%", "100%");
		Tab tabIngreso = new Tab("Ingreso Bodega");
		VLayout layout = new VLayout();
		layout.setSize("100%", "100%");
		dynamicForm = new DynamicForm();
		dynamicForm.setSize("100%", "50%");
		dynamicForm.setMinColWidth(50);
		dynamicForm.setItemLayout(FormLayoutType.TABLE);
		
		dynamicForm.setWidth100();
		
		dynamicForm.setNumCols(2);
		txtBodega.setLength(15);
		txtBodega.setTabIndex(2);
		txtBodega.setLeft(133);
		txtBodega.setTop(34);
		txtBodega.setRequired(true);
		
		txtUbicacion.setLength(100);
		txtUbicacion.setRequired(true);
		
		txtTelefono.setRequired(true);
		txtTelefono.setLength(15);
		txtTelefono.setKeyPressFilter("[0-9]");
		
		TextItem txtidBodega = new TextItem("txtidBodega", "C\u00F3digo Bodega");
		txtidBodega.setDisabled(true);
		
		
		dynamicForm.setFields(new FormItem[] { txtidBodega, txtBodega, txtUbicacion, txtTelefono});
		layout.addMember(dynamicForm);
		
		Canvas canvas = new Canvas();
		canvas.setSize("100%", "50%");
		
		btnModificar = new IButton("Modificar");
		canvas.addChild(btnModificar);
		btnModificar.moveTo(218, 6);
		btnModificar.setDisabled(true);
		
		btnEliminar = new IButton("Eliminar");
		canvas.addChild(btnEliminar);
		btnEliminar.moveTo(324, 6);
		layout.addMember(canvas);
		btnEliminar.setDisabled(true);
		
		btnGrabar = new IButton("Grabar");
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(6, 6);
		btnGrabar.setSize("100px", "22px");
		
		IButton btnNuevo = new IButton("Nuevo");
		canvas.addChild(btnNuevo);
		btnNuevo.moveTo(112, 6);
		
		tabIngreso.setPane(layout);
		tabBodega.addTab(tabIngreso);
		
		Tab tabListado = new Tab("Listado de Bodegas");
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
		
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		TextItem txtBuscarLst = new TextItem("txtBuscar", "");
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre","Ubicaci\u00F3n","Tel\u00E9fono");
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstBodegas.setSize("100%", "80%");
		lstBodegas.setAutoFitData(Autofit.VERTICAL);  
		lstBodegas.setAutoFitMaxRecords(10);  
		lstBodegas.setAutoFetchData(true);  
		ListGridField idBodega = new ListGridField("idBodega", "C\u00F3digo");
		ListGridField Bodega = new ListGridField("Bodega", "Bodega:");
		ListGridField Ubicacion = new ListGridField("Ubicacion", "Ubicaci\u00F3n:");
		ListGridField Telefono = new ListGridField("Telefono", "Tel\u00E9fono:");
		
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("Modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("Eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstBodegas.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstBodegas.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        buscarPicker.addFormItemClickHandler(new ManejadorBotones(""));
		
		lstBodegas.setFields(idBodega, Bodega, Ubicacion,Telefono);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarBodega(0, 20, objbacklst);
		layout_1.addMember(lstBodegas);
		tabListado.setPane(layout_1);
		tabBodega.addTab(tabListado);
		addMember(tabBodega);
		layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    
		
	}
	public void limpiar(){
		dynamicForm.setValue("txtBodega", " ");  
		dynamicForm.setValue("txtidBodega", " ");
		dynamicForm.setValue("txtUbicacion","");
		dynamicForm.setValue("txtTelefono","");
	}
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers

	}*/
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtBodega").getDisplayValue();
					String ubica=dynamicForm.getItem("txtUbicacion").getDisplayValue();
					String telef=dynamicForm.getItem("txtTelefono").getDisplayValue();
					BodegaDTO BodegaDTO=new BodegaDTO(nombre,ubica,telef); 
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarBodega(BodegaDTO,objback);
					SC.say("se pulso el boton grabar "+nombre);
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.say("se pulso el boton eliminar");
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	BodegaDTO bodega= new BodegaDTO(Integer.parseInt(lstBodegas.getSelectedRecord().getAttribute("idBodega")),
                			lstBodegas.getSelectedRecord().getAttribute("Bodega"),
                			lstBodegas.getSelectedRecord().getAttribute("Ubicacion"),
                			lstBodegas.getSelectedRecord().getAttribute("Telefono"),
                			Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento());
                        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
                        	getService().eliminarBodega(bodega, objback);
                        	limpiar();
            				lstBodegas.removeData(lstBodegas.getSelectedRecord());
            				
            				
                        } else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
				
				
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("Modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtBodega").getDisplayValue();
					String id=dynamicForm.getItem("txtidBodega").getDisplayValue();
					String ubica=dynamicForm.getItem("txtUbicacion").getDisplayValue();
					String telef=dynamicForm.getItem("txtTelefono").getDisplayValue();
					BodegaDTO BodegaDTO=new BodegaDTO(Integer.parseInt(id),nombre,ubica,telef,Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento()); 
					getService().modificarBodega(BodegaDTO,objback);
				}
			}else if(indicador.equalsIgnoreCase("Buscar")){
				String idBodega=dynamicForm.getItem("txtidBodega").getDisplayValue();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().buscarBodega(idBodega, objbackBodega);
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
				    btnEliminar.setDisabled(true);
				    btnModificar.setDisabled(true);
				    btnGrabar.setDisabled(false);
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarBodega(contador-20,contador, objbacklst);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarBodega(contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarBodega(contador-20,contador, objbacklst);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarBodega(registros-registros%20,registros, objbacklst);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarBodega(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}

			
		}
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				String nombre=searchForm.getItem("txtBuscar").getDisplayValue().toUpperCase();
				String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
				if(campo.equalsIgnoreCase("nombre")){
					campo="nombre";
				}else if(campo.equalsIgnoreCase("C\u00F3digo")){
					campo="idBodega";
				}else if(campo.equalsIgnoreCase("Ubicaci\u00F3n")){
					campo="ubicacion";
				}else if(campo.equalsIgnoreCase("Tel\u00E9fono")){
					campo="telefono";
				}
				SC.say(nombre+" "+campo);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().listarBodegaLike(nombre, campo, objbacklst);
			}
			
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				dynamicForm.setValue("txtBodega",lstBodegas.getSelectedRecord().getAttribute("Bodega"));  
				dynamicForm.setValue("txtidBodega", lstBodegas.getSelectedRecord().getAttribute("idBodega"));
				dynamicForm.setValue("txtUbicacion", lstBodegas.getSelectedRecord().getAttribute("Ubicacion"));
				dynamicForm.setValue("txtTelefono", lstBodegas.getSelectedRecord().getAttribute("Telefono"));
				tabBodega.selectTab(0);
				btnEliminar.setDisabled(false);
				btnModificar.setDisabled(false);
				btnGrabar.setDisabled(true);
			}
			
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			String nombre=searchForm.getItem("txtBuscar").getDisplayValue().toUpperCase();
			String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
			if(campo.equalsIgnoreCase("nombre")||campo.equalsIgnoreCase("")){
				campo="nombre";
			}else if(campo.equalsIgnoreCase("C\u00F3digo")){
				campo="idBodega";
			}else if(campo.equalsIgnoreCase("Ubicaci\u00F3n")){
				campo="ubicacion";
			}else if(campo.equalsIgnoreCase("Tel\u00E9fono")){
				campo="telefono";
			}
			//SC.say(nombre+" "+campo);
			 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
				 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			 else if(campo.equals("nombre")||campo.equals("idBodega")||campo.equals("ubicacion")||campo.equals("telefono")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarBodegaLike(nombre, campo, objbacklst);
		}
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			
		}
	}
	final AsyncCallback<List<BodegaDTO>>  objbacklst=new AsyncCallback<List<BodegaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<BodegaDTO> result) {
			getService().numeroRegistrosBodega("Tblbodega", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
				
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new BodegaRecords((BodegaDTO)result.get(i)));
            }
            lstBodegas.setData(listado);
            lstBodegas.redraw();
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
       }
		
	};
	final AsyncCallback<BodegaDTO>  objbackBodega=new AsyncCallback<BodegaDTO>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}
		@Override
		public void onSuccess(BodegaDTO result) {
			// TODO Auto-generated method stub
			if(result!=null){
				
				dynamicForm.setValue("txtBodega", result.getBodega());  
				dynamicForm.setValue("txtidBodega", result.getIdBodega());
				dynamicForm.setValue("txtUbicacion", result.getUbicacion());
				dynamicForm.setValue("txtTelefono", result.getTelefono());
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado");
			}
		}
		
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			
		}

		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			SC.say(result);
			getService().listarBodega(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
			//SC.say("Numero de registros "+registros);
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
