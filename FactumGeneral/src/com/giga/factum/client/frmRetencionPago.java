package com.giga.factum.client;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;

public class frmRetencionPago extends VLayout{
	DynamicForm frmCabecera = new DynamicForm();
	LinkedList <ImpuestoDTO> Impuestos=new LinkedList <ImpuestoDTO>();
	LinkedHashMap<String,String> MapImpuesto = new LinkedHashMap<String,String>();
	ListGridField impuesto= new ListGridField("lstImpuesto", "Impuesto");
	ListGrid listGrid = new ListGrid();
	FloatItem txtTotal = new FloatItem();
	DtocomercialDTO Fact=new DtocomercialDTO();
	LinkedList<RetencionDTO> det=new LinkedList<RetencionDTO>();
	ListGridField baseImponible= new ListGridField("baseImponible", "Base Imponible");
	ListGridField lstBaseImponible= new ListGridField("lstBaseImponible", "Base Valor");
	ListGridField lstPorcentage=new ListGridField("lstPorcentage", "% Retenci\u00F3n");
	String idImpuesto="";
	TextItem txtRuc = new TextItem("txtRuc", "R.U.C");
	TextItem txtPersona = new TextItem("txtPersona", "Se\u00F1ore(s)");
	TextItem txtDireccion=new TextItem("txtDireccion", "Direcci\u00F3n");
	TextItem txtNumDoc=new TextItem("txtNumDoc", "N&uacute;mero de Documento");
	TextItem txtTipoComprobante = new TextItem("txtTipoComprobante", "Tipo de Comprobante");
	TextItem txtAutorizacion= new TextItem("txtAutorizacion", "N\u00B0 Autorizaci\u00F3n");
	DateItem txtFechaEmision = new DateItem("fecha_emision","FECHA&nbspEMISI\u00D3N");
	TextItem  txtNumero=new TextItem("txtNumero", "N\u00FAmero Retenci&oacute;n");
	final Window winClientes = new Window();  
	frmListClientes frmlisCli=new frmListClientes("tblclientes");
	String idCliente="";
	Integer DocFis=0;
	Integer idVendedor=0;
	User usuario = new User();
	TblpagoDTO pagodto = new TblpagoDTO();
	IButton btnGuardar = new IButton("Guardar");
	double totalretencion = 0;
	int tipoTrans=0;
	int lastNumRealRet=0;
	String serie;
	 List<TblpagoDTO> pagos = null;
	 // frmPagos fp ;
	public frmRetencionPago(DtocomercialDTO fact,TblpagoDTO pagodto , List<TblpagoDTO> pagos, int tipoTrans ) {
		try{
			PuntoEmisionDTO punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			serie=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			int tipoDoc=(tipoTrans==0)?5:19;
//			getService().ultimaRetencion("where tbldtocomercialByIdDtoComercial.tipoTransaccion = "
////					+ "'"+tipoDoc+"' "
//					,tipoDoc
//					, "tbldtocomercialByIdDtoComercial.idDtoComercial", objbackDto);
			Fact=fact;
			this.tipoTrans=tipoTrans;
			txtNumero.setRequired(true);
			txtRuc.setRequired(true);
			txtPersona.setRequired(true);
		//	txtDireccion.setRequired(true);
			txtNumDoc.setRequired(true);
			txtTipoComprobante.setRequired(true);
			txtAutorizacion.setRequired(true);
			txtFechaEmision.setRequired(true);
			this.pagodto=pagodto;
			VLayout layoutCabecera = new VLayout();
			layoutCabecera.setSize("100%", "30%");
			txtFechaEmision.setUseTextField(true);
			txtFechaEmision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaEmision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaEmision.setValue(new Date());
			frmCabecera.setSize("100%", "100%"); 
			frmCabecera.setNumCols(4);
			
			txtNumero.setDisabled(false);   
			txtNumero.setColSpan(2);
//			txtNumero.setMask("###-###-#########");
			txtNumero.setMask("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
			txtNumero.setMaskPromptChar("0");
			txtNumero.setMaskPadChar("0");
			txtNumero.addChangedHandler(new com.smartgwt.client.widgets.form.fields.events.ChangedHandler(){

				@Override
				public void onChanged(com.smartgwt.client.widgets.form.fields.events.ChangedEvent event) {
					// TODO Auto-generated method stub
					String val=(String)event.getValue();
					while (val.length()<15){
						val+="0";
					}
					event.getItem().setValue(val);
				}
				
			});
			txtNumero.setMaskOverwriteMode(true);
//			txtNumero.setKeyPressFilter("[0-9]");
			txtNumero.setRequired(true);

//			txtRuc.addChangedHandler(new ManejadorBotones("RUC"));
			txtRuc.setDisabled(false);
			txtRuc.setKeyPressFilter("[0-9]");
			txtRuc.setLength(13);
			
			txtAutorizacion.setValue("1113975616"); 
			//txtRuc.setValue(String.valueOf(fact.getTblpersonaByIdPersona().getCedulaRuc()));
			txtPersona.setDisabled(false);
			
			txtPersona.addChangedHandler(new ManejadorBotones("RUC"));
			
			txtTipoComprobante.setDisabled(false);
			if(fact.getTipoTransaccion()==0){
				txtPersona.setValue(Factum.banderaNombreEmpresa);
				txtRuc.setValue(Factum.banderaRUCEmpresa);
				txtDireccion.setValue(Factum.banderaDireccionEmpresa);
				txtTipoComprobante.setValue("FACTURA DE VENTA");
				txtNumDoc.setValue(fact.getNumRealTransaccion());
				txtPersona.setDisabled(true);
				txtRuc.setDisabled(true);
				txtDireccion.setDisabled(true);
				txtTipoComprobante.setDisabled(true);
				txtNumDoc.setDisabled(true);
			}else if(fact.getTipoTransaccion()==1){
				getService().ultimaRetencion("where tbldtocomercialByIdDtoComercial.tipoTransaccion = ",tipoDoc
						, "tbldtocomercialByIdDtoComercial.idDtoComercial", objbackDto);
				txtPersona.setValue(fact.getTblpersonaByIdPersona().getRazonSocial());
				txtRuc.setValue(fact.getTblpersonaByIdPersona().getCedulaRuc());
				txtDireccion.setValue(fact.getTblpersonaByIdPersona().getDireccion());
				txtTipoComprobante.setValue("FACTURA DE COMPRA");
				txtNumDoc.setValue(fact.getNumCompra());
				txtPersona.setDisabled(true);
				txtRuc.setDisabled(true);
				txtDireccion.setDisabled(true);
				txtTipoComprobante.setDisabled(true);
				txtNumDoc.setDisabled(true);
			}
			
			
			
			//txtDireccion.setValue(Fact.getTblpersonaByIdPersona().getDireccion());
		
			frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtFechaEmision, txtRuc, txtTipoComprobante, txtAutorizacion, txtDireccion,txtNumDoc});
			layoutCabecera.addMember(frmCabecera);
			addMember(layoutCabecera);
			
			VLayout layoutDetalle = new VLayout();
			layoutDetalle.setSize("100%", "65%");
			
			listGrid.setCanEdit(true);
			listGrid.setCanSelectAll(true);
			
			//baseImponible.setValueMap("Iva","Sub Total");
			baseImponible.addChangeHandler(new ManejadorBotones("base"));
			baseImponible.setCanEdit(true);
			lstPorcentage.addChangeHandler(new ManejadorBotones("porcentaje"));
			lstBaseImponible.addChangeHandler(new ManejadorBotones("valor"));
			
			impuesto.setAutoFetchDisplayMap(true);
			
			ListGridField lstcodigoRet=new ListGridField("lstCodigo", "C\u00F3digo");
			ListGridField lstvalorRet=new ListGridField("lstValor", "Valor Retenido");
			
			baseImponible.setCanEdit(false);
//			lstBaseImponible.setCanEdit(false);
			lstcodigoRet.setCanEdit(false);
			lstPorcentage.setCanEdit(false);
			lstvalorRet.setCanEdit(false);
			
			listGrid.setFields(new ListGridField[] { impuesto,baseImponible,lstBaseImponible ,  lstcodigoRet, lstPorcentage, lstvalorRet});
			layoutDetalle.addMember(listGrid);
			impuesto.addChangeHandler(new ManejadorBotones("impuesto"));
			impuesto.addChangedHandler(new ManejadorBotones("impuesto"));
	        
	        HStack hStack_1 = new HStack();
	        hStack_1.setSize("100%", "8%");
	        
	        IButton btnAdd = new IButton("Agregar");
	        hStack_1.addMember(btnAdd);
	        
	        IButton btnNewIbutton = new IButton("Eliminar");
	        btnNewIbutton.addClickHandler(new ManejadorBotones("EImpuesto"));
	        hStack_1.addMember(btnNewIbutton);
	        btnAdd.addClickHandler(new ClickHandler() {  
	            public void onClick(ClickEvent event) {  
	            	//listGrid.startEditingNew();  
	            	listGrid.addData(new ListGridRecord());
	            	listGrid.selectRecord(listGrid.getRecords().length);
	            	listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", "Seleccione un Impuesto");
	            	//SC.say("Seleccione un Impuesto");
	            }  
	        });  
        
	        DynamicForm dynamicForm_1 = new DynamicForm();
	        dynamicForm_1.setItemLayout(FormLayoutType.TABLE);
	        txtTotal.setLeft(300);
	        txtTotal.setTop(0);
	        
	        txtTotal.setTextAlign(Alignment.LEFT);
	        txtTotal.setTitle("TOTAL");
	        dynamicForm_1.setFields(new FormItem[] { txtTotal});
	        layoutDetalle.addMember(dynamicForm_1);
	        layoutDetalle.addMember(hStack_1);
			addMember(layoutDetalle);
			
			HStack hStack = new HStack();
			hStack.setSize("100%", "5%");
			
			hStack.addMember(btnGuardar);
			btnGuardar.addClickHandler(new ManejadorBotones("grabar"));
		
			IButton btnLimpiar = new IButton("Limpiar");
			hStack.addMember(btnLimpiar);
			
			IButton btnImprimir = new IButton("Imprimir");
			hStack.addMember(btnImprimir);
			addMember(hStack);
//			getService().listarImpuesto(0,100, objbacklst);
			getService().listarImpuesto(tipoTrans+1,objbacklst);
			getService().getUserFromSession(callbackUser);
			this.pagos = pagos;
		//	this.fp = fp;
		}catch (Exception e){
			//SC.say(e.getMessage());
			
		}
		
	}
	
	


	/*public frmRetencionPago(DtocomercialDTO result, TblpagoDTO pagodto2,
			List<TblpagoDTO> pagos2, frmPagos class1) {
		// TODO Auto-generated constructor stub
	}*/

	final AsyncCallback<RetencionDTO> objbackDto = new AsyncCallback<RetencionDTO>() {
		// String coincidencias = "";
		public void onSuccess(RetencionDTO result) {
			if (result != null) {
				txtAutorizacion.setValue(result.getAutorizacionSri());
//				Integer c = Integer.valueOf(result.getNumRealRetencion().split("-")[2]);
//				c += 1;
				//String ct=c.toString();
				String ct=String.valueOf(Integer.valueOf(result.getNumRealRetencion().split("-")[2])+1);
				int cont=9-ct.length();
				while (cont>0)	{ct="0"+ct;cont--;}
				lastNumRealRet=result.getTbldtocomercialByIdDtoComercial().getNumRealTransaccion()+1;
				txtNumero.setValue(serie+"-"+ct.toString());
			} else {
				// Aqui analizamos si es diferente de gasto
				txtNumero.setValue(serie+"-"+"000000001");
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};



	private class ManejadorBotones implements DoubleClickHandler,com.smartgwt.client.widgets.grid.events.ChangedHandler,ChangedHandler,ChangeHandler,ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		String Nombre="";
		String Direccion="";
		String Ruc="";
		String Autorizacion="";
		String fecha="";
		String tipo="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void datosfrm(){
			try{ 
				Nombre=frmCabecera.getItem("txtPersona").getDisplayValue();
				Ruc=frmCabecera.getItem("txtRuc").getDisplayValue();
				Autorizacion=frmCabecera.getItem("txtAutorizacion").getDisplayValue();
				Direccion=(String)frmCabecera.getItem("txtDireccion").getDisplayValue();
				fecha=frmCabecera.getItem("txtFechaEmision").getAttribute("txtFechaEmision");
				tipo=frmCabecera.getItem("txtTipoComprobante").getDisplayValue();
			}catch(Exception e){
				//SC.say(e.getMessage());
			}
			
		}
		public void Total(){
			int j=listGrid.getRecords().length;
			try{
				Double total = 0.0;
				for(int i=0;i<j;i++){
					total=total+listGrid.getRecord(i).getAttributeAsDouble("lstValor");
				}
				total=Math.rint(total*100)/100;
//				total=(CValidarDato.getDecimal(2, total)!=0.00)?CValidarDato.getDecimal(2, total):total;
				total=CValidarDato.getDecimal(2, total);
				txtTotal.setValue(total);
			//SC.say("calculando total="+total+" "+ j);
			}catch(Exception e){
				txtTotal.setValue(0.00);
				//SC.say(e.getMessage()); 
			}
		}
		
		@Override 
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("grabar")){
				if(frmCabecera.validate()){
					if(listGrid.getRecords().length!=0){
					
						try{
							String imp=""; 
							String p="";
							Set tblretencions = new HashSet(0);
							totalretencion =0.0;
							det=new LinkedList<RetencionDTO>();
							for(int i=0;i<listGrid.getRecords().length;i++){
								imp=listGrid.getRecord(i).getAttribute("lstImpuesto");
								RetencionDTO ret=new RetencionDTO();	
								ret.setAutorizacionSri(Fact.getAutorizacion());
								ret.setBaseImponible(listGrid.getRecord(i).getAttributeAsDouble("lstBaseImponible"));
//								ret.setdtocomercial(null);
								ret.setTbldtocomercialByIdDtoComercial(null);
								ret.setEstado('1');
								ret.setNumRealRetencion(txtNumero.getDisplayValue());
								DtocomercialDTO dtoTemp=new DtocomercialDTO();
								dtoTemp.setIdEmpresa(Factum.empresa.getIdEmpresa());
								dtoTemp.setEstablecimiento(Factum.getEstablecimientoCero());
								dtoTemp.setPuntoEmision(Factum.getPuntoEmisionCero());
								dtoTemp.setIdDtoComercial(Fact.getIdDtoComercial());
								ret.setTbldtocomercialByIdFactura(dtoTemp);
//								ret.setTbldtocomercialByIdDtoComercial(Fact.getIdDtoComercial());
								ret.setNumero(txtNumDoc.getDisplayValue());
								p=p+" "+String.valueOf(ret.getBaseImponible())+" ";
								for(int j=0;j<Impuestos.size();j++){
									//SC.say(imp+" "+String.valueOf(Impuestos.get(j).getIdImpuesto()));
									if(imp.equals(String.valueOf(Impuestos.get(j).getIdImpuesto()))){
										ret.setImpuesto(Impuestos.get(j));
										break;
									}
								}
								ret.setValorRetenido(listGrid.getRecord(i).getAttributeAsDouble("lstValor"));
								det.add(ret);
								tblretencions.add(ret);
								totalretencion = totalretencion + listGrid.getRecord(i).getAttributeAsDouble("lstValor");
							}
							totalretencion=Math.rint(totalretencion*100)/100;
//							total=(CValidarDato.getDecimal(2, total)!=0.00)?CValidarDato.getDecimal(2, total):total;
							totalretencion=CValidarDato.getDecimal(2, totalretencion);
							DtocomercialDTO doc=new DtocomercialDTO();
							doc.setIdEmpresa(Factum.empresa.getIdEmpresa());
							doc.setEstablecimiento(Factum.getEstablecimientoCero());
							doc.setPuntoEmision(Factum.getPuntoEmisionCero());
							doc.setNumRealTransaccion(lastNumRealRet);
							doc.setAutorizacion(frmCabecera.getItem("txtAutorizacion").getDisplayValue());
//							doc.setAutorizacion(txtTipoComprobante.getDisplayValue()+"//"+txtNumDoc.getDisplayValue());
							doc.setEstado('1');
							doc.setExpiracion(txtFechaEmision.getDisplayValue());
							doc.setFecha(txtFechaEmision.getDisplayValue());
							doc.setIdAnticipo(null);
							doc.setNumPagos(0);
							doc.setTbldtocomercialdetalles(null);
							doc.setTbldtocomercialTbltipopagos(null);
							doc.setTblmovimientos(null);
							doc.setTblretencionsForIdDtoComercial(tblretencions);
							/*
							 * Si es un documento de venta debe ser una retencion recibida (tipo 5), 
							 * si es documento de compra debe ser una factura emitida (tipo 19)
							 */
							int tipoDoc=(tipoTrans==0)?5:19;
							doc.setTipoTransaccion(tipoDoc);
							doc.setIdNotaCredito(null);
							doc.setIdRetencion(null);
							doc.setSubtotal(0.0);
							doc.setTblpagos(null);
							
							PersonaDTO per=new PersonaDTO();
							idCliente=String.valueOf(Fact.getTblpersonaByIdPersona().getIdPersona());
							PersonaDTO facturaPor = new PersonaDTO();
							facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
							per.setIdPersona(Integer.parseInt(idCliente));
							doc.setTblpersonaByIdPersona(per);
							per=new PersonaDTO();
							per.setIdPersona(usuario.getIdUsuario());
							doc.setTblpersonaByIdVendedor(per);
							doc.setTblpersonaByIdFacturaPor(facturaPor);
							doc.setObservacion(usuario.getUserName());
							pagodto.setFormaPago("3");
							getService().pagoConRetencion(det,doc,pagodto,objback);
							//SC.say("listo el cliente "+String.valueOf(det.size())+" idC= "+String.valueOf(idCliente)
								//	+" idV= "+String.valueOf(idVendedor));
							
						}catch(Exception e){
							SC.say("error"+e.getMessage());
						}
					}else{
						SC.say("Agrege un impuesto para proceder");
					}
				}
			}else if(indicador.equalsIgnoreCase("eimpuesto")){
				 listGrid.removeSelectedData();
				// det.remove(listGrid.getRecordIndex(listGrid.getSelectedRecord()));
				 Total();
				 //SC.say("eliminar");
			}
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onChange(ChangeEvent event) {
			Double base=0.0;
			boolean ban=true;
			String evento=(String)event.getValue();
			if(indicador.equalsIgnoreCase("impuesto")){
				idImpuesto=evento;
				//if(baseImponible.getCanEdit()){
					for(int j=0;j<listGrid.getRecords().length;j++){
						if(listGrid.getRecord(j).getAttribute("lstImpuesto").equals(idImpuesto)){
							ban=false;
							listGrid.removeSelectedData();
							SC.say("Error: Impuesto ya ingresado...");
							break;
						}
						
					}
					if(ban){
						for(int i=0;i<Impuestos.size();i++){
							if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
								String baseImp=(Impuestos.get(i).getTipo()==1)?"Sub Total":"Iva";
								listGrid.getSelectedRecord().setAttribute("baseImponible",baseImp);
								double descuento =0.0;
								double valImp=0.0;
								for(DtoComDetalleDTO dtocomercialle :  Fact.getTbldtocomercialdetalles()){
									String impuestos="";
									double impuestoPorc=1.0;
									double impuestoValor=0.0;
									int i1=0;
									//com.google.gwt.user.client.Window.alert("impuestos de detalle "+dtocomercialle.getTblimpuestos().size());
									for (DtoComDetalleMultiImpuestosDTO detalleImp: dtocomercialle.getTblimpuestos()){
										i1=i1+1;
										impuestos+=detalleImp.getPorcentaje().toString();
										impuestos= (i1<dtocomercialle.getTblimpuestos().size())?impuestos+",":impuestos; 
										impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
										impuestoValor=impuestoValor+((dtocomercialle.getCantidad()*dtocomercialle.getPrecioUnitario()*(1-(dtocomercialle.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
									}
									//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
									valImp+=impuestoValor;
									descuento = descuento + ((dtocomercialle.getTotal()*dtocomercialle.getDepartamento())/(100-dtocomercialle.getDepartamento()));
								}
								if(baseImp.equalsIgnoreCase("Iva")){
//									base=(Fact.getSubtotal()-descuento)*(Factum.banderaIVA/100);
//									//////base=(Fact.getSubtotal()-descuento)+valImp;
									base=valImp;
									base=Math.rint(base*100)/100;
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
								}else if(baseImp.equalsIgnoreCase("Sub Total")){
									base=(Fact.getSubtotal()-descuento);
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
								}
								
								listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
								listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
								Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
								Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
								valor=porcentaje*valor/100;
								valor=Math.rint(valor*100)/100;
								valor=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, valor);
								//SC.say("cambio "+valor);
								listGrid.getSelectedRecord().setAttribute("lstValor",valor);
								break;
								
							}
						}
					}
				//}
//				baseImponible.setCanEdit(true);
			}else if(indicador.equalsIgnoreCase("base")){
				//SC.say("hola cambiando la base");
				double descuento =0.0;
				double valImp=0.0;
					for(DtoComDetalleDTO dtocomercialle :  Fact.getTbldtocomercialdetalles()){
						String impuestos="";
						double impuestoPorc=1.0;
						double impuestoValor=0.0;
						int i1=0;
						//com.google.gwt.user.client.Window.alert("impuestos de detalle "+dtocomercialle.getTblimpuestos().size());
						for (DtoComDetalleMultiImpuestosDTO detalleImp: dtocomercialle.getTblimpuestos()){
							i1=i1+1;
							impuestos+=detalleImp.getPorcentaje().toString();
							impuestos= (i1<dtocomercialle.getTblimpuestos().size())?impuestos+",":impuestos; 
							impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
							impuestoValor=impuestoValor+((dtocomercialle.getCantidad()*dtocomercialle.getPrecioUnitario()*(1-(dtocomercialle.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
						}
						//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
						valImp+=impuestoValor;
						descuento = descuento + ((dtocomercialle.getTotal()*dtocomercialle.getDepartamento())/(100-dtocomercialle.getDepartamento()));
					}
				if(evento.equalsIgnoreCase("iva")){
//					base=(Fact.getSubtotal()-descuento)*(Factum.banderaIVA/100);
					base=(Fact.getSubtotal()-descuento)+valImp;
					base=Math.rint(base*100)/100;
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
					//SC.say("base iva "+evento);
				}else if(evento.equalsIgnoreCase("Sub Total")){
					
					
					base=(Fact.getSubtotal()-descuento);
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
					//SC.say("base sub Total "+evento+" "); 
				}
//				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstCodigo");
				
				for(int i=0;i<Impuestos.size();i++){
//					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getCodigo()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						
						SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			
			}else if(indicador.equals("valor")){
				//SC.say("hola cambiando la base");
				//SC.say("base iva "+evento);
				listGrid.getSelectedRecord().setAttribute("lstBaseImponible",event.getValue());
				base=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
				//SC.say("base sub Total "+evento+" "); 
			
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				
				for(int i=0;i<Impuestos.size();i++){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			}else if(indicador.equals("porcentaje")){
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				for(int i=0;i<Impuestos.size();i++){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",event.getValue());
						Impuestos.get(i).setRetencion(listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage"));
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						//listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			}
		Total();	
	}	
		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("RUC")){
				winClientes.setWidth(930);  
				winClientes.setHeight(610);  
				winClientes.setTitle("Buscar Cliente");  
				winClientes.setShowMinimizeButton(false);  
				winClientes.setIsModal(true);  
				winClientes.setShowModalMask(true);  
				winClientes.centerInPage();  
				winClientes.addCloseClickHandler(new CloseClickHandler() {  
					@Override
					public void onCloseClick(CloseClientEvent event) {
						winClientes.destroy();  
						
					}  
                });   
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                frmlisCli=new frmListClientes("tblclientes");
                frmlisCli.lstCliente.addDoubleClickHandler(new ManejadorBotones("cliente"));
                frmlisCli.setSize("100%","100%"); 
                form.addMember(frmlisCli);
                winClientes.addItem(form);
                winClientes.show();
			}
			
		}
		@Override
		public void onChanged(com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
			String evento=(String)event.getValue();
			if(indicador.equalsIgnoreCase("impuesto")){
				listGrid.saveAllEdits();
				listGrid.endEditing();
				listGrid.deselectAllRecords();
			}
		}
		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equals("cliente")){
				idCliente=frmlisCli.lstCliente.getSelectedRecord().getAttribute("idPersona");
				txtPersona.setValue(frmlisCli.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
//				txtPersona.setValue(frmlisCli.lstCliente.getSelectedRecord().getAttribute("NombreComercial")+" "+
//						frmlisCli.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
				txtDireccion.setValue(frmlisCli.lstCliente.getSelectedRecord().getAttribute("Direccion"));
				txtRuc.setValue(frmlisCli.lstCliente.getSelectedRecord().getAttribute("Cedula"));
				winClientes.destroy();
				SC.say("Nombre "+frmlisCli.lstCliente.getSelectedRecord().getAttribute("NombreComercial"));
			}
			
		}

		
	} 
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			for(int i=0;i<listGrid.getRecords().length;i++){
				listGrid.removeData(listGrid.getRecord(i));
			}
			String ids=" ";
			boolean borrar=true;
			int cont=0;
			final Window winPago = new Window();
			winPago.setWidth(930);  
			winPago.setHeight(610);  
			winPago.setTitle("Pagos");  
			winPago.setShowMinimizeButton(false);  
			winPago.setIsModal(true);  
			winPago.setShowModalMask(true);  
			winPago.setKeepInParentRect(true);
			winPago.centerInPage();  
			winPago.addCloseClickHandler(new CloseClickHandler() {  
	            public void onCloseClick(CloseClientEvent event) {  
	           // 	fp.ac
	            	if (tipoTrans==0){
	            	frmPagos p = new frmPagos ();
	            	p.refrescar();
	            	}else{
	             	frmPagosProveedores pr = new frmPagosProveedores ();
	            	pr.refrescar();
	            	}
	            	winPago.destroy();
	            }   
	        });
			try{
				      
						ids=String.valueOf(pagodto.getIdPago());
						//mostrarPago(ids);
						String idpago=ids;
						double valorAPagar=totalretencion;
						String formaPago="Retenci�n";
						double saldo=0.0;
						TblpagoDTO npago=new TblpagoDTO();
						//String tipo=lstPagos.getSelectedRecord().getAttribute("lstTipo");
						String tipo="Retencion";
						npago = pagodto;
						npago.setIdEmpresa(Factum.empresa.getIdEmpresa());
						npago.setEstablecimiento(Factum.getEstablecimientoCero());
						npago.setFormaPago("Retencion");
						npago.setFechaRealPago(new Date(txtFechaEmision.getDisplayValue()));
						npago.setValor(valorAPagar);
						
						for(int i=0;i<pagos.size();i++){
							if(idpago.equals(String.valueOf(pagos.get(i).getIdPago()))){
							
								saldo=pagos.get(i).getValorPagado()+saldo;
							
							}
						}
						saldo = saldo - totalretencion;
						frmPago frmpago=new frmPago(npago,tipo,valorAPagar,String.valueOf(tipoTrans));
						frmpago.setSize("100%","100%"); 
						frmpago.setPadding(5);   
						frmpago.setLayoutAlign(VerticalAlignment.BOTTOM);
				        winPago.addItem(frmpago);
				      
						//lstPagos.removeData(lstPagos.getRecord(cont));
						//cont--;
					
					//cont++;
					/*if(cont==lstPagos.getRecordList().getLength())
					{
						borrar=false;
					}	*/
					/*for(int i=0;i<lstPagos.getRecordList().getLength();i++){
						if(lstPagos.getRecord(i).getAttributeAsString("Ban").equalsIgnoreCase("true")){
							lstPagos.removeData(lstPagos.getRecord(i));
						}
					}*/
				
				winPago.show();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				SC.say(result);
			}catch(Exception e){
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				SC.say("Err Pantalla "+e.getMessage());
			}
			
		}
	};
	final AsyncCallback<List<ImpuestoDTO>>  objbacklst=new AsyncCallback<List<ImpuestoDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(List<ImpuestoDTO> result) {
			MapImpuesto.clear();
			Impuestos.clear();
            for(int i=0;i<result.size();i++) {
            	Impuestos.add(result.get(i));
            	MapImpuesto.put(String.valueOf(result.get(i).getIdImpuesto()),result.get(i).getNombre());
			}
			impuesto.setValueMap((Map) MapImpuesto); 
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
       }
	};
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
        	
        		
        		usuario=result;
        	//funcionBloquear(true);
			}
        }

        public void onFailure(Throwable caught) {
            
        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}


}
