package com.giga.factum.client;

import com.smartgwt.client.widgets.grid.ListGridRecord;

public class Registro extends ListGridRecord {
	public Registro(){
		
	}
	public Registro(String cedula, String nombre){
		setCedula(cedula);
		setNombre(nombre);
	}
	public void setCedula(String cedula){
		setAttribute("lstCedula",cedula);
	}
	public void setNombre(String nombre){
		setAttribute("lstNombre",nombre);
	}
	public String getCedula(){
		return getAttributeAsString("lstCedula");
	}
	public String getNombre(){
		return getAttributeAsString("lstNombre");
	}
}
