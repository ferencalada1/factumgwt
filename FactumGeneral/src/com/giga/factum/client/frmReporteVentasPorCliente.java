package com.giga.factum.client;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.ReporteVentasEmpleadoDTO;
import com.giga.factum.client.DTO.detalleVentasATSDTO;
import com.giga.factum.client.regGrillas.ATSVentasRecord;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmReporteVentasPorCliente extends VLayout{
	String fechaI="";
	String fechaF="";
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	DynamicForm dynamicForm = new DynamicForm();
	ListGrid ListGridTotales = new ListGrid();
	ListGrid lstReporte = new ListGrid();
	
	HStack hStackSuperior = new HStack();
	VLayout layout_1 = new VLayout();
	double iva=0;
	double iva0=0;
	double Subtotal=0;
	double Total=0;
	
	//public frmReporteVentasPorCliente() {
	public frmReporteVentasPorCliente(int h) {
		dynamicForm.setSize("40%", "15%");
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");  
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setUseTextField(true);
		txtFechaInicial.setValue(new Date());
		txtFechaFinal.setUseTextField(true); 
		txtFechaFinal.setValue(new Date());
		
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal});
		hStackSuperior.setSize("100%", "20%");
		hStackSuperior.addMember(dynamicForm);
		addMember(hStackSuperior);
		lstReporte.setSize("100%", "90%");
		ListGridField tpIdCliente =new ListGridField("tpIdCliente", "tpIdCliente");
		ListGridField idCliente =new ListGridField("idCliente", "idCliente");
		ListGridField tipoComprobante= new ListGridField("tipoComprobante", "Tipo Comprobante");
		ListGridField numeroComprobantes =new ListGridField("numeroComprobantes", "N� Comprobantes");
		ListGridField baseNoGraIva=new ListGridField("baseNoGraIva", "Base No Graba Iva");
		ListGridField baseImponible=new ListGridField("baseImponible", "Base Imponible");
		ListGridField baseImpGrav =new ListGridField("baseImpGrav", "Base Imp. Grav.");
		ListGridField montoIva =new ListGridField("montoIva", "Monto Iva",100);
		ListGridField valorRetIva =new ListGridField("valorRetIva", "valorRetIva");
		ListGridField valorRetRenta =new ListGridField("valorRetRenta", "valorRetRenta");
		lstReporte.setFields(new ListGridField[] {tpIdCliente,idCliente,tipoComprobante,numeroComprobantes,baseNoGraIva,baseImponible,baseImpGrav,montoIva,valorRetIva,valorRetRenta});
		lstReporte.setShowRowNumbers(true);
		addMember(lstReporte); 
		
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "10%");
		
		IButton btnReporte = new IButton("Reporte");
		hStack.addMember(btnReporte);
		btnReporte.addClickHandler(new ManejadorBotones("generar"));
		
		IButton btnAtsVentas = new IButton("ATS VENTAS");
		hStack.addMember(btnAtsVentas);
		btnAtsVentas.addClickHandler(new ManejadorBotones("exportar"));
		addMember(hStack);
	}
	private class ManejadorBotones implements  com.smartgwt.client.widgets.events.ClickHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		
		public ManejadorBotones(String ind){
			indicador=ind;
		}
		public void datosfrm(){
			try{
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
			}catch(Exception e){
				SC.say(e.getMessage());
			}
		}
		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){ 
				datosfrm();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				//getService().ATSVentas(fechaI, fechaF, listaCallback);
				
			}
			if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstReporte);
			}
			
		}
	}
	final AsyncCallback<LinkedList<detalleVentasATSDTO>>  listaCallback=new AsyncCallback<LinkedList<detalleVentasATSDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(LinkedList<detalleVentasATSDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];	
			SC.say("generado");
			for(int i=0;i<result.size();i++){
				ATSVentasRecord doc =new ATSVentasRecord(result.get(i));
				listado[i]=(new ATSVentasRecord(result.get(i)));
			}
			lstReporte.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        //btnGenerarReporte.setDisabled(true);
	    }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
