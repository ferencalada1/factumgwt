package com.giga.factum.client;

public class RetencionDetalle implements java.io.Serializable 
{
	//select tc.tipoTransaccion,tr.tblimpuesto.idImpuesto,sum(tr.valorRetenido) 
	//from Tblretencion tr, Tbldtocomercial tc, Tblpersona tp 
	//where tr.tbldtocomercial.idDtoComercial=tc.idDtoComercial and 
	//tc.tblpersonaByIdPersona.idPersona=tp.idPersona  and tc.tipoTransaccion=0 
	//and tr.tblimpuesto.idImpuesto=3	
	private static final long serialVersionUID = 1L;	
	private int tipoTransaccion;
	private int idImpuesto;
	private double totalRetencion;
	
	public RetencionDetalle() {}

	public RetencionDetalle(int tipoTransaccion,int idImpuesto,double totalRetencion) 
	{
		this.tipoTransaccion=tipoTransaccion;
		this.idImpuesto=idImpuesto;
		this.totalRetencion=totalRetencion;	
	}
		
	
	public int getTipoTransaccion() 
	{return this.tipoTransaccion;}
	public void setTipoTransaccion(int tipoTransaccion) 
	{this.tipoTransaccion = tipoTransaccion;}

	
	public int getIdImpuesto() 
	{return this.idImpuesto;}
	public void setIdImpuesto(int idImpuesto) 
	{this.idImpuesto = idImpuesto;}
			
	public double getTotalRetencion() 
	{return this.totalRetencion;}
	public void setTotalRetencion(double totalRetencion) 
	{this.totalRetencion = totalRetencion;}
	
}

