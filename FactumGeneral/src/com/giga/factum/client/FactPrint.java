package com.giga.factum.client;



import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.PosicionDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class FactPrint extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	final TextItem txtPos4X = new TextItem("txtPos4X", "Largo");
	final TextItem txtPos6X = new TextItem("txtPos6X", "Largo");
	final TextItem txtPos1X = new TextItem("txtPos1X", "Largo");
	final TextItem txtPos3X = new TextItem("txtPos3X", "Largo");
	final TextItem txtPos5X = new TextItem("txtPos5X", "Largo");
	final TextItem txtCabecera=new TextItem("txtCabecera", "Cabecera Y");
	final TextItem txtCabeceraX =new TextItem("txtCabeceraX", "Cabecera X");
	TextItem txtDetalle = new TextItem("txtDetalle", "Detalle Y");
	TextItem txtDetalleX = new TextItem("txtDetalleX", "Detalle X");
	final ListGrid listGrid = new ListGrid();
	final TextItem txtPos2X = new TextItem("txtPos2X", "Largo");
	final ComboBoxItem cmbPos5 = new ComboBoxItem("cmbPos5", "Seleccione&nbspun&nbspCampo");
	final ComboBoxItem cmbPos6 = new ComboBoxItem("cmbPos6", "Seleccione&nbspun&nbspCampo");
	final ComboBoxItem cmbPos3 = new ComboBoxItem("cmbPos3", "Seleccione&nbspun&nbspCampo");
	final ComboBoxItem cmbPos1 = new ComboBoxItem("cmbPos1", "Seleccione&nbspun&nbspCampo");
	final ComboBoxItem cmbdoc=new ComboBoxItem("cmbDoc", "Seleccione&nbspun&nbspDocumento");
	final ComboBoxItem cmbPos2 = new ComboBoxItem("cmbPos2", "Seleccione&nbspun&nbspCampo");
	final ComboBoxItem cmbPos4 = new ComboBoxItem("cmbPos4", "Seleccione&nbspun&nbspCampo");
	final TextItem txtDetPos1 = new TextItem("txtDetPos1", "Largo");
	final ComboBoxItem cmbDetPos1 = new ComboBoxItem("cmbDetPos1", "Seleccione&nbspun&nbspCampo");
	ComboBoxItem cmbDetPos2 = new ComboBoxItem("cmbDetPos2", "Seleccione&nbspun&nbspCampo");
	LinkedList<PosicionDTO> listpos = new LinkedList<PosicionDTO>();
	ComboBoxItem cmbDetPos3 = new ComboBoxItem("cmbDetPos3", "Seleccione&nbspun&nbspCampo");
	ComboBoxItem cmbDetPos4 = new ComboBoxItem("cmbDetPos4", "Seleccione&nbspun&nbspCampo");
	TextItem txtDetPos2 = new TextItem("txtDetPos2", "Largo");
	TextItem txtDetPos3 = new TextItem("txtDetPos3", "Largo");
	TextItem txtDetPos4 = new TextItem("txtDetPos4", "Largo");
	TextItem txtPieY = new TextItem("txtPieY", "Pie Y");
	TextItem txtPieX = new TextItem("txtPieX", "Pie X");
	ComboBoxItem cmbPiePos1= new ComboBoxItem("cmbPiePos1", "Seleccione&nbspun&nbspCampo");
	ComboBoxItem cmbPiePos2 = new ComboBoxItem("cmbPiePos2", "Seleccione&nbspun&nbspCampo");
	ComboBoxItem cmbPiePos3 = new ComboBoxItem("cmbPiePos3", "Seleccione&nbspun&nbspCampo");
	ComboBoxItem cmbPiePos4 = new ComboBoxItem("cmbPiePos4", "Seleccione&nbspun&nbspCampo");
	
	TextItem txtPiePos1 = new TextItem("txtPiePos1", "Largo");
	TextItem txtPiePos2 = new TextItem("txtPiePos2", "Largo");
	TextItem txtPiePos3 = new TextItem("txtPiePos3", "Largo");
	TextItem txtPiePos4 = new TextItem("txtPiePos4", "Largo");
	public FactPrint() {
		
		setSize("900", "600");
		DynamicForm dynamicForm_1 = new DynamicForm();
		dynamicForm_1.setSize("100%", "30%");
		dynamicForm_1.setNumCols(8);
		
		cmbPos2.setAlign(Alignment.LEFT);
		cmbPos2.setTextAlign(Alignment.RIGHT);
		cmbPos2.setValueMap("Nombre","Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
		
		cmbPos4.setAlign(Alignment.LEFT);
		cmbPos4.setTextAlign(Alignment.RIGHT);
		cmbPos4.setValueMap("Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
		
		cmbPos5.setAlign(Alignment.LEFT);
		cmbPos5.setValueMap("Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
		
		cmbPos6.setAlign(Alignment.LEFT);
		cmbPos6.setValueMap("Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
				
		cmbPos3.setAlign(Alignment.LEFT);
		cmbPos3.setValueMap("Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
		
		cmbPos1.setAlign(Alignment.LEFT);
		cmbPos1.setValueMap("Direccion","Fecha","Cliente","Vendedor","Ruc","Tel�fono");
		
		txtPos4X.setAlign(Alignment.LEFT);
		txtPos4X.setWidth(50);
		txtPos2X.setAlign(Alignment.LEFT);
		txtPos6X.setAlign(Alignment.LEFT);
		txtPos1X.setAlign(Alignment.LEFT);
		txtPos3X.setAlign(Alignment.LEFT);
		txtPos1X.setWidth(50);
		txtPos1X.setKeyPressFilter("[0-9]");
		txtPos2X.setWidth(50);
		txtPos2X.setKeyPressFilter("[0-9]");
		txtPos3X.setWidth(50);
		txtPos3X.setKeyPressFilter("[0-9]");
		txtPos4X.setWidth(50);
		txtPos4X.setKeyPressFilter("[0-9]");
		txtPos5X.setWidth(50);
		txtPos5X.setAlign(Alignment.LEFT);
		txtPos5X.setKeyPressFilter("[0-9]");
		txtPos6X.setWidth(50);
		txtPos6X.setKeyPressFilter("[0-9]");
		txtCabecera.setRequired(true);
		txtCabecera.setTextAlign(Alignment.LEFT);
		txtCabecera.setAlign(Alignment.LEFT);
		txtCabecera.setWidth(50);
		txtCabecera.setKeyPressFilter("[0-9]");
		txtCabeceraX.setRequired(true);
		txtCabeceraX.setColSpan(4);
		txtCabeceraX.setWidth(50);
		txtCabeceraX.setKeyPressFilter("[0-9]");
		
		txtDetalle.setRequired(true);
		txtDetalle.setWidth(50);
		txtDetalle.setAlign(Alignment.LEFT);
		
		txtDetalleX.setWidth(50);
		txtDetalleX.setRequired(true);
		txtDetalleX.setAlign(Alignment.LEFT);
		
		cmbdoc.setRequired(true);
		cmbdoc.setColSpan(6);
		cmbdoc.setValueMap("Factura","Retenci�n","Nota de Venta");
		cmbdoc.addChangedHandler(new ChangedHandler() {  
			@Override
			public void onChanged(ChangedEvent event) {
					getService().listarPosiciones((String) cmbdoc.getValue(), listback);
				
			}  
        });  
		dynamicForm_1.setFields(new FormItem[] { cmbdoc, txtCabecera, txtCabeceraX, cmbPos1, txtPos1X, cmbPos2, txtPos2X, cmbPos3, txtPos3X, cmbPos4, txtPos4X, cmbPos5, txtPos5X, cmbPos6, txtPos6X, txtDetalle, txtDetalleX});
		addMember(dynamicForm_1);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		IButton cmdGuardar = new IButton("Guardar");
		hStack.addMember(cmdGuardar);
		cmdGuardar.addClickHandler( new ManejadorBotones("Grabar"));
		dynamicForm_2.setSize("100%", "40%");
		dynamicForm_2.setNumCols(16);
		
		cmbDetPos1.setAlign(Alignment.LEFT);
		cmbDetPos2.setAlign(Alignment.LEFT);
		cmbDetPos3.setAlign(Alignment.LEFT);
		cmbDetPos4.setAlign(Alignment.LEFT);
		cmbDetPos1.setValueMap("Cantidad","DESCRIPCION","Precio Unitario","Total Detalle");
		cmbDetPos2.setValueMap("Cantidad","DESCRIPCION","Precio Unitario","Total Detalle");
		cmbDetPos3.setValueMap("Cantidad","DESCRIPCION","Precio Unitario","Total Detalle");
		cmbDetPos4.setValueMap("Cantidad","DESCRIPCION","Precio Unitario","Total Detalle");
		//Descripci\u00F3n
		txtDetPos1.setAlign(Alignment.LEFT);
		txtDetPos1.setWidth(50);
		txtDetPos2.setAlign(Alignment.LEFT);
		txtDetPos2.setWidth(50);
		txtDetPos3.setAlign(Alignment.LEFT);
		txtDetPos3.setWidth(50);
		txtDetPos4.setAlign(Alignment.LEFT);
		txtDetPos4.setWidth(50);
		
		dynamicForm_2.setFields(new FormItem[] { cmbDetPos1, txtDetPos1, cmbDetPos2, txtDetPos2, cmbDetPos3, txtDetPos3, cmbDetPos4, txtDetPos4});
		
		addMember(dynamicForm_2);
		dynamicForm_3.setSize("100%", "20%");
		dynamicForm_3.setNumCols(4);
		
		
		cmbPiePos1.setAlign(Alignment.LEFT);
		cmbPiePos2.setAlign(Alignment.LEFT);
		cmbPiePos3.setAlign(Alignment.LEFT);
		cmbPiePos4.setAlign(Alignment.LEFT);
		
		cmbPiePos1.setValueMap("SubTotal","Descuento","IVA","TOTAL");
		cmbPiePos2.setValueMap("SubTotal","Descuento","IVA","TOTAL");
		cmbPiePos3.setValueMap("SubTotal","Descuento","IVA","TOTAL");
		cmbPiePos4.setValueMap("SubTotal","Descuento","IVA","TOTAL");
		
		
		txtPiePos1.setAlign(Alignment.LEFT);
		txtPiePos2.setAlign(Alignment.LEFT);
		txtPiePos3.setAlign(Alignment.LEFT);
		txtPieX.setAlign(Alignment.LEFT);
		txtPieY.setAlign(Alignment.LEFT);
		
		txtPieY.setWidth(50);
		txtPieX.setWidth(50);
		txtPieX.setWidth(50);
		txtPiePos1.setWidth(50);
		txtPiePos2.setWidth(50);
		txtPiePos3.setWidth(50);
		txtPiePos4.setWidth(50);
		
		dynamicForm_3.setFields(new FormItem[] { txtPieY, txtPieX, cmbPiePos1, txtPiePos1, cmbPiePos2, txtPiePos2, cmbPiePos3, txtPiePos3, cmbPiePos4, txtPiePos4});
		
		addMember(dynamicForm_3);
		addMember(hStack);
				
		 
	}
	
	private class ManejadorBotones implements ClickHandler{
		String indicador="";
		
		
	
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("Grabar")){
				try{
					String NombreDoc="";
					LinkedList<PosicionDTO> list = new LinkedList<PosicionDTO>();
					
					PosicionDTO pos=new PosicionDTO();
					NombreDoc=String.valueOf(cmbdoc.getValue());
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(Integer.parseInt((String) txtCabecera.getValue()));
					pos.setCampo("cabecera");
					pos.setLargo(Integer.parseInt((String) txtCabeceraX.getValue()));
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(1);
					pos.setLargo(Integer.parseInt((String) txtPos1X.getValue()));
					pos.setCampo((String) cmbPos1.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(2);
					pos.setLargo(Integer.parseInt((String) txtPos2X.getValue()));
					pos.setCampo((String) cmbPos2.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(3);
					pos.setLargo(Integer.parseInt((String) txtPos3X.getValue()));
					pos.setCampo((String) cmbPos3.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(4);
					pos.setLargo(Integer.parseInt((String) txtPos4X.getValue()));
					pos.setCampo((String) cmbPos4.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(5);
					pos.setLargo(Integer.parseInt((String) txtPos5X.getValue()));
					pos.setCampo((String) cmbPos5.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(6);
					pos.setLargo(Integer.parseInt((String) txtPos6X.getValue()));
					pos.setCampo((String) cmbPos6.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(Integer.parseInt((String) txtDetalle.getValue()));
					pos.setLargo(Integer.parseInt((String) txtDetalleX.getValue()));
					pos.setCampo("detalle");
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(7);
					pos.setLargo(Integer.parseInt((String) txtDetPos1.getValue()));
					pos.setCampo((String) cmbDetPos1.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);   
					pos.setPosicion(8);
					pos.setLargo(Integer.parseInt((String) txtDetPos2.getValue()));
					pos.setCampo((String) cmbDetPos2.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(9);
					pos.setLargo(Integer.parseInt((String) txtDetPos3.getValue()));
					pos.setCampo((String) cmbDetPos3.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(10);
					pos.setLargo(Integer.parseInt((String) txtDetPos4.getValue()));
					pos.setCampo((String) cmbDetPos4.getValue());
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(Integer.parseInt((String) txtPieY.getValue()));
					pos.setLargo(Integer.parseInt((String) txtPieX.getValue()));
					pos.setCampo("Pie");
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(11);
					pos.setLargo(Integer.parseInt((String) txtPiePos1.getValue()));
					pos.setCampo(String.valueOf(cmbPiePos1.getValue()));
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(12);
					pos.setLargo(Integer.parseInt((String) txtPiePos2.getValue()));
					pos.setCampo(String.valueOf(cmbPiePos2.getValue()));
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(13);
					pos.setLargo(Integer.parseInt((String) txtPiePos3.getValue()));
					pos.setCampo(String.valueOf(cmbPiePos3.getValue()));
					list.add(pos);
					
					pos=new PosicionDTO();
					pos.setNombreDocumento(NombreDoc);
					pos.setPosicion(14);
					pos.setLargo(Integer.parseInt((String) txtPiePos4.getValue()));
					pos.setCampo(String.valueOf(cmbPiePos4.getValue()));
					list.add(pos);
					
					getService().grabarPosiciones(list, objback);
				}catch(Exception e){
					SC.say("Error en pantalla: "+e.getMessage());
				}
				
			}
			
		}
	}
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(String result) {
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
	};
	final AsyncCallback<List<PosicionDTO>>  listback=new AsyncCallback<List<PosicionDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say("Error no existen datos:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		@Override
		public void onSuccess(List<PosicionDTO> result) {
			listpos=(LinkedList<PosicionDTO>) result;
			for(int i=0;i<listpos.size();i++){
				if(listpos.get(i).getCampo().equals("cabecera")){
					txtCabecera.setValue(String.valueOf(listpos.get(i).getPosicion()));
					txtCabeceraX.setValue(String.valueOf(listpos.get(i).getLargo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==1 ){
					txtPos1X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos1.setValue(listpos.get(i).getCampo());
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==2 ){
					txtPos2X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos2.setValue(listpos.get(i).getCampo());
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle") &&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==3 ){
					txtPos3X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos3.setValue(listpos.get(i).getCampo());
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==4 ){
					txtPos4X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos4.setValue(listpos.get(i).getCampo());
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==5 ){
					txtPos5X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos5.setValue(listpos.get(i).getCampo());
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera")&& listpos.get(i).getPosicion()==6 ){
					txtPos6X.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPos6.setValue(listpos.get(i).getCampo());
				}
				if(listpos.get(i).getCampo().equals("detalle")){
					txtDetalleX.setValue(String.valueOf(listpos.get(i).getLargo()));
					txtDetalle.setValue(String.valueOf(listpos.get(i).getPosicion()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==7){
					txtDetPos1.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbDetPos1.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==8){
					txtDetPos2.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbDetPos2.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==9){
					txtDetPos3.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbDetPos3.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==10){
					txtDetPos4.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbDetPos4.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==11){
					txtPiePos1.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPiePos1.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==12){
					txtPiePos2.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPiePos2.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==13){
					txtPiePos3.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPiePos3.setValue(String.valueOf(listpos.get(i).getCampo()));
					
				}
				if(!listpos.get(i).getCampo().equals("Pie") && !listpos.get(i).getCampo().equals("detalle")&&
						!listpos.get(i).getCampo().equals("cabecera") && listpos.get(i).getPosicion()==14){
					txtPiePos4.setValue(String.valueOf(listpos.get(i).getLargo()));
					cmbPiePos4.setValue(String.valueOf(listpos.get(i).getCampo()));
				}
				if( listpos.get(i).getCampo().equals("Pie")){
					txtPieX.setValue(String.valueOf(listpos.get(i).getLargo()));
					txtPieY.setValue(String.valueOf(listpos.get(i).getPosicion()));
				}
			}
			
		}
	};
	private final DynamicForm dynamicForm_2 = new DynamicForm();
	private final DynamicForm dynamicForm_3 = new DynamicForm();
}
