package com.giga.factum.client;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.MayorizacionDTO;
import com.giga.factum.client.DTO.TbltipodocumentoDTO;
import com.giga.factum.client.regGrillas.MayorizacionRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.RecordSummaryFunctionType;
import com.smartgwt.client.types.RowEndEditAction;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;

public class frmMayorizacion extends VLayout{
	SearchForm searchForm = new SearchForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Tipo Documento");
	TextItem txtBuscarLst=new TextItem("txtBuscarLst","Buscar No Trans.");
	LinkedList<TbltipodocumentoDTO> listTipoDocumento=new LinkedList<TbltipodocumentoDTO>();
	LinkedHashMap<String, String> MapTipoDocumento = new LinkedHashMap<String, String>();
	//ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	MayorizacionRecords mayorizacionrecord;
	final int groupSmall=1;
	
	//private final ListGrid listGrid = new ListGrid();
	Date fechaFactura;   
    ListGrid listGrid;   
    PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
    int codigo=0;
    String nrt="";
    int tipo=0;
    NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public frmMayorizacion() {
		
		final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
			public void onFailure(Throwable caught) {
				SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
				
			}
			public void onSuccess(Date result) {
				fechaFactura=result;
				txtFechaInicial.setValue(fechaFactura);
				txtFechaFinal.setValue(fechaFactura);
			}
		};		final AsyncCallback<String>  objback=new AsyncCallback<String>(){
			public void onFailure(Throwable caught) {
				SC.say("Error dado:" + caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(String result) {
				SC.say(result);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
			}
		};
		
		getService().fechaServidor(callbackFecha);
		getService().listarTipoDocumento(objbackbodega);
		txtFechaInicial.setValue(fechaFactura);
		txtFechaFinal.setValue(fechaFactura);
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtBuscarLst = new TextItem("txtBuscarLst", "Buscar");
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones("listar"));
		txtBuscarLst.setShowHintInField(true);
		searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
		DynamicForm dynamicForm = new DynamicForm();
		//cmbDocumento.setRequired(true);
		//cmbDocumento.setValueMap("Facturas de Venta","Notas de Entrega");
		//cmbDocumento.setDefaultToFirstOption(true);
		cbmBuscarLst.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				tipo=Integer.valueOf(cbmBuscarLst.getValue().toString());
			}
		});
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal,txtBuscarLst,cbmBuscarLst});
		ListGridField fecha =new ListGridField("fecha", "Fecha",85);
		
		fecha.setAlign(Alignment.CENTER);
		//fecha.setShowGroupSummary(true);  
		//fecha.setShowGridSummary(false);   
		ListGridField codigo = new ListGridField("codigo", "Codigo",70);
		codigo.setAlign(Alignment.CENTER);
		ListGridField idTipoCuenta =new ListGridField("idTipoCuenta", "id TipoCuenta",30);
		idTipoCuenta.setAlign(Alignment.CENTER);
		idTipoCuenta.setHidden(true);
		ListGridField cuenta =new ListGridField("cuenta", "Nombre Cuenta",150);
		cuenta.setAlign(Alignment.CENTER);
		cuenta.setHidden(true);  
		ListGridField numrealtran =new ListGridField("numrealtran", "Num. Transaccion",105);
		numrealtran.setAlign(Alignment.CENTER);
		ListGridField documento =new ListGridField("documento", "Num. Documento",105);
		documento.setAlign(Alignment.CENTER);
		ListGridField descripcion =new ListGridField("descripcion", "Descripcion",280);
		descripcion.setAlign(Alignment.CENTER);
		descripcion.setSummaryFunction(new SummaryFunction() {
			
			@Override
			public Object getSummaryValue(Record[] records, ListGridField field) {
				// TODO Auto-generated method stub
				return "TOTALES";
			}
		});
		ListGridField debe =new ListGridField("debe", "Debe",85);
		debe.setAlign(Alignment.RIGHT);
		debe.setRecordSummaryFunction(RecordSummaryFunctionType.MULTIPLIER);  
//		debe.setSummaryFunction(SummaryFunctionType.SUM);  
		debe.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("debe"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		debe.setShowGridSummary(true);  
		debe.setShowGroupSummary(true);
		debe.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
                if (value == null) return null;  
                try {  
                    NumberFormat formato = NumberFormat.getFormat("#,##0.00");  
                    return "$" + formato.format(((Number) value).doubleValue());  
                } catch (Exception e) {  
                    return value.toString();  
                }  
            }  
        });  
		ListGridField haber =new ListGridField("haber", "Haber",85);
		haber.setAlign(Alignment.RIGHT);
		haber.setRecordSummaryFunction(RecordSummaryFunctionType.MULTIPLIER);  
//		haber.setSummaryFunction(SummaryFunctionType.SUM);  
		haber.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("haber"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		haber.setShowGridSummary(true);  
		haber.setShowGroupSummary(true);
		haber.setCellFormatter(new CellFormatter() {
			
			@Override
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				if(value == null) return null;
				try
				{
				NumberFormat formato = NumberFormat.getFormat("#,##0.00");
				return "$" + formato.format(((Number) value).doubleValue());
				}catch(Exception e){return value.toString();}
			}
		});
		
		ListGridField signo =new ListGridField("signo", "Signo",50);
		signo.setAlign(Alignment.CENTER);
		signo.setHidden(true);
		ListGridField valor =new ListGridField("valor", "Valor",50);
		valor.setAlign(Alignment.CENTER);
		valor.setHidden(true);
		ListGridField nivel =new ListGridField("nivel", "Nivel",50);
		nivel.setAlign(Alignment.CENTER);
		nivel.setHidden(true);
		ListGridField padre =new ListGridField("padre", "Padre",50);
		padre.setAlign(Alignment.CENTER);
		padre.setHidden(true);
		ListGridField periodoAnterior =new ListGridField("periodoAnterior", "Periodo Anterior",95);
		periodoAnterior.setHidden(true);
		periodoAnterior.setAlign(Alignment.CENTER);
		
		addMember(dynamicForm); 
		listGrid = new ListGrid();
		listGrid.setShowGridSummary(true);  
        listGrid.setShowGroupSummary(true); 
        listGrid.setFields(new ListGridField[] {fecha,codigo,idTipoCuenta,cuenta,numrealtran,documento,descripcion,debe,haber,signo,valor,nivel,padre,periodoAnterior });
        listGrid.setSize("67%", "85%");
		listGrid.setGroupByField("cuenta");
		addMember(listGrid);
		HStack hStack = new HStack();
		IButton btnGenerar = new IButton("Generar");
		btnGenerar.addClickHandler(new ManejadorBotones("generar"));
		hStack.addMember(btnGenerar);
		
		IButton btnExportar = new IButton("Exportar Excel");
		btnExportar.addClickHandler(new ManejadorBotones("ExportarExcel"));
		hStack.addMember(btnExportar);
		
		addMember(hStack);

	}
    
	private class ManejadorBotones implements  com.smartgwt.client.widgets.events.ClickHandler, FormItemClickHandler, KeyPressHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		public ManejadorBotones(String s){
			indicador=s;
			
		}
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				nrt=txtBuscarLst.getDisplayValue();
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				if(nrt.equals("")&&tipo==0)
				{
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
				}else if(nrt.equals(""))
				{
					SC.say("Numero de Transaccion necesario");
				}
				else if(tipo==0)
				{
					SC.say("Tipo de Documento necesario");
				}	
				else
				{
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
				}

			}else if(indicador.equalsIgnoreCase("ExportarExcel")){
				CreateExelDTO exel=new CreateExelDTO(listGrid);
				}
			
		}

		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equals("Enter"))
				{
					nrt=txtBuscarLst.getDisplayValue();
					fechaI=txtFechaInicial.getDisplayValue();
					fechaF=txtFechaFinal.getDisplayValue();
					if(event.getKeyName().equals("Enter"))
						{
							if(nrt.equals("")&&tipo==0)
							{
								DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
								getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
							}else if(nrt.equals(""))
							{
								SC.say("Numero de Transaccion necesario");
							}
							else if(tipo==0)
							{
								SC.say("Tipo de Documento necesario");
							}	
							else
							{
								DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
								getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
							}
						}
					
				}
			
		}
		public void onFormItemClick(FormItemIconClickEvent event) {
			nrt=txtBuscarLst.getDisplayValue();
			fechaI=txtFechaInicial.getDisplayValue();
			fechaF=txtFechaFinal.getDisplayValue();
			nrt=txtBuscarLst.getDisplayValue();
			if(nrt.equals("")&&tipo==0)
			{
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
			}else if(nrt.equals(""))
			{
				SC.say("Numero de Transaccion necesario");
			}
			else if(tipo==0)
			{
				SC.say("Tipo de Documento necesario");
			}	
			else
			{
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().listarMayorizacion(fechaI, fechaF,nrt,String.valueOf(tipo), listaCallback);
			}
			
		}
					
	}
	final AsyncCallback<List<MayorizacionDTO>>  listaCallback=new AsyncCallback<List<MayorizacionDTO>>(){

		@Override
		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		@Override
		public void onSuccess(List<MayorizacionDTO> result) {
			
			if (result.size()>0)
			{	
				ListGridRecord[] listado = new ListGridRecord[result.size()];
				for(int i=0;i<result.size();i++) 
				{
					//listado[i]=(new MayorizacionRecords((MayorizacionDTO)result.get(i)));
					mayorizacionrecord=(new MayorizacionRecords((MayorizacionDTO)result.get(i)));
					listGrid.addData(mayorizacionrecord);
				}
				//listGrid.setData(listado);
				listGrid.redraw();
			}
			else {
				SC.say("No se encontraron registros");
			}
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<List<TbltipodocumentoDTO>>  objbackbodega=new AsyncCallback<List<TbltipodocumentoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}
		public void onSuccess(List<TbltipodocumentoDTO> result) {
			MapTipoDocumento.clear();
			MapTipoDocumento.put("","");
            for(int i=0;i<result.size();i++) {
            	listTipoDocumento.add(result.get(i));
            	MapTipoDocumento.put(String.valueOf(result.get(i).getIdTipoDoc()), 
						result.get(i).getTipoDoc());	
			}
            cbmBuscarLst.setValueMap(MapTipoDocumento);
            
       }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
