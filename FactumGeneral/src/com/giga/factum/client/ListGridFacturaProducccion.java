package com.giga.factum.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ListGridFacturaProducccion extends ListGrid{
	

	static ListGridField lstCategoria; 
	static ListGridField lstTotal; 
	
	public ListGridFacturaProducccion(){
		lstCategoria= new ListGridField("lstCategoria", "Categoria");
		lstTotal= new ListGridField("lstTotal", "Total");		
		setFields(lstCategoria, lstTotal);
		//setHeight("188px");		
	}

}
