package com.giga.factum.client;


import com.giga.factum.client.DTO.AreaDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.tab.Tab;

import java.util.List;

public class TabArea extends Tab{
	public TabArea(AreaDTO areadto){
		setTitle(areadto.getNombre());
	}

}

