package com.giga.factum.client;
import java.util.LinkedHashMap;
import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.layout.HStack;  
//import com.giga.factum.client.DTO.CuentaDTO;
//import com.giga.factum.client.frmCuenta.ManejadorBotones;
//import com.giga.factum.client.regGrillas.CuentaRecords;
import com.giga.factum.client.DTO.CoreDTO;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.giga.factum.client.regGrillas.CoreRecords;
import com.giga.factum.client.regGrillas.CuentaRecords;
import com.giga.factum.client.regGrillas.TipoCuentaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.types.Alignment;

public class frmCore extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	TextItem txttipoCore =new TextItem("txttipoCore", "Tipo Core");


	SearchForm searchFormPadre = new SearchForm();
	final TextItem txtCore = new TextItem("txtCore", "Nombre");
	TabSet tabCuenta = new TabSet();//sera el que contenga a todas las pesta�as
	ListGrid lstPadre = new ListGrid();
	ListGrid listCore = new ListGrid();
	public int pad=0;
	public String tipocuenta;
	TextItem txtBuscarLstPadre = new TextItem("txtBuscarLstPadre", " ");
	TextItem txtBuscar = new TextItem("txtBuscar", "");
	ComboBoxItem cmbBuscarPadre =new ComboBoxItem("cmbBuscarPadre", " ");
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	
	IButton btnGrabar = new IButton("Grabar");
	IButton btnModificar = new IButton("Modificar");
	IButton btnEliminar = new IButton("Eliminar");
	IButton btnNuevo = new IButton("Nuevo");
	
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	int tipoCore=0;
	frmCore()
	{
	 	setSize("910px", "600px");
	 	final HLayout hLayoutPadre=new  HLayout();
		Tab lTbCuenta_mant = new Tab("Ingreso");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "45%");
	 	
	 	
	 	txtCore.setShouldSaveValue(true);
	 	txtCore.setRequired(true);
	 	txtCore.setDisabled(false);
	 	TextItem txtCodigo = new TextItem("txtCodigo", "C\u00F3digo");
	 	txtCodigo.setRequired(true);
	 	
	 	txttipoCore.setRequired(true);
	 	txttipoCore.setDisabled(true);
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon searchPickerList = new PickerIcon(PickerIcon.SEARCH);
		
		searchPickerList.addFormItemClickHandler(new FormItemClickHandler(){
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				String nombre=txtBuscar.getDisplayValue().toUpperCase();
				String campo=cmbBuscar.getDisplayValue();
				
				if(campo.isEmpty()){
					getService().listarCore(0,2000, objbacklst);
				}else{
					if(nombre.isEmpty()){
						getService().listarCore(0,2000, objbacklst);
					}else{
						if(campo.equalsIgnoreCase("nombre")){
							campo="nombreCuenta";
						}else if(campo.equalsIgnoreCase("C\u00F3digo")){
							campo="idCuenta";
						}
						getService().listarCoreLike(nombre, campo, objbacklst);
					}
				}
				
			}
		});

		
	 	TextItem txtidCore =new TextItem("txtidCore", "Id Core");
	 	txtidCore.setDisabled(true);
	 	
	 	TextItem txtNivel = new TextItem("txtNivel", "Nivel");
	 	txtNivel.setRequired(true);
	 	txtNivel.setDisabled(true);
	 	txtNivel.setKeyPressFilter("[0-9]");
	 	
	 	TextItem txtPadre =new TextItem("txtPadre", "Padre");
	 	txtPadre.setValue(pad);
	 	txtPadre.setDisabled(true);
	 	
	 
	
	 	final CheckboxItem chkpadre = new CheckboxItem("chkhijo", "Hijo");
	 	
	 	dynamicForm.setFields(new FormItem[] {txtidCore, txtCodigo, txtCore, txtNivel, txtPadre, txttipoCore,chkpadre  });
	 	txtNivel.setValue("0");
	 	layout.addMember(dynamicForm);
	 	
	 	
	 	hLayoutPadre.setVisible(false);
	 	hLayoutPadre.setSize("100%", "5%");
	 	searchFormPadre.setSize("85%", "100%");
		searchFormPadre.setItemLayout(FormLayoutType.ABSOLUTE);
		
		
		
		
		txtBuscarLstPadre.setLeft(6);
		txtBuscarLstPadre.setTop(6);
		txtBuscarLstPadre.setWidth(146);
		txtBuscarLstPadre.setHeight(22);
		txtBuscarLstPadre.setIcons(searchPicker);
		txtBuscarLstPadre.setHint("Buscar");
		txtBuscarLstPadre.setShowHintInField(true);
		searchPicker.addFormItemClickHandler(new FormItemClickHandler(){

			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				
				String nombre=txtBuscarLstPadre.getDisplayValue().toUpperCase();
				String campo=cmbBuscarPadre.getDisplayValue();
				SC.say(nombre+ " "+campo);
				if(campo.equalsIgnoreCase("nombre")){
					campo="nombreCuenta";
				}else if(campo.equalsIgnoreCase("C\u00F3digo")){
					campo="idCuenta";
				}else if(campo.equalsIgnoreCase("")){
					campo="nombreCuenta";
				}
				getService().listarCoreLike(nombre, campo, objbacklst);
				
			}
			
		});
		txtBuscarLstPadre.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					buscarL();
					
				}
				
			}
			
		});
		
		cmbBuscarPadre.setLeft(158);
		cmbBuscarPadre.setTop(6);
		cmbBuscarPadre.setWidth(146);
		cmbBuscarPadre.setHeight(22);
		cmbBuscarPadre.setHint("Buscar Por");
		cmbBuscarPadre.setShowHintInField(true);
		cmbBuscarPadre.setValueMap("C\u00F3digo","Nombre");
		
		searchFormPadre.setFields(new FormItem[] { txtBuscarLstPadre, cmbBuscarPadre});
		hLayoutPadre.addMember(searchFormPadre);
		HStack hStackPadre = new HStack();
		hStackPadre.setSize("12%", "100%");
		TransferImgButton btnSiguienteP = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnteriorP = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicioP = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFinP = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        hStackPadre.addMember(btnInicioP);
        hStackPadre.addMember(btnAnteriorP);
        hStackPadre.addMember(btnSiguienteP);
        hStackPadre.addMember(btnFinP);
        hLayoutPadre.addMember(hStackPadre);
		layout.addMember(hLayoutPadre);
		layout.addMember(lstPadre);
		lstPadre.setSize("100%", "30%");
		
		HLayout hLayout_1 = new HLayout();
		hLayout_1.setSize("100%", "10%");
		
		//hLayout_1.addMember(searchFormPadre);
		
		hLayout_1.addMember(btnGrabar);
		hLayout_1.addMember(btnModificar);
		hLayout_1.addMember(btnEliminar);
		hLayout_1.addMember(btnNuevo);
		
		layout.addMember(hLayout_1);
		lstPadre.setVisible(false);
		lstPadre.addRecordClickHandler(new ManejadorBotones("Hijo"));
		lstPadre.addRecordDoubleClickHandler(new ManejadorBotones("Hijo"));
	 	lTbCuenta_mant.setPane(layout);
		
		VLayout layoutPadre = new VLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPickerPadre = new PickerIcon(PickerIcon.SEARCH);
		
		
		ListGridField idCore= new ListGridField("idCore", "Id Core");
		ListGridField nombreCore= new ListGridField("nombreCore", "Nombre");
	 	ListGridField tipoCore= new ListGridField("tipoCore", "C\u00F3digo Tipo Core");
	 	ListGridField padreCore= new ListGridField("padreCore", "Padre");
	 	ListGridField nivelCore= new ListGridField("nivelCore", "Nivel");
	 	ListGridField codigoCore= new ListGridField("codigoCore", "C\u00F3digo");
	 	
	 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	 	
	 	getService().listarCore(0,2000, objbacklst);

		
		//lstCuenta.setFields(new ListGridField[] {idCuenta,codigo,nombreCuenta,idTipocuenta,Padre,nivel});asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
	 	lstPadre.setFields(new ListGridField[] { idCore,codigoCore,nombreCore, tipoCore,padreCore,nivelCore});
        
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        /*para poner en un hstack los botones de desplazamiento*/
		chkpadre.addChangeHandler(new ChangeHandler() {
	 		public void onChange(ChangeEvent event) {
	 			lstPadre.setVisible(!chkpadre.getValueAsBoolean());
	 			hLayoutPadre.setVisible(!chkpadre.getValueAsBoolean());
	 			
	 		}
	 	});
        
        
	    tabCuenta.addTab(lTbCuenta_mant);
	    
	    Tab tabListado = new Tab("Listado");
	    
	    VLayout vLayoutListado = new VLayout();
	    vLayoutListado.setSize("100%", "100%");
	    
	    HLayout hLayout_2 = new HLayout();
	    hLayout_2.setSize("100%", "5%");
	    
	    SearchForm searchForm = new SearchForm();
	    searchForm.setSize("50%", "100%");
	    searchForm.setMinColWidth(20);
	    searchForm.setNumCols(4);
	    txtBuscar.setLeft(6);
	    txtBuscar.setTop(6);
	    txtBuscar.setWidth(146);
	    txtBuscar.setHeight(22);
	    txtBuscar.setIcons(searchPickerList);
	    txtBuscar.setHint("Buscar");
	    txtBuscar.setShowHintInField(true);
	    txtBuscar.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					String nombre=txtBuscar.getDisplayValue().toUpperCase();
					String campo=cmbBuscar.getDisplayValue();
					if(campo.equalsIgnoreCase("nombre")){
						campo="nombreCuenta";
					}else if(campo.equalsIgnoreCase("C\u00F3digo")){
						campo="idCuenta";
					}else if(campo.equalsIgnoreCase(" ")){
						campo="nombreCuenta";
					}
					
				}
				
			}
			
		});
	    
	    cmbBuscar.setLeft(158);
	    cmbBuscar.setTop(6);
	    cmbBuscar.setWidth(146);
	    cmbBuscar.setHeight(22);
	    cmbBuscar.setHint("Buscar Por");
	    cmbBuscar.setShowHintInField(true);
	    cmbBuscar.setValueMap("C\u00F3digo","Nombre");
	    
	    searchForm.setFields(new FormItem[] { txtBuscar, cmbBuscar});
	    hLayout_2.addMember(searchForm);
	    vLayoutListado.addMember(hLayout_2);
	    listCore.addRecordDoubleClickHandler(new ManejadorBotones("Buscar"));
	    listCore.setFields(new ListGridField[] {idCore,codigoCore,nombreCore, tipoCore,padreCore,nivelCore});
	    vLayoutListado.addMember(listCore);
	    tabListado.setPane(vLayoutListado);
	    tabCuenta.addTab(tabListado);
	    addMember(tabCuenta);
	    btnGrabar.addClickHandler(new ManejadorBotones("grabar"));
	    btnNuevo.addClickHandler(new ManejadorBotones("nuevo"));
	    btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
	    btnModificar.addClickHandler(new ManejadorBotones("modificar"));
       
	}
	public void limpiar(){
		dynamicForm.setValue("cmbtipocuenta"," ");  
		dynamicForm.setValue("txtidCuenta", "");
		dynamicForm.setValue("txtCuenta", " ");
		dynamicForm.setValue("txtNivel", "0");
		dynamicForm.setValue("txtPadre","");
		dynamicForm.setValue("txtCodigo"," ");
		
	}
	public void buscarL(){
		String nombre=txtBuscarLstPadre.getDisplayValue().toUpperCase();
		String campo=cmbBuscarPadre.getDisplayValue();
		if(campo.equalsIgnoreCase("nombre")){
			campo="nombreCuenta";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idCuenta";
		}else if(campo.equalsIgnoreCase(" ")){
			campo="nombreCuenta";
		}
		//lblRegisros.setText(contador+" de "+registros);
		getService().listarCoreLike(nombre, campo, objbacklst);
	}
	/**
	 * Manejador de Botones para pantalla Cuenta
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("nuevo")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
					String punto=codigo.substring(codigo.length()-1);
					if(!punto.equals(".")){
						try{
							String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
							String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
							String padre=dynamicForm.getItem("txtPadre").getDisplayValue();
							
							CoreDTO Coredto=new CoreDTO();
							
							Coredto.setNivelCore(Integer.parseInt(nivel));
							Coredto.setNombreCore(nombre);
							Coredto.setPadreCore(pad);
							Coredto.setTipoCore(tipoCore);
							Coredto.setCodigoCore(codigo);
							
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().grabarCore(Coredto,objback);
							contador=20;
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("Ingrese un C\u00F3digo correcto");
					}
				}
			}else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
					String punto=codigo.substring(codigo.length()-1);
					if(!punto.equals(".")){
						try{
							String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
							String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
							String padre=dynamicForm.getItem("txtPadre").getDisplayValue();
							String idTipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getValue();
							String TipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getDisplayValue();
							String idPlan=(String) dynamicForm.getItem("cmbPlancuenta").getDisplayValue();
							String idcuenta=dynamicForm.getItem("txtidCuenta").getDisplayValue();
							CoreDTO Coredto=new CoreDTO();
							/*
							cuenta.setIdCuenta(Integer.parseInt(idcuenta));
							cuenta.setNivel(Integer.parseInt(nivel));
							cuenta.setNombreCuenta(nombre);
							cuenta.setPadre((pad));
							cuenta.setTbltipocuenta(new TipoCuentaDTO(Integer.parseInt(idTipoCuenta),TipoCuenta));
							cuenta.setCodigo(codigo);
							*/
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().modificarCore(Coredto,objback);
							contador=20;
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("Ingrese un C\u00F3digo correcto");
					}
				}
			}else if(indicador.equalsIgnoreCase("eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	try{
                        		String idcuenta=dynamicForm.getItem("txtidCuenta").getDisplayValue();
                        		String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
            					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
            					String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
            					String padre=dynamicForm.getItem("txtPadre").getDisplayValue();
            					String idTipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getValue();
            					String TipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getDisplayValue();
            					getService().buscarCore(idcuenta, objbackCorepad);
            					TipoCuentaDTO tipocuenta =new TipoCuentaDTO(Integer.parseInt(idTipoCuenta),TipoCuenta);
            					CoreDTO Coredto=new CoreDTO(); 
            					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
            					getService().eliminarCore(Coredto, objback);
            					limpiar();
                				//lstCuenta.removeData(lstCuenta.getSelectedRecord());
                        	}catch(Exception e){
                        		SC.say(e.getMessage());
                        	}
                       } else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
				
			}else if(indicador.equalsIgnoreCase("Buscar")){
				String idCuenta=dynamicForm.getItem("txtCuenta").getDisplayValue();
				getService().buscarCoreNombre(idCuenta,"nombreCuenta", objbackCore);
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCore(contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCore(contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCore(contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCore(registros-registros%20,registros, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCore(contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
			SC.say("Listar pikcer");
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
				SC.say("Listar textbox");
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			if(indicador.equalsIgnoreCase("hijo")){
				try{
					String codigo=lstPadre.getSelectedRecord().getAttribute("codigo")+".";
					int nivel=Integer.parseInt(lstPadre.getSelectedRecord().getAttribute("nivel"));
					nivel++;
					dynamicForm.setValue("txtCodigo",codigo);
					dynamicForm.setValue("txtNivel",String.valueOf(nivel)); 
					dynamicForm.setValue("txtPadre", lstPadre.getSelectedRecord().getAttribute("nombreCuenta"));
					pad=Integer.parseInt(lstPadre.getSelectedRecord().getAttribute("idCuenta"));
					
				}catch (Exception  e){
					SC.say(e.getMessage());
				}
				
			}
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Buscar")){
				pad=Integer.parseInt(listCore.getSelectedRecord().getAttribute("idCuenta"));
				dynamicForm.setValue("cmbtipocuenta",listCore.getSelectedRecord().getAttribute("idTipocuenta"));  
				dynamicForm.setValue("txtidCuenta", listCore.getSelectedRecord().getAttribute("idCuenta"));
				dynamicForm.setValue("txtCuenta", listCore.getSelectedRecord().getAttribute("nombreCuenta"));
				dynamicForm.setValue("txtNivel", listCore.getSelectedRecord().getAttribute("nivel"));
				dynamicForm.setValue("txtPadre", listCore.getSelectedRecord().getAttribute("nombreCuenta"));
				dynamicForm.setValue("txtCodigo", listCore.getSelectedRecord().getAttribute("codigo"));
				tabCuenta.selectTab(0);
				
			}
			
		}

		
			
	}
	final AsyncCallback<List<PlanCuentaDTO>>  objbacklstPlanCuenta=new AsyncCallback<List<PlanCuentaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			
		}
		@Override
		public void onSuccess(List<PlanCuentaDTO> result) {
			/*MapPCuenta.clear();
			MapPCuenta.put("","");
            for(int i=0;i<result.size();i++) {
				MapPCuenta.put(String.valueOf(result.get(i).getIdPlan(), 
						result.get(i).get);
			}
            cmdBod.setValueMap(MapBodega)
			
			*/
			if(result.size()!=0){
				String[] lista =new String[result.size()];
				for(int i=0;i<result.size();i++) {
	            	lista[i]=String.valueOf(result.get(i).getIdPlan());
					//cmbPlancuenta.setValue(String.valueOf(new PlanCuentaRecords((PlanCuentaDTO)result.get(i)).getidPlanCuenta()));
				}
				btnGrabar.setDisabled(false);
			}else{
				btnGrabar.setDisabled(true);
				SC.say("Primero Ingrese un Plan de Cuentas");
				
				
			}
			
		}
		
		
	};
	
		
	final AsyncCallback<List<CoreDTO>>  objbacklst=new AsyncCallback<List<CoreDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<CoreDTO> result) {
			getService().numeroRegistrosCore("TblCore",objbackI);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new CoreRecords((CoreDTO)result.get(i)));
			}
			
			lstPadre.setData(listado);
			lstPadre.redraw();
			listCore.setData(listado);
			listCore.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
       }
		
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			
			SC.say("No se conecta con la base "+caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			//getService().listarCuenta(0,20, objbacklst);
			SC.say(result);
			contador=20;
			pad=0;
			getService().listarCore(0,2000, objbacklst);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		 	
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;
			
		}
	};
	final AsyncCallback<CoreDTO>  objbackCore=new AsyncCallback<CoreDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(CoreDTO result) {
			if(result!=null){
				/*
				dynamicForm.setValue("cmbtipocuenta",result.getTbltipocuenta().getNombre());  
				dynamicForm.setValue("txtidCuenta", result.getIdCuenta());
				dynamicForm.setValue("txtCuenta",result.getNombreCuenta());
				dynamicForm.setValue("txtNivel", result.getNivel());
				dynamicForm.setValue("txtPadre", result.getNombreCuenta());
				dynamicForm.setValue("txtCodigo", result.getcodigo());
				pad=result.getPadre();
				*/
				tabCuenta.selectTab(0);
				ban=true;
				SC.say("Elemento  encontrado");
				
				
				
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
				pad=0;
			}
		}
	};
	final AsyncCallback<CoreDTO>  objbackCorepad=new AsyncCallback<CoreDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(CoreDTO result) {
			if(result!=null){
				//pad=result.getPadre();
				//tipocuenta=result.getTbltipocuenta().getNombre();
				ban=true;
				SC.say("Elemento  encontrado");
				
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
				pad=0;
			}
		}
	};
	
	
}
