package com.giga.factum.client;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.BodegaProdDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.DocumentoSaveDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.LetrasANumeros;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.OrdenDTO;
import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PosicionDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.BodegaRecords;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.client.regGrillas.MesaRecords;
import com.giga.factum.client.regGrillas.ProductoRecords;
import com.giga.factum.client.regGrillas.TipoPrecioRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

public class Factura extends VLayout {// GIGACOMPUTERS
	// Objeto que me va a dar acceso a la clase Widgets para poder crear todos
	// los widgets
	int auxgrilla ;
	double peso=0.0;
	Cuerpo cp ;
	Factura fact = this;
	WidgetsForm widgets = new WidgetsForm();
	frmListClientes frmlisCli;// =new frmListClientes();
	frmListVendedor listVendedor = new frmListVendedor();
	User usuario = new User();
	DtocomercialDTO DocBase = new DtocomercialDTO();
	String serie = "";
	String tipoS = "";
	String emision = "";
	int lastNumRealRet = 0;
	
	public String impreso="";
	ProductoDTO productoBarras = new ProductoDTO();
	BodegaDTO mibodega = new BodegaDTO();
	BodegaDTO boegaaux = new BodegaDTO();
	GenerarClaveAcceso generarClaveAcceso = new GenerarClaveAcceso();
	/*
	 * OBJETOS DE LA RETENCION
	 */
	Double ivaNota = 0.0;
	BodegaDTO BodegaGRABAR = null;
	String indicadorDetalles = "";
	Boolean CalculoIVA = false;
	DynamicForm frmCabecera = new DynamicForm();
//	VLayout formRetencion = new VLayout();
	VLayout formRetencion = null;
	LinkedList<ImpuestoDTO> Impuestos = new LinkedList<ImpuestoDTO>();
	LinkedList<RetencionDTO> det = new LinkedList<RetencionDTO>();
	LinkedHashMap<String, String> MapImpuesto = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapEmpleados = new LinkedHashMap<String, String>();
	ListGridField impuesto = new ListGridField("lstImpuesto", "Impuesto");
	ListGrid listGridRet = new ListGrid();
	FloatItem txtTotalRet = new FloatItem();
	
	ListGridField baseImponible = new ListGridField("baseImponible",
			"Base Imponible");
	ListGridField lstBaseImponible = new ListGridField("lstBaseImponible",
			"Base Valor");
	ListGridField lstPorcentaje = new ListGridField("lstPorcentaje",
			"% Retenci\u00F3n");
	String idImpuesto = "";
	Window winClientes = new Window();
	TextItem txtDireccion2 = new TextItem("txtDireccion", "Direcci\u00F3n");// para
																			// la
																			// retencion
	TextItem txtNumeroC = new TextItem("txtNumeroC", "# Documento");
	TextItem txtPersonaAsignada = new TextItem("txtPersonaAsignada", "Cliente");
	TextItem txtPuntosAsignados = new TextItem("txtPuntosAsignados", "# Puntos");
	
	StaticTextItem txtTotalXML = new StaticTextItem("txtTotalXML", "Total XML");
	
	HTMLFlow textBoxTotal = new HTMLFlow("0.0");//BORRAR
	Label totalLabel = new Label("Total: ");
	String personaAsignada = "";
	private DynamicForm dfPuntos = new DynamicForm();
	private Button btnQuitarPuntos = new Button("QUITAR PUNTOS");
	/********************************************************************************/
	// ESTE ITERADOR VA A CONTENER EL LISTADO DE CLIENTES PARA PODER LUEGO
	// EXTRAER TODOS LOS DATOS DEL MISMO
	// UNA VEZ QUE SE SELECCIONE LA PERSONA SE CARGARAN LOS DATOS A LA FACTURA
	ListGridRecord cargaProducto;
	Iterator iter, iter2, itertp, itertp2;
	Set<DtoComDetalleDTO> DtoDetalles = null;
	List<JSONObject> detallesJson = null;
	Set<RetencionDTO> tblRetenciones = new HashSet<RetencionDTO>(0);
	DtocomercialDTO docAuxiliar;
	final Window winBodegas = new Window();
	Set<TblpagoDTO> PagosDTO = null;
	Set<DtocomercialTbltipopagoDTO> PagosAsociados = new HashSet<DtocomercialTbltipopagoDTO>(
			0);

	PersonaDTO persona = new PersonaDTO();// Cliente seleccionado
	Integer codigoSeleccionado = 1;// 2971 para Giga ++++2925 para la
										// ferreteria;
	Integer codigoSeleccionadoPuntos = 0;// Para saber si escogio una persona
											// para asignarle puntos
	Integer puntosAcreditados = 0;// Para saber cuantos puntos se le acredito
	Integer Anticipo = null;
	Integer Retencion = null;
	Integer NotaCredito = null;
	Integer NumPagos = 0;
	Integer afeccion = -1;
	Integer idPago = -1;
	Integer anulacion = 0; // Tiene tres estados, 0 = no es anulacion, -10 =
							// anulacion sin devolucion de productos
							// -20 = anulacion con devolucion de productos rio
	Date fechaFactura;// = new Date();
	Double cantidadAsignar = 0.0;
	Canvas canvas = new Canvas();
	Double descuentoGlobal = 0.0;
	Double impuestoadicionGlobal = 0.0;
	Double subtotalIva14 = 0.0;
	Double subtotalIva0 = 0.0;
	Double valoresIva = 0.0;
	Double total = 0.0;
	Double totalContado = 0.0;
	Double totalCredito = 0.0;
	Double totalNota = 0.0;
	Double totalAnt = 0.0;
	Double totalRet = 0.0;
	Integer tipoUsuario = 0;
	BodegaDTO Bodega = null;
	Integer idDto = null;
	ListGrid listGrid = new ListGrid();
	Integer IdProductoGlobal = 0;
	Integer rowNumGlobal = 0;
	Integer idVendedor = 0;
	//String nombreVendedor= "";
	Double costo = 0.0;
	Double Subtotal = 0.0;
	Integer ventaCompra = 0; // 0 Significa Venta
	TextAreaItem txtaConcepto = new TextAreaItem();
	TextItem txtAutorizacion_sri = new TextItem("autorizacion_s",
			"AUTORIZACI\u00D3N");
	TextItem txtNumero_factura = new TextItem("numero_factura", "N.");
	DateItem dateFecha_emision = new DateItem("fecha_emision",
			"FECHA&nbspEMISI\u00D3N");
	// TextItem mibod=new TextItem("mibod","mibod");

	Window winFactura1 = new Window();
	PersonaDTO vendedor;
	ListGridRecord registroPago;
	char confirmar = '0';
	Boolean newRet = true;
	frmReporteCaja listaDocumentos = new frmReporteCaja("Facturas de Venta");
	final TextItem txtCLiente = new TextItem("cliente", "CLIENTE");
	//final TextItem txtVendedor = new TextItem("vendedor", "VENDEDOR");
	final TextItem txtRuc_cliente = new TextItem("ruc_cliente", "R.U.C");
	final TextItem txtDireccion = new TextItem("direccion", "DIRECCI\u00D3N");
	final TextItem txtCorreo = new TextItem("correo", "CORREO");
	final TextItem txtCiudad = new TextItem("ciudad", "CIUDAD");
	final TextItem txtTelefono = new TextItem("telefono", "TELEFONO");
	final ComboBoxItem cmbVendedor=new ComboBoxItem("vendedor","VENDEDOR");
	//final CheckboxItem chkPedido=new CheckboxItem("pedido","PEDIDO");
	//final CheckboxItem chkAfectarIVA=new CheckboxItem("afectariva","Considerar IVA");
	// final ComboBoxItem cmbVendedor=new ComboBoxItem("vendedor","VENDEDOR");
	final TextItem txtCorresp = new TextItem("corresponde",
			"CORRESP. A LA FACTURA N");
	final TextItem txtMotivo = new TextItem("motivo",
			"MOTIVO");
	final TextItem txtSubtotal = new TextItem("subtotal", "SUBTOTAL");
	final TextItem txtNumCompra = new TextItem("txtNumCompra",
			"#Factura de Compra");
	final SelectItem cmbTipoPrecio = new SelectItem("cmbTipoPrecio",
			"TIPO&nbspDE&nbspPRECIO");
	/*final SelectItem cmbTipoBodega = new SelectItem("cmbTipoBodega",
			"TIPO&nbspDE&nbspBODEGA");*/
	List<TipoPrecioDTO> listaTipoPrecio = null;
	List<BodegaDTO> listaTipoBodega = null;

	final TextItem txtDescuento = new TextItem("descuento", "DESC. PROD.");
	final TextItem txtimpuestoAdicional = new TextItem("impuestoAdiciona", Factum.banderaNombreimpuestoAdicional);
	final TextItem txtDescuentoNeto = new TextItem("descuentoNeto",
			"DESC. NETO");
	final TextItem txtBase_imponible = new TextItem("base_imponible",
			"BASE&nbspIMPONIBLE");
	final TextItem txt0_iva = new TextItem("0_iva",
			"SUBTOTAL&nbsp0%&nbspI.V.A.");
	final TextItem txt14_iva = new TextItem("14_iva",
			"SUBTOTAL&nbsp"+Factum.banderaIVA+"%&nbspI.V.A.");
	final TextItem txtValor14_iva = new TextItem("Valor14_iva",
			"I.V.A.&nbsp"+Factum.banderaIVA+"%");
	final TextItem txtTotal = new TextItem("total", "TOTAL");
	final TextItem txtEfectivo = new TextItem("efectivo", "$ RECIB.");
	final TextItem txtCambio = new TextItem("cambio", "CAMBIO");
	final TextItem txtObservacion = new TextItem("observacion", "Observaci\u00F4n");
	
	private final HLayout hLayout = new HLayout();
	private final DynamicForm dfEncabezado = new DynamicForm();
	private final DynamicForm dfEncabezado2 = new DynamicForm();
	private final DynamicForm dfExtras = new DynamicForm();
	private ListGrid grdProductos, grdPagos;
	// private ListGrid grdPagos;
	// ListGrid grdProductos = new ListGrid();
	private VLayout vLayout_pie = new VLayout();
	private HLayout hLayout_2 = new HLayout();
	private HLayout hLayout_observa = new HLayout();
	private final DynamicForm dfPie_1 = new DynamicForm();
	private final VLayout layout_2 = new VLayout();
	private final HLayout hlayout_totales = new HLayout();
	
	private final HLayout hlayout_total = new HLayout();
	private final VLayout vlayout_total = new VLayout();
	
	private final DynamicForm dfPie_2 = new DynamicForm();
	private final DynamicForm dfAntesPie = new DynamicForm();
	private final Label lblTotal_letras = new Label("Son:");
	private final DynamicForm dfPie_totales = new DynamicForm();
	DynamicForm dynamicForm = new DynamicForm();
	String tipoP = "";
	final ListGridField lgfstockPadre = new ListGridField("stockPadre",
			"stockPadre", 1);
	final ListGridField lgfIDProducto = new ListGridField("idProducto", "I", 1);
	final ListGridField lgfCantidad = new ListGridField("cantidad", "Cantidad",
			60);// se declaran los campos de la grilla
	final ListGridField lgfCodigo = new ListGridField("codigoBarras",
			"Cod. Barras", 150);
	final ListGridField lgfDescripcion = new ListGridField("descripcion",
			"Descripci\u00f3n", 650);
	final ListGridField lgfPrecio_unitario = new ListGridField(
			"precioUnitario", "Precio", 60);
	final ListGridField lgfValor_total = new ListGridField("valorTotal", 
			"Subtotal", 100);
	final ListGridField lgfIva = new ListGridField("iva", "Iva", 30);
	final ListGridField lgfStock = new ListGridField("stock", "Stock", 60);
	final ListGridField lgfStockPadre = new ListGridField("stockPadre",
			"Stock M.P.", 60);
	final ListGridField lgfPrecio_iva = new ListGridField("precioConIva",
			"P.IVA", 60);
	
	final ListGridField lgfDEscuento = new ListGridField("descuento",
			"Descuento", 60);
	final ListGridField lgfBodega = new ListGridField("idbodega", "Bodega",150);
	final ListGridField lgfBodegaN = new ListGridField("bodega", "Bodega", 1);
	final ListGridField lgfBodegaU = new ListGridField("ubicacion","Direccion", 1);
	final ListGridField lgfBodegaT = new ListGridField("telefono", "Cant.", 1);
	final ListGridField lgfCosto = new ListGridField("costo", "C", 1);
	final ListGridField lgfEliminar = new ListGridField("eliminar", " ", 25);
	// Columnas para la grilla de pagos, la cual tendra dos columnas: cantidad y
	// fecha de vencimiento
	final ListGridField lgfCantidadPago = new ListGridField("cantidadPago",
			"Cantidad", 80);
	final ListGridField lgfVencimiento = new ListGridField("vencimiento",
			"Vencimiento", 150);
	final ListGridField lgfFechaRealPago = new ListGridField("fecharealpago",
			"FechaReal", 150);
	final ListGridField lgfEstado = new ListGridField("estado", "Estado", 150);
	final ListGridField lgfId = new ListGridField("id", "id", 150);
	final ListGridField lgfNumEgreso = new ListGridField("numegreso", "Egreso", 50);
	final ListGridField lgfConcepto = new ListGridField("concepto",
			"Concepto", 400);
	final ListGridField lgfFormaPago = new ListGridField("formapago",
			"FormaPago", 200);
	
	final ListGridField lgfNumero = new ListGridField("numero", "#", 20);

	/*
	 * ITEMS PARA CARGAR EL DOCUMENTO
	 */

	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	TransferImgButton btnSiguiente = new TransferImgButton(
			TransferImgButton.RIGHT);
	TransferImgButton btnAnterior = new TransferImgButton(
			TransferImgButton.LEFT);
	TransferImgButton btnInicio = new TransferImgButton(
			TransferImgButton.LEFT_ALL);
	TransferImgButton btnFin = new TransferImgButton(
			TransferImgButton.RIGHT_ALL);
	TransferImgButton btnEliminarlst = new TransferImgButton(
			TransferImgButton.DELETE);
	SearchForm searchFormProducto = new SearchForm();
	SearchForm searchFormPedido = new SearchForm();
	ListGrid lstProductos = new ListGrid();
	ListGrid lstProductosElaborados = new ListGrid();
	//TILEGRIS MESAS OCUPADAS
	TabsetAreas areas;
	TabArea area;
	TileGridMesas mesas;
	Integer idMesa=-1;
	ArrayList<PedidoProductoelaboradoDTO> listpedidoproductoelaboradodto;
	//TILEGRIS MESAS OCUPADAS
	int contador = 20;
	int registros = 0;
	String auth = "";
	Label lblRegistrosFactura = new Label("# Registros");
	HStack hStack = new HStack();
	HLayout hLayoutPr = new HLayout();
	final ComboBoxItem cmbCategoria = new ComboBoxItem("cbmCategoria",
			"Categor\u00EDa");
	final ComboBoxItem cmbUnidad = new ComboBoxItem("cbmUnidad", "Unidad");
	final ComboBoxItem cmbMarca = new ComboBoxItem("cbmMarca", "Marca");
	final ComboBoxItem cmdBod = new ComboBoxItem("cmbBod", "Bodega");
	final CheckboxItem chkServicio=new CheckboxItem("servicio","Servicio");
	final CheckboxItem chkProductoElaborado=new CheckboxItem("productoelaborado","ProductoElaborado");
	ComboBoxItem cmbBuscarProducto = new ComboBoxItem("cmbBuscar", "");
	ComboBoxItem cmbBuscarPedido = new ComboBoxItem("cmbBuscar", "");
	List<CategoriaDTO> listcat = null;
	List<MarcaDTO> listmar = null;
	List<UnidadDTO> listuni = null;
	List<BodegaDTO> listBodega = null;
	List<TipoPrecioDTO> listTipoP = null;
	LinkedHashMap<String, String> MapTipoPrecio = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapTipoBodega = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	int serialParaOrdenProds = 0;
	/************************************************************************************************/

	// ****************** VARIABLES PARA AGREGAR STOCK EN LAS BODEGAS EN CASO DE
	// UNA COMPRA ****************
	/*
	 * ProductoBodegaDTO[] proBod = new ProductoBodegaDTO[countB]; int countB;
	 * ProductoTipoPrecioDTO[] proTip = new ProductoTipoPrecioDTO[count]; int
	 * count;
	 */
	ListGridRecord[] listadoBod = null;
	ListGrid listGridBod = new ListGrid();
	ListGrid lstTipoPrecio = new ListGrid();
	ProductoDTO producto = new ProductoDTO();
	List<ProductoBodegaDTO> proBod = null;
	List<ProductoTipoPrecioDTO> proTip = null;
	// Window ventanaProductos = new Window();
	// DataSource dataSource;
	// ListGrid lstProductos = new ListGrid();

	VentanaEmergente listadoProd;
	VentanaEmergente listadoMes;
	VentanaEmergente listadoProdMes;
	DataSource dataSource;
	lstProductos VentanalstProductos;
	Window winDocumento = new Window();
	Window winXML=new Window();
	ListGrid lstDocumentos = new ListGrid();
	DynamicForm dynamicForm2 = new DynamicForm();
	DynamicForm dynamicForm3 = new DynamicForm();
	DynamicForm dfDescripcion = new DynamicForm();


	private final VLayout hLayout_3 = new VLayout();
	private final DynamicForm dfNotaCredito = new DynamicForm();
	private final VLayout layout = new VLayout();
	private final HLayout hLayout_4 = new HLayout();
	private final HLayout hLayout_5 = new HLayout();
	private final HLayout hLayout_6 = new HLayout();
	
	private final Button btnXML = new Button("CARGAR XML");
	
	//private FileItem fileXML=new FileItem("CARGAR XML");
	//private IButton buttonXML= new IButton("Subir");
	//private DynamicForm dynXML=new DynamicForm();
	
	private final Button btnNuevo = new Button("NUEVO");
	private final Button btnImprimir = new Button("IMPRIMIR");
	private final Button btnGrabar = new Button("GRABAR");
	private final Button btnEliminar = new Button("BORRAR");
	private final Button btnDocumentoElectronico= new Button("ENVIAR DOCUMENTO ELECTRONICO");
	private final Button btnTransformar = new Button("TRANSFORMAR A FACTURA");
	private final Button btnacept = new Button("Aceptar");
	
	// private final Button btnValidar = new Button("VALIDAR CEDULA");
	// private final Button btnAsignarPuntos = new Button("ASIGNAR PUNTOS");
	private final Button btnNuevaFactura = new Button("NUEVA FACTURA");
	private final Button btnExportar = new Button("EXPORTAR");
	private final Button btnImportar = new Button("IMPORTAR");

	
	//VLayout lFormasPago;
	
	VLayout layout_1 = new VLayout();
	VLayout layout_Bod = new VLayout();

	Integer NumDto = 0;
	Integer DocFis = 0;

	Integer posCliente = 0, largoCliente = 0, posDireccion = 0,
			largoDireccion = 0, posRUC = 0, largoRUC = 0, posTelefono = 0,
			largoTelefono = 0, posFecha = 0, largoFecha = 0;
	Integer posVendedor = 0, largoVendedor = 0, cabX = 0, cabY = 0, detX = 0,
			detY = 0, pieY = 0, pieX = 0;
	private String pos1 = "";
	private String pos2 = "";
	private String pos3 = "";
	private String pos4 = "";
	private String pos5 = "";
	private String pos6 = "";
	private String pos11 = "";
	private String pos12 = "";
	private String pos13 = "";
	private String pos14 = "";
	private String pos7 = "";
	private String pos8 = "";
	private String pos9 = "";
	private String pos10 = "";
	
	private int convertir = 0 ; 
	
	Double precioSugeridoServicioTecnico=1D;
	
	String FormasPago="";
	private final Label lblPie_pagina = new Label();
	String responsable="";
	//
	DtocomercialDTO documentoenviar=new DtocomercialDTO();//XAVIER ZEAS - DOCUMENTO ELECTRONICO
	int contadorModificarProductoElaborado=0; 
	ListGridField lstPrecio;
	
	String planCuentaID;
	// CONSTRUCTOR GEMELO PARA ORDENES DE TALLER
	//ProductoDTO productoActual;
	Integer TipoDocumento=0;
	Integer Documento=0;
	//NumberFormat formatoDecimal = NumberFormat.getFormat("'$'#,###0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	LinkedHashMap<Integer,ProductoDTO> linkedhashmapproductos;
	LinkedHashMap<Integer,ProductoDTO> linkedhashmapproductosAct;
	
	Boolean pagoActivo = false;
	Boolean dividir=false;
	Integer lastIndiceDiv=0;

	TextItem txtcontado;
	CheckboxItem  chkcontado;
	
	frmSubirArchivo frmsubirarchivo;
	
	public Factura(Integer Tipodoc, Integer doc,OrdenDTO orden,ClienteDTO cliente) {
		this.TipoDocumento=Tipodoc;
		this.Documento=doc;
		obtener_sesion();
		//precargarDatosOrdenTaller(orden,cliente);
		
		/*obtener_sesion();
		setSize("50%", "50%");
		// SC.say("la fecha es: "+" "+fechaFactura+" mas no ma�ana estoy cansado");
		auxgrilla = 0;
		//dateFecha_emision.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		ProductoDTO productoActual=new ProductoDTO();
		linkedhashmapproductosAct = new LinkedHashMap<Integer,ProductoDTO>();
		lgfIDProducto.setAlign(Alignment.CENTER);
		lgfCantidad.setAlign(Alignment.CENTER);
		lgfCodigo.setAlign(Alignment.CENTER);
		lgfDescripcion.setAlign(Alignment.CENTER);
		lgfPrecio_unitario.setAlign(Alignment.CENTER);
		lgfValor_total.setAlign(Alignment.CENTER);
		lgfIva.setAlign(Alignment.CENTER);
		lgfCosto.setAlign(Alignment.CENTER);
		lgfStock.setAlign(Alignment.CENTER);
		lgfDEscuento.setAlign(Alignment.CENTER);
		lgfBodega.setAlign(Alignment.CENTER);
		lgfBodegaN.setAlign(Alignment.CENTER);
		lgfBodegaU.setAlign(Alignment.CENTER);
		lgfBodegaT.setAlign(Alignment.CENTER);
		lgfPrecio_iva.setAlign(Alignment.CENTER);
		// NumberFormat formato2 = NumberFormat.getFormat("#.00");
		
		lgfNumero.setCellAlign(Alignment.CENTER);

		getService().fechaServidor(callbackFecha);		
		getService().ListEmpleados(0, 30, objbacklstEmpleado);

		btnImprimir.setDisabled(true);
		getService().listarPosiciones("Factura", listback);
		NumDto = Tipodoc;
		DocFis = doc;
		if (doc < 0)
			newRet = false;
		// Date to = new Date();
		dateFecha_emision.setUseTextField(true);
		dateFecha_emision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_emision.setEnforceDate(true);
		dateFecha_emision.setValue(fechaFactura);
		setSize("90%", "50%");
		// Espacio para el titulo segun sea el DOCUMENTO

		
		// hLayout.setSize("448px", "114px");
		// layout.setSize("504px", "127px");
		dfExtras.setAlign(Alignment.LEFT);
		layout.addMember(hLayout_3);
		//hLayout_3.setSize("50%", "114px");//cambiar
		hLayout_3.addMember(dfEncabezado);
		hLayout_3.addMember(dfNotaCredito);
		hLayout_3.addMember(dfExtras);
		
		dfEncabezado.setTitle("Cliente");
		dfEncabezado.setSize("100%", "100%");
		txtCLiente.addChangedHandler(new ManejadorBotones("persona"));// .setRedrawOnChange(true);
		txtRuc_cliente.addChangedHandler(new ManejadorBotones("persona"));// .setRedrawOnChange(true);

		dfEncabezado.setNumCols(4);
		txtAutorizacion_sri.setColSpan(2);
		txtAutorizacion_sri.setRequired(true);
		txtNumero_factura.setDisabled(false);
		txtNumero_factura.setRequired(true);
		txtRuc_cliente.setRequired(true);
		txtNumCompra.setRequired(true);
		txtNumCompra.setMask("###-###-#########");
		txtNumCompra.setKeyPressFilter("[0-9]");	
		PickerIcon pckNuevoCliente = new PickerIcon(PickerIcon.DATE);
		txtRuc_cliente.setIcons(pckNuevoCliente);
		pckNuevoCliente.addFormItemClickHandler(new ManejadorBotones(
				"nuevoCliente"));
		txtRuc_cliente.setDefaultValue("9999999999999");
		txtCLiente.setDefaultValue("CONSUMIDOR FINAL");
		txtDireccion.setDefaultValue(" ");
		txtTelefono.setDefaultValue(" ");
		txtCLiente.setRequired(true);
		// cmbVendedor.setRequired(true);
		cmbVendedor.setRequired(true);
		cmbVendedor.addChangedHandler(new ChangedHandler() 
		{  
	    	public void onChanged(ChangedEvent event) 
	    	{
	    		idVendedor = Integer.valueOf(cmbVendedor.getValue().toString());
			}  
	    });
		txtCiudad.setVisible(false);
		
		dfEncabezado.setFields(new FormItem[] { txtAutorizacion_sri,
				txtNumero_factura, dateFecha_emision, txtRuc_cliente,
				txtCLiente, txtDireccion, txtTelefono, txtCiudad, cmbVendedor,
				txtNumCompra, chkPedido, chkAfectarIVA });		
		
		if(Factum.banderaMenuRestaurant==1 && NumDto==0){
			chkPedido.setValue(true);
				
		}else{
			chkPedido.setValue(false);
			chkPedido.setVisible(false);
		}
		
		
		if(TipoDocumento==2){
			chkAfectarIVA.setVisible(true);
		}else{
			chkAfectarIVA.setVisible(false);
		}
		
		
		
		// dfExtras.setFields(cmbTipoPrecio);
		//cmbTipoPrecio.addChangedHandler(new ManejadorBotones("tipoprecio"));
		dfExtras.setFields(cmbTipoPrecio, cmbTipoBodega ,txtCorresp);
		
		if (NumDto != 1) {
			txtNumCompra.setVisible(false);
			txtNumCompra.setRequired(true);
			
		}
		if (NumDto == 7) {
			txtNumCompra.setVisible(true);
			txtNumCompra.setRequired(true);	
			cmbTipoPrecio.setTitle("TIPO&nbspDE&nbspGASTO");
			cmbTipoBodega.setVisible(false);
			btnNuevaFactura.setVisible(false);
			txtNumCompra.setTitle("#Factura de Gasto");
		}
		if (NumDto == 12) {
			cmbTipoPrecio.setTitle("TIPO&nbspDE&nbspINGRESO");
			cmbTipoBodega.setVisible(false);
		}
		if (NumDto != 0 && NumDto != 1 && NumDto != 2 &&  NumDto != 10 ) {
			btnExportar.setVisible(false);
			btnImportar.setVisible(false);
		}
		cmbTipoPrecio.setRequired(true);
		cmbTipoBodega.setRequired(true);
		if (NumDto == 4 || NumDto == 5 || NumDto == 15) {
			cmbTipoBodega.setVisible(false);
		}
		if (NumDto == 2 || NumDto == 10){
			lgfIva.setHidden(true);
			lgfPrecio_iva.setHidden(true);
		}
		txtCorresp.setRequired(true);
	    //dfNotaCredito.setSize("50%", "127px");
		//dfNotaCredito.setAlign(Alignment.CENTER);
		dfNotaCredito.setFields(new FormItem[] { txtCorresp });
		txtCorresp.setVisible(false);
		hLayout.addMember(layout);
		addMember(hLayout);
		// //para lo de la grilla
		// para que introduzca una lista desplegable en un campo de la grilla
		lgfEliminar.setAlign(Alignment.CENTER);
		lgfEliminar.setType(ListGridFieldType.IMAGE);
		lgfEliminar.setImgDir("delete.png");

		grdProductos = new ListGrid(){
			@Override  
	        protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
	            String fieldName = getFieldName(colNum);  
	            DynamicForm dynamicform = new DynamicForm();  
	            if (fieldName.equals("idbodega")) {
	            	HLayout recordCanvas = new HLayout(3); 
	            	recordCanvas.setHeight(22);  
	                recordCanvas.setWidth100();  
	                recordCanvas.setAlign(Alignment.CENTER);
	            	dynamicform = new DynamicForm();  
	            	ComboBoxItem comboxitembodegas = new ComboBoxItem();
	            	comboxitembodegas.setTabIndex(3);
	            	comboxitembodegas.setRequired(true);
	            	comboxitembodegas.setType("comboBox");
	            	
	            	final FormItem afectarProducto= new FormItem();   
	        		afectarProducto.setName("");  
	        		afectarProducto.setType("boolean");  
	        		afectarProducto.setShowTitle(false);
	        		afectarProducto.setDefaultValue(false);
	        		
	            	dynamicform.setFields(new FormItem[] {afectarProducto});
	            	recordCanvas.addMember(dynamicform);
	            	return recordCanvas;
	            }else {  
                    return null;  
                }  
	            
	            
	        }	
		};
		

		// Grilla que se creara automaticamente al poner el numero de pagos en
		// forma de pago a credito
		grdPagos = new ListGrid();
		grdPagos.setCanEdit(true);
		lgfValor_total.setCanEdit(false);
		lgfPrecio_iva.setCanEdit(true);
		grdPagos.setModalEditing(true);
		lgfPrecio_unitario.setCanEdit(true);
		grdPagos.setAutoSaveEdits(true);
		grdPagos.setEditEvent(ListGridEditEvent.DOUBLECLICK);
		lgfCantidadPago.setRequired(true);
		lgfCantidadPago.setType(ListGridFieldType.FLOAT);
		lgfVencimiento.setRequired(true);
		lgfVencimiento.setType(ListGridFieldType.DATE);
		lgfEstado.setRequired(false);
		lgfEstado.setType(ListGridFieldType.INTEGER);
		lgfEstado.setHidden(true);
		lgfId.setRequired(false);
		lgfId.setType(ListGridFieldType.INTEGER);
		lgfId.setHidden(true);

		grdPagos.setFields(lgfCantidadPago, lgfVencimiento, lgfEstado, lgfId);
		grdPagos.setHeight("150px");

		lgfEliminar.setCanEdit(false);
		lgfEliminar.setDefaultValue("   ");

		lgfEliminar.addRecordClickHandler(new Manejador("eliminar"));

		grdProductos.setCanEdit(true);
		grdProductos.setModalEditing(true);
		grdProductos.setAutoSaveEdits(true);
		grdProductos.setWidth("100%");
		grdProductos.setEditEvent(ListGridEditEvent.DOUBLECLICK);
		grdProductos.setShowRecordComponents(true); 
		grdProductos.setShowRecordComponentsByCell(true);

		lgfCantidad.setRequired(true);// los campos que son requeridos
		lgfCodigo.setRequired(true);
		lgfDescripcion.setRequired(true);
		lgfIDProducto.setRequired(true);
		lgfPrecio_unitario.setRequired(true);
		lgfPrecio_unitario.setAlign(Alignment.RIGHT);
		lgfPrecio_unitario.setType(ListGridFieldType.FLOAT);
		lgfPrecio_unitario.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		lgfValor_total.setRequired(true);
		lgfDEscuento.setType(ListGridFieldType.TEXT);
		lgfBodega.setCanEdit(true);
		lgfBodegaN.setHidden(false);
		lgfBodegaU.setHidden(false);
		lgfBodegaT.setHidden(false);
		lgfCantidad.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setAlign(Alignment.RIGHT);
		lgfValor_total.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		lgfPrecio_iva.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		
		
		lgfCantidad.addChangeHandler(new Manejador("cantidad"));
		
		lgfPrecio_unitario.addChangeHandler(new Manejador("pvp"));
		lgfCantidad.addEditorExitHandler(new Manejador("cantidad"));
		lgfPrecio_unitario.addEditorExitHandler(new Manejador("pvp"));
		lgfCodigo.addChangedHandler(new Manejador("barras"));
		lgfCodigo.addEditorExitHandler(new Manejador("barras"));
		lgfPrecio_iva.addEditorExitHandler(new Manejador("piva"));
		lgfPrecio_iva.addChangedHandler(new Manejador("piva"));
		lgfDescripcion.addChangeHandler(new Manejador("descripcion"));
		lgfIva.addEditorExitHandler(new Manejador("pvpIva"));
		lgfDEscuento.addEditorExitHandler(new Manejador("descuentoCh"));
		//lgfDEscuento.addChangeHandler(new Manejador("descuentoCh"));
		lgfBodega.addChangeHandler(new Manejador("bodega"));
		// Columnas para el producto

		// lgfIDProducto.setHidden(true);
		lgfStock.setCanEdit(false);
		grdProductos.setFields(lgfIDProducto, lgfNumero, lgfCodigo,
				lgfCantidad, lgfDescripcion, lgfPrecio_unitario, lgfIva,
				lgfStock, lgfDEscuento,
				 lgfBodega, lgfValor_total, lgfPrecio_iva, lgfEliminar,
				lgfBodega, lgfBodegaN, lgfBodegaU, lgfBodegaT, lgfCosto);

		grdProductos.setHeight("188px");

		// Formulario de opciones de formas de pago
		WidgetsForm wdg = new WidgetsForm();
		dynamicForm = new DynamicForm();
		dynamicForm.setTitle("FP");
		dynamicForm.setHeight("170px");		
		dynamicForm.setFields(new FormItem[] { 
				wdg.crearCheck("chkContado", "Contado", true),
				wdg.crearText("txtContado", "Cantidad", new FuncionSI(),false,10,"Debe ingresar numeros","[0-9.]",null,new Manejador2("Contado")),
				wdg.crearCheck("chkCredito", "Cr\u00E9dito", true),
				wdg.crearText("txtCredito", "# Pagos", new FuncionSI(),false,"Ingrese un n\u00famero",3, new Manejador("credito"),
						"Debe ingresar numeros","[0-9]")
				//Se debe generar cuadros de texto  fechas dinamicos segun el numero de pagos
				 });
	
	
		// Analizamos si se trata de un gasto
		if (NumDto == 7 || NumDto == 4 || NumDto == 12 || NumDto == 15) {
			txtAutorizacion_sri.setVisible(false);
			txtaConcepto.setTitle("Descripci\u00f3n");
			dfDescripcion.setHeight("150px");
			dfDescripcion.setWidth100();
			dfDescripcion.setFields(txtaConcepto);
			txtaConcepto.setHeight("100%");
			txtaConcepto.setWidth("100%");
			addMember(dfDescripcion);
			txtDescuento.setVisible(false);
			txtimpuestoAdicional.setVisible(false);
			txtDescuentoNeto.setVisible(false);
			txtBase_imponible.setVisible(false);
			txt0_iva.setVisible(false);
			txt14_iva.setVisible(false);
		} else {
			addMember(grdProductos);
			grdProductos.startEditingNew();
			grdProductos.draw();
			grdProductos.saveAllEdits();
			
			if (NumDto == 0 || NumDto == 1 || NumDto == 3 || NumDto == 13
					|| NumDto == 14 || NumDto == 20) {
				if (NumDto == 20) {
					txtAutorizacion_sri.setVisible(false);
				}
			} else {
				txtAutorizacion_sri.setVisible(false);
				txtDescuento.setVisible(false);
				txtimpuestoAdicional.setVisible(false);
				txtDescuentoNeto.setVisible(false);
				txtBase_imponible.setVisible(false);
				txt0_iva.setVisible(false);
				txt14_iva.setVisible(false);
				
				
			}
		}

		dynamicForm2.setTitle("FP");
		dynamicForm2.setHeight("170px");
		dynamicForm2
				.setFields(new FormItem[] {
						wdg.crearCheck("chkRetencion", "Retenci\u00F3n", true),
						wdg.crearText("txtRetencion", "Buscar",
								new FuncionSI(), false, 10, "", "[0-9a-zA-Z]",
								new Manejador("retencion"), 
								new Manejador2("retencion"), false, false, true,
								new ManejadorBotones("pckr1"),
								new ManejadorBotones("pckr2"),
								new ManejadorBotones("pckr3")),
						wdg.crearCheck("chkAnticipo", "Anticipo", true),
						wdg.crearText("txtAnticipo", "Buscar", new FuncionSI(),
								false, 10, "", "[0-9a-zA-Z]", new Manejador(
										"anticipo"),
								new Manejador2("anticipo"), true, true,
								new ManejadorBotones("pcka1"),
								new ManejadorBotones("pcka2")),
						wdg.crearCheck("chkNotaCredito",
								"Notas de Cr\u00E9dito", true),
						wdg.crearText("txtNotaCredito", "Buscar",
								new FuncionSI(), false, 10, "", "[0-9]",
								new Manejador("notacredito"), new Manejador2(
										"notacredito"), true, true,
								new ManejadorBotones("pckn1"),
								new ManejadorBotones("pckn2")) });

		dynamicForm3.setTitle("FP");
		dynamicForm3.setHeight("170px");
		dynamicForm3
				.setFields(new FormItem[] {
						wdg.crearCheck("chkTarjetaCr",
								"Tarjeta de Cr\u00E9dito", true),
						wdg.crearText("txtTarjetaCr", "Cantidad",
								new FuncionSI(), false, 10,
								"Debe ingresar n\u00fameros", "[0-9.]", null,
								new Manejador2("tarjetacredito")),
						wdg.crearText("txtTarjetaCr1", "Informaci\u00F3n",
								new FuncionSI(), false, "Num. Voucher", 10,
								null, "No ingresar simbolos", "[a-zA-Z0-9-#. ]"),
						wdg.crearCheck("chkBanco", "Banco", true),
						wdg.crearText("txtBanco", "Cantidad", new FuncionSI(),
								false, 10, "Debe ingresar numeros", "[0-9.]",
								null, new Manejador2("banco")),
						wdg.crearText("txtBanco1", "Informaci\u00F3n",
								new FuncionSI(), false, "Num. Banco y Banco",
								100, null, "No ingresar simbolos",
								"[a-zA-Z0-9-#. ]") });

		VLayout lFormasPago = new VLayout();
		lFormasPago.addMember(lblTotal_letras);
		if (NumDto != 20) {
			HLayout lFormas = new HLayout();
			lFormas.addMember(dynamicForm);
			lFormas.addMember(grdPagos);
			lFormas.addMember(dynamicForm2);
			lFormas.addMember(dynamicForm3);
			lFormasPago.addMember(lFormas);
		}
		if (NumDto == 20 && DocFis >= 0) {
		
			HLayout lFormas = new HLayout();
			lFormas.addMember(dynamicForm);
			lFormas.addMember(grdPagos);
			lFormas.addMember(dynamicForm2);
			lFormas.addMember(dynamicForm3);
			lFormasPago.addMember(lFormas);
			//lFormasPago.setVisible(false);
		}
		hLayout_2.addMember(lFormasPago);
		hLayout_2.addMember(dfPie_1);
		//hLayout_2.redraw();
		layout_2.setWidth("25%");
		dfPie_2.setSize("24%", "1%");
		// dfPie_2.setShowEdges(true);
		txtValor14_iva.addBlurHandler(new Manejador2("iva"));
		txtValor14_iva.addKeyPressHandler(new Manejador2("iva"));
		// AQUI MANEJAMOS LAS ACCIONES DEL CAMPO EFECTIVO PARA PODER CALCULAR EL
		// VUELTO
		txtEfectivo.addBlurHandler(new Manejador2("cambio"));
		txtEfectivo.addKeyPressHandler(new Manejador2("cambio"));
		txtEfectivo.addChangedHandler(new Manejador2("cambio"));

		txtDescuentoNeto.addBlurHandler(new Manejador2("descuentoNeto"));
		txtDescuentoNeto.addKeyPressHandler(new Manejador2("descuentoNeto"));

		txtSubtotal.addBlurHandler(new Manejador2("Subto"));
		txtSubtotal.addKeyPressHandler(new Manejador2("Subto"));
		dfPie_2.setFields(new FormItem[] { txtSubtotal, txtDescuento,
				txtDescuentoNeto, txtimpuestoAdicional,txtBase_imponible, txt0_iva, txt14_iva,
				txtValor14_iva, txtTotal, txtEfectivo, txtCambio });
		layout_2.addMember(dfPie_2);

		textBoxTotal.setWidth(250);
		textBoxTotal.setStyleName("heading");//BORRAR
		textBoxTotal.setVisible(false); 
		layout_2.addMember(textBoxTotal);//BORRAR
		
		txtSubtotal.setValue(0.0);
		txtDescuento.setValue(0.0);
		if(Factum.banderaImpuestoAdicional==1){
			txtimpuestoAdicional.setVisible(true);			
		}else{
			txtimpuestoAdicional.setVisible(false);
		}
		txtDescuento.setDisabled(true);
		txtimpuestoAdicional.setValue(0.0);
		txtimpuestoAdicional.setDisabled(true);
		txtDescuentoNeto.setValue(0.0);
		txtBase_imponible.setValue(0.0);
		txt0_iva.setValue(0.0);
		txt14_iva.setValue(0.0);
		txtValor14_iva.setValue(0.0);
		txtTotal.setValue(0.0);
		txtEfectivo.setValue(0.0);
		txtCambio.setValue(0.0);
		txtCambio.setDisabled(true);

		// layout_2.setShowEdges(true);
		hLayout_2.addMember(layout_2);
		
		addMember(hLayout_2);
		hLayout_4.setHeight("26px");
		btnNuevo.addClickHandler(new Manejador("nuevo"));
		hLayout_6.addMember(btnNuevo);
		hLayout_6.addMember(btnImprimir);
		hLayout_6.addMember(btnGrabar);
		if (NumDto == 20) {
			hLayout_6.addMember(btnTransformar);
		
			hLayout_6.addMember(btnacept);
			btnacept.setVisible(false);
		}
		// if(NumDto==0)
		// {
		hLayout_6.addMember(btnNuevaFactura);
		
		if (NumDto == 0 || NumDto == 2 || NumDto == 1 || NumDto == 10 || NumDto == 3 || NumDto == 14) {
			hLayout_6.addMember(btnImportar);
			hLayout_6.addMember(btnExportar);
			 txtDescuento.setAttribute("readOnly", true);
			 txtimpuestoAdicional.setAttribute("readOnly",true); 
			 txtDescuentoNeto.setVisible(false);
			 txtSubtotal.setAttribute("readOnly",true);
			 txtBase_imponible.setAttribute("readOnly",true);
			 txt0_iva.setAttribute("readOnly",true);
			 txt14_iva.setAttribute("readOnly",true);
			 txtValor14_iva.setAttribute("readOnly",true);
			 txtTotal.setAttribute("readOnly",true);
			 txtDireccion.setSelectionRange(0,0);
			 if(NumDto == 0 || NumDto == 2 ){//|| NumDto == 3
				 textBoxTotal.setVisible(true); 
			 }else{
				 textBoxTotal.setVisible(false); 
			 }
		}
		hLayout_6.addMember(btnEliminar);
		// hLayout_6.addMember(btnValidar);
		hLayout_4.addMember(hLayout_6);
		hLayout_5.setWidth("80%");
		hLayout_4.addMember(hLayout_5);
		addMember(hLayout_4);
		grdPagos.hide();
        
		if (NumDto == 7 || NumDto == 12) {
			// Aqui cargamos el listado de ceuntas para escoger la cuenta de
			// gasto
			getService().listarCuenta(0, 2000,planCuentaID, listaCuentas);
		} else {
			getService().listarTipoprecio(0, 2000, objbacklstTipoPrecio);
			getService().listarBodega(0, 2000, objbacklstTipoBodega);
		}
		// Primero vamos a analizar si la funcion debe mostrar un documento
		if (DocFis < 0) {
			// En este caso estamos por ingresar un documento nuevo y llamamos a
			// la funcion de carga de la base de datos
			analizarTipoDto(NumDto);
			// Llamamos a la funcion de listar los tipos de precios existentes

		} else {
			// En este caso debemos cargar el documento desde la base y
			// mostrarlo en pantalla
			getService().getDtoID(DocFis, callbackDoc);
			// Ademas se debe analizar el nivel de acceso del usuario
		}
		// buttonImprimir.addClickHandler(new Manejador("grabar"));
		btnGrabar.addClickHandler(new Manejador("grabar"));
		btnTransformar.addClickHandler(new Manejador("transformar"));
		btnacept.addClickHandler(new Manejador("aceptar"));
		// btnValidar.addClickHandler(new Manejador("validar"));
		btnNuevaFactura.addClickHandler(new Manejador("nuevaFactura"));
		// btnAsignarPuntos.addClickHandler(new Manejador("asignarPuntos"));
		btnEliminar.addClickHandler(new Manejador("borrar"));
		btnImprimir.addClickHandler(new Manejador("imprimir"));
		btnEliminar.setVisible(false);
		btnExportar.addClickHandler(new Manejador("exportar"));
		btnImportar.addClickHandler(new Manejador("importar"));
		// Obtenemos el tipo de usuario
		getService().getUserFromSession(callbackUser);

		 /******************************************************************* 
		 ELEMENTOS DE CARGA DE PRODUCTOS 
		getService().numeroRegistrosProducto("tblproducto" ,objbackI);

		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));

		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);

		cmbBuscarProducto.setLeft(158);
		cmbBuscarProducto.setTop(6);
		cmbBuscarProducto.setWidth(146);
		cmbBuscarProducto.setHeight(22);
		cmbBuscarProducto.setHint("Buscar Por");
		cmbBuscarProducto.setShowHintInField(true);
		cmbBuscarProducto.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n",
				"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
		
		
		
		cmdBod.setLeft(321);
		cmdBod.setTop(6);
		cmdBod.setHint("Seleccione una Bodega");
		cmdBod.setVisible(false);
		
		cmdBod.addChangedHandler(new ManejadorBotones("Buscar"));
		cmbBuscarProducto.addChangedHandler(new ManejadorBotones("Bodega"));
		
		chkServicio.setValue(false);
		chkServicio.addChangedHandler(new ManejadorBotonesServicio("Servicio"));
		chkProductoElaborado.setValue(false);
		chkProductoElaborado.addChangedHandler(new ManejadorBotonesProductoElaborado("ProductoElaborado"));
		searchFormProducto.setNumCols(6);
		searchFormProducto.setFields(new FormItem[] { txtBuscarLst, cmbBuscarProducto, cmdBod, chkServicio, chkProductoElaborado });
		if(Factum.banderaMenuServicio==1){
			chkServicio.setVisible(true);
			chkServicio.setDisabled(false);
		}else{
			chkServicio.setVisible(false);
			chkServicio.setDisabled(true);
		}
		if(Factum.banderaProduccionProductos==1){
			chkProductoElaborado.setVisible(true);
			chkProductoElaborado.setDisabled(false);
		}else{
			chkProductoElaborado.setVisible(false);
			chkProductoElaborado.setDisabled(true);
		}
		cmbBuscarPedido.setLeft(158);
		cmbBuscarPedido.setTop(6);
		cmbBuscarPedido.setWidth(146);
		cmbBuscarPedido.setHeight(22);
		cmbBuscarPedido.setHint("Buscar Por");
		cmbBuscarPedido.setShowHintInField(true);
		cmbBuscarPedido.setValueMap("Num Pedido", "Descripci\u00F3n",
				"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
		
		
		searchFormPedido.setItemLayout(FormLayoutType.ABSOLUTE);
		cmbBuscarPedido.addChangedHandler(new ManejadorBotones("Pedido"));
		searchFormPedido
				.setFields(new FormItem[] { txtBuscarLst, cmbBuscarPedido });
		
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
		btnFin.addClickHandler(new ManejadorBotones("right_all"));
		btnSiguiente.addClickHandler(new ManejadorBotones("right"));
		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
		lstProductosElaborados.addRecordDoubleClickHandler(new ManejadorBotones(
		"Seleccionar"));
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
		buscarPicker.addFormItemClickHandler(new ManejadorBotones(""));
		hStack.addMember(btnInicio);
		hStack.addMember(btnAnterior);
		hStack.addMember(btnSiguiente);
		hStack.addMember(btnFin);
		hStack.addMember(btnEliminarlst);

		ListGridField lstcodbarras = new ListGridField("codigoBarras",
				"C\u00F3digo de Barras", 100);
		lstcodbarras.setCellAlign(Alignment.LEFT);
		ListGridField lstdescripcion = new ListGridField("descripcion",
				"Descripci\u00F3n", 350);
		ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",
				100);
		lstimpuesto.setCellAlign(Alignment.CENTER);
		ListGridField lstlifo = new ListGridField("lifo", "LIFO", 60);
		lstlifo.setCellAlign(Alignment.CENTER);
		ListGridField lstfifo = new ListGridField("fifo", "FIFO", 60);
		lstfifo.setCellAlign(Alignment.CENTER);
		ListGridField lstpromedio = new ListGridField("promedio", "Costo", 60);
		lstpromedio.setCellAlign(Alignment.CENTER);
		ListGridField lstunidad = new ListGridField("Unidad", "Unidad", 100);
		ListGridField listGridField_6 = new ListGridField("stock", "Stock", 50);
		listGridField_6.setCellAlign(Alignment.CENTER);
		ListGridField lstcategoria = new ListGridField("Categoria",
				"Categoria", 100);
		lstcategoria.setCellAlign(Alignment.CENTER);
		ListGridField lstMarca = new ListGridField("Marca", "Marca", 100);
		lstMarca.setCellAlign(Alignment.CENTER);
		lstPrecio = new ListGridField("Precio", "Precio", 60);
		lstPrecio.setCellAlign(Alignment.CENTER);
		
		ListGridField buttonField = new ListGridField("buttonField", "Series",
				100);
		ListGridField lstidproducto = new ListGridField("idProductoB", "IdProducto",
				50);
		lstidproducto.setHidden(true);
		ListGridField lstcantidad = new ListGridField("cantidad", "Cantidad",
				50);
		lstcantidad.setCellAlign(Alignment.CENTER);
		lstcantidad.setType(ListGridFieldType.FLOAT);
		lstcantidad.addChangeHandler(new Manejador("agregarGrilla"));
		lstcantidad.addEditorExitHandler(new Manejador("agregarGrilla"));
		lstcantidad.setCanEdit(true);
		lstcantidad.setDefaultValue(0);// GIGACOMPUTERS
		ListGridField lstobservaciones = new ListGridField("observacion",
				"Observaciones", 250);
		lstobservaciones.addChangeHandler(new Manejador("agregarObservacion"));
		lstobservaciones.addEditorExitHandler(new Manejador(
				"agregarObservacion"));
		lstobservaciones.setCanEdit(true);
		ListGridField lstDESC = new ListGridField("DTO", "DTO %", 40);
		lstDESC.setCellAlign(Alignment.CENTER);
		lstDESC.addEditorExitHandler(new Manejador("agregarGrilla"));
		ListGridField inventarioInicial = new ListGridField("inicial",
				"I. Inicial", 50);
		inventarioInicial.setCellAlign(Alignment.CENTER);
		ListGridField lstpadre2 = new ListGridField("stockPadre", "Padre M.P",
				60);
		ListGridField lstpadre = new ListGridField("Padre", "Padre", 60);
		ListGridField lstjerarquia = new ListGridField("jerarquia", "Jerarquia", 60);
		lstProductos.setFields(new ListGridField[] { // GIGACOMPUTERS
						lstidproducto,lstcantidad, lstcodbarras, lstdescripcion, lstDESC,
						lstobservaciones, listGridField_6, inventarioInicial,
						lstpadre2, lstPrecio,
						lstimpuesto, lstpromedio, lstunidad, lstpadre, lstjerarquia});
		
		lstProductosElaborados.setFields(new ListGridField[] { // GIGACOMPUTERS
				lstcantidad, lstcodbarras, lstdescripcion, lstDESC,
						lstobservaciones, listGridField_6, inventarioInicial,
						lstpadre2, lstPrecio,
						lstimpuesto, lstpromedio, lstunidad, lstpadre, lstjerarquia});
		// layout_1.addMember(lstProductos);
		// layout_1.addMember(lblRegisros);

		// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");

		*//***********************************************************************//*
		grdProductos.setCanFocus(true);
		 
		// ;x
		//cmdBod.disable();
		
		precargarDatosOrdenTaller(orden,cliente);
		 switch(Factum.banderaElegirVendedor){
		 case 0:
			 cmbVendedor.setDisabled(true);
	     break;
		 case 1:
			 cmbVendedor.setDisabled(false);
		 break;
		 }*/
	}
	
	public Factura(Integer Tipodoc, Integer doc) {
		this.TipoDocumento=Tipodoc;
		this.Documento=doc;
		obtener_sesion();
	}
	
	public void inicializarFactura(){

		setSize("50%", "50%");
		//XAVIER ZEAS - DOCUMENTO COMERCIAL
		btnDocumentoElectronico.setVisible(false);
		btnDocumentoElectronico.setDisabled(true);
		//XAVIER ZEAS DOCUMENTO COMERCIAL	
		auxgrilla = 0;
		linkedhashmapproductosAct=new LinkedHashMap<Integer,ProductoDTO>();
		//productoActual=new ProductoDTO();
		lgfIDProducto.setAlign(Alignment.CENTER);
		lgfCantidad.setAlign(Alignment.CENTER);
		lgfCodigo.setAlign(Alignment.CENTER);
		lgfDescripcion.setAlign(Alignment.LEFT);
		lgfPrecio_unitario.setAlign(Alignment.RIGHT);
		lgfValor_total.setAlign(Alignment.RIGHT);
		lgfIva.setAlign(Alignment.CENTER);
		lgfCosto.setAlign(Alignment.RIGHT);
		
		lgfStock.setAlign(Alignment.CENTER);
		lgfDEscuento.setAlign(Alignment.CENTER);
		
	
		lgfBodega.setAlign(Alignment.CENTER);
		lgfBodegaN.setAlign(Alignment.CENTER);
		lgfBodegaU.setAlign(Alignment.CENTER);
		lgfBodegaT.setAlign(Alignment.CENTER);
		lgfPrecio_iva.setAlign(Alignment.RIGHT);
		
		//AGREGAMOS CAMPO OBSERVACION
		//txtObservacion.setAlign(Alignment.LEFT);
		txtObservacion.setHeight(30);
		txtObservacion.setWidth(730);
		//txtObservacion.setLeft(1);
		txtObservacion.setTitleStyle("lblFrm");
		txtObservacion.setTextBoxStyle("textFrm");
		
		// NumberFormat formato2 = NumberFormat.getFormat("#.00");
		lgfPrecio_iva.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					//return formatoDecimal.format(((Number) value).floatValue());
					return formatoDecimalN.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		lgfNumero.setCellAlign(Alignment.CENTER);

		getService().fechaServidor(callbackFecha);
		if (Factum.banderaElegirVendedor==1) getService().ListEmpleados(0, 30, objbacklstEmpleado);

		btnImprimir.setDisabled(true);
		getService().listarPosiciones("Factura", listback);
		NumDto = TipoDocumento;
		DocFis = Documento;
		if (DocFis < 0)
			newRet = false;
		// Date to = new Date();
		dateFecha_emision.setUseTextField(true);
		dateFecha_emision.setValue(fechaFactura);
		dateFecha_emision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		//dateFecha_emision.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		setSize("90%", "50%");
		// Espacio para el titulo segun sea el DOCUMENTO

		
		// hLayout.setSize("448px", "114px");
		// layout.setSize("504px", "127px");
		dfExtras.setAlign(Alignment.RIGHT);
		layout.addMember(hLayout_3);
		//hLayout_3.setSize("50%", "114px");//cambiar
		hLayout_3.addMember(dfEncabezado);
		hLayout_3.addMember(dfEncabezado2);
		
		hLayout_3.addMember(dfNotaCredito);
	//hLayout_3.addMember(dfExtras);
		
		dfEncabezado.setTitle("Datos");
		dfEncabezado.setSize("100%", "100%");
		
		dfEncabezado2.setTitle("Cliente");
		dfEncabezado2.setSize("100%", "100%");
		
//		txtCLiente.addChangedHandler(new ManejadorBotones("persona"));// .setRedrawOnChange(true);
//		txtRuc_cliente.addChangedHandler(new ManejadorBotones("persona"));// .setRedrawOnChange(true);
		txtRuc_cliente.addKeyPressHandler(new ManejadorBotones("persona"));
		
		dfEncabezado.setNumCols(10);
		dfEncabezado2.setNumCols(10);
		dfNotaCredito.setNumCols(10);
//		txtAutorizacion_sri.setColSpan(2);
		txtAutorizacion_sri.setRequired(true);
		txtAutorizacion_sri.setKeyPressFilter("[0-9]");
		txtAutorizacion_sri.setLength(49);
		txtAutorizacion_sri.addBlurHandler(new BlurHandler() {
			
			@Override
			public void onBlur(BlurEvent event) {
				String val=(String)event.getItem().getValue();
				if (val.length()<10) {
					SC.say("El numero de autorizacion debe tener un minimo de 10 numeros");
					event.getItem().focusInItem();
				}
			}
		});
		switch (NumDto){
		case 0:
			if (Factum.banderaModificarNumFactura==1){
				txtNumero_factura.setDisabled(false);
			}else{
				txtNumero_factura.setDisabled(true);
			}
			break;
		case 2:
			if (Factum.banderaModificarNumNota==1){
				txtNumero_factura.setDisabled(false);
			}else{
				txtNumero_factura.setDisabled(true);
			}
			break;
		case 3:
			if (Factum.banderaModificarNumNotaC==1){
				txtNumero_factura.setDisabled(false);
				
			}else{
				txtNumero_factura.setDisabled(true);
			}
			break;
		case 26:
			if (Factum.banderaModificarNumNotaV==1){
				txtNumero_factura.setDisabled(false);
				
			}else{
				txtNumero_factura.setDisabled(true);
			}
			break;
		default:
			txtNumero_factura.setDisabled(false);
			break;
		}
		
		txtNumero_factura.setRequired(true);
		txtRuc_cliente.setRequired(true);
		txtNumCompra.setRequired(true);
//		txtNumCompra.setMask("000-000-000000000");
		txtNumCompra.setMask("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
//		txtNumCompra.setMask("###-###-#########");
		txtNumCompra.setMaskPromptChar("0");
		txtNumCompra.setMaskPadChar("0");
		txtNumCompra.addChangedHandler(new com.smartgwt.client.widgets.form.fields.events.ChangedHandler(){

			@Override
			public void onChanged(com.smartgwt.client.widgets.form.fields.events.ChangedEvent event) {
				// TODO Auto-generated method stub
				String val=(String)event.getValue();
//				//com.google.gwt.user.client.Window.alert("on changeD "+(String)event.getValue());
//				//com.google.gwt.user.client.Window.alert("on changeD "+(String)event.getItem().getDisplayValue());
				while (val.length()<15){
					val+="0";
				}
				event.getItem().setValue(val);
			}
			
		});
		txtNumCompra.setMaskOverwriteMode(true);
		
		PickerIcon pckBuscarCliente = new PickerIcon(PickerIcon.SEARCH);
//		txtRuc_cliente.setIcons(pckNuevoCliente);
		pckBuscarCliente.addFormItemClickHandler(new ManejadorBotones(
				"buscarCliente"));
		
		PickerIcon pckNuevoCliente = new PickerIcon(PickerIcon.REFRESH);
//		txtRuc_cliente.setIcons(pckNuevoCliente);
		pckNuevoCliente.addFormItemClickHandler(new ManejadorBotones(
				"nuevoCliente"));
		txtRuc_cliente.setDefaultValue("9999999999999");
		txtRuc_cliente.setIcons(pckBuscarCliente);
		
		txtCLiente.setDefaultValue("CONSUMIDOR FINAL");
		txtCLiente.setIcons(pckNuevoCliente);
		txtCLiente.setColSpan(3);
		txtCLiente.setAttribute("readOnly",true);
		txtCLiente.setWidth(450);
		
		txtCorreo.setColSpan(3);
		txtCorreo.setWidth(450);
		txtCorreo.setAttribute("readOnly",true);
		
		txtDireccion.setDefaultValue(" ");
		txtCorreo.setDefaultValue(" ");
		txtTelefono.setDefaultValue(" ");
		// OBSERVACION
		txtObservacion.setDefaultValue(" ");
		txtCLiente.setRequired(true);
		cmbVendedor.setRequired(true);
		cmbVendedor.addChangedHandler(new ChangedHandler() 
		{  
	    	public void onChanged(ChangedEvent event) 
	    	{
	    		idVendedor = Integer.valueOf(cmbVendedor.getValue().toString());
	    		vendedor= new PersonaDTO();
	    		vendedor.setIdPersona(idVendedor);
	    		//vendedor.setRazonSocial(vendedor.getRazonSocial());
	    		vendedor.setNombreComercial(vendedor.getNombreComercial());
			}  
	    });
		
		//txtVendedor.setRequired(true);
		txtCiudad.setVisible(false);
		txtDireccion.setVisible(false);
		txtTelefono.setVisible(false);

		dfEncabezado.setFields(new FormItem[] { txtAutorizacion_sri,
				txtNumero_factura, dateFecha_emision
				
				,cmbVendedor
				/*, chkPedido*//*,chkAfectarIVA*/
				,cmbTipoPrecio
		});		
		
		dfEncabezado2.setFields(new FormItem[] { txtRuc_cliente,
				txtCLiente
				,txtCorreo
				, txtDireccion, txtTelefono, txtCiudad
		});		
		/*if(Factum.banderaMenuRestaurant==1 && NumDto==0){
			chkPedido.setValue(true);
				
		}else{
			chkPedido.setValue(false);
			chkPedido.setVisible(false);
		}*/
		//SC.say("NumDto: "+NumDto);
		/*
		if(NumDto==2){
			chkAfectarIVA.setVisible(true);
			chkAfectarIVA.addChangedHandler(new ChangedHandler() {
				
				@Override
				public void onChanged(ChangedEvent event) {
					SC.say("VALOR DE CHECK: "+chkAfectarIVA.getValue());					
				}
			});
		}else{
			chkAfectarIVA.setVisible(false);
		}
		*/
		
		// dfExtras.setFields(cmbTipoPrecio);
		//cmbTipoPrecio.addChangedHandler(new ManejadorBotones("tipoprecio"));
		
	//dfExtras.setFields(cmbTipoPrecio/*, cmbTipoBodega*/ /*,txtCorresp*/);
		
		if (NumDto != 1 && NumDto != 14 && NumDto != 27) {
			txtNumCompra.setVisible(false);
			txtNumCompra.setRequired(true);
		}
		if (NumDto == 7) {
			txtNumCompra.setVisible(true);
			txtNumCompra.setRequired(true);				
			cmbTipoPrecio.setTitle("TIPO&nbspDE&nbspGASTO");
			//cmbTipoBodega.setVisible(false);
			btnNuevaFactura.setVisible(false);
			txtNumCompra.setTitle("#Factura de Gasto");
		}
		if (NumDto == 12) {
			cmbTipoPrecio.setTitle("TIPO&nbspDE&nbspINGRESO");
			//cmbTipoBodega.setVisible(false);
		}
		if (NumDto != 0 && NumDto != 1 && NumDto != 2 &&  NumDto != 10 ) {
			btnExportar.setVisible(false);
			btnImportar.setVisible(false);
		}
		cmbTipoPrecio.setRequired(true);
		//cmbTipoBodega.setRequired(true);
		if(NumDto != 7 && NumDto != 12){
			cmbTipoPrecio.addChangedHandler(new ManejadorBotonesServicio("Precio"));
		}
		if (NumDto==14) {
			
			txtNumCompra.setVisible(true);
			txtNumCompra.setTitle("#Nota Proveedor");
		}
		/*if (NumDto == 4 || NumDto == 5 || NumDto == 15) {
			cmbTipoBodega.setVisible(false);
			
		}*/
	    //dfNotaCredito.setSize("50%", "127px");
		//dfNotaCredito.setAlign(Alignment.CENTER);
		txtCorresp.setRequired(true);
		txtMotivo.setRequired(true);
//		txtCorresp.setMask("###-###-#########");
//		txtCorresp.setMask("000-000-000000000");
		txtCorresp.setMask("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
		txtCorresp.setMaskPromptChar("0");
		txtCorresp.setMaskPadChar("0");
		txtCorresp.addChangedHandler(new com.smartgwt.client.widgets.form.fields.events.ChangedHandler(){

			@Override
			public void onChanged(com.smartgwt.client.widgets.form.fields.events.ChangedEvent event) {
				// TODO Auto-generated method stub
				String val=(String)event.getValue();
				while (val.length()<15){
					val+="0";
				}
				event.getItem().setValue(val);
			}
			
		});
		txtCorresp.addKeyPressHandler( new ManejadorBotones("factura"));
		txtCorresp.setMaskOverwriteMode(true);
//		txtCorresp.setKeyPressFilter("[09]");
		dfNotaCredito.setFields(new FormItem[] { txtNumCompra,txtCorresp, txtMotivo });
		if (DocFis>=0 && (NumDto==3 || NumDto==14)){
			txtCorresp.setVisible(true);
			txtMotivo.setVisible(true);
		}else{
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);
		}
		hLayout.addMember(layout);
		addMember(hLayout);
		// //para lo de la grilla
		// para que introduzca una lista desplegable en un campo de la grilla
		lgfEliminar.setAlign(Alignment.CENTER);
		lgfEliminar.setType(ListGridFieldType.IMAGE);
		lgfEliminar.setImgDir("delete.png");

		grdProductos = new ListGrid(){
			
			@Override  
	        protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
	            String fieldName = getFieldName(colNum); 
	            DynamicForm dynamicform = new DynamicForm();
	            //SC.say(productoActual.getIdProducto().toString());
	            if (fieldName.equals("idbodega")) {
	            	//SC.say("actualizar canvas: "
	            			/*+String.valueOf(productoActual.getIdProducto())+", "
	            			+String.valueOf(record.getAttributeAsInt("idProducto"))+", "
	            			+String.valueOf(productoActual.getIdProducto().equals(record.getAttributeAsInt("idProducto")))+", "*/
	            	//);
					Integer idProducto=record.getAttributeAsInt("idProducto");
					
					//ProductoDTO productoconbodegas = linkedhashmapproductos.get(idProducto);
					//ProductoDTO productoconbodegas = productoActual;
					//SC.say(String.valueOf(linkedhashmapproductosAct.containsKey(idProducto))+" "+String.valueOf(idProducto));
					ProductoDTO productoconbodegas = linkedhashmapproductosAct.get(idProducto);
					String mensaje="LONGITUD: "+productoconbodegas.getTblbodega().size();
					//SC.say(mensaje);
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					valueMap.put("SELECCIONAR", "SELECCIONAR");
					
					int i=0;
					int idBodega=0;				
					String value="";
					if(NumDto==0 || NumDto==2 || NumDto==3 || NumDto==20 || NumDto==1 || NumDto==10 || NumDto==14 || NumDto==26 || NumDto==27){
						for(BodegaDTO bodegadto: productoconbodegas.getTblbodega() ){
							//mensaje=mensaje+"-"+bodegadto.getIdBodega()+", "+bodegadto.getBodega();
							//PabloSinchiBodega
							String value2="";
							if (Factum.banderaBodegaUbicacion==1){
								value2= bodegadto.getBodega()+"/"+bodegadto.getUbicacion();
							}else{
								value2= bodegadto.getBodega();
							}
							valueMap.put(bodegadto.getIdBodega().toString(), value2);
							//valueMap.put(bodegadto.getIdBodega().toString(), bodegadto.getBodega());
							try{
			                	idBodega=record.getAttributeAsInt("idbodega");
			                	//SC.say(String.valueOf(idBodega));
			                	if (idBodega==bodegadto.getIdBodega().intValue()){
			                		value=value2;
			                	}
			                }catch(Exception e){
			                	//SC.say(e.getCause().getMessage()+", "+e.getMessage());
			                	if(i==0){
									idBodega=bodegadto.getIdBodega();
									value=value2;
								}
			                }
	
							i++;
						}	
					}else if(NumDto==1 || NumDto==10 || NumDto==14 || NumDto==27){						
						valueMap=MapBodega;
						idBodega=1;
					}
					
					//SC.say("-"+mensaje);
	            	HLayout recordCanvas = new HLayout(); 
	            	recordCanvas.setHeight100();  
	                recordCanvas.setWidth100();  
	                recordCanvas.setAlign(Alignment.CENTER);
	            	dynamicform = new DynamicForm();  
	            	SelectItem selectItem = new SelectItem();
	                selectItem.setTitle("");
	                selectItem.setWidth("130");
	                selectItem.setValueMap(valueMap);
	                //selectItem.setDefaultToFirstOption(true);
	                selectItem.setDefaultValue(String.valueOf(idBodega));
	                record.setAttribute("idbodega", idBodega);
	                selectItem.addChangedHandler(new ChangedHandler() {  
	                    public void onChanged(ChangedEvent event) {  
	                        String ds = (String) event.getValue(); 
	                        record.setAttribute("idbodega", ds);
	                        //grdProductos.redraw();
	                        //SC.say(ds);
	                    }  
	                });  
	            	dynamicform.setFields(new FormItem[] {selectItem});
	            	//dynamicform.setAlign(Alignment.CENTER);
	            	//dynamicform.setWidth100();
	            	recordCanvas.addMember(dynamicform);
	            	return recordCanvas;
	            }else {  
                    return null;  
                }  
	            
	            
	        }	
		};
		// Grilla que se creara automaticamente al poner el numero de pagos en
		// forma de pago a credito
		grdPagos = new ListGrid();
		grdPagos.setCanEdit(true);
		lgfValor_total.setCanEdit(false);
		lgfPrecio_iva.setCanEdit(true);
		grdPagos.setModalEditing(true);
		lgfPrecio_unitario.setCanEdit(true);
		grdPagos.setAutoSaveEdits(true);
		grdPagos.setEditEvent(ListGridEditEvent.DOUBLECLICK);
		grdPagos.setAutoFetchData(false);
		lgfCantidadPago.setRequired(true);
		lgfCantidadPago.setType(ListGridFieldType.FLOAT);
		lgfVencimiento.setRequired(true);
		lgfVencimiento.setType(ListGridFieldType.DATE);
//		lgfVencimiento.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		lgfVencimiento.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
//		lgfVencimiento.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		lgfFechaRealPago.setRequired(false);
		lgfFechaRealPago.setType(ListGridFieldType.DATE);
		lgfFechaRealPago.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		lgfFechaRealPago.setHidden(true);
		
		lgfNumEgreso.setRequired(false);
		lgfNumEgreso.setType(ListGridFieldType.INTEGER);
		lgfNumEgreso.setHidden(true);
		
		lgfConcepto.setRequired(false);
		lgfConcepto.setType(ListGridFieldType.TEXT);
		lgfConcepto.setHidden(true);
		
		lgfFormaPago.setRequired(false);
		lgfFormaPago.setType(ListGridFieldType.TEXT);
		lgfFormaPago.setHidden(true);
		
		lgfEstado.setRequired(false);
		lgfEstado.setType(ListGridFieldType.INTEGER);
		lgfEstado.setHidden(true);
		lgfId.setRequired(false);
		lgfId.setType(ListGridFieldType.INTEGER);
		lgfId.setHidden(true);

		grdPagos.setFields(lgfCantidadPago, lgfVencimiento, lgfEstado, lgfId, lgfFechaRealPago, lgfNumEgreso, lgfConcepto, lgfFormaPago);
		grdPagos.setWidth("300px");
		grdPagos.setHeight("150px");

		lgfEliminar.setCanEdit(false);
		lgfEliminar.setDefaultValue("   ");

		lgfEliminar.addRecordClickHandler(new Manejador("eliminar"));

		grdProductos.setCanEdit(true);
		grdProductos.setModalEditing(true);
		grdProductos.setAutoSaveEdits(true);
		grdProductos.setWidth("100%");
		grdProductos.setEditEvent(ListGridEditEvent.DOUBLECLICK);
		grdProductos.setShowRecordComponents(true); 
		grdProductos.setShowRecordComponentsByCell(true);

		lgfCantidad.setRequired(true);// los campos que son requeridos
		lgfCodigo.setRequired(true);
		lgfDescripcion.setRequired(true);
		lgfIDProducto.setRequired(true);
		lgfPrecio_unitario.setRequired(true);
		lgfPrecio_unitario.setAlign(Alignment.RIGHT);
		lgfPrecio_unitario.setType(ListGridFieldType.FLOAT);
		lgfPrecio_unitario.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					return formatoDecimalN.format(((Number) value).floatValue());
					//return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		
		lgfValor_total.setRequired(true);
		lgfDEscuento.setType(ListGridFieldType.TEXT);
		lgfBodega.setCanEdit(true);
		lgfBodegaN.setHidden(false);
		lgfBodegaU.setHidden(false);
		lgfBodegaT.setHidden(false);
		lgfCantidad.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setAlign(Alignment.RIGHT);
		lgfValor_total.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					return formatoDecimalN.format(((Number) value).floatValue());
					//return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});lgfPrecio_iva.setAlign(Alignment.RIGHT);
		lgfPrecio_iva.setType(ListGridFieldType.FLOAT);
		lgfPrecio_iva.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null) 
					return null;
				try {
					return formatoDecimalN.format(((Number) value).floatValue());
					//return formatoDecimal.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		
		lgfCantidad.addChangeHandler(new Manejador("cantidad"));
		lgfPrecio_unitario.addChangeHandler(new Manejador("pvp"));
		lgfCantidad.addEditorExitHandler(new Manejador("cantidad"));
		lgfPrecio_unitario.addEditorExitHandler(new Manejador("pvp"));
		lgfCodigo.addChangedHandler(new Manejador("barras"));
		lgfCodigo.addEditorExitHandler(new Manejador("barras"));
		lgfPrecio_iva.addEditorExitHandler(new Manejador("piva"));
		lgfPrecio_iva.addChangedHandler(new Manejador("piva"));
		lgfDescripcion.addChangeHandler(new Manejador("descripcion"));
		lgfIva.addEditorExitHandler(new Manejador("pvpIva"));
		lgfDEscuento.addEditorExitHandler(new Manejador("descuentoCh"));
		//lgfDEscuento.addChangeHandler(new Manejador("descuentoCh"));
		lgfBodega.addChangeHandler(new Manejador("bodega"));
		// Columnas para el producto
		if (NumDto==2 || NumDto==10 || NumDto==27 || NumDto==26){
			lgfIva.setHidden(true);
			lgfPrecio_iva.setHidden(true);
		}
		// lgfIDProducto.setHidden(true);
		lgfIva.setCanEdit(false);
		lgfStock.setCanEdit(false);
		grdProductos.setFields(lgfIDProducto, lgfNumero, lgfCodigo,
				lgfCantidad, lgfDescripcion, lgfPrecio_unitario, lgfIva,
				lgfStock, lgfDEscuento,
				/* lgfBodega, */lgfValor_total, lgfPrecio_iva, lgfEliminar,
				lgfBodega, lgfBodegaN, lgfBodegaU,/* lgfBodegaT, */lgfCosto);

		grdProductos.setHeight("350px");

		// Formulario de opciones de formas de pago
		WidgetsForm wdg = new WidgetsForm();
		dynamicForm = new DynamicForm();
		dynamicForm.setTitle("FP");
		dynamicForm.setHeight("170px");
		txtcontado=wdg.crearText("txtContado", "Cantidad", new FuncionSI(),false,10,"Debe ingresar numeros","[0-9.]",null
				,new Manejador2("Contado"));
		chkcontado=wdg.crearCheck("chkContado", "Contado", true);
		chkcontado.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				if((Boolean) event.getValue()){
					txtcontado.show();
				}else{
					txtcontado.hide();
				}
			}
			
		});
		if (NumDto == 4 || NumDto == 15 || NumDto==3 || NumDto==14) {
			dynamicForm.setFields(new FormItem[] { 
					chkcontado,
					txtcontado
					
					//Se debe generar cuadros de texto  fechas dinamicos segun el numero de pagos
					 });
		}else{
			dynamicForm.setFields(new FormItem[] { 
					chkcontado,
					txtcontado,
//					wdg.crearCheck("chkContado", "Contado", true),
//					wdg.crearText("txtContado", "Cantidad", new FuncionSI(),false,10,"Debe ingresar numeros","[0-9.]",new Manejador2("Contado"),new Manejador2("Contado")),
					wdg.crearCheck("chkCredito", "Cr\u00E9dito", true),
					wdg.crearText("txtCredito" , "# Pagos"   , new FuncionSI(),false,"Ingrese un n\u00famero",3, new Manejador("credito"),
							"Debe ingresar numeros","[0-9]")
					//Se debe generar cuadros de texto  fechas dinamicos segun el numero de pagos
					 });
		}
		txtBase_imponible.setVisible(false);
		txtTotal.setVisible(false);
		// Analizamos si se trata de un gasto
		if (NumDto == 7 || NumDto == 4 || NumDto == 12 || NumDto == 15) {
			txtAutorizacion_sri.setVisible(false);
			txtaConcepto.setTitle("Descripci\u00f3n: ");
			dfDescripcion.setHeight("300px");
			dfDescripcion.setWidth100();
			dfDescripcion.setFields(txtaConcepto);
			txtaConcepto.setHeight("100%");
			txtaConcepto.setWidth("100%");
			addMember(dfDescripcion);
			txtDescuento.setVisible(false);
			txtimpuestoAdicional.setVisible(false);
			txtDescuentoNeto.setVisible(false);
			txtBase_imponible.setVisible(false);
			txt0_iva.setVisible(false);
			txt14_iva.setVisible(false);
			totalLabel.setVisible(false);
			txtTotal.setVisible(true);
			txtTotal.setTitleStyle("lblFrm");
			txtTotal.setTextBoxStyle("textFrm");
			txtTotal.setHeight(30);
			txtTotal.setWidth(250);
			
		} else {
			addMember(grdProductos);
			grdProductos.startEditingNew();
			grdProductos.draw();
			grdProductos.saveAllEdits();
			
			if (NumDto == 10){
				txtTotal.setVisible(true);
				txtTotal.setTitleStyle("lblFrm");
				txtTotal.setTextBoxStyle("textFrm");
				txtTotal.setHeight(30);
				txtTotal.setWidth(250);
			}
			
			if (NumDto == 0 || NumDto == 1 || NumDto == 3 || NumDto == 13
					|| NumDto == 14 || NumDto == 20 || NumDto == 26 || NumDto == 27) {
				if (NumDto == 20) {
					txtAutorizacion_sri.setVisible(false);
				}
				if(NumDto==0||NumDto==1){//SI DOCUMENTO ES FACURA DE VENTA Y FACTURACION  DE COMPRA
					btnDocumentoElectronico.setVisible(true);//XAVIER ZEAS - DCOUMENTO ELECTRONICO
				}
			} else {
				txtAutorizacion_sri.setVisible(false);
				txtimpuestoAdicional.setVisible(false);
				txtDescuento.setVisible(false);
				txtDescuentoNeto.setVisible(false);
				txtBase_imponible.setVisible(false);
				txt0_iva.setVisible(false);
				txt14_iva.setVisible(false);
				
				
			}
		}
		
		dynamicForm2.setTitle("FP");
		dynamicForm2.setHeight("170px");
		
		if (NumDto == 0 || NumDto == 1 || (NumDto == 20 && DocFis >= 0)){
		dynamicForm2
				.setFields(new FormItem[] {
						wdg.crearCheck("chkRetencion", "Retenci\u00F3n", true),
						wdg.crearText("txtRetencion", "Buscar",
								new FuncionSI(), false, 10, "", "[0-9a-zA-Z]",
								new Manejador("retencion"), new Manejador2(
										"retencion"), false, false, true,
								new ManejadorBotones("pckr1"),
								new ManejadorBotones("pckr2"),
								new ManejadorBotones("pckr3")),
						wdg.crearCheck("chkAnticipo", "Anticipo", true),
						wdg.crearText("txtAnticipo", "Buscar", new FuncionSI(),
								false, 10, "", "[0-9a-zA-Z]", new Manejador(
										"anticipo"),
								new Manejador2("anticipo"), true, true,
								new ManejadorBotones("pcka1"),
								new ManejadorBotones("pcka2")),
						wdg.crearCheck("chkNotaCredito",
								"Notas de Credito", true),
						wdg.crearText("txtNotaCredito", "Buscar",
								new FuncionSI(), false, 10, "", "[0-9]",
								new Manejador("notacredito"), new Manejador2(
										"notacredito"), true, true,
								new ManejadorBotones("pckn1"),
								new ManejadorBotones("pckn2")) });
		}else if (NumDto != 26 && NumDto != 27){
			dynamicForm2
			.setFields(new FormItem[] {
					wdg.crearCheck("chkAnticipo", "Anticipo", true),
					wdg.crearText("txtAnticipo", "Buscar", new FuncionSI(),
							false, 10, "", "[0-9a-zA-Z]", new Manejador(
									"anticipo"),
							new Manejador2("anticipo"), true, true,
							new ManejadorBotones("pcka1"),
							new ManejadorBotones("pcka2")),
					wdg.crearCheck("chkNotaCredito",
							"Notas de Credito", true),
					wdg.crearText("txtNotaCredito", "Buscar",
							new FuncionSI(), false, 10, "", "[0-9]",
							new Manejador("notacredito"), new Manejador2(
									"notacredito"), true, true,
							new ManejadorBotones("pckn1"),
							new ManejadorBotones("pckn2")) });
		}else{
			dynamicForm2
			.setFields(new FormItem[] {
					wdg.crearCheck("chkAnticipo", "Anticipo", true),
					wdg.crearText("txtAnticipo", "Buscar", new FuncionSI(),
							false, 10, "", "[0-9a-zA-Z]", new Manejador(
									"anticipo"),
							new Manejador2("anticipo"), true, true,
							new ManejadorBotones("pcka1"),
							new ManejadorBotones("pcka2")) });
		}
		dynamicForm3.setTitle("FP");
		dynamicForm3.setHeight("170px");
		dynamicForm3
				.setFields(new FormItem[] {
						wdg.crearCheck("chkTarjetaCr",
								"Tarjeta de Credito", true),
						wdg.crearText("txtTarjetaCr", "Cantidad",
								new FuncionSI(), false, 10,
								"Debe ingresar n\u00fameros", "[0-9.]", null,
								new Manejador2("tarjetacredito")),
						wdg.crearText("txtTarjetaCr1", "Informaci\u00F3n",
								new FuncionSI(), false, "Num. Voucher", 10,
								null, "No ingresar simbolos", "[a-zA-Z0-9-#. ]"),
						wdg.crearCheck("chkBanco", "Banco", true),
						wdg.crearText("txtBanco", "Cantidad", new FuncionSI(),
								false, 10, "Debe ingresar numeros", "[0-9.]",
								null, new Manejador2("banco")),
						wdg.crearText("txtBanco1", "Informaci\u00F3n",
								new FuncionSI(), false, "Num. Cheque y/o Banco",
								100, null, "No ingresar simbolos",
								"[a-zA-Z0-9-#. ]") });
		
	

		VLayout lFormasPago = new VLayout();
		//lFormasPago.addMember(lblTotal_letras);
		if (NumDto == 4 || NumDto == 15 || NumDto==3 || NumDto==14 ) {
			HLayout lFormas = new HLayout();
			lFormas.addMember(dynamicForm);
			lFormas.addMember(dynamicForm3);
			lFormasPago.addMember(lFormas);
		}
		if (NumDto != 20 && NumDto != 4 && NumDto != 15 && NumDto!=3 && NumDto!=14) {
			HLayout lFormas = new HLayout();
			lFormas.addMember(dynamicForm);
			lFormas.addMember(grdPagos);
			lFormas.addMember(dynamicForm2);
			lFormas.addMember(dynamicForm3);
			lFormasPago.addMember(lFormas);
		}
		/*if (NumDto == 20 && DocFis >= 0) {
		
			HLayout lFormas = new HLayout();
			lFormas.addMember(dynamicForm);
			lFormas.addMember(grdPagos);
			lFormas.addMember(dynamicForm2);
			lFormas.addMember(dynamicForm3);
			lFormasPago.addMember(lFormas);
			//lFormasPago.setVisible(false);
		}*/
		//if (NumDto==20) lFormasPago.setVisible(false);

			
		if (NumDto!=20) hLayout_2.addMember(lFormasPago);
		
		//hLayout_2.addMember(dfAntesPie);
//		hLayout_2.addMember(dfPie_1);
		//hLayout_2.redraw();
		
		layout_2.setWidth("25%");
		//layout_2.setPosition("fix");
		dfPie_2.setSize("24%", "1%");
		dfAntesPie.setSize("18%", "1%");
		//dfAntesPie.setNumCols(10);
		hlayout_totales.setWidth("100%");
		//hlayout_totales.setTop(-20);
		vlayout_total.setWidth("100%");
		//vlayout_total.setTop(-20);
		dfPie_totales.setWidth("100%");
		dfPie_totales.setNumCols(10);
		//dfPie_totales.setTop(-20);
		// dfPie_2.setShowEdges(true);
		txtValor14_iva.addBlurHandler(new Manejador2("iva"));
		txtValor14_iva.addKeyPressHandler(new Manejador2("iva"));
		// AQUI MANEJAMOS LAS ACCIONES DEL CAMPO EFECTIVO PARA PODER CALCULAR EL
		// VUELTO
		txtCambio.setHeight(20);
		txtCambio.setWidth(110);
		txtCambio.setTitleStyle("lblFrm");
		txtCambio.setTextBoxStyle("textFrm");
		txtEfectivo.addBlurHandler(new Manejador2("cambio"));
		txtEfectivo.setHeight(20);
		txtEfectivo.setWidth(110);
		txtEfectivo.setTitleStyle("lblFrm");
		txtEfectivo.setTextBoxStyle("textFrm");
		txtEfectivo.addKeyPressHandler(new Manejador2("cambio"));
		txtEfectivo.addChangedHandler(new Manejador2("cambio"));

		txtDescuentoNeto.addBlurHandler(new Manejador2("descuentoNeto"));
		txtDescuentoNeto.addKeyPressHandler(new Manejador2("descuentoNeto"));

		txtSubtotal.addBlurHandler(new Manejador2("Subto"));
		txtSubtotal.addKeyPressHandler(new Manejador2("Subto"));
		
		//dfPie_2.setPosition("absolute");
		//dfPie_2.setMargin(-50);
		//dfPie_2.setTop(70);
		
		//dfPie_2.setMar
		//txtEfectivo.setTop(40);
		dfPie_2.setMargin(30);
		dfPie_2.setFields(new FormItem[] { 
//				txtSubtotal, txtDescuento,
//				txtDescuentoNeto, txtimpuestoAdicional,txtBase_imponible, txt0_iva, txt14_iva,
//				txtValor14_iva, txtTotal, 
				txtEfectivo, txtCambio });
		
		//dfAntesPie.setMargin("-25", "0", "0", "0");
		dfAntesPie.setPosition("static");
		dfAntesPie.setWidth(25);
		
		//dfAntesPie.setBackgroundColor("#22AAFF");
		//dfAntesPie.setAlign(Alignment.LEFT);
		dfAntesPie.setFields(new FormItem[]{
				txtObservacion
		});
		
		dfPie_totales.setFields(new FormItem[]{
				txtSubtotal, txtDescuento,
				txtDescuentoNeto, txtimpuestoAdicional,
				txtBase_imponible, 
				txt0_iva, txt14_iva,
				txtValor14_iva
				, txtTotal
		});
		
		/*
		layout_2.addMember(dfAntesPie);
		vLayout_pie.addMember(dfAntesPie);
		vLayout_pie.addMember(dfAntesPie);
		*/
		//vLayout_pie.setPosition("fix");
	
		hLayout_observa.setWidth(50);
		hLayout_observa.addMember(dfAntesPie);
		vLayout_pie.addMember(dfAntesPie);
		vLayout_pie.addMember(dfAntesPie);
//		;hide();
		textBoxTotal.setWidth(200);
//		textBoxTotal.setPrefix("TOTAL: ");
//		textBoxTotal.setTitle("Total");
		textBoxTotal.setStyleName("heading");//BORRAR
		textBoxTotal.setVisible(false);
		
		totalLabel.setHeight("50px");
		totalLabel.setWidth("50px");
		totalLabel.setStyleName("lblFrm");
//		totalLabel.setPixelSize(30, 30);
		totalLabel.setText("TOTAL: ");
		txtTotalXML.setValue(0.0);
		
		hlayout_total.addMember(totalLabel);
		hlayout_total.addMember(textBoxTotal);
		
		vlayout_total.setMargin(-50);

		
		
		vlayout_total.addMember(hlayout_total);
		DynamicForm dynXML=new DynamicForm();
		dynXML.setFields(new FormItem[]{txtTotalXML});
		vlayout_total.addMember(dynXML);
		
		layout_2.addMember(vlayout_total);//BORRAR
//		layout_2.addMember(textBoxTotal);//BORRAR
		layout_2.addMember(dfPie_2);
		//hLayout_6.addMember(dfAntesPie);
		//hLayout_observa.addMember(dfAntesPie);
		hlayout_totales.addMember(dfPie_totales);
		
		

		txtSubtotal.setValue(0.0);
		// cambio
		txtDescuento.setValue(0.0);
		txtDescuento.setDisabled(true);
		if(Factum.banderaImpuestoAdicional==1){
			txtimpuestoAdicional.setVisible(true);			
		}else{
			txtimpuestoAdicional.setVisible(false);
		}
		txtimpuestoAdicional.setValue(0.0);
		txtimpuestoAdicional.setDisabled(true);
		txtDescuentoNeto.setValue(0.0);
		txtBase_imponible.setValue(0.0);
		txt0_iva.setValue(0.0);
		txt14_iva.setValue(0.0);
		txtValor14_iva.setValue(0.0);
		txtTotal.setValue(0.0);
		txtEfectivo.setValue(0.0);
		txtCambio.setValue(0.0);
		txtCambio.setDisabled(true);

		
		
		// layout_2.setShowEdges(true);
		hLayout_2.addMember(layout_2);
		vLayout_pie.addMember(hLayout_2);
		vLayout_pie.addMember(hlayout_totales);
//		addMember(hLayout_2);
		addMember(vLayout_pie);
		hLayout_4.setHeight("26px");
		txtTotalXML.setVisible(false);
		txtTotalXML.hide();
		
		if(NumDto==1){
			
			//fileXML.
			txtTotalXML.setVisible(true);
			txtTotalXML.hide();
			txtTotalXML.setTitleStyle("lblFrm");
			txtTotalXML.setTextBoxStyle("textFrm");
			txtTotalXML.setHeight(30);
			txtTotalXML.setWidth(250);
			btnXML.setVisible(true);
			btnXML.addClickHandler(new Manejador("xml"));
			hLayout_6.addMember(btnXML);
		}
		btnNuevo.addClickHandler(new Manejador("nuevo"));
		
		hLayout_6.addMember(btnNuevo);
		hLayout_6.addMember(btnImprimir);
		hLayout_6.addMember(btnGrabar);
		if (NumDto == 20) {
			hLayout_6.addMember(btnTransformar);
		
			hLayout_6.addMember(btnacept);
			btnacept.setVisible(false);
			
			textBoxTotal.setVisible(true);
			
			dfAntesPie.setPosition("static");
			dfAntesPie.setWidth(25);
			
			//dfAntesPie.setBackgroundColor("#22AAFF");
			//dfAntesPie.setAlign(Alignment.LEFT);
			dfAntesPie.setFields(new FormItem[]{
					txtObservacion
			});
			
			vlayout_total.setMargin(1);
			//lFormasPago.setVisible(false);
		}
		hLayout_6.addMember(btnNuevaFactura);
		
		

				//hLayout_6.addMember(btnAsignarPuntos);
		if(NumDto==0 || NumDto==1){
			btnDocumentoElectronico.setVisible(true);//XAVIER ZEAS - DCOUMENTO ELECTRONICO FACTURA
		}						
		
		if (NumDto == 0 || NumDto == 2 || NumDto == 1 || NumDto == 10 || NumDto == 3 || NumDto == 14 || NumDto == 26 || NumDto==27) {
			hLayout_6.addMember(btnImportar);
			hLayout_6.addMember(btnExportar);
			 txtDescuento.setAttribute("readOnly",true);
			 txtimpuestoAdicional.setAttribute("readOnly",true);
			 txtDescuentoNeto.setVisible(false);
			 txtSubtotal.setAttribute("readOnly",true);
			 txtBase_imponible.setAttribute("readOnly",true);
			 txt0_iva.setAttribute("readOnly",true);
			 txt14_iva.setAttribute("readOnly",true);
			 txtValor14_iva.setAttribute("readOnly",true);
			 txtTotal.setAttribute("readOnly",true);
			 textBoxTotal.setVisible(true); 
//			 txtDireccion.setSelectionRange(0,0);
//			 if(NumDto == 0 || NumDto == 2 ){//|| NumDto == 3
//				 textBoxTotal.setVisible(true); 
//			 }else{
//				 textBoxTotal.setVisible(false); 
//			 }
		}
		
		hLayout_6.addMember(btnEliminar);
		// hLayout_6.addMember(btnValidar);
		hLayout_6.addMember(btnDocumentoElectronico);//XAVIER ZEAS - DCOUMENTO ELECTRONICO FACTURA
		
		hLayout_4.addMember(hLayout_6);
		hLayout_5.setWidth("80%");
		hLayout_4.addMember(hLayout_5);
		addMember(hLayout_4);
		grdPagos.hide();
        
		if (NumDto == 7 || NumDto == 12) {
			// Aqui cargamos el listado de ceuntas para escoger la cuenta de
			// gasto
			getService().listarCuenta(0, 2000, planCuentaID, listaCuentas);
		} else {
			getService().listarTipoprecio(0, 2000, objbacklstTipoPrecio);
			//getService().listarBodega(0, 2000, objbacklstTipoBodega);
		}
		// Primero vamos a analizar si la funcion debe mostrar un documento
		if (DocFis < 0) {
			// En este caso estamos por ingresar un documento nuevo y llamamos a
			// la funcion de carga de la base de datos
			analizarTipoDto(NumDto);
			// Llamamos a la funcion de listar los tipos de precios existentes

		} else {
			// En este caso debemos cargar el documento desde la base y
			// mostrarlo en pantalla
			analizarTipoDto(NumDto);
			getService().getDtoID(DocFis, callbackDoc);
			// Ademas se debe analizar el nivel de acceso del usuario
		}
		// buttonImprimir.addClickHandler(new Manejador("grabar"));
		btnDocumentoElectronico.addClickHandler(new Manejador("documentoelectronico"));//XAVIER ZEAS- DOCUMENTO ELECTRONICO
		btnGrabar.addClickHandler(new Manejador("grabar"));
		btnTransformar.addClickHandler(new Manejador("transformar"));
		btnacept.addClickHandler(new Manejador("aceptar"));
		// btnValidar.addClickHandler(new Manejador("validar"));
		btnNuevaFactura.addClickHandler(new Manejador("nuevaFactura"));
		// btnAsignarPuntos.addClickHandler(new Manejador("asignarPuntos"));
		btnEliminar.addClickHandler(new Manejador("borrar"));
		btnImprimir.addClickHandler(new Manejador("imprimir"));
		btnEliminar.setVisible(false);
		btnExportar.addClickHandler(new Manejador("exportar"));
		btnImportar.addClickHandler(new Manejador("importar"));
		// Obtenemos el tipo de usuario
		getService().getUserFromSession(callbackUser);

		/* /******************************************************************* */
		/* ELEMENTOS DE CARGA DE PRODUCTOS */
		getService().numeroRegistrosProducto("tblproducto" ,objbackI);

		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));

		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		txtBuscarLst.setShowTitle(false);

		cmbBuscarProducto.setLeft(158);
		cmbBuscarProducto.setTop(6);
		cmbBuscarProducto.setWidth(146);
		cmbBuscarProducto.setHeight(22);
		cmbBuscarProducto.setHint("Buscar Por");
		cmbBuscarProducto.setShowHintInField(true);
		cmbBuscarProducto.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n","Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
		cmbBuscarProducto.setShowTitle(false);
		cmbBuscarProducto.addChangedHandler(new ManejadorBotones("Bodega"));
		
		cmdBod.setLeft(321);
		cmdBod.setTop(6);
		cmdBod.setHint("Seleccione una Bodega");
		cmdBod.setVisible(false);
		cmdBod.setShowTitle(false);
		cmdBod.addChangedHandler(new ManejadorBotones("Buscar"));
		
		
		chkServicio.setValue(false);
		chkServicio.addChangedHandler(new ManejadorBotonesServicio("Servicio"));
		chkProductoElaborado.setValue(false);
		chkProductoElaborado.addChangedHandler(new ManejadorBotonesProductoElaborado("ProductoElaborado"));

		searchFormProducto.setNumCols(6);
		searchFormProducto.setFields(new FormItem[] { txtBuscarLst, cmbBuscarProducto, cmdBod, chkServicio,chkProductoElaborado });
		if(Factum.banderaMenuServicio==1){
			chkServicio.setVisible(true);
			chkServicio.setDisabled(false);
		}else{
			chkServicio.setVisible(false);
			chkServicio.setDisabled(true);
		}
		if(Factum.banderaProduccionProductos==1){
			chkProductoElaborado.setVisible(true);
			chkProductoElaborado.setDisabled(false);
		}else{
			chkProductoElaborado.setVisible(false);
			chkProductoElaborado.setDisabled(true);
		}
		
		cmbBuscarPedido.setLeft(158);
		cmbBuscarPedido.setTop(6);
		cmbBuscarPedido.setWidth(146);
		cmbBuscarPedido.setHeight(22);
		cmbBuscarPedido.setHint("Buscar Por");
		cmbBuscarPedido.setShowHintInField(true);
		cmbBuscarPedido.setValueMap("Num Pedido", "Descripci\u00F3n",
				"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
		cmbBuscarPedido.addChangedHandler(new ManejadorBotones("Bodega"));
		searchFormPedido.setItemLayout(FormLayoutType.ABSOLUTE);
		searchFormPedido
				.setFields(new FormItem[] { txtBuscarLst, cmbBuscarPedido });

		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
		btnFin.addClickHandler(new ManejadorBotones("right_all"));
		btnSiguiente.addClickHandler(new ManejadorBotones("right"));
		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones(
				"Seleccionar"));
		lstProductosElaborados.addRecordDoubleClickHandler(new ManejadorBotones(
		"Seleccionar"));
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
		//buscarPicker.addFormItemClickHandler(new ManejadorBotones(""));
		hStack.addMember(btnInicio);
		hStack.addMember(btnAnterior);
		hStack.addMember(btnSiguiente);
		hStack.addMember(btnFin);
		hStack.addMember(btnEliminarlst);

		ListGridField lstcodbarras = new ListGridField("codigoBarras",
				"C\u00F3digo de Barras", 100);
		lstcodbarras.setCellAlign(Alignment.LEFT);
		ListGridField lstdescripcion = new ListGridField("descripcion",
				"Descripci\u00F3n", 350);
		lstdescripcion.setCellAlign(Alignment.LEFT);
		ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",
				100);
		lstimpuesto.setCellAlign(Alignment.CENTER);
		ListGridField lstlifo = new ListGridField("lifo", "LIFO", 60);
		lstlifo.setCellAlign(Alignment.CENTER);
		ListGridField lstfifo = new ListGridField("fifo", "FIFO", 60);
		lstfifo.setCellAlign(Alignment.CENTER);
		ListGridField lstpromedio = new ListGridField("promedio", "Costo", 60);
		lstpromedio.setCellAlign(Alignment.CENTER);
		//SC.say(String.valueOf(tipoUsuario));
		if(tipoUsuario==1){
			lstpromedio.setHidden(false);
			lgfPrecio_unitario.setCanEdit(true);
			lgfPrecio_iva.setCanEdit(true);
		}else{
			lstpromedio.setHidden(true);
			lgfPrecio_unitario.setCanEdit(false);
			lgfPrecio_iva.setCanEdit(false);
		}
		ListGridField lstunidad = new ListGridField("Unidad", "Unidad", 100);
		ListGridField listGridField_6 = new ListGridField("stock", "Stock", 50);
		listGridField_6.setCellAlign(Alignment.CENTER);
		ListGridField lstcategoria = new ListGridField("Categoria",
				"Categoria", 100);
		lstcategoria.setCellAlign(Alignment.CENTER);
		ListGridField lstMarca = new ListGridField("Marca", "Marca", 100);
		lstMarca.setCellAlign(Alignment.CENTER);
		
		lstPrecio = new ListGridField("Precio", "Precio", 60);
		lstPrecio.setCellAlign(Alignment.CENTER);
		
		ListGridField buttonField = new ListGridField("buttonField", "Series",
				100);
		ListGridField lstidproducto = new ListGridField("idProducto", "IdProducto",
				50);
		lstidproducto.setHidden(true);
		ListGridField lstcantidad = new ListGridField("cantidad", "Cantidad",
				50);
		lstcantidad.setCellAlign(Alignment.CENTER);
		lstcantidad.setType(ListGridFieldType.FLOAT);
		lstcantidad.addChangeHandler(new Manejador("agregarGrilla"));
		lstcantidad.addEditorExitHandler(new Manejador("agregarGrilla"));
		lstcantidad.setCanEdit(true);
		lstcantidad.setDefaultValue(0);// GIGACOMPUTERS
		
		
		ListGridField lstcantidadProductoPedido = new ListGridField("cantidad", "Cantidad",
				50);
		lstcantidadProductoPedido.setCellAlign(Alignment.CENTER);
		lstcantidadProductoPedido.setType(ListGridFieldType.FLOAT);
		lstcantidadProductoPedido.addChangeHandler(new Manejador("agregarGrillaProductoPedido"));
		lstcantidadProductoPedido.addEditorExitHandler(new Manejador("agregarGrillaProductoPedido"));
		lstcantidadProductoPedido.setCanEdit(true);
		lstcantidadProductoPedido.setDefaultValue(0);// GIGACOMPUTERS
		
		ListGridField lstobservaciones = new ListGridField("observacion",
				"Observaciones", 250);
		lstobservaciones.addChangeHandler(new Manejador("agregarObservacion"));
		lstobservaciones.addEditorExitHandler(new Manejador(
				"agregarObservacion"));
		lstobservaciones.setCanEdit(true);
		ListGridField lstDESC = new ListGridField("DTO", "DTO %", 40);
		lstDESC.setCellAlign(Alignment.CENTER);
		lstDESC.addEditorExitHandler(new Manejador("agregarGrilla"));
		ListGridField inventarioInicial = new ListGridField("inicial",
				"I. Inicial", 50);
		inventarioInicial.setCellAlign(Alignment.CENTER);
		ListGridField lstpadre2 = new ListGridField("stockPadre", "Padre M.P",
				60);
		ListGridField lstpadre = new ListGridField("Padre", "Padre", 60);
		ListGridField lstjerarquia = new ListGridField("jerarquia", "Jerarquia", 60);
		lstProductos.setFields(new ListGridField[] { // GIGACOMPUTERS
						lstidproducto,lstcantidad, lstcodbarras, lstdescripcion, lstDESC,
						lstobservaciones, lstPrecio,listGridField_6, inventarioInicial,
						lstpadre2,
						lstimpuesto, lstpromedio, lstunidad, lstpadre,lstjerarquia });
		
		lstProductosElaborados.setFields(new ListGridField[] { // GIGACOMPUTERS
				lstcantidadProductoPedido, lstcodbarras, lstdescripcion, lstDESC,
						lstobservaciones, lstPrecio,listGridField_6, inventarioInicial,
						lstpadre2,
						lstimpuesto, lstpromedio, lstunidad, lstpadre,lstjerarquia});
		// layout_1.addMember(lstProductos);
		// layout_1.addMember(lblRegisros);

		// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");

		/***********************************************************************/
		grdProductos.setCanFocus(true);
		 
		// ;x
		 
	
		
		//cmdBod.disable();
		 switch(Factum.banderaElegirVendedor){
		 case 0:
			 cmbVendedor.setDisabled(true);
	     break;
		 case 1:
			 cmbVendedor.setDisabled(false);
		 break;
		 }
		 
	
		
	}
	
	
	
	public void precargarDatosOrdenTaller(OrdenDTO orden,ClienteDTO cliente){
		
		cargarcamposcliente(cliente.getTblpersona().getCedulaRuc(),0);
		
		String xxreporte = orden.getReporte();
		String xprecioSugerido =  xxreporte.substring(xxreporte.lastIndexOf("#")+1, xxreporte.length());
		try{
			Double p = Double.parseDouble(xprecioSugerido);
			this.precioSugeridoServicioTecnico = p;
		}
		catch(NumberFormatException e){
			SC.say("ATENCION! No se pudo cargar el precio sugerido. Se ha asignado 1$");
			this.precioSugeridoServicioTecnico = 1D;
		}
		
		mostrar_productosOrdenes();
		
		
	}
	
	final AsyncCallback<RetencionDTO> objbackDtoRet = new AsyncCallback<RetencionDTO>() {
		// String coincidencias = "";
		public void onSuccess(RetencionDTO result) {
			if (result != null) {
				String ct=String.valueOf(Integer.valueOf(result.getNumRealRetencion().split("-")[2])+1);
				int cont=9-ct.length();
				while (cont>0)	{ct="0"+ct;cont--;}
				emision+="-"+ct.toString();
				lastNumRealRet=result.getTbldtocomercialByIdDtoComercial().getNumRealTransaccion()+1;
			} else {
				// Aqui analizamos si es diferente de gasto
				emision+="-"+"000000001";
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};
	final AsyncCallback<String> dynCallback = new AsyncCallback<String>() {
		// String coincidencias = "";
		public void onSuccess(String result) {
			com.google.gwt.user.client.Window.alert("dynamic xml return "+result);
		}
		
		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};
	
	// Funci�n para analizar el tipo de documento, mostrar los widgets
	// necesarios y cargar los datos
	// respectivos
	public void analizarTipoDto(Integer dto) {
		PuntoEmisionDTO punto;
		String filtro = "where TipoTransaccion =", ordenamiento;
		filtro += dto + "";
		// ANALIZAMOS LAS FORMAS DE PAGO PARA VER QUE ELEMENTOS OCULTAMOS
		txtEfectivo.setVisible(false);
		txtCambio.setVisible(false);
		String campo="numRealTransaccion";
		switch (dto) {
		case 0:// Factura de venta
			txtCLiente.setTitle("CLIENTE");
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);
			afeccion = 0;
			txtEfectivo.setVisible(true);
			txtCambio.setVisible(true);
			if (Factum.banderaModificarNumFactura!=1){
				campo="iddtocomercial";
			}
			
			break;
		case 1:// Factura de compra
			punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			emision=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			txtCLiente.setTitle("PROVEEDOR");
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);

			afeccion = 1;
			getService().ultimaRetencion("where tbldtocomercialByIdDtoComercial.tipoTransaccion = "
//					+ "'"+19+"' "
					,19
					, "tbldtocomercialByIdDtoComercial.idDtoComercial", objbackDtoRet);
//					, "idRetencion", objbackDtoRet);
			break;
		case 2:// Nota de entrega
			afeccion = 0;
			serie = "NOTA DE ENTREGA # ";
			txtValor14_iva.setVisible(false);
			txtEfectivo.setVisible(true);
			txtCambio.setVisible(true);
			if (Factum.banderaModificarNumNota!=1){
				campo="iddtocomercial";
			}
			break;
		case 3:// Nota de Credito
			punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			emision=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			txtCLiente.setTitle("CLIENTE");
			afeccion = 1;
			serie = "NOTA DE CREDITO #";
			// **
			// ESTO SE DEBE HACER PREGUNTANDO SI EL ITEM VUELVE O NO
			// **
		    txtCorresp.setVisible(true);
		    txtCorresp.setRequired(true);
		    
		    txtMotivo.setVisible(true);
		    txtMotivo.setRequired(true);
		    if (Factum.banderaModificarNumNotaC!=1){
				campo="iddtocomercial";
			}
			break;
		case 4:// Anticipo de cliente
			afeccion = 0;
			serie = "ANTICIPO #";
			txtValor14_iva.setVisible(false);
			txtSubtotal.setVisible(false);
			cmbTipoPrecio.setVisible(false);
			break;
		case 5:// Retencion
			txtCLiente.setTitle("PROVEEDOR");
			afeccion = 1;
			break;
		
		case 7:// Gasto
			txtCLiente.setTitle("PROVEEDOR");
			afeccion = 1;
			serie = "GASTO #";
			txt14_iva.setDisabled(true);
			txtTotal.setDisabled(true);
			break;
		case 8:// Ajuste de Inventario
			txtCLiente.setTitle("PROVEEDOR");
			afeccion = 0;
			break;
		case 10://Nota de compra

			txtCLiente.setTitle("PROVEEDOR");
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);
			txtValor14_iva.setVisible(false);
			afeccion = 1;
			break;
		case 12:// Ingreso

			txtCLiente.setTitle("CLIENTE");
			afeccion = 0;
			serie = "INGRESO #";
			txt14_iva.setDisabled(true);
			txtTotal.setDisabled(true);
			break;
		case 13:// Factura de venta Grande

			txtCLiente.setTitle("CLIENTE");
			txtMotivo.setVisible(false);
			afeccion = 0;
			break;
		case 14:// Nota de Credito Proveedor
			punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			emision=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			txtCLiente.setTitle("PROVEEDOR");
			afeccion = 0;
			serie = "NOTA DE CREDITO A PROVEEDOR#";
			// **
			// ESTO SE DEBE HACER PREGUNTANDO SI EL ITEM VUELVE O NO
			// **
			txtCorresp.setRequired(true);
			txtCorresp.setVisible(true);
			
			txtMotivo.setRequired(true);
			txtMotivo.setVisible(true);
			
			break;
		case 15:// Anticipo de proveedor
			txtCLiente.setTitle("PROVEEDOR");
			afeccion = 1;
			serie = "ANTICIPO PROVEEDOR #";
			txtValor14_iva.setVisible(false);
			txtSubtotal.setVisible(false);
			cmbTipoPrecio.setVisible(false);
			break;

		case 20:
			txtCLiente.setTitle("CLIENTE");
			afeccion = 0;// Para que salga el listado de clientes
			// serie= "ANTICIPO PROVEEDOR #";
			cmbTipoPrecio.setVisible(true);
			break;
			
		case 26://Venta Nota de venta
			txtCLiente.setTitle("CLIENTE");
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);
			afeccion = 0;
			txtEfectivo.setVisible(true);
			txtCambio.setVisible(true);
			cmbTipoPrecio.setVisible(true);
			txtValor14_iva.setVisible(false);
			if (Factum.banderaModificarNumNotaV!=1){
				campo="iddtocomercial";
			}
			break;
		
		case 27://Compra con nota de venta
			punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			emision=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			txtCLiente.setTitle("PROVEEDOR");
			txtCorresp.setVisible(false);
			txtMotivo.setVisible(false);
			cmbTipoPrecio.setVisible(true);
			txtValor14_iva.setVisible(false);
			afeccion = 1;
			break;
		}

		if (DocFis < 0)
			obtener_UltimoDto(filtro, campo);
				
		// obtener_FormasdePago(dto);
	}

	
	public void limpiarpagos(){
		VLayout lFormasPago = new VLayout();
		hLayout_2.removeMember(hLayout_2);
		hLayout_2 =  new HLayout();
		HLayout lFormas = new HLayout();
		lFormas.addMember(dynamicForm);
		lFormas.addMember(grdPagos);
		lFormas.addMember(dynamicForm2);
		lFormas.addMember(dynamicForm3);
		
		lFormasPago.addMember(lFormas);
		hLayout_2.addMember(lFormasPago);
		//hLayout_2.addMember(dfPie_1);
		hLayout_2.addMember(layout_2);
		addMember (hLayout_2);
		reorderMember(5, 4);
		txtTelefono.setValue(hLayout_2.getID());
		redraw ();
		for (Canvas cv : getMembers()  ){
			cv.getID();
			txtDireccion.setValue(txtDireccion.getValue()+cv.getID());
		}
		
		
	}
	
	public void limpiarInterfaz() {
		//com.google.gwt.user.client.Window.alert("En limpieza");
		if (NumDto == 0) {
//			//com.google.gwt.user.client.Window.alert("En limpieza factura de venta");
			cp.facturar();
			//txtObservacion.setValue("");
		} else if (NumDto == 1) {
//			//com.google.gwt.user.client.Window.alert("En limpieza factura de compra");
			cp.facturaCompra();
		} else if (NumDto == 2) {
//			//com.google.gwt.user.client.Window.alert("En limpieza nota de entrega");
			cp.notaEntrega();
		} else if (NumDto == 20) {
//			//com.google.gwt.user.client.Window.alert("En limpieza nota de entrega");
			cp.proforma();
		} else if (NumDto == 26) {
//			//com.google.gwt.user.client.Window.alert("En limpieza nota de entrega");
			cp.notaVentas();
		} else if (NumDto == 27) {
//			//com.google.gwt.user.client.Window.alert("En limpieza nota de entrega");
			cp.notaVentasCompras();
		}else {
			for (ListGridRecord rec : grdProductos.getRecords()) {
				grdProductos.removeData(rec);
			}
//			//com.google.gwt.user.client.Window.alert("Despues de carga nueva remove grid");
			grdPagos.hide();
//			//com.google.gwt.user.client.Window.alert("hide ");
			codigoSeleccionado = 1;
			txtRuc_cliente.setValue("9999999999999");
			txtCLiente.setValue("CONSUMIDOR FINAL");
			txtDireccion.setValue(" ");
			txtCorreo.setValue(" ");
			txtTelefono.setValue(" ");
//			//com.google.gwt.user.client.Window.alert("Datos base client");
			txtSubtotal.setValue(0.0);
			txtDescuento.setValue(0.0);
			txtimpuestoAdicional.setValue(0.0);
			txtDescuentoNeto.setValue(0.0);
			txtBase_imponible.setValue(0.0);
			txt0_iva.setValue(0.0);
			txt14_iva.setValue(0.0);
			txtValor14_iva.setValue(0.0);
			txtTotal.setValue(0.0);
			txtEfectivo.setValue(0.0);
			txtCambio.setValue(0.0);
			txtObservacion.setValue("");
			//txtObservacion.setDefaultValue("");
			if (NumDto == 3 || NumDto == 14) {
				txtCorresp.setValue("000-000-000000000");
				txtMotivo.setValue("");
			}
//			//com.google.gwt.user.client.Window.alert("Textos totales");
			rowNumGlobal = 0;
			dynamicForm.getField("chkContado").setValue(false);
			dynamicForm.getField("txtContado").setValue("");
			dynamicForm.getField("txtContado").hide();
//			//com.google.gwt.user.client.Window.alert("contado");
			dynamicForm.getField("chkCredito").setValue(false);
			dynamicForm.getField("txtCredito").setValue("");
			dynamicForm.getField("txtCredito").hide();
//			//com.google.gwt.user.client.Window.alert("credito");
			if (NumDto == 0 || NumDto == 1) {
				dynamicForm2.getField("chkRetencion").setValue(false);
				dynamicForm2.getField("txtRetencion").setValue("");
				dynamicForm2.getField("txtRetencion").hide();
			}
			dynamicForm2.getField("chkAnticipo").setValue(false);
			dynamicForm2.getField("txtAnticipo").setValue("");
			dynamicForm2.getField("txtAnticipo").hide();
//			//com.google.gwt.user.client.Window.alert("anticipo");
			dynamicForm2.getField("chkNotaCredito").setValue(false);
			dynamicForm2.getField("txtNotaCredito").setValue("");
			dynamicForm2.getField("txtNotaCredito").hide();
//			//com.google.gwt.user.client.Window.alert("nota de credito");
			dynamicForm3.getField("chkTarjetaCr").setValue(false);
			dynamicForm3.getField("txtTarjetaCr").setValue("");
			dynamicForm3.getField("txtTarjetaCr1").setValue("");
			dynamicForm3.getField("txtTarjetaCr").hide();
			dynamicForm3.getField("txtTarjetaCr1").hide();
//			//com.google.gwt.user.client.Window.alert("Tarjeta de credito");
			dynamicForm3.getField("chkBanco").setValue(false);
			dynamicForm3.getField("txtBanco").setValue("");
			dynamicForm3.getField("txtBanco1").setValue("");
			dynamicForm3.getField("txtBanco").hide();
			dynamicForm3.getField("txtBanco1").hide();
//			//com.google.gwt.user.client.Window.alert("Banco");
			// dynamicForm.redraw();
			Integer nuevoNumeroDocumento = Integer.parseInt(txtNumero_factura.getValue().toString());
			nuevoNumeroDocumento = nuevoNumeroDocumento + 1;
			txtNumero_factura.setValue(nuevoNumeroDocumento.toString());
//			//com.google.gwt.user.client.Window.alert("numero de documento");
			txtNumero_factura.redraw();
			// limpiarpagos ();
			btnGrabar.setDisabled(false);
		}
	}

	public void calculo_Subtotal_Individual(String ValorActual, String indicador) {
		Double Subt = 0.0;
		Integer rowNum = grdProductos.getEditRow();
		double aux;
		double auxIva = 0.0;
		//System.out.println(indicador+" :"+ValorActual);
		if (indicador.equals("pvp")) {
			try {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				/*SC.say(String.valueOf(Float.parseFloat(ValorActual))+", "+String.valueOf(rowNum));
				SC.say(String.valueOf(Float.parseFloat(grdProductos.getEditValue(rowNum,
				"cantidad").toString())));
				SC.say(String.valueOf((1-(Float.parseFloat(grdProductos.getEditValue(rowNum,
				"descuento").toString())/100))));*/
				aux = Float.parseFloat(ValorActual)
						* Float.parseFloat(grdProductos.getEditValue(rowNum,
								"cantidad").toString())
								*(1-(Float.parseFloat(grdProductos.getEditValue(rowNum,
										"descuento").toString())/100));
				//SC.say(String.valueOf(aux)+" ,"+String.valueOf(grdProductos.getEditRow()));
				/*aux = aux
						+ (aux
								* Float.parseFloat(grdProductos.getEditValue(
										rowNum, "iva").toString()) / 100);*/
				//SC.say("AUX: "+String.valueOf(aux)+" ,"+String.valueOf(grdProductos.getEditRow()));
				// +++++++++++++++++++++PARA PRECIO CON IVA
				// +++++++++++++++++++++++++++++++++
				auxIva = Double.parseDouble(ValorActual);
				
				String[] ivas;
				String cadIva=grdProductos.getEditValue(rowNum, "iva").toString();
				ivas=cadIva.split(",");
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+((auxIva* (1 - (Float.parseFloat(grdProductos.getEditValue(rowNum,
							"descuento").toString())/100))+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				
				auxIva=auxIva+impuestoValor;
				
//				auxIva = auxIva
//						+ (auxIva
//								* Float.parseFloat(grdProductos.getEditValue(
//										rowNum, "iva").toString()) / 100);
//				auxIva=auxIva*(1-(Float.parseFloat(grdProductos.getEditValue(rowNum,
//				"descuento").toString())/100));
				//SC.say("AUXIVA: "+String.valueOf(auxIva)+" ,"+String.valueOf(grdProductos.getEditRow()));
			} catch (Exception e) {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				aux = Float.parseFloat(ValorActual)
						* Float.parseFloat(grdProductos.getRecord(rowNum)
								.getAttribute("cantidad"))
								*(1-(Float.parseFloat(grdProductos.getRecord(rowNum)
										.getAttribute("descuento"))/100));
				// +++++++++++++++++++++PARA PRECIO CON IVA
				// +++++++++++++++++++++++++++++++++
				auxIva = Double.parseDouble(ValorActual);
				
				String[] ivas;
				String cadIva=grdProductos.getRecord(rowNum).getAttribute("iva");
				ivas=cadIva.split(",");
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+((auxIva* (1 - (Float.parseFloat(grdProductos.getRecord(rowNum).getAttribute(
							"descuento"))/100))+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				
				auxIva=auxIva+impuestoValor;
				
//				auxIva = auxIva
//						+ (auxIva
//								* Float.parseFloat(grdProductos.getRecord(
//										rowNum).getAttribute("iva")) / 100);
//				auxIva=auxIva*(1-(Float.parseFloat(grdProductos.getRecord(rowNum).getAttribute(
//				"descuento").toString())/100));
			}
			// +++++++++++ PARA PRECIO CON IVA ++++++++
			if (NumDto!=2 && NumDto!=10 && NumDto!=26 && NumDto!=27){
			auxIva = CValidarDato.getDecimal(5, auxIva);
			grdProductos.setEditValue(rowNum, 10, auxIva);
			grdProductos.refreshRow(rowNum);
			}
			}else {// cantidad
			try {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				aux = Float.parseFloat(grdProductos.getEditValue(rowNum,
						"precioUnitario").toString())
						* Float.parseFloat(ValorActual)
						*(1-(Float.parseFloat(grdProductos.getEditValue(rowNum,
										"descuento").toString())/100));
				/*aux = aux
						+ (aux
								* Float.parseFloat(grdProductos.getEditValue(
										rowNum, "iva").toString()) / 100);*/
			} catch (Exception e) {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				aux = Float.parseFloat(grdProductos.getRecord(rowNum)
						.getAttribute("precioUnitario"))
						* Float.parseFloat(ValorActual)
						*(1-(Float.parseFloat(grdProductos.getRecord(rowNum)
								.getAttributeAsString("descuento"))/100));
				/*aux = aux
				+ (aux
						* Float.parseFloat(grdProductos.getRecord(rowNum).getAttributeAsString(
								"iva")) / 100);*/
			}
		}
		if (indicador.equals("precioConIva")) {
			try {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				auxIva = Double.parseDouble(ValorActual);
				
				String[] ivas;
				String cadIva=grdProductos.getEditValue(rowNum, "iva").toString();
				ivas=cadIva.split(",");
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+
							((auxIva
//									* (1 - (Float.parseFloat(grdProductos.getEditValue(rowNum,	"descuento").toString())/100))
									+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				
				auxIva=auxIva+impuestoValor;
				
//				auxIva = auxIva
//						/ (1 + (Float.parseFloat(grdProductos.getEditValue(
//								rowNum, "iva").toString()) / 100));
				/*aux = auxIva
						* Float.parseFloat(grdProductos.getEditValue(rowNum,
								"cantidad").toString());*/
				// +++++++++++++++++++++PARA PRECIO CON IVA
				// +++++++++++++++++++++++++++++++++

			} catch (Exception e) {
				// +++++++++++++++++++++PARA SUBTOTAL
				// +++++++++++++++++++++++++++++++++
				auxIva = Double.parseDouble(ValorActual);
				
				String[] ivas;
				String cadIva=grdProductos.getRecord(rowNum).getAttribute("iva");
				ivas=cadIva.split(",");
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+((auxIva
//							* (1 - (Float.parseFloat(grdProductos.getRecord(rowNum).getAttribute("descuento"))/100))
							+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				
				auxIva=auxIva+impuestoValor;
				
//				auxIva = auxIva
//						/ (1 + (Float.parseFloat(grdProductos.getRecord(rowNum)
//								.getAttribute("iva")) / 100));
				/*aux = auxIva
						* Float.parseFloat(grdProductos.getRecord(rowNum)
								.getAttribute("cantidad"));*/
				// +++++++++++++++++++++PARA PRECIO CON IVA
				// +++++++++++++++++++++++++++++++++
			}
			auxIva = CValidarDato.getDecimal(5, auxIva);
			grdProductos.setEditValue(rowNum, 5, auxIva);
			grdProductos.refreshRow(rowNum);

		}

		//Subt= Double.parseDouble(aux.toString());
		//Subt=aux;
		//SC.say(grdProductos.getRecord(rowNum).getAttribute("descuento").toString());
		//SC.say(String.valueOf(aux));
		Subt = aux;
		Subt = CValidarDato.getDecimal(5, Subt);
		int colNum = (NumDto==2 || NumDto==10 || NumDto == 26 || NumDto == 27)?8:9;
		grdProductos.setEditValue(rowNum, colNum, Subt);
		grdProductos.refreshRow(rowNum);
	}

	public void calculo_Individual_Total() {

		try {
		
			double Subt = 0.0;
			grdProductos.saveAllEdits();
			double aux;
			double auxIva = 0.0;
			Integer rowNum = 0;
			//String mensaje="";
			for (ListGridRecord rec : grdProductos.getRecords()) {
				aux = Float.parseFloat(rec.getAttributeAsString("precioUnitario").replace("$", ""))* Float.parseFloat(rec.getAttributeAsString("cantidad").replace("$", ""));
				// aux=aux+(aux*Float.parseFloat(rec.getAttributeAsString("iva"))/100);
				// +++++++++++++++++++++PARA PRECIO CON IVA
				// +++++++++++++++++++++++++++++++++
				auxIva = Double.parseDouble(rec.getAttributeAsString("precioUnitario").replace("$", ""));
				if (NumDto!=2 && NumDto!=10 && NumDto!=26 && NumDto!=27){
					
					//mensaje=mensaje+" VALOR DEL PVP "+auxIva;
					String[] ivas;
					String cadIva=rec.getAttributeAsString("iva").replace("$", "");
					ivas=cadIva.split(",");
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((auxIva* (1 - (Float.parseFloat(rec.getAttributeAsString("descuento"))/100))+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					auxIva = auxIva+ (auxIva* Float.parseFloat(rec.getAttributeAsString("iva").replace("$", "")) / 100)
//					- (auxIva* Float.parseFloat(rec.getAttributeAsString("descuento")) / 100);
					auxIva = auxIva+impuestoValor;
					
					auxIva = CValidarDato.getDecimal(5, auxIva);
					grdProductos.setEditValue(rowNum, 10, auxIva);
				}
				
				Subt = CValidarDato.getDecimal(5,aux);
				grdProductos.setEditValue(rowNum, 5, Double.parseDouble(rec.getAttributeAsString("precioUnitario").replace("$", "")));
				Subt = aux*(1-(Float.parseFloat(grdProductos.getRecord(rowNum).getAttributeAsString("descuento").replace("$", ""))/100));
				Subt = CValidarDato.getDecimal(5, Subt);
				int colNum = (NumDto==2 || NumDto==10 || NumDto==26 || NumDto==27)?8:9;
				grdProductos.setEditValue(rowNum, colNum, Subt);
				grdProductos.refreshRow(rowNum);
				rowNum++;
			}
			//SC.say(mensaje);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			calculo_Subtotal();
		} catch (Exception e) {
			SC.say("Error total."+e.getMessage());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
	}

	public void calculo_Subtotal() {
		costo = 0.0;
		Double costoIndividual = 0.0;
		grdProductos.saveAllEdits();
		String s = txtDescuentoNeto.getValue().toString();
		try {
			ListGridRecord[] selectedRecords = grdProductos.getRecords();
			Subtotal = 0.0;
			descuentoGlobal = 0.0;
			impuestoadicionGlobal = 0.0;
			subtotalIva14 = 0.0;
			valoresIva=0.0;
			subtotalIva0 = 0.0;
			int iva = 0;
			String[] ivas;
			Double desc = 1.0;
			Double descS = 1.0;
			String descSucesivo = "";
			Double descuentoNeto = 0.0;
			String reemplazo = "";
			
			try {
				for (ListGridRecord rec : selectedRecords) {
					descS = 1.0;
					costoIndividual = rec.getAttributeAsDouble("costo")
							* rec.getAttributeAsDouble("cantidad");
					
					try {
						Double valTotal=rec.getAttributeAsDouble("precioUnitario")
						* rec.getAttributeAsDouble("cantidad");
						Subtotal += valTotal;
						try {

							descSucesivo = rec.getAttributeAsString("descuento");
							
							if(descSucesivo == "100"){
								rec.setAttribute("descuento", 99.99);
								//SC.say("El valor maximo de descuento debe ser de 99.99");
							}
							 
							else{
								
							
							String[] descuentos = descSucesivo.split("\\+");
							Double desParcial = 0.0;
							for (int i = 0; i < descuentos.length; i++) {
							
								desParcial = Double.parseDouble(descuentos[i]);
								desParcial = (100 - desParcial) / 100;
								descS = descS * desParcial;
							}
							descS = 1 - descS;
							descuentoGlobal += valTotal * descS;
							}
						} catch (Exception des) {
							descuentoGlobal += 0.0;
							descS = 0.0;
							SC.say("Error: calculo descuento" + des);
						}
						if (NumDto == 2) {// was || NumDto == 3
							if (ivaNota > 0.0) {
								try {
//									iva = rec.getAttributeAsInt("iva");
									String cadIva=rec.getAttribute("iva");
									ivas=cadIva.split(",");
									String impuestos="";
									double impuestoPorc=1.0;
									double impuestoValor=0.0;
									int i1=0;
									
									
									desc = 0.0;
									if (descS > 0)
										desc = descS;
									
									//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
									for (String ivaS:ivas){
										i1=i1+1;
										impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
										impuestoValor=impuestoValor+((valTotal* (1 - desc)+impuestoValor)*(Double.parseDouble(ivaS)/100));
									}
									//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
									// desc =
									// rec.getAttributeAsDouble("descuento");
									// { aa
//									if (iva == Factum.banderaIVA) {
									if ((impuestoPorc-1) >0) {
										subtotalIva14 += (valTotal * (1 - desc));
										valoresIva=valoresIva+impuestoValor;
									} else {
										subtotalIva0 += (valTotal * (1 - desc));
									}
									// }
								} catch (Exception iv) {
									subtotalIva14 += 0.0;
									subtotalIva0 += 0.0;
								}
							}
						}
						if (NumDto == 0 || NumDto == 1 || NumDto == 13
								|| NumDto == 14 || NumDto == 20 || NumDto == 3
								|| CalculoIVA == true) {//|| NumDto == 3
							// CALCULAR EL IVA SOLO EN CASO DE COMPRA, VENTA Y
							// NOTA DE CREDITO
							try {
//								iva = rec.getAttributeAsInt("iva");
								String cadIva=rec.getAttribute("iva");
								ivas=cadIva.split(",");
								String impuestos="";
								double impuestoPorc=1.0;
								double impuestoValor=0.0;
								int i1=0;
								
								
								desc = 0.0;
								if (descS > 0)
									desc = descS;
								
								//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
								for (String ivaS:ivas){
									i1=i1+1;
									impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
									impuestoValor=impuestoValor+((valTotal* (1 - desc)+impuestoValor)*(Double.parseDouble(ivaS)/100));
								}
								//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
//								if (iva == Factum.banderaIVA) {
								if ((impuestoPorc-1) >0) {
									subtotalIva14 += (valTotal * (1 - desc));
									valoresIva=valoresIva+impuestoValor;
									//SC.say("IVA "+iva +"=="+ Factum.banderaIVA);
								} else {
									subtotalIva0 += (valTotal * (1 - desc));
								}
							} catch (Exception iv) {
								subtotalIva14 += 0.0;
								subtotalIva0 += 0.0;
							}
						}
					} catch (Exception e) {
						Subtotal += 0.0;
					}
					costo += costoIndividual;
				}
				costo = CValidarDato.getDecimal(2, costo);
				Subtotal = CValidarDato.getDecimal(2, Subtotal);
				// descuentoGlobal = descuentoGlobal/100;
				descuentoGlobal = CValidarDato.getDecimal(2, descuentoGlobal);
				impuestoadicionGlobal = CValidarDato.getDecimal(2, impuestoadicionGlobal);
				NumberFormat formato = NumberFormat.getFormat("###0.00");
				txtSubtotal.setValue(formato.format(Subtotal));
				txtDescuento.setValue(formato.format(descuentoGlobal));
				txtimpuestoAdicional.setValue(formato.format(impuestoadicionGlobal));
				descuentoNeto = Double.parseDouble(txtDescuentoNeto
						.getDisplayValue());
				txtBase_imponible.setValue(formato.format(Subtotal
						- descuentoGlobal - descuentoNeto));
				NumberFormat formato2 = NumberFormat.getFormat("#.00");
				// subtotalIva0=subtotalIva0/100;
				txt0_iva.setValue(formato.format(subtotalIva0));
				// subtotalIva12=Double.parseDouble(txtBase_imponible.getDisplayValue());
				//subtotalIva12 = subtotalIva12 - descuentoNeto;
				txt14_iva.setValue(formato.format(subtotalIva14));
//				txtValor14_iva.setValue(formato.format(subtotalIva14 * (Factum.banderaIVA/100)));
//				// cAMBIO
//				txtValor14_iva.setValue(valoresIva);
				txtValor14_iva.setValue(formato.format(valoresIva));
				if (NumDto == 0 || NumDto == 1 || NumDto == 13 || NumDto == 14 || NumDto == 3
						|| NumDto == 20 || CalculoIVA == true ) {//|| NumDto == 3
//					total = (subtotalIva14 * (1+(Factum.banderaIVA/100))) + subtotalIva0;// +
																	// Double.parseDouble(txtBase_imponible.getDisplayValue());
					total = (subtotalIva14 +valoresIva) + subtotalIva0;
					total = CValidarDato.getDecimal(2, total);
					txtTotal.setValue(formato.format(total));
					
					textBoxTotal.setContents(formato.format(total));//BORRAR
					textBoxTotal.markForRedraw();//BORRAR
					
					txtEfectivo.setValue(formato.format(total));
				} else {
					if (NumDto == 2 ) {//was || NumDto == 3
						/*if(NumDto == 2){
							total = Subtotal;
							textBoxTotal.setContents(txtTotal.getValue().toString());//BORRAR
							textBoxTotal.markForRedraw();//BORRAR
						}*/
						if (ivaNota > 0.0) {
							total = 
//									subtotalIva14* (Factum.banderaIVA/100)
									valoresIva
									+ subtotalIva0
									+ Double.parseDouble(txtBase_imponible
											.getDisplayValue());
							total = CValidarDato.getDecimal(2, total);
							txtTotal.setValue(formato.format(total));
							txtEfectivo.setValue(formato.format(total));
							// ivaNota=0.0;
						} else {
							total = Double.parseDouble((String)txtBase_imponible.getValue()
//									.getDisplayValue()
									);
							total = CValidarDato.getDecimal(2, total);
							txtTotal.setValue(formato.format(total));
							txtEfectivo.setValue(formato.format(total));
						}
						textBoxTotal.setContents(formato.format(total));//BORRAR
						textBoxTotal.markForRedraw();//BORRAR
					}

					if (NumDto == 10 || NumDto == 20 || NumDto == 26 || NumDto == 27) {
						total = Double.parseDouble((String)txtBase_imponible.getValue()
//								.getDisplayValue()
								);
						total = CValidarDato.getDecimal(2, total);
						txtTotal.setValue(formato.format(total));
						textBoxTotal.setContents(formato.format(total));//BORRAR
						textBoxTotal.markForRedraw();//BORRAR
						
						txtEfectivo.setValue(formato.format(total));
					}

				}
				if (NumDto == 0 || NumDto == 2 || NumDto == 26) {
					boolean aux=false;
					if (NumDto ==0){
						if (!(Boolean) dynamicForm.getField("chkContado")
								.getValue() 
								&& !(Boolean)dynamicForm.getField("chkCredito").getValue()&&
								!(Boolean)dynamicForm2.getField("chkRetencion").getValue()&&
								!(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
								!(Boolean)dynamicForm2.getField("chkNotaCredito").getValue()&&
								!(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
								!(Boolean)dynamicForm3.getField("chkBanco").getValue()){
							dynamicForm.getField("chkContado").setValue(true);
							dynamicForm.getField("txtContado").setValue(formato.format(total));
//							dynamicForm.getField("txtContado").setVisible(true);
							dynamicForm.getField("txtContado").show();
							aux=true;
						}
					}else if (NumDto == 26){
						if (!(Boolean) dynamicForm.getField("chkContado")
								.getValue() 
								&& !(Boolean)dynamicForm.getField("chkCredito").getValue()&&
								!(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
								!(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
								!(Boolean)dynamicForm3.getField("chkBanco").getValue()){
							dynamicForm.getField("chkContado").setValue(true);
							dynamicForm.getField("txtContado").setValue(formato.format(total));
//							dynamicForm.getField("txtContado").setVisible(true);
							dynamicForm.getField("txtContado").show();
							
							aux=true;
						}
					}else{
						if (!(Boolean) dynamicForm.getField("chkContado")
								.getValue() 
								&& !(Boolean)dynamicForm.getField("chkCredito").getValue()&&
								!(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
								!(Boolean)dynamicForm2.getField("chkNotaCredito").getValue()&&
								!(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
								!(Boolean)dynamicForm3.getField("chkBanco").getValue()){
							dynamicForm.getField("chkContado").setValue(true);
							dynamicForm.getField("txtContado").setValue(formato.format(total));
//							dynamicForm.getField("txtContado").setVisible(true);
							dynamicForm.getField("txtContado").show();
							
							aux=true;
						}
					}
						
					if(!aux){
//						//com.google.gwt.user.client.Window.alert("Else formas pago");
						Double totalTemp=0.0;
						try {
							Double totalTr = 0.0;
							Double totalBanco = 0.0;
							Double totalPagos = 0.0;
//							//com.google.gwt.user.client.Window.alert("Else totales");
							if ((Boolean) dynamicForm.getField("chkContado").getValue()) 
								totalContado = (dynamicForm.getValueAsString("txtContado")!=null)?Double.parseDouble(dynamicForm
									.getValueAsString("txtContado")):0.0;
//							//com.google.gwt.user.client.Window.alert("Else totales contado");
							if ((Boolean) dynamicForm.getField("chkCredito").getValue()){ 
								obtenerPagos();
								totalCredito = CValidarDato.getDecimal(
										2, totalCredito);
							}
//							totalCredito = (dynamicForm.getValueAsString("txtCredito")!=null)?Double.parseDouble(dynamicForm
//											.getValueAsString("txtCredito")):0.0;
//							//com.google.gwt.user.client.Window.alert("Else totales credito");
							if ((Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()) 
								totalTr = (dynamicForm.getValueAsString("txtTarjetaCr")!=null)?Double.parseDouble(dynamicForm3
									.getValueAsString("txtTarjetaCr")):0.0;
//							//com.google.gwt.user.client.Window.alert("Else totales tarjeta");
							if((Boolean)dynamicForm3.getField("chkBanco").getValue())
								totalBanco = (dynamicForm.getValueAsString("txtBanco")!=null)?Double.parseDouble(dynamicForm3
									.getValueAsString("txtBanco")):0.0;
//							//com.google.gwt.user.client.Window.alert("Else totales banco");
							
							totalPagos = totalContado + totalCredito + totalAnt + totalRet
									+ totalNota + totalTr + totalBanco;
//							//com.google.gwt.user.client.Window.alert("Else total pago "+textBoxTotal.getContents());
//							//com.google.gwt.user.client.Window.alert("Else total pago "+txtTotal.getDisplayValue());
								////com.google.gwt.user.client.Window.alert("Else get total");
								totalTemp = Double.parseDouble(txtTotal.getDisplayValue());
//							//com.google.gwt.user.client.Window.alert("Else fuera total");
							totalTemp =totalTemp- totalPagos;
//							//com.google.gwt.user.client.Window.alert("Else total menos pagos "+totalTemp);
							if (totalTemp != 0.0 || totalTemp != 0) {
//								//com.google.gwt.user.client.Window.alert("Else restar contado");
								dynamicForm.getField("txtContado").setValue(formato.format(
										Double.parseDouble(dynamicForm.getValueAsString("txtContado"))+totalTemp));
//								//com.google.gwt.user.client.Window.alert("Else restado contado");
//								if (totalTemp > 0){
//									//com.google.gwt.user.client.Window.alert("Else restar contado");
//									dynamicForm.getField("txtContado").setValue(formato.format(
//											Double.parseDouble(dynamicForm.getValueAsString("txtContado"))+totalTemp));
//									//com.google.gwt.user.client.Window.alert("Else restado contado");
//								}
//								else{
//									//com.google.gwt.user.client.Window.alert("Else sumar contado");
//									dynamicForm.getField("txtContado").setValue(formato.format(
//											Double.parseDouble(dynamicForm.getValueAsString("txtContado"))-totalTemp));
//									//com.google.gwt.user.client.Window.alert("Else sumado contado");
//								}
							}
						} catch (Exception e) {
							SC.say("Error: calculo subtotal totales " + e);
							////com.google.gwt.user.client.Window.alert("Error: " +e+" - "+ e.getMessage());
							//btnGrabar.setDisabled(false);
						}
					}
				}
//				//com.google.gwt.user.client.Window.alert("lblpagina");
				lblPie_pagina.setText("Favor emitir comprobante de retenci&oacute;n al correo: contabilidad@gigacomputers.com.ec.</br> Hasta 5 d&iacuteas despu&eacutes de emitida la factura de acuerdo al Art. 50.");
//				//com.google.gwt.user.client.Window.alert("total a letras");
				lblTotal_letras.setText("Son: "
						+ LetrasANumeros.convertNumberToLetter(formato2
								.format(total)));
			} catch (Exception e) {
				try {
					SC.say("ERROR GENERAL: "+e + "VALOR :");
				} catch (Exception a) {
					SC.say("Error GLOBAL: " + a);
				}
			}
		} catch (Exception h) {
			SC.say("Posible error al extraer todo: " + h);
		}
	}

	public void mostrarBodegas(String idProducto) {
		try {
			//SC.say(String.valueOf(Bodega==null));
			if (Bodega == null) {// Debemos cargar de la Base
				
				getService().ProdBodega(idProducto, "tblbodega", objbacklstBOD);
			} else {
				rowNumGlobal += 1;
				/*DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");*/
				RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.NONE);
			}
		} catch (Exception e) {
			SC.say("Error de bodegas carando de nuevo");
			getService().ProdBodega(idProducto, "tblbodega", objbacklstBOD);
		}
	}

	// MOSTRAR MESAS
	public void mostrar_mesasOcupadas() {
		getService().listarMesasOcupadas(objbacklstAreas);
		layout_1= new VLayout();
		layout_1.addMember(areas);

		listadoMes = new VentanaEmergente("Listado de Mesas", layout_1, false, true);
		listadoMes.setWidth(930);
		listadoMes.setHeight(610);
		listadoMes.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "block");
				calculo_Individual_Total();
				listadoMes.destroy();
			}
		});
		listadoMes.show();
	}
	
	// MOSTRAR PRODUCTOS MESAS
	public void mostrar_mesa_productos() {
		// Se muestra la ventana de busqueda de producto

		rowNumGlobal = grdProductos.getEditRow();

		// listadoProd.setHeight100();
		getService().listarCategoria(0, 40, objbacklstCat);
		getService().listarUnidad(0, 20, objbacklstUnidad);
		getService().listarMarca(0, 40, objbacklstMarca);
		getService().obtenerPedido(idMesa, listaCallbackPedido);
		getService().listarBodega(0, 100, objbacklstBod);
		getService().listarTipoprecio(0, 20, objbacklstTip);
		//getService().listarBodega(0, 100, objbacklstTipoBodega);
		layout_1= new VLayout();
		hLayoutPr= new HLayout();
		hStack= new HStack();
		DynamicForm dynamicForm = new DynamicForm(); 
		layout_1.setSize("100%", "100%");
		hLayoutPr.setSize("100%", "6%");
		searchFormProducto.setSize("85%", "100%");
		hStack.setSize("12%", "100%");

		lstProductosElaborados.setAutoFitData(Autofit.VERTICAL);
		lstProductosElaborados.setAutoFitMaxRecords(10);
		lstProductosElaborados.setAutoFetchData(true);
		lstProductosElaborados.setSize("100%", "80%");
		lblRegistrosFactura.setSize("100%", "4%");
		hLayoutPr.addMember(searchFormProducto);
		hLayoutPr.addMember(hStack);
		layout_1.addMember(hLayoutPr);
		layout_1.addMember(lstProductosElaborados);
		ButtonItem buttonItemSeleccionarTodo= new ButtonItem("Seleccionar Todo");
		//buttonItemSeleccionarTodo.addClickHandler(new ManejadorBotonesPedido("SelecionarTodo"));
		dynamicForm.setFields(buttonItemSeleccionarTodo);
		layout_1.addMember(dynamicForm);
		listadoProdMes = new VentanaEmergente("Listado de Platos Pedido", layout_1,
				false, true);
		listadoProdMes.setWidth(930);
		listadoProdMes.setHeight(610);
		listadoProdMes.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "block");
				calculo_Individual_Total();
				listadoProdMes.destroy();
			}
		});
		listadoProdMes.show();
	}
	

	
	// MOSTRAR PRODUCTOS
	public void mostrar_productos(Integer Tipo, Integer Jerarquia) {
		// Se muestra la ventana de busqueda de producto

		rowNumGlobal = grdProductos.getEditRow();

		// listadoProd.setHeight100();
		getService().listarCategoria(0, 40, objbacklstCat);
		getService().listarUnidad(0, 20, objbacklstUnidad);
		getService().listarMarca(0, 40, objbacklstMarca);
		getService().listaProductos(0, 20, Tipo,Jerarquia,Integer.valueOf(tipoP), Factum.banderaStockNegativo, listaCallback);
		getService().listarBodega(0, 100, objbacklstBod);
		getService().listarTipoprecio(0, 20, objbacklstTip);
		//getService().listarBodega(0, 100, objbacklstTipoBodega);
		
		layout_1= new VLayout();
		hLayoutPr= new HLayout();
		hStack= new HStack();
		
		layout_1.setSize("100%", "100%");
		hLayoutPr.setSize("100%", "6%");
		searchFormProducto.setSize("85%", "100%");
		hStack.setSize("12%", "100%");

//		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
//        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
//        btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
//      //  btnInicio.setDisabled(true);
//        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
//        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
//        hStack.addMember(btnInicio);
//        hStack.addMember(btnAnterior);
//        hStack.addMember(btnSiguiente);
//        hStack.addMember(btnFin);
//        hStack.addMember(btnEliminarlst);
//		hLayout.addMember(hStack);
		
		lstProductos.setAutoFitData(Autofit.VERTICAL);
		lstProductos.setAutoFitMaxRecords(10);
		lstProductos.setAutoFetchData(true);
		lstProductos.setSize("100%", "80%");
		lblRegistrosFactura.setSize("100%", "4%");
		hLayoutPr.addMember(searchFormProducto);
		hLayoutPr.addMember(hStack);
		layout_1.addMember(hLayoutPr);
		layout_1.addMember(lstProductos);
		layout_1.addMember(lblRegistrosFactura);
		listadoProd = new VentanaEmergente("Listado de Productos", layout_1,
				false, true);
		listadoProd.setWidth(930);
		listadoProd.setHeight(610);
		listadoProd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "block");
				calculo_Individual_Total();
				listadoProd.destroy();
			}
		});
		listadoProd.show();
	}
	
	
	// MOSTRAR 
	public void mostrar_productosOrdenes() {
		// Se muestra la ventana de busqueda de producto

		rowNumGlobal = grdProductos.getEditRow();

		// listadoProd.setHeight100();
		getService().listarCategoria(0, 40, objbacklstCat);
		getService().listarUnidad(0, 20, objbacklstUnidad);
		getService().listarMarca(0, 40, objbacklstMarca);
		getService().listaProductos(listaCallbackOrdenes);
		getService().listarBodega(0, 100, objbacklstBod);
		getService().listarTipoprecio(0, 20, objbacklstTip);
		//getService().listarBodega(0, 100, objbacklstTipoBodega);
		
		layout_1= new VLayout();
		hLayoutPr= new HLayout();
		hStack= new HStack();
		
		layout_1.setSize("100%", "100%");
		hLayoutPr.setSize("100%", "6%");
		searchFormProducto.setSize("85%", "100%");
		hStack.setSize("12%", "100%");

		lstProductos.setAutoFitData(Autofit.VERTICAL);
		lstProductos.setAutoFitMaxRecords(10);
		lstProductos.setAutoFetchData(true);
		lstProductos.setSize("100%", "80%");
		lblRegistrosFactura.setSize("100%", "4%");
		hLayoutPr.addMember(searchFormProducto);
		hLayoutPr.addMember(hStack);
		layout_1.addMember(hLayoutPr);
		layout_1.addMember(lstProductos);
		layout_1.addMember(lblRegistrosFactura);
		listadoProd = new VentanaEmergente("Listado de Platos por Mesa", layout_1,
				false, true);
		listadoProd.setWidth(930);
		listadoProd.setHeight(610);
		listadoProd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "block");
				calculo_Individual_Total();
				listadoProd.destroy();
			}
		});
		listadoProd.show();
	}
	
	/*********************************************************************************/
	/*********************************************************************************/
	/*********************************************************************************/
	/*********************************************************************************/

	public void verificarPagos() {
		try {
			Double totalTr = 0.0;
			Double totalBanco = 0.0;
			Double totalPagos = 0.0;
			// Aqui vamos a realizar la suma del total de la factura
			// Primero truncamos a 4 decimales
			totalContado = Double.parseDouble(dynamicForm
					.getValueAsString("txtContado"));
//			totalCredito = Double.parseDouble(dynamicForm
//					.getValueAsString("txtCredito"));
			totalTr = Double.parseDouble(dynamicForm3
					.getValueAsString("txtTarjetaCr"));
			totalBanco = Double.parseDouble(dynamicForm3
					.getValueAsString("txtBanco"));

			/**
			 * totalContado = DigitosDecimal.getDecimal(4,totalContado);
			 * totalCredito = DigitosDecimal.getDecimal(4,totalCredito);
			 * totalAnt = DigitosDecimal.getDecimal(4,totalAnt); totalRet =
			 * DigitosDecimal.getDecimal(4,totalRet); totalNota =
			 * DigitosDecimal.getDecimal(4,totalNota); totalTr =
			 * DigitosDecimal.getDecimal(4,totalTr); totalBanco =
			 * DigitosDecimal.getDecimal(4,totalBanco);
			 */

			// Ahora realizamos la suma y debe dar el valor del total
			totalPagos = totalContado + totalCredito + totalAnt + totalRet
					+ totalNota + totalTr + totalBanco;
			if (NumDto == 0 || NumDto == 1 || NumDto == 3 || NumDto == 13
					|| NumDto == 14 || NumDto == 20) {
			} else {
				total = Double.parseDouble(txtTotal.getDisplayValue());
			}
			total -= totalPagos;
			if (total == 0.0) {// Estan correctos los valores se desbloquea el
								// boton guardar
				btnGrabar.setDisabled(false);
			} else {
				if (total > 0)
					SC.say("La suma del valor de los pagos es mayor en "
							+ total + " al total del Document");
				else
					SC.say("La suma del valos de los pagos es menor en "
							+ total + " al total del Document");
				btnGrabar.setDisabled(false);
			}
		} catch (Exception e) {
			SC.say("Error: verificar pagos " + e);
			btnGrabar.setDisabled(false);
		}
	}

	public void calculoGasto() {
		Double descuentoNeto = 0.0;
		Subtotal = Double.parseDouble(txtSubtotal.getDisplayValue());
		Subtotal = CValidarDato.getDecimal(2, Subtotal);
		descuentoGlobal = Double.parseDouble(txtDescuento.getDisplayValue());
		descuentoGlobal = CValidarDato.getDecimal(2, descuentoGlobal);

		NumberFormat formato = NumberFormat.getFormat("###0.00");
		// txtSubtotal.setValue(formato.format(Subtotal));
		txtDescuento.setValue(formato.format(descuentoGlobal));
		descuentoNeto = Double.parseDouble(txtDescuentoNeto.getDisplayValue());
		txtBase_imponible.setValue(formato.format(Subtotal - descuentoGlobal
				- descuentoNeto));
		NumberFormat formato2 = NumberFormat.getFormat("#.00");
		subtotalIva14 = Double.parseDouble(txtBase_imponible.getDisplayValue());
		// subtotalIva0=subtotalIva0/100;
		// txt0_iva.setValue(formato.format(subtotalIva0));
		// subtotalIva12=subtotalIva12/100;
		txt14_iva.setValue(formato.format(subtotalIva14));
//		txtValor14_iva.setValue(formato.format(subtotalIva14 * (Factum.banderaIVA/100)));
		txtValor14_iva.setValue(valoresIva);
//		total = subtotalIva14 * (1+(Factum.banderaIVA/100));
		total = subtotalIva14 +valoresIva;
		total = CValidarDato.getDecimal(2, total);
		txtTotal.setValue(formato.format(total));
		lblTotal_letras.setText("Son: "
				+ LetrasANumeros.convertNumberToLetter(formato2.format(total)));
	}

	public void calculoCambio() {
		NumberFormat formato = NumberFormat.getFormat("#0.00");
		Double valorTotal = Double.parseDouble(txtTotal.getDisplayValue());
		Double valorEfectivo = Double
				.parseDouble(txtEfectivo.getDisplayValue());
		Double cambio = valorEfectivo - valorTotal;
		cambio = CValidarDato.getDecimal(2, cambio);
		txtCambio.setValue(formato.format(cambio));
	}

	public Factura getFactura(){
		return this;
	}
	
	final AsyncCallback<Integer> backIngreso = new AsyncCallback<Integer>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

		public void onSuccess(Integer result) {
			if (!result.equals(0)) {
				SC.say("Ingreso: "+result.toString());

			}

		}

	};
	
	private class Manejador2 implements BlurHandler, ChangedHandler,
			com.smartgwt.client.widgets.form.fields.events.KeyPressHandler {
		// manejador de cuando presione esc,etc
		String indicador = "";// para indicar que fue lo que se presiono tendra
		String cadena = "";

		Manejador2(String nombreBoton) {
			this.indicador = nombreBoton;
		}

		// @Override
		public void onKeyPress(KeyPressEvent event) {
			// TODO Auto-generated method stub
			String item = event.getItem().getName();
			////com.google.gwt.user.client.Window.alert(item+": "+String.valueOf(item.equals("txtContado") ));
			////com.google.gwt.user.client.Window.alert("Ingreso tecla contado");
			////com.google.gwt.user.client.Window.alert(event.getKeyName()+": "+String.valueOf(event.getKeyName().equals("Enter")));
			if (item.equals("txtContado") ) {
//				
//				
				////com.google.gwt.user.client.Window.alert("En texto "+event.getKeyName());
				if (event.getKeyName().equals("Enter")){
					//getService().ingresar(getFactura(), backIngreso);
					getService().ingresar( backIngreso);
//					winFactura1 = new Window();
//	            	winFactura1.setWidth("95%");
//					winFactura1.setHeight("95%");
//	            	winFactura1.setTitle("Documento Comercial");  
//	            	winFactura1.setShowMinimizeButton(false);  
//	            	winFactura1.setIsModal(true);  
//	            	winFactura1.setShowModalMask(true);  
//	            	winFactura1.centerInPage();  
//	            	frmAjuste kardex =new frmAjuste("-1");
//					winFactura1.addCloseClickHandler(new CloseClickHandler() {  
//		                public void onCloseClick(CloseClientEvent event) {
//		                	winFactura1.destroy();  
//		                }  
//		            });
//					VLayout form = new VLayout();  
//		            form.setSize("100%","100%"); 
//		            form.addMember(kardex);
//		            winFactura1.addItem(form);
//		            winFactura1.centerInPage();
//		            winFactura1.show();
				}
			}
			if (indicador.equals("Subto")) {
				cadena = event.getItem().getValue().toString();// txtSubtotal.getValue().toString();
				if (Character.isDigit(cadena.charAt(cadena.length() - 1))
						|| (cadena.charAt(cadena.length() - 1)) == '.') {
					if (NumDto == 7 || NumDto == 4 || NumDto == 2
							|| NumDto == 10 || NumDto == 12 || NumDto == 15 || NumDto == 26 || NumDto==27) {
						txtTotal.setValue(txtSubtotal.getDisplayValue());
						txtEfectivo.setValue(txtSubtotal.getDisplayValue());
					} else {
						calculoGasto();
					}
				} else {
					cadena = cadena.substring(0, cadena.length() - 1);
					txtSubtotal.setValue(cadena);
				}
				txtSubtotal.redraw();
			} else if (indicador.equals("iva")) {
				cadena = event.getItem().getValue().toString();// txtSubtotal.getValue().toString();
				if (Character.isDigit(cadena.charAt(cadena.length() - 1))
						|| (cadena.charAt(cadena.length() - 1)) == '.') {
					calculoGasto();
				} else {
					cadena = cadena.substring(0, cadena.length() - 1);
					txtValor14_iva.setValue(cadena);
				}
				txtValor14_iva.redraw();
			} else if (indicador.equals("cambio")) {
				String tecla = event.getKeyName();
				/*
				 * cadena =
				 * event.getItem().getValue().toString();//txtSubtotal.getValue
				 * ().toString();
				 * 
				 * if(Character.isDigit(cadena.charAt(cadena.length()-1))||(cadena
				 * .charAt(cadena.length()-1))=='.') { calculoCambio(); }else{
				 * cadena = cadena.substring(0, cadena.length()-1);
				 * txtEfectivo.setValue(cadena); } txtEfectivo.redraw();
				 */

				if (tecla.equals("Enter")) {
					confirmar = '0';
					int maximoItems= grdProductos.getTotalRows();
					if (maximoItems>Factum.banderaNumeroItemsFactura && NumDto==0 && Factum.banderaDividirFactura==1 && Factum.banderaMenuFacturacionElectronica==0){
						dividir=true;
					}else{
						dividir=false;
					}
//					//com.google.gwt.user.client.Window.alert("Analizado dividir "+dividir);
					// ***************************BLOQUEAR BOTONES
					// *************************
//					//com.google.gwt.user.client.Window.alert("antes grabar documento");
					if (!dividir)
						GrabarDocumento(total);
					else GrabarDocumento();
//					GrabarDocumento(total);
				}
			} else if (indicador.equals("descuentoNeto")) {
				calculo_Subtotal();
			}

		}

		public void onBlur(BlurEvent event) {
			// TODO Auto-generated method stub
			if (indicador.equals("Subto")) {
				if (NumDto == 7 || NumDto == 4 || NumDto == 2 || NumDto == 10
						|| NumDto == 12 || NumDto == 15 || NumDto == 26 || NumDto == 27) {
					txtTotal.setValue(txtSubtotal.getDisplayValue());
					txtEfectivo.setValue(txtSubtotal.getDisplayValue());
				} else {
					calculoGasto();
				}
			} else if (indicador.equals("iva")) {
				calculoGasto();
			} else if (indicador.equals("cambio")) {
				calculoCambio();
				NumberFormat formato = NumberFormat.getFormat("#0.00");
				txtEfectivo.setValue(formato.format(Double
						.parseDouble(txtEfectivo.getDisplayValue())));
			} else if (indicador.equals("descuentoNeto")) {
				calculo_Subtotal();
			}
		}

		@Override
		public void onChanged(ChangedEvent event) {
			if (indicador.equals("cambio")) {
				cadena = event.getValue().toString();

				if (Character.isDigit(cadena.charAt(cadena.length() - 1))
						|| (cadena.charAt(cadena.length() - 1)) == '.') {
					calculoCambio();
				} else {
					cadena = cadena.substring(0, cadena.length() - 1);
					txtEfectivo.setValue(cadena);

				}
				txtEfectivo.redraw();
			}

		}

	}
	
	private DialogBox showCustomDialog() {

	       final DialogBox dialog = new DialogBox(true, true);
	       // final DialogBox dialog = new DialogBox(true, true);
	       // Set caption
	       dialog.setText("DialogBox Caption");
	       // Setcontent
		Label content = new Label("This is sample text message inside "
			+ "Customized Dialogbox. In this example a Label is "
			+ "added inside the Dialogbox, whereas any custom widget "
			+ "can be added inside Dialogbox as per application'ser's need. ");
	       if (dialog.isAutoHideEnabled())  {
		   dialog.setWidget(content);
	        } else {
		VerticalPanel vPanel = new VerticalPanel();vPanel.setSpacing(2);
		vPanel.add(content);vPanel.add(new Label("\n"));
		Button close=new Button("Close");
		close.addClickHandler( new ClickHandler() {
		public void onClick(ClickEvent event) {
	         	dialog.hide();
			}
		});
		vPanel.add(close);
		dialog.setWidget(vPanel);
		}
		dialog.setPopupPosition(100, 150);
		dialog.show();
		return dialog;
	}

	public void buscarPers(String ced, int tipo){
		PersonaDTO per=new PersonaDTO();
		per.setCedulaRuc(ced);
		getService().buscarPersona(per,tipo, objbackb);
	}
	
	final AsyncCallback<String> callbackUrl= new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}

		public void onSuccess(String result) {
			frmsubirarchivo.dynamicformsubirarchivo.submitForm();
			String nombrefilecadena=frmsubirarchivo.dynamicformsubirarchivo.getItem("CARGAR XML").getDisplayValue();
			nombrefilecadena = nombrefilecadena.replace("\'", ".");
			String[] nombresfile=nombrefilecadena.split("\\\\");
			int longitud=nombresfile.length;							
			String nombreFile=nombresfile[longitud-1];
			String[] divs=GWT.getModuleBaseURL().split("\\/");
        	String ppath=divs[divs.length-1];        	
        	
			winXML.destroy();
			existeUrl(GWT.getModuleBaseURL()+nombreFile);
		}
	};
	
	public void existeUrl(final String url){
		RequestBuilder rbTest;
		rbTest = new RequestBuilder(RequestBuilder.GET,url);
		
		try {
			rbTest.sendRequest(null, new RequestCallback() {
				public void onError(
						final com.google.gwt.http.client.Request request,
						final Throwable exception) {
					com.google.gwt.user.client.Window.alert("Error no existe "
							+ exception+ "request "+request.toString());
					//existeUrl(url);
				}

				public void onResponseReceived(
						final com.google.gwt.http.client.Request request,
						final Response response) {
					//com.google.gwt.user.client.Window.alert(response.getStatusText()+" "+response.getStatusCode());
					if (response.getStatusCode()==404) existeUrl(url);
					else{
						
						String urlServ="http://"+Factum.banderaIpServidor+":8090/readxml/read?file="+url+"&type=1";
						//String url="http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/crud/readxml/read?file="+GWT.getModuleBaseURL()+nombreImagen+"&type=1";
						//com.google.gwt.user.client.Window.alert(url);
						
						
						RequestBuilder 
						rb = new RequestBuilder(RequestBuilder.GET,urlServ);
		
							try {
								rb.sendRequest(null, new RequestCallback() {
									public void onError(
											final com.google.gwt.http.client.Request request,
											final Throwable exception) {
										com.google.gwt.user.client.Window.alert("Error en el directorio del archivo "
												+ exception+ "request "+request.toString());
									}
		
									public void onResponseReceived(
											final com.google.gwt.http.client.Request request,
											final Response response) {
		
										//com.google.gwt.user.client.Window.alert("Response: "+response.getText());
										//com.google.gwt.user.client.Window.alert("Response ");
										/*com.google.gwt.user.client.Window.alert("Response "+response.getText());
										com.google.gwt.user.client.Window.alert("Response "+response.toString());*/
										String loginResponse= response.getText();
										JSONObject respuesta=JSONParser.parseLenient(loginResponse).isObject();
										//com.google.gwt.user.client.Window.alert("Response ");
										//enabled=respuesta.get("enabled").isBoolean().booleanValue();
										String razonSocial=respuesta.get("razonSocial").isString().stringValue();								
										
										txtAutorizacion_sri.setValue(respuesta.get("claveAcceso").isString().stringValue());
										
										DateTimeFormat dateForm=DateTimeFormat.getFormat("dd/MM/yyyy");
										//com.google.gwt.user.client.Window.alert("Fecha Emision "+respuesta.get("fechaEmision").isString().stringValue());
										Date date= dateForm.parse(respuesta.get("fechaEmision").isString().stringValue());
										
										dateFecha_emision.setValue(date);
										
										txtNumCompra.setValue(respuesta.get("estab").isString().stringValue()+"-"
												+ respuesta.get("ptoEmi").isString().stringValue()+"-"
												+ respuesta.get("secuencial").isString().stringValue());
										
										txtTotalXML.setValue(respuesta.get("importeTotal").isNumber().doubleValue());
										//"estab":"001", "ptoEmi":"001", "secuencial":"000062908"
										//com.google.gwt.user.client.Window.alert("Razon Social "+razonSocial);
										//com.google.gwt.user.client.Window.alert("Resp "+respuesta.toString());
										
										String ruc=respuesta.get("ruc").isString().stringValue();
										//com.google.gwt.user.client.Window.alert("ruc "+ruc);
										buscarPers(ruc,2);
									}
		
								});
							} catch (RequestException e) {
								// TODO Auto-generated catch block
		//						//com.google.gwt.user.client.Window.alert("Error ");
								SC.say("Error "+e);
								e.printStackTrace();
							}
					}
				}

			});
		} catch (RequestException e) {
			SC.say("Error "+e);
			e.printStackTrace();
		}
		
	}
	
	private class Manejador implements ClickHandler, KeyPressHandler,
			EditorExitHandler, ChangeHandler, DoubleClickHandler,
			RecordClickHandler,
			com.smartgwt.client.widgets.grid.events.ChangedHandler {
		// manejador de cuando presione esc,etc
		String indicador = "";// para indicar que fue lo que se presiono tendra
								// el nombre

		Manejador(String nombreBoton) {
			this.indicador = nombreBoton;
		}
		Boolean continuar=false;
		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
			if (indicador.equals("xml")){
				txtTotalXML.setVisible(true);
				txtTotalXML.show();
				//fileXML=new FileItem("CARGAR XML");
				winXML=new Window();
				winXML.setWidth(930);  
				winXML.setHeight(550);  
				winXML.setTitle("Buscar XML");  
				winXML.setShowMinimizeButton(false);  
				winXML.setIsModal(true);  
				winXML.setShowModalMask(true); 
				winXML.setKeepInParentRect(true);
				winXML.centerInPage();  
				winXML.addCloseClickHandler(new CloseClickHandler() {  
					@Override
					public void onCloseClick(CloseClientEvent event) {
						winXML.destroy();
					}  
                });
				
				if(frmsubirarchivo!=null)	frmsubirarchivo.iframeUpload.removeFromParent();
				frmsubirarchivo= new frmSubirArchivo("CARGAR XML");
				vlayout_total.addMember(frmsubirarchivo.iframeUpload);
				frmsubirarchivo.uploadButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						//com.google.gwt.user.client.Window.alert("Cargar xml");
						
						String nombrefilecadena=frmsubirarchivo.dynamicformsubirarchivo.getItem("CARGAR XML").getDisplayValue();
						nombrefilecadena = nombrefilecadena.replace("\'", ".");
						String[] nombresfile=nombrefilecadena.split("\\\\");
						int longitud=nombresfile.length;							
						String nombreFile=nombresfile[longitud-1];
						String[] divs=GWT.getModuleBaseURL().split("\\/");
			        	String ppath=divs[divs.length-1];
						//com.google.gwt.user.client.Window.alert(GWT.getModuleBaseURL()+" cad "+nombreimagencadena+" "+ppath+" "+nombreImagen);
			        				        	
						//com.google.gwt.user.client.Window.alert("Antes existe url");
						getService().eliminarFile(GWT.getModuleBaseURL()+nombreFile, callbackUrl);
																		
						//http://localhost:9192/readxml?file=C:/Users/Usuario/Downloads/Factura.xml
						//String url="http://"+Factum.banderaIpServidor+":9192/readxml?file="+GWT.getModuleBaseURL()+nombreImagen+"&type=1";
												
					}
				});
		        winXML.addItem(frmsubirarchivo);
		        //winXML.addItem(vlay);
                winXML.show();
				//showCustomDialog();
			}else
			if (indicador.equals("nuevo")) {
				if (btnNuevo.getTitle().equals("ANULAR")) {
					
					SC.ask("�Realmente desea Anular", new BooleanCallback() {
						public void execute(Boolean value1) {
							if (value1 != null && value1) {
								String pregunta = "recibidos";String preguntaR="recibida";
								if (afeccion == 1)	{pregunta = "realizados";preguntaR="emitida";}
//								//com.google.gwt.user.client.Window.alert("Anulando "+continuar);
								if (!pagoActivo && tblRetenciones.isEmpty()){
									confirmar = '2';
									pregunta = "devolver";
									if (afeccion == 1)	pregunta = "restar";
									if(NumDto!=7 && NumDto!=12 && NumDto!=4 && NumDto!=5){
									SC.ask("�Desea " + pregunta+ " los items al Inventario?",	new BooleanCallback() {
												public void execute(Boolean value) {
													if (value != null && value) {
														anulacion = -20;
														GrabarDocumento(total);
													} else {
														anulacion = -10;
														GrabarDocumento(total);
													}
												}});
									}else{
										anulacion = -10;
										GrabarDocumento(total);
									}
								}else{
									pregunta = "recibidos";preguntaR="recibidas";
									if (afeccion == 1)	{pregunta = "realizados";preguntaR="emitidas";}
									String razon="";String proceso="";
									if (!pagoActivo && !tblRetenciones.isEmpty()){
										razon="retenciones "+preguntaR;proceso="anularan";
									}else if (pagoActivo && tblRetenciones.isEmpty()){
										razon="pagos "+pregunta;proceso="transformaran a anticipos";
									}else if (pagoActivo && !tblRetenciones.isEmpty()){
										razon="pagos "+pregunta+ " y retenciones "+preguntaR;proceso="transformaran a anticipos los pagos y las retenciones se anularan";
									}
									SC.confirm("El documento tiene "+razon+", se "+proceso+". �Continuar anulacion?", new BooleanCallback() {
//									SC.confirm("El documento tiene pagos "+pregunta+", se transformaran a anticipos. �Continuar anulacion?", new BooleanCallback() {
										public void execute(Boolean value1) {
											if (value1 != null && value1){
												confirmar = '2';
												// afeccion = 0//VENTA
												
												String preguntad = "devolver";
												if (afeccion == 1)
													preguntad = "restar";
				
												// Se debe preguntar si se van a devolver los
												// items al inventario
												if(NumDto!=7 && NumDto!=12 && NumDto!=4 && NumDto!=5)
												{
												SC.ask("�Desea " + preguntad
														+ " los items al Inventario?",
														new BooleanCallback() {
															public void execute(Boolean value) {
																if (value != null && value) {
																	// En caso de selecciona YES
																	anulacion = -20;
																	GrabarDocumento(total);
																	//asignarPuntos(-1);
																} else {
																	// En caso de selccionar NO
																	anulacion = -10;
																	GrabarDocumento(total);
																	//asignarPuntos(-1);
																}
															}
														});
												}
												else
												{
													anulacion = -10;
													GrabarDocumento(total);
												}
											}
										}
									});
								}
//								if (pagoActivo){
//									SC.confirm("El documento tiene pagos "+pregunta+", se transformaran a anticipos. �Continuar anulacion?", new BooleanCallback() {
//										public void execute(Boolean value1) {
//											if (value1 != null && value1){
//												confirmar = '2';
//												// afeccion = 0//VENTA
//												
//												String preguntad = "devolver";
//												if (afeccion == 1)
//													preguntad = "restar";
//				
//												// Se debe preguntar si se van a devolver los
//												// items al inventario
//												if(NumDto!=7 && NumDto!=12 && NumDto!=4 && NumDto!=5)
//												{
//												SC.ask("�Desea " + preguntad
//														+ " los items al Inventario?",
//														new BooleanCallback() {
//															public void execute(Boolean value) {
//																if (value != null && value) {
//																	// En caso de selecciona YES
//																	anulacion = -20;
//																	GrabarDocumento(total);
//																	//asignarPuntos(-1);
//																} else {
//																	// En caso de selccionar NO
//																	anulacion = -10;
//																	GrabarDocumento(total);
//																	//asignarPuntos(-1);
//																}
//															}
//														});
//												}
//												else
//												{
//													anulacion = -10;
//													GrabarDocumento(total);
//												}
//											}
//										}
//									});
//								}else{
//									confirmar = '2';
//									pregunta = "devolver";
//									if (afeccion == 1)	pregunta = "restar";
//									if(NumDto!=7 && NumDto!=12 && NumDto!=4 && NumDto!=5){
//									SC.ask("�Desea " + pregunta+ " los items al Inventario?",	new BooleanCallback() {
//												public void execute(Boolean value) {
//													if (value != null && value) {
//														anulacion = -20;
//														GrabarDocumento(total);
//													} else {
//														anulacion = -10;
//														GrabarDocumento(total);
//													}
//												}});
//									}else{
//										anulacion = -10;
//										GrabarDocumento(total);
//									}
//								}
							}else {}
						}
					});

				} else {
					// Se debe limpiar todo para un nuevo documento
					txtDireccion.setValue("");
					txtCorreo.setValue("");
					txtObservacion.setValue("");
					txtTelefono.setValue("");
					txtaConcepto.setValue("");
					txtSubtotal.setValue(0.0);
					txtDescuento.setValue(0.0);
					txtTotal.setValue(0.0);
					btnGrabar.setDisabled(false);
					btnImprimir.setDisabled(true);
				}
			} else if (indicador.equals("grabar")) {

				if(NumDto == 0 && total>200 && codigoSeleccionado==1){
					SC.say("No se puede almacenar un una factura que sobre pase los $200 como Consumidor Final");
					btnGrabar.setDisabled(false);
				}else{
//					//com.google.gwt.user.client.Window.alert("Presionado el boton de grabar");
					btnGrabar.setDisabled(true);
					btnGrabar.redraw();
					//SC.say("Boton deshabilitado"+btnGrabar.isDisabled().toString());
				// Llamamos a la funcion para ingresar la factura
				// SC.say("LO QUE DEVUELVE"+this.indicador.toString());
					confirmar = '0';
//					//com.google.gwt.user.client.Window.alert("boton grabar deshabilitado confirmar "+confirmar);
					int maximoItems= grdProductos.getTotalRows();
//					//com.google.gwt.user.client.Window.alert("Antes dividir "+Factum.banderaNumeroItemsFactura+" - "+Factum.banderaDividirFactura+" - de "+maximoItems);
					if (btnGrabar.getTitle().equals("CONFIRMAR")) {
//						//com.google.gwt.user.client.Window.alert("Boton confirmar");
						confirmar = '1';
//						//com.google.gwt.user.client.Window.alert("boton confirmar "+confirmar);
						// En caso de ser una compra se debe mostrar una ventana
						// para escoger a que bodega iran los productos
					}
					else if (maximoItems>Factum.banderaNumeroItemsFactura && NumDto==0 && Factum.banderaDividirFactura==1 && Factum.banderaMenuFacturacionElectronica==0){
						dividir=true;
					}else{
						dividir=false;
					}
//					//com.google.gwt.user.client.Window.alert("Analizado dividir "+dividir);
					// ***************************BLOQUEAR BOTONES
					// *************************
//					//com.google.gwt.user.client.Window.alert("antes grabar documento");
					if (!dividir)
						GrabarDocumento(total);
					else GrabarDocumento();
//					//com.google.gwt.user.client.Window.alert("despues grabar");
				}

			
			//XAVIER ZEAS - DOCUMENTO ELECTRONICO
			}else if(indicador.equals("documentoelectronico")){
		    String estado="0";  
			getService().documentoElectronico(documentoenviar, Factum.banderaNombrePlugFacturacionElectronica,Factum.banderaRutaCertificado, 
//					Factum.banderaAmbienteFacturacionElectronica, 
					Factum.banderaSO, objback);
			}
			//XAVIER ZEAS - DOCUMENTO ELECTRONICO
			
			else if (indicador.equals("imprimir")) {
//				com.google.gwt.user.client.Window.alert("Imprimir boton "+Factum.banderaConfiguracionImpresion);
				String numtrans=txtNumero_factura.getDisplayValue();
//				com.google.gwt.user.client.Window.alert("Imprimir boton "+numtrans);
					if (Factum.banderaConfiguracionImpresion==1) {
						impresionDirecta(numtrans);
					} else if(Factum.banderaConfiguracionImpresion==0){
						imprimir();
					} else if(Factum.banderaConfiguracionImpresion==3){
						if(NumDto==0){
							if(Factum.banderaConfiguracionImpresionFactura==0){
								ListGrid grdProductosImprimir=grdProductos;
								imprimir();
							}else if(Factum.banderaConfiguracionImpresionFactura==1){
								impresionDirecta(numtrans);
							}
						}else if(NumDto==2){
							if(Factum.banderaConfiguracionImpresionNota==0){
								imprimir();			                            	
							}else{
								impresionDirecta(numtrans);
							}
						}else if(NumDto==3){
							if(Factum.banderaConfiguracionImpresionNotaCredito==0){
								imprimir();			                            	
							}else{
								impresionDirecta(numtrans);
							}
						}else if(NumDto==5){
							if(Factum.banderaConfiguracionImpresionRetencion==0){
								imprimir();			                            	
							}else{
								impresionDirecta(numtrans);
							}
						}else if(NumDto==26){
							if(Factum.banderaConfiguracionImpresionNotaVentas==0){
								imprimir();			                            	
							}else{
								impresionDirecta(numtrans);
							}
						}else if(NumDto==20 || NumDto==4){
							
								imprimir();
						}else{
								impresionDirecta(numtrans);
						}
						
					}

			} else if (indicador.equals("validar")) {
				ValidarRuc valida = new ValidarRuc();
				boolean datoValido = valida.validarRucCed(txtRuc_cliente
						.getDisplayValue());
				if (datoValido) {
					SC.say("C&Eacute;DULA CORRECTA");
				} else {
					SC.say("C&Eacute;DULA INCORRECTA, POR FAVOR VERIFIQUELA!");
				}
			} else if (indicador.equals("transformar")) {
				/*
				 * SC.ask("Desea transformar a Factura Peque&#241;a?", new
				 * BooleanCallback() {public void execute(Boolean value) { if
				 * (value != null && value) {
				 */
				confirmar = '0';
				idDto = null;
				NumDto = 0;
				convertir = 1;
				String filtro = "where TipoTransaccion =", ordenamiento;
				filtro += 0 + "";

				obtener_UltimoDtoFactura(filtro, "numRealTransaccion");
				getService().fechaServidor(callbackFecha);
				SC.say("Transformado Correctamente a Factura");
				/*
				 * } else { confirmar='0'; idDto = null; NumDto = 13; String
				 * filtro="where TipoTransaccion =", ordenamiento; filtro +=
				 * 0+""; obtener_UltimoDtoFactura(filtro, "numRealTransaccion");
				 * } }});
				 */

			} else if (indicador.equals("aceptar")) { 
				btnacept.setDisabled(true);
				NumberFormat formato = NumberFormat.getFormat("###0.00");
				if (!(Boolean) dynamicForm.getField("chkContado")
						.getValue() 
						&& !(Boolean)dynamicForm.getField("chkCredito").getValue()&&
						!(Boolean)dynamicForm2.getField("chkRetencion").getValue()&&
						!(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
						!(Boolean)dynamicForm2.getField("chkNotaCredito").getValue()&&
						!(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
						!(Boolean)dynamicForm3.getField("chkBanco").getValue()){
					dynamicForm.getField("chkContado").setValue(true);
					dynamicForm.getField("txtContado").setValue(formato.format(total));
//					dynamicForm.getField("txtContado").setVisible(true);
					dynamicForm.getField("txtContado").show();
					//aux=true;
				}
				GrabarDocumento(total);
			
			}
			else if (indicador.equals("nuevaFactura")) {
				SC.ask("Desea crear una Factura a partir de la existente",
						new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									confirmar = '0';
									idDto = null;
									// NumDto = 0;
									String filtro = "where TipoTransaccion =", ordenamiento;
									filtro += NumDto + "";

									obtener_UltimoDtoFacturaNueva(filtro,"numRealTransaccion");
									SC.say("Transformado Correctamente a Factura");
								}
							}
						});
			} else if (indicador.equals("borrar")) {
				borrarDocumento();
			} else if (indicador.equals("asignarPuntos")) {
				asignarPuntos(1);

			} else if (indicador.equals("quitarPuntos")) {

				if (codigoSeleccionadoPuntos != 0) {
					ClienteDTO cli = new ClienteDTO();
					cli.setIdEmpresa(Factum.empresa.getIdEmpresa());
					cli.setIdCliente(codigoSeleccionadoPuntos);
					puntosAcreditados = Integer.valueOf(txtPuntosAsignados
							.getValue().toString()) * -1;
					cli.setPuntos(puntosAcreditados);
					winClientes.destroy();
					getService().asignarPuntos(cli, objbackPuntos);
				}

			} else if (indicador.equals("exportar")) {
				String[] atributos = new String[3];
				atributos[0] = "codigoBarras";
				atributos[1] = "cantidad";
				atributos[2] = "precioUnitario";
				atributos[3] = "Mayorista";
				atributos[4] = "PVP";
				atributos[5] = "Afiliado";
				atributos[6] = "IDprod";
				LinkedList<String[]> ld = new LinkedList<String[]>();
				ld.add(atributos);
				ListGridRecord[] selectedRecords = grdProductos.getRecords();
				ListGrid lstProducto = new ListGrid();
				for (ListGridRecord rec : selectedRecords) {
					String[] prd = new String[3];
					prd[0] = rec.getAttribute("codigoBarras");
					prd[1] = rec.getAttribute("cantidad");
					prd[2] = rec.getAttribute("precioUnitario");
					//prd[3] = rec.getAttribute("idBodega");
					ld.add(prd);

				}
				String[] cabecera = new String[4];
				cabecera[0] = "cedula";
				cabecera[1] = "fecha";
				cabecera[2] = "bodega";
				cabecera[3] = "Tprecio";
				ld.add(cabecera);

				String[] fact = new String[4];
				fact[0] = txtRuc_cliente.getDisplayValue();
				fact[1] = dateFecha_emision.getDisplayValue();
				fact[2] = dfExtras.getItem("cmbTipoBodega").getDisplayValue();
				fact[3] = cmbTipoPrecio.getDisplayValue();
				ld.add(fact);

				CreateExelDTO exel = new CreateExelDTO(ld, atributos,
						"Exportarfact.xls");
				
			} else if (indicador.equals("importar")) {

				subirarchivo s = new subirarchivo(fact);
			} else {
				int num_registros = 0;// contendra el numero de registro que
										// contiene la grilla
				ListGridRecord[] selectedRecords = grdProductos.getRecords();
				for (ListGridRecord rec : selectedRecords)
					num_registros++;
				if (grdProductos.validateRow(selectedRecords.length)) {
					grdProductos.startEditingNew();
				} else{
					SC.say("Llenar los campos correctamente");
					btnGrabar.setDisabled(false);
				}
			}
		}

		public void onKeyPress(KeyPressEvent event) {
			// TODO Auto-generated method stub
			String item = event.getItem().getName();
			if (item.equals("txtNotaCredito")) {
				if (NumDto == 0 ){
						mostrarDocumento(3);
				} else {
						mostrarDocumento(14);
				}
			} else if (item.equals("txtAnticipo")) {
				if (NumDto == 1 || NumDto == 10 || NumDto==27){
						mostrarDocumento(15);
				} else {
						mostrarDocumento(4);
				}
			} else if (item.equals("txtRetencion")) {
				// Aqui debemos mostrar la ventana de ingreso de retenci�n
				agregarRetencion();
				//mostrarDocumento(5);
			}
			if (item.equals("txtCredito")) {
				if ("Enter".equals(event.getKeyName())) {
					Integer numPagos = 0;
					try {
						numPagos = Integer.parseInt(event.getItem().getValue()
								.toString());
						// Aqui creamos los cuadros de texto para que se indique
						// la
						// cantidad y fecha de vencimiento de cada pago.
						crear_Grilla_Pagos(numPagos);

					} catch (Exception e) {
						SC.say("Debe ingresar n\u00fameros");
						event.getItem().clearValue();
					}
					// SC.say(event.getItem().getValue().toString());
				}
			} else {
				/*
				 * if(item.equals("listaRuc") || item.equals("listaNombres")){
				 * if ("Enter".equals(event.getKeyName())) { codigoSeleccionado
				 * = Integer.parseInt(event.getItem().getValue().toString());
				 * CargarCliente(); siListaRuc.redraw();
				 * siListaNombres.redraw(); }} else {
				 * 
				 * if ("Enter".equals(event.getKeyName())) { try{
				 * Integer.parseInt(event.getItem().getValue().toString());
				 * }catch(Exception e){ SC.say("Debe ingresar numeros"); }
				 * 
				 * 
				 * } }
				 */
			}
		}

		// PARA CREAR UNA NUEVA FACTURA A PARTIR DE UNA EXISTENTE SIN PAGOS
		public void GrabarDocumentoNuevo(Double Facturacion) {

			if (validarCantidades().isEmpty()) {
				String error = "";
				String cast = afeccion + "";
				try {
					Double TotalCompleto = Facturacion;
					PagosDTO = null;
					NumPagos = 0;
					if (NumDto == 0 || NumDto == 1 || NumDto == 3
							|| NumDto == 10 || NumDto == 13 || NumDto == 14
							|| NumDto == 26 || NumDto == 27
							|| NumDto == 20) { // TotalCompleto =
												// CValidarDato.getDecimal(2,TotalCompleto);
						String total = txtTotal.getDisplayValue();
						total = total.replace(",", "");
						TotalCompleto = Double.parseDouble(total);
						TotalCompleto = CValidarDato.getDecimal(4,
								TotalCompleto);
					} else {
						try {
							TotalCompleto = Double.parseDouble(txtTotal
									.getDisplayValue());
						} catch (Exception e) {
							String total = txtTotal.getDisplayValue();
							total.replace(",", "");
							TotalCompleto = Double.parseDouble(total);
						}
					}

					if (dfEncabezado.validate()) {
						// Estan correctos los campos del encabezado
						// En caso de ser una nota de credito, se analiza el
						// dfNotaCredito
						if (NumDto == 3 || NumDto == 14) {// Nota de Credito
							if (!dfNotaCredito.validate()) {
								error += "Debe llenar el campo al que corresponde la Nota de Credito";
								error += "\n";
							}
						}
					} else {
						error += "Debe llenar los campos del encabezado correctamente.";
						error += "\n";
					}

					// Ahora vamos a validar las filas de la grilla
					if (NumDto != 7 && NumDto != 4 && NumDto != 12
							&& NumDto != 15) {
						// No se trata de un gasto
						if (grdProductos.getRecordList().getLength() == 0) {
							error += "Debe agregar productos al Documento";
							error += "\n";
						} else {
							for (Integer i = 0; i < grdProductos
									.getRecordList().getLength(); i++) {
								if (!grdProductos.validateRow(i)) {
									error += "Revise los datos del producto de la fila "
											+ i;
									error += "\n";
								}
							}
						}
					}

					if (error.isEmpty()) {
						if (TotalCompleto == 0.0 || confirmar == '0'
								|| NumDto == 20) {// Estan correctos los valores
													// se desbloquea el boton
													// guardar
							/*
							 * confirmar='0'; if
							 * (btnGrabar.getTitle().equals("CONFIRMAR")) {
							 * confirmar = '1';d //En caso de ser una compra se
							 * debe mostrar una ventana para escoger a que
							 * bodega iran los productos }
							 */
//							vendedor = new PersonaDTO();
//							vendedor.setIdPersona(idVendedor);
							

							try {
								persona.setIdPersona(codigoSeleccionado);
							} catch (Exception e) {
								SC.say("Error raro: " + e.getMessage());
								btnGrabar.setDisabled(false);
							}

							if (NumDto == 7 || NumDto == 4 || NumDto == 12
									|| NumDto == 15)// Si es un gasto
							{// El valor de la autorizacion debe lo que contiene
								// el txtaConcepto
								auth = (String) txtaConcepto.getValue();// .getDisplayValue();
							} else {
								auth = txtAutorizacion_sri.getDisplayValue();
								obtenerDetalles();
							}
							if (confirmar == '1') {
								if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27) {
									// En caso de ser una compra debemos
									// recorrer los detalles para saber si el
									// producto esta asignado
									// a la o las bodegas respectivas
									if (btnGrabar.getTitle()
											.equals("CONFIRMAR")) {
										// AQUI DEBEMOS QUITAR LA DOCUMENTACION
										// PARA QUE PERMITA ESCOGER BODEGAS
										asignacionBodegas(0);
									}
								} else {
									// if(NumDto==7){pasarDatos(vendedor,txtaConcepto.getDisplayValue(),confirmar);}
									// else{
									// pasarDatos(vendedor,auth,confirmar);}
									pasarDatos(vendedor, auth, confirmar);
								}
							} else {
								// SC.say("VENDEDOR:"+vendedor.getApellidos()+"AUROTIZACION : "+auth+"MAS"+txtaConcepto.getDisplayValue()+"CONFIRMAR"+confirmar);
								pasarDatos(vendedor, auth, confirmar);
								// SC.say("VERIFICAR EL PASAR DATOS");
							}
						} else {// fin if total completo

							if (TotalCompleto > 0)
								SC.say("La suma del valor de los pagos es menor en "
										+ TotalCompleto + " al total del Doc");
							else
								SC.say("La suma del valor de los pagos es mayor en "
										+ TotalCompleto + " al total del Doc");
							btnGrabar.setDisabled(false);
						}
					}// fin if error is empty
					else {
						btnGrabar.enable();
						String enc = "Primero debe corregir lo siguiente:";
						enc += "\n";
						enc += error;
						SC.say(enc);
						btnGrabar.setDisabled(false);
					}
					// }else{//
					// SC.say("La cantidad recibida no coincide con la cantidad a cancelar, revise las formas de pago");
					// }we
				} catch (Exception e) {
					SC.say("Error : grabardocumento nuevo " + e);
					btnGrabar.setDisabled(false);
				}
			} else {
				SC.say(validarCantidades());
				btnGrabar.setDisabled(false);
			}
		}

		private void obtener_UltimoDtoFactura(String filtro,
				String campoOrdenamiento) {
			// Analizamos de que tipo de documento se trata
			final AsyncCallback<DtocomercialDTO> Funcioncallback = new AsyncCallback<DtocomercialDTO>() {
				// String coincidencias = "";
				public void onSuccess(DtocomercialDTO result) {
					if (result != null) {
						Canvas l= 	hLayout_2.getMember(0);
						hLayout_2.removeMember(layout_2);
						//hLayout_2 =  new HLayout();
						txtAutorizacion_sri.setValue(result.getAutorizacion());
						Integer c = result.getNumRealTransaccion();
						c += 1;
						txtNumero_factura.setValue(c.toString());
						
						VLayout lFormasPago = new VLayout();
						//lFormasPago.addMember(lblTotal_letras);
				
							HLayout lFormas = new HLayout();
							lFormas.addMember(dynamicForm);
							lFormas.addMember(grdPagos);
							lFormas.addMember(dynamicForm2);
							lFormas.addMember(dynamicForm3);
							lFormasPago.addMember(lFormas);
							lFormasPago.setVisible(true);
						//Canvas l= 	hLayout_2.getMember(0);
						//com.google.gwt.user.client.Window.alert("Hacer visible las formas de pago l "+l.getID());
					//	Canvas l = 	hLayout_2.getMember(1);
						//l.setVisible(true);
							layout_2.setVisible(true);
						hLayout_2.addMember(lFormasPago);
						//hLayout_2.addMember(dfPie_1);
						//hLayout_2.addMember(l);
						hLayout_2.addMember(layout_2);
						//layout_2.redraw();
						hLayout_2.redraw();
						btnacept.setVisible(true);
						//com.google.gwt.user.client.Window.alert("Hacer visible las formas de pago "+lFormasPago.isVisible());
						lFormasPago.setVisible(true);
						//com.google.gwt.user.client.Window.alert("Hacer visible las formas de pago "+lFormasPago.isVisible());
						btnTransformar.disable();
					} else {
						// Aqui analizamos si es diferente de gasto
						if (NumDto != 7 && NumDto != 4 && NumDto != 12
								&& NumDto != 15 && NumDto != 20) {
							SC.say("Debe colocar el n\u00famero de Autorizaci\u00f3n");
							btnGrabar.setDisabled(false);
						}
						txtNumero_factura.setValue("1");
					}
				}

				public void onFailure(Throwable caught) {
					SC.say("Error al acceder al servidor: " + caught);
				}
			};
			getService().ultimoDocumento(filtro, campoOrdenamiento,
					Funcioncallback);
		}

		// ++++++++++ PARA OBTENER UNA FACTURA NUEVA A PARTIR DE LA ACTUAL
		// +++++++++++++++
		private void obtener_UltimoDtoFacturaNueva(String filtro,
				String campoOrdenamiento) {
			// Analizamos de que tipo de documento se trata
			final AsyncCallback<DtocomercialDTO> Funcioncallback = new AsyncCallback<DtocomercialDTO>() {
				// String coincidencias = "";
				public void onSuccess(DtocomercialDTO result) {
					if (result != null) {
						txtAutorizacion_sri.setValue(result.getAutorizacion());
						Integer c = result.getNumRealTransaccion();
						c += 1;
						txtNumero_factura.setValue(c.toString());
						getService().fechaServidor(callbackFecha);
						dateFecha_emision.setValue(fechaFactura);
						dynamicForm.getField("chkCredito").setValue(false);
						if(NumDto==0 || NumDto==1) dynamicForm2.getField("chkRetencion").setValue(false);
						dynamicForm2.getField("chkAnticipo").setValue(false);
						dynamicForm2.getField("chkNotaCredito").setValue(false);
						dynamicForm3.getField("chkTarjetaCr").setValue(false);
						dynamicForm3.getField("chkBanco").setValue(false);
						dynamicForm.getField("chkContado").setValue(true);
						dynamicForm.getField("txtContado").show();
						tblRetenciones = new HashSet<RetencionDTO>(0);
						// {
						// dynamicForm.getField("chkContado").setValue(true);
						// dynamicForm.getField("txtContado").setVisible(true);
						GrabarDocumento(total);
						btnTransformar.disable();
					} else {
						// Aqui analizamos si es diferente de gasto
						if (NumDto != 7 && NumDto != 4 && NumDto != 12
								&& NumDto != 15 && NumDto != 20) {
							SC.say("Debe colocar el n\u00famero de Autorizaci\u00f3n");
							btnGrabar.setDisabled(false);
						}
						txtNumero_factura.setValue("1");
					}
				}

				public void onFailure(Throwable caught) {
					SC.say("Error al acceder al servidor: " + caught);
				}
			};
			getService().ultimoDocumento(filtro, campoOrdenamiento,
					Funcioncallback);
		}

		// /******************FIN DE FUNCION GRABAR DOCUMENTO *****************

		public void onChange(ChangeEvent evento) {
			if (indicador.equals("eliminar")) {
				//SC.say("se elimino una celda");
				calculo_Subtotal();
			} else {
				String cadena = evento.getValue().toString();
				if (indicador.equals("descripcion")) {
					//Primero analizamos si ha alcanzado el numero maximo de items
					int maximoItems= grdProductos.getTotalRows();
					if(maximoItems>Factum.banderaNumeroItemsFactura && NumDto==0 && Factum.banderaDividirFactura==0 && Factum.banderaMenuFacturacionElectronica==0)//CALCLAR NUMERO MAXIMO DE ITEMS EN FACTURAS SELECCIONANDO PRODUCTO
					{
						grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
						SC.say("No puede ingresar mas items, debe crear otra factura.");
					}else
						if(maximoItems>Factum.banderaNumeroItemsNotaEntrega && NumDto==2) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA SELECCIONANDO PRODUCTO
					{
						grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
						SC.say("No puede ingresar mas items, debe crear otra nota de entrega.");
					}else if(maximoItems>Factum.banderaNumeroItemsNotaCredito && NumDto==3) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA SELECCIONANDO PRODUCTO
					{
						grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
						SC.say("No puede ingresar mas items, debe crear otra nota de credito.");
					}else if(maximoItems>Factum.banderaNumeroItemsProforma && NumDto==20) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA SELECCIONANDO PRODUCTO
					{
						grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
						SC.say("No puede ingresar mas items, debe crear otra proforma.");
					}else if(maximoItems>Factum.banderaNumeroItemsNotaVentas && NumDto==26) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA SELECCIONANDO PRODUCTO
					{
						grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
						SC.say("No puede ingresar mas items, debe crear otra proforma.");
					}
					else
					{
					// AQUI PRIMERO VAMOS A ANALIZAR SI HA ESCOGIDO EL TIPO DE
					// PRECIO DESEADO
					try {
						/*
						if (cmbTipoPrecio.getDisplayValue().equals("PUBLICO")) {
							tipoP = "2";
						} else if (cmbTipoPrecio.getDisplayValue().equals("AFILIADO")) {
							tipoP = "3";
						} else if (cmbTipoPrecio.getDisplayValue().equals("MAYORISTA")) {
							tipoP = "1";
						}
						*/
						tipoP = cmbTipoPrecio.getValue().toString();
						// tipoP = (String)
						// dfExtras.getItem("cmbTipoPrecio").getValue();
						/*if(chkPedido.getValueAsBoolean()){
							mostrar_mesasOcupadas();
						}else{
							mostrar_productos(1,0);
						}*/
						mostrar_productos(1,0);
					} catch (Exception e) {
						SC.say("Primero Debe Seleccionar el Tipo de Precio "
							+ cmbTipoPrecio.getDisplayValue());
					}
				}
				} else {
					if (indicador.equals("pvp") || indicador.equals("cantidad")
							|| indicador.equals("precioConIva") ) {//|| indicador.equals("descuentoCh")
						//SC.say(indicador+" :"+cadena+" :"+ String.valueOf(cadena.length()));
						/*SC.say(cadena.charAt(cadena.length() - 1)+" ,"+String.valueOf(Character
								.isDigit(cadena.charAt(cadena.length() - 1))));*/
						if (Character
								.isDigit(cadena.charAt(cadena.length() - 1))
								|| (cadena.charAt(cadena.length() - 1)) == '.') {

							// Se debe recalcular el subtotal
							calculo_Subtotal_Individual(cadena, indicador);
							grdProductos.refreshRow(grdProductos.getEditRow());
						} else {
							evento.getItem().setValue(
									(String) evento.getOldValue());
						}
					}

				}
			}
			if (indicador.equals("bodega")) {
				// Aqui vamos a hacer que se muestre la ventana de bodega
				mostrarBodegas(grdProductos
						.getRecord(grdProductos.getEditRow())
						.getAttributeAsString("idProducto"));
			}
		}

		public void onEditorExit(EditorExitEvent event) {
			if (indicador.equals("barras")) {
				try {
					String cod = event.getNewValue().toString();
//					grdProductos.setEditValue(event.getRowNum(),"cantidad","0.0");
					try{
						int maximoItems= grdProductos.getTotalRows();
						if(maximoItems>Factum.banderaNumeroItemsFactura && NumDto==0 && Factum.banderaDividirFactura==0 && Factum.banderaMenuFacturacionElectronica==0)//CALCLAR NUMERO MAXIMO DE ITEMS EN FACTURAS CON CODIGO DE BARRAS
						{
							grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
							SC.say("No puede ingresar mas items, debe crear otra factura.");
						}else if(maximoItems>Factum.banderaNumeroItemsNotaEntrega && NumDto==2) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA CON CODIGO DE BARRAS
						{
							grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
							SC.say("No puede ingresar mas items, debe crear otra nota de entrega.");
						}else if(maximoItems>Factum.banderaNumeroItemsNotaCredito && NumDto==3) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA CON CODIGO DE BARRAS
						{
							grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
							SC.say("No puede ingresar mas items, debe crear otra nota de credito.");
						}else if(maximoItems>Factum.banderaNumeroItemsProforma && NumDto==20) //CALCULAR NUMERO MAXIMO DE ITEMS EN PROFORMA SELECCIONANDO PRODUCTO
						{
							grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
							SC.say("No puede ingresar mas items, debe crear otra proforma.");
						}else if(maximoItems>Factum.banderaNumeroItemsNotaVentas && NumDto==26) //CALCULAR NUMERO MAXIMO DE ITEMS EN PROFORMA SELECCIONANDO PRODUCTO
						{
							grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
							SC.say("No puede ingresar mas items, debe crear otra proforma.");
						}
						else
						{
							tipoP = cmbTipoPrecio.getValue().toString();
							getService().buscarProductoCodBar(cod, "codigoBarras",Integer.valueOf(tipoP),Factum.banderaStockNegativo,callbackProducto);
						}
					}catch(Exception e){
						SC.say("error en codigo de barras");
					}
					

				} catch (Exception e) {
					// SC.say(e.getMessage());
				}

				// SC.say(grdProductos.getSelectedRecord());asd
				//
			}
			if (indicador.equals("piva")) {
//				int iva = grdProductos.getSelectedRecord().getAttributeAsInt(
//						"iva");
				double precio = Double.valueOf(event.getNewValue().toString());
				// SC.say("Hola piva "+String.valueOf(iva)+" "+String.valueOf(precio));

				String cadIva="";
				String[] ivas;
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
//				double valor = grdProductos.getSelectedRecord().getAttributeAsDouble("descuento");
//				SC.say(String.valueOf(grdProductos.getSelectedRecord().getAttributeAsDouble("descuento")));
//				if(valor == 100){
//					SC.say("El valor m�ximo de descuento es de 99.99 piva");	
//					grdProductos.getSelectedRecord().setAttribute("descuento", valor);
//					//valor = 0;
//				}
				
				try {
					cadIva=grdProductos.getSelectedRecord().getAttribute("iva");
					ivas=cadIva.split(",");
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
//						impuestoValor=impuestoValor+((Double.parseDouble(cant[i])*cargaProducto.getAttributeAsDouble("Precio")
//								+impuestoValor)*(Double.parseDouble(ivaS)/100));
//						impuestoValor=impuestoValor+((valTotal* (1 - desc)+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					if (iva == Factum.banderaIVA) {
//						precio = precio / (1+(Factum.banderaIVA/100));
					if ((impuestoPorc-1)>0.0) {
						precio = precio / (impuestoPorc);
						grdProductos.getSelectedRecord().setAttribute(
								"precioUnitario", precio);

					} else 
//						if (iva == 0) 
						{
						grdProductos.getSelectedRecord().setAttribute(
								"precioUnitario", precio);
					}
					// grdProductos.refreshRow(event.getRowNum());
					// grdProductos.saveAllEdits();
					calculo_Individual_Total();
					grdProductos.refreshRow(grdProductos.getEditRow());
				} catch (Exception e) {
					// SC.say(e.getMessage()+" Hola piva "+String.valueOf(iva)+" "+String.valueOf(precio));
				}

			}
			if (indicador.equals("descuentoCh") || indicador.equals("pvpIva")) {
//				double valor = grdProductos.getSelectedRecord().getAttributeAsDouble("descuento");
//				SC.say(String.valueOf(grdProductos.getSelectedRecord().getAttributeAsDouble("descuento")));
//				if(valor == 100){
//					SC.say("El valor m�ximo de descuento es de 99.99");	
//					grdProductos.getSelectedRecord().setAttribute("descuento", valor);
//					//valor = 0;
//				}

				try {

					calculo_Individual_Total();
					//grdProductos.refreshRow(grdProductos.getEditRow());
				} catch (Exception e) {
					// SC.say(e.getMessage()+" Hola piva "+String.valueOf(iva)+" "+String.valueOf(precio));
				}

			}
			if (indicador.equals("pvp") || indicador.equals("cantidad")
					|| indicador.equals("precioConIva")) {
				//com.google.gwt.user.client.Window.alert("Editor exit cantidad");
				calculo_Subtotal();
				//com.google.gwt.user.client.Window.alert("Despues calculo subtotal");
				if (indicador.equals("pvp")){
					grdProductos.getSelectedRecord().setAttribute(
						"precioUnitario", formatoDecimalN.format(grdProductos.getSelectedRecord().getAttributeAsDouble("precioUnitario")));
					//"precioUnitario", formatoDecimalN.format(grdProductos.getSelectedRecord().getAttributeAsDouble("precioUnitario")));
				}
				/*if (indicador.equals("pvp")){
					double precio = Double.valueOf(event.getNewValue().toString());
					//SC.say(grdProductos.getSelectedRecord().getAttribute("precioUnitario"));
					grdProductos.getSelectedRecord().setAttribute(
							"precioUnitario", formatoDecimal.format(precio));
				}*/
				
				grdProductos.refreshRow(grdProductos.getEditRow());
			} else if (indicador.equals("agregarGrilla")|| indicador.equals("agregarObservacion") || indicador.equals("agregarGrillaProductoPedido")) {
				//com.google.gwt.user.client.Window.alert("indicador agregar grilla   ");
				int maximoItems= grdProductos.getTotalRows();
				if(maximoItems>Factum.banderaNumeroItemsFactura && NumDto==0 && Factum.banderaDividirFactura==0 && Factum.banderaMenuFacturacionElectronica==0)//CALCLAR NUMERO MAXIMO DE ITEMS EN FACTURAS CON CODIGO DE BARRAS
				{
					grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
					SC.say("No puede ingresar mas items, debe crear otra factura.");
				}else if(maximoItems>Factum.banderaNumeroItemsNotaEntrega && NumDto==2) //CALCULAR NUMERO MAXIMO DE ITEMS EN NOTAS DE VENTA CON CODIGO DE BARRAS
				{
					grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
					SC.say("No puede ingresar mas items, debe crear otra nota de entrega.");
				}else if(maximoItems>Factum.banderaNumeroItemsProforma && NumDto==20) //CALCULAR NUMERO MAXIMO DE ITEMS EN PROFORMA SELECCIONANDO PRODUCTO
				{
					grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
					SC.say("No puede ingresar mas items, debe crear otra proforma.");
				}else if(maximoItems>Factum.banderaNumeroItemsNotaVentas && NumDto==26) //CALCULAR NUMERO MAXIMO DE ITEMS EN PROFORMA SELECCIONANDO PRODUCTO
				{
					grdProductos.setEditValue(maximoItems- 1, "descripcion", "");
					SC.say("No puede ingresar mas items, debe crear otra proforma.");
				}
				else
				{
					// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					// lstProductos.saveAllEdits();
					int[] rows = new int[1];
					Integer numero = event.getRowNum();
					rows[0] = numero;
					
					// lstProductos.refreshRow(numero);
					Double prueba = 0.0;
					ListGridRecord registroValidacion = (ListGridRecord) event.getRecord();		
					if (linkedhashmapproductos.get(Integer.valueOf(registroValidacion.getAttributeAsString("idProducto"))).getTblbodega().size()!=0){
						lstProductos.saveAllEdits(null, rows);
					try {
						prueba = registroValidacion.getAttributeAsDouble("cantidad") + 0.0;
						CargarProducto(registroValidacion);// 100getSelectedRecord());
						//com.google.gwt.user.client.Window.alert("Despues de cargar producto");
							if(indicador.equals("agregarGrillaProductoPedido")){							
								for (int i=0; i<listpedidoproductoelaboradodto.size();i++) {
									if (listpedidoproductoelaboradodto.get(i).getTblproducto().getCodigoBarras().equals(registroValidacion.getAttributeAsString("codigoBarras"))) {
										listpedidoproductoelaboradodto.get(i).getTblproducto().setCantidadunidad(registroValidacion.getAttributeAsDouble("cantidad"));
										contadorModificarProductoElaborado=contadorModificarProductoElaborado+1;
									}
								}
							}
					} catch (Exception e) {
					}
					}else{
						SC.warn("No se puede realizar transacciones con el producto ya que no se encuentra asignado a una bodega");
						lstProductos.setEditValue(event.getRowNum(),"cantidad",(String)null);
						lstProductos.saveAllEdits(null, rows);
					}
				}	
			}
		}

		// Funcion para controlar el doble click sobre el listado ajax que
		// aparece
		public void onDoubleClick(DoubleClickEvent event) {
			// TODO Auto-generated method stub
			codigoSeleccionado = Integer.parseInt(event.getItem().getValue()
					.toString());
			CargarCliente();
			// siListaRuc.redraw();
			// siListaNombres.redraw();
		}

		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			try {
				// grdProductos.refreshFields();
				grdProductos.removeData(event.getRecord());
				int numProductos = grdProductos.getRecords().length;
				for (int j = 0; j < numProductos; j++) {
					grdProductos.setEditValue(j, "numero", j + 1);
				}
				grdProductos.saveAllEdits();
				grdProductos.refreshFields();
				rowNumGlobal-=1;
				calculo_Subtotal();
			} catch (Exception e) {
			}
		}

		@Override
		public void onChanged(
				com.smartgwt.client.widgets.grid.events.ChangedEvent event) {

		}

	}

	public void crear_Grilla_Pagos(Integer pagos) {
		ListGridRecord[] lPagos;// = new ListGRidRecord[];
		int[] lPagosI;
		if (grdPagos.getTotalRows()<=pagos){
			while (grdPagos.getTotalRows()<pagos){
				grdPagos.startEditingNew();
			}
//			grdPagos.saveAllEdits();
			
//			//com.google.gwt.user.client.Window.alert("Pagos antes de eliminar");
//			grdPagos.endEditing();
			grdPagos.redraw();
			grdPagos.show();
		}else{
			try {
//				grdPagos.cancelEditing();
				grdPagos.saveAllEdits();
				
//				//com.google.gwt.user.client.Window.alert("Pagos antes de eliminar");
				grdPagos.endEditing();
//				for(ListGridRecord rec:grdPagos.getRecords()){
//					rec.setAttribute("cantidad", "0");
//					rec.setAttribute("vencimiento", "1999/12/31");
//					grdPagos.removeData(rec);
//				}
//				//com.google.gwt.user.client.Window.alert(" despues de Primera eliminacion");
//				grdPagos.selectAllRecords();
//				//com.google.gwt.user.client.Window.alert("Seleccionados todos");
//				grdPagos.removeSelectedData();
//				//com.google.gwt.user.client.Window.alert("Remove todos");
				
//				lPagos = grdPagos.getRecords();
//				//com.google.gwt.user.client.Window.alert("Set dato mayor pagos "+lPagos.length);
//				
//				for (int i = 0; i < lPagos.length; i++){
//					//com.google.gwt.user.client.Window.alert("Set dato "+i);
//					grdPagos.setEditValue(i,"cantidad", "0");
//					grdPagos.setEditValue(i,"vencimiento", "1999/12/31");
////					grdPagos.removeData(lPagos[i]);
//				}
				lPagosI =grdPagos.getAllEditRows();
//				//com.google.gwt.user.client.Window.alert("Set dato mayor pagos "+lPagosI.length);
				for (int i:lPagosI){
//					//com.google.gwt.user.client.Window.alert("Set dato "+i);
					grdPagos.setEditValue(i,"cantidadPago", "0");
					grdPagos.setEditValue(i,"vencimiento", "1999/12/31");
					grdPagos.endEditing();
//					grdPagos.removeData(lPagos[i]);
				}
				grdPagos.saveAllEdits();
				grdPagos.endEditing();
//				grdPagos.saveAllEdits();
				grdPagos.selectAllRecords();
				grdPagos.removeSelectedData();
			} catch (Exception e) {
				SC.say("Error JJH: " + e);
			}
			while (grdPagos.getTotalRows()<pagos){
				grdPagos.startEditingNew();
			}
			grdPagos.redraw();
			grdPagos.show();
//		for (int i = 0; i < pagos; i++) {
//			grdPagos.startEditingNew();
//		}
		}
	}

	private class ManejadorDC implements
			com.smartgwt.client.widgets.events.DoubleClickHandler {
		String ind = "";

		// public ListGridRecord registro[] = new ListGridRecord[1];
		public ManejadorDC(String ident) {
			this.ind = ident;
		}

		public void onDoubleClick(
				com.smartgwt.client.widgets.events.DoubleClickEvent event) {
			// if(Indicador == 1)
			// { //El contenedor fue llamado desde la facturacion en el doble
			// click, se debe
			// guardar los datos del producto y cerrar el formulario
			/*
			 * if(ind.equals("productos")) { //Aqui debo llamar a que se me
			 * muestre la ventana para escoger bodega
			 * CargarProducto(VentanalstProductos
			 * .obtenerListado().getSelectedRecord());} else
			 */
			if (ind.equals("documentoPago")) {

				registroPago = listaDocumentos.lstReporteCaja
						.getSelectedRecord();
				if (tipoS.equals("Notas de Credito")) {
					//SC.say(tipoS);
					if (registroPago.getAttributeAsString("Estado").equals(
							"UTILIZADO")) {
						SC.say("El documento ya ha sido utilizado, por favor seleccione otro");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("NO CONFIRMADO")) {
						SC.say("El documento no esta confirmado, por favor primero confirmelo");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("CONFIRMADO")) {
						totalNota = registroPago.getAttributeAsDouble("SubTotal");
//						totalNota = totalNota * (1+(Factum.banderaIVA/100));
						totalNota = totalNota +registroPago.getAttributeAsDouble("iva");
						totalNota = CValidarDato.getDecimal(2, totalNota);
						dynamicForm2.getItem("txtNotaCredito").setHint(
								registroPago.getAttributeAsInt("id") + "");
						// dynamicForm2.getItem("txtNotaCredito").setHint(totalNota+"");
						dynamicForm2.redraw();
						dynamicForm2.getItem("txtNotaCredito").setValue(
								totalNota);
						winDocumento.destroy();
					}
				} else if (tipoS.equals("Anticipo Clientes")) {
					if (registroPago.getAttributeAsString("Estado").equals(
							"UTILIZADO")) {
						SC.say("El documento ya ha sido utilizado, por favor seleccione otro");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("NO CONFIRMADO")) {
						SC.say("El documento no esta confirmado, por favor primero confirmelo");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("CONFIRMADO")) {
						totalAnt = registroPago
								.getAttributeAsDouble("SubTotal");
						registroPago.getAttributeAsString("Estado");
						dynamicForm2.getItem("txtAnticipo").setHint(
								registroPago.getAttributeAsInt("id") + "");
						dynamicForm2.redraw();
						dynamicForm2.getItem("txtAnticipo").setValue(totalAnt);
						// cargarIdDto(lstDocumentos.getSelectedRecord(),
						// "Anticipo");
						winDocumento.destroy();
					}
				} else if (tipoS.equals("Anticipo Proveedores")) {
					if (registroPago.getAttributeAsString("Estado").equals(
							"UTILIZADO")) {
						SC.say("El documento ya ha sido utilizado, por favor seleccione otro");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("NO CONFIRMADO")) {
						SC.say("El documento no esta confirmado, por favor primero confirmelo");
					} else if (registroPago.getAttributeAsString("Estado")
							.equals("CONFIRMADO")) {
						totalAnt = registroPago
								.getAttributeAsDouble("SubTotal");
						registroPago.getAttributeAsString("Estado");
						dynamicForm2.getItem("txtAnticipo").setHint(
								registroPago.getAttributeAsInt("id") + "");
						dynamicForm2.redraw();
						dynamicForm2.getItem("txtAnticipo").setValue(totalAnt);
						// cargarIdDto(lstDocumentos.getSelectedRecord(),
						// "Anticipo");
						winDocumento.destroy();
					}
				}
				/*
				 * ListGridField id =new ListGridField("id", "id");
				 * ListGridField NumRealTransaccion =new
				 * ListGridField("NumRealTransaccion", "Num Real Transaccion");
				 * ListGridFieeld TipoTransaccion= new
				 * ListGridField("TipoTransaccion", "Tipo Transaccion");
				 * ListGridField Fecha =new ListGridField("Fecha", "Fecha");
				 * ListGridField FechadeExpiracion=new
				 * ListGridField("FechadeExpiracion", "Fechade Expiracion");
				 * ListGridField Vendedor=new ListGridField("Vendedor",
				 * "Vendedor"); ListGridField Cliente =new
				 * ListGridField("Cliente", "Cliente"); ListGridField
				 * Autorizacion =new ListGridField("Autorizacion",
				 * "Autorizacion"); ListGridField NumPagos =new
				 * ListGridField("NumPagos", "Numero de Pagos"); ListGridField
				 * Estado =new ListGridField("Estado", "Estado"); ListGridField
				 * SubTotal =new ListGridField("SubTotal", "SubTotal");
				 */
			} else if (ind.equals("bodega")) {
				Bodega = new BodegaDTO(listGrid.getSelectedRecord()
						.getAttributeAsInt("idBodega"), listGrid
						.getSelectedRecord().getAttributeAsString("Bodega"),
						listGrid.getSelectedRecord().getAttributeAsString(
								"Ubicacion"), listGrid.getSelectedRecord()
								.getAttributeAsString("Telefono"),Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento());
				grdProductos.setEditValue(rowNumGlobal, "idbodega", listGrid
						.getSelectedRecord().getAttributeAsInt("idBodega"));
				grdProductos.setEditValue(rowNumGlobal, "bodega", listGrid
						.getSelectedRecord().getAttributeAsInt("Bodega"));
				grdProductos.setEditValue(rowNumGlobal, "ubicacion", listGrid
						.getSelectedRecord().getAttributeAsInt("Ubicacion"));
				grdProductos.setEditValue(rowNumGlobal, "telefono", listGrid
						.getSelectedRecord().getAttributeAsInt("Telefono"));
				grdProductos.saveAllEdits();
				winBodegas.destroy();
			}
		}
	}

	public Factura ObtenerObjeto() {
		return this;
	}

	public void cargarIdDto(ListGridRecord registro, String tipo) {
		// SC.say("TIPO: "+tipo);
		dynamicForm2.getItem("txt" + tipo).setValue(
				registro.getAttributeAsInt("id"));
		VentanalstProductos.destroy();
	}

	public void CargarProductoBarras(ListGridRecord registro) {
		cargaProducto = new ListGridRecord();
		cargaProducto = registro;
		// Analizamos si el producto ya existe en la grilla, en caso de existir
		// solo modificamos la cantidad
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
//		com.google.gwt.user.client.Window.alert(String.valueOf(selectedRecords.length));
		Boolean agregar = true;
		Integer numfila = 0;
//		Integer iva = 0;
		String[] ivas;
		Double precioU = 0.0;
		Double stockAlerta=0.0;
		String observacionAdjunta = "";
		stockAlerta=cargaProducto.getAttributeAsDouble("stock");
		try {
			for (ListGridRecord rec : selectedRecords) {
				if (rec.getAttributeAsString("codigoBarras").equals(cargaProducto.getAttributeAsString("codigoBarras"))) {
					
//					com.google.gwt.user.client.Window.alert("existe");
//					com.google.gwt.user.client.Window.alert(rec.getAttributeAsString("cantidad"));
//					com.google.gwt.user.client.Window.alert(cargaProducto.getAttributeAsString("cantidad"));
					Double can = Double.parseDouble(rec
							.getAttributeAsString("cantidad")) + cargaProducto.getAttributeAsDouble("cantidad");
//					com.google.gwt.user.client.Window.alert("cantidad nueva "+can);
					grdProductos.setEditValue(numfila, "cantidad", (can));
					Double precio = Double.parseDouble(cargaProducto
							.getAttributeAsString("promedio"));
//					com.google.gwt.user.client.Window.alert("precio calculo "+precio);
					precioU = 0.0;
					/*
					if (cmbTipoPrecio.getDisplayValue().equals("MAYORISTA")) {
						precioU = cargaProducto
								.getAttributeAsDouble("Mayorista");
					} else if (cmbTipoPrecio.getDisplayValue().equals(
							"AFILIADO")) {
						precioU = cargaProducto
								.getAttributeAsDouble("Afiliado");

					} else if (cmbTipoPrecio.getDisplayValue()
							.equals("PUBLICO")) {
						precioU = cargaProducto.getAttributeAsDouble("PVP");
					}*/
					precioU = cargaProducto.getAttributeAsDouble("Precio");
					if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27)
						precioU = precio;
					
					String cadIva=cargaProducto.getAttributeAsString("impuesto");
					ivas=cadIva.split(",");
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
//					com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((can*cargaProducto.getAttributeAsDouble("Precio")
								+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
//					com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					iva = Integer.parseInt(cargaProducto
//							.getAttributeAsString("impuesto"));
					if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20 /*|| NumDto == 26*/) {
						if ((impuestoPorc-1)>0.0)
							precioU = precioU / impuestoPorc;
//						if (iva == Factum.banderaIVA)
//							precioU = precioU / (1+(Factum.banderaIVA/100));
					}
//					com.google.gwt.user.client.Window.alert("PrecioU "+precioU);
					//XAVIER ZEAS precioU = CValidarDato.getDecimal(2, precioU);
					precioU = CValidarDato.getDecimal(5, precioU);
//					com.google.gwt.user.client.Window.alert("PrecioU 2 "+precioU);
//					com.google.gwt.user.client.Window.alert("idsProducto en if "+String.valueOf(!cargaProducto.getAttributeAsString("idProducto").
//							equals(rec.getAttributeAsString("idProducto"))));
					if (!cargaProducto.getAttributeAsString("idProducto").equals(rec.getAttributeAsString("idProducto"))){
//						com.google.gwt.user.client.Window.alert("idsProducto en if "+String.valueOf(!cargaProducto.getAttributeAsString("idProducto").
//								equals(rec.getAttributeAsString("idProducto"))));
						grdProductos.setEditValue(numfila, "cantidad", cargaProducto.getAttributeAsDouble("cantidad"));
						grdProductos.setEditValue(numfila,"idProducto",
								cargaProducto.getAttributeAsString("idProducto"));
//						
//						com.google.gwt.user.client.Window.alert("despues idproducto despues observacion valortotal: 0");
						grdProductos.setEditValue(numfila,"valorTotal", 0.0);
//						com.google.gwt.user.client.Window.alert("despues idproducto despues valortotal cadiva "+cadIva);
						grdProductos.setEditValue(numfila,"iva", cadIva);
//						com.google.gwt.user.client.Window.alert("despues idproducto despues cadiva costo"+cargaProducto
//								.getAttributeAsString("promedio"));
						grdProductos.setEditValue(numfila,"costo", Double.parseDouble(cargaProducto
								.getAttributeAsString("promedio")));
						
						double precioConIva = 0.0;
//						precioConIva = precioU + (precioU * iva / 100);
//						com.google.gwt.user.client.Window.alert("despues idproducto precioiva 1 preciou "+precioU+" impuestpPorc "+impuestoPorc);
						precioConIva = precioU *impuestoPorc;
//						com.google.gwt.user.client.Window.alert("despues idproducto precioiva 2 "+precioConIva);
//						precioConIva = Math.rint(precioConIva * 100) / 100;
						precioConIva = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,precioConIva );
						NumberFormat formato2 = NumberFormat.getFormat("#.00");
						grdProductos.setEditValue(numfila,"precioConIva", formato2.format(precioConIva));
//						com.google.gwt.user.client.Window.alert("despues idproducto precioConiva fin");
					}
					
					grdProductos.setEditValue(numfila, "precioUnitario",
							precioU);
					grdProductos.setEditValue(numfila, "descripcion",
							cargaProducto.getAttributeAsString("descripcion")+observacionAdjunta);
					grdProductos.setEditValue(numfila, "stock",
							cargaProducto.getAttributeAsDouble("stock"));
//					Cambios para controlar el numero de decimales que muestra el stock
//					grdProductos.setEditValue(numfila, "stock",
//							CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
//									.getAttributeAsString("stock"))) );
//					resultMap.put("stock", CValidarDato.getDecimal(2, Double.parseDouble(cargaProducto
//							.getAttributeAsString("stock"))) );
					
					Double desc=cargaProducto.getAttributeAsDouble("DTO");
					if (desc!=null) grdProductos.setEditValue(numfila, "descuento",
							cargaProducto.getAttributeAsDouble("DTO"));
					grdProductos.refreshRow(numfila);
					int nuevaCelda = grdProductos.getTotalRows();
					grdProductos.setEditValue(nuevaCelda - 1, "codigoBarras", "");
					grdProductos.startEditing(nuevaCelda - 1, 2, false);
					grdProductos.saveAllEdits();
					grdProductos.refreshFields();
					calculo_Individual_Total();
					/*
					 * calculo_Subtotal_Individual(cargaProducto.
					 * getAttributeAsString("cantidad"), "cantidad");
					 * calculo_Subtotal_Individual(precioU.toString(), "pvp");
					 */
					calculo_Subtotal();
					grdProductos.saveAllEdits();
					// SC.say(String.valueOf(nuevaCelda));
					agregar = false;
					break;
				}
				calculo_Subtotal();
				grdProductos.saveAllEdits();
				numfila++;
			}
		} catch (Exception e) {
			SC.say("Error" +e.getLocalizedMessage()+" message "+e.getMessage());
			// grdProductos.refreshRow(numfila);

			grdProductos.refreshFields();
			calculo_Individual_Total();
			/*
			 * calculo_Subtotal_Individual(cargaProducto.getAttributeAsString(
			 * "cantidad"), "cantidad");
			 * calculo_Subtotal_Individual(precioU.toString(), "pvp");
			 */
			calculo_Subtotal();
			int nuevaCelda = grdProductos.getTotalRows();
			grdProductos.setEditValue(nuevaCelda - 1, "codigoBarras", "");
			grdProductos.startEditing(nuevaCelda - 1, 2, false);
			grdProductos.saveAllEdits();
			// SC.say("try "+String.valueOf(nuevaCelda)+" "+e.getMessage());

			agregar = false;
		}

		if (agregar) {

			String cantidad = "";
			// String observacionAdjunta = "";
			try {
//				if (!cargaProducto.getAttributeAsString("impuesto").isEmpty())
//					iva = Integer.parseInt(cargaProducto.getAttributeAsString("impuesto"));
				
				String cadIva="";
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				if (!cargaProducto.getAttributeAsString("impuesto").isEmpty()){
					cadIva=cargaProducto.getAttributeAsString("impuesto");
					ivas=cadIva.split(",");
					
//					com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((Double.parseDouble(cargaProducto.getAttributeAsString("cantidad"))*cargaProducto.getAttributeAsDouble("Precio")
								+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
//					com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				}
				
				NumberFormat formato = NumberFormat.getFormat("###0.000");
				Map<String, Object> resultMap = new HashMap<String, Object>();// Utils.getMapFromRow(dsFields,
																				// getResultRow())
				resultMap.put("idProducto",
						cargaProducto.getAttributeAsString("idProducto"));
				resultMap.put("codigoBarras",
						cargaProducto.getAttributeAsString("codigoBarras"));
				cantidad = cargaProducto.getAttributeAsString("cantidad");
//				com.google.gwt.user.client.Window.alert("Cantidad en cargar producto: "+cantidad);
				resultMap.put("cantidad", cantidad);
				try {
					if (!cargaProducto.getAttributeAsString("observacion")
							.isEmpty()) {
						observacionAdjunta = "-"
								+ cargaProducto
										.getAttributeAsString("observacion");
					}
				} catch (Exception e) {

				}
				resultMap.put("descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);
				// COSTO

				Double precio = Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio"));

				precioU = 0.0;
				/*
				if (cmbTipoPrecio.getDisplayValue().equals("MAYORISTA")) {
					precioU = cargaProducto.getAttributeAsDouble("Mayorista");
				} else if (cmbTipoPrecio.getDisplayValue().equals("AFILIADO")) {
					precioU = cargaProducto.getAttributeAsDouble("Afiliado");

				} else if (cmbTipoPrecio.getDisplayValue().equals("PUBLICO")) {
					precioU = cargaProducto.getAttributeAsDouble("PVP");
				}
				*/
				precioU = cargaProducto.getAttributeAsDouble("Precio");
				
				if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27) {
					precioU = precio;
				}
				if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20 ) {
					if ((impuestoPorc-1)>0.0)
						precioU = precioU / impuestoPorc;
//					if (iva == Factum.banderaIVA)
//						precioU = precioU / (1+(Factum.banderaIVA/100));
				}
				
				precioU = CValidarDato.getDecimal(4, precioU);
				double precioConIva = 0.0;
//				precioConIva = precioU + (precioU * iva / 100);
				precioConIva = precioU *impuestoPorc;
//				precioConIva = Math.rint(precioConIva * 100) / 100;
				precioConIva = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,precioConIva );
				NumberFormat formato2 = NumberFormat.getFormat("#.00");
				// precioConIva=CValidarDato.getDecimal(2,precioConIva);
				resultMap.put("precioUnitario", formato.format(precioU));
				resultMap.put("valorTotal", 0.0);
				resultMap.put("iva", cadIva);
				
				resultMap.put("stock", Double.parseDouble(cargaProducto
						.getAttributeAsString("stock")));
				
//				Cambios para controlar el numero de decimales que muestra el stock
//				resultMap.put("stock", CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
//						.getAttributeAsString("stock"))));

				
				resultMap.put("descuento", 0);
				resultMap.put("costo", Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio")));
				resultMap.put("precioConIva", formato2.format(precioConIva));
				resultMap.put("numero", rowNumGlobal + 1);
				//Integer totalFilas = grdProductos.getRecords().length;
				grdProductos.setEditValues(rowNumGlobal, resultMap);
				grdProductos.refreshRow(rowNumGlobal);
				grdProductos.refreshCell(rowNumGlobal, 0);
				grdProductos.saveAllEdits();
				grdProductos.refreshFields();
				calculo_Individual_Total();
				//grdProductos.saveAllEdits();
				//calculo_Subtotal();
				grdProductos.startEditingNew();
				//int nuevaCelda = grdProductos.getTotalRows();
				grdProductos.startEditing(rowNumGlobal+1, 2, false);
				//grdProductos.saveAllEdits();
				grdProductos.saveAllEdits();
				
				mostrarBodegas(cargaProducto.getAttributeAsString("idProducto"));
				//grdProductos.refreshFields();
				
				// listadoProd.destroy();*/
			} catch (Exception e) {
				SC.say("Error : ingrese nuevamente la cantidad ");
				
				calculo_Individual_Total();
				calculo_Subtotal();
				grdProductos.saveAllEdits();
				grdProductos.refreshFields();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");
			}

		}
		// getService().obtenerPrecio(registro.getAttributeAsInt("idProducto"),
		// Integer.parseInt(tipoP), callbackPrecio);
	}

	
	
	
	/**
	 * Esta funci�n es la encargada de grabar los documentos
	 */
	public void GrabarDocumento(Double Facturacion) {
		if (validarCantidades().isEmpty()) {
//			//com.google.gwt.user.client.Window.alert("Luego de validar cantidades ");
			// Primero se analizaran las formas de pago seleccionadas
			// y se validara las cantidades o documentos asociadas a las formas
			// de pago
			// 1 Egreso de dinero
			// 0 Ingreso de dinero
			PagosAsociados = new HashSet<DtocomercialTbltipopagoDTO>(0);
			String error = "";
			String cast = afeccion + "";
			char tipo = cast.charAt(0);
			try {

				// String[5];
//				//com.google.gwt.user.client.Window.alert("En try para grabar 1");
				Double TotalCompleto = Facturacion;
				Double formap = 0.0;
				PagosDTO = null;
				NumPagos = 0;
//				//com.google.gwt.user.client.Window.alert("Numero de pagos ");
				if (NumDto == 0 || NumDto == 1 || NumDto == 3 || NumDto == 10
						|| NumDto == 13 || NumDto == 14 || NumDto == 20 ||  NumDto==27) { // TotalCompleto
//					//com.google.gwt.user.client.Window.alert("if de total completo");
					// =
					// CValidarDato.getDecimal(2,TotalCompleto);
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					TotalCompleto = Double.parseDouble(total);
					TotalCompleto = CValidarDato.getDecimal(4, TotalCompleto);
//					//com.google.gwt.user.client.Window.alert("fin if total completo ");

				} else {
//					//com.google.gwt.user.client.Window.alert("si no total completo ");
					try {
						TotalCompleto = Double.parseDouble(txtTotal
								.getDisplayValue());
					} catch (Exception e) {
						String total = txtTotal.getDisplayValue();
						total.replace(",", "");
						TotalCompleto = Double.parseDouble(total);
						btnGrabar.setDisabled(false);
						if (NumDto==20) btnacept.setDisabled(false);
					}
//					//com.google.gwt.user.client.Window.alert("fin si no total completo ");
				}
				if(NumDto == 4 || NumDto == 15 || NumDto==3 || NumDto==14){
					
					String[] fPagos = { "Contado", "TarjetaCr", "Banco" };// new
					// String[5];
					for (int i = 0; i < 3; i++) {
						if (i == 0) {
							try {
								if ((Boolean) dynamicForm
										.getItem("chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Contado")){
											formap = Double.parseDouble(dynamicForm
													.getItem("txt" + fPagos[i])
													.getDisplayValue());
											formap = CValidarDato.getDecimal(2,
													formap);
											TotalCompleto -= formap;
											DtocomercialTbltipopagoDTO pagoTipo = new DtocomercialTbltipopagoDTO(
													1/* "CONTADO" */, formap, 0, "",
													tipo);
											PagosAsociados.add(pagoTipo);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} 
									} catch (Exception e) {
										error += "Se ha producido un error al extraer el valor de Contado/Credito";
										error += "\n";
										btnGrabar.setDisabled(false);
										if (NumDto==20) btnacept.setDisabled(false);
										return;
										// SC.say("Se ha producido un error1: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
									}
								}
							} catch (Exception e) {
								SC.say("Error d1: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								return;
							}
						} else if (i >= 1 && i < 3) {
							try {
								if ((Boolean) dynamicForm3.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("TarjetaCr")) // ||
											// fPagos[i].equals("Banco")
										{
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoTar = new DtocomercialTbltipopagoDTO(
													6/* "TARJETA" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoTar);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Banco")) {
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoChq = new DtocomercialTbltipopagoDTO(
													7/* "BANCO" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoChq);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes valores: Tarjeta de Credito o Banco.";
										error += "\n";
										btnGrabar.setDisabled(false);
										if (NumDto==20) btnacept.setDisabled(false);
										return;
										// SC.say("Se ha producido un error3: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d3: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								return;
							}
						}
					}
				}else if (NumDto == 0 || NumDto==1){
//					//com.google.gwt.user.client.Window.alert("if Factura procesar pagos");
					String[] fPagos = { "Contado", "Credito", "Retencion",
							"Anticipo", "NotaCredito", "TarjetaCr", "Banco" };// new
					// String[5];
					for (int i = 0; i < 7; i++) {
						if (i < 2) {
//							//com.google.gwt.user.client.Window.alert("if Factura procesar pagos contado credito "+fPagos[i]);
							try {
								if ((Boolean) dynamicForm
										.getItem("chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Contado"))// ||
											// fPagos[i].equals("TarjetaCr")
											// ||
											// fPagos[i].equals("Banco"))
										{
											formap = Double.parseDouble(dynamicForm
													.getItem("txt" + fPagos[i])
													.getDisplayValue());
											formap = CValidarDato.getDecimal(2,
													formap);
											TotalCompleto -= formap;
											DtocomercialTbltipopagoDTO pagoTipo = new DtocomercialTbltipopagoDTO(
													1/* "CONTADO" */, formap, 0, "",
													tipo);
											PagosAsociados.add(pagoTipo);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Credito")) {
											// Llamamos a la funcion para recorrer
											// los pagos y grabar su fecha y valor
											obtenerPagos();
//											//com.google.gwt.user.client.Window.alert("totalcredito variable "+totalCredito);
											totalCredito = CValidarDato.getDecimal(
													2, totalCredito);
											TotalCompleto -= totalCredito;
											DtocomercialTbltipopagoDTO pagoContado = new DtocomercialTbltipopagoDTO(
													2/* "CREDITO" */, totalCredito,
													0, "", tipo);
											PagosAsociados.add(pagoContado);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer el valor de Contado/Credito";
										error += "\n";
										btnGrabar.setDisabled(false);
										if (NumDto==20) btnacept.setDisabled(false);
										return;
										// SC.say("Se ha producido un error1: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
									}
								}
							} catch (Exception e) {
								SC.say("Error d1: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								return;
							}
//							//com.google.gwt.user.client.Window.alert("fin if Factura procesar pagos contado credito");
						} else if (i >= 2 && i < 5) {
//							//com.google.gwt.user.client.Window.alert("if Factura procesar pagos retencion anticipo notacredito");
							try {
								if ((Boolean) dynamicForm2.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Retencion")) {
											formap = Double
													.parseDouble(dynamicForm2
															.getItem("txtRetencion")
															.getDisplayValue()
															.toString());
											DtocomercialTbltipopagoDTO pagoRet = new DtocomercialTbltipopagoDTO(
													3, formap, 0, "", tipo);
											// PagosAsociados.add(pagoRet);
											// formap =
											// CValidarDato.getDecimal(4,totalRet);
											formap = CValidarDato.getDecimal(2,
													formap);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Anticipo")) {

											Anticipo = Integer
													.parseInt(dynamicForm2.getItem(
															"txtAnticipo")
															.getHint());
//											Integer tipoAnt=(NumDto == 0)?4:9;
											DtocomercialTbltipopagoDTO pagoAnt = new DtocomercialTbltipopagoDTO(
													4/* "ANTICIPO" */, totalAnt,
//													tipoAnt/* "ANTICIPO" */, totalAnt,
													Anticipo, "", tipo);
											PagosAsociados.add(pagoAnt);
											formap = CValidarDato.getDecimal(2,
													totalAnt);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
										// TotalCompleto-= ***VALOR ***;
										else if (fPagos[i].equals("NotaCredito")) {
											NotaCredito = Integer
													.parseInt(dynamicForm2.getItem(
															"txtNotaCredito")
															.getHint());
											DtocomercialTbltipopagoDTO pagoNota = new DtocomercialTbltipopagoDTO(
													5/* "NOTA" */, totalNota,
													NotaCredito, "", tipo);
											PagosAsociados.add(pagoNota);
											formap = CValidarDato.getDecimal(2,
													totalNota);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
										// TotalCompleto-= ***VALOR ***;
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes "
												+ "valores: Retencion, Anticipo o Nota de Credito.";
										error += "\n";
										btnGrabar.setDisabled(false);
										if (NumDto==20) btnacept.setDisabled(false);
										// SC.say("Se ha producido un error2: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										return;
										//break;
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d2: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								return;
								//break;
							}
//							//com.google.gwt.user.client.Window.alert("end if Factura procesar pagos retencion anticipo notacredito");
						} else if (i >= 5 && i < 7) {
//							//com.google.gwt.user.client.Window.alert("if Factura procesar pagos tarjeta banco");
							try {
								if ((Boolean) dynamicForm3.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("TarjetaCr")) // ||
											// fPagos[i].equals("Banco")
										{
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoTar = new DtocomercialTbltipopagoDTO(
													6/* "TARJETA" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoTar);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Banco")) {
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoChq = new DtocomercialTbltipopagoDTO(
													7/* "BANCO" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoChq);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes valores: Tarjeta de Credito o Banco.";
										error += "\n";
										btnGrabar.setDisabled(false);
										if (NumDto==20) btnacept.setDisabled(false);
										return;
										// SC.say("Se ha producido un error3: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d3: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								return;
							}
//							//com.google.gwt.user.client.Window.alert("end if Factura procesar pagos retencion anticipo notacredito");
						}
					}
				}
				else if (NumDto == 26 || NumDto == 27){
					String[] fPagos = { "Contado", "Credito", 
							"Anticipo", "TarjetaCr", "Banco" };// new
					// String[5];
					for (int i = 0; i < 7; i++) {
						if (i < 2) {
							try {
								if ((Boolean) dynamicForm
										.getItem("chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Contado"))// ||
											// fPagos[i].equals("TarjetaCr")
											// ||
											// fPagos[i].equals("Banco"))
										{
											formap = Double.parseDouble(dynamicForm
													.getItem("txt" + fPagos[i])
													.getDisplayValue());
											formap = CValidarDato.getDecimal(2,
													formap);
											TotalCompleto -= formap;
											DtocomercialTbltipopagoDTO pagoTipo = new DtocomercialTbltipopagoDTO(
													1/* "CONTADO" */, formap, 0, "",
													tipo);
											PagosAsociados.add(pagoTipo);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Credito")) {
											// Llamamos a la funcion para recorrer
											// los pagos y grabar su fecha y valor
											obtenerPagos();
											totalCredito = CValidarDato.getDecimal(
													2, totalCredito);
											TotalCompleto -= totalCredito;
											DtocomercialTbltipopagoDTO pagoContado = new DtocomercialTbltipopagoDTO(
													2/* "CREDITO" */, totalCredito,
													0, "", tipo);
											PagosAsociados.add(pagoContado);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer el valor de Contado/Credito";
										error += "\n";
										btnGrabar.setDisabled(false);
										return;
										// SC.say("Se ha producido un error1: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
									}
								}
							} catch (Exception e) {
								SC.say("Error d1: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
							}
						} else if (i >= 2 && i < 3) {
							try {
								if ((Boolean) dynamicForm2.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Anticipo")) {

											Anticipo = Integer
													.parseInt(dynamicForm2.getItem(
															"txtAnticipo")
															.getHint());
											DtocomercialTbltipopagoDTO pagoAnt = new DtocomercialTbltipopagoDTO(
													4/* "ANTICIPO" */, totalAnt,
													Anticipo, "", tipo);
											PagosAsociados.add(pagoAnt);
											formap = CValidarDato.getDecimal(2,
													totalAnt);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
										// TotalCompleto-= ***VALOR ***;
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes "
												+ "valores: Retencion, Anticipo o Nota de Credito.";
										error += "\n";
										btnGrabar.setDisabled(false);
										// SC.say("Se ha producido un error2: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										return;
										//break;
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d2: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
								//break;
							}
						} else if (i >= 3 && i < 5) {
							try {
								if ((Boolean) dynamicForm3.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("TarjetaCr")) // ||
											// fPagos[i].equals("Banco")
										{
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoTar = new DtocomercialTbltipopagoDTO(
													6/* "TARJETA" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoTar);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Banco")) {
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoChq = new DtocomercialTbltipopagoDTO(
													7/* "BANCO" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoChq);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes valores: Tarjeta de Credito o Banco.";
										error += "\n";
										btnGrabar.setDisabled(false);
										return;
										// SC.say("Se ha producido un error3: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d3: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
							}
						}
					}
				}
				else if (NumDto != 20){
					String[] fPagos = { "Contado", "Credito", 
							"Anticipo", "NotaCredito", "TarjetaCr", "Banco" };// new
					// String[5];
					for (int i = 0; i < 7; i++) {
						if (i < 2) {
							try {
								if ((Boolean) dynamicForm
										.getItem("chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Contado"))// ||
											// fPagos[i].equals("TarjetaCr")
											// ||
											// fPagos[i].equals("Banco"))
										{
											formap = Double.parseDouble(dynamicForm
													.getItem("txt" + fPagos[i])
													.getDisplayValue());
											formap = CValidarDato.getDecimal(2,
													formap);
											TotalCompleto -= formap;
											DtocomercialTbltipopagoDTO pagoTipo = new DtocomercialTbltipopagoDTO(
													1/* "CONTADO" */, formap, 0, "",
													tipo);
											PagosAsociados.add(pagoTipo);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Credito")) {
											// Llamamos a la funcion para recorrer
											// los pagos y grabar su fecha y valor
											obtenerPagos();
											totalCredito = CValidarDato.getDecimal(
													2, totalCredito);
											TotalCompleto -= totalCredito;
											DtocomercialTbltipopagoDTO pagoContado = new DtocomercialTbltipopagoDTO(
													2/* "CREDITO" */, totalCredito,
													0, "", tipo);
											PagosAsociados.add(pagoContado);
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer el valor de Contado/Credito";
										error += "\n";
										btnGrabar.setDisabled(false);
										return;
										// SC.say("Se ha producido un error1: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
									}
								}
							} catch (Exception e) {
								SC.say("Error d1: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
							}
						} else if (i >= 2 && i < 4) {
							try {
								if ((Boolean) dynamicForm2.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("Anticipo")) {

											Anticipo = Integer
													.parseInt(dynamicForm2.getItem(
															"txtAnticipo")
															.getHint());
											DtocomercialTbltipopagoDTO pagoAnt = new DtocomercialTbltipopagoDTO(
													4/* "ANTICIPO" */, totalAnt,
													Anticipo, "", tipo);
											PagosAsociados.add(pagoAnt);
											formap = CValidarDato.getDecimal(2,
													totalAnt);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
										// TotalCompleto-= ***VALOR ***;
										else if (fPagos[i].equals("NotaCredito")) {
											NotaCredito = Integer
													.parseInt(dynamicForm2.getItem(
															"txtNotaCredito")
															.getHint());
											DtocomercialTbltipopagoDTO pagoNota = new DtocomercialTbltipopagoDTO(
													5/* "NOTA" */, totalNota,
													NotaCredito, "", tipo);
											PagosAsociados.add(pagoNota);
											formap = CValidarDato.getDecimal(2,
													totalNota);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
										// TotalCompleto-= ***VALOR ***;
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes "
												+ "valores: Retencion, Anticipo o Nota de Credito.";
										error += "\n";
										btnGrabar.setDisabled(false);
										// SC.say("Se ha producido un error2: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										return;
										//break;
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d2: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
								//break;
							}
						} else if (i >= 4 && i < 6) {
							try {
								if ((Boolean) dynamicForm3.getItem(
										"chk" + fPagos[i]).getValue()) {
									try {
										if (fPagos[i].equals("TarjetaCr")) // ||
											// fPagos[i].equals("Banco")
										{
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoTar = new DtocomercialTbltipopagoDTO(
													6/* "TARJETA" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoTar);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										} else if (fPagos[i].equals("Banco")) {
											formap = Double
													.parseDouble(dynamicForm3
															.getItem(
																	"txt"
																			+ fPagos[i])
															.getDisplayValue());
											DtocomercialTbltipopagoDTO pagoChq = new DtocomercialTbltipopagoDTO(
													7/* "BANCO" */,
													formap,
													0,
													dynamicForm3
													.getItem(
															"txt"
																	+ fPagos[i]
																			+ "1")
													.getDisplayValue(),
													tipo);
											PagosAsociados.add(pagoChq);
											TotalCompleto -= formap;
											TotalCompleto = CValidarDato
													.getDecimal(2, TotalCompleto);
										}
									} catch (Exception e) {
										error += "Se ha producido un error al extraer alguno de los siguientes valores: Tarjeta de Credito o Banco.";
										error += "\n";
										btnGrabar.setDisabled(false);
										return;
										// SC.say("Se ha producido un error3: "+e.getStackTrace()
										// + " ----- " + e.getMessage());
										// Error de validacion
									}
								}
							} catch (Exception e) {
								SC.say("Error d3: " + e + fPagos[i]);
								btnGrabar.setDisabled(false);
								return;
							}
						}
					}
				}
				// AQUI DEBEMOS ANALIZAR SI LOS CAMPOS ESTAN CORRECTOS

				//
				//
				// **********************************0**************************************
				//
				//
//				//com.google.gwt.user.client.Window.alert("validar campos");
				if (dfEncabezado.validate()) {
					// Estan correctos los campos del encabezado
					// En caso de ser una nota de credito, se analiza el
					// dfNotaCredito
					if (NumDto == 3 || NumDto == 14) {// Nota de Credito
						if (!dfNotaCredito.validate()) {
							error += "Debe llenar el campo al que corresponde la Nota de Credito";
							error += "\n";
						}
					}
				} else {
					error += "Debe llenar los campos del encabezado correctamente.";
					error += "\n";
				}
//				//com.google.gwt.user.client.Window.alert("validar productos");
				// Ahora vamos a validar las filas de la grilla
				if (NumDto != 7 && NumDto != 4 && NumDto != 12 && NumDto != 15) {
					// No se trata de un gasto

					if (grdProductos.getRecordList().getLength() == 0) {
						error += "Debe agregar productos al Documento";
						error += "\n";
					} else {
						for (Integer i = 0; i < grdProductos.getRecordList()
								.getLength(); i++) {
							if (!grdProductos.validateRow(i)) {
								error += "Revise los datos del producto de la fila "
										+ i;
								error += "\n";
							}
						}

					}
				}
//				//com.google.gwt.user.client.Window.alert("totales y pasar datos");
				if (error.isEmpty()) {
					if (NumDto == 20) {

						TotalCompleto = 0.0;
					}
					if (TotalCompleto == 0.0
							|| anulacion<0
							)// || confirmar == '0')
					{// Estan correctos los valores se desbloquea el boton
						// guardar

						//						vendedor = new PersonaDTO();
						//						vendedor.setIdPersona(idVendedor);


						try {
							persona.setIdPersona(codigoSeleccionado);


							if (NumDto == 7 || NumDto == 4 || NumDto == 12
									|| NumDto == 15)// Si es un gasto
							{// El valor de la autorizacion debe lo que contiene el
								// txtaConcepto
								auth = (String) txtaConcepto.getValue();// .getDisplayValue();
							} else {

								auth = txtAutorizacion_sri.getDisplayValue();
								obtenerDetalles();
							}
							if (indicadorDetalles.isEmpty()) {

								if (confirmar == '1') {
									if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27) {
										// En caso de ser una compra debemos
										// recorrer los detalles para saber si el
										// producto esta asignado
										// a la o las bodegas respectivas
										if (btnGrabar.getTitle()
												.equals("CONFIRMAR")) {
											// AQUI DEBEMOS QUITAR LA DOCUMENTACION
											// PARA QUE PERMITA ESCOGER BODEGAS
											asignacionBodegas(0);

										}
									} else {
										// if(NumDto==7){
										// pasarDatos(vendedor,txtaConcepto.getDisplayValue(),confirmar);}
										// else{pasarDatos(vendedor,auth,confirmar);}

										pasarDatos(vendedor, auth, confirmar);
									}
								} else {
									// SC.say("VENDEDOR:"+vendedor.getApellidos()+"AUTOR: "+auth+"CONFIRMAR"+confirmar);

									pasarDatos(vendedor, auth, confirmar);
									// SC.say("VERIFICAR EL PASAR DATOS");
								}
							} else {
								SC.say("Verifique los productos, hubo un problema al extraerlos.");
								btnGrabar.setDisabled(false);
								if (NumDto==20) btnacept.setDisabled(false);
								DOM.setStyleAttribute(RootPanel.get("cargando")
										.getElement(), "display", "none");
								return;
							}

						} catch (Exception e) {
							SC.say("Error raro: " + e.getMessage());
							btnGrabar.setDisabled(false);
							if (NumDto==20) btnacept.setDisabled(false);
							return;
						}

					} else {// fin if total completo

						if (TotalCompleto > 0)
							SC.say("La suma del valor de los pagos es menor en "
									+ TotalCompleto + " al total del Documento");
						else
							SC.say("La suma del valor de los pagos es mayor en "
									+ TotalCompleto + " al total del Documento");
						btnGrabar.setDisabled(false);
					}

				}// fin if error is empty
				else {
					String enc = "Primero debe corregir lo siguiente:";
					enc += "\n";
					enc += error;
					SC.say(enc);
					btnGrabar.setDisabled(false);
					if (NumDto==20) btnacept.setDisabled(false);
				}
//				//com.google.gwt.user.client.Window.alert("despues procesar datos");
				// }else{//
				// SC.say("La cantidad recibida no coincide con la cantidad a cancelar, revise las formas de pago");
				// }we
			} catch (Exception e) {
				SC.say("Error : grabar documento " + e);
				btnGrabar.setDisabled(false);
				if (NumDto==20) btnacept.setDisabled(false);
				return;
			}
		} else {
			SC.say(validarCantidades());
			btnGrabar.setDisabled(false);
			if (NumDto==20) btnacept.setDisabled(false);
			return;
		}
	}
	//Boolean terminadoParte=true;
	List<DocumentoSaveDTO> documentos= new ArrayList<DocumentoSaveDTO>();
	public void GrabarDocumento(){
		documentos= new ArrayList<DocumentoSaveDTO>();
		
		String error = "";
		anulacion=0;
		if (!dfEncabezado.validate()) {
			error += "Debe llenar los campos del encabezado correctamente.";
			error += "\n";
		}
		if (grdProductos.getRecordList().getLength() == 0) {
			error += "Debe agregar productos al Documento";
			error += "\n";
		} else {
			for (Integer i = 0; i < grdProductos.getRecordList()
					.getLength(); i++) {
				if (!grdProductos.validateRow(i)) {
					error += "Revise los datos del producto de la fila "
							+ i;
					error += "\n";
				}
			}

		}
		if(error.isEmpty()){
			persona.setIdPersona(codigoSeleccionado);
			auth = txtAutorizacion_sri.getDisplayValue();
			serialParaOrdenProds = 0;
			indicadorDetalles = "";
			grdProductos.saveAllEdits();
			
			try {
				int k=0;
				int maximoItems= grdProductos.getTotalRows()-1;
				Integer  numeroFact=Integer.parseInt(txtNumero_factura.getDisplayValue())-1;
				Integer limit=0;
				ListGridRecord[] selectedRecords = grdProductos.getRecords();
				DtoComDetalleDTO detallesDto = new DtoComDetalleDTO();
				JSONObject detalleJSON = new JSONObject();
				NumberFormat formato = NumberFormat.getFormat("##.00");
				while (k<maximoItems){
//					com.google.gwt.user.client.Window.alert(" parte "+terminadoParte);
//					while (!terminadoParte){};
//					terminadoParte=false;
					PagosAsociados = new HashSet<DtocomercialTbltipopagoDTO>(0);
					
					numeroFact+=1;
					serialParaOrdenProds=0;
					
					DtoDetalles = new HashSet<DtoComDetalleDTO>(0);
					detallesJson = new LinkedList<JSONObject>();
					
					
					ListGridRecord rec;
					String desAuxiliar = "";
					Subtotal = 0.0;
					descuentoGlobal = 0.0;
					impuestoadicionGlobal = 0.0;
					subtotalIva14 = 0.0;
					subtotalIva0 = 0.0;
					
					costo = 0.0;
					Double costoIndividual = 0.0;
					
//					int iva = 0;
					String[] ivas;
					Double desc = 1.0;
					Double descS = 1.0;
					String descSucesivo = "";
					Double descuentoNeto = 0.0;
					limit=((Factum.banderaNumeroItemsFactura+k)<maximoItems)?Factum.banderaNumeroItemsFactura+k:maximoItems-1;
					for (int j=k;j<limit;j++){
						descS = 1.0;
						rec =new ListGridRecord();
						rec=selectedRecords[j];
						costoIndividual = rec.getAttributeAsDouble("costo")
								* rec.getAttributeAsDouble("cantidad");
						
						Double valTotal=rec.getAttributeAsDouble("precioUnitario")
								* rec.getAttributeAsDouble("cantidad");
								Subtotal += valTotal;
	//				for (ListGridRecord rec : selectedRecords) {
						serialParaOrdenProds++;
						detallesDto = new DtoComDetalleDTO();
						detallesDto.setCantidad(rec.getAttributeAsDouble("cantidad"));

						descSucesivo = rec.getAttributeAsString("descuento");
						// cabmio desc
						if(descSucesivo == "100"){
							descSucesivo = "2";
						}else{
							descSucesivo = rec.getAttributeAsString("descuento");
						}					
						
						String[] descuentos = descSucesivo.split("\\+");
						Double desParcial = 0.0;
						for (int i = 0; i < descuentos.length; i++) {
							desParcial = Double.parseDouble(descuentos[i]);
							desParcial = (100 - desParcial) / 100;
							descS = descS * desParcial;
						}
						descS = 1 - descS;
						descS = CValidarDato.getDecimal(4, descS);
						descS = descS * 100;
						detallesDto.setDepartamento(descS);
	
						
						try {
//							iva = rec.getAttributeAsInt("iva");
							
							String cadIva=rec.getAttribute("iva");
							ivas=cadIva.split(",");
							String impuestos="";
							double impuestoPorc=1.0;
							double impuestoValor=0.0;
							int i1=0;
							
							
							desc = 0.0;
							if (descS > 0)
								desc = descS;
							
							//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
							for (String ivaS:ivas){
								i1=i1+1;
								impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
								impuestoValor=impuestoValor+((valTotal* (1 - desc)+impuestoValor)*(Double.parseDouble(ivaS)/100));
							}
							//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
//							if (iva == Factum.banderaIVA) {
//								subtotalIva14 += (valTotal * (1 - desc));
							if ((impuestoPorc-1) >0) {
								subtotalIva14 += (valTotal * (1 - desc));
								valoresIva=valoresIva+impuestoValor;
								//SC.say("IVA "+iva +"=="+ Factum.banderaIVA);
							} else {
								subtotalIva0 += (valTotal * (1 - desc));
							}
						} catch (Exception iv) {
							subtotalIva14 += 0.0;
							subtotalIva0 += 0.0;
						}
						costo += costoIndividual;
						
						
						int num = 0;
	
						BodegaGRABAR= new BodegaDTO();
						BodegaGRABAR.setIdBodega(rec.getAttributeAsInt("idbodega"));
						BodegaGRABAR.setIdEmpresa(Factum.empresa.getIdEmpresa());						
						BodegaGRABAR.setEstablecimiento(Factum.getEstablecimientoCero());
						detallesDto.setBodega(BodegaGRABAR);// rec.getAttributeAsInt("bodega"));//rec.getAttributeAsInt("bodega"));
						detallesDto.setIdDtoComercialDetalle(null);// Id que sera creado
						// al momento de
						// grabar
	
						// AQUI ESTOY SETEANDO UN ORDENAMIENTO DE CADA PRODUCTO CON UN
						// CONTADOR, PARA GUARDAR POR FACTURA
						detallesDto.setDesProducto(serialParaOrdenProds + ";"
								+ rec.getAttributeAsString("descripcion"));			
//						//com.google.gwt.user.client.Window.alert("Descripcion factura de "+numeroFact+" producto dividido "+serialParaOrdenProds + ";"
//								+ rec.getAttributeAsString("descripcion"));
						ProductoDTO pr = new ProductoDTO();
						pr.setIdEmpresa(Factum.empresa.getIdEmpresa());
						pr.setEstablecimiento(Factum.getEstablecimientoCero());
						pr.setIdProducto(rec.getAttributeAsInt("idProducto"));
//						pr.setStock(rec.getAttributeAsDouble("stock"));
						// 	Cambio para fixear error del stock
						pr.setStock(CValidarDato.getDecimal(2,rec.getAttributeAsDouble("stock")));
						pr.setDescripcion(rec.getAttributeAsString("descripcion"));
						pr.setCodigoBarras(rec.getAttributeAsString("codigoBarras"));
//						detallesDto.setCostoProducto(rec.getAttributeAsDouble("costo"));
						detallesDto.setProducto(pr);
//						detallesDto.setImpuesto(rec.getAttributeAsInt("iva"));
						detallesDto.setPrecioUnitario(rec
								.getAttributeAsDouble("precioUnitario"));
						//SC.say(formatoDecimalN.format(rec.getAttributeAsDouble("precioUnitario")));
	
						detallesDto.setTbldtocomercial(null);
						detallesDto.setTotal(rec.getAttributeAsDouble("valorTotal"));
	
						DtoDetalles.add(detallesDto);
						
						
						
						
						
					}
					
					costo = CValidarDato.getDecimal(2, costo);
					Subtotal = CValidarDato.getDecimal(2, Subtotal);
					// descuentoGlobal = descuentoGlobal/100;
					
					descuentoGlobal = CValidarDato.getDecimal(2, descuentoGlobal);
					impuestoadicionGlobal = CValidarDato.getDecimal(2, impuestoadicionGlobal);
					
					total = (subtotalIva14 +valoresIva) + subtotalIva0;
//					total = (subtotalIva14 * (1+(Factum.banderaIVA/100))) + subtotalIva0;// +
					// Double.parseDouble(txtBase_imponible.getDisplayValue());
					total = CValidarDato.getDecimal(2, total);
					
					List<DtoComDetalleDTO> milista = new ArrayList<DtoComDetalleDTO>();
	
					milista = ordenarProductos(DtoDetalles);
					for (int j = 0; j < milista.size(); j++) {
					//for (ListGridRecord rec : selectedRecords) {// aqui esta ponniendo
																// producto por producto
																// en el detalle
																// Aqui pasamos cada
																// detalle de la factura
																// a una lista de
																// DtoComDetalleDTO
																// para posteriormente
																// agregarla al
																// DtocomercialDTO a
																// grabar
						
						detalleJSON = new JSONObject();
						detalleJSON.put("cantidad",	new JSONString(formato.format(
								milista.get(j).getCantidad()
	//									rec
	//									.getAttributeAsDouble("cantidad")
										)));
						detalleJSON.put("codigoBarras",	new JSONString(
								milista.get(j).getProducto().getCodigoBarras()
	//									rec
	//									.getAttributeAsDouble("codigoBarras")
										));
						
						
						desAuxiliar = milista.get(j).getDesProducto();
						if(desAuxiliar.indexOf("#")!=-1){
							desAuxiliar=desAuxiliar.replace("#", "Num");
						}else if(desAuxiliar.indexOf("*")!=-1){
							desAuxiliar=desAuxiliar.replace("*", "");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "n");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "a");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "e");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "i");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "o");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "u");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "A");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "E");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "I");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "O");
						}else if(desAuxiliar.indexOf("�")!=-1){
							desAuxiliar=desAuxiliar.replace("�", "U");
						}else if(desAuxiliar.indexOf("&")!=-1){
							desAuxiliar=desAuxiliar.replace("&", "y");
						}
	
	
						detalleJSON.put("descripcion", new JSONString(
								desAuxiliar
								
								));
	
						detalleJSON.put("precio", new JSONString(formatoDecimalN.format(
										milista.get(j).getPrecioUnitario()
	//									rec
	//									.getAttributeAsDouble("precioUnitario")
										)));
	
						detalleJSON.put("total", new JSONString(formato.format(
										milista.get(j).getTotal()
	//									rec
	//									.getAttributeAsDouble("valorTotal")
										)));
						
//						detalleJSON.put("iva",new JSONString(formato.format(
//								milista.get(j).getImpuesto()
//	//							rec
//	//									.getAttributeAsInt("iva")
//								)));
						detalleJSON.put("descuento",new JSONString(formato.format(
								milista.get(j).getDepartamento()
	//							rec
	//									.getAttributeAsInt("descuento")
										)));
						
						detallesJson.add(detalleJSON);
	
					}
					
					if (indicadorDetalles.isEmpty()) {
						if (dfExtras.validate()) {

							try {
								if (dateFecha_emision.getSelectorFormat().toString()
										.compareTo("DAY_MONTH_YEAR") == 0) {
									String fecha = dateFecha_emision.getDisplayValue();
									// fecha.replace("00:00"," ");
									String orden[] = fecha.split("/");
									// String orden[] = fechaBase[0].split("-");
									String fechaMostrar = orden[2] + "/" + orden[1] + "/"
											+ orden[0];
									dateFecha_emision.setValue(fechaMostrar);

									// SC.say("si entre al if: "+"la fecha es:"+dateFecha_emision.getDisplayValue());
								}
								
								PagosDTO = new HashSet<TblpagoDTO>(0);
								TblpagoDTO PagoDTO = new TblpagoDTO();
								NumPagos = 1;
								Double totalTr = 0.0;
								Double totalBanco = 0.0;
								Double totalPagos = totalCredito;
								totalCredito = 0.0;
								String castEstado = "";
								char estado = '0';
							

										PagoDTO = new TblpagoDTO();
										PagoDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
										PagoDTO.setEstablecimiento(Factum.getEstablecimientoCero());
										PagoDTO.setTbldtocomercial(null);
										PagoDTO.setFechaVencimiento((Date)dateFecha_emision.getValue());
										PagoDTO.setIdPago(null);
										PagoDTO.setValor(total);

										totalCredito = 	total;							

										PagoDTO.setEstado(estado); 
										PagosDTO.add(PagoDTO);
									
										
										DtocomercialTbltipopagoDTO pagoContado = new DtocomercialTbltipopagoDTO(
												2/* "CREDITO" */, totalCredito,
												0, "", '0');
										PagosAsociados.add(pagoContado);
										
										Set<RetencionDTO> retenciones=new HashSet<RetencionDTO>();
										PersonaDTO facturaPor = new PersonaDTO();
										facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
								DtocomercialDTO dto = new DtocomercialDTO(
										Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento(),Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision(),
										idDto, persona,
										vendedor,
										facturaPor,
										numeroFact,
										NumDto,// Tipo de transaccion
										dateFecha_emision.getDisplayValue(),
										dateFecha_emision.getDisplayValue(),// new Date(),
																			// //fecha
																			// expiracion ????
										auth, NumPagos,
										confirmar,// estado, //aqui cambie
										Subtotal, DtoDetalles,
										retenciones,
										PagosDTO, // Set tblpagos
										null,// Set tblmovimientos
										Anticipo, NotaCredito, Retencion, PagosAsociados,
										costo, "", descuentoNeto, responsable, 0);// Tipos
																								// de
																								// pago
																								// asociados
																								// al
																								// documento
//								String total = txtValor14_iva.getDisplayValue();
//								total = total.replace(",", "");
//								Double TotalIva = Double.parseDouble(total);
//								
//								Double TotalIva = subtotalIva14 * (Factum.banderaIVA/100);
								Double TotalIva=valoresIva;
								TotalIva = CValidarDato.getDecimal(4, TotalIva);
								//SC.say("si entre aqui may" );
								// OBSERVACION
								//dto.setObservacion(usuario.getUserName());
	
								dto.setObservacion(txtObservacion.getDisplayValue());
								documentos.add(new DocumentoSaveDTO(dto, afeccion, TotalIva, anulacion));
								
								
								
								//getService().grabarDocumento(dto, afeccion, TotalIva, anulacion,  usuario.getUserName(),Factum.planCuenta,Factum.banderaConfiguracionTipoFacturacion,Factum.banderaIVA, Factum.banderaNumeroDecimales,factDivCallback);
//								Timer timer = new Timer(){
//									@Override
//									public void run(){
//									
//									}
//								};
//								timer.schedule(10000);	
//								Thread.sleep(4000);
//								grabar(dto, TotalIva, anulacion);// PAsamos el ID de la cuenta
							} catch (Exception e) {
								SC.say("Error en grabar documento "+e.getMessage());
								btnGrabar.setDisabled(false);
							}
						} else {
							SC.say("CORRIJA LOS DATOS E INTENTELO NUEVAMENTE");
						}
					}
					
					
					k+=Factum.banderaNumeroItemsFactura;
				}
				numDocsDiv=documentos.size();
				getService().grabarDocumentosDiv(documentos,usuario.getUserName(),Factum.planCuenta,Factum.banderaConfiguracionTipoFacturacion,Factum.banderaIVA, Factum.banderaNumeroDecimales,factsDivCallback);
				
				limpiarInterfaz();
			} catch (Exception e) {
				indicadorDetalles = e.toString();
				SC.say("Error al intentar extraer los items de la factura en grabar documento: " + e);
				btnGrabar.setDisabled(false);
				// mibod.setValue("error  "+e.getMessage()+" "+e.toString());
			}
		}// fin if error is empty
		else {
			String enc = "Primero debe corregir lo siguiente:";
			enc += "\n";
			enc += error;
			SC.say(enc);
			btnGrabar.setDisabled(false);
		}
		
	}
	
	int numDocsDiv=0;
	List<DtocomercialDTO> documentosDivs=new ArrayList<DtocomercialDTO>();
	final AsyncCallback<String[]> factsDivCallback = new AsyncCallback<String[]>() {

		public void onSuccess(String[] resultados) {
//			com.google.gwt.user.client.Window.alert("Primero"+result);
			documentosDivs=new ArrayList<DtocomercialDTO>();
			for (int i=0;i<resultados.length;i++){
				String result=resultados[i];
				String[] results = result.split("-");
				result=results[0];
				resultado=results[0];
				//SC.say("Separado "+result);
				String idDtocomercial=results[2].trim();
				//SC.say("Iddto "+idDtocomercial);
				if (result != null && results.length>1) {
					
					getService().getDtoID(Integer.valueOf(results[1].trim()), callbackVentas);
	
				} else {
					SC.say("Se ha producido el siguiente error: " + result);
					btnGrabar.setDisabled(false);
					btnImprimir.setDisabled(true);
				}
			
			}
			
			
			
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al almacenar facturas divididas " + caught+" "+caught.getCause()+"; "+caught.getStackTrace());
//			System.out.println("" + caught);
			btnGrabar.setDisabled(false);
			btnImprimir.setDisabled(true);
		}
	};
	
	final AsyncCallback<DtocomercialDTO> callbackVentas= new AsyncCallback<DtocomercialDTO>() {

		public void onSuccess(DtocomercialDTO result) {
			
				documentosDivs.add(result);
				if (documentosDivs.size()==numDocsDiv){
					for (DtocomercialDTO documento:documentosDivs){
						DocBase = documento;
						btnImprimir.setDisabled(false);
				    	btnGrabar.setDisabled(true);
				    	btnEliminar.setDisabled(true);
//				    	com.google.gwt.user.client.Window.alert("impresion division "+Factum.banderaConfiguracionImpresion+" "+resultado
//	//			    			+" parte "+terminadoParte
//				    			);
				    	switch (Factum.banderaConfiguracionImpresion) {
	
						case 1:// IMPRESION DIRECTA
//							com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 1"
//	//					+" parte "+terminadoParte
//									);
							if (resultado.equals("Grabado con exito") ) {
								impresion(documento);
							}else{
								SC.say("Resultado: " + resultado+" error "+impreso);
								btnGrabar.setDisabled(false);
								convertir = 0;
								limpiarInterfaz();
							}
	//						terminadoParte=true;
							break;
						case 3:
//							com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 3"
	//								+" parte "+terminadoParte
//									);
							impreso += "mixta ";
							if (Factum.banderaConfiguracionTipoFacturacion == 0) {
								if (resultado.equals("Grabado con exito") ) {
									if (Factum.banderaConfiguracionImpresionFactura == 0) {
										ListGrid grdProductosImprimir = grdProductos;
										imprimir();
									} else if (Factum.banderaConfiguracionImpresionFactura == 1) {
										impresion(documento);
									}
	
								}
							} 
	//						terminadoParte=true;
							break;
						}
					}
				}
		}

		public void onFailure(Throwable caught) {
			SC.say("No se puede cargar el documento solicitado");
		}
	};
	
	final AsyncCallback<String> factDivCallback = new AsyncCallback<String>() {

		public void onSuccess(String result) {
//			//com.google.gwt.user.client.Window.alert("Primero"+result);
			String[] results = result.split("-");
			result=results[0];
			resultado=results[0];
			//SC.say("Separado "+result);
			String idDtocomercial=results[2].trim();
			//SC.say("Iddto "+idDtocomercial);
			if (result != null && results.length>1) {
				
				getService().getDtoID(Integer.valueOf(results[1].trim()), callbackVenta);

			} else {
				SC.say("Se ha producido el siguiente error: " + result);
				btnGrabar.setDisabled(false);
				btnImprimir.setDisabled(true);
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al obtener el id del cliente" + caught);
			System.out.println("" + caught);
			btnGrabar.setDisabled(false);
			btnImprimir.setDisabled(true);
		}
	};
	final AsyncCallback<DtocomercialDTO> callbackVenta= new AsyncCallback<DtocomercialDTO>() {

		public void onSuccess(DtocomercialDTO result) {

			DocBase = result;
			btnImprimir.setDisabled(false);
	    	btnGrabar.setDisabled(true);
	    	btnEliminar.setDisabled(true);
//	    	//com.google.gwt.user.client.Window.alert("impresion division "+Factum.banderaConfiguracionImpresion+" "+resultado);
	    	switch (Factum.banderaConfiguracionImpresion) {

			case 1:// IMPRESION DIRECTA
//				//com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 1");
				if (resultado.equals("Grabado con exito") ) {
					impresion(result);
				}else{
					SC.say("Resultado: " + resultado+" error "+impreso);
					btnGrabar.setDisabled(false);
					convertir = 0;
					limpiarInterfaz();
				}
				break;
			case 3:
//				//com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 3");
				impreso += "mixta ";
				if (Factum.banderaConfiguracionTipoFacturacion == 0) {
					if (resultado.equals("Grabado con exito") ) {
						if (Factum.banderaConfiguracionImpresionFactura == 0) {
							ListGrid grdProductosImprimir = grdProductos;
							imprimir();
						} else if (Factum.banderaConfiguracionImpresionFactura == 1) {
							impresion(result);
						}

					}
				} 
				break;
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("No se puede cargar el documento solicitado");
		}
	};
	final AsyncCallback<BodegaDTO> retornaBodega = new AsyncCallback<BodegaDTO>() {
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}

		@Override
		public void onSuccess(BodegaDTO result) {
			boegaaux = new BodegaDTO(result.getIdBodega(), result.getBodega(),
					result.getUbicacion(), result.getTelefono(), result.getIdEmpresa(),result.getEstablecimiento());
			// BodegaGRABAR=new BodegaDTO(boegaaux.getIdBodega(),
			// boegaaux.getBodega(),
			// boegaaux.getUbicacion(),boegaaux.getTelefono());
			// BodegaGRABAR=boegaaux;
			// SC.say("MENSAJE "+boegaaux.getIdBodega()+" "+BodegaGRABAR.getBodega());
		}
	};
	public LinkedList<JSONObject> detallesCredito() {
		JSONObject pagoJSON = new JSONObject();
		LinkedList<JSONObject> pagosJSON = new LinkedList<JSONObject>();
		NumberFormat formato = NumberFormat.getFormat("##.00");
		DateTimeFormat formF=DateTimeFormat.getFormat("yyyy-MM-dd");
		////com.google.gwt.user.client.Window.alert("En detalles credito");
		for (ListGridRecord rec :grdPagos.getRecords()){
			////com.google.gwt.user.client.Window.alert("En detalles credito FOR");
			pagoJSON = new JSONObject();
			pagoJSON.put("cantidad", new JSONString(formato.format(rec.getAttributeAsDouble("cantidadPago"))));
			Date fechaV=rec.getAttributeAsDate("vencimiento");
//			DateFormat formF=new SimpleDateFormat("yyyy-MM-dd");
			
			//SimpleDateFormat formatoF=new SimpleDateFormat("yyyy-MM-dd");
			////com.google.gwt.user.client.Window.alert(formatoF.format(fechaV));
//			//com.google.gwt.user.client.Window.alert(formF.format(fechaV)+" ,"+fechaV.toLocaleString());
//			String[] fechaVe= fechaV.toLocaleString().split(" ");
			pagoJSON.put("vencimiento", new JSONString(formF.format(fechaV)));
			////com.google.gwt.user.client.Window.alert("En detalles credito DATO ID");
			//int id=(rec.getAttributeAsInt("id")!= 0 && rec.getAttributeAsInt("id")!=null )?rec.getAttributeAsInt("id"):0;
			int id=(rec.getAttribute("id")!=null )?rec.getAttributeAsInt("id"):0;
			pagoJSON.put("id", new JSONString(String.valueOf(id)));
			////com.google.gwt.user.client.Window.alert("En detalles credito DATO ESTADO");
//			String est=(rec.getAttributeAsString("estado")!= "" && rec.getAttributeAsString("estado")!=null)?rec.getAttributeAsString("estado"):"";
			String est=(rec.getAttribute("estado")!=null)?rec.getAttributeAsString("estado"):"";
			pagoJSON.put("estado", new JSONString(est));
			if (est!="0" && est!="" && est!=null){
				String numegreso=rec.getAttribute("numegreso");
				pagoJSON.put("numegreso", new JSONString(numegreso));
				String fechaR=(rec.getAttribute("fecharealpago")!=null )?formF.format(rec.getAttributeAsDate("fecharealpago")):"0000-00-00";
				pagoJSON.put("fecharealpago", new JSONString(fechaR));
//				if (!PagoDTO.getFechaRealPago().equals(null)) grdPagos.setEditValue(numPagos, "fecharealpago", PagoDTO.getFechaRealPago());
				String concepto=(rec.getAttribute("concepto")!=null)?rec.getAttribute("concepto"):"";
				pagoJSON.put("concepto", new JSONString(concepto));
//				if (!PagoDTO.getConcepto().equals(null)) grdPagos.setEditValue(numPagos, "concepto", PagoDTO.getConcepto());
				String formapago=(rec.getAttribute("formapago")!=null)?rec.getAttribute("formapago"):"";
				pagoJSON.put("formapago", new JSONString(formapago));
//				if (!PagoDTO.getFormaPago().equals(null)) grdPagos.setEditValue(numPagos, "formapago", PagoDTO.getFormaPago());
			}
			pagosJSON.add(pagoJSON);
		}
		////com.google.gwt.user.client.Window.alert("Saliendo detalles credito");
		return pagosJSON;
	}
	public JSONObject detallesPagos() {
		serialParaOrdenProds = 0;
		indicadorDetalles = "";
		grdProductos.saveAllEdits();
		JSONObject detalleJSON = new JSONObject();
		
		if(NumDto == 20){
			SC.say("Proforma Creada");
		}else{
		
		////com.google.gwt.user.client.Window.alert("En detalles pagos");
		try {
			
			String contado="0";String credito=" ";String retencion=" ";String anticipo=" ";
			String notaCred=" ";String tarjeta=" ";String tarjetaInfo=" ";String banco=" ";String bancoInfo=" ";
			////com.google.gwt.user.client.Window.alert("En detalles pagos TXT");
			//if ((Boolean) dynamicForm.getField("chkContado").getValue()) contado="X";
			if ((Boolean) dynamicForm.getField("chkContado").getValue()) contado=dynamicForm.getValueAsString("txtContado");
			////com.google.gwt.user.client.Window.alert("En detalles pagos TXT2 "+contado);
			//if ((Boolean) dynamicForm3.getField("chkTarjetaCr").getValue()) tarjeta="X";
			if ((Boolean) dynamicForm3.getField("chkTarjetaCr").getValue()){
				tarjeta=dynamicForm3.getValueAsString("txtTarjetaCr");
				tarjetaInfo=dynamicForm3.getValueAsString("txtTarjetaCr1");
			};
			//if ((Boolean) dynamicForm3.getField("chkBanco").getValue()) banco="X";
			if ((Boolean) dynamicForm3.getField("chkBanco").getValue()){
				banco=dynamicForm3.getValueAsString("txtBanco");
				bancoInfo=dynamicForm3.getValueAsString("txtBanco1");
			};
			////com.google.gwt.user.client.Window.alert("En detalles pagos TXT2 "+banco+" - "+bancoInfo);
			NumberFormat formato = NumberFormat.getFormat("##0.00");
			//SC.say("Valores: "+String.valueOf(((Boolean) dynamicForm.getField("chkContado").getValue())));
			if (NumDto!=4 && NumDto!=15 && NumDto!=3 && NumDto!=14 && NumDto!=20  ){
				//if ((Boolean) dynamicForm.getField("chkCredito").getValue()) credito="X";
//				if ((Boolean) dynamicForm2.getField("chkRetencion").getValue()) retencion="X";
//				if ((Boolean) dynamicForm2.getField("chkNotaCredito").getValue()) notaCred="X";
//				if ((Boolean) dynamicForm2.getField("chkAnticipo").getValue()) anticipo="X";
				if ((Boolean) dynamicForm.getField("chkCredito").getValue()) credito=dynamicForm.getValueAsString("txtCredito");
				if(NumDto==0 || NumDto==1) if ((Boolean) dynamicForm2.getField("chkRetencion").getValue()) retencion=dynamicForm2.getValueAsString("txtRetencion");
				if(NumDto!=26 && NumDto!=27) if ((Boolean) dynamicForm2.getField("chkNotaCredito").getValue()) notaCred=dynamicForm2.getValueAsString("txtNotaCredito");
				if ((Boolean) dynamicForm2.getField("chkAnticipo").getValue()) anticipo=dynamicForm2.getValueAsString("txtAnticipo");
			} 
			////com.google.gwt.user.client.Window.alert("En detalles pagos JSON");
				detalleJSON = new JSONObject();
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON2");
				detalleJSON.put("contado",	new JSONString(formato.format(
						Double.valueOf(contado))));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON3");
				detalleJSON.put("credito", new JSONString(credito));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON4");
				detalleJSON.put("retencion", new JSONString(retencion));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON5");
				detalleJSON.put("anticipo", new JSONString(anticipo));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON6");
				detalleJSON.put("notaCred", new JSONString(notaCred));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON7");
				detalleJSON.put("tarjeta", new JSONString(tarjeta));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON8");
				detalleJSON.put("tarjetaInfo", new JSONString(tarjetaInfo));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON9");
				detalleJSON.put("banco", new JSONString(banco));
				////com.google.gwt.user.client.Window.alert("En detalles pagos JSON10");
				detalleJSON.put("bancoInfo", new JSONString(bancoInfo));
				
			
		} catch (Exception e) {
			indicadorDetalles = e.toString();
			btnGrabar.setDisabled(false);
			SC.say("Error al intentar extraer los detalles de los pagos: " +e.getMessage());
			////com.google.gwt.user.client.Window.alert("Error al intentar extraer los detalles de los pagos: " +e.getMessage());
			////com.google.gwt.user.client.Window.alert("Error al intentar extraer los detalles de los pagos: " + e.getCause().toString()+" "+e.getMessage());
			
			// mibod.setValue("error  "+e.getMessage()+" "+e.toString());
		}
		}
		////com.google.gwt.user.client.Window.alert("Saliendo detalles pagos");
		return  detalleJSON;
	}

	public void obtenerDetalles() {
		serialParaOrdenProds = 0;
		indicadorDetalles = "";
		grdProductos.saveAllEdits();
		
		try {
			ListGridRecord[] selectedRecords = grdProductos.getRecords();
			DtoDetalles = new HashSet<DtoComDetalleDTO>(0);
			detallesJson = new LinkedList<JSONObject>();
			Map<String, String> impuestosList = new HashMap<String,String>();
			
			DtoComDetalleDTO detallesDto = new DtoComDetalleDTO();
			JSONObject detalleJSON = new JSONObject();
			NumberFormat formato = NumberFormat.getFormat("##.00");

			String desAuxiliar = "";
			String[] ivas;
			for (ListGridRecord rec : selectedRecords) {
				serialParaOrdenProds++;
				detallesDto = new DtoComDetalleDTO();
				detallesDto.setCantidad(rec.getAttributeAsDouble("cantidad"));
				
				Double descS = 1.0;
				String descSucesivo = rec.getAttributeAsString("descuento");
			
				String[] descuentos = descSucesivo.split("\\+");
				Double desParcial = 0.0;
				for (int i = 0; i < descuentos.length; i++) {
					desParcial = Double.parseDouble(descuentos[i]);
					desParcial = (100 - desParcial) / 100;
					descS = descS * desParcial;
				}
				descS = 1 - descS;
				descS = CValidarDato.getDecimal(4, descS);
				descS = descS * 100;
				detallesDto.setDepartamento(descS);
				
				int num = 0;

				BodegaGRABAR= new BodegaDTO();
				BodegaGRABAR.setIdEmpresa(Factum.empresa.getIdEmpresa());
				BodegaGRABAR.setEstablecimiento(Factum.getEstablecimientoCero());
				BodegaGRABAR.setIdBodega(rec.getAttributeAsInt("idbodega"));
				

				detallesDto.setBodega(BodegaGRABAR);// rec.getAttributeAsInt("bodega"));//rec.getAttributeAsInt("bodega"));
				detallesDto.setIdDtoComercialDetalle(null);// Id que sera creado
				// al momento de
				// grabar

				// AQUI ESTOY SETEANDO UN ORDENAMIENTO DE CADA PRODUCTO CON UN
				// CONTADOR, PARA GUARDAR POR FACTURA
				detallesDto.setDesProducto(serialParaOrdenProds + ";"
						+ rec.getAttributeAsString("descripcion"));			

				ProductoDTO pr = new ProductoDTO();
				pr.setIdEmpresa(Factum.empresa.getIdEmpresa());
				pr.setEstablecimiento(Factum.getEstablecimientoCero());
				pr.setIdProducto(rec.getAttributeAsInt("idProducto"));
//				pr.setStock(rec.getAttributeAsDouble("stock"));
				// Cambio para fixear error
				pr.setStock(CValidarDato.getDecimal(2,rec.getAttributeAsDouble("stock")));
				pr.setDescripcion(rec.getAttributeAsString("descripcion"));
				pr.setCodigoBarras(rec.getAttributeAsString("codigoBarras"));
				detallesDto.setProducto(pr);
				
				//com.google.gwt.user.client.Window.alert("En obtener detalles producto");
				
				String cadIva=rec.getAttributeAsString("iva");
				if (!cadIva.isEmpty() && !cadIva.equals(null) && !cadIva.equals("")){
					//com.google.gwt.user.client.Window.alert("En obtener detalles iva "+cadIva);
					ivas=cadIva.split(",");
					//com.google.gwt.user.client.Window.alert("En obtener detalles iva "+ivas);
					int i1=0;
					Set<DtoComDetalleMultiImpuestosDTO>  multImps=new HashSet<DtoComDetalleMultiImpuestosDTO>(0);
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					DtoComDetalleMultiImpuestosDTO mult;
					String impsStr="";
					//com.google.gwt.user.client.Window.alert("En obtener detalles antes for ivas iva "+ivas.length);
					for (String ivaS:ivas){
						//com.google.gwt.user.client.Window.alert("En obtener detalles antes for ivas "+ivaS);
						i1=i1+1;
						mult=new DtoComDetalleMultiImpuestosDTO();
						mult.setPorcentaje(Double.parseDouble(ivaS));
						impsStr=(i1<ivas.length)?impsStr+ivaS+".":impsStr+ivaS;
						//com.google.gwt.user.client.Window.alert("En obtener detalles antes for impsStr "+impsStr);
						mult.setTipo('1');
						multImps.add(mult);
					}
					//com.google.gwt.user.client.Window.alert("En obtener detalles despues for ivas iva ");
					impuestosList.put(rec.getAttributeAsString("codigoBarras"), impsStr);
					
					//com.google.gwt.user.client.Window.alert("En obtener detalles impuestos");
					
					detallesDto.setTblimpuestos(multImps);
				}
//				detallesDto.setImpuesto(rec.getAttributeAsInt("iva"));
				detallesDto.setPrecioUnitario(rec
						.getAttributeAsDouble("precioUnitario"));
				//SC.say(formatoDecimalN.format(rec.getAttributeAsDouble("precioUnitario")));

				detallesDto.setTbldtocomercial(null);
				detallesDto.setTotal(rec.getAttributeAsDouble("valorTotal"));

				DtoDetalles.add(detallesDto);
				//com.google.gwt.user.client.Window.alert("En obtener detalles fin");
			}
			//com.google.gwt.user.client.Window.alert("En obtener detalles despues for");
			List<DtoComDetalleDTO> milista = new ArrayList<DtoComDetalleDTO>();

			milista = ordenarProductos(DtoDetalles);
			//com.google.gwt.user.client.Window.alert("En obtener detalles despues obtener productos");
			for (int j = 0; j < milista.size(); j++) {
			//for (ListGridRecord rec : selectedRecords) {// aqui esta ponniendo
														// producto por producto
														// en el detalle
														// Aqui pasamos cada
														// detalle de la factura
														// a una lista de
														// DtoComDetalleDTO
														// para posteriormente
														// agregarla al
														// DtocomercialDTO a
														// grabar
				
				detalleJSON = new JSONObject();
				detalleJSON.put("cantidad",	new JSONString(formato.format(
						milista.get(j).getCantidad()
//								rec
//								.getAttributeAsDouble("cantidad")
								)));
				detalleJSON.put("codigoBarras",	new JSONString(
						milista.get(j).getProducto().getCodigoBarras()
//								rec
//								.getAttributeAsDouble("codigoBarras")
								));
				
				
				desAuxiliar = milista.get(j).getDesProducto();
				if(desAuxiliar.indexOf("#")!=-1){
					desAuxiliar=desAuxiliar.replace("#", "Num");
				}else if(desAuxiliar.indexOf("*")!=-1){
					desAuxiliar=desAuxiliar.replace("*", "");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "n");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "a");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "e");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "i");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "o");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "u");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "A");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "E");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "I");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "O");
				}else if(desAuxiliar.indexOf("�")!=-1){
					desAuxiliar=desAuxiliar.replace("�", "U");
				}else if(desAuxiliar.indexOf("&")!=-1){
					desAuxiliar=desAuxiliar.replace("&", "y");
				}


				detalleJSON.put("descripcion", new JSONString(
						desAuxiliar
						
						));

				detalleJSON.put("precio", new JSONString(formatoDecimalN.format(
								milista.get(j).getPrecioUnitario()
//								rec
//								.getAttributeAsDouble("precioUnitario")
								)));

				detalleJSON.put("total", new JSONString(formato.format(
								milista.get(j).getTotal()
//								rec
//								.getAttributeAsDouble("valorTotal")
								)));
				if (!impuestosList.isEmpty() && impuestosList!=null && impuestosList.size()!=0){
					detalleJSON.put("iva",new JSONString(
							impuestosList.get(milista.get(j).getProducto().getCodigoBarras())));
				}
//				detalleJSON.put("iva",new JSONString(formato.format(
//						milista.get(j).getImpuesto()
////						rec
////								.getAttributeAsInt("iva")
//						)));
				detalleJSON.put("descuento",new JSONString(formato.format(
						milista.get(j).getDepartamento()
//						rec
//								.getAttributeAsInt("descuento")
								)));
				
				detallesJson.add(detalleJSON);

			}
			//com.google.gwt.user.client.Window.alert("En obtener detalles despues for json");
		} catch (Exception e) {
			indicadorDetalles = e.toString() ;
			if (e.getCause()!=null ) indicadorDetalles=indicadorDetalles+e.getCause();
			
			SC.say("Error al intentar extraer los items de la factura en obtener detalles: " + e);
			btnGrabar.setDisabled(false);
			// mibod.setValue("error  "+e.getMessage()+" "+e.toString());
		}
	}

	public void obtenerPagos() {
		// Recorrer la grilla de pagos y fechas
		grdPagos.saveAllEdits();
		try {
			ListGridRecord[] selectedRecords = grdPagos.getRecords();
			PagosDTO = new HashSet<TblpagoDTO>(0);
			TblpagoDTO PagoDTO = new TblpagoDTO();
			NumPagos = Integer.parseInt(dynamicForm.getItem("txtCredito")
					.getDisplayValue());
			totalCredito = 0.0;
			String castEstado = "";
			char estado = '0';
			try {
				for (ListGridRecord rec : selectedRecords) {
					// Aqui pasamos cada detalle de la factura a una lista de
					// DtoComDetalleDTO
					// para posteriormente agregarla al DtocomercialDTO a grabar
					PagoDTO = new TblpagoDTO();
					PagoDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
					PagoDTO.setEstablecimiento(Factum.getEstablecimientoCero());
					PagoDTO.setTbldtocomercial(null);
					PagoDTO.setFechaVencimiento(rec
							.getAttributeAsDate("vencimiento"));
					// SC.say(dateFecha_emision.getDisplayValue()+" -- "+rec.getAttributeAsString("vencimiento")+
					// dt.toString()+
					// "---   "+rec.getAttributeAsDate("vencimiento"));
					// PagoDTO.setFechaVencimiento(dt.toString());
					// aidPago = rec.getAttributeAsInt("id");
					if (!btnGrabar.getTitle().equals("CONFIRMAR"))// y no se va
																	// a
																	// confirmar)
					{
						try {
							if (rec.getAttributeAsInt("id") != 0)
								PagoDTO.setIdPago(rec.getAttributeAsInt("id"));
							// SC.say("ID: "+rec.getAttributeAsInt("id"));
						} catch (Exception e) {
							PagoDTO.setIdPago(null);
							btnGrabar.setDisabled(false);
							// SC.say("no se pudo asignar id");
						}
					} else {
						PagoDTO.setIdPago(null);
					}
					PagoDTO.setValor(rec.getAttributeAsDouble("cantidadPago"));
//					//com.google.gwt.user.client.Window.alert("En obtener pagos pago: "+rec.getAttributeAsDouble("cantidadPago")+
//							"-"+(Float.parseFloat(rec.getAttribute("cantidadPago").toString())+1.0));
//					//com.google.gwt.user.client.Window.alert("Global total credito: "+totalCredito+" - "+(totalCredito+1.0));
					totalCredito = totalCredito+ (Float.parseFloat(rec.getAttribute("cantidadPago").toString()));
//					//com.google.gwt.user.client.Window.alert("Global total credito: "+totalCredito);
					try {
						castEstado = rec.getAttributeAsString("estado");
						estado = castEstado.charAt(0);
					} catch (Exception e) {
						btnGrabar.setDisabled(false);
					}
					if (estado!='0'){
						
						PagoDTO.setNumEgreso(rec.getAttributeAsInt("numegreso"));
						if (rec.getAttribute("fecharealpago")!=null ) PagoDTO.setFechaRealPago(rec.getAttributeAsDate("fecharealpago"));
//						//com.google.gwt.user.client.Window.alert(String.valueOf(rec.getAttribute("concepto")!=null));
						if (rec.getAttribute("concepto")!=null) {
//							//com.google.gwt.user.client.Window.alert(rec.getAttribute("concepto"));
							PagoDTO.setConcepto(rec.getAttribute("concepto"));
//							//com.google.gwt.user.client.Window.alert(PagoDTO.getConcepto());
							
						}
//						//com.google.gwt.user.client.Window.alert(String.valueOf(rec.getAttribute("formapago")!=null));
						if (rec.getAttribute("formapago")!=null) PagoDTO.setFormaPago(rec.getAttribute("formapago"));
						
					}

					PagoDTO.setEstado(estado); // Estado 0 = Pendiente
					// PagoDTO.setFechaRealPago(null);
					PagosDTO.add(PagoDTO);
				}
			} catch (Exception e) {
				SC.say("ERROR GENERAL: " + e);
				btnGrabar.setDisabled(false);
			}
		} catch (Exception h) {
			SC.say("Posible error al extraer pagos: " + h);
			btnGrabar.setDisabled(false);
		}

	}
	
	public void CargarProductoOrdenes(ListGridRecord registro) {
		//SC.say("1");
		cargaProducto = new ListGridRecord();
		cargaProducto = registro;
		// Analizamos si el producto ya existe en la grilla, en caso de existir
		// solo modificamos la cantidad
		// grdProductos.saveAllEdits();
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
		//SC.say("2");
		Boolean agregar = true;
		Integer numfila = 0;
		String[] ivas;
//		Integer iva = 0;
		Double cantidadNueva = 0.0;
		Double totalAux = 0.0;
		String observacionAdjunta = "";
		for (ListGridRecord rec : selectedRecords) {
			if (rec.getAttributeAsString("codigoBarras").equals(
					cargaProducto.getAttributeAsString("codigoBarras"))) {// Si
																			// el
																			// codigo
																			// a
																			// ingresar,
																			// ya
																			// esta
																			// en
																			// la
																			// grilla,
																			// solo
																			// modificamos
																			// la
																			// cantidad
				//cantidadNueva = cargaProducto.getAttributeAsDouble("cantidad");
				cantidadNueva = 1D;
				grdProductos.setEditValue(numfila, "cantidad", cantidadNueva);
				try {
					if (!cargaProducto.getAttributeAsString("observacion")
							.isEmpty()) {
						observacionAdjunta = "-"
								+ cargaProducto
										.getAttributeAsString("observacion");
					}
				} catch (Exception e) {

				}
				Double precio = Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio"));
				//SC.say("Precio: "+precio);
				Double precioU = 0.0;
				
				precioU = cargaProducto.getAttributeAsDouble("Precio");
				// precioU=result;
				// porcentaje = CValidarDato.getDecimal(4,porcentaje);
				if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27)
					precioU = precio;
				/*
				 * porcentaje = 1.0; precio = (Double) (precio * porcentaje);
				 * Integer valor = (precio.intValue());
				 * 
				 * precio = precio - valor; precio = precio * 100;
				 * if((precio.intValue())>50) { valor+=1; precioU =
				 * Double.parseDouble(valor.toString()); }else{ precioU = valor
				 * + 0.50; }
				 */
//				iva = Integer.parseInt(cargaProducto
//						.getAttributeAsString("impuesto"));
				 
				 String cadIva=cargaProducto.getAttributeAsString("impuesto");
					ivas=cadIva.split(",");
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((cantidadNueva*cargaProducto.getAttributeAsDouble("Precio")
								+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				 
				if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20) {
					if ((impuestoPorc-1)>0.0)
						precioU = precioU / impuestoPorc;
//					if (iva == Factum.banderaIVA)
//						precioU = precioU / (1+(Factum.banderaIVA/100));
				}
				//SC.say("PRECIO UNITARIO: "+precioU);
				precioU = CValidarDato.getDecimal(4, precioU);
				
				totalAux = precioU * cantidadNueva;
				totalAux = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,totalAux );
//				totalAux = Math.rint(totalAux * 100) / 100;
				totalAux = CValidarDato.getDecimal(2, totalAux);
				grdProductos.setEditValue(numfila, "precioUnitario", precioU);
				grdProductos.setEditValue(numfila, "descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);
				// SC.say("verso "+cargaProducto.getAttributeAsDouble("stockPadre"));
//				grdProductos.setEditValue(numfila, "stock",
//						cargaProducto.getAttributeAsDouble("stock"));
				
//				Cambios para controlar el numero de decimales que muestra el stock
				grdProductos.setEditValue(numfila, "stock",
						CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
								.getAttributeAsString("stock"))) );
				
				// grdProductos.setEditValue(numfila, "stockPadre",
				// cargaProducto.getAttributeAsDouble("stockPadre"));
				
				grdProductos.setEditValue(numfila, "descuento",
						cargaProducto.getAttributeAsDouble("DTO"));
				grdProductos.setEditValue(numfila, "precioConIva", totalAux);
				grdProductos.refreshRow(numfila);
				// calculo_Subtotal_Individual(cargaProducto.getAttributeAsString("cantidad"),
				// "cantidad");
				// calculo_Subtotal_Individual(precioU.toString(), "pvp");
				// calculo_Subtotal();
				// grdProductos.saveAllEdits();
				agregar = false;
				// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				break;
			}
			numfila++;
		}
		//SC.say("3");
		if (agregar) {
			String cantidad = "1";
			// String observacionAdjunta = "";
			try {
				/*productoActual=new ProductoDTO();
				productoActual=linkedhashmapproductos.get(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto")));*/
				if (!linkedhashmapproductosAct.containsKey(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto")))){
					linkedhashmapproductosAct.put(
							Integer.valueOf(cargaProducto.getAttributeAsString("idProducto")),
							linkedhashmapproductos.get(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto"))));
				}
						
//				if (!cargaProducto.getAttributeAsString("impuesto").isEmpty())
//					iva = Integer.parseInt(cargaProducto
//							.getAttributeAsString("impuesto"));
				
				String cadIva="";
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				if (!cargaProducto.getAttributeAsString("impuesto").isEmpty()){
					cadIva=cargaProducto.getAttributeAsString("impuesto");
					ivas=cadIva.split(",");
					
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((Double.parseDouble(cargaProducto.getAttributeAsString("cantidad"))*cargaProducto.getAttributeAsDouble("Precio")
								+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				}
				
				
				NumberFormat formato = NumberFormat.getFormat("###0.000");
				Map<String, Object> resultMap = new HashMap<String, Object>();// Utils.getMapFromRow(dsFields,
				
				// getResultRow())
				resultMap.put("idProducto",
						cargaProducto.getAttributeAsString("idProducto"));
				resultMap.put("codigoBarras",
						cargaProducto.getAttributeAsString("codigoBarras"));
				//cantidad = cargaProducto.getAttributeAsString("cantidad");
				resultMap.put("cantidad", cantidad);
				
				try {
					if (!cargaProducto.getAttributeAsString("observacion")
							.isEmpty()) {
						observacionAdjunta = "-"
								+ cargaProducto
										.getAttributeAsString("observacion");
					}
				} catch (Exception e) {

				}
				
				resultMap.put("descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);
				// COSTO
				
				Double precio = Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio"));
				
				Double precioU = 0.0;
				//tipoP = "2";
				/*
				switch (Integer.parseInt(tipoP)) {
				case 2: {
					// publico
					precioU = cargaProducto.getAttributeAsDouble("PVP");
					;
					break;
				}
				case 1: {
					// mayorista
					precioU = cargaProducto.getAttributeAsDouble("Mayorista");
					;
					break;
				}
				case 3: {
					// afiliado
					precioU = cargaProducto.getAttributeAsDouble("Afiliado");
					;
					break;
				}
				}
				*/
				precioU = cargaProducto.getAttributeAsDouble("Precio");
				if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27) {
					precioU = precio;
				}
				if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20 ) {
					if ((impuestoPorc-1)>0.0)
						precioU = precioU / impuestoPorc;
//					if (iva == Factum.banderaIVA)
//						precioU = precioU / (1+(Factum.banderaIVA/100));
				}
	
				precioU = CValidarDato.getDecimal(4, precioU);
				double precioConIva = 0.0;
//				precioConIva = precioU + (precioU * iva / 100);
				precioConIva = precioU *impuestoPorc;
				precioConIva = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,precioConIva );
//				precioConIva = Math.rint(precioConIva * 100) / 100;
				precioConIva = precioConIva * Double.parseDouble(cantidad);
				precioConIva = CValidarDato.getDecimal(2, precioConIva);
				NumberFormat formato2 = NumberFormat.getFormat("#.00");
				
				resultMap.put("precioUnitario", formato.format(precioU));
				resultMap.put("valorTotal", 0.0);
//				resultMap.put("iva", iva);
				resultMap.put("iva", cadIva);
//				resultMap.put("stock", Double.parseDouble(cargaProducto
//						.getAttributeAsString("stock")));
				
//				Cambios para controlar el numero de decimales que muestra el stock
				resultMap.put("stock", CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
						.getAttributeAsString("stock"))));
				
				// resultMap.put("stockPadre",
				// Double.parseDouble(cargaProducto.getAttributeAsString("stockPadre")));
				// resultMap.put("bodega",
				// Integer.parseInt(registro[0].getAttributeAsString("bodega")));
				resultMap.put("descuento", 0);
				resultMap.put("costo", Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio")));
				resultMap.put("precioConIva", formato2.format(precioConIva));
				resultMap.put("numero", rowNumGlobal + 1);
				
				Integer totalFilas = grdProductos.getRecords().length;
				grdProductos.setEditValues(rowNumGlobal, resultMap);
				//grdProductos.refreshRow(rowNumGlobal);
				grdProductos.refreshCell(rowNumGlobal, 0);
				
				
				//grdProductos.startEditing(rowNumGlobal, 2, false);
				grdProductos.saveAllEdits();
				
				grdProductos.refreshRow(0);
				
				grdProductos.startEditingNew();
				grdProductos.refreshFields();
				
				// calculo_Subtotal();
				mostrarBodegas(cargaProducto.getAttributeAsString("idProducto"));
				// listadoProd.destroy();
				
				
				calculo_Subtotal_Individual(cantidad, "cantidad");
				calculo_Subtotal_Individual(precioU.toString(), "pvp");
				this.calculo_Subtotal();
				calculo_Individual_Total();
				//calculo_Subtotal_Individual(formato2.format(precioConIva),"pvp");
				
			} 
			
			catch (Exception e) {
				//SC.say("Error : Ingrese nuevamente la cantidad ");
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");
			}

		}
		// getService().obtenerPrecio(registro.getAttributeAsInt("idProducto"),
		// Integer.parseInt(tipoP), callbackPrecio);
	}
	
	public void CargarProducto(ListGridRecord registro) {
		
		cargaProducto = new ListGridRecord();
		cargaProducto = registro;
		// Analizamos si el producto ya existe en la grilla, en caso de existir
		// solo modificamos la cantidad
		// grdProductos.saveAllEdits();
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
		Boolean agregar = true;
		Integer numfila = 0;
//		Integer iva = 0;
		String[] ivas;
		String cantidadNueva = "";
		Double totalAux = 0.0;
		String observacionAdjunta = "";
		for (ListGridRecord rec : selectedRecords) {
//			//com.google.gwt.user.client.Window.alert("codigobarras: "+cargaProducto.getAttributeAsString("codigoBarras")+" codigobarrec: "+rec.getAttributeAsString("codigoBarras"));
			if (rec.getAttributeAsString("codigoBarras").equals(
					cargaProducto.getAttributeAsString("codigoBarras"))) {// Si
																			// el
																			// codigo
																			// a
																			// ingresar,
																			// ya
																			// esta
																			// en
																			// la
																			// grilla,
																			// solo
																			// modificamos
																			// la
																			// cantidad
//				//com.google.gwt.user.client.Window.alert("Es igual "+rec.getAttributeAsString("codigoBarras").equals(
//						cargaProducto.getAttributeAsString("codigoBarras")));
				cantidadNueva = cargaProducto.getAttributeAsString("cantidad");
//				//com.google.gwt.user.client.Window.alert("1 cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//						+" -"+cantidadNueva);
				if (cantidadNueva!=null){
					grdProductos.getRecord(numfila).setAttribute("cantidad", cantidadNueva);
				}else{
					cantidadNueva=grdProductos.getRecord(numfila).getAttributeAsString("cantidad");
					cargaProducto.setAttribute("DTO", grdProductos.getRecord(numfila).getAttributeAsDouble("descuento"));
				}
//				//com.google.gwt.user.client.Window.alert("2 cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//						+" -"+cantidadNueva);
				//grdProductos.setEditValue(numfila, "cantidad", cantidadNueva);
				
				//SC.say(String.valueOf(cantidadNueva)+",cant "+grdProductos.getRecord(numfila).getAttribute("cantidad"));
				
				// grdProductos.setEditValue(numfila, "cantidad",
				// cargaProducto.getAttributeAsDouble("cantidad"));
				// grdProductos.setEditValue(numfila, "codigoBarras",
				// cargaProducto.getAttributeAsString("codigoBarras"));
				try {
					if (!cargaProducto.getAttributeAsString("observacion")
							.isEmpty()) {
						observacionAdjunta = "-"
								+ cargaProducto
										.getAttributeAsString("observacion");
					}
				} catch (Exception e) {

				}
				/*SC.say(String.valueOf(cargaProducto.getAttributeAsDouble("Precio"))+"., "+cargaProducto
						.getAttributeAsString("promedio"));*/
				Double precio = Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio"));

				Double precioU = 0.0;
				
				precioU = cargaProducto.getAttributeAsDouble("Precio");
				// precioU=result;
				// porcentaje = CValidarDato.getDecimal(4,porcentaje);
				if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27)
					precioU = precio;
				
				String cadIva=cargaProducto.getAttributeAsString("impuesto");
				ivas=cadIva.split(",");
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+((Double.parseDouble(cantidadNueva)*cargaProducto.getAttributeAsDouble("Precio")
							+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				
//				iva = Integer.parseInt(cargaProducto
//						.getAttributeAsString("impuesto"));
				if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20) {
//					if (iva == Factum.banderaIVA)
					if ((impuestoPorc-1)>0.0)
						precioU = precioU / impuestoPorc;
//						precioU = precioU / (1+(Factum.banderaIVA/100));
						//SC.say("IVA producto "+iva+" IVA "+Factum.banderaIVA +" IVA % "+ (1+(Factum.banderaIVA/100)));
				}
				
				precioU = CValidarDato.getDecimal(Factum.banderaNumeroDecimales, precioU);
				totalAux = precioU * Float.parseFloat(cantidadNueva);
				totalAux = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,totalAux );
//				totalAux = Math.rint(totalAux * 100) / 100;
				totalAux = CValidarDato.getDecimal(2, totalAux);
				//SC.say(String.valueOf(cargaProducto.getAttributeAsDouble("Precio"))+",. "+String.valueOf(precioU));
				
				grdProductos.setEditValue(numfila, "precioUnitario", precioU);
				//grdProductos.getRecord(numfila).setAttribute("precioUnitario", precioU);
				//SC.say(String.valueOf(cargaProducto.getAttributeAsDouble("Precio"))+",... "+grdProductos.getRecord(numfila).getAttribute("precioUnitario"));
				/*grdProductos.getRecord(numfila).setAttribute("descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);*/
				grdProductos.setEditValue(numfila, "descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);
//				grdProductos.setEditValues(rowNumGlobal, resultMap);
				grdProductos.refreshRow(numfila);
				grdProductos.refreshCell(numfila, 3);

//				//com.google.gwt.user.client.Window.alert("3 cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//						+" -"+cantidadNueva);
				grdProductos.setEditValue(numfila,3, Float.parseFloat(cantidadNueva));
//				//com.google.gwt.user.client.Window.alert("4 cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//						+" -"+cantidadNueva);
				
//				grdProductos.startEditing(numfila, 3, false);
				
				 //SC.say("verso "+cargaProducto.getAttributeAsDouble("stockPadre"));
//				grdProductos.setEditValue(numfila, "stock",
//						cargaProducto.getAttributeAsDouble("stock"));
				
				grdProductos.setEditValue(numfila, "stock",
						CValidarDato.getDecimal(3, cargaProducto.getAttributeAsDouble("stock")));
				
//				//com.google.gwt.user.client.Window.alert("despues de stock");
				// grdProductos.setEditValue(numfila, "stockPadre",
				// cargaProducto.getAttributeAsDouble("stockPadre"));
				//SC.say(String.valueOf(cargaProducto.getAttributeAsDouble("stock"))+",..O "+grdProductos.getRecord(numfila).getAttribute("stock"));
				grdProductos.setEditValue(numfila, "descuento",
						cargaProducto.getAttributeAsDouble("DTO"));
//				//com.google.gwt.user.client.Window.alert("despues de stock");
				//SC.say(String.valueOf(String.valueOf(cargaProducto.getAttributeAsDouble("DTO")))+",..D "+grdProductos.getRecord(numfila).getAttribute("descuento"));
				grdProductos.setEditValue(numfila, "valorTotal", totalAux);
//				//com.google.gwt.user.client.Window.alert("despues de valortotal");
				grdProductos.refreshRow(numfila);
//				//com.google.gwt.user.client.Window.alert("5 cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//						+" -"+cantidadNueva);
				grdProductos.saveAllEdits();
				// calculo_Subtotal_Individual(cargaProducto.getAttributeAsString("cantidad"),
				// "cantidad");
				// calculo_Subtotal_Individual(precioU.toString(), "pvp");
				// calculo_Subtotal();
				// grdProductos.saveAllEdits();
				agregar = false;
				// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				break;
			}
			numfila++;
		}
//		numfila--;
//		//com.google.gwt.user.client.Window.alert("cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//				+" -"+cantidadNueva);
		if (agregar) {
//			//com.google.gwt.user.client.Window.alert("No es igual "+agregar);
			//SC.say(String.valueOf(linkedhashmapproductos.get(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto"))).getTblbodega().size()));
			String cantidad = "";
			// String observacionAdjunta = "";
			try {
				//SC.say(String.valueOf(linkedhashmapproductos.containsKey(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto")))));
				//SC.say(String.valueOf(linkedhashmapproductos.size()));
				/*ProductoDTO productoActual= new ProductoDTO();
				productoActual=linkedhashmapproductos.get(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto")));
*/				
				linkedhashmapproductosAct.put(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto"))
						, linkedhashmapproductos.get(Integer.valueOf(cargaProducto.getAttributeAsString("idProducto"))));
				//SC.say(productoActual.getIdProducto().toString());
				String cadIva="";
				String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				if (!cargaProducto.getAttributeAsString("impuesto").isEmpty()){
					cadIva=cargaProducto.getAttributeAsString("impuesto");
					ivas=cadIva.split(",");
					
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
					for (String ivaS:ivas){
						i1=i1+1;
						impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
						impuestoValor=impuestoValor+((Double.parseDouble(cargaProducto.getAttributeAsString("cantidad"))*cargaProducto.getAttributeAsDouble("Precio")
								+impuestoValor)*(Double.parseDouble(ivaS)/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
				}
				
//					iva = Integer.parseInt(cargaProducto
//							.getAttributeAsString("impuesto"));
				NumberFormat formato = NumberFormat.getFormat("###0.000");
				Map<String, Object> resultMap = new HashMap<String, Object>();// Utils.getMapFromRow(dsFields,
																				// getResultRow())
				resultMap.put("idProducto",
						cargaProducto.getAttributeAsString("idProducto"));
				resultMap.put("codigoBarras",
						cargaProducto.getAttributeAsString("codigoBarras"));
				cantidad = cargaProducto.getAttributeAsString("cantidad");
				resultMap.put("cantidad", cantidad);
				try {
					if (!cargaProducto.getAttributeAsString("observacion")
							.isEmpty()) {
						observacionAdjunta = "-"
								+ cargaProducto
										.getAttributeAsString("observacion");
					}
				} catch (Exception e) {

				}
				resultMap.put("descripcion",
						cargaProducto.getAttributeAsString("descripcion")
								+ observacionAdjunta);
				// COSTO

				Double precio = Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio"));

				Double precioU = 0.0;
				
				precioU = cargaProducto.getAttributeAsDouble("Precio");
				if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 27) {
					precioU = precio;
				}
				if (NumDto == 0 || NumDto == 3 || NumDto == 13 || NumDto == 20) {
//					if (iva == Factum.banderaIVA)
//						precioU = precioU / (1+(Factum.banderaIVA/100));
					if ((impuestoPorc-1)>0.0)
						precioU = precioU / impuestoPorc;
					//SC.say("IVA producto "+iva+" IVA "+Factum.banderaIVA +" IVA % "+ (1+(Factum.banderaIVA/100)));
					
				}
				precioU = CValidarDato.getDecimal(Factum.banderaNumeroDecimales, precioU);
				//precioU = formatoDecimalN.format(precioU);
				double precioConIva = 0.0;
//				precioConIva = precioU + (precioU * iva / 100);
				precioConIva = precioU *impuestoPorc;
//				precioConIva = Math.rint(precioConIva * 100) / 100;
				precioConIva = precioConIva * Double.parseDouble(cantidad);
				precioConIva = CValidarDato.getDecimal(Factum.banderaNumeroDecimales, precioConIva);
				NumberFormat formato2 = NumberFormat.getFormat("#.00");
				resultMap.put("precioUnitario", formatoDecimalN.format(precioU));
				resultMap.put("valorTotal", 0.0);
//				resultMap.put("iva", iva);
				resultMap.put("iva", cadIva);
//				Cambios para controlar el numero de decimales que muestra el stock
//				resultMap.put("stock", CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
//						.getAttributeAsString("stock"))) );
				resultMap.put("stock", Double.parseDouble(cargaProducto
						.getAttributeAsString("stock")));
				// resultMap.put("stockPadre",
				// Double.parseDouble(cargaProducto.getAttributeAsString("stockPadre")));
				// resultMap.put("bodega",
				// Integer.parseInt(registro[0].getAttributeAsString("bodega")));
				resultMap.put("descuento", 0);
				resultMap.put("costo", Double.parseDouble(cargaProducto
						.getAttributeAsString("promedio")));
				resultMap.put("precioConIva", formato2.format(precioConIva));
				resultMap.put("numero", rowNumGlobal + 1);
				
				grdProductos.setEditValues(rowNumGlobal, resultMap);
				grdProductos.refreshRow(rowNumGlobal);
				grdProductos.refreshCell(rowNumGlobal, 0);

				grdProductos.startEditingNew();
				grdProductos.startEditing(rowNumGlobal, 3, false);
				grdProductos.saveAllEdits();
				// calculo_Subtotal_Individual(cantidad, "cantidad");
				// calculo_Subtotal_Individual(precioU.toString(), "pvp");
				// calculo_Subtotal();
				mostrarBodegas(cargaProducto.getAttributeAsString("idProducto"));
				// listadoProd.destroy();
			} 
			
			catch (Exception e) {
				SC.say("Error : Ingrese nuevamente la cantidad "+e.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");
			}

		}
//		//com.google.gwt.user.client.Window.alert("cargaproducto cantidad "+grdProductos.getRecord(numfila).getAttributeAsString("cantidad")
//				+" -"+cantidadNueva);
		// getService().obtenerPrecio(registro.getAttributeAsInt("idProducto"),
		// Integer.parseInt(tipoP), callbackPrecio);
	}

	

	private class FuncionSI implements FormItemIfFunction {

		public boolean execute(FormItem item, Object value, DynamicForm datosCl) {
//			//com.google.gwt.user.client.Window.alert("en funcionSI");
			if (datosCl.getTitle().equalsIgnoreCase("fp")) {
//				//com.google.gwt.user.client.Window.alert("en elemento cambio "+item.getName());
				String elemento = item.getName().substring(3);
				if (Character.isDigit(elemento.charAt(elemento.length() - 1)))
					elemento = elemento.substring(0, elemento.length() - 1);
				elemento = "chk" + elemento;
				// item.clearValue();
				if (elemento.equals("chkCredito"))
					if (!(Boolean) datosCl.getValue(elemento))
						grdPagos.hide();
//				//com.google.gwt.user.client.Window.alert("elemento cambio "+elemento);
				return (Boolean) datosCl.getValue(elemento);
				// AQUI SE DEBE LLAMAR A LA FUNCION DE RECALCULAR LAS CANTIDADES
				// DE LAS FORMAS DE PAGO
			} else {
//				//com.google.gwt.user.client.Window.alert("elemento cambio else");
				/*
					 * String aux; if (item.getName().equals("listaRuc") ||
					 * item.getName().equals("listaNombres")) {//LISTA CLIENTES
					 * SEGUN CI try { if (item.getName().equals("listaRuc")) {
					 * aux = datosCl.getValueAsString("ruc_cliente"); if
					 * (aux.length() > 2 && aux.length() <= 9) //Analizamos si
					 * es una longitud mayor a 2 ira a buscar las coincidencias
					 * en la BD { // SE DEBE LLAMAR A LA FUNCION DE LA BASE Q
					 * DEVUELVA LAS COINCIDENCIAS SEGUN CI // SE DEBE
					 * ESPECIFICAR SI BUSCA UN CLIENTE, PROVEEDOR O EMPLEADO //
					 * SE DEBE MOSTRAR UNA BARRA DE CARGA PARA INDICAR EL
					 * PROCESOS DE ESTRACCION DE DATOS if(NumDto==0 || NumDto==2
					 * || NumDto==3 || NumDto==4)//FACTURA DE VENTA
					 * buscar_coincidencias(aux,"tblclientes","cedulaRuc"); else
					 * if(NumDto==1 || NumDto==7|| NumDto==10)//FACTURA DE
					 * COMPRA
					 * buscar_coincidencias(aux,"tblproveedors","cedulaRuc");
					 * return true; } else { return false; } } else { aux =
					 * datosCl.getValueAsString("cliente"); if (aux.length() > 3
					 * && aux.length() < 15) //Analizamos si es una longitud
					 * mayor a 5 ira a buscar las coincidencias en la BD { //SE
					 * DEBE LLAMAR A LA FUNCION DE LA BASE Q DEVUELVA LAS
					 * COINCIDENCIAS SEGUN NOMBRE if(NumDto==0 || NumDto==2 ||
					 * NumDto==3 || NumDto==4)//FACTURA DE VENTA
					 * buscar_coincidencias(aux,"tblclientes","apellidos"); else
					 * if(NumDto==1 || NumDto==7|| NumDto==10)//FACTURA DE
					 * COMPRA
					 * buscar_coincidencias(aux,"tblproveedors","apellidos");
					 * return true; } else { return false; } } } catch
					 * (Exception e) { //SC.say("Error: " + e); return false; }
					 * } else { //SC.say("No identifica los items de listado");
					 * return false; }
					 */
				return false;
			}
		}
	}

	/*
	 * FUNCIONES DE COMUNICACION CON LA BASE DE DATOS
	 */

	// Funcion que devuelve el listado de clientes segun numero de Cedula/RUC o
	// segun el NOMBRE
	/**
	 * Funcion para buscar coincidencias de clientes segun CI
	 * 
	 * @param CI
	 *            cedula del cliente a buscar
	 */

	private void obtener_UltimoDto(String filtro, String campoOrdenamiento) {
		// Analizamos de que tipo de documento se trata
		final AsyncCallback<DtocomercialDTO> Funcioncallback = new AsyncCallback<DtocomercialDTO>() {
			// String coincidencias = "";
			public void onSuccess(DtocomercialDTO result) {
				if (result != null) {
					txtAutorizacion_sri.setValue(result.getAutorizacion());
					Integer c = result.getNumRealTransaccion();
					c += 1;
					txtNumero_factura.setValue(c.toString());
				} else {
					// Aqui analizamos si es diferente de gasto
					if (NumDto != 7 && NumDto != 4 && NumDto != 12
							&& NumDto != 15 && NumDto != 20) {
						SC.say("Debe colocar el n\u00famero de Autorizaci\u00f3n");
					}
					txtNumero_factura.setValue("1");
				}
			}

			public void onFailure(Throwable caught) {
				SC.say("Error al acceder al servidor: " + caught);
			}
		};
		getService().ultimoDocumento(filtro, campoOrdenamiento, Funcioncallback);
	}

	// Funcion para buscar el cliente seleccionado en el Iterador iter
	public void CargarCliente() {
		// Obtenemos los datos del Iterador y los cargamos en los campos de
		// texto de la factura
		Integer codigo = 0;
		
		txtCLiente.setValue(
//				frmlisCli.lstCliente.getSelectedRecord().getAttribute("NombreComercial")
//				+ " "+ 
				frmlisCli.lstCliente.getSelectedRecord().getAttribute("RazonSocial")
				);
		persona.setRazonSocial(frmlisCli.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
		txtCLiente.setTooltip("Direccion: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Direccion")+" -- Telefono: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Telefonos"));
		txtCLiente.setHoverWidth(350);
		txtDireccion.setValue(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Direccion"));
		persona.setDireccion(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Direccion"));
		txtCorreo.setValue(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("E-mail"));
		persona.setMail(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("E-mail"));
		txtCorreo.setTooltip("Direccion: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Direccion")+" -- Telefono: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Telefonos"));
		txtCorreo.setHoverWidth(350);
		txtRuc_cliente.setValue(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Cedula"));
		persona.setCedulaRuc(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Cedula"));
		txtRuc_cliente.setTooltip("Direccion: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Direccion")+" -- Telefono: "+frmlisCli.lstCliente.getSelectedRecord()
		.getAttribute("Telefonos"));
		txtRuc_cliente.setHoverWidth(350);
		txtTelefono.setValue(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Telefonos"));
		persona.setTelefono1(frmlisCli.lstCliente.getSelectedRecord()
				.getAttribute("Telefonos"));
		winClientes.destroy();
		getService().ReprotePagosVencidosCliente(
				String.valueOf(codigoSeleccionado), objback);
	}

	// Funcion para mostrar los documentos segun el tipo

	public void mostrarDocumento(Integer tipo) {
		winDocumento = new Window();

		if (tipo == 3) {
			tipoS = "Notas de Credito";
		} else if (tipo == 4) {
			tipoS = "Anticipo Clientes";
		} else if (tipo == 5) {
			tipoS = "Retencion";
		} else if (tipo == 15) {
			tipoS = "Anticipo Proveedores";
		}else if (tipo == 14){
			tipoS = "Nota de Credito Proveedores";
		}
		winDocumento.setWidth(930);
		winDocumento.setHeight(500);
		winDocumento.setTitle("Escoger " + tipoS);
		winDocumento.setShowMinimizeButton(false);
		winDocumento.setIsModal(true);
		winDocumento.setShowModalMask(true);
		winDocumento.setKeepInParentRect(true);
		winDocumento.centerInPage();
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
				"display", "block");
		winDocumento.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				// idVendedor="";
				// txtVendedor.setValue("");
				winDocumento.destroy();
			}
		});
		VLayout form = new VLayout();
		form.setSize("100%", "100%");
		form.setPadding(5);
		form.setLayoutAlign(VerticalAlignment.BOTTOM);
		listaDocumentos = new frmReporteCaja(tipoS);
		listaDocumentos.setSize("100%", "100%");
		listaDocumentos.cmbDocumento.setValue(tipoS);
		//listaDocumentos.Tipo=tipo.toString();
		//listaDocumentos.generarReporte();
		listaDocumentos.cmbDocumento.disable();
		form.setMembers(listaDocumentos);
        listaDocumentos.lstReporteCaja.addDoubleClickHandler(new ManejadorDC("documentoPago"));
        registroPago = listaDocumentos.lstReporteCaja.getSelectedRecord();
        //getService().listarDocTipoPersona(tipo,codigoSeleccionado, callbackDtoTipo);
        //listaDocumentos.generarReporte();
        winDocumento.addItem(form);
        winDocumento.show();
	}

	/**
	 * Funcion para cargar todos los datos del documento a la interfaz grafica
	 * 
	 * @param doc
	 *            de tipo DtocomercialDTO, contiene el documento con toda la
	 *            informacion a mostrar
	 */
	public void cargarDocumento(DtocomercialDTO doc) {
//		//com.google.gwt.user.client.Window.alert("En cargar documento");
		codigoSeleccionado = doc.getTblpersonaByIdPersona().getIdPersona();
//		//com.google.gwt.user.client.Window.alert("set codigoSeleccionado "+codigoSeleccionado);
		idDto = doc.getIdDtoComercial();
		// Primero analizamos el tipo de documento para saber que campos mostrar
		idVendedor = doc.getTblpersonaByIdVendedor().getIdPersona();
//		//com.google.gwt.user.client.Window.alert("set idvendedor ");

		vendedor = new PersonaDTO();
		vendedor.setIdPersona(idVendedor);
//		com.google.gwt.user.client.Window.alert("persona vendedor ");
		analizarTipoDto(NumDto);
//		com.google.gwt.user.client.Window.alert("set codigoSeleccionado "+codigoSeleccionado);
		// Primero vamos a cargar la cabecera
		if (NumDto == 7 || NumDto == 4 || NumDto == 12 || NumDto == 15)// Si se											// DESCRIPCION
		{// Bloqueamos el listado de productos
			// cmbTipoPrecio.setVisible(false);
//			com.google.gwt.user.client.Window.alert("if numdto "+NumDto);
			txtaConcepto.setValue(doc.getAutorizacion());
			txtaConcepto.redraw();
//			//com.google.gwt.user.client.Window.alert("if numdto luego de concepto");
    		txtAutorizacion_sri.setValue(doc.getAutorizacion());
//    		//com.google.gwt.user.client.Window.alert("if numdto luego autorizacion SRI");
    		if (NumDto!=4 && NumDto!=15) txtNumCompra.setValue(doc.getNumCompra().replace("-", ""));
//    		//com.google.gwt.user.client.Window.alert("if numdto luego numCompra");
		}else if(NumDto == 14){
			String numeros[]= doc.getNumCompra().split(",");
			if (numeros.length>1){
				if (!numeros[1].contains("-")){
					getService().getDtoID(Integer.valueOf(numeros[1]), callbackDocAuxiliar);
				}else{
					String numFacturaCompra =numeros[1].replace("-", "");
					txtCorresp.setValue(numFacturaCompra);
				}
			}
			String[] observaciones=doc.getObservacion().split(";");
			if (observaciones.length>1){
				txtMotivo.setValue(observaciones[1]);
			}
			String numNotaCompra=numeros[0].replace("-", "");
			txtNumCompra.setValue(numNotaCompra);
			txtAutorizacion_sri.setValue(doc.getAutorizacion());
			
		}else if(NumDto == 3){
			if (!doc.getNumCompra().contains("-")){
				getService().getDtoID(Integer.valueOf(doc.getNumCompra()), callbackDocAuxiliar);
			}else{
				String numFacturaCompra = doc.getNumCompra().replace("-", "");
				txtCorresp.setValue(numFacturaCompra);
			}
			String[] observaciones=doc.getObservacion().split(";");
			if (observaciones.length>1){
				txtMotivo.setValue(observaciones[1]);
			}
//				String numFacturaCompra = doc.getNumCompra().replace("-", "");
//				txtCorresp.setValue(numFacturaCompra);
				txtAutorizacion_sri.setValue(doc.getAutorizacion());
		}else {
//			//com.google.gwt.user.client.Window.alert("if numdto "+NumDto);
			txtAutorizacion_sri.setValue(doc.getAutorizacion());
		}
//		com.google.gwt.user.client.Window.alert("fuera eleccion nudto "+NumDto);
		txtNumero_factura.setValue(doc.getNumRealTransaccion());
//		com.google.gwt.user.client.Window.alert("num real transaccion "+doc.getNumRealTransaccion());
		String fechaBase[] = doc.getFecha().split(" ");
		String orden[] = fechaBase[0].split("-");
		String fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
//		com.google.gwt.user.client.Window.alert("fecha text "+fecha);
		dateFecha_emision.setValue(fecha);
		persona=doc.getTblpersonaByIdPersona();
		txtCLiente.setValue(
//				doc.getTblpersonaByIdPersona().getNombreComercial()); 
//				+ " " + 
			doc.getTblpersonaByIdPersona().getRazonSocial());
		
//		//com.google.gwt.user.client.Window.alert("set cliente  ");
//		txtCLiente.setValue(doc.getTblpersonaByIdPersona().getNombreComercial() + " "
//				+ doc.getTblpersonaByIdPersona().getRazonSocial());
		txtRuc_cliente.setValue(doc.getTblpersonaByIdPersona().getCedulaRuc());
		txtDireccion.setValue(doc.getTblpersonaByIdPersona().getDireccion());
		txtCorreo.setValue(doc.getTblpersonaByIdPersona().getMail());
		txtCiudad.setValue(doc.getTblpersonaByIdPersona().getMail());
		txtTelefono.setValue(doc.getTblpersonaByIdPersona().getTelefono1());
		txtCLiente.setTooltip("Direccion: "+doc.getTblpersonaByIdPersona().getDireccion()
				+" -- Telefono: "+doc.getTblpersonaByIdPersona().getTelefono1());
		// OBSERVACION
		txtObservacion.setValue(doc.getObservacion());
//		//com.google.gwt.user.client.Window.alert("set datos cliente  ");
		if (Factum.banderaElegirVendedor==0){
			cmbVendedor.setValue(
					doc.getTblpersonaByIdVendedor().getNombreComercial()); 
			/*cmbVendedor.setValue(
					doc.getTblpersonaByIdVendedor().getRazonSocial()); */
		}else{
			cmbVendedor.setValue(
					doc.getTblpersonaByIdVendedor().getIdPersona().toString()); }
//				+ " " + 
//			doc.getTblpersonaByIdVendedor().getRazonSocial());
		if (NumDto == 1 || NumDto == 27) {
			txtNumCompra.setValue(doc.getNumCompra().replace("-", ""));
		}
		txtDescuentoNeto.setValue(doc.getDescuento());
		// txtCorresp.setValue();

		// Llamamos a la funcion para cargar los dealles de la factura
//		com.google.gwt.user.client.Window.alert("1"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
		try {
			if (NumDto != 7 && NumDto != 4 && NumDto != 12 && NumDto != 15) {
				cargarDetalles(doc.getTbldtocomercialdetalles());
				calculo_Subtotal();
				/*
				 * if(!(Boolean)dynamicForm.getField("chkCredito").getValue()&&
				 * !(Boolean)dynamicForm2.getField("chkRetencion").getValue()&&
				 * !(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
				 * !
				 * (Boolean)dynamicForm2.getField("chkNotaCredito").getValue()&&
				 * !(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
				 * !(Boolean)dynamicForm3.getField("chkBanco").getValue()) {
				 * dynamicForm.getField("chkContado").setValue(true);
				 * dynamicForm.getField("txtContado").setVisible(true);
				 * dynamicForm
				 * .getField("txtContado").setValue(txtTotal.getDisplayValue());
				 * dynamicForm.getField("txtContado").show(); }
				 */
				obtenerDetalles();
			}
//			//com.google.gwt.user.client.Window.alert("1.5"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			// SC.say("vale "+(Boolean)dynamicForm.getField("chkCredito").getValue());
			// Calcular los datos de total y subtotal

			if (NumDto == 7 || NumDto == 4 || NumDto == 12 || NumDto == 15
					|| NumDto == 10 ) {
				txtSubtotal.setValue(doc.getSubtotal());
				txtTotal.setValue(doc.getSubtotal());
				/*
				 * if(!(Boolean)dynamicForm.getField("chkCredito").getValue()&&
				 * !(Boolean)dynamicForm2.getField("chkRetencion").getValue()&&
				 * !(Boolean)dynamicForm2.getField("chkAnticipo").getValue() &&
				 * !
				 * (Boolean)dynamicForm2.getField("chkNotaCredito").getValue()&&
				 * !(Boolean)dynamicForm3.getField("chkTarjetaCr").getValue()&&
				 * !(Boolean)dynamicForm3.getField("chkBanco").getValue()) {
				 * 
				 * dynamicForm.getField("chkContado").setValue(true);
				 * dynamicForm.getField("txtContado").setVisible(true);
				 * dynamicForm.getField("txtContado").show();
				 * dynamicForm.getField
				 * ("txtContado").setValue(txtTotal.getDisplayValue()); }
				 */
			}
//			//com.google.gwt.user.client.Window.alert("1.6"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			// SC.say("SUBTOTAL GASTO: ");
			if (NumDto != 20) { // Si no es proforma tiene pagos :JS
				if(NumDto==0 || NumDto==1) cargarRetencion(doc.getTblretencionsForIdFactura());
				dynamicForm.getField("chkContado").setValue(false);
//				 dynamicForm.getField("txtContado").setVisible(false);
				 dynamicForm.getField("txtContado").setValue("");
				cargarFormasPago(doc);
			}
//			//com.google.gwt.user.client.Window.alert("1.7"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
		} catch (Exception e) {
			SC.say("Error aqui: " + e);
		}
//		//com.google.gwt.user.client.Window.alert("2"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
		try {
//			if (NumDto != 20) { // Si no es proforma tiene pagos :JS
////				//com.google.gwt.user.client.Window.alert("2.1 No proforma");
//				if(NumDto==0 || NumDto==1) cargarRetencion(doc.getTblretencionsForIdFactura());
//				dynamicForm.getField("chkContado").setValue(false);
////				 dynamicForm.getField("txtContado").setVisible(false);
////				 dynamicForm.getField("txtContado").setValue("");
////				 //com.google.gwt.user.client.Window.alert("2.2 antes de cargar forma pago");
//				cargarFormasPago(doc);
//			}
//			//com.google.gwt.user.client.Window.alert("3"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			if (NumDto==0 || NumDto==1) {
				if (!(Boolean) dynamicForm.getField("chkCredito").getValue()
						&& !(Boolean) dynamicForm2.getField("chkRetencion")
								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkAnticipo")
								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkNotaCredito")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkTarjetaCr")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkBanco")
								.getValue()) {
					dynamicForm.getField("chkContado").setValue(true);
					
//					dynamicForm.getField("txtContado").setVisible(true);
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					Double TotalAux = Double.parseDouble(total);
					TotalAux = CValidarDato.getDecimal(2, TotalAux);
					total = String.valueOf(TotalAux);
					dynamicForm.getField("txtContado").setValue(total);
					dynamicForm.getField("txtContado").show();
				}
			}
			if (NumDto != 7 && NumDto != 4 && NumDto != 12 && NumDto != 15
					&& NumDto != 20 && NumDto!=0 && NumDto!=1 && NumDto!=3 && NumDto!=14
					&& NumDto != 26 && NumDto!=27) {
				if (!(Boolean) dynamicForm.getField("chkCredito").getValue()
//						&& !(Boolean) dynamicForm2.getField("chkRetencion")
//								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkAnticipo")
								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkNotaCredito")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkTarjetaCr")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkBanco")
								.getValue()) {
					dynamicForm.getField("chkContado").setValue(true);
//					dynamicForm.getField("txtContado").setVisible(true);
					
//					dynamicForm.getField("txtContado").setVisible(true);
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					Double TotalAux = Double.parseDouble(total);
					TotalAux = CValidarDato.getDecimal(2, TotalAux);
					total = String.valueOf(TotalAux);
					dynamicForm.getField("txtContado").setValue(total);
					dynamicForm.getField("txtContado").show();
				}
			}
//			//com.google.gwt.user.client.Window.alert("4"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			if (NumDto == 7 || NumDto == 12) {
				txtSubtotal.setValue(doc.getSubtotal());
				txtTotal.setValue(doc.getSubtotal());
				if (!(Boolean) dynamicForm.getField("chkCredito").getValue()
//						&& !(Boolean) dynamicForm2.getField("chkRetencion")
//								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkAnticipo")
								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkNotaCredito")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkTarjetaCr")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkBanco")
								.getValue()) {
					dynamicForm.getField("chkContado").setValue(true);
					
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					Double TotalAux = Double.parseDouble(total);
					TotalAux = CValidarDato.getDecimal(2, TotalAux);
					total = String.valueOf(TotalAux);
					dynamicForm.getField("txtContado").setValue(total);
//					dynamicForm.getField("txtContado").setVisible(true);
					dynamicForm.getField("txtContado").show();
				}
			}
			if (NumDto == 26 || NumDto == 27) {
				txtSubtotal.setValue(doc.getSubtotal());
				txtTotal.setValue(doc.getSubtotal());
				if (!(Boolean) dynamicForm.getField("chkCredito").getValue()
//						&& !(Boolean) dynamicForm2.getField("chkRetencion")
//								.getValue()
						&& !(Boolean) dynamicForm2.getField("chkAnticipo")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkTarjetaCr")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkBanco")
								.getValue()) {
					dynamicForm.getField("chkContado").setValue(true);
					
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					Double TotalAux = Double.parseDouble(total);
					TotalAux = CValidarDato.getDecimal(2, TotalAux);
					total = String.valueOf(TotalAux);
					dynamicForm.getField("txtContado").setValue(total);
//					dynamicForm.getField("txtContado").setVisible(true);
					dynamicForm.getField("txtContado").show();
				}
			}
//			//com.google.gwt.user.client.Window.alert("5"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			if (NumDto == 4 || NumDto == 15 || NumDto==3 || NumDto==14) {
				if (NumDto == 4 || NumDto == 15 ){
					txtSubtotal.setValue(doc.getSubtotal());
					txtTotal.setValue(doc.getSubtotal());
				}
				
				if (!(Boolean) dynamicForm3.getField("chkTarjetaCr")
								.getValue()
						&& !(Boolean) dynamicForm3.getField("chkBanco")
								.getValue()) {
					dynamicForm.getField("chkContado").setValue(true);
					
					String total = txtTotal.getDisplayValue();
					total = total.replace(",", "");
					Double TotalAux = Double.parseDouble(total);
					TotalAux = CValidarDato.getDecimal(2, TotalAux);
					total = String.valueOf(TotalAux);
					dynamicForm.getField("txtContado").setValue(total);
//					dynamicForm.getField("txtContado").setVisible(true);
					dynamicForm.getField("txtContado").show();
				}
			}
//			//com.google.gwt.user.client.Window.alert("6"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
			// SC.say("SUBTOTAL GASTO: ");
			
		} catch (Exception e) {
			SC.say("Error: sale " + e);
		}
//		//com.google.gwt.user.client.Window.alert("7"+String.valueOf(((Boolean)dynamicForm.getField("chkContado").getValue())));
		// Analizamos si el documento ya esta confirmado para bloquearlo y solo
		// mostrarlo
		// if(doc.getEstado()==1)
		funcionBloquear(true, doc.getEstado());
//		//com.google.gwt.user.client.Window.alert("7 Despues de bloquear");
	
		

		if (String.valueOf(doc.getEstado()).equals("1")){// para que desabilite
														// el boton confirmar al
														// momento de la factura		
			if (NumDto!=4 && NumDto!=15) grdProductos.disable();
			if (NumDto==0 || NumDto==1){
				btnDocumentoElectronico.setDisabled(false);//XAVIER ZEAS 17-03-2017
			}
		}
		if (NumDto == 0 || (NumDto == 1) || (NumDto == 2) || NumDto ==26 || NumDto == 27) {// REPORTE DE LOS PAGOS VENCIDOS
			getService().ReprotePagosVencidosCliente(
					String.valueOf(doc.getTblpersonaByIdPersona()
							.getIdPersona()), objback);
		}
	}

	public void cargarRetencion(Set<RetencionDTO> retenciones) {
		Double totalRet = 0.0;
		if (!retenciones.isEmpty()) {
			dynamicForm2.getField("chkRetencion").setValue(true);
			Iterator iter = retenciones.iterator();
			tblRetenciones = retenciones;
//			//com.google.gwt.user.client.Window.alert("Hay retenciones "+tblRetenciones.size());
			while (iter.hasNext()) {
				RetencionDTO ret = (RetencionDTO) iter.next();
//				//com.google.gwt.user.client.Window.alert("Hay retencione "+ret.getIdRetencion());
				if (ret.getTbldtocomercialByIdDtoComercial()!=null){
					docAuxiliar = new DtocomercialDTO();
					docAuxiliar=ret.getTbldtocomercialByIdDtoComercial();
//					String fechaBase[] = docAuxiliar.getFecha().split(" ");
//					String orden[] = fechaBase[0].split("-");
//					String fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
					String fechaBase[] = docAuxiliar.getFecha().split(" ");
//					com.google.gwt.user.client.Window.alert("fecha text "+Arrays.toString(fechaBase));
					String orden[] = fechaBase[0].split("-");
//					com.google.gwt.user.client.Window.alert("fecha text "+Arrays.toString(orden));
					String fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
//					com.google.gwt.user.client.Window.alert("fecha text "+fecha);
					docAuxiliar.setFecha(fecha);
					fechaBase = docAuxiliar.getExpiracion().split(" ");
//					com.google.gwt.user.client.Window.alert("fecha text "+Arrays.toString(fechaBase));
					orden = fechaBase[0].split("-");
//					com.google.gwt.user.client.Window.alert("fecha text "+Arrays.toString(orden));
					fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
//					com.google.gwt.user.client.Window.alert("fecha text "+fecha);
					docAuxiliar.setExpiracion(fecha);
				}
//				//com.google.gwt.user.client.Window.alert("Fecha documento retencion "+retencion.getFecha());
//				//com.google.gwt.user.client.Window.alert("Fecha documento retencion "+retencion.getExpiracion());
				totalRet += ret.getValorRetenido();
			}
			totalRet = CValidarDato.getDecimal(2, totalRet);
			dynamicForm2.getItem("txtRetencion").setValue(totalRet);
			dynamicForm2.redraw();
		}

	}

	public List<DtoComDetalleDTO> ordenarProductos(Set<DtoComDetalleDTO> lista) {

		List<DtoComDetalleDTO> milista = new ArrayList<DtoComDetalleDTO>(lista);
		for (int j = 0; j < milista.size(); j++) {
			for (int i = j + 1; i < milista.size(); i++) {
				DtoComDetalleDTO midetalle = (DtoComDetalleDTO) milista.get(i);// Idet.next();
				DtoComDetalleDTO midetalle1 = (DtoComDetalleDTO) milista.get(j);// Idet.next();
				
				if (midetalle.getDesProducto().contains(";") && midetalle1.getDesProducto().contains(";"))
				{
					////com.google.gwt.user.client.Window.alert("si hay ;");
					int ind0=Integer.valueOf(midetalle.getDesProducto().split(";")[0]);
					int ind1=Integer.valueOf(midetalle1.getDesProducto().split(";")[0]);
					////com.google.gwt.user.client.Window.alert("si hay ;"+ind0+" "+ind1);
					if (ind0<ind1){
						DtoComDetalleDTO temp = midetalle1;// .getDesProducto();
						milista.set(j, midetalle);
						milista.set(i, temp);
					}
				}
				else if (midetalle.getDesProducto().compareTo(
						midetalle1.getDesProducto()) < 0) {
					////com.google.gwt.user.client.Window.alert("no hay ;");
					DtoComDetalleDTO temp = midetalle1;// .getDesProducto();
					milista.set(j, midetalle);
					milista.set(i, temp);
				}
			}
			// mibod.setValue(mibod.getDisplayValue()+" "+milista.get(j).getDesProducto());
		}
		return milista;
	}

	public void cargarDetalles(Set<DtoComDetalleDTO> detalles) {
		List<DtoComDetalleDTO> milista = new ArrayList<DtoComDetalleDTO>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Integer det = 0;
		// ORDENAR PRODUCTOS ANTES DE MOSTRAR
		milista = ordenarProductos(detalles);

		// mibod.setValue(mibod.getDisplayValue()+" en el metodo"+milista.size()+milista.get(0).getDesProducto()+"sali");
		for (int j = 0; j < milista.size(); j++) {
			DtoComDetalleDTO detalle = (DtoComDetalleDTO) milista.get(j);
			// Utils.getMapFromRow(dsFields, getResultRow())
			resultMap.put("idProducto", detalle.getProducto().getIdProducto());// registro.getAttributeAsString("idProducto"));
			resultMap.put("codigoBarras", detalle.getProducto()
					.getCodigoBarras());
			resultMap.put("cantidad", detalle.getCantidad());
			// mibod.setValue(mibod.getDisplayValue()+" describo "+detalle.getDesProducto());

			// SACAR LOS NUMEROS DEL INICIO DE DECRIPCION
			String[] miseparador = detalle.getDesProducto().split(";");
			if (miseparador.length>1){
				resultMap.put("numero", miseparador[0]);
				resultMap.put("descripcion", miseparador[1]);
			}else{
				resultMap.put("numero", j+1);
				resultMap.put("descripcion",detalle.getDesProducto());
			}
			
			String impuestos="";
			double impuestoPorc=1.0;
			double impuestoValor=0.0;
			int i1=0;
//			com.google.gwt.user.client.Window.alert("impuestos de detalle "+detalle.getTblimpuestos().size());
			for (DtoComDetalleMultiImpuestosDTO detalleImp: detalle.getTblimpuestos()){
				i1=i1+1;
//				com.google.gwt.user.client.Window.alert("en for impuestos de detalle "+i1+" "+detalleImp.getPorcentaje().toString());
				impuestos+=detalleImp.getPorcentaje().toString();
				impuestos= (i1<detalle.getTblimpuestos().size())?impuestos+",":impuestos; 
				impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
				impuestoValor=impuestoValor+((detalle.getCantidad()*detalle.getPrecioUnitario()*(1-(detalle.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
			}
//			com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
			resultMap.put("precioUnitario", detalle.getPrecioUnitario());// Float.parseFloat(registro.getAttributeAsString("promedio")));
			double subTotal = 0.0;
			subTotal = detalle.getTotal();
			subTotal = Math.rint(subTotal * 10000) / 10000;
			resultMap.put("valorTotal", subTotal);
//			resultMap.put("iva", detalle.getImpuesto());
			resultMap.put("iva", impuestos);
			 resultMap.put("stock",
			 0);//Integer.parseInt(registro.getAttributeAsString("stock")));
			//resultMap.put("stock", detalle.getProducto().getStock());// Integer.parseInt(registro.getAttributeAsString("stock")));
//			Cambios para controlar el numero de decimales que muestra el stock
//			resultMap.put("stock", CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
//					.getAttributeAsString("stock"))) );
			Double precioConIva = Double.valueOf(detalle.getPrecioUnitario())
//					+ (Double.valueOf(detalle.getPrecioUnitario()) * 
//							Double.valueOf(detalle.getImpuesto() / 100));
						*impuestoPorc;
			precioConIva = Math.rint(precioConIva * 100) / 100;
			precioConIva = CValidarDato.getDecimal(2, precioConIva);
			NumberFormat formato2 = NumberFormat.getFormat("#.00");
			resultMap.put("precioConIva", formato2.format(precioConIva));
			// resultMap.put("bodega",
			// Integer.parseInt(registro[0].getAttributeAsString("bodega")));
			resultMap.put("descuento", detalle.getDepartamento());
			//resultMap.put("numero", det + 1);
			// resultMap.put("stockPadre", detalle.getStockPadre());
			grdProductos.setEditValues(/* det */j, resultMap);
			grdProductos.setEditValue(/* det */j, "costo", detalle.getProducto()
					.getPromedio());
			grdProductos.setEditValue(/* det */j, "idbodega", detalle.getBodega()
					.getIdBodega());
			grdProductos.setEditValue(/* det */j, "bodega", detalle.getBodega()
					.getBodega());
			grdProductos.setEditValue(/* det */j, "ubicacion", detalle
					.getBodega().getUbicacion());
			grdProductos.setEditValue(/* det */j, "telefono", detalle.getBodega()
					.getTelefono());
			// det++;
			if (Bodega == null) {
				Bodega = new BodegaDTO();
				Bodega.setIdEmpresa(detalle.getBodega().getIdEmpresa());
				Bodega.setEstablecimiento(detalle.getBodega().getEstablecimiento());
				Bodega.setIdBodega(detalle.getBodega().getIdBodega());
				Bodega.setBodega(detalle.getBodega().getBodega());
				Bodega.setUbicacion(detalle.getBodega().getUbicacion());
				Bodega.setTelefono(detalle.getBodega().getTelefono());
				BodegaGRABAR = Bodega;
			}
			grdProductos.startEditingNew();
		}
	}

	public void cargarFormasPago(DtocomercialDTO doc) {
		// Aqui vamos a ca3rgar las formas de pago asociadas al documento
//		//com.google.gwt.user.client.Window.alert("EN cargar formas de pago ");
		Set<DtocomercialTbltipopagoDTO> pagos = doc.getTbldtocomercialTbltipopagos();
		Iterator Ipagos = pagos.iterator();
		while (Ipagos.hasNext()) {
			DtocomercialTbltipopagoDTO pago = (DtocomercialTbltipopagoDTO) Ipagos.next();
//			//com.google.gwt.user.client.Window.alert("Tipo pago "+String.valueOf(pago.getTipoPago()));
			switch (pago.getTipoPago()) {
			case 1: {// CONTADO
//				//com.google.gwt.user.client.Window.alert("Contado "+pago.getCantidad());
				dynamicForm.getField("chkContado").setValue(true);
				dynamicForm.getField("txtContado").show();
//				dynamicForm.getField("txtContado").setVisible(true);
				dynamicForm.getField("txtContado").setValue(pago.getCantidad());
				
				break;
			}
			case 2: {// CREDITO
//				//com.google.gwt.user.client.Window.alert("Credito "+pago.getCantidad());
				dynamicForm.getField("chkCredito").setValue(true);
				// Llamamos a la funcion para cargar los pagos en la grilla de
				// pagos
				Integer pag = cargarPagos(doc.getTblpagos());
				// AQUI DEBE IR EL NUMERO DE PAGOS Y PASAR LOS PAGOS A LA GRILLA
				// DE PAGOS
				dynamicForm.getField("txtCredito").setValue(pag);
				
				break;
			}
			case 3: {// RETENCION
//				//com.google.gwt.user.client.Window.alert("Retencion "+pago.getCantidad());
				dynamicForm2.getField("chkRetencion").setValue(true);
				dynamicForm2.getField("txtRetencion").setValue(
						pago.getCantidad());
				
				break;
			}
			case 4: {// ANTICIPO
//				//com.google.gwt.user.client.Window.alert("Anticipo "+pago.getCantidad());
				dynamicForm2.getField("chkAnticipo").setValue(true);
				// dynamicForm2.getField("txtAnticipo").setValue(pago.ge.getReferenciaDto());
				dynamicForm2.getItem("txtAnticipo").setHint(
						pago.getReferenciaDto() + "");
				dynamicForm2.getItem("txtAnticipo")
						.setValue(pago.getCantidad());
				totalAnt = pago.getCantidad();
				dynamicForm2.redraw();

				
				break;
			}
			case 5: {// NOTA
//				//com.google.gwt.user.client.Window.alert("Nota "+pago.getCantidad());
				dynamicForm2.getField("chkNotaCredito").setValue(true);
				dynamicForm2.getField("txtNotaCredito").setHint(
						pago.getReferenciaDto() + "");
				dynamicForm2.getItem("txtNotaCredito")
				.setValue(pago.getCantidad());
				totalNota=pago.getCantidad();
				//totalAnt = pago.getCantidad();
				dynamicForm2.redraw();
				break;
			}
			case 6: {// TARJETA
				dynamicForm3.getField("chkTarjetaCr").setValue(true);
				dynamicForm3.getField("txtTarjetaCr").setValue(
						pago.getCantidad());
				dynamicForm3.getField("txtTarjetaCr1").setValue(
						pago.getObservaciones());
				
				break;
			}
			case 7: {// BANCO
				dynamicForm3.getField("chkBanco").setValue(true);
				dynamicForm3.getField("txtBanco").setValue(pago.getCantidad());
				dynamicForm3.getField("txtBanco1").setValue(
						pago.getObservaciones());
				
				break;
			}
			}
			dynamicForm.redraw();
			dynamicForm2.redraw();
			dynamicForm3.redraw();
		}
	}

	// ***********************************************************************************************************
	// FUNCION PARA AGREGAR EL STOCK A LAS BODEGAS EN CASO DE COMPRA DE UN
	// PRODUCTO
	// ***********************************************************************************************************

	// ***********************************************************************************************************
	// ***********************************************************************************************************

	public Integer cargarPagos(Set<TblpagoDTO> pagos) {
		// Aqui vamos a cargar los pagos a la grilla
		Integer numPagos = 0;

		Iterator Ipagos = pagos.iterator();
		TblpagoDTO PagoDTO = new TblpagoDTO();
		Integer estado = 0;

		while (Ipagos.hasNext()) {
			PagoDTO = (TblpagoDTO) Ipagos.next();
			estado = Integer.parseInt(PagoDTO.getEstado() + "");
			grdPagos.setEditValue(numPagos, "cantidadPago", PagoDTO.getValor());
			grdPagos.setEditValue(numPagos, "vencimiento",
					PagoDTO.getFechaVencimiento());
			grdPagos.setEditValue(numPagos, "estado", estado);
			grdPagos.setEditValue(numPagos, "id", PagoDTO.getIdPago());
			if (!estado.equals(0)){
				grdPagos.setEditValue(numPagos, "numegreso", PagoDTO.getNumEgreso());
//				//com.google.gwt.user.client.Window.alert(String.valueOf(!PagoDTO.getFechaRealPago().equals(null)));
//				if (!PagoDTO.getFechaRealPago().equals(null)) grdPagos.setEditValue(numPagos, "fecharealpago", PagoDTO.getFechaRealPago());
				if (PagoDTO.getFechaRealPago()!=null) grdPagos.setEditValue(numPagos, "fecharealpago", PagoDTO.getFechaRealPago());
//				//com.google.gwt.user.client.Window.alert(String.valueOf(!PagoDTO.getConcepto().equals(null)));
				if (PagoDTO.getConcepto()!=null) {
//					//com.google.gwt.user.client.Window.alert(PagoDTO.getConcepto());
					grdPagos.setEditValue(numPagos, "concepto", PagoDTO.getConcepto());
				}
//				//com.google.gwt.user.client.Window.alert(String.valueOf(!PagoDTO.getFormaPago().equals(null)));
				if (PagoDTO.getFormaPago()!=null) {
//					//com.google.gwt.user.client.Window.alert("Forma pago "+PagoDTO.getFormaPago());
					grdPagos.setEditValue(numPagos, "formapago", PagoDTO.getFormaPago());
				}
//				//com.google.gwt.user.client.Window.alert("Forma pago activo-- "+pagoActivo);
				pagoActivo=true;
//				//com.google.gwt.user.client.Window.alert("Forma pago activo-- "+pagoActivo);
			}
			numPagos++;
		}
		grdPagos.saveAllEdits();
		grdPagos.setVisible(true);
		return numPagos;
	}

	public void funcionBloquear(Boolean valor, char estado) {
		// Esta funcion bloqueara los controles de la ventana a excepcion del
		// boton imprimir
		String est = estado + "";
		// if(DocFis>=0)
		// {
		// txtAutorizacion_sri.setDisabled(valor);
		// txtNumero_factura.setDisabled(valor);

		// .setDisabled(valor);

		// }else{
		btnImprimir.setDisabled(false);
		btnEliminar.setVisible(false);
		if (est.equals("0")) {// NO SE CONFIRMA TODAVIA
			btnGrabar.setTitle("CONFIRMAR");
			if (btnGrabar.getTitle().equals("CONFIRMAR")) {
				// btnAsignarPuntos.enable();
			}
			btnEliminar.setVisible(true);
			// le documento y le pongo mas abajo para que se desabilite el boton
			// confirmar en proforma
			// if(NumDto==20 && DocFis>=0)
			// {
			// btnGrabar.disable();
			// }
		} else if (est.equals("1") || est.equals("5")) {// cambiado por Israel
														// solo esta linea
			btnGrabar.setTitle("CONFIRMADO");
			btnImprimir.setTitle("REIMPRIMIR");
			// btnGrabar.setDisabled(true);
			btnNuevo.setTitle("ANULAR");
			// btnNuevo.setDisabled(true);
		} else if (est.equals("2")) {
			btnGrabar.setTitle("ANULADO");
			btnImprimir.setTitle("REIMPRIMIR");
			// btnGrabar.setDisabled(true);
			btnNuevo.setTitle("ANULADO");
			// btnNuevo.setDisabled(true);
		}
		if (est.equals("1")){
			btnGrabar.setDisabled(valor);
			bloqueo(valor);
			btnNuevo.setDisabled(false);
		}else if (est.equals("2")) {
			btnGrabar.setDisabled(valor);
			btnNuevo.setDisabled(valor);
			bloqueo(valor);
		} else {
			if (tipoUsuario == 1) {
				bloqueo(false);

				// btnGrabar.setDisabled(true);

			} else if (tipoUsuario == 5)// Usuario CAJA
			{
				if (est.equals("0")) {
					bloqueo(false);
				} else {
					bloqueo(true);
				}
			}
		}
		if (est.equals("0"))// para que desabilite el boton confirmar al momento
							// de la factura
		{
			if (NumDto == 20 && DocFis >= 0) {
				btnNuevaFactura.hide();
				
				btnGrabar.disable();
			}
		}

		if (btnGrabar.getTitle().equals("CONFIRMADO"))
			btnGrabar.setDisabled(true);
		/*
		 * if(tipoUsuario==1 && !est.equals("2")) {//PUEDE CONFIRMAR
		 * //btnGrabar.setDisabled(false); valor = false; bloqueo(valor);
		 * //btnImprimir.setTitle("REIMPRIMIR"); }else{ bloqueo(true); }
		 */
		/*
		 * if(est.equals("1")) { btnGrabar.setDisabled(true);
		 * btnImprimir.setDisabled(false); }
		 */
		// }
		// Analizamos que nivel de acceso tiene para poder mostrar el boton de
		// confirmacion

	}

	public void bloqueo(Boolean valor) {
		dateFecha_emision.setDisabled(valor);

		txtCLiente.setDisabled(valor);
		txtRuc_cliente.setDisabled(valor);
		txtDireccion.setDisabled(valor);
		txtCorreo.setDisabled(valor);
		//lgfPrecio_unitario.setCanEdit(valor);
		if (NumDto!=4 && NumDto!=15) txtCiudad.setDisabled(valor);
		txtTelefono.setDisabled(valor);
		cmbVendedor.setDisabled(valor);

		if (NumDto!=4 && NumDto!=15) grdProductos.setDisabled(valor);
		dynamicForm.setDisabled(valor);
		if (NumDto!=4 && NumDto!=15) dynamicForm2.setDisabled(valor);
		dynamicForm3.setDisabled(valor);

		if (NumDto!=4 && NumDto!=15) grdPagos.setDisabled(valor);
		btnGrabar.setDisabled(valor);
		btnNuevo.setDisabled(valor);
	}
	final AsyncCallback<ProductoDTO> callbackProductoPeso = new AsyncCallback<ProductoDTO>() {
		// String coincidencias = "";
		public void onSuccess(ProductoDTO result) {
			if (result != null) {
				ProductoRecords grid = new ProductoRecords(result);
				
				
				//grid.setAttribute("observacion", " ");
				grid.setAttribute("cantidad", peso);
				
				CargarProductoBarras(grid);
				grdProductos.saveAllEdits();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");
			} else {
				int nuevaCelda = grdProductos.getTotalRows();
				grdProductos.setEditValue(nuevaCelda - 1, "codigoBarras", "");
				// SC.say("No Existe el producto");
				grdProductos.saveAllEdits();
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};
	
	final AsyncCallback<ProductoDTO> callbackProducto = new AsyncCallback<ProductoDTO>() {
		// String coincidencias = "";
		public void onSuccess(ProductoDTO result) {
			if (result != null) {
				//ProductoDTO productoActual=result;
				if (result.getTblbodega().size()!=0){
//					com.google.gwt.user.client.Window.alert(String.valueOf(linkedhashmapproductosAct.containsKey(Integer.valueOf(result.getIdProducto()))));
					if (!linkedhashmapproductosAct.containsKey(Integer.valueOf(result.getIdProducto()))){
						linkedhashmapproductosAct.put(Integer.valueOf(result.getIdProducto()), result);
					}
//					com.google.gwt.user.client.Window.alert(String.valueOf(linkedhashmapproductosAct.size()));
					ProductoRecords grid = new ProductoRecords(result);
					
					
					//grid.setAttribute("observacion", " ");
					grid.setAttribute("cantidad", 1);
					
					CargarProductoBarras(grid);
					//grdProductos.saveAllEdits();
					//grdProductos.redraw();
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
							"display", "none");
				}else{
					
					SC.warn("No se puede realizar transacciones con el producto ya que no se encuentra asignado a una bodega",
							new BooleanCallback() {
						public void execute(Boolean value) {
							int nuevaCelda = grdProductos.getTotalRows();
							grdProductos.setEditValue(nuevaCelda - 1, "codigoBarras",  "");
							//getSelectedRecord().setAttribute("codigoBarras", "");
							
							grdProductos.startEditing(nuevaCelda - 1, 2, false);
							grdProductos.saveAllEdits();
						}
					}
						);
					
				}
			} else {
				int nuevaCelda = grdProductos.getTotalRows();
				grdProductos.setEditValue(nuevaCelda - 1, "codigoBarras", "");
				// SC.say("No Existe el producto");
				grdProductos.saveAllEdits();
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};
	final AsyncCallback<List<DtocomercialDTO>> callbackDtoTipo = new AsyncCallback<List<DtocomercialDTO>>() {
		// String coincidencias = "";
		public void onSuccess(List<DtocomercialDTO> result) {
			if (result != null) {
				ListGridRecord[] listado = new ListGridRecord[result.size()];

				for (int i = 0; i < result.size(); i++) {
					DtocomercialRecords doc = new DtocomercialRecords(
							result.get(i));
					listado[i] = (new DtocomercialRecords(result.get(i)));
				}
				lstDocumentos.setData(listado);
				lstDocumentos.redraw();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");
			} else {
				SC.say("No hay documentos ingresados");
			}
		}

		public void onFailure(Throwable caught) {
			SC.say("Error al acceder al servidor: " + caught);
		}
	};

	// MANEJADOR ASINCRONO PARA OBTENER EL LISTADO DE BODEGAS Y STOCK DE
	// DETERMINADO PRODUCTO

	final AsyncCallback<List<BodegaProdDTO>> objbacklstBOD = new AsyncCallback<List<BodegaProdDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");

			SC.say("Error no se conecta  a la base" + caught);
		}

		public void onSuccess(List<BodegaProdDTO> result) {

			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for (int i = 0; i < result.size(); i++) {
				listado[i] = (new BodegaRecords((BodegaProdDTO) result.get(i)));
			}
			// AQUI DEBEMOS CARGAR LO DE LA BODEGA Y LISTO

			Bodega = new BodegaDTO(listado[0].getAttributeAsInt("idBodega"),
					listado[0].getAttributeAsString("Bodega"),
					listado[0].getAttributeAsString("Ubicacion"),
					listado[0].getAttributeAsString("Telefono"),
					Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento());

			if (BodegaGRABAR == null) {
				BodegaGRABAR = Bodega;
			}
			
			rowNumGlobal += 1;
			grdProductos.saveAllEdits();
			// ********************************************
			// listGrid.setData(listado);
			// listGrid.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
	};

	/**
	 * Manejador asincrono para la funcion de comprobacion del tipo de usuario
	 */
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

		public void onSuccess(User result) {
			// Segun sea el tipo de Usuario que este grabando la factura sabemos
			// si se deben grabar
			// los asientos contables respectivos o no
			// El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra
			// CONFIRMAR el dto
			if (result == null) {//La sesion expiro
        		
                SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

                    public void execute(Boolean value) {
                        if (value) {
                        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
                            //getService().LeerXML(asyncCallbackXML);
                        }
                    }
                });
            }else{
			if (DocFis < 0) {
				idVendedor = result.getIdUsuario();
				vendedor = new PersonaDTO();
				vendedor.setIdPersona(idVendedor);
				if (Factum.banderaElegirVendedor==0) cmbVendedor.setValue(result.getUserName());
				else cmbVendedor.setValue(result.getIdUsuario().toString());
				
				usuario = result;
				tipoUsuario = result.getNivelAcceso();
				planCuentaID=result.getPlanCuenta();
				
//				if (tipoUsuario == 5) {//REVISAR LOS PERMISOS DEL USUARIO
//					if(Factum.banderaCambiarPVP==1){
//						lgfPrecio_unitario.setCanEdit(false);
//					}else{
//						lgfPrecio_unitario.setCanEdit(true);
//					}
//					if(Factum.banderaCambiarDescuento==1){
//						lgfDEscuento.setCanEdit(true);
//					}else{
//						lgfDEscuento.setCanEdit(false);
//					}					
//						
//				}
				
			
//				if (tipoUsuario == 1) {//REVISAR LOS PERMISOS DEL USUARIO
//					if(Factum.banderaCambiarPVP==1){
//						lgfPrecio_unitario.setCanEdit(false);
//					}else{
//						lgfPrecio_unitario.setCanEdit(true);
//					}
//					if(Factum.banderaCambiarDescuento==1){
//						lgfDEscuento.setCanEdit(false);
//					}else{
//						lgfDEscuento.setCanEdit(false);
//					}					
//						
//				}
				
				

			} else {
				tipoUsuario = result.getNivelAcceso();
			}
//			SC.say(String.valueOf(tipoUsuario));
			if(tipoUsuario==1 || tipoUsuario==3){
				lgfPrecio_unitario.setCanEdit(true);
				lgfPrecio_iva.setCanEdit(true);
			}else{
				lgfPrecio_unitario.setCanEdit(false);
				lgfPrecio_iva.setCanEdit(false);
			}
			
			
			
            }
			// funcionBloquear(true);
		}

		public void onFailure(Throwable caught) {
			tipoUsuario = null;
			SC.say("No se puede obtener el nombre de usuario");
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
		}
	};

	/**
	 * Manejador asincrono para la funcion de carga de Documento comercial
	 */
	final AsyncCallback<DtocomercialDTO> callbackDoc = new AsyncCallback<DtocomercialDTO>() {

		public void onSuccess(DtocomercialDTO result) {
			// Segun sea el tipo de Usuario que este grabando la factura sabemos
			// si se deben grabar
			// los asientos contables respectivos o no
			// El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra
			// CONFIRMAR el dto
			// ipoUsuario = result.getTipoUsuario();
			// SC.say("Tipo de usuario: "+result.getTipoUsuario());
			DocBase = result;
			documentoenviar=result;
			cargarDocumento(result);
		}

		public void onFailure(Throwable caught) {
			SC.say("No se puede cargar el documento solicitado");
		}
	};

	/*
	 * 
	 * FUNCIONES DEL LISTADO DE PRODUCTO
	 */
	public void buscarL() {
		int ban = 1;
		Integer Tipo=0;
		Integer Jerarquia=0;
		String nombre = searchFormProducto.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String tabla = searchFormProducto.getItem("cmbBuscar").getDisplayValue();
		String campo = null;
		if (tabla.equals("Ubicaci\u00F3n")) {
			ban = 3;
			tabla = "codigoBarras";
		} else if (tabla.equals("C\u00F3digo de Barras")) {
			ban = 1;
			tabla = "codigoBarras";
		} else if (tabla.equals("Descripci\u00F3n") || tabla.equals("")) {
			ban = 1;
			tabla = "descripcion";
		} else if (tabla.equals("Marca")) {
			ban = 0;
			campo = "marca";
			tabla = "tblmarca";
		} else if (tabla.equals("Categoria")) {
			ban = 0;
			campo = "categoria";
			tabla = "tblcategoria";
		} else if (tabla.equals("Unidad")) {
			ban = 0;
			campo = "nombre";
			tabla = "tblunidad";
		}
		
		if(chkServicio.getValueAsBoolean()){
			Tipo=2;
		}else{
			Tipo=1;
		}
		
		if(chkProductoElaborado.getValueAsBoolean()){
			Jerarquia=1;
		}else{
			Jerarquia=0;
		}
		
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
				"display", "block");
		if (ban == 1) {
			getService().listarProductoLike2(nombre, tabla, 2,Tipo,Jerarquia, Integer.valueOf(tipoP),Factum.banderaStockNegativo,listaCallback);
		} else if (ban == 0) {
			getService().listarProductoJoin2(nombre, tabla, campo, 2,Tipo,Jerarquia,Integer.valueOf(tipoP),Factum.banderaStockNegativo,listaCallback);
		} else if (ban == 3) {
			getService().listarProductoJoin2(cmdBod.getDisplayValue(),"tblbodegas", "nombre", 2,Tipo,Jerarquia, Integer.valueOf(tipoP),Factum.banderaStockNegativo,listaCallback);
		}
	}
	
	final AsyncCallback<PersonaDTO>  objbackb=new AsyncCallback<PersonaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		@Override
		public void onSuccess(PersonaDTO result) {
			if(result!=null){
					codigoSeleccionado=result.getIdPersona();
					persona=result;

					txtCLiente.setValue(result.getRazonSocial());
					txtCLiente.setTooltip("Direccion: "+result.getDireccion()+" -- Telefono: "+result.getTelefono1());
					txtCLiente.setHoverWidth(350);
					txtDireccion.setValue(result.getDireccion());
					txtCorreo.setValue(result.getMail());
					txtCorreo.setTooltip("Direccion: "+result.getDireccion()+" -- Telefono: "+result.getTelefono1());
					txtCorreo.setHoverWidth(350);
					txtRuc_cliente.setValue(result.getCedulaRuc());
					txtRuc_cliente.setTooltip("Direccion: "+result.getDireccion()+" -- Telefono: "+result.getTelefono1());
					txtRuc_cliente.setHoverWidth(350);
					txtTelefono.setValue(result.getTelefono1());
					getService().ReprotePagosVencidosCliente(
							String.valueOf(codigoSeleccionado), objback);
			}else{
				SC.say("Elemento No Encontrado");
			}
			
		}
	};

	private class ManejadorBotones implements ClickHandler,
			RecordDoubleClickHandler, KeyPressHandler, FormItemClickHandler,
			ChangedHandler,
			com.smartgwt.client.widgets.events.DoubleClickHandler, com.smartgwt.client.widgets.tile.events.RecordClickHandler {
		String indicador = "";
		Double catidadU = cantidadAsignar;

		ManejadorBotones(String nombreBoton) {
			this.indicador = nombreBoton;
		}
		
		public void onClick(ClickEvent event) {
			if (indicador.equalsIgnoreCase("left")) {
				if (contador > 20) {
					contador = contador - 20;
					DOM.setStyleAttribute(RootPanel.get("cargando")
							.getElement(), "display", "block");
					getService().listaProductos(contador - 20, contador,1,0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,
							listaCallback);

				} else {
					contador = 20;
					DOM.setStyleAttribute(RootPanel.get("cargando")
							.getElement(), "display", "block");
					getService().listaProductos(contador - 20, contador,1,0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,
							listaCallback);

				}

			} else if (indicador.equalsIgnoreCase("right")) {
				if (contador < registros) {
					contador = contador + 20;
					DOM.setStyleAttribute(RootPanel.get("cargando")
							.getElement(), "display", "block");
					getService().listaProductos(contador - 20, contador,1,0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,
							listaCallback);

				}
			} else if (indicador.equalsIgnoreCase("right_all")) {
				if (registros > 20) {
					contador = registros - registros % 20;
					DOM.setStyleAttribute(RootPanel.get("cargando")
							.getElement(), "display", "block");
					getService().listaProductos(registros - registros % 20,1,0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,
							registros, listaCallback);
				}
			} else if (indicador.equalsIgnoreCase("left_all")) {
				contador = 20;
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "block");
				getService().listaProductos(contador - 20, contador,1,0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,
						listaCallback);
				//lblRegisros.setText(contador + " de " + registros);
			} else if (indicador.equalsIgnoreCase("agregar")) {
				try {
					String mensaje = "";
					int i = 0;

					// ProductoBodegaDTO[] proBod = new
					// ProductoBodegaDTO[countB];
					proBod = new ArrayList<ProductoBodegaDTO>();
					// catidadU = 0.0;
					Double cu = 0.0;
					for (i = 0; i < listGridBod.getRecords().length; i++) {
						cantidadAsignar -= listGridBod.getRecord(i)
								.getAttributeAsDouble("Cantidad");
						cantidadAsignar = CValidarDato.getDecimal(2,
								cantidadAsignar);
					}
					if (cantidadAsignar == 0) {
						i = 0;
						for (i = 0; i < listGridBod.getRecords().length; i++) {
							ProductoBodegaDTO pb = new ProductoBodegaDTO();
							pb.setCantidad(listGridBod.getRecord(i)
									.getAttributeAsDouble("Cantidad"));
							BodegaDTO bodega = new BodegaDTO();
							bodega.setIdEmpresa(Factum.empresa.getIdEmpresa());
							bodega.setEstablecimiento(Factum.getEstablecimientoCero());
							bodega.setBodega(listGridBod.getRecord(i)
									.getAttribute("Bodega"));
							bodega.setIdBodega(listGridBod.getRecord(i)
									.getAttributeAsInt("idBodega"));
							bodega.setUbicacion(listGridBod.getRecord(i)
									.getAttribute("Ubicacion"));
							bodega.setTelefono(listGridBod.getRecord(i)
									.getAttribute("Telefono"));
							pb.setTblbodega(bodega);
							pb.setTblproducto(producto);
							proBod.add(pb);
							// mibod.setValue("DESDE OTRO LADO"+pb.getTblbodega().getBodega());
						}
						proTip = null;
						listadoProd.destroy();
						// mibod.setValue("otro lado"+1);
						asignacionBodegas(1);
					} else {
						SC.say("La cantidad asignada a las bodegas no coincide con la "
								+ "cantidad ingresada en el Documento");
						cantidadAsignar = catidadU;
					}
				} catch (Exception e) {
					SC.say("Error al agregar " + e.getMessage());
				}
			}

		}

		public void onKeyPress(KeyPressEvent event) {
//			//com.google.gwt.user.client.Window.alert("indicador "+indicador+" key "+event.getKeyName());
			if (indicador.equalsIgnoreCase("persona")) {
				if (event.getKeyName().equalsIgnoreCase("Enter")){
					if (afeccion == 1 || NumDto == 14 || NumDto == 8)// NOTA DE CREDITO PROVEEDOR
					{
						if (NumDto == 3) {
	//						frmlisCli = new frmListClientes("tblclientes");
							String ced= txtRuc_cliente.getDisplayValue();
							PersonaDTO per=new PersonaDTO();
							per.setCedulaRuc(ced);
							//getService().buscarPersona(per,0, objbackb);
							buscarPers(ced,0);
						} else {
							//frmlisCli = new frmListClientes("tblproveedors");
							String ced= txtRuc_cliente.getDisplayValue();
							PersonaDTO per=new PersonaDTO();
							per.setCedulaRuc(ced);
							//getService().buscarPersona(per,2, objbackb);
							buscarPers(ced,2);
						}
					} else {
	//					frmlisCli = new frmListClientes("tblclientes");
						String ced= txtRuc_cliente.getDisplayValue();
						PersonaDTO per=new PersonaDTO();
						per.setCedulaRuc(ced);
						//getService().buscarPersona(per,0, objbackb);
						buscarPers(ced,0);
					}
				}
			}
			if (event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			if(indicador.equalsIgnoreCase("factura")){
				winDocumento = new Window();
				winDocumento.clear();
				winDocumento.setWidth(930);  
				winDocumento.setHeight(610);  
				winDocumento.setTitle("Ingresar Documento");  
				winDocumento.setShowMinimizeButton(false);  
				winDocumento.setIsModal(true);  
				winDocumento.setShowModalMask(true);  
				winDocumento.centerInPage();  
				String tipoText= (TipoDocumento==3)?"Facturas de Venta":"Facturas de Compra";
//				form=new frmReporteCaja("Facturas de Venta");
				listaDocumentos=new frmReporteCaja(tipoText);
				listaDocumentos.cmbDocumento.setVisible(false);
				winDocumento.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	winDocumento.destroy();  
	                }  
	            });
				
				listaDocumentos.lstReporteCaja.addDoubleClickHandler(new ManejadorBotones("ListadoFac"));
				listaDocumentos.setSize("100%","100%"); 
				listaDocumentos.setPadding(5);  
				listaDocumentos.setLayoutAlign(VerticalAlignment.BOTTOM);
				winDocumento.addItem(listaDocumentos);
				winDocumento.show();
			}
		 
		}
		
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			/*
			 * AQUI DEBEMOS HACER QUE LLAME A LA FUNCION DE CARGA DEL PRODUCTO
			 * EN LA FACTURA
			 */
			// CargarProducto(lstProductos.getSelectedRecord());
		}

		public void onChanged(ChangedEvent event) {
			if (indicador.equalsIgnoreCase("Bodega")) {
				boolean ban;
				if (cmbBuscarProducto.getDisplayValue().equals("Ubicaci\u00F3n")) {
					ban = true;

				} else {
					ban = false;
				}
				cmdBod.setShowHint(true);// muestra el combo de bodegas para
											// seleccionnar una
				cmdBod.setVisible(ban);

			} else if (indicador.equalsIgnoreCase("Buscar")) {
	
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display", "block");
				if(chkServicio.getValueAsBoolean()){
					getService().listarProductoJoin(cmdBod.getDisplayValue(),"tblbodegas", "nombre", 2, 0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,listaCallback);
				}else{
					getService().listarProductoJoin(cmdBod.getDisplayValue(),"tblbodegas", "nombre", 1, 0,Integer.valueOf(tipoP),Factum.banderaStockNegativo,listaCallback);
				}
				//SC.say("Buscar servicio");
			} 
//			else if (indicador.equalsIgnoreCase("persona")) {
//
//				winClientes = new Window();
//				winClientes.setWidth(930);
//				winClientes.setHeight(610);
//				winClientes.setShowMinimizeButton(false);
//				winClientes.setIsModal(true);
//				winClientes.setShowModalMask(true);
//				winClientes.setKeepInParentRect(true);
//				winClientes.centerInPage();
//				winClientes.addCloseClickHandler(new CloseClickHandler() {
//					@Override
//					public void onCloseClick(CloseClientEvent event) {
//						txtRuc_cliente.setValue(txtRuc_cliente.getDisplayValue().replace(" ",""));
//						winClientes.destroy();
//
//					}
//				});
//				VLayout form = new VLayout();
//				form.setSize("100%", "100%");
//				form.setPadding(5);
//				form.setLayoutAlign(VerticalAlignment.BOTTOM);
//				if (afeccion == 1 || NumDto == 14 || NumDto == 8)// NOTA DE CREDITO PROVEEDOR
//				{
//					if (NumDto == 3) {
//						frmlisCli = new frmListClientes("tblclientes");
//						frmlisCli.lstCliente
//								.addDoubleClickHandler(new ManejadorBotones(
//										"cliente"));
//						frmlisCli.setSize("100%", "100%");
//						// frmlisCli.txtBuscarLst.focusInItem();
//						winClientes.setTitle("Buscar Cliente");
//						form.addMember(frmlisCli);
//					} else {
//						frmlisCli = new frmListClientes("tblproveedors");
//						frmlisCli.lstCliente
//								.addDoubleClickHandler(new ManejadorBotones(
//										"proveedor"));
//						frmlisCli.setSize("100%", "100%");
//						winClientes.setTitle("Buscar Proveedor");
//						form.addMember(frmlisCli);
//					}
//				} else {
//					frmlisCli = new frmListClientes("tblclientes");
//					frmlisCli.lstCliente
//							.addDoubleClickHandler(new ManejadorBotones(
//									"cliente"));
//					frmlisCli.setSize("100%", "100%");
//					winClientes.setTitle("Buscar Cliente");
//					// frmlisCli.txtBuscarLst.focusInItem();
//					form.addMember(frmlisCli);
//				}
//				frmlisCli.txtBuscarLst.setSelectOnFocus(true);
//				winClientes.addItem(form);
//				winClientes.show();
//			}
		}

		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			// Funcion que maneja el click de los PICKER
			Integer doc = 0;
			if (indicador.equalsIgnoreCase("nuevocliente")) {
				if (Factum.banderaClienteFactura==1){
					winFactura1 = new Window();
					winFactura1.setWidth(1250);
					winFactura1.setHeight(650);
					winFactura1.setTitle("Ingreso de Persona");
					winFactura1.setShowMinimizeButton(false);
					winFactura1.setIsModal(true);
					winFactura1.setShowModalMask(true);
					winFactura1.setKeepInParentRect(true);
					winFactura1.centerInPage();
					winFactura1.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClientEvent event) {
							winFactura1.destroy();
						}
					});
					if ((afeccion == 0 && NumDto!=8 && NumDto!=14) || (afeccion == 1 && NumDto==3)) {
						frmCliente cliente = new frmCliente(fact);
						cliente.setSize("100%", "95%");
						// cliente.tabSet.removeTab(0);
						// cliente.lstCliente.addDoubleClickHandler(new
						// ManejadorBotones("ingresoCliente"));
						winFactura1.addItem(cliente);
					}else if(afeccion == 0 && NumDto==8){
						//SC.say("DATOS:"+ afeccion +" "+NumDto);
						frmProveedor proveedor = new frmProveedor(fact);
						proveedor.setSize("100%", "95%");
						// proveedor.tabSet.removeTab(1);
						// proveedor.lstProveedor.addDoubleClickHandler(new
						// ManejadorBotones("ingresoProveedor"));
						winFactura1.addItem(proveedor);
					
					}else {
						frmProveedor proveedor = new frmProveedor(fact);
						proveedor.setSize("100%", "95%");
						// proveedor.tabSet.removeTab(1);
						// proveedor.lstProveedor.addDoubleClickHandler(new
						// ManejadorBotones("ingresoProveedor"));
						winFactura1.addItem(proveedor);
	
					}
					winFactura1.show();
				}else{
					String url = GWT.getHostPageBaseURL() ;
					String direccion = "http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul;
					//SC.say(url);
					if ((afeccion == 0 && NumDto!=8 && NumDto!=14) || (afeccion == 1 && NumDto==3)) {
						com.google.gwt.user.client.Window.open(direccion+"/crud/newCliente", "Crear cliente", null);
					}else if(afeccion == 0 && NumDto==8){
						com.google.gwt.user.client.Window.open(direccion+"/crud/newProveedor", "Crear proveedor", null);
					}else {
						com.google.gwt.user.client.Window.open(direccion+"/crud/newProveedor", "Crear proveedor", null);
					}
				    //com.google.gwt.user.client.Window.open(+"/crud/newCliente", "Crear cliente", null);
				}

			}else if (indicador.equals("buscarCliente")) {
				winClientes = new Window();
				winClientes.setWidth(930);
				winClientes.setHeight(610);
				winClientes.setShowMinimizeButton(false);
				winClientes.setIsModal(true);
				winClientes.setShowModalMask(true);
				winClientes.setKeepInParentRect(true);
				winClientes.centerInPage();
				winClientes.addCloseClickHandler(new CloseClickHandler() {
					@Override
					public void onCloseClick(CloseClientEvent event) {
						txtRuc_cliente.setValue(txtRuc_cliente.getDisplayValue().replace(" ",""));
						winClientes.destroy();

					}
				});
				VLayout form = new VLayout();
				form.setSize("100%", "100%");
				form.setPadding(5);
				form.setLayoutAlign(VerticalAlignment.BOTTOM);
				if (afeccion == 1 || NumDto == 14 || NumDto == 8)// NOTA DE CREDITO PROVEEDOR
				{
					if (NumDto == 3) {
						frmlisCli = new frmListClientes("tblclientes");
						frmlisCli.lstCliente
								.addDoubleClickHandler(new ManejadorBotones(
										"cliente"));
						frmlisCli.setSize("100%", "100%");
						// frmlisCli.txtBuscarLst.focusInItem();
						winClientes.setTitle("Buscar Cliente");
						form.addMember(frmlisCli);
					} else {
						frmlisCli = new frmListClientes("tblproveedors");
						frmlisCli.lstCliente
								.addDoubleClickHandler(new ManejadorBotones(
										"proveedor"));
						frmlisCli.setSize("100%", "100%");
						winClientes.setTitle("Buscar Proveedor");
						form.addMember(frmlisCli);
					}
				} else {
					frmlisCli = new frmListClientes("tblclientes");
					frmlisCli.lstCliente
							.addDoubleClickHandler(new ManejadorBotones(
									"cliente"));
					frmlisCli.setSize("100%", "100%");
					winClientes.setTitle("Buscar Cliente");
					// frmlisCli.txtBuscarLst.focusInItem();
					form.addMember(frmlisCli);
				}
				frmlisCli.txtBuscarLst.setSelectOnFocus(true);
				winClientes.addItem(form);
				winClientes.show();
			}
			else if (indicador.equals("pckr1")) {
				//mostrarDocumento(5);
				agregarRetencion();
			} else if (indicador.equals("pckr2")) {
				doc = 0;
				try {
					doc = Integer.parseInt((dynamicForm2
							.getItem("txtRetencion").getDisplayValue()));
					winFactura1 = new Window();
					winFactura1.setWidth(1100);
					winFactura1.setHeight(700);
					winFactura1.setTitle("Documento Comercial");
					winFactura1.setShowMinimizeButton(false);
					winFactura1.setIsModal(true);
					winFactura1.setShowModalMask(true);
					winFactura1.setKeepInParentRect(true);
					winFactura1.centerInPage();
					// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					Factura fac = new Factura(5, doc);// lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
					winFactura1.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClientEvent event) {
							winFactura1.destroy();
						}
					});
					VLayout form = new VLayout();
					form.setSize("100%", "100%");
					form.setPadding(5);
					form.setLayoutAlign(VerticalAlignment.BOTTOM);
					form.addMember(fac);
					winFactura1.addItem(form);
					winFactura1.show();
				} catch (Exception e) {
					SC.say("Primero debe seleccionar un documento");
				}
			} else if (indicador.equals("pckr3")) {
				if(NumDto==1){	
					if(txtNumCompra.validate()){
						agregarRetencion();
					}
				}else{
					agregarRetencion();
				}
			} else if (indicador.equals("pcka1")) {
				// mostrarDocumento(4);
				// solo estaba la linea de arriba no el if else de abajo
				if (NumDto == 1 || NumDto == 10 || NumDto == 27)// si es factura de compra se le
												// hace con anticipo a
												// proveedores
				{
					mostrarDocumento(15);
				} else {
					mostrarDocumento(4);
				}

			} else if (indicador.equals("pcka2")) {
				doc = 0;
				try {
					doc = Integer.parseInt((dynamicForm2.getItem("txtAnticipo").getDisplayValue()));
					winFactura1 = new Window();
					winFactura1.setWidth(1100);
					winFactura1.setHeight(700);
					winFactura1.setTitle("Anticipos");
					winFactura1.setShowMinimizeButton(false);
					winFactura1.setIsModal(true);
					winFactura1.setShowModalMask(true);
					winFactura1.setKeepInParentRect(true);
					winFactura1.centerInPage();
					// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					Factura fac = new Factura(4, doc);// lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
					winFactura1.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClientEvent event) {
							winFactura1.destroy();
						}
					});
					VLayout form = new VLayout();
					form.setSize("100%", "100%");
					form.setPadding(5);
					form.setLayoutAlign(VerticalAlignment.BOTTOM);
					form.addMember(fac);
					winFactura1.addItem(form);
					winFactura1.show();
				} catch (Exception e) {
					SC.say("Primero debe seleccionar un documento");
				}
			} else if (indicador.equals("pckn1")) {
				mostrarDocumento(3);
			} else if (indicador.equals("pckn2")) {
				doc = 0;
				try {
					doc = Integer.parseInt((dynamicForm2
							.getItem("txtNotaCredito").getDisplayValue()));
					winFactura1 = new Window();
					winFactura1.setWidth(1100);
					winFactura1.setHeight(700);
					winFactura1.setTitle("Nota de Credito");
					winFactura1.setShowMinimizeButton(false);
					winFactura1.setIsModal(true);
					winFactura1.setShowModalMask(true);
					winFactura1.setKeepInParentRect(true);
					winFactura1.centerInPage();
					// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					Factura fac = new Factura(5, doc);// lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
					winFactura1.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClientEvent event) {
							winFactura1.destroy();
						}
					});
					VLayout form = new VLayout();
					form.setSize("100%", "100%");
					form.setPadding(5);
					form.setLayoutAlign(VerticalAlignment.BOTTOM);
					form.addMember(fac);
					winFactura1.addItem(form);
					winFactura1.show();
				} catch (Exception e) {
					SC.say("Primero debe seleccionar un documento");
				}
			}else if(indicador.equals("Buscar")){
				buscarL();
			}
		}

		@Override
		public void onDoubleClick(
				com.smartgwt.client.widgets.events.DoubleClickEvent event) {
			// TODO Auto-generated method stub
				if(indicador.equals("ListadoFac")){
					int idFactura=listaDocumentos.lstReporteCaja.getSelectedRecord().getAttributeAsInt("id");
					getService().getDtoID(idFactura, callbackDocAuxiliar);
				}

			if (indicador.equals("cliente")) {
				codigoSeleccionado = Integer.parseInt(frmlisCli.lstCliente
						.getSelectedRecord().getAttribute("idPersona")
						.toString());
				CargarCliente();
				//SC.say("ESTO SE HIZO");

			} else {
				if (indicador.equals("asignar")) {
					//SC.say("SE ASIGNO!");
					// codigoSeleccionado =
					// Integer.parseInt(frmlisCli.lstCliente.getSelectedRecord().getAttribute("idPersona").toString());
					// CargarPuntos();
					txtPersonaAsignada.setValue(
//							frmlisCli.lstCliente.getSelectedRecord().getAttribute("NombreComercial")
//							+ " "+ 
							frmlisCli.lstCliente.getSelectedRecord().getAttribute("RazonSocial")
							);
					// txtPuntosAsignados.setValue("0");
					codigoSeleccionadoPuntos = Integer
							.parseInt(frmlisCli.lstCliente.getSelectedRecord()
									.getAttribute("idPersona").toString());
					puntosAcreditados = Integer.valueOf(txtPuntosAsignados
							.getValue().toString());
					// SC.say("Hay q asignarle puntos");

				} else {
					codigoSeleccionado = Integer.parseInt(frmlisCli.lstCliente
							.getSelectedRecord().getAttribute("idPersona")
							.toString());
					CargarCliente();
				}
			}

		}

		@Override
		public void onRecordClick(
				com.smartgwt.client.widgets.tile.events.RecordClickEvent event) {
			if(indicador.equals("mesa")){
				//SC.say(event.getRecord().getAttribute("idMesa"));
				idMesa=event.getRecord().getAttributeAsInt("idMesa");
				listadoMes.destroy();
				mostrar_mesa_productos();
			}// TODO Auto-generated method stub
			
		}

		

		
	}
	
	final AsyncCallback<DtocomercialDTO>  callbackDocAuxiliar=new AsyncCallback<DtocomercialDTO>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(DtocomercialDTO result) {
			
			docAuxiliar=result;
			persona=result.getTblpersonaByIdPersona();
			codigoSeleccionado = result.getTblpersonaByIdPersona().getIdPersona();
			txtCLiente.setValue(result.getTblpersonaByIdPersona().getRazonSocial());
			txtDireccion.setValue(result.getTblpersonaByIdPersona().getDireccion());
			txtCorreo.setValue(result.getTblpersonaByIdPersona().getMail());
			txtRuc_cliente.setValue(result.getTblpersonaByIdPersona().getCedulaRuc());
			txtTelefono.setValue(result.getTblpersonaByIdPersona().getTelefono1());
//			codigoSeleccionado = Integer.valueOf(listaDocumentos.lstReporteCaja.getSelectedRecord().getAttributeAsString("idPersona"));
//			txtCLiente.setValue(listaDocumentos.lstReporteCaja.getSelectedRecord().getAttribute("Cliente"));
//			txtDireccion.setValue(listaDocumentos.lstReporteCaja.getSelectedRecord().getAttribute("direccion"));
//			txtRuc_cliente.setValue(listaDocumentos.lstReporteCaja.getSelectedRecord().getAttribute("RUC"));
//			txtTelefono.setValue(frmlisCli.lstCliente.getSelectedRecord()	.getAttribute("Telefonos"));
//			//com.google.gwt.user.client.Window.alert();
			String numreal=String.valueOf(result.getNumRealTransaccion());
			while (numreal.length()<9) numreal="0"+numreal;
			String numCorresp=(TipoDocumento==3)?emision.replace("-","")+numreal:result.getNumCompra().replace("-","");
			txtCorresp.setValue(numCorresp);

				winDocumento.destroy();
		}
	};

	/*
	 * INICIO DE FUNCIONES DE RETENCIONES
	 */
	public void formRetencion(int NumDoc) {
 		
//		//com.google.gwt.user.client.Window.alert("ret1 "+NumDoc);
		 		VLayout layoutCabecera = new VLayout();
				layoutCabecera.setSize("100%", "30%");
				frmCabecera = new DynamicForm();
				frmCabecera.setSize("100%", "100%");
				frmCabecera.setNumCols(4);
				TextItem  txtNumero=new TextItem("txtNumero", "N\u00FAmero Retenci&oacute;n");
				//txtNumero.setDisabled(false);
				if(Factum.banderaModificarNumeroRetencion == 0){
					txtNumero.setDisabled(true);	
				}else{
					txtNumero.setDisabled(false);
				}
				
				txtNumero.setColSpan(2);
				txtNumero.setValue("");
				txtNumero.setRequired(true);
//				txtNumero.setMask("###-###-#########");
				txtNumero.setMask("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
				txtNumero.setMaskPromptChar("0");
				txtNumero.setMaskPadChar("0");
				txtNumero.addChangedHandler(new com.smartgwt.client.widgets.form.fields.events.ChangedHandler(){

					@Override
					public void onChanged(com.smartgwt.client.widgets.form.fields.events.ChangedEvent event) {
						// TODO Auto-generated method stub
						String val=(String)event.getValue();
						while (val.length()<15){
							val+="0";
						}
						event.getItem().setValue(val);
					}
					
				});
				txtNumero.setMaskOverwriteMode(true);
//				txtNumero.setKeyPressFilter("[0-9]");	
//				//com.google.gwt.user.client.Window.alert("ret2");
				DateTimeItem txtFechaEmision=new DateTimeItem("txtFechaEmision", "Fecha de Emisi\u00F3n");
				txtFechaEmision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
				txtFechaEmision.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
				txtFechaEmision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
				txtFechaEmision.setRequired(true);
				//setUseTextField(true);
				//txtFechaEmision.setValue(new Date());
				TextItem txtRuc = new TextItem("txtRuc", "R.U.C");
				txtRuc.setDisabled(true);
				txtRuc.setKeyPressFilter("[0-9]");
				txtRuc.setLength(13);
				//txtRuc.setValue(Fact.getTblpersonaByIdPersona().getCedulaRuc());
				TextItem txtPersona = new TextItem("txtPersona", "Se\u00F1ore(s)");
				txtPersona.setDisabled(true);
				TextItem txtTipoComprobante = new TextItem("txtTipoComprobante", "Tipo de Comprobante");
				txtTipoComprobante.setDisabled(true);
				 switch (NumDoc) {
				 case 0:// Factura de venta
				 txtTipoComprobante.setValue("FACTURA DE VENTA");
				 
				 break;
				 case 1:// Factura de compra
				 txtTipoComprobante.setValue("FACTURA DE COMPRA");
				 txtNumero.setValue(emision);
				 break;
				 case 2:// Nota de entrega
				 txtTipoComprobante.setValue("NOTA DE ENTREGA");
				 
				 break;
				 case 3:// Nota de Credito
				 txtTipoComprobante.setValue("NOTA DE CREDITO");
				 txtCorresp.setRequired(true);
				 txtCorresp.setVisible(true);
				 
				 txtMotivo.setRequired(true);
				 txtMotivo.setVisible(true);
				 break;
				 case 4:// Anticipo de cliente
				 txtTipoComprobante.setValue("ANTICIPO");

				 break;
				 case 5:// Retencion
				 txtTipoComprobante.setValue("RETENCION");

				 break;
				
				 case 7:// Gasto
				 txtTipoComprobante.setValue("GASTO");
				 break;
				 case 8:// Ajuste de Inventario
					 txtTipoComprobante.setValue("AJUSTE DE INVENTARIO");
				 break;
				
				 case 10:
				 txtTipoComprobante.setValue("NOTA DE COMPRA");
				 
				 break;
				 case 12:// Ingreso
				 txtTipoComprobante.setValue("INGRESO");

				 break;
				 case 13:// Factura de venta Grande
				 txtTipoComprobante.setValue("FACTURA DE VENTA GRANDE");
				 break;
				 case 14:// Nota de Credito
				 txtTipoComprobante.setValue("NOTA DE CREDITO PROVEEDORES");
				 txtCorresp.setRequired(true);
				 txtCorresp.setVisible(true);
				 
				 txtMotivo.setRequired(true);
				 txtMotivo.setVisible(true);
				 break;
				}
				TextItem txtAutorizacion= new TextItem("txtAutorizacion", "N\u00B0 Autorizaci\u00F3n");
				//+++++++ PARA PROBAR CUAL TEXT ITEM FALTA PARA PDOER CARGAR A CADA RATO LA RETENCION
				txtNumeroC=new TextItem("txtNumeroC", "# Documento");
				txtDireccion2=new TextItem("txtDireccion", "Direcci\u00F3n");//para la retencion
			//	final TextItem txtNumCompra=new TextItem("txtNumCompra", "#Factura de Compra");
			//	TextItem txtNumero_factura = new TextItem("numero_factura","N.");
//				//com.google.gwt.user.client.Window.alert("ret3");
				//txtAutorizacion.setValue("1112151582");1113015098
				if(NumDto==1)
				{
					txtAutorizacion.setValue("1113015098");
				}
				else
				{
					txtAutorizacion.setValue("");
				}
				//TextItem txtDireccion2=new TextItem("txtDireccion", "Direcci\u00F3n");
				
//				//com.google.gwt.user.client.Window.alert("ret4");
				try{txtNumeroC.setValue(txtNumCompra.getDisplayValue());}
				catch(Exception e){txtNumeroC.setValue("");}
				if(NumDto==0)
				{
					try{txtNumeroC.setValue(txtNumero_factura.getDisplayValue());}
					catch(Exception e){txtNumeroC.setValue("");}
				}
//				//com.google.gwt.user.client.Window.alert("entrando a la retencion v3");
				txtAutorizacion.setLength(60);
				txtNumeroC.setRequired(true);
//				//com.google.gwt.user.client.Window.alert("ret5");
				if(NumDto==0){
//					//com.google.gwt.user.client.Window.alert("ret5.1");
					txtDireccion2.setValue(Factum.banderaDireccionEmpresa);
//					//com.google.gwt.user.client.Window.alert("ret5.2");
					txtPersona.setValue(Factum.banderaNombreEmpresa);
//					//com.google.gwt.user.client.Window.alert("ret5.3");
					txtRuc.setValue(Factum.banderaRUCEmpresa);
//					//com.google.gwt.user.client.Window.alert("ret5.4");
				}
				else
				{				
					try
					{
						txtDireccion2.setValue(DocBase.getTblpersonaByIdPersona().getDireccion());
						//txtPersona.setValue(DocBase.getTblpersonaByIdPersona().getNombreComercial());
						txtPersona.setValue(DocBase.getTblpersonaByIdPersona().getRazonSocial());
						txtRuc.setValue(String.valueOf(DocBase.getTblpersonaByIdPersona().getCedulaRuc()));
					}
					catch(Exception e){
						txtDireccion2.setValue(txtDireccion.getDisplayValue().toString());
						txtPersona.setValue(txtCLiente.getDisplayValue().toString());
						txtRuc.setValue(txtRuc_cliente.getDisplayValue().toString());
						}
				}
//				//com.google.gwt.user.client.Window.alert("ret6");
				////com.google.gwt.user.client.Window.alert("entrando a la retencion v4");
				frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtFechaEmision, txtRuc, txtTipoComprobante, 
						txtAutorizacion, txtDireccion2, txtNumeroC});
				layoutCabecera.addMember(frmCabecera);
				formRetencion.addMember(layoutCabecera);
				
				VLayout layoutDetalle = new VLayout();
				layoutDetalle.setSize("100%", "65%");
//				//com.google.gwt.user.client.Window.alert("ret7");
				
				listGrid.setCanEdit(true);
				listGrid.setCanSelectAll(true);
				
//				//com.google.gwt.user.client.Window.alert("entrando a la retencion v5");
				//baseImponible.setValueMap("Iva","Sub Total");
				baseImponible.addChangeHandler(new ManejadorBotonesRetencion("base"));
//				baseImponible.addEditorExitHandler(new ManejadorBotonesRetencion("base"));
				baseImponible.setCanEdit(false);
				
//				//com.google.gwt.user.client.Window.alert("ret8");
				//listGrid.setFields(new ListGridField[] { impuesto,baseImponible, new ListGridField("lstBaseImponible", "Base Valor"),  new ListGridField("lstCodigo", "C\u00F3digo"), lstPorcentaje, new ListGridField("lstValor", "Valor Retenido")});
				impuesto.setAutoFetchDisplayMap(true);
				
				ListGridField lstcodigoRet=new ListGridField("lstCodigo", "C\u00F3digo");
				ListGridField lstvalorRet=new ListGridField("lstValor", "Valor Retenido");
				
				baseImponible.setCanEdit(false);
//				lstBaseImponible.setCanEdit(false);
				lstcodigoRet.setCanEdit(false);
				lstPorcentaje.setCanEdit(false);
				lstvalorRet.setCanEdit(false);
				
				listGrid.setFields(new ListGridField[] { impuesto,baseImponible,lstBaseImponible ,  lstcodigoRet, lstPorcentaje, lstvalorRet});
				
//				listGrid.setFields(new ListGridField[] { impuesto,baseImponible,lstBaseImponible,  new ListGridField("lstCodigo", "C\u00F3digo"), lstPorcentaje, new ListGridField("lstValor", "Valor Retenido")});
				layoutDetalle.addMember(listGrid);
				impuesto.addChangeHandler(new ManejadorBotonesRetencion("impuesto"));
				impuesto.addChangedHandler(new ManejadorBotonesRetencion("impuesto"));
				impuesto.addEditorExitHandler(new ManejadorBotonesRetencion("impuesto"));
				lstPorcentaje.addChangeHandler(new ManejadorBotonesRetencion("porcentaje"));
				lstPorcentaje.addEditorExitHandler(new ManejadorBotonesRetencion("porcentaje"));
				lstBaseImponible.addChangeHandler(new ManejadorBotonesRetencion("valor"));
		        lstBaseImponible.addEditorExitHandler(new ManejadorBotonesRetencion("valor"));
				HStack hStack_1 = new HStack();
		        hStack_1.setSize("100%", "8%");
//		        //com.google.gwt.user.client.Window.alert("entrando a la retencion v6");
		        IButton btnAdd = new IButton("Agregar Imp.");
		        hStack_1.addMember(btnAdd);
//		        //com.google.gwt.user.client.Window.alert("ret9");
		        IButton btnNewIbutton = new IButton("Borrar Imp.");
		        btnNewIbutton.addClickHandler(new ManejadorBotonesRetencion("EImpuesto"));
		        hStack_1.addMember(btnNewIbutton); 
		        btnAdd.addClickHandler(new ClickHandler() {  
		            public void onClick(ClickEvent event) {  
		            	//listGrid.startEditingNew();  
		            	listGrid.addData(new ListGridRecord());
		            	listGrid.selectRecord(listGrid.getRecords().length);
		            	listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", "Seleccione un Impuesto");
//		            	//com.google.gwt.user.client.Window.alert("Seleccione un Impuesto");
		            }  
		        });
		        DynamicForm dynamicForm_1 = new DynamicForm();
		        dynamicForm_1.setItemLayout(FormLayoutType.TABLE);
		        txtTotalRet=new FloatItem();
		        txtTotalRet.setLeft(300);
		        txtTotalRet.setTop(0);
//		        //com.google.gwt.user.client.Window.alert("ret10");
		        txtTotalRet.setTextAlign(Alignment.LEFT);
		        txtTotalRet.setTitle("TOTAL");
		        dynamicForm_1.setFields(new FormItem[] { txtTotalRet});
		        layoutDetalle.addMember(dynamicForm_1);
		        layoutDetalle.addMember(hStack_1);
		        formRetencion.addMember(layoutDetalle);
				
				HStack hStack = new HStack();
				hStack.setSize("100%", "5%");
				
				IButton btnGuardarRet = new IButton("Guardar");
				hStack.addMember(btnGuardarRet);
				btnGuardarRet.addClickHandler(new ManejadorBotonesRetencion("grabar"));
			
				IButton btnLimpiar = new IButton("Limpiar");
				hStack.addMember(btnLimpiar);
				
				IButton btnImprimir = new IButton("Imprimir");
				hStack.addMember(btnImprimir);
				btnImprimir.addClickHandler(new ManejadorBotonesRetencion("imprimir"));
//				//com.google.gwt.user.client.Window.alert("ret11");
				formRetencion.addMember(hStack);
				if(TipoDocumento!=20) getService().listarImpuesto(TipoDocumento+1, objbacklstRet);
				else getService().listarImpuesto(NumDoc+1, objbacklstRet);
				Double porcentaje = 0.0;
				if(!tblRetenciones.isEmpty()){
					RetencionDTO retencion=new RetencionDTO();
					try{
						retencion=new RetencionDTO();
						Iterator resultado = tblRetenciones.iterator();
						double total=0;
						while(resultado.hasNext()){
							retencion=(RetencionDTO)resultado.next();
							porcentaje = (retencion.getValorRetenido()*100)/retencion.getBaseImponible();
							porcentaje=CValidarDato.getDecimal(Factum.banderaNumeroDecimales,porcentaje);
//							//com.google.gwt.user.client.Window.alert("VALOR RETENIDO: "+retencion.getValorRetenido());
							
							ListGridRecord recordret=new ListGridRecord();
							
							//recordret.setAttribute("lstImpuesto", retencion.getImpuesto().getNombre());
//							//com.google.gwt.user.client.Window.alert("antes impuesto");

							recordret.setAttribute("lstImpuesto", String.valueOf(retencion.getImpuesto().getIdImpuesto()));

							//							//com.google.gwt.user.client.Window.alert("despues impuesto");
							//recordret.setAttribute("lstImpuesto", new String[]{String.valueOf(retencion.getImpuesto().getIdImpuesto()),retencion.getImpuesto().getNombre()});
//							impuestoReten=MapImpuesto.get(retencion.getImpuesto().getIdImpuesto());
//							(String.valueOf(result.get(i).getIdImpuesto()),
//									result.get(i).getNombre());
							String baseImp=(retencion.getImpuesto().getTipo()==1)?"Sub Total":"Iva";
							recordret.setAttribute("baseImponible",baseImp);
							recordret.setAttribute("lstPorcentaje",retencion.getImpuesto().getRetencion());
//							recordret.setAttribute("lstPorcentaje",porcentaje);
							recordret.setAttribute("lstBaseImponible",retencion.getBaseImponible());
							recordret.setAttribute("lstCodigo",retencion.getImpuesto().getCodigo());
							recordret.setAttribute("lstValor",retencion.getValorRetenido());
							
							
							//recordret.fireEvent(new ChangeEvent(recordret.getJsObj()));
							total=total+retencion.getValorRetenido();
							txtAutorizacion.setValue(retencion.getAutorizacionSri());
							txtNumeroC.setValue(retencion.getNumero());
							txtNumero.setValue(retencion.getNumRealRetencion());
//							//com.google.gwt.user.client.Window.alert("Despues valores");
							
							listGrid.addData(recordret);
							listGrid.startEditing(listGrid.getRecords().length-1, 0, false);
							listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", String.valueOf(retencion.getImpuesto().getIdImpuesto()));
							
							
							listGrid.endEditing();
							listGrid.saveAllEdits();
							listGrid.fetchData();
							
							//listGrid.endEditing();listGrid.saveAllEdits();

							listGrid.focus();
							
			            	//listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", String.valueOf(retencion.getImpuesto().getIdImpuesto()));
			            	
			            	
//			            	listGrid.addData(new ListGridRecord());
//			            	listGrid.selectRecord(listGrid.getRecords().length-1);
//			            	listGrid.removeData(listGrid.getSelectedRecord());
//			            	listGrid.selectRecord(listGrid.getRecords().length-1);
			            	
							//listGrid.fireEvent(new ChangeEvent(listGrid.getRecord(listGrid.getTotalRows()-1).getAttributeAsJavaScriptObject("lstImpuesto")));
//							listGrid.startEditing();
//							listGrid.endEditing();
//							listGrid.saveAllEdits();
						}
//						String fechaBase[] = retencionDoc.getFecha().split(" ");
//						String orden[] = fechaBase[0].split("-");
//						String fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
						if (docAuxiliar!=null) txtFechaEmision.setValue(docAuxiliar.getFecha());
						txtTotalRet.setValue(total);
//						//com.google.gwt.user.client.Window.alert("ret12");
					}catch(Exception e){
						SC.say("Error al cargar retencion "+e.getMessage());
					}
					
				}
											
				
		 		
		 	}
	
	
	private class ManejadorBotonesServicio implements ChangedHandler{
		String indicador = "";


		ManejadorBotonesServicio(String nombreBoton) {
			this.indicador = nombreBoton;
		}


		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("Servicio")){
				if(chkServicio.getValueAsBoolean()){
					cmbBuscarProducto.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n");		
					cmbBuscarProducto.redraw();
				}else{
					cmbBuscarProducto.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n",
							"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
					cmbBuscarProducto.redraw();
				}
				if(chkProductoElaborado.getValueAsBoolean()){
					chkProductoElaborado.setValue(false);
				}

			}else if(indicador.equals("Precio")){
				tipoP=cmbTipoPrecio.getValue().toString();
				lstPrecio.setTitle("Precio "+cmbTipoPrecio.getDisplayValue());
				actualizarPrecios();
			}
			
		}


		private void actualizarPrecios(){
			
			final AsyncCallback<ProductoDTO> callback = new AsyncCallback<ProductoDTO>() {
				
				public void onFailure(Throwable caught) {
					SC.say(caught.toString());
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
							"display", "none");

				}

				public void onSuccess(ProductoDTO result) {
					 //List<TipoPrecioDTO> tipoPrec= productodto.getTbltipoprecio();
					boolean exist=false;
					
					 for (ProductoTipoPrecioDTO tipoPrec:result.getProTip()){
						 if (tipoPrec.getTbltipoprecio().getIdTipoPrecio().equals(Integer.valueOf(tipoP))){
							 List<ProductoTipoPrecioDTO> pro=new ArrayList<ProductoTipoPrecioDTO>();
							 pro.add(tipoPrec);
							 result.setProTip(pro);
							 
							 ProductoRecords prod=new ProductoRecords(result);
							 
							 CargarProducto(prod);
							 /*SC.say(String.valueOf(result.getProTip().size())+", "+
									 String.valueOf(result.getProTip().get(0).getPorcentaje())+", "+
									 prod.getAttribute("Precio"));*/
							 calculo_Individual_Total();
							 exist=true;
						 }
					 }
					 if (!exist){
						 int num=0;
						 for (ListGridRecord record:grdProductos.getRecords()){
							 if (result.getIdProducto().equals(record.getAttributeAsInt("idProducto"))){
								 grdProductos.removeData(record);
							 }
							 num++;
						 }
					 }
				}
			};
			ListGridRecord[] listado=grdProductos.getRecords();
			 for (ListGridRecord record:listado){
				 getService().buscarProducto(String.valueOf(record.getAttribute("idProducto")), "idProducto", callback);
				 
			 }
			 
		}
	}
	
	private class ManejadorBotonesProductoElaborado implements ChangedHandler{
		String indicador = "";


		ManejadorBotonesProductoElaborado(String nombreBoton) {
			this.indicador = nombreBoton;
		}


		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("ProductoElaborado")){
				if(chkServicio.getValueAsBoolean()){
					cmbBuscarProducto.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n",
							"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
					cmbBuscarProducto.redraw();
				}
				if(chkServicio.getValueAsBoolean()){
					chkServicio.setValue(false);
				}
			}
		}


		
	}
	

	private class ManejadorBotonesRetencion implements ChangeHandler,com.smartgwt.client.widgets.grid.events.ChangedHandler,
			ClickHandler, EditorExitHandler {
		String indicador = "";
		String Nombre = "";
		String Direccion = "";
		String Ruc = "";
		String Autorizacion = "";
		String fecha = "";
		String tipo = "";

		ManejadorBotonesRetencion(String nombreBoton) {
			this.indicador = nombreBoton;
		}

		public void datosfrm() {
			try {
				Nombre = frmCabecera.getItem("txtPersona").getDisplayValue();
				//SC.say(Nombre);
				Ruc = frmCabecera.getItem("txtRuc").getDisplayValue();
				//SC.say(Ruc);
				Autorizacion = frmCabecera.getItem("txtAutorizacion")
						.getDisplayValue();
				//SC.say(Autorizacion);
				Direccion = (String) frmCabecera.getItem("txtDireccion")
						.getDisplayValue();
				//SC.say(Direccion);
				fecha = frmCabecera.getItem("txtFechaEmision").getAttribute(
						"txtFechaEmision");
				//SC.say(String.valueOf(fecha));
				tipo = frmCabecera.getItem("txtTipoComprobante")
						.getDisplayValue();
				//SC.say(tipo);
				SC.say(Nombre + " " + Ruc + " " + Autorizacion + " "
						+ Direccion + "  " + tipo);

			} catch (Exception e) {
				SC.say(e.getMessage());
			}

		}

		public void Total() {
			int j = listGrid.getRecords().length;
			try {
				Double total = 0.0;
				for (int i = 0; i < j; i++) {
					total = total
							+ listGrid.getRecord(i).getAttributeAsDouble(
									"lstValor");
				}
				total = Math.rint(total * 100) / 100;
//				total=(CValidarDato.getDecimal(2, total)!=0.00)?CValidarDato.getDecimal(2, total):total;
				total=CValidarDato.getDecimal(2, total);
				txtTotalRet.setValue(total);
//				dynamicForm2.getItem("txtRetencion").focusInItem();
				dynamicForm2.getItem("txtRetencion").setValue(total);
				// SC.say("calculando total="+total+" "+ j);
			} catch (Exception e) {
				total = Math.rint(total * 100) / 100;
				txtTotalRet.setValue(total);
//				dynamicForm2.getItem("txtRetencion").focusInItem();
				dynamicForm2.getItem("txtRetencion").setValue(total);
			}
		}

		@Override
		public void onClick(ClickEvent event) {
			if (indicador.equalsIgnoreCase("grabar")) {
				if (listGrid.getRecords().length > 0) {
					Double totalRetenido = 0.0;
					if (frmCabecera.validate()) {
						try {
//							com.google.gwt.user.client.Window.alert("Va  agrabar retencion");
							String imp = "";
							String p = "";
							det = new LinkedList<RetencionDTO>();
							tblRetenciones = new HashSet<RetencionDTO>(0);
//							com.google.gwt.user.client.Window.alert(String.valueOf(listGrid.getRecords().length)+"DocBase "+DocBase.getIdDtoComercial());
//							com.google.gwt.user.client.Window.alert(String.valueOf(!docAuxiliar.getIdDtoComercial().equals(null))
//									+" tiene id "+(docAuxiliar.getIdDtoComercial()==null));
							
							for (int i = 0; i < listGrid.getRecords().length; i++) {
								imp = listGrid.getRecord(i).getAttribute(
										"lstImpuesto");
								RetencionDTO ret = new RetencionDTO();
								ret.setAutorizacionSri((String) frmCabecera
										.getItem("txtAutorizacion").getValue());// DocBase.getAutorizacion());

								ret.setBaseImponible(listGrid.getRecord(i)
										.getAttributeAsDouble(
												"lstBaseImponible"));
//								ret.setdtocomercial(DocBase);
								ret.setTbldtocomercialByIdFactura(DocBase);
								ret.setTbldtocomercialByIdDtoComercial(null);
								ret.setEstado('1');
								ret.setNumRealRetencion((frmCabecera.getItem(
												"txtNumero").getDisplayValue()));
								ret.setNumero(frmCabecera.getItem("txtNumeroC")
										.getDisplayValue());
								p = p
										+ " "
										+ String.valueOf(ret.getBaseImponible())
										+ " ";
								//SC.say(imp);
								for (int j = 0; j < Impuestos.size(); j++) {
									 //com.google.gwt.user.client.Window.alert(imp+" "+String.valueOf(Impuestos.get(j).getIdImpuesto()));
									if (imp.equals(String.valueOf(Impuestos
											.get(j).getIdImpuesto()))) {
										ret.setImpuesto(Impuestos.get(j));
//										//com.google.gwt.user.client.Window.alert("impuesto de retencion "+ret.getImpuesto().getIdImpuesto());
										break;
									}
								}
								ret.setValorRetenido(listGrid.getRecord(i)
										.getAttributeAsDouble("lstValor"));
								totalRetenido = totalRetenido
										+ ret.getValorRetenido();
//								
								// SC.say(String.valueOf(ret.getImpuesto().getIdImpuesto()));
								det.add(ret);
								tblRetenciones.add(ret);

							}
//							com.google.gwt.user.client.Window.alert("Despues for detalle retencion");
//							totalRetenido=(CValidarDato.getDecimal(2, totalRetenido)!=0.00)?CValidarDato.getDecimal(2, totalRetenido):totalRetenido;
							totalRetenido=CValidarDato.getDecimal(2, totalRetenido);
//							com.google.gwt.user.client.Window.alert("Total "+totalRetenido);
							DtocomercialDTO doc=new DtocomercialDTO();
							if (docAuxiliar != null){
							if (docAuxiliar.getIdDtoComercial()!=null){
//								com.google.gwt.user.client.Window.alert(String.valueOf(listGrid.getRecords().length)+"DocAuxiliar "+docAuxiliar.getIdDtoComercial());
								doc.setIdDtoComercial(docAuxiliar.getIdDtoComercial());
								}}
							doc.setIdEmpresa(Factum.empresa.getIdEmpresa());
							doc.setEstablecimiento(Factum.getEstablecimientoCero());
							doc.setPuntoEmision(Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision());
							doc.setNumRealTransaccion(lastNumRealRet);
//							doc.setAutorizacion(TipoDocumento+"//"+frmCabecera.getItem("txtNumeroC").getDisplayValue());
							doc.setAutorizacion((String) frmCabecera
									.getItem("txtAutorizacion").getValue());
							
							doc.setEstado('1');
							doc.setExpiracion(frmCabecera.getItem("txtFechaEmision").getDisplayValue());
							doc.setFecha(frmCabecera.getItem("txtFechaEmision").getDisplayValue());
//							doc.setFecha(dateFecha_emision.getDisplayValue());
							doc.setIdAnticipo(null);
							doc.setNumPagos(0);
							doc.setTbldtocomercialdetalles(null);
							doc.setTbldtocomercialTbltipopagos(null);
							doc.setTblmovimientos(null);
							doc.setTblretencionsForIdDtoComercial(tblRetenciones);
//							com.google.gwt.user.client.Window.alert("Despues doc retencion 1");
							int tipoDoc=5;
							if (TipoDocumento==0) tipoDoc=5;
							else if (TipoDocumento==1) tipoDoc=19;
//							int tipoDoc=(TipoDocumento==0)?5:19;
							doc.setTipoTransaccion(tipoDoc);
							
							if (tipoDoc==19){ doc.setNumCompra(frmCabecera.getItem("txtNumeroC")
									.getDisplayValue());}
							
//							doc.setTipoTransaccion(5);
							doc.setIdNotaCredito(null);
							doc.setIdRetencion(null);
//							doc.setSubtotal(Double.parseDouble(txtTotal.getDisplayValue()));
							doc.setSubtotal(totalRetenido);
							doc.setTblpagos(null);
//							com.google.gwt.user.client.Window.alert("Despues doc retencion 2");
							PersonaDTO per=new PersonaDTO();
							//SC.say(String.valueOf("codigo seleccionado "+codigoSeleccionado));
							per.setIdPersona(codigoSeleccionado); 
							per.setCedulaRuc(frmCabecera.getItem("txtRuc").getDisplayValue());
							doc.setTblpersonaByIdPersona(per);
							per=new PersonaDTO();
							per.setIdPersona(usuario.getIdUsuario());
							doc.setTblpersonaByIdVendedor(per);
							PersonaDTO facturaPor = new PersonaDTO();
							facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
							doc.setTblpersonaByIdFacturaPor(facturaPor);
							doc.setObservacion(usuario.getUserName());
//							com.google.gwt.user.client.Window.alert("Despues doc retencion 3");
							docAuxiliar=doc;
							dynamicForm2.getField("txtRetencion").setValue(
									String.valueOf(totalRetenido));
							
							winFactura1.destroy();
							dynamicForm2.getField("txtRetencion").focusInItem();
							if (grdPagos.isVisible()){
								grdPagos.redraw();
//								//com.google.gwt.user.client.Window.alert("grabado retencion "+lgfCantidadPago.getCanEdit()+ " "+lgfCantidadPago.getValueField());
							}
							/*CODIGO PARA CALCULAR AL CONTANTO
							if (){
								dynamicForm2.getField("txtRetencion").setValue(
										String.valueOf(totalRetenido));
							}
							*/
							// getService().grabarDetalle(det, objback);
						} catch (Exception e) {
							SC.say(e.getMessage());
						}
					}

				} else {
					SC.say("Error ingrese al menos un impuesto");
				}
			} else if (indicador.equalsIgnoreCase("eimpuesto")) {
				listGrid.removeSelectedData();
				listGrid.saveAllEdits();
				listGrid.redraw();
				// det.remove(listGrid.getRecordIndex(listGrid.getSelectedRecord()));
				Total();

			} else if (indicador.equalsIgnoreCase("imprimir")) {
				// imprimirRetencion2();
				imprimirRetencionGiga();
			}
		}

		@Override
		public void onChange(ChangeEvent event) {

			Double base = 0.0;
			boolean ban = true;
			String evento = (String) event.getValue();
			if (indicador.equalsIgnoreCase("impuesto")) {
//				//com.google.gwt.user.client.Window.alert("impuesto");
				idImpuesto=evento;// que se carga aqui porque es un valor numerico y no el nombre del impuesto
//				if(baseImponible.getCanEdit()){
					for(int j=0;j<listGrid.getRecords().length;j++){
//						//com.google.gwt.user.client.Window.alert("idImpuesto "+listGrid.getRecord(j).getAttribute("lstImpuesto")+" : "+idImpuesto);
						if(listGrid.getRecord(j).getAttribute("lstImpuesto").equals(idImpuesto)){
							ban=false;
							listGrid.removeSelectedData();
							SC.say("Error: Impuesto ya ingresado...");
							break;
						}								
					}
					if(ban){
						for(int i=0;i<Impuestos.size();i++){
							//SC.say(idImpuesto+"-"+String.valueOf(Impuestos.get(i).getCodigo()));
							if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
								String baseImp=(Impuestos.get(i).getTipo()==1)?"Sub Total":"Iva";
//								//com.google.gwt.user.client.Window.alert("tipoimpuesto "+baseImp);
//								//com.google.gwt.user.client.Window.alert("base "+listGrid.getSelectedRecord().getAttribute("baseImponible"));
								listGrid.getSelectedRecord().setAttribute("baseImponible",baseImp);
//								//com.google.gwt.user.client.Window.alert("base "+listGrid.getSelectedRecord().getAttribute("baseImponible"));
								if(baseImp.equalsIgnoreCase("Iva")){
									
//									base=Double.parseDouble(txt14_iva.getDisplayValue());//DocBdase.getSubtotal()*0.12;
//									base = base *(Factum.banderaIVA/100);
//									base=base+valoresIva;
									base=valoresIva;
									base=CValidarDato.getDecimal(2,base);
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
									//SC.say("base iva "+evento);
								}else if(baseImp.equalsIgnoreCase("Sub Total")){
									base=Double.parseDouble(txtBase_imponible.getDisplayValue());
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
									//SC.say("base sub Total "+evento+" "); 
								}
								listGrid.getSelectedRecord().setAttribute("lstPorcentaje",Impuestos.get(i).getRetencion());
								listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
								//SC.say(Impuestos.get(i).getRetencion()+"-"+Impuestos.get(i).getCodigo()); 
								Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentaje");
								Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
								valor=porcentaje*valor/100;
								valor=Math.rint(valor*100)/100;
								valor=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, valor);
//								SC.say("cambio "+valor);
								listGrid.getSelectedRecord().setAttribute("lstValor",valor);
								break;
								
							}
						}
					}
//				}
//				baseImponible.setCanEdit(true);
			
				
				
			} else if (indicador.equalsIgnoreCase("base")) {

				//SC.say("hola cambiando la base");
//				//com.google.gwt.user.client.Window.alert("base "+baseImponible.getValueField()+", "+baseImponible.getDisplayField()
//				+", "+baseImponible.getName()+", "+baseImponible.getValueField());
				if(evento.equalsIgnoreCase("iva")){
//					base=Double.parseDouble(txt14_iva.getDisplayValue());//DocBdase.getSubtotal()*0.12;
//					base = base *(Factum.banderaIVA/100);
					base = 
//							base +
							valoresIva;
					base=CValidarDato.getDecimal(2,base);
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
					//SC.say("base iva "+evento);
				}else if(evento.equalsIgnoreCase("Sub Total")){
					base=Double.parseDouble(txtBase_imponible.getDisplayValue());
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
					//SC.say("base sub Total "+evento+" "); 
				}
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				//idImpuesto=listGrid.getSelectedRecord().getAttribute("lstCodigo");
				for(int i=0;i<Impuestos.size();i++){
					//SC.say(idImpuesto+"-"+String.valueOf(Impuestos.get(i).getCodigo()));
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
					//if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getCodigo()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentaje",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						//listGrid.getSelectedRecord().setAttribute("lstCodigo",idImpuesto);
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=Impuestos.get(i).getRetencion();
						//listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentaje");
						Double valor=base;
								//listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
//						SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
						
					}
				}
			
			} else if (indicador.equals("valor")) {

				//SC.say("hola cambiando la base");
				//SC.say("base iva "+evento);
				listGrid.getSelectedRecord().setAttribute("lstBaseImponible",event.getValue());
				base=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
				//SC.say("base sub Total "+evento+" "); 
			
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				
				for(int i=0;i<Impuestos.size();i++){
					//SC.say(idImpuesto+"-"+String.valueOf(Impuestos.get(i).getCodigo()));
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentaje",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=Impuestos.get(i).getRetencion();
						//listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentaje");
						Double valor=base;
								//listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			
				
			} else if (indicador.equalsIgnoreCase("porcentaje")) {

				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				for(int i=0;i<Impuestos.size();i++){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentaje",event.getValue());
						Impuestos.get(i).setRetencion(listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentaje"));
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						//listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=Double.valueOf((Double)event.getValue());
								//listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentaje");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
			}
		
				
			}
			Total();
		}
		
		
		@Override
		public void onChanged(com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
			String evento=(String)event.getValue();
			if(indicador.equalsIgnoreCase("impuesto")){
				listGrid.saveAllEdits();
				listGrid.endEditing();
				listGrid.deselectAllRecords();
			}
		}
		
		@Override
		public void onEditorExit(EditorExitEvent event) {
			String evento = event.getRecord().getAttributeAsString(
					"baseImponible");
			Double base = 0.0;
			if (indicador.equalsIgnoreCase("base")) {
				// SC.say("hola cambiando la base");
				if (evento.equalsIgnoreCase("iva")) {
//					base = Double.parseDouble(txtSubtotal.getDisplayValue());// DocBdase.getSubtotal()*0.14;
//					base = base * (Factum.banderaIVA/100);
					base=
//							base+
							valoresIva;
					base = CValidarDato.getDecimal(2, base);
					listGrid.getSelectedRecord().setAttribute(
							"lstBaseImponible", base);
					// SC.say("base iva "+evento);
				} else if (evento.equalsIgnoreCase("Sub Total")) {
					base = Double.parseDouble(txtSubtotal.getDisplayValue());
					listGrid.getSelectedRecord().setAttribute(
							"lstBaseImponible", base);
					// SC.say("base sub Total "+evento+" ");
				}
				idImpuesto = listGrid.getSelectedRecord().getAttribute(
						"lstImpuesto");
				for (int i = 0; i < Impuestos.size(); i++) {
					if (idImpuesto.equals(String.valueOf(Impuestos.get(i)
							.getCodigo()))) {
						listGrid.getSelectedRecord().setAttribute(
								"lstPorcentaje",
								Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",
								Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute(
								"lstBaseImponible", base);
						Double porcentaje = listGrid.getSelectedRecord()
								.getAttributeAsDouble("lstPorcentaje");
						Double valor = listGrid.getSelectedRecord()
								.getAttributeAsDouble("lstBaseImponible");
						valor = porcentaje * valor / 100;
						valor = Math.rint(valor * 100) / 100;
						// SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",
								valor);
						break;

					}
				}

			}
		}
	}

	public void CalculoRetencion(String indicador, ChangeEvent event) {

	}

	final AsyncCallback<List<ImpuestoDTO>> objbacklstRet = new AsyncCallback<List<ImpuestoDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");

		}

		public void onSuccess(List<ImpuestoDTO> result) {
			MapImpuesto.clear();
			Impuestos.clear();
			for (int i = 0; i < result.size(); i++) {
				Impuestos.add(result.get(i));
				MapImpuesto.put(String.valueOf(result.get(i).getIdImpuesto()),
						result.get(i).getNombre());
			}
			impuesto.setValueMap((Map) MapImpuesto);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
	};

	/*
	 * FIN DE FUNCIONES DE RETENCIONES
	 */

	public void asignacionBodegas(Integer ban) {
		// Aqui vamos a recorrer los detalles para saber si el producto tiene
		// asignadas las bodegas
		try {
			Iterator det;// = new Iterator();
			Integer asignado = 0;
			// DtoDetalles = new HashSet<DtoComDetalleDTO>(0);
			DtoComDetalleDTO detallesDto = new DtoComDetalleDTO();
			ProductoDTO proDto;
			DtoComDetalleDTO[] list = DtoDetalles
					.toArray(new DtoComDetalleDTO[0]);// (DtoComDetalleDTO[])
														// DtoDetalles.toArray();

			det = DtoDetalles.iterator();
			// mibod.setValue("mi bd tiene"+ban);
			// while(det.hasNext()){
			if (ban == 1)
				DtoDetalles = new HashSet<DtoComDetalleDTO>(0);
			for (int i = 0; i < list.length; i++) {
				detallesDto = new DtoComDetalleDTO();
				detallesDto = list[i];// (DtoComDetalleDTO) det.next();
				proDto = new ProductoDTO();
				proDto = detallesDto.getProducto();
				// mibod.setValue("paso por "+ban);
				if (proDto.getProBod() == null) {// El producto no tiene
													// asignado bodegas, se debe
													// asignar
					if (ban == 0) {
						// mibod.setValue("paso por aqui");
						crearBodegas(proDto, detallesDto.getBodega());
						// ban=1;
						break;
					} else {

						if (asignado == 0) {
							proDto.setProBod(proBod);
							proDto.setProTip(proTip);
							asignado = 1;
							list[i].setProducto(proDto);
							ban = 0;
							for (int j = i; j < list.length; j++) {
								DtoDetalles.add(list[j]);
							}
						}
					}
					// Aqui agregarmos al producto los stock por bodegas y los
					// precios por producto
				}
				if (ban == 1) {
					DtoDetalles.add(list[i]);
				}
				String valores = "";
				if (i + 1 == list.length) {
					pasarDatos(vendedor, auth, confirmar);
					// SC.say("Datos: "+valores);
				}

				// mibod.setValue("paso ss"+proDto.getProBod().size());
			}
		} catch (Exception e) {
			SC.say("Error en el recorrido: " + e);
		}
	}

	public void pasarDatos(PersonaDTO vendedor, String auth, char confirmar) {

		if (dfExtras.validate()) {

			try {
				//SC.say("VALOR: "+cmbTipoPrecio.getValue());
				//com.google.gwt.user.client.Window.alert("En pasar datos");
				if (NumDto == 7 || NumDto == 12) {
					if (anulacion<0)
					{anulacion = Integer.parseInt(cmbTipoPrecio.getValue().toString())+3000;}
					else
					{anulacion = Integer.parseInt(cmbTipoPrecio.getValue().toString());}
				}
				if (NumDto == 7 || NumDto == 4 || NumDto == 12 || NumDto == 15) {
					DtoDetalles = null;
					Subtotal = Double.parseDouble(txtTotal.getDisplayValue());
				}
				//com.google.gwt.user.client.Window.alert("En pasar datos despues ifs ");
				if (dateFecha_emision.getSelectorFormat().toString()
						.compareTo("DAY_MONTH_YEAR") == 0) {
					String fecha = dateFecha_emision.getDisplayValue();
					// fecha.replace("00:00"," ");
					String orden[] = fecha.split("/");
					// String orden[] = fechaBase[0].split("-");
					String fechaMostrar = orden[2] + "/" + orden[1] + "/"
							+ orden[0];
					dateFecha_emision.setValue(fechaMostrar);

					// SC.say("si entre al if: "+"la fecha es:"+dateFecha_emision.getDisplayValue());
				}
				//com.google.gwt.user.client.Window.alert("En pasar datos despues fecha ");				
				Double descuentoNeto = Double.parseDouble(txtDescuentoNeto
						.getDisplayValue());
				//com.google.gwt.user.client.Window.alert("En pasar datos descuento neto ");
				/*
				 * char estado='1';
				 * if(confirmar=='1'&&NumDto==7){estado='0';}else
				 * {estado=confirmar;}
				 * 
				 */
				//SC.say("RETENCIONES: "+String.valueOf(tblRetenciones.size()));
				//SC.say("RETENCIONES: "+String.valueOf(tblRetenciones.size())+" id:"+Retencion.toString());
				
				String numCompra ="";
				String observaciones = txtObservacion.getDisplayValue();
				//observaciones=usuario.getUserName();
				//AGREGAMOS EL CAMPO OBSERVACION
					
					/*if(txtObservacion.isEmpty()){
						observaciones=usuario.getUserName();	
					}else{
						observaciones=txtObservacion.getDisplayValue();		
					}*/
				
				//com.google.gwt.user.client.Window.alert("Observaciones ");
				if (NumDto !=14 && NumDto!=3){
//					observaciones=usuario.getUserName();
					observaciones = txtObservacion.getDisplayValue();
					numCompra= txtNumCompra.getDisplayValue();
				}else if (NumDto ==14){
					observaciones+=";"+txtMotivo.getDisplayValue();
					//NotaCredito=Integer.parseInt(txtNumCompra.getDisplayValue().);
//					numCompra=txtNumCompra.getDisplayValue()+","+txtCorresp.getDisplayValue();
					if (docAuxiliar!=null)	numCompra=txtNumCompra.getDisplayValue()+","+docAuxiliar.getIdDtoComercial();
					else	numCompra=txtNumCompra.getDisplayValue()+","+txtCorresp.getDisplayValue();
				}else{
					observaciones+=";"+txtMotivo.getDisplayValue();
					if (docAuxiliar!=null) numCompra=String.valueOf(docAuxiliar.getIdDtoComercial());
					else numCompra=txtCorresp.getDisplayValue();
				}
				Boolean checkRet=false;
				if (NumDto ==0 || NumDto==1){
				checkRet = (Boolean) dynamicForm2.getItem(
						"chkRetencion").getValue();
				}
				Set<RetencionDTO> retenciones=new HashSet<RetencionDTO>();
				if (checkRet) {
					if (docAuxiliar!=null) {
						docAuxiliar.setEstado(confirmar);
//						retencionDoc.setFecha(dateFecha_emision.getDisplayValue());
					}
						for (RetencionDTO retencionT:tblRetenciones){
							retencionT.setTbldtocomercialByIdDtoComercial(docAuxiliar);
							retenciones.add(retencionT);
						}
					}else{retenciones = new HashSet<RetencionDTO>(0);
						
					}
				////com.google.gwt.user.client.Window.alert("observaciones "+observaciones);
				//com.google.gwt.user.client.Window.alert("PAGOS: "+PagosDTO.size()+" "+PagosAsociados.size());
//				Set<RetencionDTO> retenciones = (checkRet)? tblRetenciones:new HashSet<RetencionDTO>(0);
//				Set<RetencionDTO> retenciones = new HashSet<RetencionDTO>(0);
				PersonaDTO facturaPor = new PersonaDTO();
				facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
				DtocomercialDTO dto = new DtocomercialDTO(
						Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento(),Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision(),
						idDto, persona,
						vendedor,
						facturaPor,
						Integer.parseInt(txtNumero_factura.getDisplayValue()),
						NumDto,// Tipo de transaccion
						dateFecha_emision.getDisplayValue(),
						dateFecha_emision.getDisplayValue(),// new Date(),
															// //fecha
															// expiracion ????
						auth, NumPagos,
						confirmar,// estado, //aqui cambie
						Subtotal, DtoDetalles,
						retenciones, // Set tblretencions
//						null,
						PagosDTO, // Set tblpagos
						null,// Set tblmovimientos
						Anticipo, NotaCredito, Retencion, PagosAsociados,
						costo, numCompra, descuentoNeto, 
//						responsable);// Tipos
						observaciones, 0);// Tipos
																				// de
																				// pago
																				// asociados
																				// al
																				// documento
				String total = txtValor14_iva.getDisplayValue();
				total = total.replace(",", "");
				Double TotalIva = Double.parseDouble(total);
				TotalIva = CValidarDato.getDecimal(4, TotalIva);
				//SC.say("si entre aqui may" );
				dto.setObservacion(observaciones);
//				dto.setObservacion(usuario.getUserName());
				grabar(dto, TotalIva, anulacion);// PAsamos el ID de la cuenta
			} catch (Exception e) {
				SC.say("Error al pasar datos "+e.getMessage());
				btnGrabar.setDisabled(false);
			}
		} else {
			SC.say("CORRIJA LOS DATOS E INTENTELO NUEVAMENTE");
		}
	}
	final AsyncCallback<DtocomercialDTO> callbackDocGuardado = new AsyncCallback<DtocomercialDTO>() {

		public void onSuccess(DtocomercialDTO result) {
			// Segun sea el tipo de Usuario que este grabando la factura sabemos
			// si se deben grabar
			// los asientos contables respectivos o no
			// El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra
			// CONFIRMAR el dto
			// ipoUsuario = result.getTipoUsuario();
			// SC.say("Tipo de usuario: "+result.getTipoUsuario());
			////com.google.gwt.user.client.Window.alert("Documento "+result.getNumRealTransaccion());
			DocBase = result;
		}

		public void onFailure(Throwable caught) {
			SC.say("No se puede cargar el documento solicitado");
		}
	};
	public void terminarProceso(Integer anulado){
//		//com.google.gwt.user.client.Window.alert("En terminar proceso");
		String idDtocomercial=String.valueOf(DocBase.getNumRealTransaccion());
		impreso+="id: "+idDtocomercial+""+" es fact: "+String.valueOf(NumDto==0)+" anul "+anulado+" conver"+convertir;
		bloqueo(true);
    	btnImprimir.setDisabled(false);
    	btnGrabar.setDisabled(true);
    	btnEliminar.setDisabled(true);
    	//SC.say("entro1 "+DocBase.getNumRealTransaccion());
		final String imprimirRet = resultado.toString();// Para
														// imprimir
														// si no
														// desea
														// imprimir
														// la
														// retenci�n
		
//		//com.google.gwt.user.client.Window.alert("terminarproceso Antes checkret");
		Boolean checkRet = false;
		//SC.say("entro2 ");
		if (NumDto == 0 || NumDto == 1) {
			checkRet = (Boolean) dynamicForm2.getItem(
					"chkRetencion").getValue();
		}
		
//		//com.google.gwt.user.client.Window.alert("terminarproceso antes imprimir retencion");

		if (NumDto == 1 && checkRet) {
			
			
			// Debemos preguntar si desea imprimir la retenci�n
//			//com.google.gwt.user.client.Window.alert("terminarproceso antes pregunta imprimir retencion");
			SC.ask("¿Desea imprimir la Retencion?",
					new BooleanCallback() {
						public void execute(Boolean value) {
							if (value != null && value) {
								// En caso de selecciona YES
								//SC.say("si imprimir");
								// imprimirRetencion2();
								winFactura1 = new Window();
								winFactura1.setWidth(800);
								winFactura1.setHeight(450);
								winFactura1.setTitle("Retencion");
								winFactura1.setShowMinimizeButton(false);
								winFactura1.setIsModal(true);
								winFactura1.setShowModalMask(true);
								winFactura1.setKeepInParentRect(true);
								winFactura1.centerInPage();
								// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
								// frmRetencion ret=new
								// frmRetencion(DocBase);//lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
								winFactura1.addCloseClickHandler(new CloseClickHandler() {
									public void onCloseClick(CloseClientEvent event) {
										winFactura1.destroy();
									}
								});
								VLayout form = new VLayout();
								form.setSize("100%", "100%");
								form.setPadding(5);
								form.setLayoutAlign(VerticalAlignment.BOTTOM);
								formRetencion = new VLayout();
								formRetencion(NumDto);
								form.addMember(formRetencion);
								winFactura1.addItem(form);
								winFactura1.show();
								//imprimirRetencionGiga();
							} else {
								if (imprimirRet
										.equals("Grabado con exito")
										&& btnGrabar
												.getTitle()
												.equals("CONFIRMAR")) {
									//imprimir();
									SC.say("Confirmado con exito");
								}else{
									SC.say(imprimirRet);
								}

							}
						}
					});
			limpiarInterfaz();
			
			if(String.valueOf(confirmar).equals("1")){
    			getService().ultimoDocumento("where TipoTransaccion = '1' ", "idDtoComercial", objbackDto);	                				                		
    		}
			//limpiarInterfaz();
		} else {
//			//com.google.gwt.user.client.Window.alert("terminarproceso else no retencion");
			//SC.say("entro3 ");
			if (NumDto == 0 || NumDto == 2 || NumDto == 4 || NumDto == 20 || NumDto == 3 || NumDto == 26){
//				//com.google.gwt.user.client.Window.alert("terminarproceso antes factura");
				if(NumDto == 0){
					if(String.valueOf(confirmar).equals("1")){
						impreso+=" "+"confirmar1"+""+" ";
						getService().ultimoDocumento("where TipoTransaccion = '0'", "idDtoComercial", objbackDto);                				                		
					}
				}
				//SC.say("entro4 "+NumDto+" impresion "+Factum.banderaConfiguracionImpresion+" docbase "+DocBase.getNumRealTransaccion());
//				//com.google.gwt.user.client.Window.alert("terminarproceso antes switch configuracion impresion");
				switch (Factum.banderaConfiguracionImpresion) {
				case 0:// IMPRESION POR PRINTVIEWE DESPUES DE
						// CONFIRMAR
//					//com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 0");
					impreso += "printpreview ";
					if (resultado.equals("Grabado con exito") && btnGrabar.getTitle().equals("CONFIRMAR")) {
						imprimir();
						limpiarInterfaz();
					}
					SC.say("Resultado: " + resultado);
					break;
				case 1:// IMPRESION DIRECTA
//					//com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 1");
					impreso += "directa ";
					if (resultado.equals("Grabado con exito") && NumDto == 3
							&& btnGrabar.getTitle().equals("CONFIRMAR") && anulado != 0 && convertir == 0
							&& Factum.banderaConfiguracionImpresionNotaCredito == 1) {
						// imprimirFacturaVentaMini();
						impreso += "nota de credito confirmacion ";
						impresionDirecta(idDtocomercial);
						// imprimir();
						limpiarInterfaz();
					} else if (resultado.equals("Grabado con exito") && NumDto == 0
							&& btnGrabar.getTitle().equals("GRABAR") && anulado != 0 && convertir == 0) {
						impreso += "convertir0 ";
						impresionDirecta(idDtocomercial);
						/*
						 * if(Factum.banderaMenuRestaurant==1 &&
						 * contadorModificarProductoElaborado>0){
						 * getService().modificarPedido(
						 * listpedidoproductoelaboradodto,
						 * objbackPedido); }
						 */
						limpiarInterfaz();// imprimirFacturaVentaMini();
					} else if (resultado.equals("Grabado con exito") && NumDto == 2
							&& btnGrabar.getTitle().equals("GRABAR") && anulado != 0 && convertir == 0) {
						// imprimirFacturaVentaMini();
						impresionDirecta(idDtocomercial);
						

						// imprimir();
						limpiarInterfaz();
					} else if (resultado.equals("Grabado con exito") && NumDto == 3
							&& btnGrabar.getTitle().equals("GRABAR") && anulado < 0 && convertir == 0) {
						// imprimirFacturaVentaMini();
						impresionDirecta(idDtocomercial);

						// imprimir();
						limpiarInterfaz();
					} else if (resultado.equals("Grabado con exito") && NumDto == 4
							&& anulado != 0 && convertir == 0) {
						// imprimirFacturaVentaMini();
						impresionDirecta(idDtocomercial);
						// imprimir();
						limpiarInterfaz();
						
					} else if (resultado.equals("Grabado con exito") && NumDto == 20
							&& btnGrabar.getTitle().equals("GRABAR") && anulado != 0 && convertir == 0) {
						// imprimirFacturaVentaMini();
						impresionDirecta(idDtocomercial);
						
						// imprimir();
						limpiarInterfaz();

					}else if (resultado.equals("Grabado con exito") && NumDto == 26
							&& btnGrabar.getTitle().equals("GRABAR") && anulado != 0 && convertir == 0) {
						// imprimirFacturaVentaMini();
						impresionDirecta(idDtocomercial);
						
						// imprimir();
						limpiarInterfaz();

					} else if (resultado.equals("Grabado con exito")
							&& btnGrabar.getTitle().equals("CONFIRMAR") && anulado != 0 && convertir == 0) {
						impreso += "ventanamini ";
						imprimirFacturaVentaMini();
						limpiarInterfaz();
					} else if (resultado.equals("Grabado con exito") && anulado >=0){
						SC.say("Resultado: " + resultado);
						convertir = 0;
						impresionDirecta(idDtocomercial);
						limpiarInterfaz();
					}else{
						SC.say("Resultado: " + resultado+" error "+impreso);
						btnGrabar.setDisabled(false);
						convertir = 0;
						limpiarInterfaz();
					}
					break;
				case 3:
//					//com.google.gwt.user.client.Window.alert("terminarproceso configuracion impresion 3");
					impreso += "mixta ";
					if (Factum.banderaConfiguracionTipoFacturacion == 0) {
						if (resultado.equals("Grabado con exito") && NumDto == 0
								&& btnGrabar.getTitle().equals("GRABAR") && anulado != 0
								&& convertir == 0) {// ESTA
													// CONFIRMANDO
													// EL DOCUMENTO

							impreso += "PreviewConf ";
							/*
							 * if(Factum.banderaMenuRestaurant==1 &&
							 * contadorModificarProductoElaborado>0)
							 * { getService().modificarPedido(
							 * listpedidoproductoelaboradodto,
							 * objbackPedido); }
							 */
							if (Factum.banderaConfiguracionImpresionFactura == 0) {
								ListGrid grdProductosImprimir = grdProductos;
								imprimir();
							} else if (Factum.banderaConfiguracionImpresionFactura == 1) {
								impresionDirecta(idDtocomercial);
							}
							limpiarInterfaz();
						}
						if (resultado.equals("Grabado con exito") && NumDto == 2
								&& btnGrabar.getTitle().equals("GRABAR") && anulado < 0
								&& convertir == 0) {
							ListGrid grdProductosImprimir = grdProductos;
							if (Factum.banderaConfiguracionImpresionNota == 0) {
								imprimir();
							} else if (Factum.banderaConfiguracionImpresionNota == 1) {
								impresionDirecta(idDtocomercial);
							}
							limpiarInterfaz();
						}
						if (resultado.equals("Grabado con exito") && NumDto == 3
								&& btnGrabar.getTitle().equals("GRABAR") && anulado < 0
								&& convertir == 0) {
							ListGrid grdProductosImprimir = grdProductos;
							if (Factum.banderaConfiguracionImpresionNotaCredito == 0) {
								imprimir();
							} else if (Factum.banderaConfiguracionImpresionNotaCredito == 1) {
								impresionDirecta(idDtocomercial);
							}
							limpiarInterfaz();
						}
						if (resultado.equals("Grabado con exito") && NumDto == 26
								&& btnGrabar.getTitle().equals("GRABAR") && anulado < 0
								&& convertir == 0) {
							ListGrid grdProductosImprimir = grdProductos;
							if (Factum.banderaConfiguracionImpresionNotaVentas == 0) {
								imprimir();
							} else if (Factum.banderaConfiguracionImpresionNotaVentas == 1) {
								impresionDirecta(idDtocomercial);
							}
							limpiarInterfaz();
						}
					} else if (Factum.banderaConfiguracionTipoFacturacion == 1) {
						impreso += "directaConf ";
						if (resultado.equals("Grabado con exito") && btnGrabar.getTitle().equals("CONFIRMAR")
								&& convertir == 0) {
							if (NumDto == 0) {
								ListGrid grdProductosImprimir = grdProductos;
								if (Factum.banderaConfiguracionImpresionFactura == 0) {
									SC.say("bandera fact");
									imprimir();
								} else if (Factum.banderaConfiguracionImpresionFactura == 1) {
									SC.say("bandera else if fact");
									impresionDirecta(idDtocomercial);
								}
								limpiarInterfaz();
							} else if (NumDto == 2) {
								ListGrid grdProductosImprimir = grdProductos;
								if (Factum.banderaConfiguracionImpresionNota == 0) {
									SC.say("bandera nota");
									imprimir();
								} else if (Factum.banderaConfiguracionImpresionNota == 1) {
									SC.say("bandera nota else if ");
									impresionDirecta(idDtocomercial);
								}
								limpiarInterfaz();
							}else if (NumDto == 3) {
								ListGrid grdProductosImprimir = grdProductos;
								if (Factum.banderaConfiguracionImpresionNotaCredito == 0) {
									SC.say("bandera notacred");
									imprimir();
								} else if (Factum.banderaConfiguracionImpresionNotaCredito == 1) {
									impresionDirecta(idDtocomercial);
								}
								limpiarInterfaz();
							}else if (NumDto == 26) {
								ListGrid grdProductosImprimir = grdProductos;
								if (Factum.banderaConfiguracionImpresionNotaVentas == 0) {
									imprimir();
								} else if (Factum.banderaConfiguracionImpresionNotaVentas == 1) {
									impresionDirecta(idDtocomercial);
								}
								limpiarInterfaz();
							}else {
									impresionDirecta(idDtocomercial);
								limpiarInterfaz();
							}
						}
					}
					break;
				}
//				//com.google.gwt.user.client.Window.alert("fin switch ");
				impreso+=" FIN";
			} else {
				limpiarInterfaz();
				SC.say("Resultado: " + resultado+impreso);
			}
//			//com.google.gwt.user.client.Window.alert("terminarproceso impreso="+impreso);
		}
		btnGrabar.setDisabled(true);
		serialParaOrdenProds = 0;
//		//com.google.gwt.user.client.Window.alert("terminarproceso antes DocFis");
		DocFis=Integer.valueOf(idDtocomercial);
	}
	String resultado="";
	public void grabar(DtocomercialDTO dto, Double txtIva, final Integer anular) {
		final Integer anulado = anular;
		final DtocomercialDTO dtoCom = dto;
//		final String resultado="";
		final AsyncCallback<String> objbackRetenc = new AsyncCallback<String>() {
			public void onSuccess(String result) {
				terminarProceso(anulado);
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		};
	
		final AsyncCallback<String> Funcioncallback = new AsyncCallback<String>() {

			public void onSuccess(String result) {
				////com.google.gwt.user.client.Window.alert("Primero"+result);
				String[] results = result.split("-");
				result=results[0];
				resultado=results[0];
				//SC.say("Separado "+result);
				String idDtocomercial=results[2].trim();
				//SC.say("Iddto "+idDtocomercial);
				if (result != null && results.length>1) {
					
					getService().getDtoID(Integer.valueOf(results[1].trim()), callbackDocGuardado);
					DocBase=dtoCom;
					DocBase.setIdDtoComercial(Integer.valueOf(results[1].trim()));
					
					//OBSERVACION
					//DocBase.setObservacion("prueba");
					
					DocBase.setNumRealTransaccion(Integer.valueOf(idDtocomercial));
					LinkedList<RetencionDTO> retenciones=new LinkedList<RetencionDTO>();
					
					if ((NumDto == 0 || NumDto == 1)){
//						if ((Boolean) dynamicForm2.getItem(
//							"chkRetencion").getValue()) {
//							for (RetencionDTO retencion:tblRetenciones){
//								retencion.setTbldtocomercialByIdFactura(DocBase);
//								retenciones.add(retencion);
//							}
//							getService().grabarRetencion(retenciones,retencion,objbackRetenc);
//						}else{
							terminarProceso(anulado);
//						}
					}else{
						terminarProceso(anulado);
					}
														
					
				} else {
					SC.say("Se ha producido el siguiente error: " + result);
					btnGrabar.setDisabled(false);
					btnImprimir.setDisabled(true);
				}
			}

			public void onFailure(Throwable caught) {
				SC.say("Error al obtener el id del cliente" + caught);
				System.out.println("" + caught);
				btnGrabar.setDisabled(false);
				btnImprimir.setDisabled(true);
			}
		};
		
		getService().grabarDocumento(dto, afeccion, txtIva, anular,  usuario.getUserName(),Factum.planCuenta,Factum.banderaConfiguracionTipoFacturacion,Factum.banderaIVA, Factum.banderaNumeroDecimales,Funcioncallback);
	}
	
	public void impresion(DtocomercialDTO dtocomercial) {
//		//com.google.gwt.user.client.Window.alert("En impresion ");
//		//com.google.gwt.user.client.Window.alert("En impresion "+dtocomercial.getNumRealTransaccion());
		boolean php=true;
		JSONObject detalleJSON = new JSONObject();
		
		List<DtoComDetalleDTO> milista = ordenarProductos(dtocomercial.getTbldtocomercialdetalles());
		String desAuxiliar;
		List<JSONObject> detallesJson= new ArrayList<JSONObject>();
		NumberFormat formato = NumberFormat.getFormat("##.00");
		Double subtotalIva14=0.0
				,Subtotal=0.0
				,baseImponible=0.0
				,total=0.0
				,totalIva14=0.0
				,impuestoadicionGlobal=0.0
				,descuentoGlobal=0.0
				,subtotalIva0=0.0
				;
//		//com.google.gwt.user.client.Window.alert("En impresion antes detalles "+milista.size());
		for (int j = 0; j < milista.size(); j++) {
			
			detalleJSON = new JSONObject();
			detalleJSON.put("cantidad",	new JSONString(formato.format(
					milista.get(j).getCantidad()
							)));
			detalleJSON.put("codigoBarras",	new JSONString(
					milista.get(j).getProducto().getCodigoBarras()
							));
			
			
			desAuxiliar = milista.get(j).getDesProducto();
			if(desAuxiliar.indexOf("#")!=-1){
				desAuxiliar=desAuxiliar.replace("#", "Num");
			}else if(desAuxiliar.indexOf("*")!=-1){
				desAuxiliar=desAuxiliar.replace("*", "");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "n");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "a");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "e");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "i");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "o");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "u");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "A");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "E");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "I");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "O");
			}else if(desAuxiliar.indexOf("�")!=-1){
				desAuxiliar=desAuxiliar.replace("�", "U");
			}else if(desAuxiliar.indexOf("&")!=-1){
				desAuxiliar=desAuxiliar.replace("&", "y");
			}


			detalleJSON.put("descripcion", new JSONString(
					desAuxiliar
					
					));

			detalleJSON.put("precio", new JSONString(formatoDecimalN.format(
							milista.get(j).getPrecioUnitario()
							)));

			detalleJSON.put("total", new JSONString(formato.format(
							milista.get(j).getTotal()
							)));
			
			String impuestos="";
			double impuestoPorc=1.0;
			double impuestoValor=0.0;
			int i1=0;
			//com.google.gwt.user.client.Window.alert("impuestos de detalle "+milista.get(j).getTblimpuestos().size());
			for (DtoComDetalleMultiImpuestosDTO detalleImp: milista.get(j).getTblimpuestos()){
				i1=i1+1;
				impuestos+=detalleImp.getPorcentaje().toString();
				impuestos= (i1<milista.get(j).getTblimpuestos().size())?impuestos+",":impuestos; 
				impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
				impuestoValor=impuestoValor+((milista.get(j).getCantidad()*milista.get(j).getPrecioUnitario()*(1-(milista.get(j).getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
			}
			//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
			
//			detalleJSON.put("iva",new JSONString(formato.format(
//					milista.get(j).getImpuesto()
//					)));
			
			detalleJSON.put("iva",new JSONString(
					impuestos
					));
			
			detalleJSON.put("descuento",new JSONString(formato.format(
					milista.get(j).getDepartamento()
							)));
			
			detallesJson.add(detalleJSON);
			Double valTotal=milista.get(j).getPrecioUnitario()* milista.get(j).getCantidad();
			Subtotal += valTotal;
			try {
//				int ivap=(int)milista.get(j).getImpuesto();
				descuentoGlobal += valTotal *(milista.get(j).getDepartamento()/100);
				if ((impuestoPorc-1)>0.0) {
					
					subtotalIva14 += (valTotal * (1 - (milista.get(j).getDepartamento()/100)));
					totalIva14+=(valTotal * (1 - (milista.get(j).getDepartamento()/100)))*(impuestoPorc);
				} else {
					subtotalIva0 += (valTotal * (1 - (milista.get(j).getDepartamento()/100)));
				}
			} catch (Exception iv) {
				subtotalIva14 += 0.0;
				subtotalIva0 += 0.0;
			}
			
			
			
		}
		Subtotal = CValidarDato.getDecimal(2, Subtotal);
		descuentoGlobal = CValidarDato.getDecimal(2, descuentoGlobal);
		impuestoadicionGlobal = CValidarDato.getDecimal(2, impuestoadicionGlobal);
		
//		total = (subtotalIva14 * (1+impuestoValor)) + subtotalIva0;
		total = totalIva14+subtotalIva0;
		total = CValidarDato.getDecimal(2, total);
		String fechaBase[] = dtocomercial.getFecha().split(" ");
		String orden[] = fechaBase[0].split("-");
		String fecha = orden[0] + "/" + orden[1] + "/" + orden[2];
		String ruc = dtocomercial.getTblpersonaByIdPersona().getCedulaRuc();
		String cliente = dtocomercial.getTblpersonaByIdPersona().getRazonSocial();
		String direccion = dtocomercial.getTblpersonaByIdPersona().getDireccion();
		String telefono = dtocomercial.getTblpersonaByIdPersona().getTelefono1();
		String observacion = dtocomercial.getObservacion();
//		String iva=String.valueOf(subtotalIva14 * (Factum.banderaIVA/100));
		String iva=String.valueOf(totalIva14);
		//String vendedor=dtocomercial.getTblpersonaByIdVendedor().getRazonSocial();
		String vendedor=dtocomercial.getTblpersonaByIdVendedor().getNombreComercial();
		String numtransaccion=String.valueOf(dtocomercial.getNumRealTransaccion());
		String archivoPHP="";
		String banderaImpresion="";
		String contado="0.00";String credito=" ";String retencion=" ";String anticipo=" ";
		String notaCred=" ";String tarjeta=" ";String tarjetaInfo=" ";String banco=" ";String bancoInfo=" ";
		JSONObject detallecJSON = new JSONObject();
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON2");
			detallecJSON.put("contado",	new JSONString(formato.format(
					Double.valueOf(contado))));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON3");
			detallecJSON.put("credito", new JSONString(formato.format(total)));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON4");
			detallecJSON.put("retencion", new JSONString(retencion));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON5");
			detallecJSON.put("anticipo", new JSONString(anticipo));
////			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON6");
//			detallecJSON.put("notaCred", new JSONString(notaCred));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON7");
			detallecJSON.put("tarjeta", new JSONString(tarjeta));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON8");
			detallecJSON.put("tarjetaInfo", new JSONString(tarjetaInfo));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON9");
			detallecJSON.put("banco", new JSONString(banco));
//			//com.google.gwt.user.client.Window.alert("En detalles pagos JSON10");
			detallecJSON.put("bancoInfo", new JSONString(bancoInfo));
		String detallesPagos="&detallesPagos="+detallecJSON;
			archivoPHP=Factum.banderaImprecionPHPFactura;//INICIALIZAMO LA VARIABLE DE CONFIGURACION ALMACENADA EN LA CLASE PRINCIPAL FACTUM
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionFactura;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
//			//com.google.gwt.user.client.Window.alert("Antes print request");
		if(php){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
					"http://"+banderaImpresion+"/IMPRESION/"+archivoPHP+"?fecha=" + fecha
							+ "&empresa=" + Factum.empresa.getRazonSocial() + "&rucEmpresa=" +Factum.empresa.getRUC() 
							+ "&ruc=" + ruc + "&cliente=" + cliente + "&direccion="
							+ direccion + "&telefono=" + telefono + "&subtotal="+ Subtotal + "&baseimponible="+ baseImponible
							+ "&iva=" + iva + "&total=" + total+ "&impuestoAdicional=" + impuestoadicionGlobal+ "&descuento=" + descuentoGlobal
							+ "&subtotalCero=" + subtotalIva0+ "&subtotalCatorce=" + subtotalIva14
							+ "&numtransaccion="+numtransaccion+  "&vendedor="+vendedor +  "&observacion="+observacion
							+ detallesPagos
							+"&detalles=" + detallesJson
							);
			try {
				rb.setHeader("Access-Control-Allow-Origin", "*");
			    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
			    rb.setHeader("Access-Control-Max-Age", "3600");
			    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
				rb.sendRequest(null, new RequestCallback() {
					public void onError(
							final com.google.gwt.http.client.Request request,
							final Throwable exception) {
						SC.say("Error al imprimir verifique el estado de la impresora: "
								+ exception);
					}

					public void onResponseReceived(
							final com.google.gwt.http.client.Request request,
							final Response response) {
						// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
						// LA GRILLA
						
						grdProductos.startEditing(0, 2, false);
						if (response.getStatusCode()!=200){
							SC.say(response.getStatusText());
						}else{
							SC.say(response.getStatusText());
						}

					}

				});
			} catch (final Exception e) {
				SC.say("Error en impresion directa "+e);
			}
		}
		
	}
	
	public void impresionDirecta(String idDtocomercial) {
		// AQUI VAMOS A EXTRAER LOS DATOS DE LA FACTURA PARA PROCEDER A
		// ENVIARLOS
//		com.google.gwt.user.client.Window.alert("Imprimir funcion "+NumDto+" id:"+idDtocomercial);
		boolean php=true;
		//String[] ids=idDtocomercial.split("-");
		
		String fecha = this.dateFecha_emision.getDisplayValue();
		String ruc = txtRuc_cliente.getDisplayValue().replace(" ","");
		String cliente = this.txtCLiente.getDisplayValue();
		//String nombreVendedor = this.txtVendedor.getDisplayValue();
//		String direccion = this.txtDireccion.getDisplayValue();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte2 "+NumDto+" id:"+txtDireccion.getValue());
		String direccion=this.txtDireccion.getValue().toString();
		String correo = this.txtCorreo.getDisplayValue();
		String telefono=persona.getTelefono1();
//		String telefono = this.txtTelefono.getDisplayValue();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3 "+NumDto+" id:"+idDtocomercial);
		String subtotal = txtSubtotal.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3.1 "+NumDto+" id:"+idDtocomercial);
		String baseImponible=txtBase_imponible.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3.2 "+NumDto+" id:"+idDtocomercial);
		String descuento = txtDescuento.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3.3 "+NumDto+" id:"+idDtocomercial);
		String impuestoAdicional = txtimpuestoAdicional.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3.4 "+NumDto+" id:"+idDtocomercial);
		String subtotalCero = txt0_iva.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte3.5 "+NumDto+" id:"+idDtocomercial);
		String iva = txtValor14_iva.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte4 "+NumDto+" id:"+idDtocomercial);
		String total = txtTotal.getValue().toString();
//		com.google.gwt.user.client.Window.alert("Imprimir funcion parte5 "+NumDto+" id:"+idDtocomercial);
		String efectivo = txtEfectivo.getValue().toString();
		String cambio = txtCambio.getValue().toString();
		// OBSERVACION
		String observacion = txtObservacion.getValue().toString();
		String subtotalCatorce=txt14_iva.getValue().toString();
		String vendedor=cmbVendedor.getValue().toString();
		//String nVendedor = 
		//String nombreVendedor = this.vendedor.getRazonSocial();
		String numtransaccion=idDtocomercial;
//		com.google.gwt.user.client.Window.alert("Imprimir funcion datos 1");
		
		//SC.say("claveAcceso "+impreso);
		//SC.say("IP DE SERVIDOR: "+Factum.banderaIpServidor);
		String archivoPHP="";
		String banderaImpresion="";
		
//		for (DtocomercialTbltipopagoDTO pago:DocBase.getTbldtocomercialTbltipopagos()){
//			pago.getTipoPago()
//		}
		//SC.say("Contado: "+String.valueOf(((Boolean) dynamicForm.getField("chkContado").getValue())));
		
		//String contado=" ";String credito=" ";String retencion=" ";String anticipo=" ";String notaCred=" ";String tarjeta=" ";String banco=" ";
		
		//if ((Boolean) dynamicForm.getField("chkContado").getValue()) contado="X";
//		if ((Boolean) dynamicForm.getField("chkContado").getValue()) contado=dynamicForm.getValueAsString("txtContado");
//		if ((Boolean) dynamicForm3.getField("chkTarjetaCr").getValue()) tarjeta="X";
////		if ((Boolean) dynamicForm3.getField("chkTarjetaCr").getValue()){
////			DocBase.getTblpagos();
////		};
//		if ((Boolean) dynamicForm3.getField("chkBanco").getValue()) banco="X";
		//SC.say("Valores: "+String.valueOf(((Boolean) dynamicForm.getField("chkContado").getValue())));
		String detallesPagos="&detallesPagos="+detallesPagos();
		//JSONObject detallesPagosJSON=detallesPagos();
		
		if (NumDto!=4 && NumDto!=15 && NumDto!=3 && NumDto!=14 && NumDto !=20){
			if ((Boolean) dynamicForm.getField("chkCredito").getValue()){
				//credito="X";
//				LinkedList<JSONObject> detallesCredito = new LinkedList<JSONObject>();
//				detallesCredito=detallesCredito();
				detallesPagos+="&detallesCredito="+detallesCredito();
			}
//			if ((Boolean) dynamicForm2.getField("chkRetencion").getValue()) retencion="X";
//			if ((Boolean) dynamicForm2.getField("chkNotaCredito").getValue()) notaCred="X";
//			if ((Boolean) dynamicForm2.getField("chkAnticipo").getValue()) anticipo="X";
		} 
		//SC.say("Valores2: "+String.valueOf(((Boolean) dynamicForm.getField("chkContado").getValue())));
		if (Factum.banderaMenuFacturacionElectronica==1){
			//SC.say("docbase tipo "+DocBase.getTipoTransaccion());
			String tipotransaccion=(DocBase.getTipoTransaccion()==0)?"01":"07";
			//SC.say("docbase tipotransaccion "+tipotransaccion+" "+DocBase.getFecha()+" "+DocBase.getNumRealTransaccion()+" "+DocBase.getIdDtoComercial());
//			String claveacceso=generarClaveAcceso.Generar(DocBase, "1", tipotransaccion);
//			//com.google.gwt.user.client.Window.alert("Llamar generar clave");
			String claveacceso=generarClaveAcceso.Generar(fecha,numtransaccion, "1", tipotransaccion);
			//SC.say("clave generada "+claveacceso);
			numtransaccion=numtransaccion+"&claveacceso="+claveacceso;
		}
//		com.google.gwt.user.client.Window.alert("Antes bandera");
		//SC.say("Aft claveAcceso "+impreso);
		if(NumDto==0){
			archivoPHP=Factum.banderaImprecionPHPFactura;//INICIALIZAMO LA VARIABLE DE CONFIGURACION ALMACENADA EN LA CLASE PRINCIPAL FACTUM
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionFactura;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
		}else if(NumDto==2){
			archivoPHP=Factum.banderaImprecionPHPNota;//INICIALIZAMOS LA VARIABLE DE CONFIGURACION ALAMCENADA EN LA CLASE PRINCIPAL FACTUM
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionNota;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
		}else if(NumDto==3){
			archivoPHP=Factum.banderaImprecionPHPNotaCredito;//INICIALIZAMOS LA VARIABLE DE CONFIGURACION ALAMCENADA EN LA CLASE PRINCIPAL FACTUM
			numtransaccion=numtransaccion+"&corresponde="+txtCorresp.getValue().toString();
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionNotaCredito;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
		}else if(NumDto==26){
			archivoPHP=Factum.banderaImprecionPHPNotaVentas;//INICIALIZAMOS LA VARIABLE DE CONFIGURACION ALAMCENADA EN LA CLASE PRINCIPAL FACTUM
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionNotaVentas;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
		}else if(NumDto==20){
			archivoPHP=Factum.banderaImprecionPHPProforma;//INICIALIZAMOS LA VARIABLE DE CONFIGURACION ALAMCENADA EN LA CLASE PRINCIPAL FACTUM
			if(Factum.banderaConfiguracionImpresion==3){
				banderaImpresion=Factum.banderaIpImpresionProforma;
			}else{
				banderaImpresion=Factum.banderaIpImpresion;
			}
		}else if(NumDto==8){
			banderaImpresion=Factum.banderaIpImpresion;
			archivoPHP=Factum.banderaImprecionPHPAjuste;//INICIALIZAMOS LA VARIABLE DE CONFIGURACION ALAMCENADA EN LA CLASE PRINCIPAL FACTUM
		}else if(NumDto==4){
			numtransaccion=numtransaccion+"&concepto="+txtaConcepto.getDisplayValue();
			banderaImpresion=Factum.banderaIpImpresion;
			archivoPHP="imprimirAnticipo.php";
		}else {
			//SC.say("Otro");
			imprimir();
			php=false;
		}
		if(php){
			//SC.say(banderaImpresion+"-"+Factum.banderaImprecionPHPProforma);
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
					"http://"+banderaImpresion+"/IMPRESION/"+archivoPHP+"?fecha=" + fecha
							+ "&empresa=" + Factum.empresa.getRazonSocial() + "&rucEmpresa=" +Factum.empresa.getRUC() 
							+ "&ruc=" + ruc + "&cliente=" + cliente + "&direccion="
							+ direccion + "&telefono=" + telefono + "&subtotal="+ subtotal + "&baseimponible="+ baseImponible
							+ "&iva=" + iva + "&total=" + total+ "&impuestoAdicional=" + impuestoAdicional+ "&descuento=" + descuento
							+ "&efectivo=" + efectivo + "&cambio=" + cambio + "&subtotalCero=" + subtotalCero+ "&subtotalCatorce=" + subtotalCatorce
							+ "&numtransaccion="+numtransaccion+  "&vendedor="+vendedor + "&observacion="+observacion
//							+ "&contado="+contado+"&credito=" +credito+"&retencion="+retencion+"&anticipo="+anticipo+"&notaCred="+notaCred
//							+"&tarjeta="+tarjeta+"&banco="+banco
//							+"&detallePagos=" + detallesPagos()
//							+"&detalleCredito=" + detallesCredito
							+ detallesPagos
							+"&detalles=" + detallesJson
							);
			try {
				rb.setHeader("Access-Control-Allow-Origin", "*");
			    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
			    rb.setHeader("Access-Control-Max-Age", "3600");
			    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
				rb.sendRequest(null, new RequestCallback() {
					public void onError(
							final com.google.gwt.http.client.Request request,
							final Throwable exception) {
						// Window.alert(exception.getMessage());
						SC.say("Error al imprimir verifique el estado de la impresora: "
								+ exception);
					}

					public void onResponseReceived(
							final com.google.gwt.http.client.Request request,
							final Response response) {
						// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
						// LA GRILLA
						
						grdProductos.startEditing(0, 2, false);
						if (response.getStatusCode()!=200){
							SC.say(response.getStatusText());
						}else{
							SC.say(response.getStatusText());
									//+": "+response.getText());
						}
//						SC.say(response.getStatusText()
//								+" texto: "+response.getText()
//								);
						//response.
					}

				});
			} catch (final Exception e) {
				SC.say("Error en impresion directa "+e);
			}
		}
		
	}

	final AsyncCallback<String> objback = new AsyncCallback<String>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

		public void onSuccess(String result) {
			if (!result.isEmpty()) {
				String comienzoMensaje=(afeccion==0)?"El cliente tiene pagos pendientes":"Pagos pendientes con el proveedor";
				SC.say(comienzoMensaje+result);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");

			}

		}

	};

	final AsyncCallback<String> objbackPuntos = new AsyncCallback<String>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

		public void onSuccess(String result) {
			if (!result.isEmpty()) {
				SC.say(result);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");

			}

		}

	};

	final AsyncCallback<List<CategoriaDTO>> objbacklstCat = new AsyncCallback<List<CategoriaDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());

		}

		public void onSuccess(List<CategoriaDTO> result) {
			listcat = result;
			MapCategoria.clear();
			MapCategoria.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapCategoria.put(
						String.valueOf(result.get(i).getIdCategoria()), result
								.get(i).getCategoria());
			}
			cmbCategoria.setValueMap(MapCategoria);
		}

	};
	final AsyncCallback<List<MarcaDTO>> objbacklstMarca = new AsyncCallback<List<MarcaDTO>>() {

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<MarcaDTO> result) {
			listmar = result;
			MapMarca.clear();
			MapMarca.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapMarca.put(String.valueOf(result.get(i).getIdMarca()), result
						.get(i).getMarca());
			}
			cmbMarca.setValueMap(MapMarca);

		}
	};
	final AsyncCallback<List<UnidadDTO>> objbacklstUnidad = new AsyncCallback<List<UnidadDTO>>() {

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}

		public void onSuccess(List<UnidadDTO> result) {
			listuni = result;
			MapUnidad.clear();
			MapUnidad.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapUnidad.put(String.valueOf(result.get(i).getIdUnidad()),
						result.get(i).getUnidad());
			}
			cmbUnidad.setValueMap(MapUnidad);
		}
	};
	final AsyncCallback<List<BodegaDTO>> objbacklstBod = new AsyncCallback<List<BodegaDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<BodegaDTO> result) {
			MapBodega.clear();
			MapBodega.put("", "");
			listBodega = result;
			for (int i = 0; i < result.size(); i++) {
				MapBodega.put(String.valueOf(result.get(i).getIdBodega()),
						result.get(i).getBodega());
			}
			cmdBod.setValueMap(MapBodega);
		}

	};
	final AsyncCallback<List<TipoPrecioDTO>> objbacklstTip = new AsyncCallback<List<TipoPrecioDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<TipoPrecioDTO> result) {
			listTipoP = result;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};

	final AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> listaCallback = new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");

		}

		public void onSuccess(LinkedHashMap<Integer,ProductoDTO> result) {
			linkedhashmapproductos= new LinkedHashMap<Integer,ProductoDTO>();
			linkedhashmapproductos=result;
			//SC.say(String.valueOf(linkedhashmapproductos.size()));
			getService().numeroRegistrosProducto("tblproducto", objbackI);
			
			if(contador>registros){
				lblRegistrosFactura.setText(registros+" de "+registros);
			}else{
				lblRegistrosFactura.setText(contador+" de "+registros);
			}
			RecordList listado = new RecordList();
			int numProductos = 0;
			Iterator iter = result.values().iterator();
			while (iter.hasNext()) {
				ProductoRecords pro = new ProductoRecords((ProductoDTO) iter.next());
				pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
				pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
				pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
				listado.add(pro);
				numProductos++;

			}
			lblRegistrosFactura.setText(numProductos + " de " + registros);
			lstProductos.setData(new RecordList());
			lstProductos.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};
	
	
	final AsyncCallback<ArrayList<PedidoProductoelaboradoDTO>> listaCallbackPedido = new AsyncCallback<ArrayList<PedidoProductoelaboradoDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");

		}

		public void onSuccess(ArrayList<PedidoProductoelaboradoDTO> result) {
			RecordList listado = new RecordList();
			//int numProductos = 0;
			//Iterator iter = result.iterator();
			//String mensaje="";
			//SC.say("TAMA�O LIST: "+result.size()+result.get(1).getTblproducto().getDescripcion());
			listpedidoproductoelaboradodto=result;
			for(PedidoProductoelaboradoDTO pedidoproductoelaboradodto: result) {				
				ProductoDTO productodto = new ProductoDTO(); 
				productodto=pedidoproductoelaboradodto.getTblproducto();
				
				ProductoRecords pro = new ProductoRecords(productodto);
				pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
				pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
				pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));				
				listado.add(pro);
				//numProductos++;
				
				//mensaje = mensaje+"/"+String.valueOf(productodto.getEstado()+"-"+productodto.getIdProducto()+"-"+productodto.getCodigoBarras()+"-"+productodto.getDescripcion()+"-"+productodto.getStock()+"-"+productodto.getImpuesto()+"-"+productodto.getLifo()+"-"+productodto.getFifo()+"-"+productodto.getPromedio()+"-"+productodto.getTblunidad()+"-"+productodto.getTblcategoria()+"-"+productodto.getTblmarca()+"-"+productodto.getProTip().get(1).getPorcentaje()+"-"+productodto.getProTip().get(0).getPorcentaje()+"-"+productodto.getProTip().get(2).getPorcentaje()+"-"+productodto.getJerarquia()+"/");
				//mensaje = mensaje+String.valueOf("P"+productodto.getEstado()+"-"+productodto.getIdProducto()+"-"+productodto.getCodigoBarras()+"-"+productodto.getDescripcion()+"-"+productodto.getStock()+"-"+productodto.getImpuesto()+"-"+productodto.getLifo()+"-"+productodto.getFifo());
			}
			
			//SC.say(mensaje);
			lstProductosElaborados.setData(new RecordList());
			lstProductosElaborados.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};
	
	final AsyncCallback<List<AreaDTO>>  objbacklstAreas=new AsyncCallback<List<AreaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(List<AreaDTO> result) {
			List<AreaDTO>  listareadto= result;
			areas=new TabsetAreas();
			for(AreaDTO areadto: listareadto){
				area=new TabArea(areadto);	
				Iterator iteratormesas=areadto.getMesasDTO().iterator();
				mesas=new TileGridMesas();
				if(iteratormesas.hasNext()){
					MesaRecords[] tmpList = new MesaRecords[areadto.getMesasDTO().size()];
					int i=0;
					 while(iteratormesas.hasNext()){
						 MesaDTO mesadto = (MesaDTO) iteratormesas.next();	
						 tmpList[i]=  new MesaRecords(mesadto.getIdMesa(),mesadto.getNombreMesa(), mesadto.getImagen());
						 i=i+1;		 
					 }
					 mesas.setData(tmpList);
					 mesas.addRecordClickHandler(new ManejadorBotones("mesa"));
				}
				area.setPane(mesas);
				areas.addTab(area);
			}		
		}
	};
	
	final AsyncCallback<LinkedHashMap<Integer,ProductoDTO>> listaCallbackOrdenes = new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");

		}

		public void onSuccess(LinkedHashMap<Integer,ProductoDTO> result) {
			
			linkedhashmapproductos=new LinkedHashMap<Integer,ProductoDTO>();
			linkedhashmapproductos=result;
			getService().numeroRegistrosProducto("tblproducto", objbackI);
			
			if (contador > registros) {
				//lblRegisros.setText(registros + " de " + registros);
			} else {
				//lblRegisros.setText(contador + " de " + registros);
			}
			 
			RecordList listado = new RecordList();
			int numProductos = 0;
			Iterator iter = result.values().iterator();
			while (iter.hasNext()) {
				ProductoRecords pro = new ProductoRecords((ProductoDTO) iter.next());
				pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
				pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
				pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
				listado.add(pro);
				numProductos++;

			}
			lblRegistrosFactura.setText(numProductos + " de " + registros);
			lstProductos.setData(new RecordList());
			lstProductos.setData(listado);
			
			
			/** PRECARGO A LA FUERZA LA ORDEN **/
			
			ListGridRecord[] recordsprod = lstProductos.getRecords();
			for(ListGridRecord r:recordsprod){
				String desc = r.getAttribute("descripcion");
				if(desc.equals("SERVICIO TECNICO")){
					r.setAttribute("cantidad", "1");
					r.setAttribute("Precio", String.valueOf(Factura.this.precioSugeridoServicioTecnico));
					r.setAttribute("promedio", String.valueOf(Factura.this.precioSugeridoServicioTecnico));
					CargarProductoOrdenes(r);
				}
			}
			//Factura.this.calculo_Subtotal();
			listadoProd.hide();
			SC.say("ATENCION! Si esta de acuerdo con el precio sugerido digite el campo Precio sin IVA nuevamente");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};
	
	final AsyncCallback<Integer> objbackI = new AsyncCallback<Integer>() {
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());

		}

		public void onSuccess(Integer result) {
			//registros = result;
			//auxgrilla++;
			if (auxgrilla == 0 && DocFis <0){
			grdProductos.startEditing(0, 2, false);
			}
			// SC.say("Numero de registros "+registros);
			auxgrilla=1;
		}
	};

	/************************************ FIN DE FUNCIONES DEL LISTADO DE PRODUCTO ***************************************/
	public static GreetingServiceAsync getService() {
		return GWT.create(GreetingService.class);
	}

	/************** FUNCIONES PARA MOSTRAR BODEGAS EN CASO DE COMPRA ************************/
	// ESTA FUNCION NO SE ESTA USANDO, SE PUSO DIRECTAMENTE EN GENERARDETALLE ..
	// PERO NO SE EN QUE OTRO LUGAR USE
	/*public void crearBodegas(ProductoDTO pro, Double cant) {
		try {

			this.producto = pro;
			// En caso de ser 0 se lo llamaria del menu de Producto
			proBod = new ArrayList<ProductoBodegaDTO>();

			ProductoBodegaDTO pb = new ProductoBodegaDTO();
			pb.setCantidad(cant);
			BodegaDTO bodega = new BodegaDTO();

			String mostrar = dfExtras.getItem("cmbTipoBodega")
					.getDisplayValue();
			getService().buscarBodegaSegunNombre(mostrar, retornaBodega);
			bodega.setIdBodega(mibodega.getIdBodega());// AQUI CAMBIA

			// mibod.setValue("bodega "+mostrar+"el id es "+mibodega.getIdBodega());
			pb.setTblbodega(bodega);
			pb.setTblproducto(producto);
			proBod.add(pb);
			proTip = null;
			// listadoProd.destroy();

			asignacionBodegas(1);
		} catch (Exception e) {
			SC.say(e.getMessage());
		}
	}*/
	
	public void crearBodegas(ProductoDTO pro, BodegaDTO bod) {
		try {

			this.producto = pro;
			// En caso de ser 0 se lo llamaria del menu de Producto
			proBod = new ArrayList<ProductoBodegaDTO>();

			ProductoBodegaDTO pb = new ProductoBodegaDTO();
			pb.setCantidad(0.0);
			//BodegaDTO bodega = new BodegaDTO();

			/*String mostrar = dfExtras.getItem("cmbTipoBodega")
					.getDisplayValue();
			getService().buscarBodegaSegunNombre(mostrar, retornaBodega);
			bodega.setIdBodega(mibodega.getIdBodega());// AQUI CAMBIA*/

			// mibod.setValue("bodega "+mostrar+"el id es "+mibodega.getIdBodega());
			pb.setTblbodega(bod);
			pb.setTblproducto(producto);
			proBod.add(pb);
			proTip = null;
			// listadoProd.destroy();

			asignacionBodegas(1);
		} catch (Exception e) {
			SC.say(e.getMessage());
		}
	}

	final AsyncCallback<List<TipoPrecioDTO>> objbacklstTipo = new AsyncCallback<List<TipoPrecioDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say(caught.toString());

		}

		public void onSuccess(List<TipoPrecioDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for (int i = 0; i < result.size(); i++) {
				listado[i] = (new TipoPrecioRecords(
						(TipoPrecioDTO) result.get(i)));
			}
			lstTipoPrecio.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};

	final AsyncCallback<List<BodegaDTO>> objbacklst = new AsyncCallback<List<BodegaDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<BodegaDTO> result) {
			/*
			 * getService().numeroRegistrosBodega("Tblbodega", objbackI);
			 * if(contador>registros){
			 * lblRegisros.setText(registros+" de "+registros); }else
			 * lblRegisros.setText(contador+" de "+registros);
			 */
			ListGridRecord[] listado = new ListGridRecord[result.size()];

			for (int i = 0; i < result.size(); i++) {
				listado[i] = (new BodegaRecords((BodegaDTO) result.get(i)));
			}
			listadoBod = listado;
			listGridBod.setData(listado);
			listGridBod.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};

	/******** FIN DE FUNCIONES DE CREACION DE BODEGAS EN CASO DE COMPRA ********************************************************************************/

	final AsyncCallback<List<TipoPrecioDTO>> objbacklstTipoPrecio = new AsyncCallback<List<TipoPrecioDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());

		}

		public void onSuccess(List<TipoPrecioDTO> result) {
			listaTipoPrecio=result;
			MapTipoPrecio.clear();
			MapTipoPrecio.put("","");
			TipoPrecioDTO publico = new TipoPrecioDTO();
			for(TipoPrecioDTO tipopreciodto:result) {
            	MapTipoPrecio.put(String.valueOf(tipopreciodto.getIdTipoPrecio()), tipopreciodto.getTipoPrecio());
            } 
			/*
				MapTipoPrecio.put(String.valueOf(result.get(2).getIdTipoPrecio()), 
						result.get(2).getTipoPrecio());
        		MapTipoPrecio.put(String.valueOf(result.get(0).getIdTipoPrecio()), 
						result.get(0).getTipoPrecio());
        		MapTipoPrecio.put(String.valueOf(result.get(1).getIdTipoPrecio()), 
						result.get(1).getTipoPrecio());
			*/
        		
			cmbTipoPrecio.setValueMap(MapTipoPrecio);
			tipoP=result.get(1).getIdTipoPrecio().toString();
			lstPrecio.setTitle("Precio "+result.get(1).getTipoPrecio());
			cmbTipoPrecio.setDefaultValue(result.get(1).getIdTipoPrecio());
			//cmbTipoPrecio.setDefaultToFirstOption(true);
			//try{cmbTipoPrecio.setDefaultValue(2);}
			//catch(Exception e){}
			//cmbTipoPrecio.setDefaultValue(result.get(1).getIdTipoPrecio());
			cmbTipoPrecio.redraw();
			
			
			
		}	
	};
	
	/*final AsyncCallback<List<BodegaDTO>> objbacklstTipoBodega = new AsyncCallback<List<BodegaDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}

		@Override
		public void onSuccess(List<BodegaDTO> result) {

			MapTipoBodega.clear();
			// MapTipoBodega.put("","");
			listBodega = result;
			for (int i = 0; i < result.size(); i++) {
				MapTipoBodega.put(String.valueOf(result.get(i).getIdBodega()),
						result.get(i).getBodega());
				// SC.say("esto esta bodega"+result.get(i).getBodega().toString());
				// mibod.setValue("ahora"+result.get(i).getBodega().toString());
			}
			cmbTipoBodega.setValueMap(MapTipoBodega);
			cmbTipoBodega.setDefaultToFirstOption(true);

		}
	};*/

	protected LinkedList<PosicionDTO> listpos;

	/***************************** FUNCION PARA IMPRIMIR ********************************/
	/***************************** FUNCION PARA IMPRIMIR ********************************/

	public String validarCantidades() {
		String mensaje = "";
		try {
			grdProductos.saveAllEdits();
			for (ListGridRecord rec : grdProductos.getRecords()) {
				if (rec.getAttributeAsDouble("cantidad") <= 0.0) {
					mensaje = "Revise las cantidades de los productos ingresados";
				}
			}
		} catch (Exception e) {
			mensaje = "Error al intentar extraer los items de la factura: " + e;
		}
		return mensaje;
	}

	public void imprimirRetencion() {
		String encabezado = "";
		// ListGridRecord[] selectedRecords = grdProductos.getRecords();
		// com.smartgwt.client.widgets.Label[] listTxt=new
		// com.smartgwt.client.widgets.Label[selectedRecords.length];
		int i = 0;
		int count = 0;
		String total = "";
		String des = "<table width='100%' border='0' class='estilo1'>";

		// if(NumDto==0)//Si es la factura peque�a, agregamos una fila
		// {
		des = des
				+ "<tr style='line-height:10px;'>"
				+ "<td width=30%'><div align='center'><strong>&nbsp;</strong></div></td>"
				+ "<td width='12%'><div align='center'><strong>&nbsp;</strong></div></td>"
				+ "<td width='8%'><div align='right'><strong>&nbsp;</strong></div></td>"
				+ "<td width='10%'><div align='right'><strong>&nbsp;</strong></div></td>"
				+ "<td width='10%'><div align='right'><strong>&nbsp;</strong></div></td>"
				+ "<td width='30%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
		// }
		String descripcion = "";
		Iterator ret = tblRetenciones.iterator();
		RetencionDTO retencion = new RetencionDTO();
		String impuesto = "";
		Double porciento = 0.0;

		while (ret.hasNext()) {
			retencion = new RetencionDTO();
			retencion = (RetencionDTO) ret.next();
			count++;
			impuesto = retencion.getImpuesto().getNombre();
			if (impuesto.contains("RENTA")) {
				impuesto = "RENTA";
			} else if (impuesto.contains("IVA")) {
				impuesto = "IVA";
			}
//			porciento = (retencion.getValorRetenido() * 100)
//					/ retencion.getBaseImponible();
//			porciento = CValidarDato.getDecimal(0, porciento);
			porciento = CValidarDato.getDecimal(2, retencion.getImpuesto().getRetencion());

			des = des
					+ // 30-12-8-10-10-30
					"<tr style='line-height:10px;'>"
					+ "<td width='30%'><div align='center'>"
					+ txtNumCompra.getDisplayValue()
					+ "</div></td>"
					+ "<td width='12%'><div align='center'>"
					+ retencion.getBaseImponible()
					+ "</div></td>"
					+ "<td width='8%'><div align='center'>"
					+ impuesto
					+ "</div></td>"
					+ "<td width='10%'><div align='center'>"
					+ porciento.toString()
					+ "</div></td>"
					+ "<td width='10%'><div align='right'>"
					+ retencion.getValorRetenido()
					+ "</div></td>"
					+ "<td width='30%'><div align='right'>&nbsp;</div></td></tr>";

		}

		String numeracion = "";

		// if(NumDto==0){

		for (i = count; i < 4; i++) {
			des = des
					+ "<tr style='line-height:9px;'>"
					+ "<td width='30%'><div align='center'><strong>&nbsp;</strong></div></td>"
					+ "<td width='12%'><div align='center'><strong>&nbsp;</strong></div></td>"
					+ "<td width='8%'><div align='right'><strong>&nbsp;</strong></div></td>"
					+ "<td width='10%'><div align='right'><strong>&nbsp;</strong></div></td>"
					+ "<td width='10%'><div align='right'><strong>&nbsp;</strong></div></td>"
					+ "<td width='30%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
		}
		// }
		des = des + "</table>";
		// this.dateFecha_emision.getva
		// if(NumDto==0)
		// {
		total = "<table width='100%' border='0' class='estilo1'>";
		total = total
				+ "<tr><td width='58%'>&nbsp;</td>"
				+ "<td width='12%'><div align='right'></div></td>"
				+ "<td width='30%'>&nbsp;</td></tr>"
				+ "<tr style='line-height:10px;'><td><div align='right'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></td>"
				+ "<td><div align='right'></div></td>"
				+ "<td width='30%'>&nbsp;</td></tr>"
				+ "<tr style='line-height:10px;'><td>&nbsp;</td><td><div align='right'>"
				+ this.txtTotalRet.getValue().toString() + "</div></td>"
				+ "<td width='30%'>&nbsp;</td></tr>";
		total = total + "</table>";
		// }

		dateFecha_emision
				.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_emision.setMaskDateSeparator("-");
		dateFecha_emision
				.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_emision
				.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		String fecha = dateFecha_emision.getDisplayValue();
		fecha.replace("00:00", " ");
		String anioFiscal[] = fecha.split("/");
		String anioF = anioFiscal[0];
		// SC.say(fecha+"   "+dateFecha_emision.getValue());
		String cabecera = "";
		String direccion = this.txtDireccion2.getValue().toString();

		// if(NumDto==0)
		// {
		cabecera = " <tr>"
				+ "	<td width='40%'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ fecha
				+ "</td>"
				+ "	<td width='10%'>&nbsp;</td>"
				+ " 	<td width='30%'><div align='left'>"
				+ this.txtRuc_cliente.getValue().toString()
				+ "</div></td>"
				+ "<td width='20%'><div align='center'><strong>&nbsp</strong></div></td>"
				+ "   </tr>"
				+ "<tr style='line-height:10px;'>"
				+ "<td colspan='2'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ this.txtCLiente.getValue().toString()
				+ "</td>"
				+ "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ anioF
				+ "</td>"
				+ "</tr>"
				+ "<tr style='line-height:10px;'>"
				+ "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
				+ direccion
				+ "</td>"
				+ "<td></td>"
				+ "<td></td>"
				+ "</tr>"
				+ "</table>";
		// }
		encabezado = "<table width='100%' border='0' class='estilo1'>";
		encabezado = encabezado + cabecera;

		String espacio = "";
		String tipoLetra = "";

		tipoLetra = " font-family: 'MS Serif', 'New York', serif;"
				+ "	font-size: 11px;";

		espacio = "<p>&nbsp;</t>";

		String ajaxDefinition = "	</head>" + "<style type='text/css'>"
				+ "	<!--" + "	.estilo1 {"
				+

				// "	font-family: 'Gill Sans MT Condensed';"+
				tipoLetra + "	}" + "	-->" + "</style>"
				+ "<body>	"
				+
				// espacio+
				// "<p>&nbsp;</p>"+
				encabezado + "<p>&nbsp;</p>" + des + total + "</body>"
				+ "</html>";
		canvas.setContents(ajaxDefinition);
		canvas.showPrintPreview(canvas);
	}

	public void imprimirRetencion2() {
		String tablaEncabezado1 = "";
		String tablaEncabezado2 = "";
		String tablaEncabezado3 = "";
		String tablaItems = "";
		String tablaItemsVacia = "";
		String tablaTotal = "";
		String ajaxDefinition = "";

		int i = 0;
		int count = 0;
		Iterator ret = tblRetenciones.iterator();
		RetencionDTO retencion = new RetencionDTO();
		String impuesto = "";
		Double porciento = 0.0;
		dateFecha_emision
				.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_emision.setMaskDateSeparator("-");
		dateFecha_emision
				.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_emision
				.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		String fecha = dateFecha_emision.getDisplayValue();
		fecha.replace("00:00", " ");
		String anioFiscal[] = fecha.split("/");
		String anioF = anioFiscal[2];
		// SC.say(fecha+"   "+dateFecha_emision.getValue());
		String cabecera = "";
		String direccion = this.txtDireccion2.getValue().toString();
		tablaItems = tablaItems
				+ "<table id='items' style=' margin: 1px 0 0 0; margin-top: 6px;  width: 100%;font-family: MS Serif, New York, serif;font-size: 11px;'>"
				+ "<tr>"
				+ "<th style='border: none; padding: 1px; width:35%'></th>"
				+ "<th style='border: none; padding: 1px; width:12%'></th>"
				+ "<th style='border: none; padding: 1px; width:9%'></th>"
				+ "<th style='border: none; padding: 1px; width:12%'></th>"
				+ "<th style='border: none; padding: 1px; width:30%'></th>"
				+ "</tr>";
		while (ret.hasNext()) {
			retencion = new RetencionDTO();
			retencion = (RetencionDTO) ret.next();
			count++;
			impuesto = retencion.getImpuesto().getNombre();
			if (impuesto.contains("RENTA")) {
				impuesto = "RENTA";
			} else if (impuesto.contains("IVA")) {
				impuesto = "IVA";
			}
//			porciento = (retencion.getValorRetenido() * 100)
//					/ retencion.getBaseImponible();
//			porciento = CValidarDato.getDecimal(0, porciento);
			porciento = CValidarDato.getDecimal(2, retencion.getImpuesto().getRetencion());
			tablaItems = tablaItems
					+

					"<tr class='item-row' style='border: 0; vertical-align: top;'>"
					+ "<td class='item-name' style='border: none; padding: 1px;text-align:left'>"
					+ txtNumCompra.getDisplayValue()
					+ "</td>"
					+ "<td class='description' style='border: none; padding: 1px; text-align:left'>"
					+ retencion.getBaseImponible()
					+ "</td>"
					+ "<td style='border: none; padding: 1px;text-align:left'>"
					+ impuesto
					+ "</td>"
					+ "<td style='border: none; padding: 1px;text-align:left'>"
					+ porciento.toString()
					+ "</td>"
					+ "<td style='border: none; padding: 1px; text-align:left'>"
					+ retencion.getValorRetenido() + "</td>" + "</tr>";
		}
		tablaItems = tablaItems + "</table>";

		String numeracion = "";

		tablaItemsVacia = "<table id='itemsVacia' style=' margin: 1px 0 0 0; margin-top: 6px;  width: 100%;font-family: MS Serif, New York, serif;font-size: 11px;'>"
				+ "<tr>";
		for (i = count; i < 5; i++) {
			tablaItemsVacia = tablaItemsVacia + "<tr>"
					+ "<th style='border: none; padding: 7px; width:35%'></th>"
					+ "<th style='border: none; padding: 7px; width:12%'></th>"
					+ "<th style='border: none; padding: 7px; width:9%'></th>"
					+ "<th style='border: none; padding: 7px; width:12%'></th>"
					+ "<th style='border: none; padding: 7px; width:30%'></th>"
					+ "</tr>";
			/*
			 * "<tr class='item-row' style='border: 0; vertical-align: top;'>"+
			 * "<td class='item-name' style='border: none; padding: 1px;text-align:left'></td>"
			 * +
			 * "<td class='description' style='border: none; padding: 1px; text-align:left'></td>"
			 * + "<td style='border: none; padding: 1px;text-align:left'></td>"+
			 * "<td style='border: none; padding: 1px;text-align:left'></td>"+
			 * "<td style='border: none; padding: 1px; text-align:left'></td>"+
			 * "</tr>";
			 */
		}
		// }
		tablaItemsVacia = tablaItemsVacia + "</table>";

		tablaEncabezado1 = "<html>"
				+ "<body>"
				+ "<div id='page-wrap'style='width: 600px; margin: 0 auto;'>"
				+ "<div id='customer' style='overflow:hidden;'>"
				+ "<table id='meta' style='margin-top: 1px; width: 100%; float: right; border:none;font-family: MS Serif, New York, serif;font-size: 11px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 1px; width:12%'></td>"
				+ "<td style='text-align: left; border: none; padding: 1px; width:20%'>"
				+ fecha
				+ "</td>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 1px;width:15%'></td>"
				+ "<td style='text-align: left; border: none; padding: 1px;width:53%'>"
				+ this.txtRuc_cliente.getValue().toString() + "</td>" + "</tr>"
				+ "</table>";
		tablaEncabezado2 = "<table id='meta' style='margin-top: 0px; width: 100%; border:none;font-family: MS Serif, New York, serif; font-size: 11px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 1px;width:5%'></td>"
				+ "<td style='text-align: left; border:none; padding: 1px;width:52%'>"
				+ this.txtCLiente.getValue().toString().substring(0, 30)
				+ "</td>"
				+ "<td class='meta-head' style='text-align: right; border: none; padding: 1px;width:3%'></td>"
				+ "<td style='text-align: left; border: none; padding: 1px;width:40%'>"
				+ anioF + "</td>" + "</tr>" + "</table>";

		tablaEncabezado3 = "<table id='meta' style='margin-top: 0px; width: 100%; float: right; border:none;font-family: MS Serif, New York, serif;font-size: 11px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 1px;width:8%'></td>"
				+ "<td style='text-align: left; border:none; padding: 1px;width:92%'>"
				+ direccion.substring(0, 80)
				+ "</td>"
				+ "</tr>"
				+ "</table>"
				+ "</div>";
		tablaTotal = "<table id='total' style=' margin: 0px 0 0 0; font-family: MS Serif, New York, serif;font-size: 11px; width: 100%;'>"
				+ "<tr>"
				+ "<th style='border: none; padding: 1px; width:67%'></th>"
				+ "<th style='border: none; padding: 1px; width:33%; text-align:left'>"
				+ this.txtTotalRet.getValue().toString()
				+ "</th>"
				+ "</tr>"
				+ "</table>" + "</div>" + "</body>" + "</html>";

		ajaxDefinition = tablaEncabezado1 + tablaEncabezado2 + tablaEncabezado3
				+ tablaItems + tablaItemsVacia + tablaTotal;
		canvas.setContents(ajaxDefinition);

		canvas.showPrintPreview(canvas);
	}

	public void imprimirRetencionGiga() {
		//numtransaccion, anio, baseimponible, impuesto, codigo, porcentaje, valorretenido, total
		String tablaEncabezado1 = "";
		String tablaEncabezado2 = "";
		String tablaEncabezado3 = "";
		String tablaItems = "";
		String tablaItemsVacia = "";
		String tablaTotal = "";
		String ajaxDefinition = "";

		int i = 0;
		int count = 0;
		
		Iterator ret = tblRetenciones.iterator();
		//SC.say(String.valueOf(ret.hasNext()));
		RetencionDTO retencion = new RetencionDTO();
		String impuesto = "";
		Double porciento = 0.0;
		dateFecha_emision
				.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_emision.setMaskDateSeparator("-");
		dateFecha_emision
				.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_emision
				.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		String fecha = dateFecha_emision.getDisplayValue();
		fecha.replace("00:00", " ");
		String[] anioFiscal = fecha.split("/");
		String anioF = anioFiscal[0];
		String mesF = anioFiscal[1];
		String diaF = anioFiscal[2];
		
		String Mes[] = { "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO",
				"JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE",
				"DICIEMBRE" };
		int mesEntero = Integer.valueOf(mesF);
		String fechaLetras = diaF + " DE " + Mes[mesEntero - 1] + " DE "
				+ anioF;
		
		String cabecera = "";
		String direccion="";
		//SC.say(String.valueOf(NumDto));
		if (NumDto==20){
			direccion = this.txtDireccion2.getValue().toString();
		}else{
			direccion = this.txtDireccion.getValue().toString();
		}
		
		tablaItems = tablaItems
				+ "<table id='items' style=' margin: 1px 0 0 0; margin-top: 39px;  width: 100%;font-family: MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>"
				+ "<th style='border: none; padding: 1px; width:8%'></th>" + //
				"<th style='border: none; padding: 1px; width:12%'></th>" + // anio
				"<th style='border: none; padding: 1px; width:20%'></th>" + // base
				"<th style='border: none; padding: 1px; width:15%'></th>" + // impuesto
				"<th style='border: none; padding: 1px; width:10%'></th>" + // codigo
				"<th style='border: none; padding: 1px; width:21%'></th>" + // %
				"<th style='border: none; padding: 1px; width:24%'></th>" + // valor
																			// retenido
				"</tr>";
		//SC.say(direccion+" 2tb "+tablaItems);
		LinkedList<JSONObject> detallesJson3 = new LinkedList<JSONObject>();
		JSONObject detalleJSON = new JSONObject();
		//SC.say(direccion+" 3tb "+tablaItems);
		while (ret.hasNext()) {
			
			retencion = new RetencionDTO();
			retencion = (RetencionDTO) ret.next();
			count++;
			if (retencion.getImpuesto().getTipo() == 1) {
				impuesto = "RENTA";
			} else {
				impuesto = "IVA";
			}
			//SC.say(impuesto);
			/*
			 * impuesto=retencion.getImpuesto().getNombre();
			 * if(impuesto.contains("RENTA")) {impuesto="RENTA";} else
			 * if(impuesto.contains("IVA")) {impuesto = "IVA";}
			 */
//			porciento = (retencion.getValorRetenido() * 100)
//					/ retencion.getBaseImponible();
//			porciento = CValidarDato.getDecimal(0, porciento);
			porciento = CValidarDato.getDecimal(2, retencion.getImpuesto().getRetencion());
			//SC.say(String.valueOf(porciento));
			// String baseImponible2="";
			// baseImponible2=String.valueOf(CValidarDato.getDecimal(2,
			// retencion.getBaseImponible()));
			// double baseImponible3 = Integer.valueOf(baseImponible2);
			NumberFormat formato = NumberFormat.getFormat("###0.00");
			Double baseImponible2 = CValidarDato.getDecimal(2,
					retencion.getBaseImponible());
			String baseImponible3 = formato.format(baseImponible2);
			//SC.say(baseImponible3);
			Double valorRetenido2 = CValidarDato.getDecimal(2,
					retencion.getValorRetenido());
			String valorRetenido3 = formato.format(valorRetenido2);
			//SC.say(valorRetenido3);
			tablaItems = tablaItems
					+

					"<tr class='item-row' style='border: 0; vertical-align: top;'>"
					+

					"<td class='item-name' style='border: none; padding: 1px;text-align:left'></td>"
					+ "<td class='description' style='border: none; padding: 1px; text-align:left'>"
					+ anioF
					+ "</td>"
					+ "<td class='description' style='border: none; padding: 1px; text-align:left'>$&nbsp;"
					+ baseImponible3
					+ "</td>"
					+ "<td style='border: none; padding: 1px;text-align:left'>"
					+ impuesto
					+ "</td>"
					+ "<td style='border: none; padding: 1px; text-align:left'>"
					+ retencion.getImpuesto().getCodigo()
					+ "</td>"
					+ "<td style='border: none; padding: 1px;text-align:left'>"
					+ porciento.toString()
					+ "&nbsp;%</td>"
					+ "<td style='border: none; padding: 1px; text-align:left'>$&nbsp;"
					+ valorRetenido3 + "</td>" + "</tr>";
			
			detalleJSON = new JSONObject();

			detalleJSON.put("baseimponible",new JSONString(baseImponible3));
			detalleJSON.put("impuesto",	new JSONString(impuesto));
			detalleJSON.put("codigo",new JSONString(String.valueOf(retencion.getImpuesto().getCodigo())));
			detalleJSON.put("porcentaje",new JSONString(porciento.toString()));
			detalleJSON.put("valorretenido",new JSONString(valorRetenido3));
			detallesJson3.add(detalleJSON);
			
		}
		tablaItems = tablaItems + "</table>";
		
		String numeracion = "";

		tablaItemsVacia = "<table id='itemsVacia' style=' margin: 1px 0 0 0; margin-top: 6px;  width: 100%;font-family: MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>";
		for (i = count; i < 9; i++) {
			tablaItemsVacia = tablaItemsVacia + "<tr>"
					+ "<th style='border: none; '>&nbsp;</th>"
					+ "<th style='border: none;'></th>"
					+ "<th style='border: none;'></th>"
					+ "<th style='border: none;'></th>"
					+ "<th style='border: none;'></th>"
					+ "<th style='border: none;'></th>" + "</tr>";
		}
		// }
		tablaItemsVacia = tablaItemsVacia + "</table>";

		//SC.say("Despues items vacios");
		tablaEncabezado1 = "<html>"
				+ "<body>"
				+ "<div id='page-wrap'style='width: 800px; margin: 0 auto;'>"
				+ "<div id='customer' style='overflow:hidden;'>"
				+ "<table id='meta' style='margin-top: 25px; width: 100%; float: right; border:none;MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px; width:12%'></td>"
				+ "<td style='text-align: left; border: none; padding: 2px; width:53%'>"
				+ this.txtCLiente.getDisplayValue().toString()
				+ "</td>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px;width:10%'></td>"
				+ "<td style='text-align: left; border: none; padding: 2px;width:25%'>"
				+ fechaLetras.toString() + "</td>" + "</tr>" + "</table>";
		tablaEncabezado2 = "<table id='meta' style='margin-top: 0px; width: 100%; border:none;MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px;width:12%'></td>"
				+ "<td style='text-align: left; border:none; padding: 2px;width:53%'>"
				+ this.txtRuc_cliente.getDisplayValue().toString()
				+ "</td>"
				+ "<td class='meta-head' style='text-align: right; border: none; padding: 2px;width:16%'></td>"
				+ "<td style='text-align: left; border: none; padding: 2px;width:19%'>Factura</td>"
				+ "</tr>" + "</table>";

		tablaEncabezado3 = "<table id='meta' style='margin-top: 0px; width: 100%; float: right; border:none;MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px;width:12%'></td>"
				+ "<td style='text-align: left; border:none; padding: 2px;width:53%'>"
				+ direccion.substring(0, 80)
				+ "</td>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px;width:15%'></td>"
				+ "<td class='meta-head' style='text-align: left; border: none; padding: 2px;width:20%'>"
				+ this.txtNumeroC.getDisplayValue().toString()
				+ "</td>"
				+ "</tr>" + "</table>" + "</div>";
		NumberFormat formato = NumberFormat.getFormat("#,##0.00");
		Double total2 = CValidarDato.getDecimal(2,
				Double.valueOf(this.txtTotalRet.getValue().toString()));
		String total3 = formato.format(total2);
		tablaTotal = "<table id='total' style=' margin: 0px 0 0 0; width: 100%;MS Serif, New York, serif;font-size: 14px;'>"
				+ "<tr>"
				+ "<th style='border: none; padding: 1px; width:91%'></th>"
				+ "<th style='border: none; padding: 1px; width:9%; text-align:left'>$ &nbsp;"
				+ total3
				+ "</th>"
				+ "</tr>"
				+ "</table>"
				+ "</div>"
				+ "</body>" + "</html>";

		ajaxDefinition = tablaEncabezado1 + tablaEncabezado2 + tablaEncabezado3
				+ tablaItems + tablaItemsVacia + tablaTotal;
		/*canvas.setContents(ajaxDefinition);
		canvas.setWidth100();
		canvas.setHeight100();*/
		
		String banderaImpresion=Factum.banderaIpImpresion;
		String archivoPHP =Factum.banderaImprecionPHPRetencion;
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				"http://"+banderaImpresion+"/IMPRESION/"+archivoPHP+"?fecha=" + fecha 
				+ "&anio="+anioF
						+ "&ruc=" + this.txtRuc_cliente.getDisplayValue().toString() 
						+ "&cliente=" + this.txtCLiente.getDisplayValue().toString() 
						+ "&direccion=" + direccion + "&total=" + total3
						+ "&TipoComprobante=Factura"
						+ "&numtransaccion=" +this.txtNumeroC.getDisplayValue().toString() +"&detalles=" + detallesJson3);
		try {
			
			rb.setHeader("Access-Control-Allow-Origin", "*");
		    //rb.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
		    rb.setHeader("Access-Control-Max-Age", "3600");
		    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
			rb.sendRequest(null, new RequestCallback() {
				public void onError(
						final com.google.gwt.http.client.Request request,
						final Throwable exception) {
					// Window.alert(exception.getMessage());
					SC.say("Error al imprimir verifique el estado de la impresora: "
							+ exception);
				}

				public void onResponseReceived(
						final com.google.gwt.http.client.Request request,
						final Response response) {
					// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
					// LA GRILLA
					
					//response.getText();
					//grdProductos.startEditing(0, 2, false);
					
					JSONValue val= JSONParser.parseStrict(response.getText());
					canvas.setContents(val.toString().replace("\\/", "/"));
					canvas.redraw();
					canvas.setWidth100();
					canvas.setHeight100();
					//getService().impresion(val.toString(), callbackImpresion);
					showPrintPreview(canvas);
					//response.
				}

			});
		} catch (final Exception e) {
			SC.say("Error en impresion ret"+e);
		}
		//showPrintPreview(canvas);
	}
	final AsyncCallback<String> callbackImpresion= new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}

		public void onSuccess(String result) {
			SC.say(result);
		}
	};
	final AsyncCallback<List<CuentaDTO>> listaCuentas = new AsyncCallback<List<CuentaDTO>>() {
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}

		public void onSuccess(List<CuentaDTO> result) {

			MapCategoria.clear();
			MapCategoria.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapCategoria.put(String.valueOf(result.get(i).getIdCuenta()),
						result.get(i).getNombreCuenta());
			}

			cmbTipoPrecio.setValueMap(MapCategoria);
			if (NumDto != 7) {
				cmbTipoPrecio.setDefaultToFirstOption(true);
			}

			cmbTipoPrecio.redraw();
			// registros=result;
			//SC.say("Numero de registros "+result.size());
		}
	};

	final AsyncCallback<List<PosicionDTO>> listback = new AsyncCallback<List<PosicionDTO>>() {
		public void onFailure(Throwable caught) {
			SC.say("Error no existen datos:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
		
		

		public void onSuccess(List<PosicionDTO> result) {
			listpos = (LinkedList<PosicionDTO>) result;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
	};
	
	
	final AsyncCallback<PersonaDTO> retornoper = new AsyncCallback<PersonaDTO>() {
		public void onFailure(Throwable caught) {
			SC.say("Error no existe persona:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}
		
		

		public void onSuccess(PersonaDTO per) {
			persona=per;
			codigoSeleccionado = per.getIdPersona();
//			txtCLiente.setValue(per.getNombreComercial()+" "+per.getRazonSocial());
			txtCLiente.setValue(per.getRazonSocial());
			txtDireccion.setValue(per.getDireccion());
			txtCorreo.setValue(per.getMail());
			txtRuc_cliente.setValue(per.getCedulaRuc());
			txtTelefono.setValue(per.getTelefono1());
		
			getService().ReprotePagosVencidosCliente(
					String.valueOf(codigoSeleccionado), objback);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			}
	};

	final AsyncCallback<Date> callbackFecha = new AsyncCallback<Date>() {
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");

		}

		public void onSuccess(Date result) {
			fechaFactura = result;
			
			dateFecha_emision.setValue(fechaFactura);
		}
	};

	/*
	 * final AsyncCallback<List<PersonaDTO>> objbacklstEmpleado=new
	 * AsyncCallback<List<PersonaDTO>>(){ public void onFailure(Throwable
	 * caught) { SC.say(caught.toString());
	 * DOM.setStyleAttribute(RootPanel.get("cargando"
	 * ).getElement(),"display","none"); } public void
	 * onSuccess(List<PersonaDTO> result) { MapEmpleados.clear();
	 * 
	 * //ListGridRecord[] listado = new ListGridRecord[result.size()]; for(int
	 * i=0;i<result.size();i++) { if(result.get(i).getEstado()=='1') {
	 * MapEmpleados.put(result.get(i).getIdPersona().toString(),
	 * result.get(i).getNombres()+" "+result.get(i).getApellidos());
	 * //listado[i]=(new PersonaRecords((PersonaDTO)result.get(i))); } }
	 * idVendedor=result.get(0).getIdPersona();
	 * cmbVendedor.setValueMap(MapEmpleados); cmbVendedor.redraw();
	 * DOM.setStyleAttribute
	 * (RootPanel.get("cargando").getElement(),"display","none"); } };
	 */
	final AsyncCallback<String> objbacklstBorrar = new AsyncCallback<String>() {
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			SC.say(result);
			bloqueo(true);
			btnImprimir.setDisabled(true);
			btnGrabar.setDisabled(true);
			btnEliminar.setDisabled(true);
		}
	};

	public void borrarDocumento() {
		// AQUI VAMOS A BORRAR EL DOCUMENTO SIEMPRE Y CUANDO NO ESTE CONFIRMADO
		SC.ask("DESEA REALMENTE ELIMINAR", new BooleanCallback() {
			public void execute(Boolean value) {
				if (value != null && value) {
					// En caso de selecciona YES
					getService().eliminarDocSinConfirmar(DocBase,
							objbacklstBorrar);
				} else {
				}
			}
		});

	}

	public void imprimirFacturaVentaMini() {
		String encabezado = "";
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
		encabezado = "</br></br></br>" + "<table class='estilo1'>" + "<tr>"
				+ "</tr>" + "<tr>" + "	<td width='75%'>"
				+ this.dateFecha_emision.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "	<td><div align='center'>RUC:"
				+ txtRuc_cliente.getDisplayValue()
				+ "</div></td>"
				+ "</tr>"
				+ "<tr>"
				+ "	<td>Nombre"
				+ this.txtCLiente.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "   <td width='80%'>Dir:"
				+ this.txtDireccion.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "   <td><div align='right'>"
				+ this.txtTelefono.getDisplayValue()
				+ "</div></td>"
				+ "</tr>"
				+ "</table>";
		// FIN ENCABEZADO
		// DETALLE
		String detalle = "</br><table class='estilo2'>";

		int count = 0;
		for (ListGridRecord rec : selectedRecords) {
			count++;// 15-52-11-12-10
			detalle = detalle
					+ "<tr><td width='15%'><div align='left'>&nbsp&nbsp"
					+ rec.getAttribute("cantidad")
					+ "</div></td>"
					+ "<td width='65%'><div align='left'>"
					+ rec.getAttribute("descripcion")
					+ "</div></td>"
					+ "<td width='10%'><div align='right'>"
					+ NumberFormat.getFormat("00.00").format(
							rec.getAttributeAsDouble("precioUnitario"))
					+ "</div></td>"
					+ "<td width='11%'><div align='right'>"
					+ NumberFormat.getFormat("00.00").format(
							rec.getAttributeAsDouble("valorTotal"))
					+ "</div></td>"
					+ "<td width='4%'><div align='center'>&nbsp;</div></td></tr>";
		}

		detalle = detalle + "</table>";
		while (count < 13) {
			detalle = detalle + "</br>";
			count++;
		}
		// FIN DETALLE
		// TOTALES
		String totales = "</br>";
		totales = "<table width='100%' border='0' class='estilo1'>";
		totales = totales + "<tr>"
				+ "<td width='90%'><div align='right'>Sub. Tot."
				+ txtSubtotal.getValue() + "</div></td>" + "</tr>" + "<tr>"
				+ "<td width='90%'><div align='right'>Iva"
				+ txtValor14_iva.getValue() + "</div></td>" + "</tr>" + "<tr>"
				+ "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'><strong>Total:"
				+ txtTotal.getValue() + "</strong></div></td>" + "</tr>"
				+ "<tr>" + "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'><strong>EFECTIVO:"
				+ txtEfectivo.getValue() + "</strong></div></td>" + "</tr>"
				+ "<tr>" + "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'><strong>CAMBIO:"
				+ txtCambio.getValue() + "</strong></div></td>" + "</tr>";
		totales = totales + "</table>";

		String ajaxDefinition = "	</head>" + "<style type='text/css'>"
				+ "	<!--" + "	.estilo1 {" +

				"	font-family: Verdana, Geneva, sans-serif;"
				+ "	font-size: 7px;" + "	}" + ".estilo2 {" +

				"	font-family: Verdana, Geneva, sans-serif;"
				+ "	font-size: 7px;" + "	}" + "	-->" + "</style>" + "<body>	"
				+ encabezado + detalle + totales + "<p>&nbsp;</p>" + "</body>"
				+ "</html>";
		canvas.setContents(ajaxDefinition);
		canvas.showPrintPreview(canvas);

		limpiarInterfaz();

	}

	public void imprimirFacturaVenta() {
		String encabezado = "";
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
		encabezado = "" + "<table class='estilo1'>" + "<tr>"
				+ "	<td width='75%'>Fecha:"
				+ this.dateFecha_emision.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "	<td>Cliente:"
				+ this.txtCLiente.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "	<td width='90%'><div >Ced/RUC:"
				+ txtRuc_cliente.getDisplayValue()
				+ "</div></td>"
				+ "   <td><div align='right'>Telef:"
				+ this.txtTelefono.getDisplayValue()
				+ "</div></td>"
				+ "</tr>"
				+ "<tr>"
				+ "   <td width='80%'>Dir:"
				+ this.txtDireccion.getDisplayValue()
				+ "</td>"
				+ "</tr>"
				+ "<tr>" + "</tr>" + "</table>";
		// FIN ENCABEZADO
		// DETALLE
		String detalle = "</br><table class='estilo2'>";

		int count = 0;
		for (ListGridRecord rec : selectedRecords) {
			count++;// 15-52-11-12-10
			detalle = detalle
					+ "<tr><td width='7%'><div align='left'>"
					+ rec.getAttribute("cantidad")
					+ "</div></td>"
					+ "<td width='60%'><div align='left'>"
					+ rec.getAttribute("descripcion")
					+ "</div></td>"
					+ "<td width='15%'><div align='left'>"
					+ NumberFormat.getFormat("00.00").format(
							rec.getAttributeAsDouble("precioUnitario"))
					+ "</div></td>"
					+ "<td width='15%'><div align='right'>"
					+ NumberFormat.getFormat("00.00").format(
							rec.getAttributeAsDouble("valorTotal"))
					+ "</div></td>";
		}

		detalle = detalle + "</table>";
		while (count < 8) {
			detalle = detalle + "</br>";
			count++;
		}
		// FIN DETALLE
		// TOTALES
		String totales = "";
		totales = "<table width='100%' border='0' class='estilo1'>";
		totales = totales + "<tr>" + "</tr>" + "<tr>" + "</tr>" + "<tr>"
				+ "</tr>" + "<tr>" + "</tr>" + "<tr>"
				+ "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'>Desc: 0.00</div></td>" +

				"</tr>" + "<tr>" + "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'>Sub Tot: "
				+ txtSubtotal.getValue() + "</div></td>" + "</tr>" + "<tr>"
				+ "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'>IVA Tar 0%: "
				+ this.txt0_iva.getValue() + "</div></td>" + "</tr>" + "<tr>"
				+ "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'>IVA Tar "+Factum.banderaIVA+"%: "
				+ txtValor14_iva.getValue() + "</div></td>" + "</tr>" + "<tr>"
				+ "<td width='10%'>&nbsp;</td>"
				+ "<td width='90%'><div align='right'><strong>TOTAL: "
				+ txtTotal.getValue() + "</strong></div></td>" +

				"</tr>";
		totales = totales + "</table>";

		String ajaxDefinition = "	</head>" + "<style type='text/css'>"
				+ "	<!--" + "	.estilo1 {" +

				"	font-family: Verdana, Geneva, sans-serif;"
				+ "	font-size: 8px;" + "	}" + ".estilo2 {" +

				"	font-family: Verdana, Geneva, sans-serif;"
				+ "	font-size: 8px;" + "	}" + "	-->" + "</style>" + "<body>	"
				+ encabezado + detalle + totales + "<p>&nbsp;</p>" + "</body>"
				+ "</html>";
		canvas.setContents(ajaxDefinition);
		showPrintPreview(canvas);
	}

	public void imprimir() {
		////com.google.gwt.user.client.Window.alert("si es "+NumDto);
		// REVISAR PARA IMPRIMIR UN GASTO
		String encabezado = "";
		
		//SC.say("ESTADO 1");
		ListGridRecord[] selectedRecords = grdProductos.getRecords();
		//SC.say("ESTADO 2");
		int i = 0;
		int count = 0;

		if (NumDto != 20 && NumDto != 4) {
			VerificarFormasPago();
			String total = "";
			String des = "<table width='100%' border='0' class='estilo1'>";
			//SC.say("primIf");
			if (NumDto != 7 && NumDto != 12)// si es diferente de
															// 7 porque el gasto
															// no tiene este
															// detalle
			{
				des = des
						+ "<tr><td width='15%'><div align='center'><strong>CANTIDAD</strong></div></td>"
						+ "<td width='60%'><div align='center'><strong>DESCRIPCION</strong></div></td>"
						+ "<td width='10%'><div align='center'><strong>P. UNITARIO</strong></div></td>"
						+ "<td width='10%'><div align='center'><strong>P. TOTAL</strong></div></td>"
						+ "<td width='4%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
			}
			//SC.say("primIfEnd");
			if (NumDto == 7) {
				analizarTipoDto(NumDto);
				des = des
						+ "<tr><td width='25%'><div align='center'><strong>DESCRIPCION DEL GASTO: </strong></div></td>"
						+ "<td width='70%'><div align='left'><strong>"/*
																	 * +
																	 * txtAutorizacion_sri
																	 * .
																	 * getDisplayValue
																	 * (
																	 * )+" MAS "
																	 * +
																	 * auth+" MAS "
																	 */
						+ txtaConcepto.getDisplayValue()
						+ "</strong></div></td>";
			}
			if (NumDto == 12) {
				analizarTipoDto(NumDto);
				des = des
						+ "<tr><td width='25%'><div align='center'><strong>DESCRIPCION DEL INGRESO: </strong></div></td>"
						+ "<td width='70%'><div align='left'><strong>"/*
																	 * +
																	 * txtAutorizacion_sri
																	 * .
																	 * getDisplayValue
																	 * (
																	 * )+" MAS "
																	 * +
																	 * auth+" MAS "
																	 */
						+ txtaConcepto.getDisplayValue()
						+ "</strong></div></td>";
			}
			//SC.say("ESTADO 3");
			for (ListGridRecord rec : selectedRecords) {
				count++;// 15-52-11-12-10
				String precioUnitario=formatoDecimalN.format(CValidarDato.getDecimal(4, rec.getAttributeAsDouble("precioUnitario")));
				//String precioUnitario=rec.getAttribute("precioUnitario");
				int indiceprecioUnitario= precioUnitario.indexOf(".");
				String precioTotal=formatoDecimalN.format(CValidarDato.getDecimal(4,rec.getAttributeAsDouble("valorTotal")));
				//String precioTotal=rec.getAttribute("valorTotal");
				int indiceprecioTotal= precioTotal.indexOf(".");
				if(indiceprecioUnitario==-1){
					precioUnitario=precioUnitario+".00";
				}else{	
					if(precioUnitario.substring(indiceprecioUnitario, precioUnitario.length()).length()<Factum.banderaNumeroDecimales+1){
						while(precioUnitario.substring(indiceprecioUnitario, precioUnitario.length()).length()<Factum.banderaNumeroDecimales+1){
							precioUnitario=precioUnitario+"0";
						}
					}else{
						precioUnitario=precioUnitario.substring(0, indiceprecioUnitario+Factum.banderaNumeroDecimales+1);
					}
				}
				if(indiceprecioTotal==-1){
					precioTotal=precioTotal+".00";
				}else{
					if(precioTotal.substring(indiceprecioTotal, precioTotal.length()).length()<Factum.banderaNumeroDecimales+1){
						while(precioUnitario.substring(indiceprecioUnitario, precioUnitario.length()).length()<Factum.banderaNumeroDecimales+1){
							precioTotal=precioTotal+"0";
						}
					}else{
						precioTotal=precioTotal.substring(0, indiceprecioTotal+Factum.banderaNumeroDecimales+1);
					}					

				}
					
				des = des
						+ "<tr><td width='15%'><div align='center'><strong>&nbsp&nbsp"
						+ rec.getAttribute("cantidad")
						+ "</strong></div></td>"
						+ "<td width='60%'><div align='left'><strong>"
						+ rec.getAttribute("descripcion")
						+ "</strong></div></td>"
						+ "<td width='10%'><div align='right'><strong>"
						+ precioUnitario
						+ "</strong></div></td>"
						+ "<td width='11%'><div align='right'><strong>"
						+ precioTotal
						+ "</strong></div></td>"
						+ "<td width='4%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
			}
			//SC.say("ESTADO 4");
			String numeracion = "";
			String encabezadoEmpresa = "";
			String encabezadoDireccionTelefono = "";

			if (NumDto != 1) {
				String leyendadto="";
				if(NumDto==0){
					leyendadto="FACTURA 001-001-";	
				}else if(NumDto==3){
					leyendadto="NOTA DE CREDITO 001-001-";
				}else if(NumDto==26){
					leyendadto="NOTA DE VENTA 001-001-";
				}
				encabezadoEmpresa= "<tr>" + "<td colspan='4'><div align='center'><strong>"+
				Factum.banderaNombreEmpresa
				+ "</strong></div></td>" + "</tr>";
				encabezadoDireccionTelefono= "<tr>" + "<td colspan='4'><div align='center'><strong>"
				+Factum.banderaDireccionEmpresa+" "+Factum.banderaTelefonoCelularEmpresa+"/"+Factum.banderaTelefonoConvencionalEmpresa
				+ "</strong></div></td>" + "</tr>";
				numeracion = "<tr>" + "<td colspan='4'><div align='center'>"
						+leyendadto+ serie + txtNumero_factura.getDisplayValue()
						+ "</div></td>" + "</tr>";
			}
			/*if (NumDto == 2)// En caso de ser una nota de entrega
			{

				if (count < Factum.banderaNumeroItemsNotaEntrega) {
					for (i = count; i < Factum.banderaNumeroItemsNotaEntrega; i++) {
						des = des
								+ "<tr><td width='15%'><div align='center'><strong>&nbsp</strong></div></td>"
								+ "<td width='60%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='10%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='11%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='4%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
					}
				}
			}else if (NumDto == 3)// En caso de ser una nota de entrega
			{

				if (count < Factum.banderaNumeroItemsNotaCredito) {
					for (i = count; i < Factum.banderaNumeroItemsNotaCredito; i++) {
						des = des
								+ "<tr><td width='15%'><div align='center'><strong>&nbsp</strong></div></td>"
								+ "<td width='60%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='10%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='11%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='4%'><div align='center'><strong>&nbsp;</strong></div></td></tr>";
					}
				}
			}
			else {
				if (count < Factum.banderaNumeroItemsFactura) {
					for (i = count; i < Factum.banderaNumeroItemsFactura; i++) {
						des = des
								+ "<tr><td width='15%'><div align='center'><strong>&nbsp</strong></div></td>"
								+ "<td width='60%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='10%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='11%'><div align='right'><strong>&nbsp</strong></div></td>"
								+ "<td width='4%'><div align='right'><strong>&nbsp</strong></div></td></tr>";

					}
				}
			}*/
			des = des + "<td width='4%'><div align='right'><strong>&nbsp</strong></div></td></tr>"+ "</table>";
			if (NumDto == 2 || NumDto == 26)// En caso de ser una nota de entrega
			{
				total = "<table width='100%' border='0' class='estilo1'>";
				total = total + "" + "<tr><td width='83%'><div align='right'>"
						+ "TOTAL" + "</div></td>"
						+ "<td width='17%'><div align='right'><strong>"
						+ txtTotal.getValue() + "</strong></div></td></tr>"
						+ "";
				total = total + "</table>";
			} else {
				// ESTO MUESTRA LA SECCION DERECHA DE TOTAL DE DINERO: SUBTOTAL,
				// IVA, TOTAL
				//this.dateFecha_emision.getva
				if(NumDto==0){ 
				total="<table width='100%' border='0' class='estilo1'>";
				total=total+"<tr><td width='4%'>&nbsp;</td>"+
				"<td><div align='left'>"+lblPie_pagina.getText()+"</div></td>"+
			    "<td width='14%'><div align='right'><strong>SUBTOTAL: "+ txtSubtotal.getValue()+"</strong></div></td>" +
			    "<td width='4%'>&nbsp;</td></tr>"+
			    
			    "<tr><td width='4%'>&nbsp;</td><td><div align='right'>"+lblTotal_letras.getText()+"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></td>"+
			    //"<td><div align='right'>14&nbsp&nbsp&nbsp</div></td>"+
			    "<td><div align='right'><strong>"+Factum.banderaIVA+"&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIVA: "+txtValor14_iva.getValue()+"</strong></div></td><td>&nbsp;</td></tr>"+
			    "<tr><td>&nbsp;</td><td>&nbsp;</td><td><div align='right'><strong>TOTAL: "+txtTotal.getValue()+"</strong></div></td><td>&nbsp;</td></tr>";
				total=total+"</table>";	
				}else{
					total="<table width='100%' border='0' class='estilo1'>";
					total=total+"<tr><td width='78%'>&nbsp;</td>"+
					"<td width='4%'>&nbsp;</td>"+
				    "<td width='14%'><div align='right'><strong>"+ txtSubtotal.getValue()+"</strong></div></td>" +
				    		"<td width='4%'>&nbsp;</td></tr>"+
				    
				    "<tr><td><div align='right'>"+lblTotal_letras.getText()+"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></td>"+
					
				    "<td><div align='right'>"+Factum.banderaIVA+"&nbsp&nbsp&nbsp</div></td>"+
				    "<td><div align='right'><strong>"+txtValor14_iva.getValue()+"</strong></div></td><td>&nbsp;</td></tr>"+
				    "<tr><td>&nbsp;</td><td>&nbsp;</td><td><div align='right'><strong>"+txtTotal.getValue()+"</strong></div></td><td>&nbsp;</td></tr>";
					total=total+"</table>";	
				}
			}

			dateFecha_emision
					.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			dateFecha_emision.setMaskDateSeparator("-");
			dateFecha_emision
					.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			dateFecha_emision
					.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			String fecha = dateFecha_emision.getDisplayValue();
			fecha.replace("00:00", " ");
			//SC.say(fecha+"   "+dateFecha_emision.getValue());			
			encabezado = "<table width='100%' border='0' class='estilo1'>"
					+ encabezado
					+ encabezadoEmpresa
					+ encabezadoDireccionTelefono
					+ numeracion+
				    " <tr>"+
					"	<td width='60%'><strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFECHA: "+fecha+"</strong></td>"+
				    "	<td width='10%'>&nbsp;</td>"+
				    " 	<td width='30%'>" +
				    "   <div align='center'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspVENDEDOR: "+this.cmbVendedor.getValue()+"</div></td>"+
				    "   </tr>"+
				    
					"<tr>"+
					    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspNOMBRE: "+this.txtCLiente.getValue()+"</td>"+
					    "<td>&nbsp;</td>"+
					    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIDENTIFICACION: "+this.txtRuc_cliente.getValue()+"</td>"+
					"</tr>"+
					
					"<tr>"+
					    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspDIRECCION: "+this.txtDireccion.getValue()+"</td>"+
					    "<td>&nbsp;</td>"+
					    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspTELEFONO: "+this.txtTelefono.getValue()+"</td>"+
					"</tr>"+
					"<tr>"+
				    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>"+
				    "<td>&nbsp;</td>"+
				    "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFORMA PAGO: "+this.FormasPago+"</td>"+
				    "</tr>"+
					"</table>";
			if (NumDto == 2) {
				encabezado = "<table width='100%' border='0' class='estilo1'>"
						+ numeracion
						+ " <tr>"
						+ "<td width='5%'>Fecha:</td>"
						+ "	<td width='60%'><strong>&nbsp&nbsp&nbsp"
						+ fecha
						+ "</strong></td>"
						+ "	<td width='10%'>&nbsp;</td>"
						+ "<td width='5%'>Vendedor:</td>"
						+

						" 	<td width='30%'><div align='center'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
						+ this.cmbVendedor.getValue()
						+ "</div></td>"
						+ "   </tr>"
						+ "<tr>"
						+ "<td width='5%'>Nombre:</td>"
						+

						"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
						+ this.txtCLiente.getValue()
						+ "</td>"
						+ "<td>&nbsp;</td>"
						+ "<td width='5%'>RUC/CI:</td>"
						+

						"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
						+ this.txtRuc_cliente.getValue()
						+ "</td>"
						+ "</tr>"
						+ "<tr>"
						+ "<td width='5%'>Direccion:</td>"
						+

						"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
						+ this.txtDireccion.getValue()
						+ "</td>"
						+ "<td>&nbsp;</td>"
						+ "<td width='5%'>Telefono:</td>"
						+

						"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
						+ this.txtTelefono.getValue() + "</td>" + "</tr>"
						+ "<tr>" + "<td>&nbsp;</td>" + "<td>&nbsp;</td>"
						+ "<td>&nbsp;</td>" + "<td>&nbsp;</td>" + "</tr>"
						+ "</table>";
			}
			String espacio = "";
			if (NumDto != 2) {
				espacio = "<p>&nbsp;</t>";
			}
			String ajaxDefinition = "	</head>" + "<style type='text/css'>"
					+ "	<!--" + "	.estilo1 {" +

					"	font-family: Verdana, Geneva, sans-serif;"
					+ "	font-size: 12px;" + "	}" + "	-->" + "</style>"
					+ "<body>	" + espacio + encabezado + "<p>&nbsp;</p>" + des
					+ total +

					"</body>" + "</html>";
			canvas.setContents(ajaxDefinition);
			showPrintPreview(canvas);
			

		}// fin if = 20 osea si es una proforma y si es igual a 4
		else {
			if (NumDto == 20) {

				System.out.println("condicional");
				dateFecha_emision
						.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
				dateFecha_emision.setMaskDateSeparator("-");
				dateFecha_emision
						.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
				dateFecha_emision
						.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
				String fecha = dateFecha_emision.getDisplayValue();
				fecha.replace("00:00", " ");

				String ajaxDefinition = "";
				String cabecera = "";
				String imagen = "";
				String tablaDatos = "";
				String tablaProductosEncabezado = "";
				String tablaProductosCuerpo = "";
				String atentamente = "";
				String condiciones = "";
				cabecera = "	<head>" + "<title>Proforma Giga-Computers</title>"
						+ "</head>";

				imagen = "<body style='font: 14px/1.4 Georgia, serif;'>"
						+ "<div id='page-wrap' style='width: 800px; margin: 0 auto;'>"
						+ "<div id='logo' style='text-align:center; float: right; position: relative; margin-top: 10px; border: 1px solid #fff; max-width: 1000px; max-height: 220px; overflow: hidden;'>"
						+ "<img id='image' src='images/logoGigaProforma.png' alt='logo' />"
						+ "</div>" +

						"<div style='clear:both'></div>";

				tablaDatos = "<div id='customer' style='overflow:hidden;'>"
						+ "<table id='meta' style='margin-top: 1px; width: 800px; float: right; border-collapse: collapse; font: 13px Helvetica;'>"
						+ "<tr>"
						+ "<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>Proforma #</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ txtNumero_factura.getDisplayValue()
						+ "</td>"
						+ "<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>Cliente</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ this.txtCLiente.getValue().toString()
						+ "</td>"
						+ "</tr>"
						+

						"<tr>"
						+ "<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>Fecha</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ fecha
						+ "<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>Direcci&oacute;n</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ this.txtDireccion.getValue().toString()
						+ "</td>"
						+ "</tr>"
						+

						"<tr>"
						+

						"<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>C&eacute;dula</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ this.txtRuc_cliente.getValue().toString()
						+ "</td>"
						+ "<td class='meta-head' style='text-align: left; background: #eee; border: 1px solid black; padding: 4px;'>Tel&eacute;fono</td>"
						+ "<td style='text-align: right; border: 1px solid black; padding: 4px;'>"
						+ this.txtTelefono.getValue().toString()
						+ "</td>"
						+ "</tr>" + "</table>" + "</div>";

				tablaProductosEncabezado = "<table id='items' style='border-collapse: collapse; border: 1px solid black; margin: 30px 0 0 0; font: 11px Helvetica; width: 800px;'>"
						+

						"<tr>"
						+ "<th style='border: 1px solid black; padding: 4px; background: #eee;'>C&oacute;digo</th>"
						+ "<th style='border: 1px solid black; padding: 4px; background: #eee;'>Descripci&oacute;n</th>"
						+ "<th style='border: 1px solid black; padding: 4px; background: #eee;'>Cantidad</th>"
						+ "<th style='border: 1px solid black; padding: 4px; background: #eee;'>Costo Unitario</th>"
						+ "<th style='border: 1px solid black; padding: 4px; background: #eee;'>Total</th>"
						+ "</tr>";

				int numRegProforma = grdProductos.getRecords().length;
				for (int num = 0; num < numRegProforma; num++) {
					tablaProductosCuerpo = tablaProductosCuerpo
							+ "<tr class='item-row' style='border: 0; vertical-align: top; font: 11px Helvetica;'>"
							+ "<td class='item-name' style='border: 1px solid black; padding: 4px; width: 175px; text-align:center'>"
							+ grdProductos.getRecord(num).getAttribute(
									"codigoBarras")
							+ "</td>"
							+ "<td class='description' style='border: 1px solid black; padding: 4px; width: 300px;text-align:center'>"
							+ grdProductos.getRecord(num).getAttribute(
									"descripcion")
							+ "</td>"
							+ "<td style='border: 1px solid black; padding: 4px;text-align:center'>"
							+ grdProductos.getRecord(num).getAttribute(
									"cantidad")
							+ "</td>"
							+ "<td style='border: 1px solid black; padding: 4px;text-align:right'>"
							+ "$"
							+ formatoDecimalN.format(CValidarDato.getDecimal(4,grdProductos.getRecord(num).getAttributeAsDouble(
									"precioUnitario")))
							+ "</td>"
							+ "<td style='border: 1px solid black; padding: 4px; text-align:right'><span class='price'>"
							+ "$"
							+ formatoDecimalN.format(CValidarDato.getDecimal(4,grdProductos.getRecord(num).getAttributeAsDouble(
									"valorTotal"))) + "</span></td>" + "</tr>";
				}

				tablaProductosCuerpo = tablaProductosCuerpo
						+ "<tr id='hiderow'>"
						+ "<td style='border: 1px solid black; padding: 4px;' colspan='5'>&nbsp;</td>"
						+ "</tr>"
						+

						"<tr>"
						+ "<td colspan='2' class='blank'> </td>"
						+ "<td style='border: 1px solid black; padding: 4px;' colspan='2' class='total-line' style='border-right: 0; text-align: right;'>Subtotal</td>"
						+ "<td class='total-value' style='border: 1px solid black; text-align:right;'><div id='subtotal'>"
						+ "$"
						+ txtSubtotal.getValue().toString()
						+ "</div></td>"
						+ "</tr>"
						+

						"<tr>"
						+ "<td colspan='2' class='blank'> </td>"
						+ "<td style='border: 1px solid black; padding: 4px;' colspan='2' class='total-line'>Iva "+Factum.banderaIVA+"%</td>"
						+ "<td class='total-value' style='border: 1px solid black; text-align:right;'><div id='total'>"
						+ "$"
						+ txtValor14_iva.getValue().toString()
						+ "</div></td>"
						+ "</tr>"
						+

						"<tr>"
						+ "<td colspan='2' class='blank'> </td>"
						+ "<td style='border: 1px solid black; padding: 4px; background: #eee;' colspan='2' class='total-line balance'>Total</td>"
						+ "<td style='border: 1px solid black; padding: 4px;text-align:right; background: #eee;' class='total-value balance'><div class='due'>"
						+ "$" + txtTotal.getValue().toString() + "</div></td>"
						+ "</tr>" +

						"</table>";				

				atentamente = "<div id='terms' style='text-align: center; margin: 15px 0 0 0;'>"
						+ "<h5 style='font: 15px Helvetica, Sans-Serif; letter-spacing: 10px; padding: 0 0 8px 0; margin: 0 0 8px 0; border-bottom: 1px solid black; '><b>ATENTAMENTE</b></h5>"
						+ "</div>"
						+ "<h5 style='font: 11px Helvetica, Sans-Serif;  margin: 0 0 4px 0; text-align:center'>"
						+ this.cmbVendedor.getValue().toString()
						+ "</h5>"
						+ "<h5 style='font: 11px Helvetica, Sans-Serif;  margin: 0 0 4px 0; text-align:center'><b>"+Factum.banderaNombreEmpresa+"</b></h5>"
						+ "<h5 style='font: 11px Helvetica, Sans-Serif;  margin: 0 0 4px 0;text-align:center' >Presidente C&oacute;rdova 8-42 y Luis Cordero Telefax: (593) 7 2821561 Ext: 14 e-mail: jcarmilema@gigacomputers.com.ec</h5>"
						+ "<h5 style='font: 11px Helvetica, Sans-Serif;   margin: 0 0 4px 0;text-align:center'>Cuenca-Ecuador</h5>"
						+ "<p align='center'><a href='http://www.gigacomputers.com.ec/' target='_blank'>www.gigacomputers.com.ec</a></p>";

				condiciones = "<div id='condiciones' style='text-align: center; margin: 15px 0 0 0;'>"
						+ "<h5 style='font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0;'><b>CONDICIONES</b></h5>"
						+ "</div>"
						+ "<TEXTAREA COLS=90 ROWS=5 NAME='Texto' style=' width:800px;border: none'> * Oferta Valida por 15 dias      * Entrega inmediata     * Precios aplican a pagos en efectivo"
						+ "</TEXTAREA>" + "</div>" + "</body>";
				ajaxDefinition = cabecera + imagen + tablaDatos
						+ tablaProductosEncabezado + tablaProductosCuerpo
						+ atentamente + condiciones;
				canvas.setContents(ajaxDefinition);
				showPrintPreview(canvas);

			}// fin if NumDto!=20
			if (NumDto == 4) {
				////com.google.gwt.user.client.Window.alert("si es anticipo");
				String total = "";
				String des = "<table width='100%' border='0' class='estilo1'>";
				String numeracion = "";
				numeracion = "<tr>"
						+ "<td colspan='4'><strong><div align='center'>"
						+ serie + txtNumero_factura.getDisplayValue()
						+ "</strong></div></td>" + "</tr>";
				des = "<tr>"
						+ "<td colspan='4'><strong><div align='center' class='estilo1'>CONCEPTO:</strong></div></td>"
						+ "</tr>"
						+ "<tr>"
						+ "<td colspan='4'>&nbsp</td>"
						+ "</tr>"
						+ "<tr>"
						+ "<td colspan='4'><div align='left' class='estilo1'>&nbsp&nbsp"
						+ txtaConcepto.getDisplayValue() + "</div></td>"
						+ "</tr>";
				// for(i=0;i<2;i++)
				// {
				des = des
						+ "<tr><td width='15%'><div align='center'><strong>&nbsp</strong></div></td>"
						+ "<td width='60%'><div align='right'><strong>&nbsp</strong></div></td>"
						+ "<td width='10%'><div align='right'><strong>&nbsp</strong></div></td>"
						+ "<td width='11%'><div align='right'><strong>&nbsp</strong></div></td>"
						+ "<td width='4%'><div align='right'><strong>&nbsp</strong></div></td></tr>";
				// }
				des = des + "</table>";

				total = "<table width='100%' border='0' class='estilo1'>";
				total = total
						+ "<tr><td width='70%'>&nbsp;</td>"
						+ "<td width='25%'>&nbsp;</td>"
						+ "<td width='5%'>&nbsp;</td></tr>"
						+ "<tr><td colspan='3'>&nbsp;</td></tr>"
						+ "<tr><td>&nbsp;</td><td><div align='right'><strong>TOTAL:&nbsp&nbsp&nbsp"
						+ txtTotal.getValue()
						+ "</strong></div></td><td>&nbsp;</td></tr>";
				total = total + "</table>";

				dateFecha_emision
						.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
				dateFecha_emision.setMaskDateSeparator("-");
				dateFecha_emision
						.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
				dateFecha_emision
						.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
				String fecha = dateFecha_emision.getDisplayValue();
				fecha.replace("00:00", " ");
				encabezado = "<table width='100%' border='0' class='estilo1'>"
						+ numeracion
						+ " <tr>"
						+ "	<td width='60%'>&nbsp&nbsp&nbsp;FECHA:&nbsp&nbsp&nbsp;"
						+ fecha
						+ "</td>"
						+ "	<td width='10%'>&nbsp;</td>"
						+ " 	<td width='30%'><div align='left'>VENDEDOR:&nbsp&nbsp&nbsp;"
						+ this.cmbVendedor.getValue() + "</div></td>"
						+ "   </tr>" +

						"<tr>" + "<td>CLIENTE:&nbsp&nbsp&nbsp;"
						+ this.txtCLiente.getValue() + "</td>"
						+ "<td>&nbsp;</td>" + "<td>RUC:&nbsp&nbsp&nbsp;"
						+ this.txtRuc_cliente.getValue() + "</td>" + "</tr>" +

						"<tr>" + "<td>DIRECCI&Oacute;N:&nbsp&nbsp&nbsp;"
						+ this.txtDireccion.getValue() + "</td>"
						+ "<td>&nbsp;</td>"
						+ "<td>TEL&Eacute;FONO:&nbsp&nbsp&nbsp;"
						+ this.txtTelefono.getValue() + "</td>" + "</tr>" +

						"</table>";
				String espacio = "";
				espacio = "<p>&nbsp;</t>";
				String ajaxDefinition = "	</head>" + "<style type='text/css'>"
						+ "	<!--" + "	.estilo1 {" +

						"	font-family: Verdana, Geneva, sans-serif;"
						+ "	font-size: 12px;" + "	}" + "	-->" + "</style>"
						+ "<body>	" + espacio + encabezado + "<p>&nbsp;</p>"
						+ des + total + "</body>" + "</html>";
				canvas.setContents(ajaxDefinition);
				showPrintPreview(canvas);
				
			} // fin if NumDto=4
		}

	}

	public void asignarPuntos(final int signo) {
		// signo=1;//positivo sumo los puntos
		// signo=-1;//negativo resto los puntos
		winClientes = new Window();
		txtPersonaAsignada = new TextItem("txtPersonaAsignada", "Cliente");
		txtPuntosAsignados = new TextItem("txtPuntosAsignados", "# Puntos");
		winClientes.setWidth(930);
		winClientes.setHeight(610);
		winClientes.setTitle("Asignar Puntos");
		winClientes.setShowMinimizeButton(false);
		winClientes.setIsModal(true);
		winClientes.setShowModalMask(true);
		winClientes.setKeepInParentRect(true);
		winClientes.centerInPage();
		winClientes.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				if (txtPersonaAsignada.getValueField().toString() == "") {
					codigoSeleccionadoPuntos = 0;
					puntosAcreditados = 0;
				} else {
					try {
						puntosAcreditados = Integer.valueOf(txtPuntosAsignados
								.getValue().toString()) * signo;
						personaAsignada = String.valueOf(txtPersonaAsignada
								.getValue().toString());
					} catch (Exception e) {
						// SC.say("error 1");
					}
				}
				winClientes.destroy();
			}
		});
		VLayout form = new VLayout();
		form.setSize("100%", "80%");
		form.setPadding(5);
		form.setLayoutAlign(VerticalAlignment.BOTTOM);
		frmlisCli = new frmListClientes("tblclientes");
		frmlisCli.lstCliente.addDoubleClickHandler(new ManejadorBotones(
				"asignar"));
		frmlisCli.setSize("100%", "100%");
		// frmlisCli.txtBuscarLst.focusInItem();
		form.addMember(frmlisCli);
		frmlisCli.txtBuscarLst.setSelectOnFocus(true);
		VLayout form2 = new VLayout();
		form2.setSize("100%", "20%");
		try {
			// Por cada 10 dolares 1 punto del subtotal
			Double puntos = Double.valueOf(txtSubtotal.getValue().toString()) * 0.1;
			// puntos=CValidarDato.getDecimal(0, puntos);
			Integer puntosEnteros = (int) Math.floor(puntos);// Integer.valueOf(puntos.toString());
			txtPuntosAsignados.setValue(puntosEnteros.toString());
		} catch (Exception e) {

		}
		dfPuntos = new DynamicForm();
		dfPuntos.setFields(txtPersonaAsignada, txtPuntosAsignados);
		try {

			txtPersonaAsignada.setValue(personaAsignada);
		} catch (Exception e) {
			// SC.say("error 2");
		}
		form2.addMember(dfPuntos);
		if (btnNuevo.getTitle().equals("ANULAR")) {
			form2.addMember(btnQuitarPuntos);
			btnQuitarPuntos.addClickHandler(new Manejador("quitarPuntos"));
		}
		winClientes.addItem(form);
		winClientes.addItem(form2);
		winClientes.show();
	}

	public void cargardesdeexcel(LinkedList datos) { // SC.say("Cargado"+datos.size());
		String cantidad = "";
		rowNumGlobal = 0;
		// grdProductos = new ListGrid();
		int codigoprec = 0;
		String pre = (String) datos.get((datos.size() - 1));
		for (int i = 0; i < datos.size() - 3; i++) {

			// calculo_Subtotal();
			// grdProductos.saveAllEdits();

			ProductoDTO producto = (ProductoDTO) datos.get(i);
			ProductoRecords grid = new ProductoRecords(producto);
			cargaProducto = new ListGridRecord();
			grid.setAttribute("observacion", " ");
			grid.setAttribute("cantidad", 1);
			cargaProducto = grid;
			// String observacionAdjunta = "";
			// Integer numfila = 0;
//			Integer iva = 0;
			Double precioU = 0.0;
			String observacionAdjunta = "";
			
			String cadIva="";
			String[] ivas;
			
			String impuestos="";
			double impuestoPorc=1.0;
			double impuestoValor=0.0;
			int i1=0;
			
			String[] cant = (String[]) datos.get(datos.size() - 3);

			if (!cargaProducto.getAttributeAsString("impuesto").isEmpty()){
//				iva = Integer.parseInt(cargaProducto
//						.getAttributeAsString("impuesto"));
				cadIva=cargaProducto.getAttributeAsString("impuesto");
				ivas=cadIva.split(",");
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+ivas.length);
				for (String ivaS:ivas){
					i1=i1+1;
					impuestoPorc=impuestoPorc*(1+(Double.parseDouble(ivaS)/100));
					impuestoValor=impuestoValor+((Double.parseDouble(cant[i])*cargaProducto.getAttributeAsDouble("Precio")
							+impuestoValor)*(Double.parseDouble(ivaS)/100));
//					impuestoValor=impuestoValor+((valTotal* (1 - desc)+impuestoValor)*(Double.parseDouble(ivaS)/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
			}

			
			
			
			
			NumberFormat formato = NumberFormat.getFormat("###0.000");
			Map<String, Object> resultMap = new HashMap<String, Object>();// Utils.getMapFromRow(dsFields,
																			// getResultRow())
			resultMap.put("idProducto",
					cargaProducto.getAttributeAsString("idProducto"));
			resultMap.put("codigoBarras",
					cargaProducto.getAttributeAsString("codigoBarras"));
			cantidad = cargaProducto.getAttributeAsString("cantidad");
//			String[] cant = (String[]) datos.get(datos.size() - 3);
			resultMap.put("cantidad", cant[i]);
			try {
				if (!cargaProducto.getAttributeAsString("observacion")
						.isEmpty()) {
					observacionAdjunta = "-"
							+ cargaProducto.getAttributeAsString("observacion");
				}
			} catch (Exception e) {

			}
			resultMap.put("descripcion",
					cargaProducto.getAttributeAsString("descripcion")
							+ observacionAdjunta);
			// COSTO

			Double precio = Double.parseDouble(cargaProducto 
					.getAttributeAsString("promedio"));

			precioU = 0.0;
			
			precioU = cargaProducto.getAttributeAsDouble("Precio");
			
			if (pre.equals("MAYORISTA")) {
				precioU = cargaProducto.getAttributeAsDouble("Mayorista");
				
			} else if (pre.equals("AFILIADO")) {
				precioU = cargaProducto.getAttributeAsDouble("P NORMAL");
				codigoprec = 3;

			} else if (pre.equals("PUBLICO")) {
				precioU = cargaProducto.getAttributeAsDouble("PVP");
				codigoprec = 2;

			} 
			
			if (NumDto == 1 || NumDto == 10 || NumDto == 14 || NumDto == 20 || NumDto == 27) {
				precioU = precio;
			}
			if (NumDto == 0 || NumDto == 3 || NumDto == 13) {
//				if (iva == Factum.banderaIVA)
				if ((impuestoPorc-1)>0.0)
					precioU = precioU / (impuestoPorc);
//					precioU = precioU / (1+(Factum.banderaIVA/100));
			}
			/*
			 * SC.say(cargaProducto.getAttributeAsString("idProducto")+" "+
			 * cargaProducto
			 * .getAttributeAsString("codigoBarras")+" "+cargaProducto
			 * .getAttributeAsString("descripcion")
			 * +" "+cargaProducto.getAttributeAsString
			 * ("promedio")+" "+cargaProducto
			 * .getAttributeAsString("cantidad")+" "
			 * +cargaProducto.getAttributeAsString
			 * ("impuesto")+" "+cargaProducto.
			 * getAttributeAsString("observacion"));
			 */
			precioU = CValidarDato.getDecimal(4, precioU);
			double precioConIva = 0.0;
//			precioConIva = precioU + (precioU * iva / 100);
			precioConIva = precioU*impuestoPorc;
			precioConIva = Math.rint(precioConIva * 100) / 100;
			NumberFormat formato2 = NumberFormat.getFormat("#.00");
			// precioConIva=CValidarDato.getDecimal(2,precioConIva);
			resultMap.put("precioUnitario", formato.format(precioU));
			resultMap.put("valorTotal", 0.0);
//			resultMap.put("iva", iva);
			resultMap.put("iva",impuestoPorc-1);
			resultMap.put("stock", Double.parseDouble(cargaProducto
					.getAttributeAsString("stock")));
//			Cambios para controlar el numero de decimales que muestra el stock
//			resultMap.put("stock", CValidarDato.getDecimal(3, Double.parseDouble(cargaProducto
//					.getAttributeAsString("stock"))) );
			resultMap.put("descuento", 0);
			resultMap.put("costo", Double.parseDouble(cargaProducto
					.getAttributeAsString("promedio")));
			resultMap.put("precioConIva", formato2.format(precioConIva));
			resultMap.put("numero", rowNumGlobal + 1);
			Integer totalFilas = grdProductos.getRecords().length;
			grdProductos.setEditValues(rowNumGlobal, resultMap);
			grdProductos.refreshRow(rowNumGlobal);
			grdProductos.refreshCell(rowNumGlobal, 0);
			/*
			 * grdProductos.startEditingNew(); int nuevaCelda =
			 * grdProductos.getTotalRows();
			 * grdProductos.startEditing(nuevaCelda-1, 2, false);
			 */
			// grdProductos.saveAllEdits();
			calculo_Individual_Total();

			calculo_Subtotal();
			grdProductos.saveAllEdits();
			// mostrarBodegas(cargaProducto.getAttributeAsString("idProducto"));
			rowNumGlobal++;

		}

		grdProductos.saveAllEdits();
		grdProductos.startEditingNew();
		int nuevaCelda = grdProductos.getTotalRows();
		grdProductos.startEditing(nuevaCelda - 1, 2, false);

		if (Bodega == null) {
			Bodega = (BodegaDTO) datos.get(datos.size() - 2);
			if (BodegaGRABAR == null) {
				BodegaGRABAR = Bodega;
			}
		}

		// grdProductos.refreshFields();

		//cmbTipoBodega.setValue(Bodega.getIdBodega());
		// cmbTipoBodega.s
		cmbTipoPrecio.setValue(codigoprec);
		//cmbTipoPrecio.setdefaultValue(MapTipoPrecio.get(pre));
		// rowNumGlobal = 4;
		// rowNumGlobal =rowNumGlobal-3;
	}
	
	private void obtener_sesion(){
		final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {
			public void onSuccess(User result) {
				if (result == null) {//La sesion expiro

					SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

						public void execute(Boolean value) {
							if (value) {
								com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
								//getService().LeerXML(asyncCallbackXML);
							}
						}
					});
				}else{
					Integer nivel = result.getNivelAcceso();
					//NIVEL 1=gerente, 3=vendedor, 5=cajero
					usuario=result;//Borrar
					planCuentaID=result.getPlanCuenta();
					if (nivel==3){
						//cmbTipoPrecio.disable();
						/*if(Factum.banderaMenuBodega==1){
	        	    		   cmbTipoBodega.enable();
	        	    	   }else{
	        	    		   cmbTipoBodega.disable();   
	        	    	   }*/

						lgfPrecio_unitario.setCanEdit(false);
						lgfValor_total.setCanEdit (false);
						lgfIva.setCanEdit (false);

						/*
	        				MapTipoPrecio.clear();
	        				MapTipoPrecio.put("","");
	        				TipoPrecioDTO publico = new TipoPrecioDTO();
						 */

						/*	MapTipoPrecio.put(String.valueOf(listaTipoPrecio.get(0).getIdTipoPrecio()), 
	        							listaTipoPrecio.get(0).getTipoPrecio());*/
						/*
	        	        		MapTipoPrecio.put(String.valueOf(listaTipoPrecio.get(1).getIdTipoPrecio()), 
	        	        				listaTipoPrecio.get(1).getTipoPrecio());
	        	        		MapTipoPrecio.put(String.valueOf(listaTipoPrecio.get(2).getIdTipoPrecio()), 
	        	        				listaTipoPrecio.get(2).getTipoPrecio());
	        	        		if(Factum.banderaFacturarMayorista==1){
	        	        		MapTipoPrecio.put(String.valueOf(listaTipoPrecio.get(3).getIdTipoPrecio()), 
	        	        				listaTipoPrecio.get(3).getTipoPrecio());
	        	        		}

	        				cmbTipoPrecio.setValueMap(MapTipoPrecio);

	        				//cmbTipoPrecio.setDefaultValue(2);
	        				cmbTipoPrecio.setDefaultToFirstOption(true);
	        				//try{cmbTipoPrecio.setDefaultValue(2);}
	        				//catch(Exception e){}
	        				cmbTipoPrecio.redraw();
						 */

					}

					/*cmbTipoBodega.setDefaultToFirstOption(true);
	        	       if(Factum.banderaMenuBodega==1){
	        	    	   cmbTipoBodega.setDisabled(false);   
	        	       }else{
	        	    	   cmbTipoBodega.setDisabled(true); 
	        	       }*/
					if(Factum.banderaExportar==0){
						btnExportar.setVisible(false);
					}else{
						btnExportar.setVisible(true);	        	    	   
					}

					if(Factum.banderaImportar==0){
						btnImportar.setVisible(false);	        		     
					}else{
						btnImportar.setVisible(true);		        		     	        	    	  
					}
					btnNuevaFactura.setVisible (true);	
					inicializarFactura();
				}
			}
			public void onFailure(Throwable caught) {
				SC.say("ERROR AL COMPROBAR EL TIPO DE USUARIO");
				cmdBod.setVisible(false);
				com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			}
	    };
	    getService().getUserFromSession(callbackUser);
		}
	  
	 public void cargarcamposcliente (String ced, int tipopersona) {
			PersonaDTO p = new PersonaDTO ();
			p.setCedulaRuc(ced);
			
		    getService ().buscarPersona(p, tipopersona, retornoper);
			
	}
	 public void setcuerpo (Cuerpo c){
		 cp = c;
	 }
	 
	 final AsyncCallback<List<PersonaDTO>>  objbacklstEmpleado=new AsyncCallback<List<PersonaDTO>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(List<PersonaDTO> result) {
				MapEmpleados.clear();
				
				//ListGridRecord[] listado = new ListGridRecord[result.size()];
				for(int i=0;i<result.size();i++) 
				{
					if(result.get(i).getEstado()=='1')
					{
						MapEmpleados.put(result.get(i).getIdPersona().toString(), 
						result.get(i).getNombreComercial()
//						+" "+
//						result.get(i).getRazonSocial()
						);
						//listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
					}
				}
				idVendedor=result.get(0).getIdPersona();
				vendedor = new PersonaDTO();
				vendedor.setIdPersona(idVendedor);
				cmbVendedor.setValueMap(MapEmpleados);
				cmbVendedor.redraw();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		   }
		};
		
			
	 
	 public void  VerificarFormasPago(){
		 if (NumDto == 4 || NumDto == 15 || NumDto==3 || NumDto==14){
			 String []fPagos = {"Contado","TarjetaCr","Banco"};
						
				FormasPago="";
				for(int i=0; i<7; i++){
					
					if(i<1){
						if((Boolean)dynamicForm.getItem("chk"+fPagos[i]).getValue())
						{
							
								if(fPagos[i].equals("Contado"))//|| fPagos[i].equals("TarjetaCr") || fPagos[i].equals("Banco"))
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}								
									FormasPago=FormasPago+"Contado";
									
								}				
							
						}
						
					} else if(i>=1 && i<3){
						
						if((Boolean)dynamicForm2.getItem("chk"+fPagos[i]).getValue())
						{
							
							if(fPagos[i].equals("TarjetaCr")) //|| fPagos[i].equals("Banco")
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}	    								
									FormasPago=FormasPago+"TarjetaCr";
									
								}
								else if(fPagos[i].equals("Banco"))
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}
									FormasPago=FormasPago+"Banco";
									
								}	
																														
						}			
					}
				}		
				if(FormasPago.equals("")){
					FormasPago="Contado";
				}
		 }else if (NumDto == 26 || NumDto == 27){
				String []fPagos = {"Contado","Credito",
						"Anticipo","TarjetaCr","Banco"};
						
				FormasPago="";
				for(int i=0; i<7; i++){
					
					if(i<2){
						if((Boolean)dynamicForm.getItem("chk"+fPagos[i]).getValue())
						{
							
								if(fPagos[i].equals("Contado"))//|| fPagos[i].equals("TarjetaCr") || fPagos[i].equals("Banco"))
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}								
									FormasPago=FormasPago+"Contado";
									
								}
								else if(fPagos[i].equals("Credito")){
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}
									FormasPago=FormasPago+"Credito";
									
								}				
							
						}
						
					} else if(i>=2 && i<5){
						
						if((Boolean)dynamicForm2.getItem("chk"+fPagos[i]).getValue())
						{
							
								if(fPagos[i].equals("Anticipo")){
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}
									FormasPago=FormasPago+"Anticipo";
									
								}
													
							
						}
						
					}else if(i>=5 && i<7){
						if((Boolean)dynamicForm3.getItem("chk"+fPagos[i]).getValue())
						{
								
								if(fPagos[i].equals("TarjetaCr")) //|| fPagos[i].equals("Banco")
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}	    								
									FormasPago=FormasPago+"TarjetaCr";
									
								}
								else if(fPagos[i].equals("Banco"))
								{
									if(FormasPago.equals("")){
										FormasPago=FormasPago+"";
									}else{
										FormasPago=FormasPago+"/";	
									}
									FormasPago=FormasPago+"Banco";
									
								}																						
						}			
					}
				}		
				if(FormasPago.equals("")){
					FormasPago="Contado";
				}
		 }else{
			String []fPagos = {"Contado","Credito","Retencion",
					"Anticipo","NotaCredito","TarjetaCr","Banco", "DineroElec"};
					
			FormasPago="";
			for(int i=0; i<7; i++){
				
				if(i<2){
					if((Boolean)dynamicForm.getItem("chk"+fPagos[i]).getValue())
					{
						
							if(fPagos[i].equals("Contado"))//|| fPagos[i].equals("TarjetaCr") || fPagos[i].equals("Banco"))
							{
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}								
								FormasPago=FormasPago+"Contado";
								
							}
							else if(fPagos[i].equals("Credito")){
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"Credito";
								
							}				
						
					}
					
				} else if(i>=2 && i<5){
					
					if((Boolean)dynamicForm2.getItem("chk"+fPagos[i]).getValue())
					{
						
							if(fPagos[i].equals("Retencion")){
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"Retencion";
								
							}	
							else if(fPagos[i].equals("Anticipo")){
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"Anticipo";
								
							}
								//TotalCompleto-= ***VALOR ***;	
							else if(fPagos[i].equals("NotaCredito")){
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"NoraCredito";
								
							}
								//TotalCompleto-= ***VALOR ***;					
						
					}
					
				}else if(i>=5 && i<7){
					if((Boolean)dynamicForm3.getItem("chk"+fPagos[i]).getValue())
					{
							
							if(fPagos[i].equals("TarjetaCr")) //|| fPagos[i].equals("Banco")
							{
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}	    								
								FormasPago=FormasPago+"TarjetaCr";
								
							}
							else if(fPagos[i].equals("Banco"))
							{
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"Banco";
								
							}	
							else if(fPagos[i].equals("DineroElec"))
							{
								if(FormasPago.equals("")){
									FormasPago=FormasPago+"";
								}else{
									FormasPago=FormasPago+"/";	
								}
								FormasPago=FormasPago+"DineroElec";						
							}																						
					}			
				}
			}		
			if(FormasPago.equals("")){
				FormasPago="Contado";
			}
	 }
			}	
	
	 public void agregarRetencion(){
		 	winFactura1 = new Window();
			winFactura1.setWidth(800);
			winFactura1.setHeight(450);
			winFactura1.setTitle("Retencion");
			winFactura1.setShowMinimizeButton(false);
			winFactura1.setIsModal(true);
			winFactura1.setShowModalMask(true);
			winFactura1.setKeepInParentRect(true);
			winFactura1.centerInPage();
			// DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			// frmRetencion ret=new
			// frmRetencion(DocBase);//lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
			winFactura1.addCloseClickHandler(new CloseClickHandler() {
				public void onCloseClick(CloseClientEvent event) {
					winFactura1.destroy();
				}
			});
			VLayout form = new VLayout();
			form.setSize("100%", "100%");
			form.setPadding(5);
			form.setLayoutAlign(VerticalAlignment.BOTTOM);
//			if (formRetencion==null){
//				//com.google.gwt.user.client.Window.alert("Formretencion null ");
				formRetencion = new VLayout();
				formRetencion(NumDto);
//			}
//			else{
//				formRetencion = new VLayout();
//				formRetencion2(NumDto);
//				//com.google.gwt.user.client.Window.alert("Formretencion no null ");
//			}
//			formRetencion = new VLayout();
//			formRetencion(NumDto);
			
			form.addMember(formRetencion);
			winFactura1.addItem(form);
			winFactura1.show();
	 }
	 
	 final AsyncCallback<DtocomercialDTO>  objbackDto=new AsyncCallback<DtocomercialDTO>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(DtocomercialDTO result) {	
				
				documentoenviar= new DtocomercialDTO();
				documentoenviar=result;
				switch(result.getTipoTransaccion()){
				case 0:
					btnDocumentoElectronico.setDisabled(false);
				break;
				case 1:
					if(documentoenviar.getTblretencionsForIdFactura().size()>0){
						btnDocumentoElectronico.setDisabled(false);
					}
				break;
				}
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
		   }
		};
		
		final AsyncCallback<String>  objbackPedido=new AsyncCallback<String>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(String result) {	
				
		   }
		};
	
}
