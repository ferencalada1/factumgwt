package com.giga.factum.client;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.data.fields.DataSourceLinkField;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;
public class Cargar_grilla_factura extends DataSource {  
  
    private static Cargar_grilla_factura instance = null;  
  
    public static Cargar_grilla_factura getInstance() {  
        if (instance == null) {  
            instance = new Cargar_grilla_factura("supplyItemLocal");  
        }  
        return instance;  
    }    
    public Cargar_grilla_factura(String id) {  
    	setID(id);               
        DataSourceIntegerField Pk = new DataSourceIntegerField("idProducto", "PK", 5);  
        Pk.setHidden(true);  
        Pk.setPrimaryKey(true);  
        
        IntegerRangeValidator rangeValidator = new IntegerRangeValidator();  
        rangeValidator.setMin(0);  
        rangeValidator.setErrorMessage("Por favor ingrese una cantidad valida");          
        DataSourceTextField Codigo = new DataSourceTextField("codigo", "CODIGO", 10);           
        DataSourceTextField Descripcion = new DataSourceTextField("descripcion", "DESCRIPCION", 30);
        //Descripcion.setCanEdit(false);
        DataSourceIntegerField Cantidad = new DataSourceIntegerField("cantidad", "CANTIDAD", 5);  
        DataSourceFloatField Precio_unitario = new DataSourceFloatField("precio_unitario", "PRECIO UNITARIO", 5);          
        //DataSourceFloatField Valor_total = new DataSourceFloatField("valor_total", "VALOR TOTAL", 5);
        Precio_unitario.setValidators(rangeValidator);
        Cantidad.setValidators(rangeValidator);
        setFields(Pk,Codigo,Descripcion,Cantidad,Precio_unitario);//,Valor_total); 
        setClientOnly(true);  
        setTestData(null);
    }  
}  
