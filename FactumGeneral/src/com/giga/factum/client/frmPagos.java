package com.giga.factum.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.regGrillas.PagoRecords;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.PrintProperties;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.ChangedEvent;
import com.smartgwt.client.widgets.grid.events.ChangedHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;

// METODO PARA QUE SUME 1EL NUMERO DE FACUTRA
//Integer nuevoNumeroDocumento = Integer.parseInt(txtNumero_factura.getValue().toString());
//nuevoNumeroDocumento = nuevoNumeroDocumento + 1;
//txtNumero_factura.setValue(nuevoNumeroDocumento.toString());
//////com.google.gwt.user.client.Window.alert("numero de documento");
//txtNumero_factura.redraw();

public class frmPagos extends VLayout{
	frmListClientes listClientes =new frmListClientes();
	ListGridField lstFactura =new ListGridField("lstFactura", "# Factura",50);
	ListGridField lstCliente =new ListGridField("lstCliente", "Cliente");
	ListGridField lstFechaV =new ListGridField("lstFechaV", "FechaVencimiento",100);
	ListGridField lstValor =new ListGridField("lstValor", "Valor",80);
	ListGridField lstTipo = new ListGridField("lstTipo", "Tipo",100);
	ListGridField lstTipoDocumento = new ListGridField("tipoDocumento", "TipoDocumento",100);
	ListGridField lstDireccion = new ListGridField("lstDireccion", "Direccion",100);
	ListGridField lstLocalizacion = new ListGridField("lstLocalizacion", "Localizacion",100);
	ListGridField lstTelefono = new ListGridField("lstTelefono", "Telefono",100);
	private  String idCliente="";
	private String idFactura="";
	DateTimeItem txtFechaI = new DateTimeItem("txtFechaI", "Desde");
	DateTimeItem txtFechaF = new DateTimeItem("txtFechaF", "Hasta");
	TextItem txtCliente = new TextItem("txtCliente", "Cliente");
	TextItem txtFactura = new TextItem("txtFactura", "Factura");
	//TextItem txtNumIngreso = new TextItem("txtNumIngreso", "Numero Ingreso");
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	ComboBoxItem cmbEstado = new ComboBoxItem("cmbEstado", "Estado del pago");
	ListGrid lstPagos = new ListGrid();
	final Window winCliente = new Window(); 
	final Window winPago = new Window(); 
	Window winFactura1 = new Window(); 
	Window winRetencion = new Window(); 
	Window winDocumento = new Window();
	String totalPagos ="";
	//frmIngersos ingreso=new frmIngersos();
	Window winFactura = new Window(); 
	frmReporteCaja listaDocumentos;
	frmReporteCaja form;
	List<TblpagoDTO> pagos=null;
	String tipoS ="";
	//DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	DynamicForm dynamicForm3 = new DynamicForm();
	HStack hStackSuperior = new HStack();
	ListGridRecord registroPago;
	User usuario = new User();
	
	 NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public frmPagos() { 
		setSize("80%", "50%");
			lstPagos=new ListGrid() {  
            @Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
            	final int col=colNum;
                String fieldName = this.getFieldName(colNum);  
                
                if (fieldName.equals("buttonField")) {  
                    IButton button = new IButton();  
                    button.setHeight(18);  
                    button.setWidth(65);                      
                    button.setTitle("Informaci&oacute;n");  
                    button.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) { 
                    	try{
                    		winFactura1 = new Window();
                    		winFactura1.setWidth("95%");
    						winFactura1.setHeight("95%");
                        	winFactura1.setTitle("Documento Comercial");  
                        	winFactura1.setShowMinimizeButton(false);  
                        	winFactura1.setIsModal(true);  
                        	winFactura1.setShowModalMask(true); 
                        	winFactura1.setKeepInParentRect(true);
                        	winFactura1.centerInPage();  
        					//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
                        	int Tipo=0;
                        	if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
            					Tipo=0;
            				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
            					Tipo=1;
            				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
            					Tipo=2;
            				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
            					Tipo=3;
            				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
            					Tipo=4;
            				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
            					Tipo=10;
            				}else if(cmbDocumento.getDisplayValue().equals("Factura Grande")){
            					Tipo=13; 
            				}else if(cmbDocumento.getDisplayValue().equals("Nota de Venta")){
            					Tipo=26; 
            				}
                        	Factura fac=new Factura(Tipo,Integer.parseInt(lstPagos.getSelectedRecord().getAttribute("idDocumento")));
        					winFactura1.addCloseClickHandler(new CloseClickHandler() {  
        		                public void onCloseClick(CloseClientEvent event) {  
        		                	winFactura1.destroy();  
        		                }  
        		            });
        					//SC.say(lstPagos.getSelectedRecord().getAttribute("idDocumento"));
        					VLayout form = new VLayout();  
        		            form.setSize("100%","100%"); 
        		            form.setPadding(5);  
        		            form.setLayoutAlign(VerticalAlignment.BOTTOM);
        		            form.addMember(fac);
        		            winFactura1.addItem(form);
        		            winFactura1.show();}
                        	catch(Exception e){
                        		SC.say("ERROR: "+e.getLocalizedMessage()+"----"+e.getMessage());
                        	}
                        }  
                    });  
                    return button;  
                } else {  
                    return null;  
                }  
            }  
        };  
        lstPagos.setShowRecordComponents(true);          
        lstPagos.setShowRecordComponentsByCell(true);  
        lstPagos.setShowAllRecords(true); 
        lstPagos.setShowRowNumbers(true);//drag and drop
        lstPagos.setCanDragRecordsOut(true);  
        lstPagos.setDragDataAction(DragDataAction.MOVE);
        
        cmbDocumento.setDefaultToFirstOption(true);
        cmbEstado.setDefaultToFirstOption(true);
        txtFechaI.setValue(new Date());
		txtFechaF.setValue(new Date());
        txtFechaF.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaF.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaF.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaI.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaI.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaI.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		
		txtCliente.addKeyPressHandler( new ManejadorBotones("cliente"));
		
		//txtFactura.addKeyPressHandler( new ManejadorBotones("factura"));
		
		PickerIcon pckLimpiarCliente = new PickerIcon(PickerIcon.CLEAR);
		txtCliente.setIcons(pckLimpiarCliente);
		pckLimpiarCliente.addFormItemClickHandler(new ManejadorBotones("borrarCliente"));
		PickerIcon pckBuscarIngreso = new PickerIcon(PickerIcon.SEARCH);
		//txtNumIngreso.setIcons(pckBuscarIngreso);
		pckBuscarIngreso.addFormItemClickHandler(new ManejadorBotones("buscarIngreso"));
		PickerIcon pckLimpiarFactura = new PickerIcon(PickerIcon.CLEAR);
		txtFactura.setIcons(pckLimpiarFactura);
		pckLimpiarFactura.addFormItemClickHandler(new ManejadorBotones("borrarFactura"));
		
		cmbDocumento.setValueMap("Facturas de Venta","Notas de Entrega","Nota de Venta","Notas de Credito","Anticipo Clientes");
		cmbEstado.setValueMap("PENDIENTE","COBRADO");
		dynamicForm.setFields(new FormItem[] { txtFechaI, txtFechaF});
		dynamicForm2.setFields(new FormItem[] { cmbDocumento,cmbEstado});
		dynamicForm3.setFields(new FormItem[] { txtCliente, txtFactura});
		dynamicForm.setWidth("50%");
		dynamicForm2.setWidth("40%");
		dynamicForm3.setWidth("10%");
		hStackSuperior.setSize("100%", "20%");
		hStackSuperior.addMember(dynamicForm);
		hStackSuperior.addMember(dynamicForm2);
		hStackSuperior.addMember(dynamicForm3);
		//dynamicForm.setNumCols(4);
		//addMember(dynamicForm);
		addMember(hStackSuperior);
		//dynamicForm.setFields(new FormItem[] { txtFechaI, txtFechaF, txtCliente, txtFactura,txtNumIngreso,cmbDocumento,cmbEstado});
		//addMember(dynamicForm);
		ListGridField lstId =new ListGridField("lstId", "Id Pago",20);
	//	ListGridField lstFactura =new ListGridField("lstFactura", "#Factura",50);
	//	ListGridField lstCliente =new ListGridField("lstCliente", "Cliente");
	//	ListGridField lstFechaV =new ListGridField("lstFechaV", "FechaVencimiento",100);
		ListGridField lstFechaP =new ListGridField("lstFechaP", "FechaRealPago",100);
		//lstFechaP.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		lstFechaP.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		//lstFechaP.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
//		ListGridField lstValor =new ListGridField("lstValor", "Valor",80);
		ListGridField lstEstado =new ListGridField("lstEstado", "Estado",50);
		ListGridField lstidFactura =new ListGridField("lstidFactura", "idFactura",50);
		ListGridField lstidCliente =new ListGridField("lstidCliente", "idCliente",50);
		ListGridField buttonField = new ListGridField("buttonField", "Info",80); 
		ListGridField idDocumento = new ListGridField("idDocumento", "idDocumento");  
		ListGridField Ban = new ListGridField("Ban", "Activo",60);
		ListGridField lstValorAPagar = new ListGridField("lstValorAPagar", "Valor a Pagar",100);
		ListGridField lstTipo = new ListGridField("lstTipo", "Tipo",100);
		lstTipo.setValueMap("Efectivo","Banco","Tarjeta de Credito","Nota de Credito","Anticipo","Retencion");
		ListGridField lstNumEgreso= new  ListGridField("lstNumEgreso", "Num Ingreso",30);
		lstTipo.addChangeHandler(new ManejadorBotones("tipo"));
//		ListGridField lstTipoDocumento = new ListGridField("tipoDocumento", "TipoDocumento",100);
		ListGridField lstObs = new ListGridField("lstObs", "Observaciones");
		lstNumEgreso.setAlign(Alignment.CENTER); 
		
		lstObs.setCanEdit(true);
		lstTipo.setCanEdit(true);
		Ban.setType(ListGridFieldType.BOOLEAN); 
		Ban.setCanEdit(true);
		Ban.addChangeHandler(new ManejadorBotones("ban"));
		lstValorAPagar.setAlign(Alignment.RIGHT);
		lstValorAPagar.setType(ListGridFieldType.FLOAT);
		lstValorAPagar.setCanEdit(true);
		lstFechaP.setType(ListGridFieldType.DATE);
		lstFechaP.setCanEdit(true);
		lstValor.setType(ListGridFieldType.FLOAT);
		lstValor.setAlign(Alignment.RIGHT);
		lstValor.setShowGridSummary(true);
		
		lstValor.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                	sum += Double.valueOf(records[i].getAttribute("lstValor"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		//buttonField.setType(ListGridFieldType.IMAGE);
        buttonField.setAlign(Alignment.CENTER); 
        lstFactura.setAlign(Alignment.CENTER);
        lstFechaV.setAlign(Alignment.CENTER);
        lstFechaP.setAlign(Alignment.CENTER);
        lstFactura.setWidth("5%");
        buttonField.setWidth("8%");
        lstCliente.setWidth("23%");
        lstFechaV.setWidth("8%"); 
        lstValor.setWidth("8%");
        lstTipo.setWidth("8%");
        lstValorAPagar.setWidth("8%");
        lstFechaP.setWidth("8%");
        lstObs.setWidth("10%");
        Ban.setWidth("5%");
        lstTipoDocumento.setWidth("9%");
		lstPagos.setFields(new ListGridField[] { lstFactura,buttonField, lstCliente,lstFechaV,lstValor,lstTipo,lstValorAPagar,lstFechaP,lstObs,Ban,lstDireccion,lstLocalizacion,lstTelefono,lstNumEgreso,lstTipoDocumento});
		lstPagos.setCanResizeFields(true);
		lstPagos.setShowGridSummary(true);
		addMember(lstPagos);
		
		//lstPagos.addDoubleClickHandler(new ManejadorBotones("pago"));
		Canvas canvas = new Canvas();
		canvas.setSize("100%", "5%");
		IButton btnReporte = new IButton("Reporte");
		btnReporte.moveTo(6, 6);
		canvas.addChild(btnReporte);
		
		IButton btnExportar = new IButton("Exportar");
        btnExportar.addClickHandler(new ManejadorBotones("exportar"));
        canvas.addChild(btnExportar);
        btnExportar.moveTo(112, 6);
		
		IButton btnImprimir = new IButton("Imprimir");
		btnImprimir.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	//SC.say("imprimir");
            
            		SC.say(String.valueOf(lstPagos));
            	totalPagos = "Total: "+totalPagos;
            	Object[] a=new Object[]{dynamicForm,lstPagos,totalPagos};
            	Canvas.showPrintPreview(a);             	
            }
		});
		canvas.addChild(btnImprimir);
		btnImprimir.moveTo(323, 6);
		IButton btnGrabar = new IButton("Grabar");
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(429, 6);
		IButton btnMostrar = new IButton("Mostrar Pago");
		canvas.addChild(btnMostrar);
		btnMostrar.moveTo(535, 6);
		IButton btnBuscarFechaPago = new IButton("Buscar Por Fecha de Pago");
		canvas.addChild(btnBuscarFechaPago);
		btnBuscarFechaPago.moveTo(641, 6);
		IButton btnLimpiar = new IButton("Limpiar");
		btnLimpiar.moveTo(217, 6);
		canvas.addChild(btnLimpiar);
		addMember(canvas);
		btnLimpiar.addClickHandler( new ManejadorBotones("limpiar"));
		btnReporte.addClickHandler( new ManejadorBotones("reporte"));
		btnGrabar.addClickHandler( new ManejadorBotones("grabar"));
		btnMostrar.addClickHandler( new ManejadorBotones("mostrar"));
		btnBuscarFechaPago.addClickHandler( new ManejadorBotones("buscarFechaPago"));
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		//addMember(ingreso);
		//getService().listarPagosVencidos(objbacklst);	
		getService().getUserFromSession(callbackUser);
	}
	private class ManejadorBotones implements ClickHandler, KeyPressHandler,DoubleClickHandler,ChangeHandler,ChangedHandler, FormItemClickHandler {
		String indicador="";
		String fechaI="";
		String fechaF="";
		String Tipo="0",estado="";
		String numIngreso="";
		public ManejadorBotones(String ind){
			indicador=ind;
		}
		public void datosfrm(){
			try  {
				fechaI=txtFechaI.getDisplayValue();
				fechaF=txtFechaF.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
					Tipo="10";
				}else if(cmbDocumento.getDisplayValue().equals("Factura Grande")){
					Tipo="13";
				}else if(cmbDocumento.getDisplayValue().equals("Nota de Venta")){
					Tipo="26";
				}
				if(cmbEstado.getDisplayValue().equals("PENDIENTE")){
					estado="0"; //cambiar por 0
				}else{
					estado="1";
				}
			}catch(Exception e){
				SC.say("Error del formulario datos mal Ingresados");
			}
		} 
		@Override
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("reporte")){
				datosfrm();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				idFactura=String.valueOf(txtFactura.getDisplayValue());
				if(idFactura.equals("") && idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,"","",0, objbacklst);
				}else if(!idFactura.equals("") && idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,idFactura,"",1, objbacklst);
				}else if(idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,"",idCliente,2, objbacklst);
				}else if(!idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,idFactura,idCliente,3, objbacklst);
				}
				
			}
			else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstPagos);
			}
			else if(indicador.equalsIgnoreCase("limpiar")){
				txtFechaI.setValue("");
				txtFechaF.setValue("");
				txtCliente.setValue("");
				txtFactura.setValue("");
				idCliente="";
				idFactura="";
				
			}else if(indicador.equalsIgnoreCase("grabar")){
				LinkedList<TblpagoDTO> Listpago=new LinkedList<TblpagoDTO>();
				try{
					for(int i=0;i<lstPagos.getRecordList().getLength();i++){
						if(lstPagos.getRecord(i).getAttributeAsString("Ban").equalsIgnoreCase("true")){
							if(lstPagos.getRecord(i).getAttributeAsString("lstEstado").equals("0") && !lstPagos.getRecord(i).getAttributeAsString("lstValorAPagar").equals("")){
								//if(lstPagos.validateRow(i)){
									String idpago=String.valueOf(lstPagos.getRecord(i).getAttribute("lstId"));
									double valor=Double.valueOf(lstPagos.getRecord(i).getAttribute("lstValor"));
									valor=CValidarDato.getDecimal(2, valor);
									double pag=Double.valueOf(lstPagos.getRecord(i).getAttribute("lstValorAPagar"));
									pag=CValidarDato.getDecimal(2, pag);
									if(pag<=valor){
										for(int j=0;j<pagos.size();j++){
											if(idpago.equals(String.valueOf(pagos.get(j).getIdPago()))){
												TblpagoDTO pagodto=new TblpagoDTO();
												pagodto=pagos.get(j);
												int tipoPago=1;
												if(lstPagos.getRecord(i).getAttribute("lstTipo").equals("Efectivo")){
													tipoPago=1;
												}else if(lstPagos.getRecord(i).getAttribute("lstTipo").equals("Banco")){
													tipoPago=7;
												}else if(lstPagos.getRecord(i).getAttribute("lstTipo").equals("Tarjeta de Credito")){
													tipoPago=6;
												}else if(lstPagos.getRecord(i).getAttribute("lstTipo").equals("Nota de Credito")){
													tipoPago=5;
												}else if(lstPagos.getRecord(i).getAttribute("lstTipo").equals("Anticipo")){
													tipoPago=4;
												}
												
												DtocomercialTbltipopagoDTO tipo=new DtocomercialTbltipopagoDTO();
												tipo.setTipoPago(tipoPago);
												tipo.setObservaciones(lstPagos.getRecord(i).getAttributeAsString("lstObs"));
												//tipo.setReferenciaDto(lstPagos.getRecord(i).getAttributeAsInt("idDocumento"));
												tipo.setReferenciaDto(Integer.valueOf(idpago));
												
												pagodto.setIdEmpresa(Factum.empresa.getIdEmpresa());
												pagodto.setEstablecimiento(Factum.getEstablecimientoCero());
												
												pagodto.setConcepto(lstPagos.getRecord(i).getAttributeAsString("lstObs"));
												pagodto.setEstado('1');
												pagodto.setFechaRealPago((lstPagos.getRecord(i).getAttributeAsDate("lstFechaP")));
												HashSet<DtocomercialTbltipopagoDTO> tipospago = new HashSet<DtocomercialTbltipopagoDTO>(0);
												tipospago.add(tipo);
												pagodto.getTbldtocomercial().setTbldtocomercialTbltipopagos(tipospago);
												pagodto.setValorPagado(pag);
												pagodto.setValor(valor);
												pagodto.setFormaPago(lstPagos.getRecord(i).getAttributeAsString("lstTipo"));
											
												Listpago.add(pagodto);
											}
										}
									}else{
										SC.say("Imposible generar el pago, el valor a pagar es mayor que el valor de la deuda");
										Listpago=new LinkedList<TblpagoDTO>();
										break;
									}
								}
								//}else{
									//SC.say("Ingrese valores correctos");
								//}
							}
						}
						if(Listpago.isEmpty()){
							SC.say("Error  No se puede Guardar los Pagos Verifique valores o seleccione un pago");
						}else{
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().grabarPagos(Listpago,usuario,objbackString);
						//	SC.say(String.valueOf("id "+Listpago.get(0).getTbldtocomercial().getIdDtoComercial())+" "+Listpago.get(0).getFechaRealPago()+" "+String.valueOf(Listpago.get(0).getValorPagado()));
						}
				}catch(Exception e){
					SC.say("Error  No se puede Guardar los Pagos Verifique valores o seleccione un pago");
				}
				
			}else if(indicador.equals("pagoreten")){
				winRetencion.destroy();
				winRetencion.hide();
			//	SC.say("hola grabar");
			//	getService().listarPagosVencidos(objbacklst);
			}else if(indicador.equals("mostrar")){
				try{ 
					String idpago=String.valueOf(lstPagos.getSelectedRecord().getAttribute("lstId"));
					TblpagoDTO pago=new TblpagoDTO();
					//String tipo=lstPagos.getSelectedRecord().getAttribute("lstTipo");
					String tipo=lstPagos.getSelectedRecord().getAttribute("tipoDocumento");
					for(int i=0;i<pagos.size();i++){
						if(idpago.equals(String.valueOf(pagos.get(i).getIdPago()))){
							pago=pagos.get(i);
							break;
						}
					}
					final Window winPago = new Window();
					winPago.setWidth(930);  
					winPago.setHeight(610);  
					winPago.setTitle("Pagos");  
					winPago.setShowMinimizeButton(false);  
					winPago.setIsModal(true);  
					winPago.setShowModalMask(true);  
					winPago.setKeepInParentRect(true);
					winPago.centerInPage();  
					winPago.addCloseClickHandler(new CloseClickHandler() {  
		                public void onCloseClick(CloseClientEvent event) {  
		                //	getService().listarPagosVencidos(objbacklst);//CAMBIAR
		                	winPago.destroy();
		                }   
		            });
				
					frmPago frmpago=new frmPago(pago,tipo,"0"); 
					
					frmpago.setSize("100%","100%"); 
					frmpago.setPadding(5);   
					frmpago.setLayoutAlign(VerticalAlignment.BOTTOM);
		            winPago.addItem(frmpago);
		            winPago.show();
				}catch(Exception e){
					//SC.say(e.getMessage());
					SC.say("Seleccione un elemento");
				}
				
			}else if(indicador.equals("buscarFechaPago")){
				datosfrm();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				idFactura=String.valueOf(txtFactura.getDisplayValue());
				if(idFactura.equals("") && idCliente.equals("")){
					getService().listarPagosFechaPago(fechaI,fechaF,"1",Tipo,"","",0, objbacklst);
				}else if(!idFactura.equals("") && idCliente.equals("")){
					getService().listarPagosFechaPago(fechaI,fechaF,"1",Tipo,idFactura,"",1, objbacklst);
				}else if(idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagosFechaPago(fechaI,fechaF,"1",Tipo,"",idCliente,2, objbacklst);
				}else if(!idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagosFechaPago(fechaI,fechaF,"1",Tipo,idFactura,idCliente,3, objbacklst);
				}
			}
		}
		
		
		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(indicador.equalsIgnoreCase("cliente")){
				winCliente.setWidth(930);  
				winCliente.setHeight(610);  
				winCliente.setTitle("Listado Clientes");  
				winCliente.setShowMinimizeButton(false);  
				winCliente.setIsModal(true);  
				winCliente.setShowModalMask(true);  
				winCliente.setKeepInParentRect(true);
				winCliente.centerInPage();  
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
            	//getService().ListJoinPersona("tblclientes",0,20, objbacklstCliente);
				winCliente.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	idCliente="";
	                	txtCliente.setValue("");
	                	winCliente.destroy();  
	                }  
	            });
				VLayout layout_1 = new VLayout();
				listClientes=new frmListClientes("tblclientes");
				listClientes.lstCliente.addDoubleClickHandler(new ManejadorBotones("Cliente"));
				layout_1.addChild(listClientes);
				winCliente.addItem(layout_1);
	            winCliente.show();
					
				
				
			}else if(indicador.equalsIgnoreCase("factura")){
				TblpagoDTO  pagodto=new TblpagoDTO();
				winPago.setWidth(930);  
				winPago.setHeight(610);  
				winPago.setTitle("Ingresar Documento");  
				winPago.setShowMinimizeButton(false);  
				winPago.setIsModal(true);  
				winPago.setShowModalMask(true);  
				winPago.centerInPage();  
				winPago.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	winPago.destroy();  
	                }  
	            });
				form = new frmReporteCaja("Facturas de Venta");
				form.lstReporteCaja.addDoubleClickHandler(new ManejadorBotones("pagos"));
				form.setSize("100%","100%"); 
	            form.setPadding(5);  
	            form.setLayoutAlign(VerticalAlignment.BOTTOM);
	            winPago.addItem(form);
	            winPago.show();
			}
		}
		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("cliente")){
				idCliente=listClientes.lstCliente.getSelectedRecord().getAttribute("idPersona");
				txtCliente.setValue(
//						listClientes.lstCliente.getSelectedRecord().getAttribute("NombreComercial")+" "+
						listClientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
				winCliente.destroy();
			}else if(indicador.equalsIgnoreCase("pagos")){
				idFactura=form.lstReporteCaja.getSelectedRecord().getAttribute("id");
				txtFactura.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("NumRealTransaccion"));
				winPago.destroy();
			}
		}
		@Override
		public void onChange(ChangeEvent event) {
			if(indicador.equals("ban")){
				if((Boolean) event.getValue()){ 
					//lstPagos.getSelectedRecord().setAttribute("lstValorAPagar",lstPagos.getSelectedRecord().getAttribute("lstValor"));
				}else{
					lstPagos.getSelectedRecord().setAttribute("lstValorAPagar","");
				}
				
			}
			else if(indicador.equals("tipo")){
				if(event.getValue().equals("Retencion") && (Integer.parseInt(lstPagos.getRecord(event.getRowNum()).getAttribute("idRetencion"))==0)){
//					SC.say("El documento correspondiente no tiene retencion");		
					lstPagos.getRecord(event.getRowNum()).setAttribute("lstTipo",event.getValue());
					getService().getDtoID(Integer.parseInt(lstPagos.getRecord(event.getRowNum()).getAttribute("idDocumento")), callbackDoc);
				} else if(event.getValue().equals("Retencion") && (Integer.parseInt(lstPagos.getRecord(event.getRowNum()).getAttribute("idRetencion"))!=0)){
					lstPagos.getRecord(event.getRowNum()).setAttribute("lstTipo",event.getOldValue());
//					lstPagos.getRecord(event.getRowNum()).setAttribute("lstTipo",lstPagos.getRecord(event.getRowNum()).getAttribute("lstTipo"));
					SC.say("El documento correspondiente ya consta con una retencion");
					lstPagos.getRecord(event.getRowNum()).setAttribute("lstTipo",event.getOldValue());
					event.getItem().setValue((String)event.getOldValue());
				}else if(event.getValue().equals("Nota de Credito")){
					mostrarDocumento(3);					
				}else if(event.getValue().equals("Anticipo")){
					mostrarDocumento(4);					
				}
			}
			
		}
		@Override
		public void onChanged(ChangedEvent event) {
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			
			 if(indicador.equalsIgnoreCase("borrarCliente")){
					idCliente="";
					txtCliente.setValue("");
				}else if(indicador.equalsIgnoreCase("borrarFactura")){
					idFactura="";
					txtFactura.setValue("");
				}else if(indicador.equalsIgnoreCase("buscarIngreso")){
					if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
						Tipo="0";
					}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
						Tipo="1";
					}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
						Tipo="2";
					}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
						Tipo="3";
					}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
						Tipo="4";
					}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
						Tipo="10";
					}else if(cmbDocumento.getDisplayValue().equals("Factura Grande")){
						Tipo="13";
					}else if(cmbDocumento.getDisplayValue().equals("Nota de Venta")){
						Tipo="26";
					}
					//numIngreso=txtNumIngreso.getDisplayValue();
					getService().listarPagosEgreso(Tipo,numIngreso,objbacklst);
					
				}
		}		
	}
	/*final AsyncCallback<List<PersonaDTO>>  objbacklstCliente=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
		
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			lstCliente.setData(listado);
			lstCliente.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};*/
	final AsyncCallback<List<TblpagoDTO>>  objbacklst=new AsyncCallback<List<TblpagoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<TblpagoDTO> result) {
			pagos=result;
			//ingreso.pagos=pagos; //descomentar para poner la funcionalidad de ingresos o egresos
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			double total = 0;
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PagoRecords((TblpagoDTO)result.get(i)));
				if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("0")){
					listado[i].setAttribute("tipoDocumento", "Factura de Venta");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("1")){
					listado[i].setAttribute("tipoDocumento", "Factura de Compra");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("2")){
					listado[i].setAttribute("tipoDocumento", "Nota de Entrega");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("3")){
					listado[i].setAttribute("tipoDocumento", "Nota de Credito");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("4")){
					listado[i].setAttribute("tipoDocumento", "Anticipo Clientes");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("10")){
					listado[i].setAttribute("tipoDocumento", "Nota de Compra");
				}else if(String.valueOf(result.get(i).getTbldtocomercial().getTipoTransaccion()).equals("26")){
					listado[i].setAttribute("tipoDocumento", "Nota de Venta");
				}
				total=total + result.get(i).getValor();
				if(!cmbEstado.getDisplayValue().equals("PENDIENTE")){
					//	SC.say("obser"+result.get(i).getFormaPago());
						listado[i].setAttribute("lstObs", result.get(i).getConcepto());
						listado[i].setAttribute("lstTipo", result.get(i).getFormaPago());
						
					}
			}
			total=CValidarDato.getDecimal(4, total);
			totalPagos=String.valueOf(total);
			lstPagos.setData(listado);
			lstPagos.redraw();
			if(cmbEstado.getDisplayValue().equals("PENDIENTE"))
			{
				lstPagos.setCanEdit(true);
				lstFactura.setCanEdit(false);
				lstCliente.setCanEdit(false);
				lstFechaV.setCanEdit(false);
				lstValor.setCanEdit(false);
				lstTipoDocumento.setCanEdit(false);
			}
			else
			{
				lstPagos.setCanEdit(false);
			}

			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
//	public void mostrarPago(String idPago){
//		String idpago=idPago;
//		double saldo=0.0;
//		TblpagoDTO pago=new TblpagoDTO();
//		//String tipo=lstPagos.getSelectedRecord().getAttribute("lstTipo");
//		String tipo=lstPagos.getSelectedRecord().getAttribute("tipoDocumento");
//		for(int i=0;i<pagos.size();i++){
//			if(idpago.equals(String.valueOf(pagos.get(i).getIdPago()))){
//				pago=pagos.get(i);
//				saldo=pago.getValorPagado();
//				break;
//			}
//		}
//		final Window winPago = new Window();
//		winPago.setWidth(930);  
//		winPago.setHeight(610);  
//		winPago.setTitle("Pagos");  
//		winPago.setShowMinimizeButton(false);  
//		winPago.setIsModal(true);  
//		winPago.setShowModalMask(true);  
//		winPago.setKeepInParentRect(true);
//		winPago.centerInPage();  
//		winPago.addCloseClickHandler(new CloseClickHandler() {  
//            public void onCloseClick(CloseClientEvent event) {  
//            //	getService().listarPagosVencidos(objbacklst);//CAMBIAR
//            	winPago.destroy();
//            }   
//        });
//	
//		frmPago frmpago=new frmPago(pago,tipo,saldo); 
//		
//		frmpago.setSize("100%","100%"); 
//		frmpago.setPadding(5);   
//		frmpago.setLayoutAlign(VerticalAlignment.BOTTOM);
//        winPago.addItem(frmpago);
//        winPago.show();
//	}
	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			String ids=" ";
			boolean borrar=true;
//			com.google.gwt.user.client.Window.alert("Boolean borrar "+borrar);
			int cont=0;
			final Window winPago = new Window();
			winPago.setWidth(930);  
			winPago.setHeight(610);  
			winPago.setTitle("Pagos");  
			winPago.setShowMinimizeButton(false);  
			winPago.setIsModal(true);  
			winPago.setShowModalMask(true);  
			winPago.setKeepInParentRect(true);
			winPago.centerInPage();  
			winPago.addCloseClickHandler(new CloseClickHandler() {  
	            public void onCloseClick(CloseClientEvent event) {  
	            	refrescar ();
	            	winPago.destroy();
	            }   
	        });
			try{
				while(borrar)
				{	
					if(lstPagos.getRecord(cont).getAttributeAsString("Ban").equalsIgnoreCase("true"))
					{
//						com.google.gwt.user.client.Window.alert("Dentro de while cont "+cont);
						ids=String.valueOf(lstPagos.getRecord(cont).getAttributeAsString("lstId"));
//						com.google.gwt.user.client.Window.alert("En borar pagos ids "+ids);
						//mostrarPago(ids);
						String idpago=ids;
						double valorAPagar=lstPagos.getRecord(cont).getAttributeAsDouble("lstValorAPagar");
//						com.google.gwt.user.client.Window.alert("En borar pagos valorApagar "+valorAPagar);
						String formaPago=lstPagos.getRecord(cont).getAttributeAsString("lstTipo");
//						com.google.gwt.user.client.Window.alert("En borar pagos formaPago "+formaPago);
						double saldo=0.0;
						TblpagoDTO pago=new TblpagoDTO();
						//String tipo=lstPagos.getSelectedRecord().getAttribute("lstTipo");
						String tipo=lstPagos.getSelectedRecord().getAttribute("tipoDocumento");
//						com.google.gwt.user.client.Window.alert("En borar pagos tipo "+tipo);
						for(int i=0;i<pagos.size();i++){
//							com.google.gwt.user.client.Window.alert("En borar pagos pagos "+pagos.get(i).getIdPago());
							if(idpago.equals(String.valueOf(pagos.get(i).getIdPago()))){
								pago=pagos.get(i);
//								com.google.gwt.user.client.Window.alert("id Empresa "+Factum.empresa.getIdEmpresa());
								pago.setIdEmpresa(Factum.empresa.getIdEmpresa());
//								com.google.gwt.user.client.Window.alert("id Empresa "+Factum.getEstablecimientoCero());
								pago.setEstablecimiento(Factum.getEstablecimientoCero());
								pago.setValor(valorAPagar);
								saldo=pago.getValorPagado();
								pago.setFormaPago(formaPago);
								break;
							}
						}
						//frmPago frmpago=new frmPago(pago,tipo,saldo,"0");
						frmPago frmpago=new frmPago(pago,tipo,"0"); 
						frmpago.setSize("100%","100%"); 
						frmpago.setPadding(5);   
						frmpago.setLayoutAlign(VerticalAlignment.BOTTOM);
				        winPago.addItem(frmpago);
						lstPagos.removeData(lstPagos.getRecord(cont));
						cont--;
					}
					cont++;
					if(cont==lstPagos.getRecordList().getLength())
					{
						borrar=false;
					}	
					/*for(int i=0;i<lstPagos.getRecordList().getLength();i++){
						if(lstPagos.getRecord(i).getAttributeAsString("Ban").equalsIgnoreCase("true")){
							lstPagos.removeData(lstPagos.getRecord(i));
						}
					}*/
				}
				
//				com.google.gwt.user.client.Window.alert("Despues de while");
				winPago.show();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				SC.say(result);
			}catch(Exception e){
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				SC.say("Err Pantalla "+e.getMessage());
			}
		
			//SC.say(ids);
			
		}
	};
	final AsyncCallback<String>  objbackString1=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
		}
	};
	final AsyncCallback<DtocomercialDTO> callbackDoc = new AsyncCallback<DtocomercialDTO>() {

        public void onSuccess(DtocomercialDTO result) {
        	try{
				winRetencion = new Window();
				winRetencion.setWidth(800);  
				winRetencion.setHeight(600);  
				winRetencion.setTitle("Ingresar Retenci�n: "+lstPagos.getSelectedRecord().getAttribute("lstCliente"));  
				winRetencion.setShowMinimizeButton(false);  
				winRetencion.setIsModal(true);  
				winRetencion.setShowModalMask(true);  
				winRetencion.setKeepInParentRect(true);
				winRetencion.centerInPage(); 
				TblpagoDTO pagodto=new TblpagoDTO();
				//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		
				//if(lstPagos.validateRow(i)){
				String idpago=String.valueOf(lstPagos.getSelectedRecord().getAttribute("lstId"));
				double valor=Double.valueOf(lstPagos.getSelectedRecord().getAttribute("lstValor"));
				//SC.say(idpago+" "+String.valueOf(valor));
				for(int j=0;j<pagos.size();j++){
					if(idpago.equals(String.valueOf(pagos.get(j).getIdPago()))){
						pagodto=new TblpagoDTO();
						pagodto=pagos.get(j);
						DtocomercialTbltipopagoDTO tipo=new DtocomercialTbltipopagoDTO();
						tipo.setTipoPago(3); //retenci�n
						tipo.setObservaciones("PAGO CON RETENCI�N"); 
						pagodto.setEstado('1');
						//pagodto.setFechaRealPago((lstPagos.getSelectedRecord().getAttributeAsDate("lstFechaP")));
						HashSet<DtocomercialTbltipopagoDTO> tipospago = new HashSet<DtocomercialTbltipopagoDTO>(0);
						tipospago.add(tipo);
						pagodto.setIdEmpresa(Factum.empresa.getIdEmpresa());
						pagodto.setEstablecimiento(Factum.getEstablecimientoCero());
						pagodto.getTbldtocomercial().setTbldtocomercialTbltipopagos(tipospago);
						//pagodto.setValorPagado(pag);
						pagodto.setValor(valor);
						break;
					}
				}
			
				
					
				frmRetencionPago retencion= new frmRetencionPago(result,pagodto , pagos, 0 );
				winRetencion.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	              //  	getService().listarPagosVencidos(objbacklst);
	                	refrescar ();
	            		winRetencion.destroy();  
	                	
	                }  
	            });
			
				//SC.say(lstPagos.getSelectedRecord().getAttribute("idDocumento"));
				VLayout form = new VLayout();  
	            form.setSize("100%","100%"); 
	            form.setPadding(5);  
	            form.setLayoutAlign(VerticalAlignment.BOTTOM);
	            form.addMember(retencion);
	            retencion.btnGuardar.addClickHandler(new ManejadorBotones("pagoreten"));
	            winRetencion.addItem(form);
	            winRetencion.show();
	            lstPagos.removeData(lstPagos.getSelectedRecord());
        		}catch(Exception e){
            		SC.say("ERROR: "+e.getLocalizedMessage()+"----"+e.getMessage());
            	}

        	
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
        	//ipoUsuario = result.getTipoUsuario();
        	//SC.say("Tipo de usuario: "+result.getTipoUsuario());
       	 //DocBase = result;
       	 
        }

        
        
        
        public void onFailure(Throwable caught) {
        	SC.say("No se puede cargar el documento solicitado");
        }
    };

	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	public void refrescar (){
		String indicador="";
		String fechaI="";
		String fechaF="";
		String Tipo="0",estado="";
		String numIngreso="";
		
		
				fechaI=txtFechaI.getDisplayValue();
				fechaF=txtFechaF.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
					Tipo="10";
				}else if(cmbDocumento.getDisplayValue().equals("Factura Grande")){
					Tipo="13";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas y Notas")){
					Tipo="20";
				}
				else if(cmbDocumento.getDisplayValue().equals("Nota de Venta")){
					Tipo="26";
				}
				if(cmbEstado.getDisplayValue().equals("PENDIENTE")){
					estado="0"; //cambiar por 0
				}else{
					estado="1";
				}
			
				
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				idFactura=String.valueOf(txtFactura.getDisplayValue());
				if(idFactura.equals("") && idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,"","",0, objbacklst);
				}else if(!idFactura.equals("") && idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,idFactura,"",1, objbacklst);
				}else if(idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,"",idCliente,2, objbacklst);
				}else if(!idFactura.equals("") && !idCliente.equals("")){
					getService().listarPagos2(fechaI,fechaF,estado,Tipo,idFactura,idCliente,3, objbacklst);
				}
				
	}	
	
	public void mostrarDocumento(Integer tipo) {
		winDocumento = new Window();
		
		if (tipo == 3) {
			tipoS = "Notas de Credito";
		} else if (tipo == 4) {
			tipoS = "Anticipo Clientes";
		} else if (tipo == 5) {
			tipoS = "Retencion";
		} else if (tipo == 15) {
			tipoS = "Anticipo Proveedores";
		}
		winDocumento.setWidth(930);
		winDocumento.setHeight(500);
		winDocumento.setTitle("Escoger " + tipoS);
		winDocumento.setShowMinimizeButton(false);
		winDocumento.setIsModal(true);
		winDocumento.setShowModalMask(true);
		winDocumento.setKeepInParentRect(true);
		winDocumento.centerInPage();
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
				"display", "block");
		winDocumento.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				// idVendedor="";
				// txtVendedor.setValue("");
				winDocumento.destroy();
			}
		});
		VLayout form = new VLayout();
		form.setSize("100%", "100%");
		form.setPadding(5);
		form.setLayoutAlign(VerticalAlignment.BOTTOM);
		listaDocumentos = new frmReporteCaja(tipoS);
		listaDocumentos.setSize("100%", "100%");
		listaDocumentos.cmbDocumento.setValue(tipoS);
		//listaDocumentos.generarReporte();
		listaDocumentos.cmbDocumento.disable();
		form.setMembers(listaDocumentos);
        listaDocumentos.lstReporteCaja.addDoubleClickHandler(new ManejadorDC("documentoPago"));
        //registroPago = listaDocumentos.lstReporteCaja.getSelectedRecord();
        //getService().listarDocTipoPersona(tipo,codigoSeleccionado, callbackDtoTipo);
        
        winDocumento.addItem(form);
        winDocumento.show();
	}
	
	private class ManejadorDC implements com.smartgwt.client.widgets.events.DoubleClickHandler {
		String ind="";
		//public ListGridRecord registro[] = new ListGridRecord[1];
        public ManejadorDC(String ident){
        	this.ind = ident;
        }
        
		public void onDoubleClick(com.smartgwt.client.widgets.events.DoubleClickEvent event) {
			if(ind.equals("documentoPago")){
				//SC.say(tipoS);
				
				registroPago = listaDocumentos.lstReporteCaja.getSelectedRecord();
				if(tipoS.equals("Notas de Credito"))
				{
					if(registroPago.getAttributeAsString("Estado").equals("UTILIZADO"))
					{
						SC.say("El documento ya ha sido utilizado, por favor seleccione otro");
					}
					else if(registroPago.getAttributeAsString("Estado").equals("NO CONFIRMADO")){
						SC.say("El documento no esta confirmado, por favor primero confirmelo");
					}else if(registroPago.getAttributeAsString("Estado").equals("CONFIRMADO")){
						//com.google.gwt.user.client.Window.alert("En cargar documento");
						lstPagos.getSelectedRecord().setAttribute("lstValorAPagar", ""+registroPago.getAttribute("Total"));
						//com.google.gwt.user.client.Window.alert("Al cargar el total "+registroPago.getAttribute("Total"));
//						String[] fechaPar=registroPago.getAttribute("Fecha").replace("-", "/").split("/");
//						String fechaform=String.valueOf(Integer.valueOf(fechaPar[0]))+"/"+String.valueOf(Integer.valueOf(fechaPar[1]))+"/"+
//								String.valueOf(Integer.valueOf(fechaPar[2]));
//						SC.say("F1 "+fechaform);
//						lstPagos.getSelectedRecord().setAttribute("lstFechaP", ""+fechaform);
//						
//						SC.say("Fecha");
//						
//						SC.say("F2 "+String.valueOf(lstPagos.getSelectedRecord().getAttribute("lstFechaP")));
//						SC.say("F3 "+String.valueOf(lstPagos.getSelectedRecord().getAttributeAsDate("lstFechaP")));
						
						//SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
						//lstPagos.getSelectedRecord().setAttribute("lstFechaP", ""+formatter.format(new Date()));
						//Date fechaform =new Date(new Date().getYear(),new Date().getMonth() , new Date().getDate());
						Date fechaform =new Date();
						//com.google.gwt.user.client.Window.alert("Al crear fecha ");
						lstPagos.getSelectedRecord().setAttribute("lstFechaP", fechaform);
						//com.google.gwt.user.client.Window.alert("Despues colocar fecha ");
						//SC.say("F2 "+String.valueOf(lstPagos.getSelectedRecord().getAttributeAsDate("lstFechaP")));
						lstPagos.getSelectedRecord().setAttribute("lstObs", "Nota de Credito, Numero: "+registroPago.getAttribute("NumRealTransaccion"));
						//com.google.gwt.user.client.Window.alert("Observaciones "+registroPago.getAttribute("NumRealTransaccion"));
						winDocumento.destroy();
						winDocumento.hide();
						lstPagos.endEditing();
					}
				}if(tipoS.equals("Anticipo Clientes"))
				{
					if(registroPago.getAttributeAsString("Estado").equals("UTILIZADO"))
					{
						SC.say("El documento ya ha sido utilizado, por favor seleccione otro");
					}
					else if(registroPago.getAttributeAsString("Estado").equals("NO CONFIRMADO")){
						SC.say("El documento no esta confirmado, por favor primero confirmelo");
					}else if(registroPago.getAttributeAsString("Estado").equals("CONFIRMADO")){
						//com.google.gwt.user.client.Window.alert("En cargar documento");
						lstPagos.getSelectedRecord().setAttribute("lstValorAPagar", ""+registroPago.getAttribute("Total"));
						//com.google.gwt.user.client.Window.alert("Al cargar el total "+registroPago.getAttribute("Total"));
//						String[] fechaPar=registroPago.getAttribute("Fecha").replace("-", "/").split("/");
//						String fechaform=String.valueOf(Integer.valueOf(fechaPar[0]))+"/"+String.valueOf(Integer.valueOf(fechaPar[1]))+"/"+
//								String.valueOf(Integer.valueOf(fechaPar[2]));
//						SC.say("F1 "+fechaform);
//						lstPagos.getSelectedRecord().setAttribute("lstFechaP", ""+fechaform);
//						
//						SC.say("Fecha");
//						
//						SC.say("F2 "+String.valueOf(lstPagos.getSelectedRecord().getAttribute("lstFechaP")));
//						SC.say("F3 "+String.valueOf(lstPagos.getSelectedRecord().getAttributeAsDate("lstFechaP")));
						
						//SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
						//lstPagos.getSelectedRecord().setAttribute("lstFechaP", ""+formatter.format(new Date()));
						//Date fechaform =new Date(new Date().getYear(),new Date().getMonth() , new Date().getDate());
						Date fechaform =new Date();
						//com.google.gwt.user.client.Window.alert("Al crear fecha ");
						lstPagos.getSelectedRecord().setAttribute("lstFechaP", fechaform);
						//com.google.gwt.user.client.Window.alert("Despues colocar fecha ");
						//SC.say("F2 "+String.valueOf(lstPagos.getSelectedRecord().getAttributeAsDate("lstFechaP")));
						lstPagos.getSelectedRecord().setAttribute("lstObs", "Anticipo, Numero: "+registroPago.getAttribute("NumRealTransaccion"));
						//com.google.gwt.user.client.Window.alert("Observaciones "+registroPago.getAttribute("NumRealTransaccion"));
						winDocumento.destroy();
						winDocumento.hide();
						lstPagos.endEditing();
					}
				}
				

			}

        }
    }
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

					SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

						public void execute(Boolean value) {
							if (value) {
								com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
								//getService().LeerXML(asyncCallbackXML);
							}
						}
					});
				}else{
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
        		usuario=result;
        		//SC.say(usuario.getUserName());
        	//funcionBloquear(true);
        		}
        }

        public void onFailure(Throwable caught) {
            
        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };

}
