package com.giga.factum.client;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.EquipoDTO;
import com.giga.factum.client.DTO.EquipoOrdenDTO;
import com.giga.factum.client.DTO.OrdenDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.types.Alignment;  
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.Page;


public class Cuerpo extends VLayout {
	HLayout hLayoutCerrarSesion=new HLayout();
	HLayout hLayout=new HLayout();
	HLayout hLayoutMenu = new HLayout();
	VLayout layout=new VLayout();

	public com.smartgwt.client.widgets.Label docNom;
	HLayout nombreDoc = new HLayout();
		
	private final HStack hStack = new HStack();
	private OrdenDTO tmporden;
	DynamicForm form;
	DynamicForm formIn;
	LinkItem linkItem;
	LinkItem linkItemIn;
	MenuPrincipal menuPrincipal;
	//Integer idVendedor = 0;
	HashMap<String, com.smartgwt.client.widgets.menu.MenuItem> MapMenu = new HashMap<String, com.smartgwt.client.widgets.menu.MenuItem>();
	public HashMap<String, com.smartgwt.client.widgets.menu.MenuItem> getMapMenu() {
		return MapMenu;
	}

	public void setMapMenu(HashMap<String,com.smartgwt.client.widgets.menu.MenuItem> mapMenu) {
		MapMenu = mapMenu;
	}

	public Cuerpo() {
		//com.google.gwt.user.client.Window.alert("Constructor cuerpo");
		hLayout.setShowEdges(true);
		hLayoutMenu.setShowEdges(false);
		hLayoutMenu.setAnimateMembers(true);
		hLayoutMenu.setAlign(Alignment.LEFT);
		menuPrincipal = new MenuPrincipal();
		
		//+++++++++MANEJADORES PAGOS++++++++++++
		menuPrincipal.menupagosProveedores.addClickHandler(new ManejadorBotones("PagosProveedores"));
		menuPrincipal.menupagoss.addClickHandler(new ManejadorBotones("Pagos"));
		if(Factum.banderaMicroServicios==1) menuPrincipal.menugastos.addClickHandler(new ManejadorBotones("Gastos"));
		menuPrincipal.menuingresos.addClickHandler(new ManejadorBotones("Ingresos"));
		menuPrincipal.menuCuadreCaja.addClickHandler(new ManejadorBotones("caja"));
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuPrincipalPagos.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		
		//++++++++++++++++++++++++++++++++++++++
		
		
		//+++++++++++++++++++MANEJADORES VENTAS+++++++++++++++++
		//com.google.gwt.user.client.Window.alert("Constructor cuerpo MENU VENTAS");
		menuPrincipal.menuFact.addClickHandler(new ManejadorBotones("factura"));
		menuPrincipal.menuNotaVentas.addClickHandler(new ManejadorBotones("Nota de Venta"));
		if(Factum.banderaMenuProforma==1){
		menuPrincipal.menuProforma.addClickHandler(new ManejadorBotones("Proforma"));
		}
		if(Factum.banderaMenuCuentasPorCobrar==1){
		menuPrincipal.menuCuentasPorCobrar.addClickHandler(new ManejadorBotones("Cuentas por Cobrar"));		
		}
		menuPrincipal.menuNotaCredito.addClickHandler(new ManejadorBotones("Nota de Credito"));
		menuPrincipal.menuAnticipoCliente.addClickHandler(new ManejadorBotones("Anticipos Clientes"));
		menuPrincipal.menuOperacionesAnticipoCliente.addClickHandler(new ManejadorBotones("Operaciones Anticipos Clientes"));
		menuPrincipal.menuNotaEntrega.addClickHandler(new ManejadorBotones("Nota de Entrega"));
		menuPrincipal.menuRet.addClickHandler(new ManejadorBotones("retencion"));
		//+++++++++++++++++++++++++++++++++++++++++++++++
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuVentas.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		
		//+++++++++MANEJADORES REPORTES++++++++++++
			// menuReportesClientes.addClickHandler(new ManejadorBotones("ReportesClientes"));
		menuPrincipal.menuReporte.addClickHandler(new ManejadorBotones("Reportes"));
		menuPrincipal.menuReportesEmpleados.addClickHandler(new ManejadorBotones("ReportesVentasEmpleado"));
		menuPrincipal.menuReportesProductoProveedor.addClickHandler(new ManejadorBotones("Reportes Producto por Proveedor"));
		menuPrincipal.menuReportesProductoCliente.addClickHandler(new ManejadorBotones("Reportes Producto por Cliente"));
		menuPrincipal.menuReportesCliente.addClickHandler(new ManejadorBotones("Reportes Cliente"));
		menuPrincipal.menuReportesUtilidadDetalle.addClickHandler(new ManejadorBotones("ReportesUtilidadDetalle"));
		menuPrincipal.menuReporteCaja.addClickHandler(new ManejadorBotones("Listado de Documentos"));
		
		menuPrincipal.menuDocComercial.addClickHandler(new ManejadorBotones("Documentos Comerciales"));
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuReportes.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		
		//+++++++++++++++++++++++MANEJAORES TALLER+++++++++++++++++++++++++++
		if(Factum.banderaMenuTaller==1){
		menuPrincipal.menuTaller.addClickHandler(new ManejadorBotones("Taller"));//agregar a manejadores
		}
		/*if(Factum.banderaMenuTaller==1){
		menuPrincipal.ordenMant.addClickHandler(new ManejadorBotones("ordenMantenimiento"));
		menuPrincipal.crudMarcas.addClickHandler(new ManejadorBotones("crudMarcas"));
		menuPrincipal.crudEquipos.addClickHandler(new ManejadorBotones("crudEquipos"));
		menuPrincipal.reporteOrdenes.addClickHandler(new ManejadorBotones("reporteOrdenes"));
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuTaller.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		}*/
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		//++++++++++++++++++++++++++MANEJADORES CONTABILIDAD 2+++++++++++++++	
		
		if(Factum.banderaMenuContabilidad==1){
		if(Factum.banderaMicroServicios==1) menuPrincipal.menuReporteRetenciones.addClickHandler(new ManejadorBotones("Reporte Retenciones"));
    	menuPrincipal.menuListaRetenciones.addClickHandler(new ManejadorBotones("Lista Retenciones"));
    	if(Factum.banderaMicroServicios==1) menuPrincipal.menuATS.addClickHandler(new ManejadorBotones("ATS"));	
    	menuPrincipal.menuMayorizacion.addClickHandler(new ManejadorBotones("Mayorizacion"));
    	menuPrincipal.menuComprobacion.addClickHandler(new ManejadorBotones("Comprobacion"));
    	menuPrincipal.menuBalanceGeneral.addClickHandler(new ManejadorBotones("Balance General"));
    	menuPrincipal.menuEstadoResultados.addClickHandler(new ManejadorBotones("Estado Resultados"));
    	menuPrincipal.menuAsientosManuales.addClickHandler(new ManejadorBotones("Asientos Manuales"));
    	if(Factum.banderaMenuLibroDiario==1){
    		menuPrincipal.menuLibroDiario.addClickHandler(new ManejadorBotones("Libro Diario"));
    		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuContabilidad.getMenu().getItems()){
    			MapMenu.put(item.getTitle(),item);
    		}
    	}
	    }
		//+++++++++++++++++++++MANEJADORES COMPRAS+++++++++++++++++++++++++++
		//com.google.gwt.user.client.Window.alert("Constructor cuerpo MENU COMPRAS");
		menuPrincipal.menuFactCompra.addClickHandler(new ManejadorBotones("factura compra"));
		menuPrincipal.menuNotaVentasCompras.addClickHandler(new ManejadorBotones("Nota de Venta en Compras"));
		menuPrincipal.menuNotaCompra.addClickHandler(new ManejadorBotones("nota compra"));
		menuPrincipal.menuNotaCreditoProveedor.addClickHandler(new ManejadorBotones("nota credito proveedores"));
		menuPrincipal.menuAnticipoProveedor.addClickHandler(new ManejadorBotones("anticipo proveedores"));
		menuPrincipal.menuOpAntPro.addClickHandler(new ManejadorBotones("Operaciones Anticipos Proveedores"));
		menuPrincipal.menuRetCompra.addClickHandler(new ManejadorBotones("retencionCompras"));
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuCompras.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		
		//+++++++++++++++++++++MANEJADORES INVENTARIO++++++++++++++++++++++++
		menuPrincipal.menuUnidad.addClickHandler(new ManejadorBotones("unidad"));
		menuPrincipal.menuMarca.addClickHandler(new ManejadorBotones("Marca"));		
		menuPrincipal.menuSeries.addClickHandler(new ManejadorBotones("Series"));
		menuPrincipal.menuProducto.addClickHandler(new ManejadorBotones("producto"));
		menuPrincipal.menuServicio.addClickHandler(new ManejadorBotones("Servicio"));
		menuPrincipal.menuCategoria.addClickHandler(new ManejadorBotones("Categoria"));
		if(Factum.banderaMicroServicios==1) menuPrincipal.menuListadoBasico.addClickHandler(new ManejadorBotones("ListadoBasico"));
		if(Factum.banderaCatalogoProductos==1) menuPrincipal.menuCatalogo.addClickHandler(new ManejadorBotones("Catalogo"));
		//////////////////
		
		if(Factum.banderaMenuBodega==1){
		menuPrincipal.menuBodega.addClickHandler(new ManejadorBotones("Bodega"));
		menuPrincipal.menuTraspasoBodega.addClickHandler(new ManejadorBotones("Traspaso de Bodegas"));
		/*for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuBodega.getSubmenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}*/
		}
		if(Factum.banderaMicroServicios==1) menuPrincipal.menuAjusteInventario.addClickHandler(new ManejadorBotones("Ajuste de Inventario"));
		if(Factum.banderaMicroServicios==1) menuPrincipal.menuKardex.addClickHandler(new ManejadorBotones("Kardex"));
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.productosItem.getSubmenu().getItems()){
		MapMenu.put(item.getTitle(),item);
	    }
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		//+++++++++++++++++++++MANEJADORES PERSONAS+++++++++++++++++++++++++++
		//menuPrincipal.menuPersonas.addClickHandler(new ManejadorBotones("personas"));
		
		//menuPrincipal.menuCliente.addClickHandler(new ManejadorBotones("cliente"));
		//menuPrincipal.menuProveedor.addClickHandler(new ManejadorBotones("proveedor"));
		menuPrincipal.menuUsuario.addClickHandler(new ManejadorBotones("usuario"));
		
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuPersonas.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		
		//+++++++++++++++++++++MANEJADORES BACKUP+++++++++++++++++++++++++++++
		if(Factum.banderaMenuBackup==1){
	    menuPrincipal.menuBackup.addClickHandler(new ManejadorBotones("backup"));
		}
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
		//+++++++++++++++++++++MANEJADORES CONFIGURACIONES+++++++++++++++++++++++++++++
	    menuPrincipal.menuEmpresa.addClickHandler(new ManejadorBotones("empresa"));
	    menuPrincipal.menuTcuenta.addClickHandler(new ManejadorBotones("Tipo de Cuenta"));
	    menuPrincipal.menuCuenta.addClickHandler(new ManejadorBotones("Cuenta"));
	    menuPrincipal.menuPcuenta.addClickHandler(new ManejadorBotones("Plan de Cuentas"));
	    menuPrincipal.menuImpuesto.addClickHandler(new ManejadorBotones("Impuesto"));
	    menuPrincipal.menuCargo.addClickHandler(new ManejadorBotones("Cargos Personas"));
	    menuPrincipal.menuPrintFac.addClickHandler(new ManejadorBotones("PrintFact"));
	    menuPrincipal.menuAsoCuentas.addClickHandler(new ManejadorBotones("Aso"));
	    menuPrincipal.menuTipoPrecio.addClickHandler(new ManejadorBotones("Tipo Precio"));
	    menuPrincipal.menuFormasPago.addClickHandler(new ManejadorBotones("Formas de Pago"));
	   for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuSubCuenta.getSubmenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
	   
	   for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuSubCargo.getSubmenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	  for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuConfiguraciones.getMenu().getItems()){
			MapMenu.put(item.getTitle(),item);
		}
	    
		//+++++++++++++++++++++MANEJADORES RESTAURANT+++++++++++++++++++++++++++++
	    /*if(Factum.banderaMenuRestaurant==1){
	    menuPrincipal.menuPedido.addClickHandler(new ManejadorBotones("Pedido"));
	    menuPrincipal.menuMesa.addClickHandler(new ManejadorBotones("Mesa"));
	    menuPrincipal.menuArea.addClickHandler(new ManejadorBotones("Area"));
		for (com.smartgwt.client.widgets.menu.MenuItem item: menuPrincipal.menuRestaurant.getMenu().getItems()){
		MapMenu.put(item.getTitle(),item);
		}
	    }*/

		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	    //+++++++++++++++++++++MANEJADORES SERVICIOS+++++++++++++++++++++++++++++
	    if(Factum.banderaMenuCore==1){
	    menuPrincipal.menuCore.addClickHandler(new ManejadorBotones("Core"));
	 
	    }
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    //+++++++++++++++++++++MANEJADORES ELECTRONICOS+++++++++++++++++++++++++++++
	    /*
	    if(Factum.banderaMenuFacturacionElectronica==1){
	    menuPrincipal.menuReporteElectronicos.addClickHandler(new ManejadorBotones("Electronicos"));	    
	    }
	    */
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	    
		Page page = new Page();
		// Tama�o de pagina y de formularios
//		hLayoutMenu.setSize("100%", String.valueOf(page.getHeight()/60));
		hLayoutMenu.setSize("100%", "3%");
		
		
		hLayoutMenu.addMember(menuPrincipal);
		nombreDoc.setBackgroundImage(Factum.banderaNombreLogo);
		nombreDoc = new HLayout();
		
		hLayoutMenu.addMember(nombreDoc);
		
		formIn = new DynamicForm();
		
		linkItemIn = new LinkItem("linkIn");
        linkItemIn.setLinkTitle("<h1><b id=\"para1\">INICIO</b></h1>");
        linkItemIn.setShowTitle(false);
	    formIn.setFields(linkItemIn);
	    
	    formIn.setBackgroundImage(Factum.banderaNombreLogo);
	    hLayoutMenu.addMember(formIn);
	    
	    form = new DynamicForm();
	    
        linkItem = new LinkItem("link");
        linkItem.setLinkTitle("<h1><b id=\"para1\">CERRAR SESION</b></h1>");
        linkItem.setShowTitle(false);
	    form.setFields(linkItem);
	    //form.setOpacity(0);
	    
	    form.setBackgroundImage(Factum.banderaNombreLogo);
	    
	    hLayoutMenu.setBackgroundImage(Factum.banderaNombreLogo);
	    hLayoutMenu.addMember(form);
	    addMember(hLayoutMenu);
		
		hLayout.setSize("100%", String.valueOf(page.getHeight()-(page.getHeight()/10)));
		//hLayout.setAnimateMembers(false);
		hLayout.setAlign(Alignment.CENTER);
		setAlign(Alignment.CENTER);
		addMember(hLayout);
		
		if(Factum.Menu!=null){
    		switch(Integer.valueOf(Factum.Menu)){
    		case 0:
    			facturar();				
    		break;
    		case 1:
    			Integer idOrden = Integer.parseInt(Factum.idorden);
				getService().listarEquipoOrdenFiltroID(idOrden,objbackListOrdenID);				
    		break;
    		case 2:
    			notaEntrega();
    			break;
    		case 3:
    			notaCredito();
    			break;
    		case 4:
    			anticipoClientes();
    			break;
    		case 5:
    			retencion();
    			break;
    		case 6:
    			facturaCompra();
    			break;
    		case 7:
    			compraNota();
    			break;
    		case 8:
    			notaCreditoProveedores();
    			break;
    		case 9:
    			anticipoProveedores();
    			break;
    		case 10:
    			operacionesAnticipos("1");//id proveedores
    			break;
    		case 11:
    			pagos();
    			break;
    		case 12:
    			pagosProveedores();
    			break;
    		case 13:
    			gastos();
    			break;
    		case 14:
    			ingresos();
    			break;
    		case 15:
    			caja();
    			break;
    		case 18:
    			unidad();
    			break;
    		case 19:
    			producto();
    			break;
    		case 20:
    			categoria();
    			break;
    		case 21:
    			ajusteInventario();
    			break;
    		case 22:
    			kardex();
    			break;
    		case 23:
    			//personas();
    			break;
    		case 24:
    			reportes();
    			break;
    		case 25:
    			reporteCaja();
    			break;
    		case 26:
    			reportesEmpleados();
    			break;	
    		case 27:
    			reportesUtilidadDetalle();
    			break;
    		case 28:
    			ATSVentas();
    			break;
    		case 29:
    			actualizarIva();
    			break;					
    		case 30:
    			cliente();
    			break;
    		case 31:
    			proveedor();
    			break;
    		case 32:
    			usuario();
    			break;
    		}	 
            
        } 
		//com.google.gwt.user.client.Window.alert("Constructor cuerpo FIN MENU COMPRAS");
		
	}
	public static class EmployeeTreeNode extends TreeNode {  
		  
        public EmployeeTreeNode(String employeeId, String reportsTo, String name) {  
            setEmployeeId(employeeId);  
            setReportsTo(reportsTo);  
            setName(name);  
        }  
  
        public void setEmployeeId(String value) {  
            setAttribute("EmployeeId", value);  
        }  
  
        public void setReportsTo(String value) {  
            setAttribute("ReportsTo", value);  
        }  
  
        public void setName(String name) {  
            setAttribute("Name", name);  
        }
        public String getName(){
    		return getAttributeAsString("Name");
    	}
    }
	public void impuesto(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmImpuesto impuesto=new frmImpuesto();
		impuesto.setSize("100%","98%");
		hLayout.addMember(impuesto);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void bodega(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmBodega bod=new frmBodega();
		bod.setSize("100%","98%");
		hLayout.addMember(bod);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void producto(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		
		hLayout.setMembers(layout);
		frmProducto Producto=new frmProducto();
		Producto.setLeft(25);
		Producto.setSize("100%","98%");
		hLayout.addMember(Producto);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void ATSVentas()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmATS reporteATS =new FrmATS(0);
		reporteATS.setSize("100%","98%");
		hLayout.addMember(reporteATS);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void retencion(){//cambiar por algo v�lido
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
//		DtocomercialDTO doc=new DtocomercialDTO();
//		doc.setIdDtoComercial(4);
//		doc.setSubtotal(100);
//		doc.setAutorizacion("1111111"); 
//		doc.setEstado('1');
//		PersonaDTO per=new PersonaDTO(); 
//		per.setIdPersona(4);
//		doc.setTblpersonaByIdPersona(per);
//		doc.setTblpersonaByIdVendedor(per);
//		doc.setTipoTransaccion(2);
//		doc.setFecha("01/10/2012");
//		doc.setExpiracion("01/10/2012");		
//		frmDetRetencion ret=new frmDetRetencion(doc);
		frmDetRetencion ret=new frmDetRetencion(0);
		ret.setSize("100%","98%");
		hLayout.addMember(ret);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		ret.setCanFocus(true);
	}
	public void retencionCompra(){//cambiar por algo v�lido
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
//		DtocomercialDTO doc=new DtocomercialDTO();
//		doc.setIdDtoComercial(4);
//		doc.setSubtotal(100);
//		doc.setAutorizacion("1111111"); 
//		doc.setEstado('1');
//		PersonaDTO per=new PersonaDTO(); 
//		per.setIdPersona(4);
//		doc.setTblpersonaByIdPersona(per);
//		doc.setTblpersonaByIdVendedor(per);
//		doc.setTipoTransaccion(2);
//		doc.setFecha("01/10/2012");
//		doc.setExpiracion("01/10/2012");		
//		frmDetRetencion ret=new frmDetRetencion(doc);
		frmDetRetencion ret=new frmDetRetencion(1);
		ret.setSize("100%","98%");
		hLayout.addMember(ret);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		ret.setCanFocus(true);
	}
	public void cuentasPorCobrar(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCuentasPorCobrar cuentasPC=new frmCuentasPorCobrar();
		cuentasPC.setSize("100%","98%");
		hLayout.addMember(cuentasPC);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		cuentasPC.setCanFocus(true);
	}
	public void cargo(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCargo cargo=new frmCargo();
		cargo.setSize("100%","98%");
		hLayout.addMember(cargo);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		cargo.setCanFocus(true);
	}
	public void gastos(){
		
//
		VLayout layout =new VLayout();
		layout.setSize("1%", "100%");
		hLayout.setMembers(layout); 
		frmGasto kardex =new frmGasto("-1",String.valueOf(Factum.idusuario));
		kardex.setSize("100%","98%");
		hLayout.addMember(kardex);
		
//		VLayout layout =new VLayout();
//		layout.setSize("10px", "100%");
//		//layout.addMember(treeGrid);
//		hLayout.setMembers(layout);
//		Factura gastos=new Factura(7,-1);
//		gastos.setSize("80%","100%");
//		hLayout.addMember(gastos);
//		VLayout layoutD =new VLayout();
//		layout.setSize("10px", "100%");
//		hLayout.addMember(layoutD);
//		hLayout.setCanFocus(true);
//		gastos.setCanFocus(true);
//		gastos.setcuerpo (this);
	}
	public void ingresos(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura ingresos=new Factura(12,-1);
		ingresos.setSize("100%","98%");
		hLayout.setMembers(ingresos);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		hLayout.setCanFocus(true);
		ingresos.setCanFocus(true);
		ingresos.setcuerpo (this);
		
	}
	public void facturar(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(0,-1);
		//frmFactura factura=new frmFactura(null);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);	
		hLayout.setCanFocus(true);
		factura.setCanFocus(true);
		factura.setcuerpo (this);
		
	
	}
	
	public void facturarOrden(OrdenDTO orden, ClienteDTO cliente){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(0,-1,orden,cliente);
		//frmFactura factura=new frmFactura(null);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);	
		hLayout.setCanFocus(true);
		factura.setCanFocus(true);
		factura.setcuerpo (this);
		
	
	}
	
public void facturarGrande(){
		
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(13,-1);
		//frmFactura factura=new frmFactura(null);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);	
	}
	

	public void proforma(){
	
	VLayout layout =new VLayout();
	layout.setSize("10px", "100%");
	//layout.addMember(treeGrid);
	hLayout.setMembers(layout);
	Factura factura =new Factura(20,-1);//el -1 significa que es un nuevo documento
	//frmFactura factura=new frmFactura(null);
	factura.setSize("100%","98%");
	hLayout.addMember(factura);
	VLayout layoutD =new VLayout();
	layoutD.setSize("10px", "100%");
	hLayout.addMember(layoutD);	
	hLayout.setCanFocus(true);
	factura.setCanFocus(true);
	factura.setcuerpo(this);
	}
	public void cuentasporcobrar(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCuentasPorCobrar cuentasPC=new frmCuentasPorCobrar();
		cuentasPC.setSize("100%","98%");
		hLayout.addMember(cuentasPC);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}

	public void reporteCaja(){
		try{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmReporteCaja reportecaja=new frmReporteCaja("Facturas de Venta");
		reportecaja.cmbDocumento.setValue("Facturas de Venta");
		reportecaja.Tipo="0";
		reportecaja.setSize("100%","98%");
		hLayout.addMember(reportecaja);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		}
		catch(Exception e){SC.say("Error: "+e);}
	}
	public void reporteDocComercial(){
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":9999/reportes/document_commercial", "_blank", "");
	}
	
	public void imprimir(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmLstBodPre imprimi=new frmLstBodPre(null,0);
		imprimi.setSize("100%","98%");
		hLayout.addMember(imprimi);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void categoria(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCategoriaa cat=new frmCategoriaa();
		cat.setSize("100%","98%");
		hLayout.addMember(cat);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void unidad(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmUnidad Empleado=new frmUnidad();
		Empleado.setSize("100%","98%");
		hLayout.addMember(Empleado);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void personas(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmPersonas Empleado=new frmPersonas();
		Empleado.setSize("100%","98%");
		hLayout.addMember(Empleado);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void cliente(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCliente2 cliente=new frmCliente2();
		cliente.setLeft(25);
		cliente.setSize("100%","95%");
		//setMembers(dynamicForm_1);
		hLayout.addMember(cliente);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/crud/cliente", "Cliente", null);
	}
	public void proveedor(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmProveedor2 proveedor=new frmProveedor2();
		proveedor.setSize("100%","95%");
		//setMembers(dynamicForm_1);
		hLayout.addMember(proveedor);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/crud/proveedor", "Proveedor", null);
	}
	public void usuario(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmEmpleado empleado =new frmEmpleado();
		empleado.setSize("100%","95%");
		//setMembers(dynamicForm_1);
		hLayout.addMember(empleado);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void backup(){
		//SC.say(Factum.banderaUsuarioBD);
		getService().backupDB(false,Factum.banderaNombreBD, Factum.banderaPasswordBD, Factum.banderaUsuarioBD,Factum.banderaSO, Factum.banderaPasswordComprimido, Factum.destinoBackup, callback);
		//getService().backupDB(false, callback);
	}
	public void taller(){
		String url = GWT.getHostPageBaseURL() ;
		//com.google.gwt.user.client.Window.alert(url+ " http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/ordenTaller/orden ");
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/ordenTaller/orden", "Taller", null);
	}
	public void configuracion(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmConfiguracion Configuracion=new frmConfiguracion();
		Configuracion.setSize("100%","98%");
		hLayout.addMember(Configuracion);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void tipoPrecio(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmTipoPrecio Empleado=new frmTipoPrecio();
		Empleado.setSize("100%","98%");
		hLayout.addMember(Empleado);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void formaPago(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmFormaPago FormaPago=new frmFormaPago();
		FormaPago.setSize("100%","98%");
		hLayout.addMember(FormaPago);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void empresa(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmEmpresa Empresa=new frmEmpresa();
		Empresa.setSize("100%","98%");
		hLayout.addMember(Empresa);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void cuenta(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCuenta Cuenta=new frmCuenta();
		Cuenta.setSize("100%","98%");
		hLayout.addMember(Cuenta);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void tipoCuenta(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmTipoCuenta TCuenta=new frmTipoCuenta();
		TCuenta.setSize("100%","98%");
		hLayout.addMember(TCuenta);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void plancuenta(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmPlanCuenta PCuenta=new frmPlanCuenta();
		PCuenta.setSize("100%","98%");
		hLayout.addMember(PCuenta);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void facturaCompra(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.setMembers(layout);
		Factura factura =new Factura(1,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		
		factura.setCanFocus(true);
		factura.setcuerpo (this);
	}
	public void compraNota(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(10,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void notaEntrega(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(2,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		hLayout.setCanFocus(true);
		factura.setCanFocus(true);
		factura.setcuerpo (this);
	}
	public void notaVentas(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(26,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		hLayout.setCanFocus(true);
		factura.setCanFocus(true);
		factura.setcuerpo (this);
	}
	public void notaVentasCompras(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(27,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		hLayout.setCanFocus(true);
		factura.setCanFocus(true);
		factura.setcuerpo (this);
	}
	public void notaCredito(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(3,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void anticipoClientes(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(4,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void notaCreditoProveedores(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(14,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void anticipoProveedores(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		Factura factura =new Factura(15,-1);
		factura.setSize("100%","98%");
		hLayout.addMember(factura);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void ajusteInventario(){
		/*VLayout layout =new VLayout();
		layout.setSize("10px", "100%");*/
		//layout.addMember(treeGrid);
		//hLayout.setMembers(layout);
		//Factura factura =new Factura(8,-1);
		//factura.setSize("80%","100%");
		//hLayout.addMember(factura);
		/*VLayout layoutD =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.addMember(layoutD);*/
		//Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/InventarioInicialConsignacion/Inventario/page","_blank","enabled");
		VLayout layout =new VLayout();
		
		layout.setSize("1%", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmAjuste kardex =new frmAjuste("-1",String.valueOf(Factum.idusuario));
		kardex.setSize("100%","98%");
		hLayout.addMember(kardex);
		/*VLayout layoutD =new VLayout();
		layout.setSize("5%", "100%");
		hLayout.addMember(layoutD);*/
	}
	public void pagos(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmPagos pagos =new frmPagos();
		pagos.setSize("100%","98%");
		hLayout.addMember(pagos);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void pagosProveedores(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmPagosProveedores pagosPro =new frmPagosProveedores();
		pagosPro.setSize("100%","98%");
		hLayout.addMember(pagosPro);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void caja(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmCuadreCaja caja =new frmCuadreCaja();
		caja.setSize("100%","98%");
		hLayout.addMember(caja);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void factPrint(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FactPrint factPrint =new FactPrint();
		factPrint.setSize("100%","98%");
		hLayout.addMember(factPrint);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void reportesProductosProveedor()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmProductosProveedor reporteProdProveedor =new FrmProductosProveedor();
		reporteProdProveedor.setSize("100%","98%");
		hLayout.addMember(reporteProdProveedor);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportesRetenciones()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmReporteRetenciones reporteRetenciones =new FrmReporteRetenciones();
		reporteRetenciones.setSize("100%","98%");
		hLayout.addMember(reporteRetenciones);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	//++++++++++   PARA REALIZAR OPERACIONES CON CLIENTES Y CON PROVEEDORES
	public void operacionesAnticipos(String tipoPersona)
	{
		//tipoPersona  puede ser "Clientes" o "Proveedores"
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmOperacionesAnticipos opAnticipos =new FrmOperacionesAnticipos(tipoPersona);
		//opAnticipos.setSize("80%","100%");
		hLayout.addMember(opAnticipos);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	
	public void reportes(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmReportes frmreportes =new frmReportes();
		frmreportes.setSize("100%","98%");
		hLayout.addMember(frmreportes);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void reportesClientes(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmReportesClientes frmreportesCli =new frmReportesClientes();
		frmreportesCli.setSize("100%","98%");
		hLayout.addMember(frmreportesCli);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void aso(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		AsoCuentas aso =new AsoCuentas();
		aso.setSize("100%","98%");
		hLayout.addMember(aso);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportesEmpleados(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmReporteEmpleado reporteEmp =new frmReporteEmpleado();
		reporteEmp.setSize("100%","98%");
		hLayout.addMember(reporteEmp);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void actualizarIva(){
		
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmIVA reporteIVA =new FrmIVA();
		reporteIVA.setSize("100%","98%");
		hLayout.addMember(reporteIVA);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportesUtilidadDetalle(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmUtilidadCosto reporteEmp =new frmUtilidadCosto();
		reporteEmp.setSize("100%","98%");
		hLayout.addMember(reporteEmp);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void kardex(){
		/*VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmKardex kardex =new frmKardex();
		kardex.setSize("80%","100%");
		hLayout.addMember(kardex);
		VLayout layoutD =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.addMember(layoutD);
		*/
		
		//Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Kardex/Kardex/page","_blank","enabled");
		
		
		
		VLayout layout =new VLayout();
		
		layout.setSize("1%", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmKardex kardex =new frmKardex();
		kardex.setSize("100%","98%");
		hLayout.addMember(kardex);
		//VLayout layoutD =new VLayout();
		//layout.setSize("5%", "100%");
		//hLayout.addMember(layoutD);
		
		 
		

		
	}
	public void listadoBasico(){
		
		VLayout layout =new VLayout();
		
		layout.setSize("1%", "100%");

		hLayout.setMembers(layout); 
		frmListadoBasico kardex =new frmListadoBasico();
		kardex.setSize("100%","98%");
		hLayout.addMember(kardex);

	}
	public void catalogo(){
		VLayout layout =new VLayout();
		
		layout.setSize("1%", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmListaCatalogo kardex =new frmListaCatalogo();
		kardex.setSize("100%","98%");
		hLayout.addMember(kardex);
	}
	public void pedido(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.setMembers(layout); 
		frmPedido pedido =new frmPedido();
		pedido.setSize("100%","98%");
		hLayout.addMember(pedido);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}	
	public void mesa(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.setMembers(layout); 
		frmMesa mesa =new frmMesa();
		mesa.setSize("100%","98%");
		hLayout.addMember(mesa);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}	
	public void area(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.setMembers(layout); 
		frmArea area =new frmArea();
		area.setSize("100%","98%");
		hLayout.addMember(area);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}	
	public void core(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmCore Core=new frmCore();
		Core.setSize("100%","98%");
		hLayout.addMember(Core);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void servicio(){
		
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmServicio Servicio=new frmServicio();
		Servicio.setSize("100%","98%");
		hLayout.addMember(Servicio);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	/*
	public void reporteElectronicos(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmReporteElectronicos reporteElectronicos=new frmReporteElectronicos("0");
		reporteElectronicos.setSize("80%","100%");
		hLayout.addMember(reporteElectronicos);
		VLayout layoutD =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	*/
	public void reporteRetenciones()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmReporteRetenciones reporteRetenciones =new FrmReporteRetenciones();
		reporteRetenciones.setSize("100%","98%");
		hLayout.addMember(reporteRetenciones);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void listaRetenciones()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmListaRetenciones listaRetenciones =new frmListaRetenciones();
		listaRetenciones.setSize("100%","98%");
		hLayout.addMember(listaRetenciones);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}

	
	private void mayorizacion() {
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		hLayout.setMembers(layout); 
		frmMayorizacion mayorizacion =new frmMayorizacion();
		mayorizacion.setSize("90%","100%");
		hLayout.addMember(mayorizacion);
		VLayout layoutD =new VLayout();
		layoutD.setSize("30%", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void comprobacion()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmATS reporteATS =new FrmATS(2);
		reporteATS.setSize("100%","98%");
		hLayout.addMember(reporteATS);
		VLayout layoutD =new VLayout();
		layoutD.setSize("30%", "100%");
		hLayout.addMember(layoutD);
	}
	public void balanceGeneral()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmATS reporteATS =new FrmATS(3);
		reporteATS.setSize("100%","98%");
		hLayout.addMember(reporteATS);
		VLayout layoutD =new VLayout();
		layoutD.setSize("30%", "100%");
		hLayout.addMember(layoutD);
	}	
	public void estadoResultados()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmATS reporteATS =new FrmATS(4);
		reporteATS.setSize("100%","98%");
		hLayout.addMember(reporteATS);
		VLayout layoutD =new VLayout();
		layoutD.setSize("30%", "100%");
		hLayout.addMember(layoutD);
	}
	public void ajusteAsiento()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmAjusteAsiento AjusteAsiento =new frmAjusteAsiento();
		AjusteAsiento.setSize("100%","98%");
		hLayout.addMember(AjusteAsiento);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);

		
		
		
		
	}
	public void marca(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmMarca frmarca=new frmMarca();
		frmarca.setSize("100%","98%");
		hLayout.addMember(frmarca);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void traspasobodega(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmTraspasoBodega frmtraspaso =new frmTraspasoBodega();
		frmtraspaso.setSize("100%","98%");
		hLayout.addMember(frmtraspaso);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void serie(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmSerie frmserie =new frmSerie(/*null,2,null,1*/null,0);
		frmserie.setSize("100%","98%");
		hLayout.addMember(frmserie);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	
	public void formapago(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmFormaPago FormaPago=new frmFormaPago();
		FormaPago.setSize("100%","98%");
		hLayout.addMember(FormaPago);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void tipoprecio(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout);
		frmTipoPrecio frmtipoprecio=new frmTipoPrecio();
		frmtipoprecio.setSize("100%","98%");
		hLayout.addMember(frmtipoprecio);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void librodiario(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmLibroDiario frmlibrodiario =new frmLibroDiario();
		frmlibrodiario.setSize("100%","98%");
		hLayout.addMember(frmlibrodiario);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportesproductosproveedor()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmProductosProveedor reporteProdProveedor =new FrmProductosProveedor();
		reporteProdProveedor.setSize("100%","98%");
		hLayout.addMember(reporteProdProveedor);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportesproductoscliente()
	{
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		FrmProductosCliente reporteProdCliente =new FrmProductosCliente();
		reporteProdCliente.setSize("100%","98%");
		hLayout.addMember(reporteProdCliente);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	public void reportescliente(){
		VLayout layout =new VLayout();
		layout.setSize("10px", "100%");
		//layout.addMember(treeGrid);
		hLayout.setMembers(layout); 
		frmReportesClientes frmreportesCli =new frmReportesClientes();
		frmreportesCli.setSize("100%","98%");
		hLayout.addMember(frmreportesCli);
		VLayout layoutD =new VLayout();
		layoutD.setSize("10px", "100%");
		hLayout.addMember(layoutD);
	}
	private class ManejadorBotones implements com.smartgwt.client.widgets.menu.events.ClickHandler, ClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		@Override
		public void onClick(MenuItemClickEvent event) {
			
			if(indicador.equalsIgnoreCase("Empresa")){
				empresa();
			}else if(indicador.equalsIgnoreCase("producto")){
				producto();
			}else if(indicador.equalsIgnoreCase("Traspaso de Bodegas")){
				traspasobodega();
			}else if(indicador.equalsIgnoreCase("Reportes Producto por Proveedor")){
				reportesproductosproveedor();
			}else if(indicador.equalsIgnoreCase("Reportes Producto por Cliente")){
				reportesproductoscliente();
			}else if(indicador.equalsIgnoreCase("Reportes Cliente")){
				reportescliente();
			}else if(indicador.equalsIgnoreCase("Libro Diario")){
				librodiario();
			}else if(indicador.equalsIgnoreCase("Tipo Precio")){
				tipoprecio();
			}else if(indicador.equalsIgnoreCase("Formas de Pago")){
				formapago();
			}else if(indicador.equalsIgnoreCase("Series")){
				serie();
			}else if(indicador.equalsIgnoreCase("categoria")){
				categoria();
			}else if(indicador.equalsIgnoreCase("unidad")){
				unidad();
			}else if(indicador.equalsIgnoreCase("Cuentas por Cobrar")){
				cuentasporcobrar();
			}else if(indicador.equalsIgnoreCase("Marca")){
				marca();
			}else if(indicador.equalsIgnoreCase("bodega")){
				bodega();
			}else if(indicador.equalsIgnoreCase("cliente")){
				cliente();
			}else if(indicador.equalsIgnoreCase("proveedor")){
				proveedor();
			}else if(indicador.equalsIgnoreCase("usuario")){
				usuario();
			}/*else if(indicador.equalsIgnoreCase("Personas")){
				//personas();
			}*/else if(indicador.equalsIgnoreCase("Backup")){
					backup();
			}else if(indicador.equalsIgnoreCase("Taller")){
				//com.google.gwt.user.client.Window.alert("Click on Taller");
				taller();
			}else if(indicador.equalsIgnoreCase("Forma de Pago")){
				formaPago();
			}else if(indicador.equalsIgnoreCase("cuenta")){
				cuenta();
			}else if(indicador.equalsIgnoreCase("Tipo de Cuenta")){
				tipoCuenta();
			}else if(indicador.equalsIgnoreCase("Plan de Cuentas")){
				plancuenta();
			}else if(indicador.equalsIgnoreCase("Listado de Documentos")){
				reporteCaja();
			}else if(indicador.equalsIgnoreCase("Documentos Comerciales")){
				reporteDocComercial();
			}else if(indicador.equalsIgnoreCase("Cuentas por Cobrar")){
				cuentasPorCobrar();
			}else if(indicador.equalsIgnoreCase("Impuesto")){
				impuesto();
			}else if(indicador.equalsIgnoreCase("factura")){
				facturar();
			}else if(indicador.equalsIgnoreCase("facturaGrande")){
				facturarGrande();
			}else if(indicador.equalsIgnoreCase("proforma")){
				proforma();	
			}else if(indicador.equalsIgnoreCase("Cargos Personas")){
				cargo();
			}else if(indicador.equalsIgnoreCase("retencion")){
				retencion();
			}else if(indicador.equalsIgnoreCase("retencionCompras")){
				retencionCompra();
			}else if(indicador.equalsIgnoreCase("factura compra")){
				facturaCompra();
			}else if(indicador.equalsIgnoreCase("nota compra")){
				compraNota();
			}else if(indicador.equalsIgnoreCase("Nota de Entrega")){
				notaEntrega();
			}else if(indicador.equalsIgnoreCase("Nota de Venta")){
				notaVentas();
			}else if(indicador.equalsIgnoreCase("Nota de Venta en Compras")){
				notaVentasCompras();
			}else if(indicador.equalsIgnoreCase("Nota de Credito")){
				notaCredito();
			}else if(indicador.equalsIgnoreCase("Anticipos Clientes")){
				anticipoClientes();
			}else if(indicador.equalsIgnoreCase("Operaciones Anticipos Clientes")){
					operacionesAnticipos("Clientes");
			}else if(indicador.equalsIgnoreCase("Nota credito proveedores")){
				notaCreditoProveedores();
			}else if(indicador.equalsIgnoreCase("Anticipo proveedores")){
				anticipoProveedores();
			}else if(indicador.equalsIgnoreCase("Ajuste de Inventario")){
				ajusteInventario();
			}else if(indicador.equalsIgnoreCase("Pagos")){
				pagos();
			}else if(indicador.equalsIgnoreCase("PagosProveedores")){
				pagosProveedores();
			}else if(indicador.equalsIgnoreCase("caja")){
				caja();
			}else if(indicador.equalsIgnoreCase("Gastos")){
				gastos();
			}else if(indicador.equalsIgnoreCase("Ingresos")){
				ingresos();
			}else if(indicador.equalsIgnoreCase("PrintFact")){
				factPrint();
			}else if(indicador.equalsIgnoreCase("Reportes")){
				reportes();
			}else if(indicador.equalsIgnoreCase("ReportesClientes")){
				reportesClientes();
			}else if(indicador.equalsIgnoreCase("Aso")){
				aso();
			}else if(indicador.equals("ReportesVentasEmpleado")){
				reportesEmpleados();
			}else if(indicador.equals("actualizarIvaCatorce")){
				actualizarIva();
			}else if(indicador.equals("ReportesUtilidadDetalle")){
				reportesUtilidadDetalle();
			}else if(indicador.equals("Kardex")){
				kardex();
			}else if(indicador.equalsIgnoreCase("ReportesProductosProveedor")){
				reportesProductosProveedor();
			}else if(indicador.equalsIgnoreCase("Operaciones Anticipos Proveedores")){
				operacionesAnticipos("Proveedores");
			}else if(indicador.equalsIgnoreCase("Operaciones Anticipos Clientes")){
				operacionesAnticipos("Clientes");
			}else if(indicador.equalsIgnoreCase("ReportesRetenciones")){
				reportesRetenciones();
			}			
			else if(indicador.equalsIgnoreCase("ordenMantenimiento")){
				ordenMantenimiento();
			}
			else if(indicador.equalsIgnoreCase("crudMarcas")){
				crudMarcas();
			}
			else if(indicador.equalsIgnoreCase("crudEquipos")){
				crudEquipos();
			}
			else if(indicador.equalsIgnoreCase("reporteOrdenes")){
				reporteOrdenes();
			}else if(indicador.equalsIgnoreCase("Pedido")){
				pedido();
			}else if(indicador.equalsIgnoreCase("Mesa")){
				mesa();
			}else if(indicador.equalsIgnoreCase("Area")){
				area();
			
			}else if(indicador.equalsIgnoreCase("Servicio")){
				servicio();
			}else if(indicador.equalsIgnoreCase("ListadoBasico")){
				listadoBasico();
			}else if(indicador.equalsIgnoreCase("Catalogo")){
				catalogo();
			}
			else if(indicador.equalsIgnoreCase("Reporte Retenciones"))
			{
				reporteRetenciones();
			}
			else if(indicador.equalsIgnoreCase("Lista Retenciones"))
			{
				listaRetenciones();
			}
			else if(indicador.equalsIgnoreCase("ATS"))
			{
				ATSVentas();
			}
			else if(indicador.equalsIgnoreCase("Mayorizacion"))
			{
				mayorizacion();
			}
			else if(indicador.equalsIgnoreCase("Comprobacion"))
			{
				comprobacion();
			}
			else if(indicador.equalsIgnoreCase("Balance General"))
			{
				balanceGeneral();
			}
			else if(indicador.equalsIgnoreCase("Estado Resultados"))
			{
				estadoResultados();
			}else if(indicador.equalsIgnoreCase("Asientos Manuales")){
				ajusteAsiento();
			}else if(indicador.equalsIgnoreCase("Electronicos"))
			{
				//reporteElectronicos();
			}
			
			docNom= new Label();
			//Aqu� se cambia el titulo del documento ejm:Factura, nota de venta, etc.
			docNom.setContents("<h1 id=\"title2\" style=\"font-size:20px; color:white\">"+indicador.toUpperCase()+"</h1>");
			
			//docNom.setStyleName("stiloLabel");
			//docNom.redraw();
			nombreDoc.setAlign(Alignment.LEFT);
			nombreDoc.setMembers(docNom);
			nombreDoc.setHeight100();
			nombreDoc.setWidth100();
			nombreDoc.setAlign(VerticalAlignment.CENTER);
			nombreDoc.redraw();
		}
		@Override
		public void onClick(ClickEvent event) {
			/*if(indicador.equalsIgnoreCase("Personas")){
				personas();
			}else*/ if(indicador.equalsIgnoreCase("Backup")){
				backup();
			}else if(indicador.equalsIgnoreCase("Core")){
				core();
			}else if(indicador.equalsIgnoreCase("Taller")){
				//com.google.gwt.user.client.Window.alert("Click on Taller");
				taller();
			}
			docNom= new Label();
			docNom.setContents("<b id=\"para1\">"+indicador.toUpperCase()+"</b>");
			//docNom.setStyleName("stiloLabel");
			//docNom.redraw();
			nombreDoc.setAlign(Alignment.LEFT);
			nombreDoc.setMembers(docNom);
			nombreDoc.setHeight100();
			nombreDoc.setWidth100();
			nombreDoc.setAlign(VerticalAlignment.CENTER);
			nombreDoc.redraw();
		}
			
	}
	
	//*************************************************************************	
	
	
	public void ordenMantenimiento(){
		// Guardo la cookie de sesion
		Date now = new Date();
		long nowLong = now.getTime();
		nowLong = nowLong + (8000);//8 segundos
		now.setTime(nowLong);
		
			Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
 		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
 		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
 		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
		
		Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("menu","ordenes", now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/"+Factum.banderaNombreProyectoTaller+"/", "_self", "");		
	}	
	
	public void crudEquipos(){
		// Guardo la cookie de sesion
		Date now = new Date();
		long nowLong = now.getTime();
		nowLong = nowLong + (8000);//8 segundos
		now.setTime(nowLong);
		
			Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
 		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
 		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
 		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
		
		Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("menu","equipos", now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/"+Factum.banderaNombreProyectoTaller+"/", "_self", "");
		
	}	
	
	public void crudMarcas(){
		// Guardo la cookie de sesion
		Date now = new Date();
		long nowLong = now.getTime();
		nowLong = nowLong + (8000);//8 segundos
		now.setTime(nowLong);
		
			Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
 		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
 		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
 		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
		
		Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("menu","marcas", now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/"+Factum.banderaNombreProyectoTaller+"/", "_self", "");
		
	}	
	public void reporteOrdenes(){
		// Guardo la cookie de sesion
		Date now = new Date();
		long nowLong = now.getTime();
		nowLong = nowLong + (8000);//8 segundos
		now.setTime(nowLong);
		
		Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
		
		Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		Cookies.setCookie("menu","reportes", now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
		com.google.gwt.user.client.Window.open("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/"+Factum.banderaNombreProyectoTaller+"/", "_self", "");
	}
	
	//**************** FUNCIONES PARA COMPROBAR EL USUARIO ********************
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
		}
	
	//FUNCION PARA OBTENER EL NIVEL DE ACCESO DEL CLIENTE 
	private void obtener_sesion(){
		final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {
			public void onSuccess(User result) {
				if (result == null) {//La sesion expiro

					SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

						public void execute(Boolean value) {
							if (value) {
								com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
								//getService().LeerXML(asyncCallbackXML);
							}
						}
					});
				}else{


					Integer nivel = result.getNivelAcceso();
					//NIVEL 1=gerente, 2=vendedor, 5=cajero
					//idVendedor=result.getIdUsuario();
					switch(nivel)
					{
					case 2:{//VENDEDOR
						menuPrincipal.menuFactCompra.setEnabled(false);
						//menuNotaCreditoProveedor.setEnabled(false);
						//menuPrincipalPagos.setDisabled(true);
						//menuInventario.setDisabled(true);
						//menuContabilidad.setDisabled(true);
						menuPrincipal.menuPersonas.setDisabled(true);
						menuPrincipal.menuBackup.setDisabled(false);
						//menuReportes.setEnabled(true);
						;break;}
					case 5:{//CAJERO
						menuPrincipal.menuFactCompra.setEnabled(false);
						menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
						menuPrincipal.menuInventario.setDisabled(true);
						menuPrincipal.menuReporte.setEnabled(false);
						//menuContabilidad.setDisabled(true);
						menuPrincipal.menuPersonas.setDisabled(true);
						menuPrincipal.menuBackup.setDisabled(false);
						;break;}
					//case 0:{;break;}
					}
				}

			}

	        public void onFailure(Throwable caught) {
	        	 SC.say("ERROR AL COMPROBAR EL TIPO DE USUARIO");
	        	 com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
	        }
	    };
	    getService().getUserFromSession(callbackUser);
		}

	final AsyncCallback<String>  callback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			//SC.say("/factum/backupServlet?fileName=backup"+Factum.banderaNombreBD+".zip");
			//String url = ""+"factum\\backupServlet?fileName=backup"+Factum.banderaNombreBD+".zip";
			String baseURL=GWT.getModuleBaseURL();
			String url = baseURL+"backupServlet?fileName=backup"+Factum.banderaNombreBD+".zip";
			Window.open( url, "", "");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	
	final AsyncCallback<List<EquipoOrdenDTO>>  objbackListOrdenID=new AsyncCallback<List<EquipoOrdenDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<EquipoOrdenDTO> result) {
			EquipoOrdenDTO equipoOrdenDTO = result.get(0);
			EquipoDTO equipo = equipoOrdenDTO.getTblequipo();
			OrdenDTO orden= equipoOrdenDTO.getTblorden();
			Cuerpo.this.tmporden = orden;
			getService().buscarCliente(orden.getClienteDTO().getIdCliente(), callbackClient);
			
			//SC.say("FACTURANDO ORDEN!\nPRECIO SUGERIDO :"+Factum.MontoFact+"\nIDCLIENTE: "+Factum.idcliente+"\nIDORDEN: "+Factum.idorden);
			//SC.say("FACTURANDO ORDEN! FALLA :"+orden.getDescripcionfalla()+"      REPORTE: "+orden.getReporte()+"  EQUIPO: "+equipo.getModeloequipo());
		}
	};
	final AsyncCallback<ClienteDTO>  callbackClient=new AsyncCallback<ClienteDTO>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(ClienteDTO result) {
			facturarOrden(tmporden, result);
		}
	};
}
