package com.giga.factum.client;
import java.util.List;

import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.regGrillas.MarcaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;


public class frmMarca extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	//SearchForm dynamicForm_buscar = new SearchForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtMarca = new TextItem("txtMarca", "Nombre Marca");
	ListGrid lstMarca = new ListGrid();//crea una grilla
	TabSet tabMarca = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	IButton btnModificar;
	IButton btnEliminar;
	IButton btnGrabar;
	
	
	
	frmMarca()
	{
		getService().numeroRegistrosMarca("TblMarca", objbackI);
		
		setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Marca");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "95%");
	 	txtMarca.setShouldSaveValue(true);
	 	txtMarca.setRequired(true);
	 	txtMarca.setTextAlign(Alignment.LEFT);
	 	txtMarca.setDisabled(false);
	 	
	 	
	 	TextItem textidMarca = new TextItem("txtidMarca", "C\u00F3digo Marca");
	 	textidMarca.setDisabled(true);
	 	textidMarca.setKeyPressFilter("[0-9]");
	 	dynamicForm.setFields(new FormItem[] { textidMarca, txtMarca});
	 	layout.addMember(dynamicForm);
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "70%");
	 	
	 	btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	btnModificar = new IButton("Modificar");
	 	//deshabilitando boton inicialmente
	 	btnModificar.setDisabled(true);
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	btnEliminar = new IButton("Eliminar");
	 	//deshabilitando boton inicialmente
	 	btnEliminar.setDisabled(true);
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(324, 6);
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_Marca = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstMarca.setSize("100%", "80%");
		ListGridField idMarca = new ListGridField("idMarca", "C\u00F3digo");
		ListGridField Marca = new ListGridField("Marca", "Nombre");
		
		lstMarca.setFields(new ListGridField[] {idMarca,Marca});
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarMarca(0, contador, objbacklst);
		layout_1.addMember(lstMarca);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_Marca.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstMarca.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstMarca.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
	    tabMarca.setSize("100%", "100%");
        
	    tabMarca.addTab(lTbCuenta_mant);  
	    tabMarca.addTab(lTbListado_Marca);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabMarca);
  
       
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		if(campo.equalsIgnoreCase("nombre")||(campo.equalsIgnoreCase(""))){
			campo="marca";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idMarca";
		}
		//validacion de caja de busqueda, debe contener algo a buscar
		 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		}
		 else if(campo.equals("marca")||campo.equals("idMarca")){
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarMarcaLike(nombre, campo, objbacklst);
	    }
	}
	public void limpiar(){
		dynamicForm.setValue("txtMarca", "");
		dynamicForm.setValue("txtidMarca","");
	}
	/**
	 * Manejador de Botones para pantalla Marca
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtMarca").getDisplayValue();
					MarcaDTO MarcaDTO=new MarcaDTO(Factum.empresa.getIdEmpresa(),nombre); 
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarMarca(MarcaDTO,objback);
					contador=20;
					lstMarca.redraw();
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				final String  nombre=dynamicForm.getItem("txtMarca").getDisplayValue();
				final String id=dynamicForm.getItem("txtidMarca").getDisplayValue();
				if((id!="")&&(nombre!="")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if ( value) {
        					MarcaDTO MarcaDTO=new MarcaDTO(Factum.empresa.getIdEmpresa(),Integer.parseInt(id),nombre); 	
                        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
        			    	getService().eliminarMarca(MarcaDTO, objback);
                        	limpiar();
            				lstMarca.removeData(lstMarca.getSelectedRecord());		
                        }   
                    }  
                });  
			  }
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtMarca").getDisplayValue();
					String id=dynamicForm.getItem("txtidMarca").getDisplayValue();
					MarcaDTO MarcaDTO=new MarcaDTO(Factum.empresa.getIdEmpresa(),Integer.parseInt(id),nombre); 
					//SC.say("id"+id+"  nombre "+nombre);
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().modificarMarca(MarcaDTO,objback);
					
				}
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
				//deshabilita botones despues de ingresar un nuevo
			    btnEliminar.setDisabled(true);
			    btnModificar.setDisabled(true);
			    btnGrabar.setDisabled(false);
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMarca(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMarca(contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMarca(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMarca(registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMarca(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtMarca",lstMarca.getSelectedRecord().getAttribute("Marca"));  
				dynamicForm.setValue("txtidMarca", lstMarca.getSelectedRecord().getAttribute("idMarca"));
				tabMarca.selectTab(0);
				
				//habilito despues de hacer doble clic sobre el registro deseado
				btnEliminar.setDisabled(false);
				btnModificar.setDisabled(false);
				btnGrabar.setDisabled(true);
			}
			
		}

		
			
	}
		
	final AsyncCallback<List<MarcaDTO>>  objbacklst=new AsyncCallback<List<MarcaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<MarcaDTO> result) {
			getService().numeroRegistrosMarca("TblMarca", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new MarcaRecords((MarcaDTO)result.get(i)));
				//SC.say(listado[i].getMarca()+" "+listado[i].getidMarca());
            }
			lstMarca.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
		
	};
	final AsyncCallback<MarcaDTO>  objbackMarca=new AsyncCallback<MarcaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(MarcaDTO result) {
			if(result!=null){
				dynamicForm.setValue("txtMarca", result.getMarca());  
				dynamicForm.setValue("txtidMarca", result.getIdMarca()); 
				ban=true;
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
			}
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};

	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			SC.say(result);
			getService().listarMarca(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
