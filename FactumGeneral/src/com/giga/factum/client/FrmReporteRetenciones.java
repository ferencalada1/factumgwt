package com.giga.factum.client;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.Window;
import java.util.Iterator;


public class FrmReporteRetenciones extends VLayout
{
	ListGrid lstRetenciones = new ListGrid();//+++ LISTADO GRANDE
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	HStack hStackSuperior = new HStack();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtRuc =new TextItem("txtRuc", "<b>R.U.C</b>");
	TextItem txtNombreComercial =new TextItem("txtNombreComercial", "<b>Nombre Comercial</b>");
	TextItem txtRazonSocial =new TextItem("txtRazonSocial", "<b>Razon Social</b>");
	final ComboBoxItem cmbTipoRetencion=new ComboBoxItem("tipoRetencion","<b>Tipo Retenci&oacute;n</b>");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnVerDocumento = new IButton("Ver Documento");
	IButton btnImprimir = new IButton("Imprimir");
	IButton btnExportar = new IButton("Exportar");
	LinkedHashMap<String,String> MapTipoRetencion = new LinkedHashMap<String,String>();
//++++++++++++++++++++++++++++PARA RETENCION+++++++++++++++++++++++++++++++++++++++++++++++	
	DynamicForm frmCabecera = new DynamicForm();
	VLayout formRetencion = new VLayout();
	LinkedList <ImpuestoDTO> Impuestos=new LinkedList <ImpuestoDTO>();
	LinkedList<RetencionDTO> det=new LinkedList<RetencionDTO>();
	LinkedHashMap<String,String> MapImpuesto = new LinkedHashMap<String,String>();
	LinkedHashMap<String,String> MapEmpleados = new LinkedHashMap<String,String>();
	ListGridField impuesto= new ListGridField("lstImpuesto", "Impuesto");
	ListGrid listGridRet = new ListGrid();
	//FloatItem txtTotalRet = new FloatItem();
	ListGridField baseImponible= new ListGridField("baseImponible", "Base Imponible");
	ListGridField lstBaseImponible= new ListGridField("lstBaseImponible", "Base Valor");
	ListGridField lstPorcentaje=new ListGridField("lstPorcentaje", "% Retenci\u00F3n");
	String idImpuesto="";
	ListGrid lstRetDetalle = new ListGrid();
	VLayout layoutDetalle = new VLayout();
	Set<RetencionDTO> tblRetenciones=new HashSet<RetencionDTO>(0);
//+++++++++++++++++++     PARA GRILLA PEQUE�A     +++++++++++++++++++++++++++++++++++++++++
	ListGrid lstImpuesto = new ListGrid();//+++ LISTADO PEQUE�O, TOTALES.
	String totalImpuesto ="";
	ListGridField lstidImpuesto = new ListGridField("lstidImpuesto", "C\u00F3digo");
	ListGridField lstCodigo =new ListGridField("lstCodigo", "C\u00F3digo Impuesto");
	ListGridField lstNombre = new ListGridField("lstNombre", "Nombre");
	ListGridField lstTotal= new ListGridField("lstTotal", "Total");
	ListGridRecord[] listadoRetTotales; //para los records
	
	LinkedList <Integer> linkImpuestos=new LinkedList <Integer>();
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	Date fechaFactura;// Para la fecha del sistema
	int idTipoRetencion=0;
	Window winRetenciones = new Window();
	frmListClientes frmlisCli;
	Integer numDto=0;//Para saber cual registro seleccion�
	double TotalRetencionCadaTipo=0;
	//int contadorRetencionTipo=0; 
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public static GreetingServiceAsync getService()
	{
		return GWT.create(GreetingService.class);
	}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public FrmReporteRetenciones() 
	{
		getService().fechaServidor(callbackFecha);
		MapTipoRetencion.put("0", "Retenciones en Venta");
		MapTipoRetencion.put("1", "Retenciones en Compra");
	//	MapTipoRetencion.put("13", "Retenciones en Ventas Grandes");
	//	MapTipoRetencion.put("5", "Retenciones");
	//	MapTipoRetencion.put("7", "Gastos");
//		MapTipoRetencion.put("33", "Todas");
		
		try
		{
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
//			txtProveedor.addChangedHandler(new ManejadorBotones("persona"));
			dynamicForm.setSize("20%", "20%");
			dynamicForm2.setSize("20%", "20%");
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		  
			dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal,cmbTipoRetencion});
			dynamicForm2.setFields(new FormItem[] { txtRuc, txtNombreComercial,txtRazonSocial});
			HStack hStackEspacio = new HStack();
			hStackEspacio.setSize("4%", "20%");
			HStack hStackEspacio1 = new HStack();
			hStackEspacio1.setSize("4%", "20%");
		
			
			hStackSuperior.setSize("100%", "35%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(hStackEspacio);
			hStackSuperior.addMember(dynamicForm2);
			hStackSuperior.addMember(hStackEspacio1);
			hStackSuperior.addMember(lstImpuesto);
//hStackSuperior.addMember(btnGrabar);		
			VStack vStackFinal = new VStack();
			vStackFinal.setSize("10%", "100%");
			HStack hStackSup1 = new HStack();
			hStackSup1.setSize("100%", "35%");
//	hStackSup1.setBackgroundColor("blue");
			HStack hStackSup2 = new HStack();
			hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
		
			vStackFinal.addMember(hStackSup1);
			vStackFinal.addMember(hStackSup2);
			hStackSuperior.addMember(vStackFinal);
			addMember(hStackSuperior);
			lstRetenciones.setSize("100%", "40%");
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,tc.fecha,tc.idDtoComercial,
		//tc.tipoTransaccion,tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido)
		ListGridField idPersona =new ListGridField("idPersona", "<b>idPer</b>");
		idPersona.setWidth("1%");
		idPersona.setHidden(true);
		ListGridField rucPersona =new ListGridField("rucPersona", "<b>Ruc</b>");
		rucPersona.setWidth("15%");
		ListGridField nomPersona =new ListGridField("nomPersona", "<b>Nombre Comercial</b>");
		nomPersona.setWidth("15%");
		ListGridField razonSocialPersona =new ListGridField("razonSocialPersona", "<b>Razon Social</b>");
		razonSocialPersona.setWidth("16%");
		razonSocialPersona.setAlign(Alignment.CENTER);
		//ListGridField dirPersona= new ListGridField("dirPersona", "<b>Direcci&oacute;n</b>");
		//dirPersona.setWidth("11%");
		//dirPersona.setHidden(true);
	//	dirPersona.setAlign(Alignment.CENTER);
		ListGridField fecha =new ListGridField("fecha", "<b>Fecha</b>");
		fecha.setWidth("10%");
		fecha.setAlign(Alignment.CENTER);
		ListGridField idDtoComercial =new ListGridField("idDtoComercial", "<b>idDto</b>");
		idDtoComercial.setWidth("10%");
		idDtoComercial.setAlign(Alignment.CENTER);
		ListGridField tipTransaccion =new ListGridField("tipTransaccion", "<b>Tipo Transacci&oacute;n</b>");
		tipTransaccion.setWidth("12%");
		//tipTransaccion.setHidden(true);
		ListGridField idRetencion =new ListGridField("idRetencion", "<b>idRet</b>");
		idRetencion.setWidth("1%");
		idRetencion.setHidden(true);
		ListGridField autorizacionSri=new ListGridField("autorizacionSri", "<b>Autorizaci&oacute;n SRI</b>");
		autorizacionSri.setWidth("10%");
		autorizacionSri.setAlign(Alignment.CENTER);
		ListGridField numero=new ListGridField("numero", "<b>N&uacute;mero Retenci&oacute;n</b>");
		numero.setWidth("10%");
		numero.setAlign(Alignment.CENTER);
		ListGridField totalRet=new ListGridField("totalRetencion", "<b>Total Retenci&oacute;n</b>");
		totalRet.setWidth("10%");
		totalRet.setAlign(Alignment.RIGHT);
		totalRet.setType(ListGridFieldType.FLOAT);
		totalRet.setShowGridSummary(true);
		totalRet.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("totalRetencion"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,tc.fecha,tc.idDtoComercial,
		//tc.tipoTransaccion,tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido)
		lstRetenciones.setFields(new ListGridField[] {idPersona,rucPersona,nomPersona,razonSocialPersona,
				idDtoComercial,tipTransaccion,idRetencion,autorizacionSri,numero,totalRet,fecha});
		lstRetenciones.setShowRowNumbers(true);
		lstRetenciones.setShowGridSummary(true);
		lstRetenciones.addSelectionChangedHandler(new SelectionChangedHandler() {  
            public void onSelectionChanged(SelectionEvent event) 
            {  
         //   	PrecioCompraSeleccionado=Double.valueOf(lstProductoProveedor.getSelectedRecord().getAttribute("precioCompra"));
          //  	Integer codPro = Integer.valueOf(lstProductoProveedor.getSelectedRecord().getAttribute("idProducto"));
			//	getService().listarTipoPrecio(codPro, objbacklstTipoPrecio);
        }  
    });
		addMember(lstRetenciones);
		//addMember(lstRetDetalle);
		IButton btnLimpiar = new IButton("Limpiar");
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		hStack.addMember(btnGenerarReporte);
		hStack.addMember(btnLimpiar);
		hStack.addMember(btnVerDocumento);
		hStack.addMember(btnImprimir);
		hStack.addMember(btnExportar);
		
		
		//IButton btnVerDocumento = new IButton("Ver Documento");
		txtRuc.setValue("");
		txtNombreComercial.setValue("");
		txtRazonSocial.setValue("");
		cmbTipoRetencion.setValueMap(MapTipoRetencion);
		cmbTipoRetencion.setDefaultToFirstOption(true);
	    
		cmbTipoRetencion.addChangedHandler(new ChangedHandler() 
		{  
	    	public void onChanged(ChangedEvent event) 
	    	{
	    		idTipoRetencion = Integer.valueOf(cmbTipoRetencion.getValue().toString());
	    		//SC.say("el tipo es:"+idTipoRetencion);
			}  
	    });
//+++++++++++++++++++++++++++     LISTADO PEQUE�O    ++++++++++++++++++++++++++++++++++++++++++
		lstidImpuesto.setWidth("1%");
		lstidImpuesto.setHidden(true);
		lstCodigo.setWidth("20%");
		lstCodigo.setAlign(Alignment.CENTER);
		lstNombre.setWidth("55%");
		lstNombre.setAlign(Alignment.CENTER);
		lstTotal.setWidth("24%");
		lstTotal.setAlign(Alignment.RIGHT);
		lstTotal.setType(ListGridFieldType.FLOAT);
		lstTotal.setShowGridSummary(true);
		lstTotal.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstTotal"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		
		lstImpuesto.setFields(new ListGridField[] {lstidImpuesto,lstCodigo,lstNombre,lstTotal});
		lstImpuesto.setWidth("52%");
		lstImpuesto.setShowGridSummary(true);
		
		

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		btnGenerarReporte.setDisabled(false);
		btnGenerarReporte.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event){     	  
	       // 	getService().listarProductosProveedor(, , Integer.valueOf(txtStockMinimo.getValue().toString()), Integer.valueOf(txtStockMaximo), objProductoProveedor);
	   			getService().listarRetenciones(txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(), idTipoRetencion, txtRuc.getValue().toString(), txtNombreComercial.getValue().toString(), txtRazonSocial.getValue().toString(), objReporteRetencion);
	        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	           }
	       });
		btnLimpiar.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event)
	           {
	       		txtRuc.setValue("");
	    		txtNombreComercial.setValue("");
	    		txtRazonSocial.setValue("");
	        	cmbTipoRetencion.setValue("Retenciones en Venta");
	           }
	       });
		btnVerDocumento.setVisible(false);	
	btnVerDocumento.addClickHandler(new ClickHandler(){
	   public void onClick(ClickEvent event){
	      //Integer numDto=0;
	     // try{
	      	numDto=Integer.parseInt(lstRetenciones.getSelectedRecord().getAttributeAsString("idDtoComercial"));
	      	winRetenciones = new Window();
	      	winRetenciones.setWidth("100%");
	      	winRetenciones.setHeight("100%");
	      	winRetenciones.setTitle("Documento Comercial");  
	        winRetenciones.setShowMinimizeButton(false);  
	        winRetenciones.setIsModal(true);  
	        winRetenciones.setShowModalMask(true);  
	        winRetenciones.centerInPage();  
	        winRetenciones.addCloseClickHandler(new CloseClickHandler() {  
	        	public void onCloseClick(CloseClientEvent event) {  
	        		winRetenciones.destroy();  
	        	   		                }});
	        VLayout form = new VLayout();  
	        form.setSize("100%","100%"); 
	        form.setPadding(5);  
	        form.setLayoutAlign(VerticalAlignment.BOTTOM);
	        formRetencion = new VLayout();
	        formRetencion();
	        form.addMember(formRetencion);
	        winRetenciones.addItem(form);
	        winRetenciones.show();
	     // }catch(Exception e){
	    //	  SC.say("Primero debe seleccionar una fila");
	     //  }
	          
	   }});

	btnImprimir.addClickHandler(new ClickHandler(){
		   public void onClick(ClickEvent event){
		    	  //Object[] a=new Object[]{dynamicForm,formRetencion,lstRetenciones};
			   VLayout espacio = new VLayout();  
               espacio.setSize("100%","5%");
               totalImpuesto="Total: "+totalImpuesto;
				Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +				  
				   		"&nbsp;&nbsp;&nbsp;Reporte de Retenciones",espacio,hStackSuperior,lstRetenciones,lstImpuesto,totalImpuesto};					   
			        Canvas.showPrintPreview(a);  
		   }});
	
	btnExportar.addClickHandler(new ClickHandler(){
		   public void onClick(ClickEvent event){
					CreateExelDTO exel=new CreateExelDTO(lstRetenciones);
									  
		   }});


		addMember(hStack);
		}catch(Exception e ){SC.say("Error: "+e);}
	}

	final AsyncCallback<List<ReporteRetencion>>  objReporteRetencion = new AsyncCallback<List<ReporteRetencion>>()
	{
		public void onFailure(Throwable caught) 
		{
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<ReporteRetencion> resultRet) 
		{
			ListGridRecord[] listadoRet = new ListGridRecord[resultRet.size()];
			int contadorProdPro=0;
			if (resultRet.size()>0)
			{	
				double totReten=0;
				for(int i=0;i<resultRet.size();i++) 
				{					
					listadoRet[contadorProdPro]=new ListGridRecord();
					listadoRet[contadorProdPro].setAttribute("idPersona",resultRet.get(i).getIdPersona());
					listadoRet[contadorProdPro].setAttribute("rucPersona",resultRet.get(i).getCedPersona());
					listadoRet[contadorProdPro].setAttribute("nomPersona", resultRet.get(i).getNomPersona());
					listadoRet[contadorProdPro].setAttribute("apePersona", resultRet.get(i).getApePersona());
					listadoRet[contadorProdPro].setAttribute("dirPersona", resultRet.get(i).getDirPersona());
					listadoRet[contadorProdPro].setAttribute("fecha", resultRet.get(i).getFecha());
					listadoRet[contadorProdPro].setAttribute("idDtoComercial", resultRet.get(i).getIdDtoComercial());
					String tipoTransa = "";
					int tip=resultRet.get(i).getTipoTransaccion();
					if (tip==0)
					{
						tipoTransa="Retenciones en Venta";
					}
					if (tip==1)
					{
						tipoTransa="Retenciones en Compra";
					}
					if (tip==5)
					{
						tipoTransa="Retenciones";
					}
					if (tip==7)
					{
						tipoTransa="Gastos";
					}
					if (tip==13)
					{
						tipoTransa="Retenciones en Ventas Grandes";
					}
					listadoRet[contadorProdPro].setAttribute("tipTransaccion",tipoTransa );
					listadoRet[contadorProdPro].setAttribute("idRetencion", resultRet.get(i).getIdRetencion());
					listadoRet[contadorProdPro].setAttribute("autorizacionSri", resultRet.get(i).getAutorizacionSri());
					listadoRet[contadorProdPro].setAttribute("numero", resultRet.get(i).getNumero());
					listadoRet[contadorProdPro].setAttribute("totalRetencion", resultRet.get(i).getTotalRetencion());
					contadorProdPro++;
					totReten=totReten+resultRet.get(i).getTotalRetencion();
					
				}
				totReten=CValidarDato.getDecimal(4,totReten);
				totalImpuesto=String.valueOf(totReten);
				getService().listarImpuesto(0, 20, objbacklst);
			}
			else
			{
				SC.say("No hay retenciones que cumplan los parametros establecidos");
				getService().listarImpuesto(0, 20, objbacklst);
			}
			lstRetenciones.setData(listadoRet);
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        
	        lstRetenciones.show();
	        lstRetenciones.redraw();
		}
	};
 	
	
	
	final AsyncCallback<List<RetencionDTO>>  objRetencionDetalle = new AsyncCallback<List<RetencionDTO>>()
	{
		public void onFailure(Throwable caught) 
		{
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<RetencionDTO> resultRet) 
		{
			double total=0;
			for(int i=0;i<resultRet.size();i++) 
			{
				Double porcentaje = 0.0;
				
				porcentaje = (resultRet.get(i).getValorRetenido()*100)/resultRet.get(i).getBaseImponible();
				porcentaje=CValidarDato.getDecimal(0,porcentaje);
				//SC.say("VALOR RETENIDO: "+retencion.getValorRetenido());
				ListGridRecord recordret=new ListGridRecord();
				//recordret.setAttribute("lstImpuesto", retencion.getImpuesto().getNombre());
				//recordret.setAttribute("baseImponible",retencion.getBaseImponible());
				recordret.setAttribute("lstPorcentaje",porcentaje);
				recordret.setAttribute("lstBaseImponible",resultRet.get(i).getBaseImponible());
	//			recordret.setAttribute("lstCodigo",resultRet.get(i).getCodigo());
				recordret.setAttribute("lstValor",resultRet.get(i).getValorRetenido());
				total=total+resultRet.get(i).getValorRetenido();
	//			txtAutorizacion.setValue(resultRet.get(i).getAutorizacionSri());
	//			txtNumeroC.setValue(resultRet.get(i).getNumero());
	//			txtNumero.setValue(resultRet.get(i).getNumRealRetencion());
				lstRetDetalle.addData(recordret);
				}
		//	totalImpuesto=String.valueOf(total);
		//	SC.say("lucerita tiste "+totalImpuesto);
			//	txtTotalRet.setValue(total);
/*					if (tip==0)
					{
						tipoTransa="Retenciones en Compra";
					}
					if (tip==1)
					{
						tipoTransa="Retenciones en Venta";
					}
					if (tip==5)
					{
						tipoTransa="Retenciones";
					}
					if (tip==7)
					{
						tipoTransa="Gastos";
					}	*/
				
		}			
	};
 		
 	final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
			
		}
		public void onSuccess(Date result) {
			fechaFactura=result;
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
		}
	};
		final AsyncCallback<String>  objback=new AsyncCallback<String>(){
				public void onFailure(Throwable caught) {
					SC.say("Error dado:" + caught.toString());
				}
				public void onSuccess(String result) {
					SC.say(result);
				}
			};
			
	public void formRetencion()
	{	 		
		VLayout layoutCabecera = new VLayout();
		layoutCabecera.setSize("100%", "30%");
		frmCabecera.setSize("100%", "100%");
		frmCabecera.setNumCols(4);
		TextItem  txtNumero=new TextItem("txtNumero", "N\u00FAmero");
		txtNumero.setDisabled(false);

		txtNumero.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("idDtoComercial"));
		
		//txtNumero.setRequired(true);
//		TextItem txtFechaEmision=new TextItem("txtFechaEmision", "Fecha de Emisi\u00F3n");
		//txtFechaEmision.setRequired(true);
		
	//	txtFechaEmision.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("fecha"));
//		txtFechaEmision.disable();
		//setUseTextField(true);
		SC.say("lucerita3");
		TextItem txtRuc = new TextItem("txtRuc", "R.U.C");
		txtRuc.setDisabled(true);
		TextItem txtFecha = new TextItem("txtFecha", "Fecha");
		txtFecha.setDisabled(true);
		
		txtRuc.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("rucPersona"));
		//txtRuc.setValue(Fact.getTblpersonaByIdPersona().getCedulaRuc());
		TextItem txtPersona = new TextItem("txtPersona", "Se\u00F1ore(s)");
		txtPersona.setDisabled(true);
		txtPersona.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("nomPersona")+" "+ lstRetenciones.getSelectedRecord().getAttributeAsString("apePersona"));
		TextItem txtTipoComprobante = new TextItem("txtTipoComprobante", "Tipo de Comprobante");
		txtTipoComprobante.setDisabled(true);
	//	txtTipoComprobante.setValue(label.getContents());
		TextItem txtAutorizacion= new TextItem("txtAutorizacion", "N\u00B0 Autorizaci\u00F3n");
		txtAutorizacion.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("autorizacionSri"));
		//TextItem txtDireccion=new TextItem("txtDireccion", "Direcci\u00F3n");
		TextItem txtNumeroC=new TextItem("txtNumeroC", "# Documento");
		txtNumeroC.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("numero"));		
		//try{txtNumeroC.setValue(txtNumCompra.getDisplayValue());}
		txtFecha.setValue(lstRetenciones.getSelectedRecord().getAttributeAsString("fecha"));
		
	//	txtDireccion.setValue(DocBase.getTblpersonaByIdPersona().getDireccion());
	//	txtPersona.setValue(DocBase.getTblpersonaByIdPersona().getNombres());
	//	txtRuc.setValue(String.valueOf(DocBase.getTblpersonaByIdPersona().getCedulaRuc()));
				
		//frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtFechaEmision, txtRuc, txtTipoComprobante,
		frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtRuc, txtTipoComprobante,
						txtAutorizacion,  txtFecha,txtNumeroC});
		layoutCabecera.addMember(frmCabecera);
		formRetencion.addMember(layoutCabecera);
		
		
		layoutDetalle.setSize("100%", "65%");
		lstRetDetalle.setSize("100%", "100%");
		lstRetDetalle.setCanEdit(true);
		lstRetDetalle.setCanSelectAll(true);
		
		baseImponible.setValueMap("Iva","Sub Total");
		//baseImponible.addChangeHandler(new ManejadorBotonesRetencion("base"));
		//baseImponible.addEditorExitHandler(new ManejadorBotonesRetencion("base"));
		baseImponible.setCanEdit(false);
		lstRetDetalle.setFields(new ListGridField[] { impuesto,baseImponible, new ListGridField("lstBaseImponible", "Base Valor"),  new ListGridField("lstCodigo", "C\u00F3digo"), lstPorcentaje, new ListGridField("lstValor", "Valor Retenido")});
				layoutDetalle.addMember(lstRetDetalle);
				HStack hStack_1 = new HStack();
		        hStack_1.setSize("100%", "8%");
		        
		        DynamicForm dynamicForm_1 = new DynamicForm();
		        dynamicForm_1.setItemLayout(FormLayoutType.TABLE);
		  //      txtTotalRet.setLeft(300);
		   //     txtTotalRet.setTop(0);
		        
		   //     txtTotalRet.setTextAlign(Alignment.LEFT);
		    //    txtTotalRet.setTitle("TOTAL");
		    //    dynamicForm_1.setFields(new FormItem[] { txtTotalRet});
		        layoutDetalle.addMember(dynamicForm_1);
		        layoutDetalle.addMember(hStack_1);
		        formRetencion.addMember(layoutDetalle);
				
				HStack hStack = new HStack();
				hStack.setSize("100%", "5%");
					
				IButton btnImprimir = new IButton("Imprimir");
				hStack.addMember(btnImprimir);
				//btnImprimir.addClickHandler(new ManejadorBotonesRetencion("imprimir"));
				
				formRetencion.addMember(hStack);
				getService().listarDetallesRetencion(numDto, objRetencionDetalle);
						 	
	}
	
	 		
//++++++++++++++++++++++     LISTADO DE IMPUESTOS     ++++++++++++++++++++++++++++++++++++++++	 		
	 final AsyncCallback<List<ImpuestoDTO>>  objbacklst=new AsyncCallback<List<ImpuestoDTO>>()
	 {			
	 	public void onFailure(Throwable caught) {
	 	SC.say(caught.toString());
	 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	 	}
	 	public void onSuccess(List<ImpuestoDTO> result) {
	 		if(result.size()<1)
	 		{
	 			SC.say("No hay registros");
	 		}
	 		else
	 		{	
	 			listadoRetTotales = new ListGridRecord[result.size()];
	 			linkImpuestos.clear();
	 			for(int i=0;i<result.size();i++) 
	 			{
	 				listadoRetTotales[i]=new ListGridRecord();
	 				listadoRetTotales[i].setAttribute("lstidImpuesto",result.get(i).getIdImpuesto());
	 				listadoRetTotales[i].setAttribute("lstCodigo",result.get(i).getCodigo());
	 				listadoRetTotales[i].setAttribute("lstNombre",result.get(i).getNombre());
	 				linkImpuestos.add(result.get(i).getIdImpuesto());
	 			}
	 			lstImpuesto.setData(listadoRetTotales);
	 			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	 			getService().listarRetTotales(idTipoRetencion, linkImpuestos, txtRuc.getValue().toString(), txtNombreComercial.getValue().toString(), txtRazonSocial.getValue().toString(),txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(),objRetTotal);	
	 		}
	 	}};
	 	//++++++ PARA PONER LOS TOTALES DE CADA SUB-TIPO DE RETENCION ++++++++++++++++++++++++++++
	 	final AsyncCallback<List<RetencionDetalle>>  objRetTotal=new AsyncCallback<List<RetencionDetalle>>()
		 {			
		 	public void onFailure(Throwable caught) 
		 	{
		 	SC.say(caught.toString());
		 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		 	}
		 	public void onSuccess(List<RetencionDetalle> result) {
		 		if (result.size()>0)
		 		{
		 			for(int i=0;i<result.size();i++)
		 			{	
		 				TotalRetencionCadaTipo=result.get(i).getTotalRetencion();
			 		lstImpuesto.setEditValue(i, "lstTotal", TotalRetencionCadaTipo);
			 		lstImpuesto.refreshRow(i);
			 		}
				}
		 	}};
	 		 	
}
