package com.giga.factum.client;

import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.grid.ListGridRecord;  
import com.smartgwt.client.widgets.layout.HStack;  
import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.regGrillas.AreaRecords;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;


public class frmArea extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	//SearchForm dynamicForm_buscar = new SearchForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtArea = new TextItem("txtArea", "Nombre Area");
	ListGrid lstArea = new ListGrid();//crea una grilla
	TabSet tabArea = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegistros = new Label("# Registros");
	
	IButton btnGrabar ;
	IButton btnModificar;
	
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	
	frmArea()
	{
		getService().numeroRegistrosArea("Tblarea", objbackI);
		
		setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Area");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "50%");
	 	txtArea.setShouldSaveValue(true);
	 	txtArea.setRequired(true);
	 	txtArea.setTextAlign(Alignment.LEFT);
	 	txtArea.setDisabled(false);
	 	
	 	
	 	TextItem textidArea = new TextItem("txtidArea", "C\u00F3digo Area");
	 	textidArea.setDisabled(true);
	 	textidArea.setKeyPressFilter("[0-9]");
	 	dynamicForm.setFields(new FormItem[] { textidArea, txtArea});
	 	layout.addMember(dynamicForm);
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "50%");
	 	
	 	btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	btnModificar = new IButton("Modificar");
	 	btnModificar.setDisabled(true);
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_Area = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstArea.setSize("100%", "80%");
		ListGridField idArea = new ListGridField("idArea", "C\u00F3digo");
		ListGridField Area = new ListGridField("Area", "Nombre");
		
		lstArea.setFields(new ListGridField[] {idArea,Area});
		//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarArea(0, contador, objbacklst);
		layout_1.addMember(lstArea);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegistros);
	    lblRegistros.setSize("100%", "4%");
	    lTbListado_Area.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstArea.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstArea.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        
	    tabArea.addTab(lTbCuenta_mant);  
	    tabArea.addTab(lTbListado_Area);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabArea);
  
       
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		
		if(campo.equalsIgnoreCase("nombre")||(campo.equalsIgnoreCase(""))){ 
			campo="nombre";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idArea";
		}
		//validando que la busqueda no sea il�gica y tenga al menos una opcion v�lida
		 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equalsIgnoreCase(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		 }else if(campo.equals("nombre")||campo.equals("idArea")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarAreaLike(nombre, campo, objbacklst);
		 }
	}
	public void limpiar(){
		dynamicForm.setValue("txtArea", "");  
		dynamicForm.setValue("txtidArea", "");  
	}
	/**
	 * Manejador de Botones para pantalla Area
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtArea").getDisplayValue();
					AreaDTO AreaDTO=new AreaDTO(Factum.empresa.getIdEmpresa(),Factum.getEstablecimientoCero() ,nombre,"area.png", "area.png");//AGREGAR IMAGEN E IMAGEN-OCUPADO
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarArea(AreaDTO,objback);
					contador=20;
					lstArea.redraw();
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					String nombre=dynamicForm.getItem("txtArea").getDisplayValue();
					String id=dynamicForm.getItem("txtidArea").getDisplayValue();
					AreaDTO AreaDTO=new AreaDTO(Factum.empresa.getIdEmpresa(),Factum.getEstablecimientoCero(),Integer.parseInt(id),nombre, "area.png", "area.png"); 
					//SC.say("id"+id+"  nombre "+nombre);
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().modificarArea(AreaDTO,objback);
					
				}
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
				//deshabilita botones despues de ingresar un nuevo
			    btnModificar.setDisabled(true);
			    btnGrabar.setDisabled(false);
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarArea(contador-20,contador, objbacklst);
					lblRegistros.setContents(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarArea(contador-20,contador, objbacklst);
					lblRegistros.setContents(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarArea(contador-20,contador, objbacklst);
					lblRegistros.setContents(contador+" de "+registros);
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarArea(registros-registros%20,registros, objbacklst);
					lblRegistros.setContents(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarArea(contador-20,contador, objbacklst);
					lblRegistros.setContents(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtArea",lstArea.getSelectedRecord().getAttribute("Area"));  
				dynamicForm.setValue("txtidArea", lstArea.getSelectedRecord().getAttribute("idArea"));
				tabArea.selectTab(0);
				
				//habilito despues de hacer doble clic sobre el registro deseado
				btnModificar.setDisabled(false);
				btnGrabar.setDisabled(true);
				
			}
		}
	}
		
	
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers

	}*/
	
	
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			SC.say(result);
			getService().listarArea(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	/**
	 * LLamada asincrona para buscar un objeto
	 */
	final AsyncCallback<AreaDTO>  objbackArea=new AsyncCallback<AreaDTO>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			
		}
		@Override
		public void onSuccess(AreaDTO result) {
			// TODO Auto-generated method stub
			if(result!=null){
				dynamicForm.setValue("txtArea", result.getNombre());  
				dynamicForm.setValue("txtIdArea", result.getIdArea());  
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado");
			}
		}
	};
	/**
	 * LLamada asincrona para construir una grilla
	 */
	final AsyncCallback<List<AreaDTO>>  objbacklst=new AsyncCallback<List<AreaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
		}
		public void onSuccess(List<AreaDTO> result) {
			//SC.say("NUMERO REGISTROS: "+result.size());
			getService().numeroRegistrosArea("Tblarea", objbackI);
			if(contador>registros){
				lblRegistros.setContents(registros+" de "+registros);
			}else{
				lblRegistros.setContents(contador+" de "+registros);
			}
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			String mensaje="";
			
			for(int i=0;i<result.size();i++) {
				AreaDTO areadto = new AreaDTO();
				areadto=result.get(i);
				AreaRecords arearecords= new AreaRecords(areadto.getIdArea(), areadto.getNombre());
				listado[i]=(arearecords);
				mensaje=mensaje+areadto.getIdArea()+"-"+areadto.getNombre();
            }
			//SC.say(mensaje);
			lstArea.setData(listado);
			lstArea.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
            
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
