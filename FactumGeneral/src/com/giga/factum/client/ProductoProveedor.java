package com.giga.factum.client;

public class ProductoProveedor implements java.io.Serializable 
{
	private static final long serialVersionUID = 1L;
	
	private int idProducto;
	private String nomProducto;
	private double cantidad;
	private double precioCompra;
	private double stock;
	private double precioVenta;
	private int idPersona;
	private String proveedor;
	private String fechaCompra;
	
	public ProductoProveedor() {}

	public ProductoProveedor(int idProducto, String nomProducto, double cantidad, double precioCompra,
			double stock,double precioVenta,int idPersona,String proveedor,String fechaCompra) {
		this.idProducto=idProducto;
		this.nomProducto=nomProducto;
		this.cantidad=cantidad;
		this.precioCompra=precioCompra;
		this.stock=stock;
		this.precioVenta=precioVenta;
		this.idPersona=idPersona;
		this.proveedor=proveedor;
		this.fechaCompra=fechaCompra;
			
	}
	
	
	public int getIdProducto() 
	{return this.idProducto;}
	public void setIdProducto(int idProducto) 
	{this.idProducto = idProducto;}
	
	public String getNomProducto() 
	{return this.nomProducto;}
	public void setNomProducto(String nomProducto) 
	{this.nomProducto = nomProducto;}
	
	public void setCantidad(double cantidad) 
	{this.cantidad = cantidad;}
	public double getCantidad() 
	{return this.cantidad;}
	
	public void setPrecioCompra(double precioCompra) 
	{this.precioCompra = precioCompra;}
	public double getPrecioCompra() 
	{return this.precioCompra;}

	public double getStock() 
	{return this.stock;}
	public void setStock(double stock) 
	{this.stock = stock;}

	public double getPrecioVenta() 
	{return this.precioVenta;}
	public void setPrecioVenta(double precioVenta) 
	{this.precioVenta = precioVenta;}
		
	public double getIdPersona() 
	{return this.idPersona;}
	public void setIdPersona(int idPersona) 
	{this.idPersona = idPersona;}

	public String getProveedor() 
	{return this.proveedor;}
	public void setProveedor(String proveedor) 
	{this.proveedor = "  "+proveedor;}

	public String getFechaCompra() 
	{return this.fechaCompra;}
	public void setFechaCompra(String fechaCompra) 
	{this.fechaCompra = fechaCompra;}
	
}
