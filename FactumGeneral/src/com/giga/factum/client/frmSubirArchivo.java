package com.giga.factum.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.NamedFrame;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.types.FormMethod;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmSubirArchivo extends VLayout{
	IButton uploadButton;
	final DynamicForm dynamicformsubirarchivo ;
	NamedFrame iframeUpload;
	String nombreImagen;
	UploadItem fileItem;
	StaticTextItem uploadPathItem;
	private static void onFileUploadFinished(){
        SC.say("upload finished");
    }

    private native void registerOnFileUploadFinished()/*-{
        $wnd.onFileUploadFinished = @com.giga.factum.client.frmSubirArchivo::onFileUploadFinished();
    }-*/;
	public frmSubirArchivo(String tipo){
		
        dynamicformsubirarchivo = new DynamicForm();  
        iframeUpload = new NamedFrame("iframeUpload");
        iframeUpload.setWidth("1px");
        iframeUpload.setHeight("1px");
        iframeUpload.setVisible(false);
        dynamicformsubirarchivo.setTarget(iframeUpload.getName());
        dynamicformsubirarchivo.setSize("50%", "25%");
        dynamicformsubirarchivo.setEncoding(Encoding.MULTIPART);
        dynamicformsubirarchivo.setAlign(Alignment.CENTER);
        dynamicformsubirarchivo.setMethod(FormMethod.POST);
        //fileItem = new UploadItem("Imagen");
        fileItem = new UploadItem(tipo);
        //fileItem.setAttribute("Path", Factum.banderaImagenes);
        uploadPathItem = new StaticTextItem("");
        String ppath="";
        if (!tipo.equals("Imagen")){
        	String[] divs=GWT.getModuleBaseURL().split("\\/");
        	ppath=divs[divs.length-2]+"\\"+divs[divs.length-1];
        }
        String uploadPathI=(tipo.equals("Imagen"))?Factum.banderaImagenes:ppath;
        uploadPathItem.setName(uploadPathI);
        //uploadPathItem.setValue("");
        uploadPathItem.setTitle("");
        //uploadPathItem.setTitle("uploadPath");
        //uploadPathItem.setValueField("");
        //uploadPathItem.setVisible(false);
        //uploadPathItem.hide();
        if(tipo.equals("Imagen")) registerOnFileUploadFinished();
        dynamicformsubirarchivo.setMethod(FormMethod.POST);
        dynamicformsubirarchivo.setAction(GWT.getModuleBaseURL()+"uploadFileServlet");
        uploadButton = new IButton("Subir");
        //dynamicformsubirarchivo.setItems(fileItem);
        dynamicformsubirarchivo.setItems(uploadPathItem,fileItem);
       /* VLayout vl=new VLayout();
        vl.addMember(iframeUpload);*/
        setWidth100();
        setHeight100();
        setMembers(dynamicformsubirarchivo, uploadButton/*,vl*/);	
        setAlign(Alignment.CENTER);
        //RootPanel.get().add(iframeUpload);
        
	}
	

}
