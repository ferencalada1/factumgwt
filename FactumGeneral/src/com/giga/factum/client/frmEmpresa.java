package com.giga.factum.client;
import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.types.Alignment;  
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.grid.ListGridRecord;  
import com.smartgwt.client.widgets.layout.HStack;  
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.regGrillas.EmpresaRecords;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;

public class frmEmpresa extends VLayout
{
	DynamicForm dynamicForm = new DynamicForm();
	//SearchForm dynamicForm_buscar = new SearchForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtEmpresa = new TextItem("txtEmpresa", "Nombre Empresa");
	final TextItem txtRUC = new TextItem("txtRUC", "R.U.C.");
	final TextItem txtRazonSocial = new TextItem("txtRazonSocial", "Razon Social");
	final TextItem txtDireccionMatriz = new TextItem("txtDireccionMatriz", "Direccion Matriz");
	final TextItem txtContribuyenteEspecial = new TextItem("txtContribuyenteEspecial", "Contribuyente Especial");
	CheckboxItem chkObligadoContabilidad; 
	ListGrid lstEmpresa = new ListGrid();//crea una grilla
	TabSet tabEmpresa = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	
	char obligado;
	frmEmpresa()
	{
		getService().numeroRegistros("Tblempresa", objbackI);
		
		setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Empresa");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setNumCols(4);
	 	dynamicForm.setSize("100%", "95%");
	 	txtEmpresa.setShouldSaveValue(true);
	 	txtEmpresa.setRequired(true);
	 	txtEmpresa.setTextAlign(Alignment.LEFT);
	 	txtEmpresa.setDisabled(false);
	 	
	 	txtRUC.setShouldSaveValue(true);
	 	txtRUC.setRequired(true);
	 	txtRUC.setTextAlign(Alignment.LEFT);
	 	txtRUC.setDisabled(false);
	 	
	 	txtRazonSocial.setShouldSaveValue(true);
	 	txtRazonSocial.setRequired(true);
	 	txtRazonSocial.setTextAlign(Alignment.LEFT);
	 	txtRazonSocial.setDisabled(false);
	 	
	 	txtDireccionMatriz.setShouldSaveValue(true);
	 	txtDireccionMatriz.setRequired(true);
	 	txtDireccionMatriz.setTextAlign(Alignment.LEFT);
	 	txtDireccionMatriz.setDisabled(false);
	 	
	 	txtContribuyenteEspecial.setShouldSaveValue(true);
	 	txtContribuyenteEspecial.setRequired(true);
	 	txtContribuyenteEspecial.setTextAlign(Alignment.LEFT);
	 	txtContribuyenteEspecial.setDisabled(false);
	 	
	 	chkObligadoContabilidad= new CheckboxItem("chkObligadoContabilidad", "Obligado Contabilidad");
	 	chkObligadoContabilidad.setTextAlign(Alignment.LEFT);
	 	chkObligadoContabilidad.setDisabled(false);
	 	chkObligadoContabilidad.addChangedHandler(new ManejadorBotones("obligado"));
	 	
	 	
	 	TextItem textidEmpresa = new TextItem("txtidEmpresa", "C\u00F3digo Empresa");
	 	textidEmpresa.setDisabled(true);
	 	textidEmpresa.setKeyPressFilter("[0-9]");
	 	dynamicForm.setFields(new FormItem[] { textidEmpresa, txtEmpresa, txtRUC,txtRazonSocial,txtDireccionMatriz,txtContribuyenteEspecial, chkObligadoContabilidad});
	 	layout.addMember(dynamicForm);
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "70%");
	 	
	 	IButton btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	IButton btnModificar = new IButton("Modificar");
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	IButton btnEliminar = new IButton("Eliminar");
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(324, 6);
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_empresa = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstEmpresa.setSize("100%", "80%");
		ListGridField idEmpresa = new ListGridField("idEmpresa", "C\u00F3digo");
		ListGridField Empresa = new ListGridField("Empresa", "Nombre");
		ListGridField RUC = new ListGridField("RUC", "RUC");
		ListGridField razonSocial = new ListGridField("razonSocial", "Razon Social");
		ListGridField direccionMatriz = new ListGridField("direccionMatriz", "Direccion Matriz");
		ListGridField contribuyenteEspecial = new ListGridField("contribuyenteEspecial", "Contribuyente Especial");
		ListGridField obligadoContabilidad = new ListGridField("obligadoContabilidad", "Obligado Contabilidad");
		
		lstEmpresa.setFields(new ListGridField[] {idEmpresa,Empresa,RUC,razonSocial,direccionMatriz,contribuyenteEspecial,obligadoContabilidad});
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarEmpresa(0, contador, objbacklst);
		layout_1.addMember(lstEmpresa);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_empresa.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstEmpresa.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstEmpresa.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        
	    tabEmpresa.addTab(lTbCuenta_mant);  
	    tabEmpresa.addTab(lTbListado_empresa);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabEmpresa);
  
       
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		if(campo.equalsIgnoreCase("nombre")){
			campo="nombre";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idEmpresa";
		}
		getService().listarEmpresaLike(nombre, campo, objbacklst);
	}
	public void limpiar(){
		dynamicForm.setValue("txtEmpresa", "");
		dynamicForm.setValue("txtidEmpresa","");
		dynamicForm.setValue("txtRUC","");
		dynamicForm.setValue("txtRazonSocial","");
		dynamicForm.setValue("txtDireccionMatriz","");
		dynamicForm.setValue("txtContribuyenteEspecial", "0000");
		dynamicForm.setValue("chkObligadoContabilidad", false);
	}
	/**
	 * Manejador de Botones para pantalla empresa
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler, ChangedHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtEmpresa").getDisplayValue();
					String RUC=dynamicForm.getItem("txtRUC").getDisplayValue();
					String razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
					String direccionMatriz=dynamicForm.getItem("txtDireccionMatriz").getDisplayValue();
					String contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
					char obligadoContabilidad=obligado;
					EmpresaDTO empresaDTO=new EmpresaDTO(nombre, RUC, razonSocial, direccionMatriz, contribuyenteEspecial,obligadoContabilidad); 
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarEmpresa(empresaDTO,objback);
					contador=20;
					lstEmpresa.redraw();
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.say("se pulso el boton eliminar");
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	EmpresaDTO empresa= new EmpresaDTO(Integer.parseInt(lstEmpresa.getSelectedRecord().getAttribute("idEmpresa")),
                        			lstEmpresa.getSelectedRecord().getAttribute("Empresa"));
                        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
        					getService().eliminarEmpresa(empresa, objback);
                        	limpiar();
            				lstEmpresa.removeData(lstEmpresa.getSelectedRecord());
            				
            				
                        } else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
				
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtEmpresa").getDisplayValue();
					String id=dynamicForm.getItem("txtidEmpresa").getDisplayValue();
					String RUC=dynamicForm.getItem("txtRUC").getDisplayValue();
					String razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
					String direccionMatriz=dynamicForm.getItem("txtDireccionMatriz").getDisplayValue();
					String contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
					char obligadoContabilidad=obligado;
					EmpresaDTO empresaDTO=new EmpresaDTO(nombre, RUC, razonSocial, direccionMatriz, contribuyenteEspecial,obligadoContabilidad);
					empresaDTO.setIdEmpresa(Integer.parseInt(id));
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().modificarEmpresa(empresaDTO,objback);
					
				}
			}else if(indicador.equalsIgnoreCase("Buscar")){
				String idEmpresa=dynamicForm.getItem("txtidEmpresa").getDisplayValue();
				getService().buscarEmpresa(idEmpresa, objbackempresa);
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarEmpresa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarEmpresa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarEmpresa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarEmpresa(registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarEmpresa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtEmpresa",lstEmpresa.getSelectedRecord().getAttribute("Empresa"));  
				dynamicForm.setValue("txtidEmpresa", lstEmpresa.getSelectedRecord().getAttribute("idEmpresa"));
				dynamicForm.setValue("txtRUC", lstEmpresa.getSelectedRecord().getAttribute("RUC"));
				dynamicForm.setValue("txtRazonSocial", lstEmpresa.getSelectedRecord().getAttribute("razonSocial"));
				dynamicForm.setValue("txtDireccionMatriz", lstEmpresa.getSelectedRecord().getAttribute("direccionMatriz"));
				dynamicForm.setValue("txtContribuyenteEspecial", lstEmpresa.getSelectedRecord().getAttribute("contribuyenteEspecial"));
				boolean obligadocontabilidad;
				if(lstEmpresa.getSelectedRecord().getAttribute("obligadoContabilidad").equals("SI")){
					obligadocontabilidad=true;
				}else{
					obligadocontabilidad=false;
				}
				dynamicForm.setValue("chkObligadoContabilidad", obligadocontabilidad);
				
				tabEmpresa.selectTab(0);
			}
			
		}

		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("obligado")){
				//SC.say("ESTADO CHECK: "+chkobligadoContabilidad.getValueAsBoolean());
				if(chkObligadoContabilidad.getValueAsBoolean()){
					obligado='1';
				}else{
					obligado='0';
				}
			}			
		}		
	}
		
	final AsyncCallback<List<EmpresaDTO>>  objbacklst=new AsyncCallback<List<EmpresaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
			
		}
		public void onSuccess(List<EmpresaDTO> result) {
			getService().numeroRegistros("Tblempresa", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new EmpresaRecords((EmpresaDTO)result.get(i)));
				//SC.say(listado[i].getMarca()+" "+listado[i].getidMarca());
            }
			lstEmpresa.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
		
	};
	final AsyncCallback<EmpresaDTO>  objbackempresa=new AsyncCallback<EmpresaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(EmpresaDTO result) {
			if(result!=null){
				dynamicForm.setValue("txtEmpresa", result.getNombre());  
				dynamicForm.setValue("txtidEmpresa", result.getIdEmpresa()); 
				ban=true;
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
			}
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
			//SC.say("Numero de registros "+registros);
		}
	};

	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			//SC.say("Error no se conecta  a la base");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			SC.say(result);
			getService().listarEmpresa(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
