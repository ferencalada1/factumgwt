package com.giga.factum.client;


import java.util.Date;
import java.util.List;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.client.regGrillas.PedidoProductoelaboradoRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.events.NodeClickHandler;

public class frmPedido extends VLayout{
	VLayout vlayoutPedido;
	TabSet tabsetPedido;
	Tab tabPedido;
	
	DynamicForm dynamicFormTop;
	
	TextItem textitemBusqueda;
	RadioGroupItem radioStock;
	DateItem dateitemInicial;
	DateItem dateitemFinal; 
	
	GridPedido gridPedido;
	
	DynamicForm dynamicFormBotton;
	ButtonItem buttonitemGenerar;
	public frmPedido(){		
		dynamicFormTop = new DynamicForm();
		radioStock  = new RadioGroupItem();
		
		dateitemInicial = new DateItem("fecha_inicial", "Fecha&nbspInicio");
		dateitemInicial.setUseTextField(true);
		dateitemInicial.setEnforceDate(true);
		dateitemInicial.setValue(new Date());
		dateitemInicial.setMaskDateSeparator("-");
		dateitemInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateitemInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		
		dateitemFinal = new DateItem("fecha_final", "Fecha&nbspFin");		
		dateitemFinal.setUseTextField(true);
		dateitemFinal.setEnforceDate(true);		
		dateitemFinal.setValue(new Date());
		dateitemFinal.setMaskDateSeparator("-");
		dateitemFinal.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateitemFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);		
		
		textitemBusqueda = new TextItem("Busqueda");
		
       	radioStock.setVertical(false);  
       	radioStock.setValueMap("Todos", "Num Pedido", "Num Mesa", "Mesero", "Producto");
       	radioStock.setShowTitle(false);
       	radioStock.setDefaultValue("Todos");
		
		dynamicFormTop.setNumCols(10);
		dynamicFormTop.setFields(radioStock,textitemBusqueda, dateitemInicial, dateitemFinal);
		dynamicFormTop.setIsGroup(false);
		dynamicFormTop.setGroupTitle("Filtros");
		dynamicFormTop.setLayoutAlign(Alignment.CENTER);
		
		gridPedido= new GridPedido();

		dynamicFormBotton= new DynamicForm();
		buttonitemGenerar= new ButtonItem("Generar");
		buttonitemGenerar.addClickHandler(new ManejadorBotones("generar"));
		dynamicFormBotton.setFields(buttonitemGenerar);
		
		tabPedido = new Tab();
		
		vlayoutPedido = new VLayout();
		
		vlayoutPedido.addMember(dynamicFormTop);		
		vlayoutPedido.addMember(gridPedido);
		vlayoutPedido.addMember(dynamicFormBotton);
		vlayoutPedido.setMembersMargin(30);
		tabPedido.setPane(vlayoutPedido);
		
		tabsetPedido = new TabSet();
		tabsetPedido.addTab(tabPedido);
		
		addMember(tabsetPedido);
			
	}
	
	public void generarReporte(){
		/*
		String fechaBase[] = dateitemInicial.getDisplayValue().split(" ");
		String orden[] = fechaBase[0].split("-");
		String fecha = orden[1] + "/" + orden[2] + "/" + orden[0];
 		*/
		String fechaI=dateitemInicial.getDisplayValue().replace("/", "-");
		String fechaF=dateitemFinal.getDisplayValue().replace("/", "-");
		String busqueda = textitemBusqueda.getDisplayValue();
		int banderaFiltro=0;
			
				if(radioStock.getValue().toString()=="Producto"){
					banderaFiltro=4;
				}else if(radioStock.getValue().toString()=="Todos"){
					banderaFiltro=3;
				}else if(radioStock.getValue().toString()=="Num Pedido"){
					banderaFiltro=2;
				}else if(radioStock.getValue().toString()=="Num Mesa"){
					banderaFiltro=1;
				}else if(radioStock.getValue().toString()=="Mesero"){
					banderaFiltro=0;
				}
				
				SC.say(busqueda+" - "+fechaI+" - "+fechaF+" - "+banderaFiltro);
				getService().listarPedidos(busqueda, fechaI, fechaF, banderaFiltro, listaCallback);
			
				/*
			if(busqueda.equals("") || busqueda.isEmpty() || busqueda==null && banderaFiltro!=3){
				switch(banderaFiltro){
				case 4:
					SC.say("Ingrese el nombre del producto");
					break;
				case 2:
					SC.say("Ingrese el numero de la orden");
					break;
				case 1:
					SC.say("Ingrese el numero de mesa");
					break;					
				case 0:
					SC.say("Ingrese el nombre del mesero");
					break;
				}				
				
			}else{
				getService().listarPedidos(busqueda, fechaI, fechaF, banderaFiltro, listaCallback);
			}
			*/
			

		
				
			
		
		//getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", "nombre",StockDeseado, listaCallback);
	}
	
	private class ManejadorBotones implements com.smartgwt.client.widgets.form.fields.events.ClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}

		@Override
		public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
			if(indicador.equals("generar")){
				generarReporte();
			}
			
		}


	}
		
	
	final AsyncCallback<List<PedidoProductoelaboradoDTO>>  listaCallback=new AsyncCallback<List<PedidoProductoelaboradoDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			textitemBusqueda.setValue("");
		}
		public void onSuccess(List<PedidoProductoelaboradoDTO> result) {
			SC.say("TAMA�O LISTA: "+result.size());
			ListGridRecord[] listado = new ListGridRecord[result.size()];		
			for(int i=0;i<result.size();i++){
				listado[i]=(new PedidoProductoelaboradoRecords(result.get(i)));
			}
			
			gridPedido.setData(listado);
			
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	    }
	};
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
