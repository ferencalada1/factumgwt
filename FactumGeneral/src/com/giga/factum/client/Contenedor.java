package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.VLayout;

public class Contenedor extends VLayout{

public Contenedor() {
		
		VLayout layout = new VLayout();
		layout.setHeight("39px");
		layout.setWidth100();
		layout.setHeight100();
		
		Cuerpo cuerpo=new Cuerpo();
		cuerpo.setSize("100%","88%");
		
		//cuerpo.setDisabled(true);
		
		Cabecera cabecera=new Cabecera(cuerpo);
		cabecera.setSize("100%","12%");
		layout.addMember(cabecera);
		layout.addMember(cuerpo);
		
		//Footer footer=new Footer();
		//footer.setSize("100%","15%");
		//layout.addMember(footer);
		//footer.setHeight("21px");
		addMember(layout);
	}
	
}
