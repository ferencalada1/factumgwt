package com.giga.factum.client;

import java.util.List;

import com.smartgwt.client.widgets.layout.VLayout;

import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.FormLayoutType;

import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.MaskValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.TransferImgButton;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.google.gwt.user.client.ui.Label;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;

public class frmListClientes extends VLayout{
	ListGrid lstCliente = new ListGrid();
	TabSet tabSet = new TabSet();
	TextItem txtBuscarLst = new TextItem();
	Label lblRegisros = new Label("# Registros");
	int contador=20;
	int registros=0;
   SearchForm searchForm = new SearchForm();
	String tabla="";   
	
		public frmListClientes() {
		
		getService().numeroRegistrosPersona("tblclientes", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		//buscarPicker.addFormItemClickHandler(new ManejadorBotones("Grabar"));

		PickerIcon buscarPickerLst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerLst.addFormItemClickHandler(new ManejadorBotones("buscar"));

		setSize("900", "600");
		tabSet.setSize("100%", "100%");
		
		Tab tabListado = new Tab("Listado Clientes");
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
         
        HLayout hLayout = new HLayout();
        hLayout.setSize("100%", "5%");
         
        searchForm.setSize("88%", "100%");
        searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
        searchForm.setNumCols(4);
        ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n","Tarjeta");
        
 
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerLst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones("buscar")); 
        
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(56);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        hLayout.addMember(searchForm);
        // hLayout.addMember(canvas_1);
         //layout_1.addMember(hLayout);
          
        HStack hStack = new HStack();
        hLayout.addMember(hStack);
        hStack.setSize("12%", "100%");
        TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        layout_1.addMember(hLayout);
        
       /* btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));*/
        
        lstCliente.setSize("100%", "80%");
        lstCliente.setFields(new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), new ListGridField("NombreComercial", "Nombre Comercial:",250), new ListGridField("RazonSocial", "Razon Social",250), new ListGridField("Telefonos", "Tel\u00E9fono",175)
        		,new ListGridField("E-mail", "Correo",175),new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("observaciones","Zona",200),new ListGridField("tarjeta", "C�digo Tarjeta",150),new ListGridField("puntos", "Puntos",150)});
        layout_1.addMember(lstCliente);
        
        //HLayout hLayout = new HLayout();
        //hLayout.setSize("100%", "10%");
        
        
        
        
        tabListado.setPane(layout_1);
        tabSet.addTab(tabListado);
		
		
		
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().ListJoinPersona("tblclientes",0,contador, objbacklst);
		//lstCliente.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
	}
	public frmListClientes(String tabla) {//"tblclientes"
		this.tabla=tabla;
		getService().numeroRegistrosPersona(tabla, objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		//buscarPicker.addFormItemClickHandler(new ManejadorBotones("Grabar"));

		PickerIcon buscarPickerLst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerLst.addFormItemClickHandler(new ManejadorBotones("buscar"));

		setSize("900", "600");
		tabSet.setSize("100%", "100%");
		Tab tabListado = new Tab();
		if(tabla.equals("tblclientes")){
			tabListado = new Tab("Listado Clientes");
		}else if(tabla.equals("tblproveedors")){
			tabListado = new Tab("Listado Proveedores");
		}
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
         
        HLayout hLayout = new HLayout();
        hLayout.setSize("100%", "5%");
         
        searchForm.setSize("88%", "100%");
        searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
        searchForm.setNumCols(4);
        ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        if(tabla.equals("tblclientes")){
        	cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n","Tarjeta");
		}else if(tabla.equals("tblproveedors")){
			cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n");
		}
        
        TextItem txtBuscarLst=new TextItem();
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerLst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones("buscar")); 
        
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(56);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        hLayout.addMember(searchForm);
        // hLayout.addMember(canvas_1);
         //layout_1.addMember(hLayout);
          
        HStack hStack = new HStack();
        hLayout.addMember(hStack);
        hStack.setSize("12%", "100%");
        TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        layout_1.addMember(hLayout);
        
       /* btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));*/
        
        lstCliente.setSize("100%", "80%");
                
                
        if(tabla.equals("tblclientes")){
        	lstCliente.setFields(new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), new ListGridField("NombreComercial", "Nombre Comercial:",250), new ListGridField("RazonSocial", "Razon Social",250), new ListGridField("Telefonos", "Tel\u00E9fono",175)
        			,new ListGridField("E-mail", "Correo",175),new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("observaciones","Zona",200),new ListGridField("tarjeta", "C�digo Tarjeta",150),new ListGridField("puntos", "Puntos",150)});
        }else if(tabla.equals("tblproveedors")){
        	lstCliente.setFields(new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), new ListGridField("NombreComercial", "Nombre Comercial:",250), new ListGridField("RazonSocial", "Razon Social",250), new ListGridField("Telefonos", "Tel\u00E9fono",175)
        			,new ListGridField("E-mail", "Correo",175),new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("Contacto","Contacto",200),new ListGridField("observaciones","Zona",200)});
        }
        
        layout_1.addMember(lstCliente);
        
        //HLayout hLayout = new HLayout();
        //hLayout.setSize("100%", "10%");
        
        
        
        
        tabListado.setPane(layout_1);
        tabSet.addTab(tabListado);
		
		
		
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().ListJoinPersona(tabla,0,contador, objbacklst);
		//lstCliente.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
	}
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String campo=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			//String campo=null;
			if(campo.equals("Cedula")){
				campo="cedulaRuc";
			}else if(campo.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(campo.equals("Razon Social")||campo.equals("")){
				campo="razonSocial";
			}else if(campo.equals("Direcci\u00F3n")){
				campo="direccion";
			}else if(campo.equals("Tarjeta")){
				campo="codigoTarjeta";
			}
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().findJoin2(nombre, tabla, campo, objbacklst); 
			
		}catch(Exception e){
			SC.say(e.getMessage());
		}
			
	}
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void onClick(ClickEvent event){
			String nombre="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones;
			String telefono="",direccion="";
			if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona(tabla,contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona(tabla,contador-20,contador, objbacklst);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona(tabla,contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona(tabla,registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona(tabla,contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}

		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}else if(event.getKeyName().equalsIgnoreCase("tab")) {
				lstCliente.focus();
				try{
				lstCliente.selectRecord(0);}
				catch(Exception e)
				{
					
				}
			}
			
			
		}
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equalsIgnoreCase("buscar")){
				buscarL();
			
			}
			
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		
		
	}
	
	
	
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().ListJoinPersona(tabla,0,contador, objbacklst);
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
		
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			lstCliente.setData(listado);
			lstCliente.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};

}


