package com.giga.factum.client;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.regGrillas.Producto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.gwtext.client.widgets.form.Label;

import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;

public class frmProductoElaborado extends ListGrid{
	//###############################
	//ListGrid grdProductos;
	ListGrid lstProductosCatalogo = new ListGrid();
	LinkedHashMap<Integer, ProductoDTO> listadoProductosCatalogo=null;
	ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",350); 
	
	
	Boolean jerarquiaProducto=false;
	Integer rowNumGlobal = 0;
	HLayout hLayoutPr = new HLayout();
	HStack hStack = new HStack();
	VLayout layout_1 = new VLayout();
	VentanaEmergente listadoProd;
	VentanaEmergente listadoProdCatalogo;

	final ListGridField lgfIDProducto = new ListGridField("idProducto", "I");
	final ListGridField lgfNumero = new ListGridField("numero", "#", 20);
	final ListGridField lgfCodigo = new ListGridField("codigoBarras","Cod. Barras", 150);
	final ListGridField lgfCantidad = new ListGridField("cantidad", "Cantidad",60);// se declaran los campos de la grilla
	final ListGridField lgfDescripcion = new ListGridField("descripcion","Descripci\u00f3n", 400);
	final ListGridField lgfPrecio_unitario = new ListGridField("precioUnitario", "Precio");
	final ListGridField lgfIva = new ListGridField("iva", "Iva");
	final ListGridField lgfStock = new ListGridField("stock", "Stock");
	final ListGridField lgfJerarquia = new ListGridField("jerarquia", "Jerarquia");
	final ListGridField lgfDEscuento = new ListGridField("descuento","Descuento");
	final ListGridField lgfValor_total = new ListGridField("valorTotal", "Subtotal");
	final ListGridField lgfPrecio_iva = new ListGridField("precioConIva","P.IVA");
	final ListGridField lgfEliminar = new ListGridField("eliminar", " ", 25);
	final ListGridField lgfBodega = new ListGridField("idbodega", "IdBodega");
	final ListGridField lgfBodegaN = new ListGridField("bodega", "Bodega");
	final ListGridField lgfBodegaU = new ListGridField("ubicacion","Direccion");
	final ListGridField lgfCosto = new ListGridField("costo", "C");
	final ListGridField lgfCantidadUnidad = new ListGridField("cantidadUnidad", "Cantidad/Unidad");

	SearchForm searchForm = new SearchForm();
	
	Label lblRegisros = new Label("");
	
	int bandera=0;
	int contador=20;
	int registros=0;
	int StockDeseado=2;//Para seleccionar Todos=2, SinStock=0, ConStock=1
	
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapImpuesto = new LinkedHashMap<String, String>();
	TextItem txtBuscarLst = new TextItem();
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	final SelectItem cmdBod = new SelectItem("cmbBod", "Bodega");
	final RadioGroupItem radioStock = new RadioGroupItem();
	HLayout hLayout = new HLayout();
	int op=2;
	int filtroEliminados=0;//para los eliminados, estado=0
	int tipoProducto=0;//para los eliminados, estado=0
	List<ProductoDTO> listadoProductos=null;
	ListGridField lstcantidadcatalogo;
	ListGridField lstcodbarrascatalogo;
	ListGridField lstdescripcioncatalogo;
	ListGridField lstimpuestocatalogo;
	ListGridField lstlifocatalogo;
	ListGridField lstfifocatalogo;
	ListGridField lstpromediocatalogo;
	ListGridField lstunidadcatalogo;
	ListGridField listGridField_6catalogo;
	ListGridField lstcategoriacatalogo;
	ListGridField lstMarcacatalogo;
	ListGridField lstPVPcatalogo;
	ListGridField lstAfiliadocatalogo;
	ListGridField lstMayoristacatalogo;
	ListGridField lstobservacionescatalogo;
	ListGridField lstDESCcatalogo;
	ListGridField inventarioInicialcatalogo;
	ListGridField lstpadre2catalogo;
	ListGridField lstcantidadunidadcatalogo;
	ListGridField lstjerarquiacatalogo;
	ListGridField lstpadrecatalogo;
	
	public frmProductoElaborado() {
		hLayout.setSize("100%", "6%");
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.addFormItemClickHandler(new Manejador("Buscar"));
		
		setAlign(Alignment.CENTER);
		
		setCanEdit(true);
		setModalEditing(true);
		setAutoSaveEdits(true);
		setWidth("20%");
		setEditEvent(ListGridEditEvent.DOUBLECLICK);
		
		lgfCantidad.setRequired(true);// los campos que son requeridos
		lgfCantidad.setCanEdit(true);
		lgfCantidad.setCellAlign(Alignment.CENTER);
		lgfCantidad.setType(ListGridFieldType.FLOAT);
		lgfCantidad.setCanEdit(true);
		
		lgfCodigo.setRequired(true);
		
		lgfDescripcion.setRequired(true);
		
		lgfIDProducto.setRequired(true);
		lgfIDProducto.setHidden(true);
		
		lgfEliminar.setAlign(Alignment.CENTER);  
		lgfEliminar.setType(ListGridFieldType.IMAGE);
		lgfEliminar.setImgDir("delete.png");
		lgfEliminar.setCanEdit(false);
		lgfEliminar.setDefaultValue("   ");		
		lgfCantidadUnidad.setRequired(true);	
		lgfPrecio_unitario.addChangeHandler(new Manejador("pvp"));
		lgfPrecio_unitario.setHidden(true);		
		
		lgfDescripcion.addChangeHandler(new Manejador("descripcion"));
		
		lgfIva.setHidden(true);
		
		lgfDEscuento.setHidden(true);
		lgfBodega.addChangeHandler(new Manejador("bodega"));
		lgfStock.setCanEdit(false);
		lgfStock.setHidden(true);
		lgfJerarquia.setHidden(true);
		lgfValor_total.setHidden(true);
		lgfPrecio_iva.setHidden(true);
		lgfBodega.setHidden(true);
		lgfBodegaN.setHidden(true);
		lgfBodegaU.setHidden(true);
		lgfCosto.setHidden(true);
		lgfCantidadUnidad.setHidden(true);
		
		setFields(lgfIDProducto, lgfNumero, lgfCodigo,
				lgfCantidad, lgfDescripcion, lgfEliminar,lgfCantidadUnidad, lgfPrecio_unitario, lgfIva,
				lgfStock, lgfDEscuento,
				lgfValor_total, lgfPrecio_iva,
				lgfBodega, lgfBodegaN, lgfBodegaU,lgfCosto, lgfJerarquia);

		setHeight("50%");		
		
		setWidth100();
		setAlign(Alignment.CENTER);
		//this.addMember(grdProductos);
		
		startEditingNew();
		draw();
		saveAllEdits();
		
		//######################################
		lstcodbarrascatalogo = new ListGridField("codigoBarras",
				"C\u00F3digo de Barras", 100);
		lstcodbarrascatalogo.setCellAlign(Alignment.LEFT);
		lstdescripcioncatalogo = new ListGridField("descripcion",
				"Descripci\u00F3n", 350);
		lstimpuestocatalogo = new ListGridField("impuesto", "Impuesto",
				100);
		lstimpuestocatalogo.setCellAlign(Alignment.CENTER);
		lstlifocatalogo = new ListGridField("lifo", "LIFO", 60);
		lstlifocatalogo.setCellAlign(Alignment.CENTER);
		lstfifocatalogo = new ListGridField("fifo", "FIFO", 60);
		lstfifocatalogo.setCellAlign(Alignment.CENTER);
		lstpromediocatalogo = new ListGridField("promedio", "Costo", 60);
		lstpromediocatalogo.setCellAlign(Alignment.CENTER);
		lstunidadcatalogo = new ListGridField("Unidad", "Unidad", 100);
		listGridField_6catalogo = new ListGridField("stock", "Stock", 50);
		listGridField_6catalogo.setCellAlign(Alignment.CENTER);
		lstcategoriacatalogo = new ListGridField("Categoria",
				"Categoria", 100);
		lstcategoriacatalogo.setCellAlign(Alignment.CENTER);
		lstMarcacatalogo = new ListGridField("Marca", "Marca", 100);
		lstMarcacatalogo.setCellAlign(Alignment.CENTER);
		lstPVPcatalogo = new ListGridField("PVP", "PVP", 60);
		lstPVPcatalogo.setCellAlign(Alignment.CENTER);
		lstAfiliadocatalogo = new ListGridField("Afiliado", "Afiliado",60);
		lstAfiliadocatalogo.setCellAlign(Alignment.CENTER);
		lstMayoristacatalogo = new ListGridField("Mayorista","Mayorista", 60);
		lstMayoristacatalogo.setCellAlign(Alignment.CENTER);
		lstcantidadcatalogo = new ListGridField("cantidad", "Cantidad",50);
		lstcantidadcatalogo.setCellAlign(Alignment.CENTER);
		lstcantidadcatalogo.setType(ListGridFieldType.FLOAT);
		lstcantidadcatalogo.setCanEdit(true);

		lstobservacionescatalogo = new ListGridField("observacion",
				"Observaciones", 250);
		lstobservacionescatalogo.addChangeHandler(new Manejador("agregarObservacion"));
		
		lstobservacionescatalogo.setCanEdit(true);
		lstDESCcatalogo = new ListGridField("DTO", "DTO %", 40);
		lstDESCcatalogo.setCellAlign(Alignment.CENTER);
		//lstDESCcatalogo.addEditorExitHandler(new Manejador("agregarGrilla"));
		inventarioInicialcatalogo = new ListGridField("inicial",
				"I. Inicial", 50);
		inventarioInicialcatalogo.setCellAlign(Alignment.CENTER);
		lstpadre2catalogo = new ListGridField("stockPadre", "Padre M.P",60);
		lstcantidadunidadcatalogo = new ListGridField("cantidadUnidad", "Cantidad/Unidad",60);
		lstjerarquiacatalogo = new ListGridField("jerarquia", "Jerarquia",60);
		lstpadrecatalogo = new ListGridField("Padre", "Padre", 60);

		lstProductosCatalogo.setFields(new ListGridField[] { // GIGACOMPUTERS
				lstcantidadcatalogo, lstcodbarrascatalogo, lstdescripcioncatalogo, lstDESCcatalogo,
						lstobservacionescatalogo, listGridField_6catalogo, inventarioInicialcatalogo,
						lstpadre2catalogo, lstMayoristacatalogo, lstAfiliadocatalogo, lstPVPcatalogo,
						lstimpuestocatalogo, lstpromediocatalogo, lstunidadcatalogo, lstpadrecatalogo, lstjerarquiacatalogo,lstcantidadunidadcatalogo});
		
		
		//######################################
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		txtBuscarLst = new TextItem("txtBuscarLst", "");
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		txtBuscarLst.addKeyPressHandler(new Manejador(""));
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n",/*"Ubicaci\u00F3n",*/"Categoria"   // No se requiere en la version basica
				,"Unidad","Marca");
		cmdBod.setLeft(321);
		cmdBod.setTop(6); 
		cmdBod.setHint("Seleccione una ");
		cmdBod.setVisible(false);
		cmdBod.addChangedHandler(new Manejador("Buscar"));
		cmbBuscar.addChangedHandler(new Manejador(""));
		radioStock.setColSpan("*");  //Para que el titulo se ponga en una linea
       	radioStock.setVertical(false);  
       	radioStock.setValueMap("Todos", "Con Stock", "Sin Stock","Eliminados", "Materia Prima");
       	radioStock.setTitle(" ");
       //	radioStock.setTitleOrientation(TitleOrientation.TOP);
       	radioStock.setDefaultValue("Todos");
       	
       
       	
       	DynamicForm DContenedor = new DynamicForm();
       	DContenedor.setSize("20%", "100%");
		searchForm.setFields(new FormItem[] {txtBuscarLst, cmbBuscar, cmdBod});
		//txtRegistros.disable();
		DContenedor.setItems(radioStock);
		hLayout.addMember(DContenedor);
		//dinReg.setFields(txtRegistros);
		hLayout.addMember(searchForm);
		//######################################33
	}
	
	
	private class Manejador implements  KeyPressHandler, ChangeHandler, FormItemClickHandler, ChangedHandler{
		// manejador de cuando presione esc,etc
		String indicador = "";// para indicar que fue lo que se presiono tendra
								// el nombre
		
		Manejador(String nombreBoton) {
			this.indicador = nombreBoton;
		}
								
								

								public void onChange(ChangeEvent event) {
									// TODO Auto-generated method stub
									String cadena = event.getValue().toString();
									if (indicador.equals("descripcion")) {
										//Primero analizamos si ha alcanzado el numero maximo de items

											mostrarProductos();
										
									}
								}
								
								
								
								@Override
								public void onKeyPress(KeyPressEvent event) {
									if(event.getKeyName().equalsIgnoreCase("enter")) {
										buscarL();
									}

									
								}
								
								
								
								
								public void onFormItemClick(FormItemIconClickEvent event) {
									if(indicador.equalsIgnoreCase("Buscar")){ 
										buscarL();
									}
									
								}



								@Override
								public void onChanged(ChangedEvent event) {
									if(indicador.equalsIgnoreCase("Buscar")){
										DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
										String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
										if(radioStock.getValue().toString()=="Todos")
										{
												StockDeseado=2;
										}
										else
										{
											if(radioStock.getValue().toString()=="Con Stock")
											{
													StockDeseado=1;
											}
											else
											{
												StockDeseado=0;
											}
										}
										getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,StockDeseado,1,0,1,Factum.banderaStockNegativo, listaCallback);
							    		
										//getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", "nombre",StockDeseado, listaCallback);
									}
												
								}
									
								
								
		
		}
	//#################################################################33	
		public static GreetingServiceAsync getService(){
			return GWT.create(GreetingService.class);
		}
		//#################################################################33
		public void mostrarProductos() {
			// Se muestra la ventana de busqueda de producto
			rowNumGlobal = getEditRow();
			getService().listarProductoMateriaPrimaLike("", "codigoBarras",StockDeseado, listaCallback);
			layout_1.setSize("100%", "100%");
			hLayoutPr.setSize("100%", "6%");
			searchForm.setSize("85%", "100%");
			hStack.setSize("12%", "100%");
	
			lstProductosCatalogo.setAutoFitData(Autofit.VERTICAL);
			lstProductosCatalogo.setAutoFitMaxRecords(10);
			lstProductosCatalogo.setAutoFetchData(true);
			lstProductosCatalogo.setSize("100%", "80%");
			lblRegisros.setSize("100%", "4%");
			hLayoutPr.addMember(searchForm);
			hLayoutPr.addMember(hStack);
			layout_1.addMember(hLayoutPr);
			layout_1.addMember(lstProductosCatalogo);
			//layout_1.addMember(lblRegisros);
			listadoProdCatalogo = new VentanaEmergente("Listado de Productos", layout_1,false, true);
			listadoProdCatalogo.setWidth(930);
			listadoProdCatalogo.setHeight(610);
			listadoProdCatalogo.addCloseClickHandler(new CloseClickHandler() {
				public void onCloseClick(CloseClientEvent event) {
					listadoProdCatalogo.destroy();
				}
			});
			listadoProdCatalogo.show();
		}
		
		//#################################################################33		
		final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
			public void onFailure(Throwable caught) {
				SC.say("Error dado:" + caught.toString());
				
			}
			public void onSuccess(Integer result) {
			//	registros=result;
			
			}
		};
		//#################################################################33
		public void mostrarBodegas(String idProducto) {			
			rowNumGlobal += 1;

		}
		//#################################################################33		
		public void buscarL(){
			bandera=1;
			int ban=1;
			contador=20;
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cmbBuscar").getDisplayValue();
			String campo=null;
			if(tabla.equals("Ubicaci\u00F3n")){
				ban=3;
				tabla="codigoBarras";
			}else if(tabla.equals("C\u00F3digo de Barras")){
				ban=1;
				tabla="codigoBarras";
			}else if(tabla.equals("Descripci\u00F3n") ||tabla.equals("")||tabla.equals("&nbsp;")){
				ban=1;
				tabla="descripcion";
			}else if(tabla.equals("Marca")){
				ban=0;
				campo="marca";
				tabla="tblmarca";
			}else if(tabla.equals("Categoria")){
				ban=0;
				campo="categoria";
				tabla="tblcategoria";
			}else if(tabla.equals("Unidad")){
				ban=0;
				campo="nombre";
				tabla="tblunidad";
			}
			if(radioStock.getValue().toString()=="Todos"){
					StockDeseado=2;
					op=2;//OP sirve para las opciones de filtrado al momento de usar los botones direccionales
			}else if(radioStock.getValue().toString()=="Con Stock")
			{		StockDeseado=1;
			        op=1;
			}else if(radioStock.getValue().toString()=="Sin Stock"){
					StockDeseado=0;
					op=0;
			}else if(radioStock.getValue().toString()=="Eliminados"){
				filtroEliminados=3;//para el filtro de eliminados (0 inicialmente, 3 cuando se ha pulsado esta opcion)
				op=3;
			}else if(radioStock.getValue().toString()=="Materia Prima"){
				tipoProducto=3;//para el filtro de eliminados (0 inicialmente, 3 cuando se ha pulsado esta opcion)
				op=4;
			}
				
			//validando que la busqueda no tenga valores ilogicos o vacios en el combo o caja de busqueda
	       
	        if(filtroEliminados==3){
	        	getService().listarProductoLikeEliminados(nombre, tabla,filtroEliminados, listaCallback);
	        	
	        	filtroEliminados=0;
	        }
	        else{
	        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	        	//SC.say("Bandera: "+ban);
	    		if(ban==1){
	    			getService().listarProductoLike2(nombre, tabla,StockDeseado,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	    			
	    		}else if(ban==0){
	    			getService().listarProductoJoin2(nombre, tabla, campo,StockDeseado,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	    		}else if(ban==3){
	    			getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,StockDeseado,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	    		}else if(ban==4){
	    				if(tipoProducto==0){
	    					getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,tipoProducto,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	    				}else{
	    					getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,tipoProducto,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	    				}
	    		}
	    		
	        }
			
		}
		//#################################################################33
		
		final AsyncCallback<List<ProductoDTO>>  listaCallbackProducto=new AsyncCallback<List<ProductoDTO>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(List<ProductoDTO> result) {
				listadoProductos=null;
				listadoProductos=result;
				
				
				if(bandera==1){
				getService().numeroRegistrosProducto("tblproducto", objbackI);
				int numProductos=result.size();	
				registros=numProductos;
				}
				
			RecordList listado = new RecordList();
				Iterator iter = result.iterator();
				int numProd=0;
	            while (iter.hasNext()) {
	            	Producto pro =new Producto((ProductoDTO)iter.next());
	            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
	            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
	            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
	            	listado.add(pro);      	
	             	numProd++;
	            }
	     
	            lblRegisros.setText(contador+" def "+registros);
	            bandera=0;
			
	            lstProductosCatalogo.setData(new RecordList());
	            lstProductosCatalogo.setData(listado);
	            lstdescripcion.setSortDirection(SortDirection.ASCENDING);
	            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
		};
		
		
		//#################################################################33	
		final AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(LinkedHashMap<Integer, ProductoDTO> result) {
				
				//SC.say("TAMA�O LISTA: "+ result.size());
				listadoProductosCatalogo=null;
				listadoProductosCatalogo=result;
				
				
				if(bandera==1){
				//getService().numeroRegistrosProducto("tblproducto", objbackI);
				int numProductos=result.size();	
				registros=numProductos;
				}
				
				RecordList listado = new RecordList();
				//	ListGridRecord[] listadoProd = new ListGridRecord[result.size()];
				Iterator iter = result.values().iterator();
				
				
				int numProd=0;
	            while (iter.hasNext()) {
	            	Producto pro =new Producto((ProductoDTO)iter.next());
	            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
	            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
	            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
	            	listado.add(pro);      	
	             	numProd++;
	            }
	     
	            lblRegisros.setText(contador+" def "+registros);
	            bandera=0;
			
	            lstProductosCatalogo.setData(new RecordList());
	            lstProductosCatalogo.setData(listado);
	            
	            
	            lstdescripcion.setSortDirection(SortDirection.ASCENDING);
	            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
		};
				

}
