package com.giga.factum.client;

import com.google.gwt.i18n.client.Constants;

public interface FactumProp extends Constants{
	String port();
}
