package com.giga.factum.client;

public class ReporteRetencion implements java.io.Serializable 
{
	//select tp.idPersona,tp.nombres,tp.apellidos,tp.direccion,tc.fecha,tc.idDtoComercial,
	//tc.tipoTransaccion,tr.idRetencion,tr.autorizacionSri,tr.numero,sum(tr.valorRetenido)
	private static final long serialVersionUID = 1L;	
	private int idPersona;
	private String nomPersona;
	private String apePersona;
	private String dirPersona;
	private String fecha;
	private int idDtoComercial;
	private int tipoTransaccion;
	private int idRetencion;
	private String autorizacionSri;
//	private String numero;
	private int numero;
	private double totalRetencion;
	private String cedPersona;
	
	public ReporteRetencion() {}

	public ReporteRetencion(int idPersona,String nomPersona,String apePersona,String dirPersona,String fecha,int idDtoComercial,
			int tipoTransaccion,int idRetencion,String autorizacionSri,/*String numero*/ int numero,double totalRetencion, String cedPersona) 
	{
		this.idPersona=idPersona;
		this.nomPersona=nomPersona;
		this.apePersona=apePersona;
		this.dirPersona=dirPersona;
		this.fecha=fecha;
		this.idDtoComercial=idDtoComercial;
		this.tipoTransaccion=tipoTransaccion;
		this.idRetencion=idRetencion;
		this.autorizacionSri=autorizacionSri;
		this.numero=numero;
		this.totalRetencion=totalRetencion;	
		this.cedPersona= cedPersona;
	}
		
	public int getIdPersona() 
	{return this.idPersona;}
	public void setIdPersona(int idPersona) 
	{this.idPersona = idPersona;}
	
	public String getNomPersona() 
	{return this.nomPersona;}
	public void setNomPersona(String nomPersona) 
	{this.nomPersona = nomPersona;}
	
	public void setApePersona(String apePersona) 
	{this.apePersona = apePersona;}
	public String getApePersona() 
	{return this.apePersona;}
	
	public void setDirPersona(String dirPersona) 
	{this.dirPersona = dirPersona;}
	public String getDirPersona() 
	{return this.dirPersona;}
	
	public void setIdDtoComercial(int idDtoComercial) 
	{this.idDtoComercial = idDtoComercial;}
	public int getIdDtoComercial() 
	{return this.idDtoComercial;}

	public int getTipoTransaccion() 
	{return this.tipoTransaccion;}
	public void setTipoTransaccion(int tipoTransaccion) 
	{this.tipoTransaccion = tipoTransaccion;}

	
	public int getIdRetencion() 
	{return this.idRetencion;}
	public void setIdRetencion(int idRetencion) 
	{this.idRetencion = idRetencion;}
		
	public String getAutorizacionSri() 
	{return this.autorizacionSri;}
	public void setAutorizacionSri(String autorizacionSri) 
	{this.autorizacionSri = autorizacionSri;}

	
	/*public String getNumero() 
	{return this.numero;}
	public void setNumero(String numero) 
	{this.numero = numero;}
*/
	public int getNumero() 
	{return this.numero;}
	public void setNumero(int numero) 
	{this.numero = numero;}
	
	public String getFecha() 
	{return this.fecha;}
	public void setFecha(String fecha) 
	{this.fecha = fecha;}
	
	public double getTotalRetencion() 
	{return this.totalRetencion;}
	public void setTotalRetencion(double totalRetencion) 
	{this.totalRetencion = totalRetencion;}
	
	public String getCedPersona() 
	{return this.cedPersona;}
	public void setCedPersona(String cedPersona) 
	{this.cedPersona = cedPersona;}
}

