package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TmpAuxgeneralDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class BalanceGeneralRecords extends ListGridRecord{
	public BalanceGeneralRecords()
	{
		
	}
	public BalanceGeneralRecords(TmpAuxgeneralDTO bal)
	{
		setcodigo(bal.getTmpcodigo());
		setcuenta(bal.getTmpcuenta());
		setresta(bal.getResta());
	}
	public void setcodigo(String codigo)
	{

		setAttribute("codigo",codigo);
	}
	public void setcuenta(String cuenta)
	{

		setAttribute("cuenta",cuenta);
	}
	public void setresta(java.math.BigDecimal resta)
	{

		setAttribute("resta",resta);
	}
}
