package com.giga.factum.client.regGrillas;


import com.giga.factum.client.DTO.UnidadDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class UnidadRecords extends ListGridRecord {
	public UnidadRecords() {
    }
	public UnidadRecords(UnidadDTO mar) {
        setidUnidad(mar.getIdUnidad());
        setUnidad(mar.getUnidad());
	}
    public UnidadRecords(int id,String Unidad) {
        setidUnidad(id);
        setUnidad(Unidad);
    }

	private void setUnidad(String Unidad) {
		// TODO Auto-generated method stub
		setAttribute("Unidad", Unidad);
	}

	private void setidUnidad(int idUnidad) {
		// TODO Auto-generated method stub
		setAttribute("idUnidad", idUnidad);
	}
	public String getUnidad() {
        return getAttributeAsString("Unidad");
    }
    public int getidUnidad(){
    	return getAttributeAsInt("idUnidad");
    }

}
