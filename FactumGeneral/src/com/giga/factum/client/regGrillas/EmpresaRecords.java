package com.giga.factum.client.regGrillas;


import com.giga.factum.client.DTO.EmpresaDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class EmpresaRecords extends ListGridRecord {
	public EmpresaRecords() {
    }
	public EmpresaRecords(EmpresaDTO mar) {
        setidEmpresa(String.valueOf(mar.getIdEmpresa()));
        setEmpresa(mar.getNombre());
        setRUC(mar.getRUC());
        setRazonSocial(mar.getRazonSocial());
        setDireccionMatriz(mar.getDireccionMatriz());
        setContribuyenteEspecial(mar.getContribuyenteEspecial());
        String obligadocontabilidad="";
        if(String.valueOf(mar.getObligadoContabilidad()).equals("1")){
        	obligadocontabilidad="SI";
        }else{
        	obligadocontabilidad="NO";
        }
        setObligadoContabilidad(obligadocontabilidad);
        
	}
	
    public EmpresaRecords(String id,String empresa) {
        setidEmpresa(id);
        setEmpresa(empresa);
    }

	private void setEmpresa(String empresa) {
		
		setAttribute("Empresa", empresa);
	}
	private void setidEmpresa(String idEmpresa) {
		
		setAttribute("idEmpresa", idEmpresa);
	}
	public String getEmpresa() {
        return getAttributeAsString("Empresa");
    }
	public String getidEmpresa() {
        return getAttributeAsString("idEmpresa");
    }
	private void setRUC(String RUC) {
		
		setAttribute("RUC", RUC);
	}
	public String getRUC() {
        return getAttributeAsString("RUC");
    }
	private void setDireccionMatriz(String direccionMatriz) {
		
		setAttribute("direccionMatriz", direccionMatriz);
	}
	public String getDireccionMatriz() {
        return getAttributeAsString("direccionMatriz");
    }
	private void setContribuyenteEspecial(String contribuyenteEspecial) {
		
		setAttribute("contribuyenteEspecial", contribuyenteEspecial);
	}
	public String getContribuyenteEspecial() {
        return getAttributeAsString("contribuyenteEspecial");
    }
	private void setObligadoContabilidad(String obligadoContabilidad) {
		
		setAttribute("obligadoContabilidad", obligadoContabilidad);
	}
	public String getObligadoContabilidad() {
        return getAttributeAsString("razonSocial");
    }
private void setRazonSocial(String razonSocial) {
		
		setAttribute("razonSocial", razonSocial);
	}
	public String getRazonSocial() {
        return getAttributeAsString("razonSocial");
    }
}
