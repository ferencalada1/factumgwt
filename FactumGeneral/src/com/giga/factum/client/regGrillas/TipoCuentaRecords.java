package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TipoCuentaRecords  extends ListGridRecord{
	public TipoCuentaRecords() {
    }
	public TipoCuentaRecords(TipoCuentaDTO mar) {
        setidTipoCuenta(String.valueOf(mar.getIdTipoCuenta()));
        setTipoCuenta(mar.getNombre());
	}
    public TipoCuentaRecords(String id,String TipoCuenta) {
    	setidTipoCuenta(id);
        setTipoCuenta(TipoCuenta);
    }

	private void setTipoCuenta(String TipoCuenta) {
		
		setAttribute("TipoCuenta", TipoCuenta);
	}
	
	private void setidTipoCuenta(String idTipoCuenta) {
		
		setAttribute("idTipoCuenta", idTipoCuenta);
	}
	public String getTipoCuenta() {
        return getAttributeAsString("TipoCuenta");
    }
	public String getidTipoCuenta() {
        return getAttributeAsString("idTipoCuenta");
    }

}
