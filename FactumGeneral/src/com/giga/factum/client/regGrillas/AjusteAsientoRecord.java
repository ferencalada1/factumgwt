package com.giga.factum.client.regGrillas;

import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AjusteAsientoRecord extends ListGridRecord {
	
	
	public AjusteAsientoRecord(String idAsiento, String idCuenta, String nombreCuenta,String valorDebe,String valorHaber){
		setIdAsiento(idAsiento);
		setIdCuenta(idCuenta);
		setNombreCuenta(nombreCuenta);
		setValorDebe(valorDebe);
		setValorHaber(valorHaber);
	}
	
	private void setIdAsiento(String idAsiento) {		
		setAttribute("idAsiento", idAsiento);
	}
	private void setIdCuenta(String idCuenta) {		
		setAttribute("idCuenta", idCuenta);
	}	
	private void setNombreCuenta(String nombreCuenta) {		
		setAttribute("nombreCuenta", nombreCuenta);
	}
	private void setValorDebe(String valorDebe) {		
		setAttribute("valorDebe", valorDebe);
	}
	private void setValorHaber(String valorHaber) {		
		setAttribute("valorHaber", valorHaber);
	}	
}
