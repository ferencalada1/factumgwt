package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TipoPrecioRecords extends ListGridRecord {
	public TipoPrecioRecords() {
    }
	public TipoPrecioRecords(TipoPrecioDTO tipo) {
        setidTipoPrecio(tipo.getIdTipoPrecio());
        setTipoPrecio(tipo.getTipoPrecio());
        setPrecioC(0);
        setPrecioV(0);
	}
	public void setPorcentaje(double i) {
    	setAttribute("Porcentaje", i);
	}
    public void setPrecioC(double i) {
    	setAttribute("PrecioC", i);
	}
    public void setPrecioV(double i) {
    	setAttribute("PrecioV", i);
	}
    
    private double getPrecioC() {
    	return this.getAttributeAsDouble("PrecioC");
	}
    private double getPrecioV() {
    	return this.getAttributeAsDouble("PrecioV");
	}
	public TipoPrecioRecords(int id,String marca) {
    	setidTipoPrecio(id);
    	setTipoPrecio(marca);
    }

	private void setTipoPrecio(String tipo) {
		setAttribute("TipoPrecio", tipo);
	}
	
	private float getProcentaje() {
		return getAttributeAsFloat("Porcentaje");
	}
	private void setidTipoPrecio(int idTipo) {
		setAttribute("idTipoPrecio", idTipo);
	}
	public String getTipoPrecio() {
        return getAttributeAsString("TipoPrecio");
    }


}
