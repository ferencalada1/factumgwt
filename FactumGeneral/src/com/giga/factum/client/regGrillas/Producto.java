package com.giga.factum.client.regGrillas;

import java.util.List;
import java.util.Set;

import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class Producto extends ListGridRecord{
	
	 /**
	 * 
	 */
	public Producto() {
	    }

	public Producto(ProductoDTO pr) {
        String estado = String.valueOf(pr.getEstado());
		setIdProducto(pr.getIdProducto());
        setCodigoBarras(pr.getCodigoBarras());
        setDescripcion(pr.getDescripcion());
        setStock(pr.getStock());
//        setImpuesto(pr.getImpuesto());
        setMultiImpuesto(pr.getTblmultiImpuesto());
        setLifo(pr.getLifo());
        setFifo(pr.getFifo());
        setPromedio(pr.getPromedio());
        setIdUnidad(pr.getTblunidad());
        setIdCategoria(pr.getTblcategoria());
        setIdMarca(pr.getTblmarca());
        setEstado(estado);
        setImagen(pr.getImagen());
        setSerie("Serie");
        setJerarquia(String.valueOf(pr.getJerarquia()));
        setCantidadunidad(String.valueOf(pr.getCantidadunidad()));
        for(ProductoTipoPrecioDTO  productotipopreciodto:pr.getProTip()){
			if (productotipopreciodto.getTbltipoprecio().getTipoPrecio().toUpperCase().equals("PUBLICO"))
				setAttribute("PVP", productotipopreciodto.getPorcentaje());
			if (productotipopreciodto.getTbltipoprecio().getTipoPrecio().toUpperCase().equals("AFILIADO"))
				setAttribute("Afiliado", productotipopreciodto.getPorcentaje());
			if (productotipopreciodto.getTbltipoprecio().getTipoPrecio().toUpperCase().equals("MAYORISTA"))
				setAttribute("Mayorista", productotipopreciodto.getPorcentaje());
		setAttribute(productotipopreciodto.getTbltipoprecio().getTipoPrecio().toUpperCase(), productotipopreciodto.getPorcentaje());
		}
        //setPVP(pr.getProTip().get(1).getPorcentaje());
        //setMayorista(pr.getProTip().get(0).getPorcentaje());
        //setAfiliado(pr.getProTip().get(2).getPorcentaje());
        
    }
			

			public Producto(
	            Integer IdProducto,
	            String CodigoBarras,
	            String Descripcion,
	            Double Stock,
	            /*Double Impuesto,*/
	            Double Lifo,
	            Double Fifo,
	            Double Promedio,
	            Integer IdUnidad,
	            String unidad,
	            Integer IdCategoria,
	            String categoria,
	            Integer IdMarca,
	            String marca,
	            String Estado) {
	        setIdProducto(IdProducto);
	        setCodigoBarras(CodigoBarras);
	        setDescripcion(Descripcion);
	        setStock(Stock);
	        //setImpuesto(Impuesto);
	        setLifo(Lifo);
	        setFifo(Fifo);
	        setPromedio(Promedio);
	        setIdUnidad(IdUnidad);
	        setIdCategoria(IdCategoria);
	        setIdMarca(IdMarca);
	        setEstado(Estado);
	    }
			private void setPVP(double d) {
				try{
					setAttribute("PVP",d);
				}catch(Exception e){
					SC.say(e.getMessage());
				}
				
			}
			private void setAfiliado(double porcentaje) {
				setAttribute("Afiliado",porcentaje);
				
			}

			private void setMayorista(double porcentaje) {
				setAttribute("Mayorista",porcentaje);
				
			}
		private void setSerie(String string) {
			setAttribute("buttonField",string);
		}
	    public void setIdProducto(Integer idProducto) {
	        setAttribute("idProducto", idProducto);
	    }
	    public void setUnidad(String Unidad) {
	        setAttribute("Unidad", Unidad);
	    }
	    public void setCategoria(String Cat){
	    	this.setAttribute("Categoria",Cat);
	    }
	    public void setMarca(String marca){
	    	this.setAttribute("Marca",marca);
	    }
	    public String setUnidad() {
	        return getAttribute("Unidad");
	    }
	    public String getCategoria(){
	    	return this.getAttribute("Categoria");
	    }
	    public String getMarca(){
	    	return this.getAttribute("Marca");
	    }
	    public void setCodigoBarras(String codigoBarras) {
	        setAttribute("codigoBarras", codigoBarras);
	    }
	    public void setDescripcion(String descripcion) {
	        setAttribute("descripcion", descripcion);
	    }
	    public void setStock(Double stock) {
	        setAttribute("stock", stock);
	    }
//	    public void setImpuesto(Double impuesto) {
//	        setAttribute("impuesto", impuesto);
//	    }
	    public void setMultiImpuesto(Set<TblproductoMultiImpuestoDTO> tblmultiImpuesto) {
	    	String valores="";int cont=0;
	    	for (TblproductoMultiImpuestoDTO multi:tblmultiImpuesto){
	    		cont++;
	    		valores+=String.valueOf(multi.getTblmultiImpuesto().getPorcentaje());
	    		if (cont<tblmultiImpuesto.size()) valores+=",";
	    	}
	        setAttribute("impuesto", valores);
	    }
	    public void setLifo(Double lifo) {
	        setAttribute("lifo", lifo);
	    }
	    public void setFifo(Double fifo) {
	        setAttribute("fifo", fifo);
	    }
	    public void setPromedio(Double promedio) {
	        setAttribute("promedio", promedio);
	    }
	    public void setIdUnidad(Integer idUnidad) {
	        setAttribute("unidad", idUnidad);
	    }
	    public void setIdCategoria(Integer idCategoria) {
	        setAttribute("categoria", idCategoria);
	    }
	    public void setIdMarca(Integer idMarca) {
	        setAttribute("marca", idMarca);
	    }
	    public void setEstado(String estado) {
	        setAttribute("estado", estado);
	    }
	    public void setJerarquia(String jerarquia) {
	    	setAttribute("jerarquia", jerarquia);
	    }
	    public void setImagen(String imagen) {
	    	setAttribute("Imagen", imagen);
	    }
	    public void setDescuento(String descuento) {
	    	setAttribute("Descuento", descuento);
	    }
	    public void setCantidadunidad(String cantidadunidad) {
	    	setAttribute("cantidadUnidad", cantidadunidad);
	    }

	    public Integer getIdProducto() {
	        return getAttributeAsInt("idProducto");
	    }
	    
	    public String getCodigoBarras() {
	        return getAttributeAsString("codigoBarras");
	    }
	    public String getDescripcion() {
	    	return getAttributeAsString("descripcion");
	    }
	    public Double getStock() {
	    	return getAttributeAsDouble("stock");
	    }
//	    public Double getImpuesto() {
//	    	return getAttributeAsDouble("impuesto");
//	    }
	    public Double getLifo() {
	        return getAttributeAsDouble("lifo");
	    }
	    public Double getFifo() {
	    	return getAttributeAsDouble("fifo");
	    }
	    public Double getPromedio() {
	    	return getAttributeAsDouble("promedio");
	    }
	    public Integer getIdUnidad() {
	        return getAttributeAsInt("unidad");
	    }
	    public Integer getIdCategoria() {
	        return getAttributeAsInt("categoria");
	    }
	    public Integer getIdMarca() {
	        return getAttributeAsInt("marca");
	    }
	    public String getEstado() {
	        return getAttributeAsString("estado");
	    }
	    public String getJerarquia() {
	        return getAttributeAsString("jerarquia");
	    }


}
