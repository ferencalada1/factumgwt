package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.CargoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CargoRecords extends ListGridRecord {
	public CargoRecords() {
    }
	public CargoRecords(CargoDTO mar) {
        setidCargo(String.valueOf(mar.getIdCargo()));
        setCargo(mar.getDescripcion());
	}
    public CargoRecords(String id,String Cargo) {
        setidCargo(id);
        setCargo(Cargo);
    }

	private void setCargo(String Cargo) {
		// TODO Auto-generated method stub
		setAttribute("Cargo", Cargo);
	}

	private void setidCargo(String idCargo) {
		// TODO Auto-generated method stub
		setAttribute("idCargo", idCargo);
	}
	public String getCargo() {
        return getAttributeAsString("Cargo");
    }
	public String getidCargo() {
        return getAttributeAsString("idCargo");
    }
	

}
