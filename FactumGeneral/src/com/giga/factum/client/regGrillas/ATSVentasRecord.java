package com.giga.factum.client.regGrillas;

import java.text.DecimalFormat;

import com.giga.factum.client.DTO.detalleVentasATSDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ATSVentasRecord extends ListGridRecord {
	
	public ATSVentasRecord(){
		
	}
	public ATSVentasRecord(detalleVentasATSDTO det){
		
		if(det.getPersona().getCedulaRuc().length()==10){
			setAttribute("tpIdCliente", "05");
		}else if(det.getPersona().getCedulaRuc().length()==13){
			setAttribute("tpIdCliente", "04");
		}
		if(det.getPersona().getCedulaRuc().equals("9999999999999")){
			setAttribute("tpIdCliente", "07");
		}
		setAttribute("idCliente", det.getPersona().getCedulaRuc());
		setAttribute("tipoComprobante", "18");
		setAttribute("numeroComprobantes", det.getNumeroComprobantes()); 
		setAttribute("baseNoGraIva", det.getBaseNoGraIva());
		setAttribute("baseImponible", det.getBaseImponible());
		setAttribute("baseImpGrav", det.getBaseImpGrav());
		setAttribute("montoIva", det.getMontoIva());
		setAttribute("valorRetIva", det.getValorRetIva());
		setAttribute("valorRetRenta", det.getValorRetRenta()); 
		
	}

}
