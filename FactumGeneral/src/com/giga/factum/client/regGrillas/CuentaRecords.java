package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.CuentaDTO;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CuentaRecords extends ListGridRecord{
	public CuentaRecords() {
    }
	public CuentaRecords(CuentaDTO cue) {
       try {
    	   //SC.say(String.valueOf(cue.getIdCuenta()));
    	   setidCuenta(cue.getIdCuenta());
           setCuenta(cue.getNombreCuenta());
           setidTipocuenta(String.valueOf(cue.getTbltipocuenta().getIdTipoCuenta()));
           setPadre(String.valueOf(cue.getPadre()));
           setNivel(String.valueOf(cue.getNivel()));
           setCodigo(cue.getcodigo());
       }catch (Exception e){
    	   
    	   SC.say(e.getLocalizedMessage());
       }
		
        
	}
    public CuentaRecords(int idCuenta,String nombreCuenta,String idTipocuenta,String Padre,String nivel, String codigo) {
    	setidCuenta(idCuenta);
        setCuenta(nombreCuenta);
        setidTipocuenta(idTipocuenta);
        setPadre(Padre);
        setNivel(nivel);
        setCodigo(codigo);
    }
    private void setCodigo(String codigo) {
    	setAttribute("codigo", codigo);
		
	}
	private void setNivel(String nivel) {
    	setAttribute("nivel", nivel);
	}
	private void setPadre(String padre) {
    	setAttribute("Padre", padre);
	}
	private void setidTipocuenta(String idTipocuenta) {
    	setAttribute("idTipocuenta", idTipocuenta);
	}
	private void setCuenta(String nombreCuenta) {
    	setAttribute("nombreCuenta", nombreCuenta);
	}
	private void setidCuenta(int idCuenta) {
		setAttribute("idCuenta", idCuenta);
	}
	public int getidCuenta() {
        return getAttributeAsInt("idCuenta");
    }
	public String getnombreCuenta() {
        return getAttributeAsString("nombreCuenta");
    }
	public String getidTipocuenta() {
        return getAttributeAsString("idTipocuenta");
    }
	public String getidPadre() {
        return getAttributeAsString("Padre");
    }
	public String getnivel() {
        return getAttributeAsString("nivel");
    }
	public String getcodigo() {
        return getAttributeAsString("codigo");
    }
	
}
