package com.giga.factum.client.regGrillas;


import com.giga.factum.client.DTO.MayorizacionDTO;

import com.smartgwt.client.widgets.grid.ListGridRecord;

public class MayorizacionRecords extends ListGridRecord{
	public MayorizacionRecords()
	{
		
	}
	public MayorizacionRecords(MayorizacionDTO maydto)
	{
		setfecha(maydto.getFecha().toString());
		setcodigo(maydto.getCodigo());
		setidTipoCuenta(maydto.getOidTipoCuenta());
		setidDocumento(maydto.getnumDocumento());
		setnumrealtran(maydto.getnumeroTransaccion());
		setcuenta(maydto.getCuenta());
		setidDocumento(maydto.getnumeroDocumento());
		setnumrealtran(maydto.getnumeroTransaccion());
		setdescripcion(maydto.getDescripcion());
		setsigno(maydto.getSigno(),maydto.getValor());
		setvalor(maydto.getValor());
		setnivel(maydto.getNivel());
		setpadre(maydto.getPadre());
		setnumDocumento(maydto.getnumDocumento());
		setperiodoAnterior(maydto.getPeriodoAnterior());
		
	}
	
	public void setfecha(String fecha)
	{
		String[] fecha1;
		
		fecha1=fecha.split("01:00:00.000000000");
		fecha1=fecha.split("00:00:00.000000000");
		setAttribute("fecha",fecha1);
	}
	public void setcodigo(String codigo)
	{

		setAttribute("codigo",codigo);
	}
	public void setidTipoCuenta(int idTipoCuenta)
	{

		setAttribute("idTipoCuenta",idTipoCuenta);
	}
	public void setcuenta(String cuenta)
	{

		setAttribute("cuenta",cuenta);
	}
	public String getcuenta() {
    	return getAttribute("cuenta");
	}
	public void setnumrealtran(int numeroreal)
	{

		setAttribute("numrealtran",numeroreal);
	}
	public int getnumrealtran() {
    	return getAttributeAsInt("numrealtran");
	}
	public void setidDocumento(int id)
	{

		setAttribute("documento",id);
	}
	public int getidDocumento() {
    	return getAttributeAsInt("documento");
	}
	public void setdescripcion(String descripcion)
	{

		setAttribute("descripcion",descripcion);
	}
	public void setsigno(char signo,double valor)
	{
		String sl = String.valueOf(signo);
		double aux=0.00;
		if(sl.equalsIgnoreCase("d"))
		{
			
			setAttribute("debe",valor);
			setAttribute("haber",aux);
		}
		else
		{
			setAttribute("haber",valor);
			setAttribute("debe",aux);
		}
		setAttribute("signo",sl);
	}
	public void setvalor(double valor)
	{

		setAttribute("valor",valor);
	}
	public void setnivel(int nivel)
	{

		setAttribute("nivel",nivel);
	}
	public void setpadre(int padre)
	{

		setAttribute("padre",padre);
	}
	public void setnumDocumento(int numero)
	{

		setAttribute("documento",numero);
	}
	public void setperiodoAnterior(double periodoAnterior)
	{

		setAttribute("periodoAnterior",periodoAnterior);
	}

}
