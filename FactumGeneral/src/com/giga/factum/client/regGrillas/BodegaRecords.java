package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.BodegaProdDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class BodegaRecords extends ListGridRecord {
	DataSourceBooleanField bool = new DataSourceBooleanField("Ban", "Activo");  
    
	public BodegaRecords() {
    }
	public BodegaRecords(BodegaDTO bod) {
		setidEmpresa(bod.getIdEmpresa());
		setEstablecimiento(bod.getEstablecimiento());
        setidBodega(bod.getIdBodega());
        setBodega(bod.getBodega());
        setUbicacion(bod.getUbicacion());
        setTelefono(bod.getTelefono());
        setBan(false);
        setCantidad("");
        String cantidad= (bod.getCantidad()==null)?"":String.valueOf(bod.getCantidad());
        //setStock(String.valueOf(bod.getCantidad()));
        setStock(cantidad);
	}
	
	
	
	public BodegaRecords(BodegaProdDTO bod) {
//		setidEmpresa(bod.getIdEmpresa());
//		setEstablecimiento(bod.getEstablecimiento());
        setidBodega(bod.getIdBodega());
        setBodega(bod.getBodega());
        setUbicacion(bod.getUbicacion());
        setTelefono("");
        setBan(false);
        setCantidad(bod.getCantidad()+"");
        
	}
	
    public BodegaRecords(int id,String Bodega,String ubica,String telef) {
        setidBodega(id);
        setBodega(Bodega);
        setUbicacion(ubica);
        setTelefono(telef);
        setBan(false);
    }
    
    
    private void setCantidad(String can) {
    	setAttribute("Cantidad",can);
	}
    private void setStock(String stock) {
    	setAttribute("Stock",stock);
	}
    public String getCantidad() {
    	return getAttribute("Cantidad");
	}
    private void setBan(Boolean Bodega) {
		bool.setAttribute("Ban",Bodega);
    }
    public Boolean getBan() {
		return bool.getAttributeAsBoolean("Ban");
	}
	private void setBodega(String Bodega) {
		setAttribute("Bodega", Bodega);
	}
	private void setUbicacion(String ubicacion) {
		setAttribute("Ubicacion", ubicacion);
	}
	private void setTelefono(String telefono) {
		setAttribute("Telefono", telefono);
	}
	private void setEstablecimiento(String establecimiento) {
		setAttribute("Establecimiento", establecimiento);
	}
	private void setidBodega(int idBodega) {
		setAttribute("idBodega", idBodega);
	}
	private void setidEmpresa(Integer idEmpresa) {
		setAttribute("idEmpresa", idEmpresa);
	}
	public String getBodega() {
        return getAttributeAsString("Bodega");
    }
	
	public String getUbicacion(){
		return getAttributeAsString("Ubicacion");
	}
	public String getTelefono(){
		return getAttributeAsString("Telefono");
	}
	public String getEstablecimiento(){
		return getAttributeAsString("Establecimiento");
	}
	public int getidBodega(){
		return getAttributeAsInt("idBodega");
	}
	private Integer getidEmpresa() {
		return getAttributeAsInt("idEmpresa");
	}
	public ProductoBodegaDTO getProductoBodegaDTO(){
		BodegaDTO bodega=new BodegaDTO();
		ProductoBodegaDTO proBodega=new ProductoBodegaDTO();
		bodega.setIdEmpresa(this.getidEmpresa());
		bodega.setEstablecimiento(this.getEstablecimiento());
		bodega.setBodega(getBodega());
		bodega.setIdBodega(this.getidBodega());
		bodega.setTelefono(this.getTelefono());
		bodega.setUbicacion(this.getUbicacion());
		proBodega.setCantidad(Integer.parseInt(this.getCantidad()));
		proBodega.setTblbodega(bodega);
		return proBodega;
	}
}
