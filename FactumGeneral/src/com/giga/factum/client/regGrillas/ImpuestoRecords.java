package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.ImpuestoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ImpuestoRecords extends ListGridRecord{
	public ImpuestoRecords(){
		
	}
	public ImpuestoRecords(ImpuestoDTO imp){
		setidImpuesto(imp.getIdImpuesto());
		setCodigo(imp.getCodigo());
		setidCuenta(imp.getIdCuenta());
		setidPlan(imp.getIdPlan());
		setNombre(imp.getNombre());
		setRetencion(imp.getRetencion());
		setTipo(imp.getTipo());
		setCodigoElectronico(imp.getCodigoElectronico());
	}
	private String getNombre() {
		return getAttributeAsString("lstNombre");
		
	}
	private int getidPlan() {
		return getAttributeAsInt("lstidPlan");
	}
	private int getidCuenta() {
		return getAttributeAsInt("lstCuenta");
	}
	private String getCodigo() {
		return getAttributeAsString("lstCodigo");
		
	}
	private String getCodigoElectronico() {
		return getAttributeAsString("lstCodigoElectronico");
		
	}
	private int getidImpuesto() {
		return getAttributeAsInt("lstidImpuesto");
		
	}
	
	public Float getRetencion() {
        return getAttributeAsFloat("lstPorcentage");
    }
	
	private void setRetencion(double retencion) {
		setAttribute("lstPorcentage", retencion);
		
	}
	private void setNombre(String nombre) {
		setAttribute("lstNombre", nombre);
		
	}
	private void setidPlan(int idPlan) {
		setAttribute("lstidPlan", idPlan);
	}
	private void setidCuenta(int idCuenta) {
		setAttribute("lstCuenta", idCuenta);
		
	}
	private void setCodigo(int codigo) {
			setAttribute("lstCodigo", codigo);		
	}
	private void setCodigoElectronico(String codigo) {
		setAttribute("lstCodigoElectronico", codigo);		
}
	private void setidImpuesto(Integer idImpuesto) {
		setAttribute("lstidImpuesto", idImpuesto);
		
	}
	
	private void setTipo(Integer tipo) {
		if(tipo==0)
		{	
			setAttribute("lstTipo", "IVA");
		}
		if(tipo==1)
		{	
			setAttribute("lstTipo", "RENTA");
		}
		
	}

	
}
