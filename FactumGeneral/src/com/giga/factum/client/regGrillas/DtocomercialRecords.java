package com.giga.factum.client.regGrillas;

import java.util.Date;
import java.util.Iterator;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.Factum;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class DtocomercialRecords extends ListGridRecord{
	
	public DtocomercialRecords() {
    }
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public DtocomercialRecords(DtocomercialDTO doc) {
       try {
    	   setidDoc(doc.getIdDtoComercial());
           setNumReal(doc.getNumRealTransaccion());
           setTipo(doc.getTipoTransaccion());
           //setFecha(String.valueOf(doc.getFecha()));
           //setExpiracion(String.valueOf(doc.getExpiracion()));
           String fechaAux=doc.getFecha();
           fechaAux=fechaAux.substring(0, 10);
           setFecha(fechaAux);
           String fechaAuxExp=doc.getExpiracion();
           fechaAuxExp=fechaAuxExp.substring(0, 10);
           setExpiracion(fechaAuxExp);
           setRuc(doc.getTblpersonaByIdPersona().getCedulaRuc());
           setAutorizacion(doc.getAutorizacion());
           setNumPagos(doc.getNumPagos());
           setEstado(doc.getEstado());
             
           //setSubtotal(doc.);
           setVendedor(String.valueOf(doc.getTblpersonaByIdVendedor().getRazonSocial())
        		   +" "+String.valueOf(doc.getTblpersonaByIdVendedor().getNombreComercial()));
           setIdCliente(Integer.valueOf(doc.getTblpersonaByIdPersona().getIdPersona()));
           setIdVendedor(Integer.valueOf(doc.getTblpersonaByIdVendedor().getIdPersona()));
           //setCliente(String.valueOf(doc.getTblpersonaByIdPersona().getRazonSocial()+" "+doc.getTblpersonaByIdPersona().getNombreComercial()));
			setCliente(doc.getTblpersonaByIdPersona().getRazonSocial());
			setNombreComercial(doc.getTblpersonaByIdPersona().getNombreComercial());
           Iterator resultado = doc.getTbldtocomercialdetalles().iterator();
           double base=0.0;
           double descuento=0.0;
           double subiva=0.0;
           double subiva0=0.0;
           double subtotal=0;
           double total=0.0;
           double iva=0.0;
           
           double subImp=0.0;
           while (resultado.hasNext()) {
        	   DtoComDetalleDTO det=(DtoComDetalleDTO) resultado.next();
        	   subtotal=subtotal+((det.getTotal()*100)/(100-det.getDepartamento()));
        	   //subtotal=subtotal+det.getTotal()*(1+det.getDepartamento()/100);
        	   descuento=descuento+(det.getDepartamento()*det.getTotal())/(100-det.getDepartamento());
        	   
        	   String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
//				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+det.getTblimpuestos().size());
				for (DtoComDetalleMultiImpuestosDTO detalleImp: det.getTblimpuestos()){
					i1=i1+1;
					impuestos+=detalleImp.getPorcentaje().toString();
					impuestos= (i1<det.getTblimpuestos().size())?impuestos+",":impuestos; 
					impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
					impuestoValor=impuestoValor+((det.getCantidad()*det.getPrecioUnitario()*(1-(det.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
				}
//				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
        	   
        	   if(doc.getTipoTransaccion()==2){
        		   subiva0=subiva0+det.getTotal();
        	   }else{
//        		   if(det.getImpuesto()==Factum.banderaIVA){
        		   if(impuestoPorc-1>0){
	        		   subiva=subiva+det.getTotal();
	        		   subImp+=impuestoValor;
	        	   }else if(impuestoPorc-1==0){
	        		   subiva0=subiva0+det.getTotal();
	        	   }        		   
        	   }
           }
           base=subtotal-descuento;
           //SC.say("TIPOTRANSACCION: "+doc.getTipoTransaccion());
           if(doc.getTipoTransaccion()==2 
//        		   || doc.getTipoTransaccion()==3
        		   )
           {
        	   iva=0;
           }else{
//        	   iva=subiva*(Factum.banderaIVA/100);
        	   iva=subImp;
           }
           /*
           if(doc.getTipoTransaccion()==2||doc.getTipoTransaccion()==7||doc.getTipoTransaccion()==3||doc.getTipoTransaccion()==10||doc.getTipoTransaccion()==14
        		   ||doc.getTipoTransaccion()==4||doc.getTipoTransaccion()==15||doc.getTipoTransaccion()==12)
           {
        	   setSubtotal(doc.getSubtotal());
           }
           else
           {
           */
           if(doc.getTipoTransaccion()==5 ||doc.getTipoTransaccion()==4||doc.getTipoTransaccion()==15||doc.getTipoTransaccion()==12)
           {
        	   setSubtotal(formatoDecimalN.format(doc.getSubtotal()));
        	   setTotal(formatoDecimalN.format(doc.getSubtotal()));
//        	   setSubtotal(doc.getSubtotal());
//        	   setTotal(doc.getSubtotal());
           }
           else
           {
        	   /*double totalLista =subiva+subiva0+descuento;//(doc.getSubtotal()-descuento - doc.getDescuento())*1.12;
        	   totalLista=CValidarDato.getDecimal(2, totalLista);
        	   setSubtotal(totalLista);*/
        	   setSubtotal(formatoDecimalN.format(subtotal));
//        	   setSubtotal(subtotal);
        	   total=iva+subiva+subiva0-doc.getDescuento();
//        	   setTotal(Math.rint(total*100)/100);
        	   setTotal(formatoDecimalN.format(total));
//        	   setTotal(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,total));
       	   } 
       	   
           
           
           subiva = subiva -doc.getDescuento();
           Double descuentoGlobal = descuento + doc.getDescuento();
//           setDescuento(Math.rint(descuentoGlobal*100)/100);
//           setBase( Math.rint(base*100)/100);
//           setIva0(Math.rint(subiva0*100)/100);
//           setIva14(Math.rint(subiva*100)/100);
//           setIva(Math.rint(iva*100)/100);
           setDescuento(formatoDecimalN.format(descuentoGlobal));
           setBase(formatoDecimalN.format(base));
           setIva0(formatoDecimalN.format(subiva0));
           setIva14(formatoDecimalN.format(subiva));
           setIva(formatoDecimalN.format(iva));
//           setDescuento(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,descuentoGlobal));
//           setBase(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,base));
//           setIva0(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,subiva0));
//           setIva14(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,subiva));
//           setIva(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,iva));
           
           setTipoDocumento(doc.getTipoTransaccion());
           
         //  setRuc(doc.getTblpersonaByIdPersona().getCedulaRuc());
           setDireccion(doc.getTblpersonaByIdPersona().getDireccion());
           setIdPersona(doc.getTblpersonaByIdPersona().getIdPersona());
           setRetencion(formatoDecimalN.format(doc.getTotalRetencion()));
           setRetencionIVA(formatoDecimalN.format(doc.getTotalRetencionIVA()));
           setRetencionRENTA(formatoDecimalN.format(doc.getTotalRetencionRENTA()));
//           setRetencion(doc.getTotalRetencion());
//           setRetencionIVA(doc.getTotalRetencionIVA());
//           setRetencionRENTA(doc.getTotalRetencionRENTA());
           if(!doc.getNumCompra().equals(null) || doc.getNumCompra()!=null){
        	   setNumCompra(doc.getNumCompra());
           }
           if(!doc.getDtoelectronico().equals(null) || doc.getDtoelectronico()!=null){
        	   setTipoEmision("ELECTRONICO");
        	   setEstadoElectronico(doc.getDtoelectronico().getEstado());
        	   setNombreRIDE(doc.getDtoelectronico().getNombreRide());
           }else{
        	   setTipoEmision("FISICO");
           }
           
           
       }catch (Exception e){ 
    	   SC.say("error al listar datos");
       }

	}
	private void setNumCompra(String numCompra) {
		// TODO Auto-generated method stub
		setAttribute("numCompra",numCompra);
	}
	
	private void setRetencionRENTA(String totalRetencionRENTA) {
		setAttribute("retencionRENTA",totalRetencionRENTA);
		
	}
	private void setRetencionIVA(String totalRetencion) {
		setAttribute("retencionIVA",totalRetencion);
		
	}
	private void setRetencion(String totalRetencion) {
		setAttribute("retencion",totalRetencion);
		
	}
//	private void setRetencionRENTA(double totalRetencionRENTA) {
//		setAttribute("retencionRENTA",totalRetencionRENTA);
//		
//	}
//	private void setRetencionIVA(double totalRetencion) {
//		setAttribute("retencionIVA",totalRetencion);
//		
//	}
//	private void setRetencion(double totalRetencion) {
//		setAttribute("retencion",totalRetencion);
//		
//	}
	private void setIdPersona(Integer idPersona) {
		setAttribute("idPersona",idPersona);
		
	}
	private void setDireccion(String direccion) {
		setAttribute("direccion",direccion);
		
	}
	private void setRuc(String cedulaRuc) {
		setAttribute("RUC",cedulaRuc);
		
	}
	private void setTipoDocumento(int tipoTransaccion) {
		switch (tipoTransaccion) {
			case 0:{
				setAttribute("TipoTransaccion","Facturas de Venta");
				break;
			}
			case 1:{
				setAttribute("TipoTransaccion","Facturas de Compra");
				break;
			}
			case 2:{
				setAttribute("TipoTransaccion","Notas de Entrega");
				break;
			}
			case 3:{
				setAttribute("TipoTransaccion","Notas de Credito");
				break;
			}
			case 4:{
				setAttribute("TipoTransaccion","Anticipo Clientes");
				break;
			}
			case 5:{
				setAttribute("TipoTransaccion","Retencion");
				break;
			}
			case 7:{
				setAttribute("TipoTransaccion","Gastos");
				break;
			}			
			case 10:{
				setAttribute("TipoTransaccion","Notas de Compra");
				break;
			}
			case 15:{
				setAttribute("TipoTransaccion","Anticipo a Proveedores");
				break;
			}
			case 16:{
				setAttribute("TipoTransaccion","Ajustes de Inventario");
				break;
			}
			case 17:{
				setAttribute("TipoTransaccion","Ajustes Inicial");
				break;
			}
			case 18:{
				setAttribute("TipoTransaccion","Consignacion");
				break;
			}
			case 20:{
				setAttribute("TipoTransaccion","Proforma");
				break;
			}
		}
	}
	private void setIva14(String iva) {
		setAttribute("Iva14",iva);
		
	}
	private void setTotal(String d) {
		setAttribute("Total",d);
		
	}
	private void setBase(String base) {
		setAttribute("Base",base);
		
	}
	private void setDescuento(String descuento) {
		setAttribute("Descuento",descuento);
		
		
	}
	private void setIva(String iva) {
		setAttribute("Iva",iva);
		
	}
	private void setIva0(String iva0) {
		setAttribute("Iva0",iva0);
		
	}
//	private void setIva14(double iva) {
//		setAttribute("Iva14",iva);
//		
//	}
//	private void setTotal(double d) {
//		setAttribute("Total",d);
//		
//	}
//	private void setBase(double base) {
//		setAttribute("Base",base);
//		
//	}
//	private void setDescuento(double descuento) {
//		setAttribute("Descuento",descuento);
//		
//		
//	}
//	private void setIva(double iva) {
//		setAttribute("Iva",iva);
//		
//	}
//	private void setIva0(double iva0) {
//		setAttribute("Iva0",iva0);
//		
//	}
	private String getNumCompra(String numCompra) {
		return getAttributeAsString("numCompra");
	}
	private Double getSubtotal() {
		return getAttributeAsDouble("SubTotal");
		
	}
	private void setSubtotal(String d) {
		setAttribute("SubTotal",d);
		
	}
//	private Double getSubtotal() {
//		return getAttributeAsDouble("SubTotal");
//		
//	}
//	private void setSubtotal(double d) {
//		setAttribute("SubTotal",d);
//		
//	}
	private String getEstado() {
		return getAttributeAsString("Estado");
		
	}
	private void setEstado(char estado) {
		
		setAttribute("Estado",estado);
		if(getAttributeAsString("Estado").equals("49")){
			setAttribute("Estado","CONFIRMADO");
		}else if(getAttributeAsString("Estado").equals("48")){
			setAttribute("Estado","NO CONFIRMADO");
		}else if(getAttributeAsString("Estado").equals("50")){
			setAttribute("Estado","ANULADA");
		}else if(getAttributeAsString("Estado").equals("53")){
			setAttribute("Estado","UTILIZADO");
		}
		else if(getAttributeAsString("Estado").equals("51")){
			   setAttribute("Estado","TRANSFORMADO");
			}				
	}
	private int getNumPagos() {
		return getAttributeAsInt("NumPagos");
		
	}
	
	private void setNumPagos(int numPagos) {
		setAttribute("NumPagos",numPagos);
		
	}
	private String getAutorizacion() {
		return getAttribute("Autorizacion");
		
	}
	
	private void setAutorizacion(String autorizacion) {
		setAttribute("Autorizacion",autorizacion);
		
	}
	private String getCliente() {
		return getAttribute("Cliente");
		
	} 
	private void setCliente(String string) {
		setAttribute("Cliente",string);
		
	}
	private void setNombreComercial(String string) {
		setAttribute("NombreComercial",string);
		
	}
	private String getVendedor() {
		return getAttribute("Vendedor");
		
	}
	private void setVendedor(String string) {
		setAttribute("Vendedor",string);
		
	}
	private Date getExpiracion() {
		return getAttributeAsDate("FechadeExpiracion");
		
	}
	
	private void setExpiracion(String string) {
		setAttribute("FechadeExpiracion",string);
		
	}
	private Date getFecha() {
		return getAttributeAsDate("Fecha");
		
	}
	private void setFecha(String string) {
		setAttribute("Fecha",string);
		
	}
	private String getTipo() {
		return getAttribute("TipoTransaccion");
	}
	private void setTipo(int tipoTransaccion) {
		if(tipoTransaccion==1){
			setAttribute("TipoTransaccion","Venta con Factura");
		}else if(tipoTransaccion==2){
			setAttribute("TipoTransaccion","Factura de Compra");
		}else if(tipoTransaccion==3){
			setAttribute("TipoTransaccion","Nota de Credito");
		}
	}
	private int getNumReal() {
		return getAttributeAsInt("NumRealTransaccion");
		
	}
	private void setNumReal(int numRealTransaccion) {
		setAttribute("NumRealTransaccion", numRealTransaccion);
		
	}
	private int getidDoc() {
		return getAttributeAsInt("id");
	}
	
	private void setidDoc(Integer idDtoComercial) {
		setAttribute("id", idDtoComercial);
	}
	
	private int getIdCliente() {
		return getAttributeAsInt("idCliente");
	}
	private int getIdVendedor() {
		return getAttributeAsInt("idVendedor");
	}
	private void setIdCliente(Integer idCliente) {
		setAttribute("idCliente", idCliente);
	}
	private void setIdVendedor(Integer idVendedor) {
		setAttribute("idVendedor", idVendedor);
	}
	private String getEstadoElectronico() {
		return getAttributeAsString("EstadoElectronico");
		
	}
//	private void setEstadoElectronico(char estadoelectronico) {
//		String estadoElectronico=String.valueOf(estadoelectronico);	
//		if(estadoElectronico.equals("0")){
//			setAttribute("EstadoElectronico","GENERADO");
//		}else if(estadoElectronico.equals("1")){
//			setAttribute("EstadoElectronico","FIRMADO");
//		}else if(estadoElectronico.equals("2")){
//			setAttribute("EstadoElectronico","REGISTRADO");
//		}else if(estadoElectronico.equals("3")){
//			setAttribute("EstadoElectronico","AUTORIZADO");
//		}else if(estadoElectronico.equals("4")){
//			   setAttribute("EstadoElectronico","RIDE");
//		}else if(estadoElectronico.equals("5")){
//			   setAttribute("EstadoElectronico","ENVIADO");
//		}
//	}
	private void setEstadoElectronico(char estadoelectronico) {
		String estadoElectronico=String.valueOf(estadoelectronico);	
		if(estadoElectronico.equals("0")){
			setAttribute("EstadoElectronico","GENERADO");
		}else if(estadoElectronico.equals("1")){
			setAttribute("EstadoElectronico","FIRMADO");
		}else if(estadoElectronico.equals("2")){
			setAttribute("EstadoElectronico","RIDE");
		}else if(estadoElectronico.equals("3")){
			setAttribute("EstadoElectronico","REGISTRADO");
		}else if(estadoElectronico.equals("4")){
			   setAttribute("EstadoElectronico","AUTORIZADO");
		}else if(estadoElectronico.equals("5")){
			   setAttribute("EstadoElectronico","FINALIZADO");
		}
	}
	private void setTipoEmision(String TipoEmision) {
		setAttribute("TipoEmision", TipoEmision);
	}
	private String getTipoEmision() {
		return getAttributeAsString("EstadoElectronico");
		
	}
	
	private void setEnviarElectronico(String EnviarElectronico) {
		setAttribute("buttonField", EnviarElectronico);
	}
	
	private void setNombreRIDE(String NombreRIDE) {
		setAttribute("NombreRIDE", NombreRIDE);
	}
	private String NombreRIDE() {
		return getAttributeAsString("NombreRIDE");
		
	}
	
}
