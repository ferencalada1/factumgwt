package com.giga.factum.client.regGrillas;


import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class MesaRecords extends ListGridRecord {
	public MesaRecords() {
    }
	public MesaRecords(MesaDTO mesadto) {
        setidMesa(mesadto.getIdMesa());
        setMesa(mesadto.getNombreMesa());
        setArea(mesadto.getTblarea().getNombre());
        setEstado(mesadto.getEstado());
        setidArea(mesadto.getTblarea().getIdArea().toString());
	}
    public MesaRecords(int id,String Mesa, String Area) {
        setidMesa(id);
        setMesa(Mesa);
        setArea(Area);
    }

	private void setMesa(String mesa) {
		// TODO Auto-generated method stub
		setAttribute("Mesa", mesa);
	}

	private void setidMesa(int idMesa) {
		// TODO Auto-generated method stub
		setAttribute("idMesa", idMesa);
	}
	private void setEstado(String estado) {
		// TODO Auto-generated method stub
		setAttribute("Estado", estado);
	}
	private void setidArea(String idArea) {
		// TODO Auto-generated method stub
		setAttribute("idArea", idArea);
	}
	private void setArea(String Area) {
		// TODO Auto-generated method stub
		setAttribute("Area", Area);
	}	
	public String getMesa() {
        return getAttributeAsString("Mesa");
    }
    public int getidMesa(){
    	return getAttributeAsInt("idMesa");
    }
    public String getArea(){
    	return getAttributeAsString("Area");
    }
}
