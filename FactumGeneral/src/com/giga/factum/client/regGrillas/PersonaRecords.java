package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.PersonaDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PersonaRecords extends  ListGridRecord{
	public PersonaRecords(){
		
	}
	public PersonaRecords(PersonaDTO per) {
		setidPersona(String.valueOf(per.getIdPersona()));
        setCedula(per.getCedulaRuc());
        setNombreComercial(per.getNombreComercial());
        setRazonSocial(per.getRazonSocial());
        setDireccion(per.getDireccion());
        setLocalizacion(per.getLocalizacion());
        setTelefono1(per.getTelefono1());
        setTelefono2(per.getTelefono2());
        setMail(per.getMail());
        setObservaciones(per.getObservaciones());
        setEstado(per.getEstado());
        if(per.getEmpleadoU()!=null){
        	int op=per.getEmpleadoU().getNivelAcceso();

        	String fechaAux=String.valueOf(per.getEmpleadoU().getFechaContratacion());
        	fechaAux=fechaAux.substring(0, 10);
        	//setFechaContratacion(String.valueOf((per.getEmpleadoU().getFechaContratacion())));
        	setFechaContratacion(fechaAux);
        	setSueldo(String.valueOf(per.getEmpleadoU().getSueldo()));
        	setHorasExtra(String.valueOf(per.getEmpleadoU().getHorasExtras()));
        	setIngresosExtra(String.valueOf(per.getEmpleadoU().getIngresosExtras()));
        	setIdEmpleado(String.valueOf(per.getEmpleadoU().getIdEmpleado()));
        	setIdCargo(String.valueOf(per.getEmpleadoU().getTblcargo().getIdCargo()));
        	setCargo(per.getEmpleadoU().getTblcargo().getDescripcion());
        	setPass(per.getEmpleadoU().getPassword());
        	setSalario(String.valueOf(per.getEmpleadoU().getSueldo()));
        	setNivelA(String.valueOf(op));
        	/*if(op==1){
        		setNivelA("Administrador");
	        }else if(op==2){
	        		setNivelA("Contabilidad");
	        }else if(op==3){
        		setNivelA("Ventas");
        	}else if(op==4){
        		setNivelA("Bodega");
        	}else if(op==5){
        		setNivelA("Caja");
        		
        	}*/
	    	setBackground("Nivel de Acceso: "+getNivelA()+"\n Fecha de Contrataci�n: "+getFechaContratacion()+
        			" \\n    Salario: "+getSalario()+" \u0001    Cargo: "+getCargo()+" \n    E-mail: "+getMail()+" \n    Observaciones: "+getObservaciones()+
        			" Tel�fono: ");
        }else if(per.getProveedorU()!=null){
        	setContacto(per.getProveedorU().getContacto());
        	setCupoCredito(String.valueOf(per.getProveedorU().getCupoCredito()));
        	setIdProveedor(String.valueOf(per.getProveedorU().getIdProveedor()));
        }else if(per.getClienteU()!=null){
        	setIdCliente(String.valueOf(per.getClienteU().getIdCliente()));
        	setMontoCredito(String.valueOf(per.getClienteU().getMontoCredito()));
        	setCupoCreditoCliente(String.valueOf(per.getClienteU().getCupoCredito()));
        	if(per.getClienteU().getCodigoTarjeta().equals(null) || per.getClienteU().getCodigoTarjeta()==null || per.getClienteU().getCodigoTarjeta().equals("")){
        		setCodigoTarjeta("0");	
        	}else{
        	setCodigoTarjeta(per.getClienteU().getCodigoTarjeta());
        	}
        	if(per.getClienteU().getPuntos().equals(null) || per.getClienteU().getPuntos()==null || per.getClienteU().getPuntos().equals("")){
        		setPuntos(0);	
        	}else{
        		setPuntos(per.getClienteU().getPuntos());
        	}
        	setContribuyenteEspecial(per.getClienteU().getContribuyenteEspecial());
        	setObligadoContabilidad(per.getClienteU().getObligadoContabilidad());
        	
        }
	}
	private void setPuntos(int puntos) {
		setAttribute("puntos",puntos);  
		
	}
	private void setCodigoTarjeta(String codigoTarjeta) {
		setAttribute("tarjeta",codigoTarjeta);  
		
	}
	private String getEstado() {
		return this.getAttributeAsString("estado");  
		
	}
	private void setEstado(char estado) {
		setAttribute("estado",estado);  
		
	}
	private String getObservaciones() {
		return this.getAttributeAsString("observaciones");  
		
	}
	private void setObservaciones(String observaciones) {
		setAttribute("observaciones",observaciones);  
		
	}
	private void setSalario(String salario) {
		setAttribute("salario",salario);  
	}
	private String getSalario() {
		return this.getAttributeAsString("salario");  
	}
	public void setBackground(String background) {  
        setAttribute("background", background);  
    }  
  
    public String getBackground() {  
        return getAttributeAsString("background");  
    }  
	private void setPass(String password) {
		setAttribute("password",password);
	}
	private void setNivelA(String nivel) {
		setAttribute("NivelA",nivel);
		
	}
	
	private void setCargo(String descripcion) {
		setAttribute("Cargo",descripcion);
	}
	private void setIdCargo(String valueOf) {
		setAttribute("idCargo",valueOf);
	}
	
	private void setIdCliente(String valueOf) {
		setAttribute("idCliente",valueOf);
		
	}
	private void setIdProveedor(String valueOf) {
		setAttribute("idProveedor",valueOf);
		
	}
	private void setIdEmpleado(String valueOf) {
		setAttribute("idEmpleado",valueOf);
		
	}
	private String getIdEmpleado() {
		return this.getAttributeAsString("idEmpleado");
	}
	private void setCupoCreditoCliente(String cupoC) {
		setAttribute("CupoCreditoCliente",cupoC);
	}
	private void setMontoCredito(String MontoC) {
		setAttribute("MontoCredito",MontoC);
	}
	private void setCupoCredito(String cupoC) {
    	setAttribute("CupoCredito",cupoC);
	}
	private void setContacto(String contacto) {
    	setAttribute("Contacto",contacto);
	}
	private void setIngresosExtra(String IngresosE) {
    	setAttribute("IngresosE",IngresosE);
	}
	private void setHorasExtra(String HorasE) {
    	setAttribute("HorasExtra",HorasE);
	}
	private void setSueldo(String sueldo) {
    	setAttribute("Sueldo",sueldo);
	}
	private void setFechaContratacion(String fechaC) {
		setAttribute("FechaContratacion",fechaC);
	}
	private void setMail(String mail2) {
    	setAttribute("E-mail",mail2);
	}
	private void setTelefono1(String telefono12) {
    	setAttribute("Telefonos",telefono12);
	}
	
	private void setTelefono2(String telefono2) {
    	setAttribute("Telefonos2",telefono2);
	}
	
	private void setDireccion(String direccion2) {
    	setAttribute("Direccion",direccion2);
	}
	private void setLocalizacion(String localizacion) {
    	setAttribute("Localizacion",localizacion);
	}
	private void setRazonSocial(String razonSocial) {
    	setAttribute("RazonSocial",razonSocial);
	}
	private void setNombreComercial(String nombres2) {
    	setAttribute("NombreComercial",nombres2);
	}
	private void setCedula(String cedulaRuc) {
    	setAttribute("Cedula",cedulaRuc);
	}
	private void setidPersona(String id) {
		setAttribute("idPersona",id);
	}
	private void setContribuyenteEspecial(String contribuyenteEspecial) {
    	setAttribute("contribuyenteEspecial",contribuyenteEspecial);
	}
	private void setObligadoContabilidad(char obligadoContabilidad) {
		boolean val=(obligadoContabilidad=='0')?false:true;
		setAttribute("obligadoContabilidad",val);
//		setAttribute("obligadoContabilidad",obligadoContabilidad);
	}	
	public String getDireccion(){
		 return getAttributeAsString("Direccion");
	}
	public String getLocalizacion(){
		 return getAttributeAsString("Localizacion");
	}
	public String getRazonSocial(){
		 return getAttributeAsString("RazonSocial");
	}
	public String getNombreComercial(){
		 return getAttributeAsString("NombreComercial");
	}
	public  String getidPersona() {
		return getAttributeAsString("idPersona");
    }
	public String getCedula(String cedula){
		return getAttributeAsString("Cedula");
	}
	public String getMail(){
		 return getAttributeAsString("E-mail");
	}
	public String getTelefono(){
		 return getAttributeAsString("Telefonos");
	}
	private String getIngresosExtra() {
		return getAttributeAsString("IngresosE");
	}
	private String getHorasExtra() {
		return getAttributeAsString("HorasExtra");
	}
	private String getSueldo() {
		return getAttributeAsString("Sueldo");
	}
	private String getFechaContratacion() {
		return getAttributeAsString("FechaContratacion");
	}
	private String getCupoCredito() {
    	return this.getAttributeAsString("CupoCredito");
	}
	private String getContacto() {
		return this.getAttributeAsString("Contacto");
	}
	private String getCupoCreditoCliente() {
		return this.getAttributeAsString("CupoCreditoCliente");
	}
	private String getMontoCredito() {
		return this.getAttributeAsString("MontoCredito");
	}
	private String getIdCliente() {
		return this.getAttributeAsString("idCliente");
	}
	private String getIdProveedor() {
		return this.getAttributeAsString("idProveedor");
	}
	private String getNivelA() {
		return this.getAttributeAsString("NivelA");
		
	}
	private String getCargo() {
		return this.getAttributeAsString("Cargo");
	}
	private String getIdCargo() {
		return this.getAttributeAsString("idCargo");
	}
	private String getPass() {
		return this.getAttribute("password");
	}
	private String getContribuyenteEspecial() {
		return this.getAttribute("contribuyenteEspecial");
	}	
	private String getObligadoContabilidad() {
		return this.getAttribute("obligadoContabilidad");
	}
}
