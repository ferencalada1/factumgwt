package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TipoPagoRecords extends ListGridRecord{
public TipoPagoRecords(){
		
	}
	public TipoPagoRecords(TipoPagoDTO imp){
		setidImpuesto(imp.getIdTipoPago());
		setidCuenta(imp.getIdCuenta());
		setidPlan(imp.getIdPlan());
		setNombre(imp.getTipoPago());
	}
	private String getNombre() {
		return getAttributeAsString("lstNombre");
		
	}
	private int getidPlan() {
		return getAttributeAsInt("lstidPlan");
	}
	private int getidCuenta() {
		return getAttributeAsInt("lstCuenta");
	}
	private int getidImpuesto() {
		return getAttributeAsInt("lstidTipoPago");
		
	}
	
	private void setNombre(String nombre) {
		setAttribute("lstNombre", nombre);
		
	}
	private void setidPlan(int idPlan) {
		setAttribute("lstidPlan", idPlan);
	}
	private void setidCuenta(int idCuenta) {
		setAttribute("lstCuenta", idCuenta);
		
	}
	private void setidImpuesto(Integer idImpuesto) {
		setAttribute("lstidImpuesto", idImpuesto);
		
	}

}
