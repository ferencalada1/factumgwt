package com.giga.factum.client.regGrillas;

import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FacturaRecord extends ListGridRecord{
	
	public FacturaRecord(){
		
	}
	    public FacturaRecord(
	            String IdProducto,
	            String CodigoBarras,
	            String Cantidad,
	            String Descripcion,
	            String PrecioUnitario,
	            String ValorTotal,
	            String IVA,
	            String Stock,
	            String Descuento) {
	        setIdProducto(IdProducto);
	        setCodigoBarras(CodigoBarras);
	        setCantidad(Cantidad);
	        setDescripcion(Descripcion);
	        setPrecioUnitario(PrecioUnitario);
	        setValorTotal(ValorTotal);
	        setIVA(IVA);
	        setStock(Stock);
	        setDescuento(Descuento);
	    }


	    public void setIdProducto(String idProducto) {
	        setAttribute("idProducto", idProducto);
	    }
	    public void setCodigoBarras(String codigoBarras) {
	        setAttribute("codigoBarras", codigoBarras);
	    }
	    public void setCantidad(String cantidad) {
	        setAttribute("cantidad", cantidad);
	    }
	    public void setDescripcion(String descripcion) {
	        setAttribute("descripcion", descripcion);
	    }
	    public void setPrecioUnitario(String pUnitario) {
	        setAttribute("precioUnitario", pUnitario);
	    }
	    public void setValorTotal(String vtotal) {
	        setAttribute("valorTotal", vtotal);
	    }
	    public void setIVA(String iva) {
	        setAttribute("iva", iva);
	    }
	    public void setStock(String stock) {
	        setAttribute("stock", stock);
	    }
	    public void setDescuento(String descuento) {
	        setAttribute("descuento", descuento);
	    }
	    
	    public String getIdProducto() {
	        return getAttributeAsString("idProducto");
	    }
	    public String getCodigoBarras() {
	        return getAttributeAsString("codigoBarras");
	    }
	    public String getCantidad() {
	        return getAttributeAsString("cantidad");
	    }
	    public String getDescripcion() {
	    	return getAttributeAsString("descripcion");
	    }
	    public String getPrecioUnitario() {
	    	return getAttributeAsString("precioUnitario");
	    }
	    public String getValorTotal() {
	    	return getAttributeAsString("valorTotal");
	    }
	    public String getIVA() {
	    	return getAttributeAsString("iva");
	    }
	    public String getStock() {
	    	return getAttributeAsString("stock");
	    }
	    public String getDescuento() {
	    	return getAttributeAsString("descuento");
	    }
	    
}
