package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.CoreDTO;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CoreRecords extends ListGridRecord{
	public CoreRecords() {
    }
	public CoreRecords(CoreDTO Coredto) {
       try {
    	   //SC.say(String.valueOf(cue.getIdCore()));
    	   setIdCore(Coredto.getIdCore());
           setNombreCore(Coredto.getNombreCore());
           setTipoCore(Coredto.getTipoCore());
           setPadreCore(String.valueOf(Coredto.getPadreCore()));
           setNivelCore(String.valueOf(Coredto.getNivelCore()));
           setCodigoCore(Coredto.getCodigoCore());
       }catch (Exception e){
    	   
    	   SC.say(e.getLocalizedMessage());
       }
		
        
	}
    public CoreRecords(int idCore,String nombreCore, int TipoCore,String Padre,String nivel, String codigo, String precio) {
    	setIdCore(idCore);
        setNombreCore(nombreCore);
        setTipoCore(TipoCore);
        setPadreCore(Padre);
        setNivelCore(nivel);
        setCodigoCore(codigo);
    }
    private void setCodigoCore(String codigo) {
    	setAttribute("codigoCore", codigo);
		
	}
	private void setNivelCore(String nivel) {
    	setAttribute("nivelCore", nivel);
	}
	private void setPadreCore(String padre) {
    	setAttribute("padreCore", padre);
	}
	private void setTipoCore(int TipoCore) {
    	setAttribute("tipoCore", TipoCore);
	}
	private void setNombreCore(String nombreCore) {
    	setAttribute("nombreCore", nombreCore);
	}
	private void setIdCore(int idCore) {
		setAttribute("idCore", idCore);
	}
	
	public int getIdCore() {
        return getAttributeAsInt("idCore");
    }
	public String getNombreCore() {
        return getAttributeAsString("nombreCore");
    }
	public int getTipoCore() {
        return getAttributeAsInt("tipoCore");
    }
	public String getPadreCore() {
        return getAttributeAsString("padreCore");
    }
	public String getNivelCore() {
        return getAttributeAsString("nivelCore");
    }
	public String getCodigoCore() {
        return getAttributeAsString("codigoCore");
    }

	
}
