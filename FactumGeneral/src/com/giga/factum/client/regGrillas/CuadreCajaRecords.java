package com.giga.factum.client.regGrillas;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.Factum;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CuadreCajaRecords extends ListGridRecord{
	NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));;
	public CuadreCajaRecords(DtocomercialDTO doc){
		String tipoTransaccion="";
		LinkedList<DtocomercialTbltipopagoDTO> list=new  LinkedList<DtocomercialTbltipopagoDTO>();
 		Iterator it = doc.getTbldtocomercialTbltipopagos().iterator();
		while(it.hasNext()){
			DtocomercialTbltipopagoDTO obj=new DtocomercialTbltipopagoDTO();
			obj = (DtocomercialTbltipopagoDTO) it.next();
			//if(obj.getTipo()==0){
				list.add(obj);
			//}
		}
		for(int i=0; i<list.size();i++){
			DtocomercialTbltipopagoDTO obj=new DtocomercialTbltipopagoDTO();
			obj=list.get(i);
			//NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
				switch (doc.getTipoTransaccion()){
					case 0:
					{
						tipoTransaccion="Factura de Venta";


						break; 
					}
					case 1:
					{
						tipoTransaccion=("Factura de Compra");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break; 
					}
					case 2:
					{
						tipoTransaccion=("Nota de Entrega");
						break;
					}
					case 3:{
						tipoTransaccion=("Nota de Credito");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}
					case 4:{
						tipoTransaccion=("Anticipo de Clientes");
						break;
					}
					case 5:{
						tipoTransaccion=("Retencion");
						break;
					}case 6:{
						if(obj.getTipo()=='1')
						{
							tipoTransaccion=("Pago Realizado");
							double sub=doc.getSubtotal()*-1;
							doc.setSubtotal(sub);
							obj.setCantidad(obj.getCantidad()*-1);
							break;
						}
						else
						{
							tipoTransaccion=("Pago Recibido");
							break;
						}	
						
					}	
					case 7:{
						tipoTransaccion=("Gastos");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}
					case 8:{
						tipoTransaccion=("Ajuste de Inventario");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}
					case 10:
					{
						tipoTransaccion=("Nota de Compra");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}	
					case 11:{
						tipoTransaccion=("Pago Realizado");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}case 12:{
						tipoTransaccion=("Otros Ingresos");
						break;
					}
					case 15:{
						tipoTransaccion=("Anticipo Proveedor");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break;
					}
					case 26:{
						tipoTransaccion=("Nota de Venta");
						break; 
					}
					case 27:{
						tipoTransaccion=("Nota de Venta en Compras");
						double sub=doc.getSubtotal()*-1;
						doc.setSubtotal(sub);
						obj.setCantidad(obj.getCantidad()*-1);
						break; 
					}
					
				}
				if(obj.getTipoPago()!=1 && (doc.getTipoTransaccion()==1 || doc.getTipoTransaccion()==11)){
					setTipoTransaccion("");
				}else{
					setTipoTransaccion(tipoTransaccion);
					setObservaciones(obj.getObservaciones());
					//setCliente(doc.getTblpersonaByIdPersona().getNombreComercial()+" "+doc.getTblpersonaByIdPersona().getRazonSocial());
					setCliente(doc.getTblpersonaByIdPersona().getRazonSocial());
					setNombreComercial(doc.getTblpersonaByIdPersona().getNombreComercial());
					setNumRealTransaccion(doc.getNumRealTransaccion());
					setTipo(obj.getTipo());
					
					switch (obj.getTipoPago()){
						case 1:{
							setEfectivo(obj.getCantidad());
							break;
						}
						case 2:{
							setCredito(obj.getCantidad());
							break;
						}
						case 3:{
							setRetencion(obj.getCantidad());
							break;
						}
						case 4:{
							setAnticipo(obj.getCantidad());
							break;
						}
						case 5:{
							setNotaCredito(obj.getCantidad());
							break;
						}
						case 6:{
							setTarjetaCredito(obj.getCantidad());
							break;
						}
						case 7:{
							setBanco(obj.getCantidad());
							break;
						}
					}
					if(doc.getEstado()=='2')
					{
						setObservaciones("ANULADO");
					}
				}
				
			
		}
		if(doc.getTipoTransaccion()==5)
		{
			SC.say("Retencion");
			setTipoTransaccion("Retencion");
			//setCliente(doc.getTblpersonaByIdPersona().getNombreComercial()+" "+doc.getTblpersonaByIdPersona().getRazonSocial());
			setCliente(doc.getTblpersonaByIdPersona().getRazonSocial());
			setNombreComercial(doc.getTblpersonaByIdPersona().getNombreComercial());
			setNumRealTransaccion(doc.getNumRealTransaccion());
			//char tip=doc.getAutorizacion().charAt(0);
			//if(tip=='0')
			{
				setEfectivo(doc.getSubtotal()*-1);
			}
			//else
			{
				//setEfectivo(doc.getSubtotal());
			}	
		}
	}

	private void setTipo(char tipo) {
		setAttribute("Tipo", String.valueOf(tipo));
	}

	private void setObservaciones(String observaciones) {
		setAttribute("Observaciones", observaciones);
	}
	private void setBanco(double cantidad) {
		setAttribute("Banco", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setTarjetaCredito(double cantidad) {
		setAttribute("TarjetaCredito", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setNotaCredito(double cantidad) {
		setAttribute("NotaCredito", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setAnticipo(double cantidad) {
		setAttribute("Anticipo", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setRetencion(double cantidad) {
//		setAttribute("Retencion", cantidad);
		setAttribute("Retencion", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setCredito(double cantidad) {
//		setAttribute("Credito", cantidad);
		setAttribute("Credito", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setEfectivo(double cantidad) {
//		setAttribute("Efectivo", cantidad);
		setAttribute("Efectivo", CValidarDato.getDecimal(Factum.banderaNumeroDecimales,cantidad));
	}
	private void setCliente(String string) {
		setAttribute("Cliente", string);
	}
	private void setNombreComercial(String string) {
		setAttribute("NombreComercial", string);
	}
	private void setTipoTransaccion(String string) {
		setAttribute("TipoTransaccion", string);
	}
	private void setNumRealTransaccion(int numRealTransaccion) {
		setAttribute("NumRealTransaccion", numRealTransaccion);
	}

}
