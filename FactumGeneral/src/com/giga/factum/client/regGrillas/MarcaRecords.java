package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.MarcaDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class MarcaRecords extends ListGridRecord {
	public MarcaRecords() {
    }
	public MarcaRecords(MarcaDTO mar) {
        setidMarca(String.valueOf(mar.getIdMarca()));
        setMarca(mar.getMarca());
	}
    public MarcaRecords(String id,String marca) {
        setidMarca(id);
        setMarca(marca);
    }

	private void setMarca(String marca) {
		// TODO Auto-generated method stub
		setAttribute("Marca", marca);
	}

	private void setidMarca(String idMarca) {
		// TODO Auto-generated method stub
		setAttribute("idMarca", idMarca);
	}
	public String getMarca() {
        return getAttributeAsString("Marca");
    }
	public String getidMarca() {
        return getAttributeAsString("idMarca");
    }
	

}
