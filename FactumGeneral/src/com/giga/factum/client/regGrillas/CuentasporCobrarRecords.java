package com.giga.factum.client.regGrillas;

import java.util.Date;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CuentasporCobrarRecords extends ListGridRecord{
	public CuentasporCobrarRecords() {
		// TODO Auto-generated constructor stub
	}
	
	public CuentasporCobrarRecords(DtocomercialDTO doc)
	{	
		
		setfechaEmision(doc.getFecha());
		
	}
	public void setCliente(String Cliente)
	{

		setAttribute("Cliente",Cliente);
	}
	public void setfacturasVencidas(String fechaVencimiento) {
		setAttribute("Factura",fechaVencimiento);
	}
	public void setvalorTotal(double costo)
	{
			setAttribute("ValorTotal",Math.round(costo));
	}
	public void setdiasVencidos(double dias,double saldo)
	{
		double aux=0;
		aux=(double)Math.round(saldo * 100d) / 100d;
		if(aux<=0)
		{
			setAttribute("Dias","--");
		}
		else{
			setAttribute("Dias",dias);
			}
		//saldo1=valor-saldo;
		/*if(aux.equals("1"))
		{
			setAttribute("Dias","--");
		}
		else
		{
			if(saldo!=0&&estado.equals("1"))
			{
				saldo1=valor-saldo;
				if(saldo1==0)
				{
					setAttribute("Dias","--");
				}
				else{ 
					setAttribute("Dias",dias);
				}
			}
			else if(saldo!=0 && estado.equals("0"))
			{
				//setAttribute("Dias",dias);
			//}
			
		//}*/
	}
	public void setfechaEmision(String fechaEmision)
	{

		setAttribute("FechaEmision",fechaEmision);
	}
	public void setsaldo(double saldo)
	{
		String aux="";
		double saldo1=0;
		/*if(saldo!=0&&estado.equals("1"))
		{
			saldo1=valor-saldo;
			if(saldo1==0)
			{
				setAttribute("Saldo","0");
			}
			else{ 
				setAttribute("Saldo",(double)Math.round(saldo1 * 100d) / 100d);
			}
		}else if(saldo!=0 && estado.equals("0"))
		{*/
			setAttribute("Saldo",(double)Math.round(saldo * 100d) / 100d);
		//}
	}
	public String getCliente()
	{
		return getAttributeAsString("Cliente");
		
	}
	public String getfacturasVencidas()
	{
		return getAttributeAsString("Factura");
		
	}
	public double getvalorTotal()
	{
		return getAttributeAsDouble("ValorTotal");
		
	}
	public double getdiasVencidos()
	{
		return getAttributeAsDouble("Dias");
		
	}
	public String getfechaEmision()
	{
		return getAttributeAsString("FechaEmision");
		
	}
	public double getsaldo()
	{
		return getAttributeAsDouble("Saldo");
		
	}
	public void setestado(String estado,double saldo)
	{
		double aux=0;
		aux=(double)Math.round(saldo * 100d) / 100d;
		if(aux<=0)
		{
			setAttribute("Estado","CANCELADO");
		}
		else{
			setAttribute("Estado","PENDIENTE");
			}
		/*if(saldo!=0&&estado.equals("1"))
		{
			saldo1=valor-saldo;
			if(saldo1==0)
			{
				setAttribute("Estado","CANCELADO");
			}
			else{ 
				setAttribute("Estado","PENDIENTE");;
			}
		}
		else if(saldo!=0 && estado.equals("0"))
		{
			setAttribute("Estado","PENDIENTE");;
		}
		*/
		
				
	}
	public String getestado()
	{
		return getAttributeAsString("Estado");
		
	}
//	public void setimpuesto(double impuesto)
//	{
//
//		setAttribute("impuesto",impuesto);
//	}
//	public double getimpuesto()
//	{
//		return getAttributeAsDouble("impuesto");
//		
//	}
	public void settipo(int tipo)
	{

		setAttribute("tipo",tipo);
	}
	public double gettipo()
	{
		return getAttributeAsDouble("tipo");
		
	}
	public Date getfechaRealPago()
	{
		return getAttributeAsDate("fechadepago");
		
	}
	public void setfechaRealPago(Date fechaPago) {
		setAttribute("fechadepago",fechaPago);
	}
	public Date getformadePago()
	{
		return getAttributeAsDate("formadePago");
		
	}
	public void setformadePago(String formadePago) {
		
		if(formadePago.equals(""))
		{
			setAttribute("formadePago","Pendiente");
		}
		
		else{setAttribute("formadePago",formadePago);}
	}


	

}
