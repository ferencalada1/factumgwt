package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TmpComprobacionDTO;
import com.ibm.icu.math.BigDecimal;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ComprobacionRecords extends ListGridRecord {
	public ComprobacionRecords()
	{
		
	}
	public ComprobacionRecords(TmpComprobacionDTO tmpcom)
	{
		setcodigo(tmpcom.getTmpcodigo());
		setcuenta(tmpcom.getTmpcuenta());
		setdebe(tmpcom.getTmpdebe());
		sethaber(tmpcom.getTmphaber());
		setdeudor(tmpcom.getTmpdeudor());
		setacreedor(tmpcom.getTmpacreedor());
	}
	
	public void setcodigo(String codigo)
	{

		setAttribute("codigo",codigo);
	}
	public void setcuenta(String cuenta)
	{

		setAttribute("cuenta",cuenta);
	}
	public void setdebe(java.math.BigDecimal debe)
	{

		setAttribute("debe",debe);
	}
	public void sethaber(java.math.BigDecimal haber)
	{

		setAttribute("haber",haber);
	}
	public void setdeudor(java.math.BigDecimal deudor)
	{

		setAttribute("deudor",deudor);
	}
	public void setacreedor(java.math.BigDecimal acreedor)
	{

		setAttribute("acreedor",acreedor);
	}

}
