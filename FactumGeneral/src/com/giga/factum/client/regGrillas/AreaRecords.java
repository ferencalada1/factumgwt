package com.giga.factum.client.regGrillas;


import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AreaRecords extends ListGridRecord {
	public AreaRecords() {
    }
	public AreaRecords(AreaDTO areadto) {
        setidArea(areadto.getIdArea());
        setArea(areadto.getNombre());
	}
    public AreaRecords(int id,String Area) {
        setidArea(id);
        setArea(Area);
    }

	private void setArea(String Area) {
		setAttribute("Area", Area);
	}

	private void setidArea(int idArea) {
		setAttribute("idArea", idArea);
	}
	public String getArea() {
        return getAttributeAsString("Area");
    }
    public int getidArea(){
    	return getAttributeAsInt("idArea");
    }

}
