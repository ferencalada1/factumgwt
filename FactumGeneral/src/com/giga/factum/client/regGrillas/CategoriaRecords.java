package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.CategoriaDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CategoriaRecords extends ListGridRecord {
	public CategoriaRecords() {
    }
	public CategoriaRecords(CategoriaDTO mar) {
        setidCategoria(mar.getIdCategoria());
        setCategoria(mar.getCategoria());
	}
    public CategoriaRecords(int id,String Categoria) {
        setidCategoria(id);
        setCategoria(Categoria);
        
    }

	private void setCategoria(String Categoria) {
		setAttribute("Categoria", Categoria);
	}

	private void setidCategoria(int idCategoria) {
		
		setAttribute("idCategoria", idCategoria);
	}
	public String getCategoria() {
        return getAttributeAsString("Categoria");
    }
	
	public int getidCategoria(){
		return getAttributeAsInt("idCategoria");
	}


}
