package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TmpAuxgeneralDTO;
import com.giga.factum.client.DTO.TmpAuxresultadosDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class EstadoResultadosRecord extends ListGridRecord{
	public EstadoResultadosRecord()
	{
		
	}
	public EstadoResultadosRecord(TmpAuxresultadosDTO res)
	{
		setcodigo(res.getTmpcodigo());
		setcuenta(res.getTmpcuenta());
		setresta(res.getResta());
	}
	public void setcodigo(String codigo)
	{

		setAttribute("codigo",codigo);
	}
	public void setcuenta(String cuenta)
	{

		setAttribute("cuenta",cuenta);
	}
	public void setresta(java.math.BigDecimal resta)
	{

		setAttribute("resta",resta);
	}
}
