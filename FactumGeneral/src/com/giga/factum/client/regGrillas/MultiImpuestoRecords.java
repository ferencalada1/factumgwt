package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class MultiImpuestoRecords extends ListGridRecord{
	DataSourceBooleanField checkField = new DataSourceBooleanField("checkBox"," ");
	public MultiImpuestoRecords(){
			
		}
	public MultiImpuestoRecords(TblmultiImpuestoDTO imp){
		setidImpuesto(imp.getIdmultiImpuesto());
		setPorcentaje(imp.getPorcentaje());
		setDescripcion(imp.getDescripcion());
		setTipo(String.valueOf(imp.getTipo()));
	}
	
	private int getidImpuesto() {
		return getAttributeAsInt("idImpuestoGrid");
	}
	
	public String getDescripcion() {
        return getAttributeAsString("impuestoGrid");
    }
	
	public int getPorcentaje() {
        return getAttributeAsInt("porcentajeGrid");
    }
	
	public String getTipo(){
		return getAttributeAsString("tipoImpuestoGrid");
	}
	
	private void setPorcentaje(Integer retencion) {
		setAttribute("porcentajeGrid", retencion);
	}
	private void setDescripcion(String nombre) {
		setAttribute("impuestoGrid", nombre);
	}
	private void setidImpuesto(Integer idImpuesto) {
		setAttribute("idImpuestoGrid", idImpuesto);
	}
	private void setTipo(String tipo){
		setAttribute("tipoImpuestoGrid",tipo);
	}
}
