package com.giga.factum.client.regGrillas;

import java.util.Date;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class RetencionRecords extends ListGridRecord{
	public RetencionRecords()
	{
	}
	public RetencionRecords(RetencionDTO ret)
	{
		
		setcodigoImpuesto(ret.getImpuesto().getCodigo());
		setporcentajeRetencion(ret.getImpuesto().getRetencion());
		setnumeroRealRetencion(ret.getNumRealRetencion());
		setbaseImponible(ret.getBaseImponible());
		setvalorRetenido(ret.getValorRetenido());
		
	}
	 
	public void setCliente (String cliente)
	{
		setAttribute("cliente", cliente);
	}
	public void setRuc(String ruc)
	{
		setAttribute("ruc", ruc);
	}
	public void setfechaRetencion(String fecha)
	{
		setAttribute("fecha", fecha);
	}
	public void settipoDocumento(int tipoDocumento)
	{
		int aux=tipoDocumento;
		switch(aux)
		{
	
		case 0:
			setAttribute("tipoDocumento", "Factura de Venta");
			break;
		case 1:
			setAttribute("tipoDocumento", "Factura de Compra");
			break;
		case 2:
			setAttribute("tipoDocumento", "Nota de Entrega");
		case 3:
			setAttribute("tipoDocumento", "Nota de Credito");
			break;
		case 4:
			setAttribute("tipoDocumento", "Anticipo Clientes");
			break;
		case 5:
			setAttribute("tipoDocumento", "Retencion");
		case 6:
			setAttribute("tipoDocumento", "Pago Recibido");
			break;
		case 7:
			setAttribute("tipoDocumento", "Gastos");
			break;
		case 8:
			setAttribute("tipoDocumento", "Iva Cobrado");
			break;
		case 9:
			setAttribute("tipoDocumento", "Iva Pagado");
			break;
		case 10:
			setAttribute("tipoDocumento", "Nota de Compra");
			break;
		case 11:
			setAttribute("tipoDocumento", "Pago Realizado");
			break;
		case 12:
			setAttribute("tipoDocumento", "Ingresos");
			break;
		case 13:
			setAttribute("tipoDocumento", "Factura Grande");
			break;
		case 14:
			setAttribute("tipoDocumento", "Nota Credito Proveedores");
			break;
		case 15:
			setAttribute("tipoDocumento", "Anticipos Proveedores");
			break;
		}
	}
	public void setnumeroFatura(int numeroFactura, DtocomercialDTO dtocomercialDTO)
	{
		int aux=numeroFactura; 
		if(aux==0)
		{
			DtocomercialDTO dto= dtocomercialDTO;
			setAttribute("numeroFactura", dto.getNumRealTransaccion());
		}else
		{
			if(aux==1)
			{
				DtocomercialDTO dto= dtocomercialDTO;
				setAttribute("numeroFactura", dto.getNumCompra());
			}
		}
		setAttribute("numeroFatura", numeroFactura);
	}
	public void setnumeroRealTransaccion(int numeroRealTransaccion)
	{
		setAttribute("numeroDocumento", numeroRealTransaccion);
	}
	public void setbaseImponible(double baseImponible)
	{
		setAttribute("baseImponible", baseImponible);
	}
	public void setvalorRetenido(double valorRetenido)
	{
		setAttribute("valorRetenido", valorRetenido);
	}
	public void setsubTotal14(double subTotal14)
	{
		setAttribute("subTotal14", subTotal14);
	}
	public void setvalorRetenido14(double valorRetenido14)
	{
		setAttribute("valorRetenido14", valorRetenido14);
	}
	public void setsubTotal0(double subTotal0)
	{
		setAttribute("subTotal0", subTotal0);
	}
	public void setvalorRetenido0(double valorRetenido0)
	{
		setAttribute("valorRetenido0", valorRetenido0);
	}
	public void setcodigoImpuesto(int codigoImpuesto)
	{
		setAttribute("codigoImpuesto", codigoImpuesto);
	}
	public void setporcentajeRetencion(double d)
	{
		setAttribute("porcentajeRetencion", d);
	}
	public void setnumeroRealRetencion(String numeroRealRetencion)
	{
		setAttribute("numeroRealRetencion", numeroRealRetencion);
	}
	public String getcliente()
	{
		return getAttributeAsString("cliente");		
	}
	public String getRuc()
	{
		return getAttributeAsString("ruc");
	}
	public String getfechaRetencion()
	{
		return getAttributeAsString("fecha");
		
	}
	public int gettipoDocumento()
	{
		return getAttributeAsInt("tipoDocumento");
	}
	public String getnumeroFatura()
	{
		return getAttributeAsString("numeroFactura");
	}
	public int getnumeroRealTransaccion()
	{
		return getAttributeAsInt("numeroDocumento");
	}
	public double getbaseImponible()
	{
		return getAttributeAsDouble("baseImponible");
	}
	public double getvalorRetenido()
	{
		return getAttributeAsDouble("valorRetenido");
	}
	public double getsubTotal14()
	{
		return getAttributeAsDouble("subTotal14");
	}
	public double getvalorRetenido14()
	{
		return getAttributeAsDouble("valorRetenido14");
	}
	public double getsubTotal0()
	{
		return getAttributeAsDouble("subTotal0");
	}
	public double getvalorRetenido0()
	{
		return getAttributeAsDouble("valorRetenido0");
	}
	public int getcodigoImpuesto()
	{
		return getAttributeAsInt("codigoImpuesto");
	}
	public double getporcentajeRetencion()
	{
		return getAttributeAsInt("porcentajeRetencion");
	}
	public String getnumeroRealRetencion()
	{
		return getAttributeAsString("numeroRealRetencion");
	}
	

}
