package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.PedidoProductoelaboradoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PedidoProductoelaboradoRecords extends ListGridRecord{
	
	public PedidoProductoelaboradoRecords(){
		
	}
	
	public PedidoProductoelaboradoRecords(PedidoProductoelaboradoDTO pedidoproductoelboradodto) {
		setNumPedido(String.valueOf(pedidoproductoelboradodto.getIdtblpedidoTblproductoelaborado()));
		//setEmpleado(String.valueOf(pedidoproductoelboradodto.getTblpedido().getTblempleado().getTblpersona().getNombreComercial()+" "+String.valueOf(pedidoproductoelboradodto.getTblpedido().getTblempleado().getTblpersona().getRazonSocial())));
		setEmpleado(String.valueOf(pedidoproductoelboradodto.getTblpedido().getTblempleado().getTblpersona().getRazonSocial()));
		setMesa(String.valueOf(pedidoproductoelboradodto.getTblpedido().getTblmesa().getNombreMesa()));
		setFecha(String.valueOf(pedidoproductoelboradodto.getTblpedido().getFecha()));
		
		
		// TODO Auto-generated constructor stub
	}
	
	public void setNumPedido(String numpedido){
		setAttribute("Num Pedido", numpedido);
	}
	
	public String getNumPedido(){
		return getAttributeAsString("Num Pedido");
	}
	
	public void setMesa(String mesa){
		this.setAttribute("Mesa", mesa);
	}
	
	public String getMesa(){
		return getAttributeAsString("Mesa");
	}
	
	public void setEmpleado(String empleado){
		this.setAttribute("Empleado", empleado);
	}
	
	public String getEmpleado(){
		return getAttributeAsString("Empleado");
	}

	public void setFecha(String fecha){
		this.setAttribute("Fecha", fecha);
	}
	
	public String getFecha(){
		return getAttributeAsString("Fecha");
	}
}
