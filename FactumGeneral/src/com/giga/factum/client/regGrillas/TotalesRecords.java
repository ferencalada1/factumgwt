package com.giga.factum.client.regGrillas;

import com.giga.factum.client.Factum;
import com.giga.factum.client.DTO.CargoDTO;
import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TotalesRecords extends ListGridRecord {
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public TotalesRecords() {
    }
	public TotalesRecords(String nom,Double tot) {
        settot(formatoDecimalN.format(tot));
        setNombre(nom);
	}
	private void setNombre(String nom) {
		setAttribute("Nombre", nom);
	}
	private void settot(String valueOf) {
		setAttribute("Total", valueOf);
	}

}
