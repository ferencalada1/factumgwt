package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.smartgwt.client.util.SC;
import java.util.Date;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PlanCuentaRecords extends ListGridRecord{
	public PlanCuentaRecords(){

	}
	
	public PlanCuentaRecords(PlanCuentaDTO cue) {
	       try {
	    	   setidPlanCuenta(cue.getIdPlan());
	           setFechaCreacion(cue.getFechaCreacion().toString());
	           setPeriodo(cue.getPeriodo());
	           setEmpresa(cue.getTblempresa().getNombre());
	           setidEmpresa(cue.getTblempresa().getIdEmpresa());
	           
	       }catch (Exception e){
	    	    
	    	   SC.say(e.getLocalizedMessage());
	       }
			
	        
		}
	    private void setidEmpresa(Integer idEmpresa) {
	    	setAttribute("idEmpresa", idEmpresa);
		
	    }

		public PlanCuentaRecords(int idCuenta,String FechaCreacion,String Periodo,String Empresa) {
	    	setidPlanCuenta(idCuenta);
	    	setFechaCreacion(FechaCreacion);
	    	setPeriodo(Periodo);
	    	setEmpresa(Empresa);
	        
	    }

		private void setEmpresa(String empresa) {
			setAttribute("Empresa", empresa);
			
		}

		private void setPeriodo(String periodo) {
			setAttribute("Periodo", periodo);
			
		}

		private void setFechaCreacion(String fechaCreacion) {
			setAttribute("FechaCreacion", fechaCreacion);
			
		}

		private void setidPlanCuenta(int idCuenta) {
			setAttribute("idPlanCuenta", idCuenta);
			
		}
		public int getidPlanCuenta() {
	        return getAttributeAsInt("idPlanCuenta");
	    }
		public String getFechaCreacion() {
			return getAttribute("FechaCreacion");
		}
		public String getEmpresa() {
			return getAttribute("Empresa");
			
		}
		public String getPeriodo() {
			return getAttribute("Periodo");
			
		}
		public String getidEmpresa() {
			return getAttribute("idEmpresa");
		}
		

}
