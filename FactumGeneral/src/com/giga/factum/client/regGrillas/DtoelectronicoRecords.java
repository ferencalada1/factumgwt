package com.giga.factum.client.regGrillas;

import java.util.Date;
import java.util.Iterator;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.Factum;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtoelectronicoDTO;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class DtoelectronicoRecords extends ListGridRecord{
	
	public DtoelectronicoRecords() {
    }
	public DtoelectronicoRecords(DtoelectronicoDTO doc) {
       try {
    	   setidDoc(doc.getDtocomercialDTO().getIdDtoComercial());
           setNumReal(doc.getDtocomercialDTO().getNumRealTransaccion());
           setTipo(doc.getDtocomercialDTO().getTipoTransaccion());
           //setFecha(String.valueOf(doc.getFecha()));
           //setExpiracion(String.valueOf(doc.getExpiracion()));
           String fechaAux=doc.getFechaautorizacion().toString();
           fechaAux=fechaAux.substring(0, 10);
           setFecha(fechaAux);

           setRuc(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getCedulaRuc());
           setAutorizacion(doc.getAutorizacion());
           setEstado(doc.getEstado());
             
           //setSubtotal(doc.);
           setVendedor(String.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdVendedor().getRazonSocial()));
           setIdCliente(Integer.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getIdPersona()));
           setIdVendedor(Integer.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdVendedor().getIdPersona()));
           //setCliente(String.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getRazonSocial()+" "+doc.getDtocomercialDTO().getTblpersonaByIdPersona().getNombreComercial()));
           setCliente(String.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getRazonSocial()));
           setNombreComercial(String.valueOf(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getNombreComercial()));
           Iterator resultado = doc.getDtocomercialDTO().getTbldtocomercialdetalles().iterator();
           double base=0;
           double descuento=0;
           double subiva=0.0;
           double subiva0=0.0;
           double subtotal=0;
           double total=0;
           double iva=0;
           
           double subImp=0.0;
           while ( resultado.hasNext() ) {
        	   DtoComDetalleDTO det=(DtoComDetalleDTO) resultado.next();
        	   /*subtotal=subtotal+det.getTotal();
        	   descuento=descuento+det.getDepartamento()/100*det.getTotal();*/
        	   subtotal=subtotal+((det.getTotal()*100)/(100-det.getDepartamento()));
        	   descuento=descuento+(det.getDepartamento()*det.getTotal())/(100-det.getDepartamento());
        	   
        	   String impuestos="";
				double impuestoPorc=1.0;
				double impuestoValor=0.0;
				int i1=0;
				//com.google.gwt.user.client.Window.alert("impuestos de detalle "+det.getTblimpuestos().size());
				for (DtoComDetalleMultiImpuestosDTO detalleImp: det.getTblimpuestos()){
					i1=i1+1;
					impuestos+=detalleImp.getPorcentaje().toString();
					impuestos= (i1<det.getTblimpuestos().size())?impuestos+",":impuestos; 
					impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
					impuestoValor=impuestoValor+((det.getCantidad()*det.getPrecioUnitario()*(1-(det.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
				}
				//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
        	   
//        	   if(det.getImpuesto()==Factum.banderaIVA){
				if(impuestoPorc-1>0){
        		   subiva=subiva+det.getTotal()-det.getDepartamento()/100*det.getTotal();
        		   subImp+=impuestoValor;
        	   }else if(impuestoPorc-1==0){
        		   subiva0=subiva0+det.getTotal()-det.getDepartamento()/100*det.getTotal();
        	   }
           }
           base=subtotal-descuento-doc.getDtocomercialDTO().getDescuento();
           if(doc.getDtocomercialDTO().getTipoTransaccion()==2 || doc.getDtocomercialDTO().getTipoTransaccion()==3)
           {
        	   if(doc.getDtocomercialDTO().getCosto()<=0)
        	   {   
        		   iva=0;
        	   }
        	   else
        	   {
        		   iva=doc.getDtocomercialDTO().getCosto();
        	   }
           }else{
//        	   iva=(subiva-doc.getDtocomercialDTO().getDescuento())*(Factum.banderaIVA/100);
        	   iva=subImp;
           }
           
           if(doc.getDtocomercialDTO().getTipoTransaccion()==2||doc.getDtocomercialDTO().getTipoTransaccion()==7||doc.getDtocomercialDTO().getTipoTransaccion()==3||doc.getDtocomercialDTO().getTipoTransaccion()==10||doc.getDtocomercialDTO().getTipoTransaccion()==14
        		   ||doc.getDtocomercialDTO().getTipoTransaccion()==4||doc.getDtocomercialDTO().getTipoTransaccion()==15||doc.getDtocomercialDTO().getTipoTransaccion()==12)
           {
        	   setSubtotal(doc.getDtocomercialDTO().getSubtotal());
           }
           else
           {
        	   double totalLista =iva+subiva+subiva0-doc.getDtocomercialDTO().getDescuento();//(doc.getSubtotal()-descuento - doc.getDescuento())*1.12;
        	   totalLista=CValidarDato.getDecimal(2, totalLista);
        	   setSubtotal(totalLista);
           } 
           
           total=iva+subiva+subiva0-doc.getDtocomercialDTO().getDescuento();
           subiva = subiva -doc.getDtocomercialDTO().getDescuento();
           Double descuentoGlobal = descuento + doc.getDtocomercialDTO().getDescuento();
//           setDescuento(Math.rint(descuentoGlobal*100)/100);
//           setBase( Math.rint(base*100)/100);
//           setIva0(Math.rint(subiva0*100)/100);
//           setIva14(Math.rint(subiva*100)/100);
//           setIva(Math.rint(iva*100)/100);
//           setTotal(Math.rint(total*100)/100);
           setDescuento(CValidarDato.getDecimal(Factum.banderaNumeroDecimales, descuentoGlobal));
           setBase( CValidarDato.getDecimal(Factum.banderaNumeroDecimales,base));
           setIva0(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,subiva0));
           setIva14(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,subiva));
           setIva(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,iva));
           setTotal(CValidarDato.getDecimal(Factum.banderaNumeroDecimales,total));
           setTipoDocumento(doc.getDtocomercialDTO().getTipoTransaccion());
           
         //  setRuc(doc.getTblpersonaByIdPersona().getCedulaRuc());
           setDireccion(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getDireccion());
           setIdPersona(doc.getDtocomercialDTO().getTblpersonaByIdPersona().getIdPersona());
           setRetencion(doc.getDtocomercialDTO().getTotalRetencion());
           setRetencionIVA(doc.getDtocomercialDTO().getTotalRetencionIVA());
           setRetencionRENTA(doc.getDtocomercialDTO().getTotalRetencionRENTA());
           
           
       }catch (Exception e){ 
    	   SC.say("error al listar datos");
       }

	}
	private void setRetencionRENTA(double totalRetencionRENTA) {
		setAttribute("retencionRENTA",totalRetencionRENTA);
		
	}
	private void setRetencionIVA(double totalRetencion) {
		setAttribute("retencionIVA",totalRetencion);
		
	}
	private void setRetencion(double totalRetencion) {
		setAttribute("retencion",totalRetencion);
		
	}
	private void setIdPersona(Integer idPersona) {
		setAttribute("idPersona",idPersona);
		
	}
	private void setDireccion(String direccion) {
		setAttribute("direccion",direccion);
		
	}
	private void setRuc(String cedulaRuc) {
		setAttribute("RUC",cedulaRuc);
		
	}
	private void setTipoDocumento(int tipoTransaccion) {
		switch (tipoTransaccion) {
			case 0:{
				setAttribute("TipoTransaccion","Facturas de Venta");
				break;
			}
			case 1:{
				setAttribute("TipoTransaccion","Facturas de Compra");
				break;
			}
			case 2:{
				setAttribute("TipoTransaccion","Notas de Entrega");
				break;
			}
			case 3:{
				setAttribute("TipoTransaccion","Notas de Credito");
				break;
			}
			case 4:{
				setAttribute("TipoTransaccion","Anticipo Clientes");
				break;
			}
			case 5:{
				setAttribute("TipoTransaccion","Retencion");
				break;
			}
			case 7:{
				setAttribute("TipoTransaccion","Gastos");
				break;
			}
			case 8:{
				setAttribute("TipoTransaccion","Ajustes de Inventario");
				break;
			}
			
			case 10:{
				setAttribute("TipoTransaccion","Notas de Compra");
				break;
			}
			case 15:{
				setAttribute("TipoTransaccion","Anticipo a Proveedores");
				break;
			}
			case 20:{
				setAttribute("TipoTransaccion","Proforma");
				break;
			}
		}
	}
	private void setIva14(double iva) {
		setAttribute("Iva14",iva);
		
	}
	private void setTotal(double d) {
		setAttribute("Total",d);
		
	}
	private void setBase(double base) {
		setAttribute("Base",base);
		
	}
	private void setDescuento(double descuento) {
		setAttribute("Descuento",descuento);
		
		
	}
	private void setIva(double iva) {
		setAttribute("Iva",iva);
		
	}
	private void setIva0(double iva0) {
		setAttribute("Iva0",iva0);
		
	}
	private Double getSubtotal() {
		return getAttributeAsDouble("SubTotal");
		
	}
	private void setSubtotal(double d) {
		setAttribute("SubTotal",d);
		
	}
	private String getEstado() {
		return getAttributeAsString("Estado");
		
	}
	private void setEstado(char estado) {
		
		setAttribute("Estado",estado);
		if(getAttributeAsString("Estado").equals("49")){
			setAttribute("Estado","CONFIRMADO");
		}else if(getAttributeAsString("Estado").equals("48")){
			setAttribute("Estado","NO CONFIRMADO");
		}else if(getAttributeAsString("Estado").equals("50")){
			setAttribute("Estado","ANULADA");
		}else if(getAttributeAsString("Estado").equals("53")){
			setAttribute("Estado","UTILIZADO");
		}
		else if(getAttributeAsString("Estado").equals("51")){
			   setAttribute("Estado","TRANSFORMADO");
			  }				
	}
	private int getNumPagos() {
		return getAttributeAsInt("NumPagos");
		
	}
	
	private void setNumPagos(int numPagos) {
		setAttribute("NumPagos",numPagos);
		
	}
	private String getAutorizacion() {
		return getAttribute("Autorizacion");
		
	}
	
	private void setAutorizacion(String autorizacion) {
		setAttribute("Autorizacion",autorizacion);
		
	}
	private String getCliente() {
		return getAttribute("Cliente");
		
	} 
	private void setCliente(String string) {
		setAttribute("Cliente",string);
		
	}
	private void setNombreComercial(String string) {
		setAttribute("NombreComercial",string);
		
	}
	private String getVendedor() {
		return getAttribute("Vendedor");
		
	}
	private void setVendedor(String string) {
		setAttribute("Vendedor",string);
		
	}
	private Date getExpiracion() {
		return getAttributeAsDate("FechadeExpiracion");
		
	}
	
	private void setExpiracion(String string) {
		setAttribute("FechadeExpiracion",string);
		
	}
	private Date getFecha() {
		return getAttributeAsDate("Fecha");
		
	}
	private void setFecha(String string) {
		setAttribute("Fecha",string);
		
	}
	private String getTipo() {
		return getAttribute("TipoTransaccion");
	}
	private void setTipo(int tipoTransaccion) {
		if(tipoTransaccion==1){
			setAttribute("TipoTransaccion","Venta con Factura");
		}else if(tipoTransaccion==2){
			setAttribute("TipoTransaccion","Factura de Compra");
		}else if(tipoTransaccion==3){
			setAttribute("TipoTransaccion","Nota de Credito");
		}
	}
	private int getNumReal() {
		return getAttributeAsInt("NumRealTransaccion");
		
	}
	private void setNumReal(int numRealTransaccion) {
		setAttribute("NumRealTransaccion", numRealTransaccion);
		
	}
	private int getidDoc() {
		return getAttributeAsInt("id");
	}
	
	private void setidDoc(Integer idDtoComercial) {
		setAttribute("id", idDtoComercial);
	}
	
	private int getIdCliente() {
		return getAttributeAsInt("idCliente");
	}
	private int getIdVendedor() {
		return getAttributeAsInt("idVendedor");
	}
	private void setIdCliente(Integer idCliente) {
		setAttribute("idCliente", idCliente);
	}
	private void setIdVendedor(Integer idVendedor) {
		setAttribute("idVendedor", idVendedor);
	}
}
