package com.giga.factum.client.regGrillas;

import java.util.Iterator;

import com.giga.factum.client.CValidarDato;
import com.giga.factum.client.Factum;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class DetallesRecords extends ListGridRecord{
	
	public DetallesRecords(){
		
	}
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public DetallesRecords(DtoComDetalleDTO dtocomecialdetalledto,int numrealtransaccion, int tipotransaccion) {
       try {
    	   int decimal=Factum.banderaNumeroDecimales;
    	   setNumReal(numrealtransaccion);
    	   DtoComDetalleDTO det=dtocomecialdetalledto;
    	   setIdDetalle(det.getIdDtoComercialDetalle());
    	   setDescripcion(det.getDesProducto());
//    	   setPrecioUnitario(CValidarDato.getDecimal(decimal, det.getPrecioUnitario()));
    	   setPrecioUnitario(formatoDecimalN.format(det.getPrecioUnitario()));
    	   setCantidad(det.getCantidad());
//    	   setTotal(CValidarDato.getDecimal(decimal,det.getTotal()));
//    	   setCosto(CValidarDato.getDecimal(decimal,det.getCostoProducto()));
    	   setTotal(formatoDecimalN.format(det.getTotal()));
    	   setCosto(formatoDecimalN.format(det.getCostoProducto()));
    	   double utilidad=0.0;
    	   double utilidadP=0.0;
    	   /*if(tipotransaccion==0){    		   
    		   utilidad=det.getTotal()-(det.getCantidad()*(det.getCostoProducto()*(1+(det.getImpuesto()/100))));
    	   }else if(tipotransaccion==2){
    		   utilidad=det.getTotal()-(det.getCantidad()*(det.getCostoProducto()));
    	   }*/
    	   if(tipotransaccion==0 || tipotransaccion==2){
    		   if (det.getCostoProducto()==0.0){
    			   utilidad=det.getTotal();
    			   utilidadP=(det.getTotal()==0)?0:100.0;
    		   }else{
    			   utilidad=det.getTotal()-(det.getCantidad()*(det.getCostoProducto()));
    			   utilidadP=(100*(det.getPrecioUnitario()-det.getCostoProducto()))/(det.getCostoProducto());
    			  //utilidad=(Factum.banderautilidadPorcentaje==1)?((100*(det.getPrecioUnitario()-det.getCostoProducto()))/(det.getCostoProducto()))
    					   //:det.getTotal()-(det.getCantidad()*(det.getCostoProducto()));
    		   }
    		}
//    	   setUtilidad(CValidarDato.getDecimal(decimal,Math.rint(utilidad*100)/100));
//    	   setUtilidadPorc(CValidarDato.getDecimal(decimal,Math.rint(utilidadP*100)/100));
    	   
    	   setUtilidad(formatoDecimalN.format(utilidad));
    	   
    	   setUtilidadPorc(formatoDecimalN.format(utilidadP));
//    	   setUtilidad(CValidarDato.getDecimal(decimal,utilidad));
//    	   setUtilidadPorc(CValidarDato.getDecimal(decimal,utilidadP));
           
    	   
       }catch (Exception e){ 
    	   SC.say("error al listar datos ");
       }
	}

	private void setUtilidadPorc(String d) {
		// TODO Auto-generated method stub
		setAttribute("lstUtilidadP", d+"%");
	}
	private void setUtilidad(String d) {
		setAttribute("lstUtilidad", d);
		
	}
//	private void setUtilidadPorc(double d) {
//		// TODO Auto-generated method stub
//		setAttribute("lstUtilidadP", d);
//	}
//	private void setUtilidad(double d) {
//		setAttribute("lstUtilidad", d);
//		
//	}
	private void setCosto(String costoProducto) {
		setAttribute("lstCosto", costoProducto);
		
	}
	private void setTotal(String total) {
		setAttribute("lstTotal", total);
	}
//	private void setCosto(double costoProducto) {
//		setAttribute("lstCosto", costoProducto);
//		
//	}
//	private void setTotal(double total) {
//		setAttribute("lstTotal", total);
//	}
	private void setCantidad(double cantidad) {
		setAttribute("lstCantidad", cantidad);
		
	}
	private void setPrecioUnitario(String precioUnitario) {
		setAttribute("lstPrecio", precioUnitario);
		
	}
//	private void setPrecioUnitario(double precioUnitario) {
//		setAttribute("lstPrecio", precioUnitario);
//		
//	}
	private void setDescripcion(String desProducto) {
		String[] descripciones = desProducto.split(";");
		if (descripciones.length>1){
			setAttribute("lstDescripcion", descripciones[1]);
		}else{
			setAttribute("lstDescripcion", desProducto);
		}
	}
	private void setIdDetalle(Integer idDtoComercialDetalle) {
		setAttribute("lstIdDetalle", idDtoComercialDetalle);
		
	}
	private void setNumReal(int numRealTransaccion) {
		setAttribute("lstNumFactura", numRealTransaccion);
		
	}
}
