package com.giga.factum.client.regGrillas;

import java.util.Date;

import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PagoRecords extends ListGridRecord  {
	public PagoRecords() {
    }
	public PagoRecords(TblpagoDTO pago) {
        setidPago(String.valueOf(pago.getIdPago()));
        setNumRealDoc(pago.getTbldtocomercial().getNumRealTransaccion());
        //setFechaV(String.valueOf(pago.getFechaVencimiento()));
        //setFechaR(String.valueOf(pago.getFechaRealPago()));
        String fechaAuxV=String.valueOf(pago.getFechaVencimiento());
        fechaAuxV=fechaAuxV.substring(0, 10);
        setFechaV(fechaAuxV);
        String fechaAuxR=String.valueOf(pago.getFechaRealPago());
        fechaAuxR=fechaAuxR.substring(0, 10);
        setFechaR(fechaAuxR);
//        if(pago.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial().equals(pago.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial())){
//        	setPersona(pago.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial());
//        }else{
//        	setPersona(pago.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial()+" "+pago.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial());
//        }
        setidRetencion(pago.getTbldtocomercial().getTblretencionsForIdFactura().size());
        setPersona(pago.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial());
        setLocalizacion(pago.getTbldtocomercial().getTblpersonaByIdPersona().getLocalizacion());
        setDireccion(pago.getTbldtocomercial().getTblpersonaByIdPersona().getDireccion());
        setTelefono(pago.getTbldtocomercial().getTblpersonaByIdPersona().getTelefono1()+"/"+pago.getTbldtocomercial().getTblpersonaByIdPersona().getTelefono2());
        setNombreComercial(pago.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial());
        setValor(String.valueOf(pago.getValor()));
        setEstado(String.valueOf(pago.getEstado()));
        setIddocumento(String.valueOf(pago.getTbldtocomercial().getIdDtoComercial()));
        setInfo("Info");
        setBan(false);
        try{
        	setNumEgreso(pago.getNumEgreso());
        	setObservaciones(pago.getConcepto());
            setValorPagado(pago.getValorPagado());
            setFechaPago(pago.getFechaRealPago().toString().substring(0, 11));
        }catch (Exception e){
        	setObservaciones("");
            //setValorPagado("0");
            //setFechaPago(pago.getFechaRealPago().toString());
        }
        
        
	}
	private void setNumEgreso(int numEgreso) {
		setAttribute("lstNumEgreso",numEgreso);
		
	}
	private void setFechaPago(String fechaRealPago) {
		setAttribute("lstFechaP",fechaRealPago);
		
	}
	private void setValorPagado(double valorPagado) {
		setAttribute("lstValorAPagar",valorPagado);
		
	}
	private void setLocalizacion(String string) {
		setAttribute("lstLocalizacion",string);
		
	}
	private void setDireccion(String string) {
		setAttribute("lstDireccion",string);
		
	}
	private void setObservaciones(String string) {
		setAttribute("lstObs",string);
		
	}
	private void setTelefono(String string) {
		setAttribute("lstTelefono",string);
		
	}
	private void setBan(Boolean string) {
		this.setAttribute("Ban",string);
		
	}
	private void setIddocumento(String valueOf) {
		setAttribute("idDocumento",valueOf);
		
	}
	private void setInfo(String string) {
		setAttribute("buttonField",string);
	}
	private void setEstado(String string) {
		setAttribute("lstEstado",string);
	}
	private void setValor(String string) {
		setAttribute("lstValor",string);
		
	}
	private void setPersona(String apellidos) {
		setAttribute("lstCliente",apellidos);
	}
	private void setNombreComercial(String apellidos) {
		setAttribute("lstNombreComercial",apellidos);
	}
	private void setFechaR(String string) {
		setAttribute("lstFechaP",string);
		
	}
	private void setFechaV(String string) {
		setAttribute("lstFechaV",string);
		
	}
	private void setNumRealDoc(int numRealTransaccion) {
		setAttribute("lstFactura",numRealTransaccion);
		
	}
	private void setidPago(String valueOf) {
		setAttribute("lstId", valueOf);
	}
	private void setidRetencion(Integer valueOf) {
		setAttribute("idRetencion", valueOf);
	}
	private String getidPago() {
		return getAttribute("lstId");
	}
	private String getNumRealDoc() {
		return getAttribute("lstFactura");
	}
	private String getFechaV() {
		return getAttribute("lstFechaV");
	}
	private String getFechaP() {
		return getAttribute("lstFechaP");
	}
	private String getPersona() {
		return getAttribute("lstCliente");
	}
	private String getvalor() {
		return getAttribute("lstValor");
	}
	private String getEstado() {
		return getAttribute("lstEstado");
	}
	private String getLocalizacion() {
		return getAttribute("lstLocalizacion");
	}
	private String getDireccion() {
		return getAttribute("lstDireccion");
	}
	private String getTelefono() {
		return getAttribute("lstTelefono");
	}
	private String getIddocumento() {
		return getAttribute("idDocumento");
	}
	private String getidRetencion() {
		return getAttribute("idRetencion");
		
	}
}
