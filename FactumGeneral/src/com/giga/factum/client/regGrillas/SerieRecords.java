package com.giga.factum.client.regGrillas;

import com.giga.factum.client.DTO.SerieDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SerieRecords extends ListGridRecord{
	
	public SerieRecords(){
		
	}
	
	public SerieRecords(SerieDTO serie){
		setCodBarrasProducto(serie.getTblproductos().getCodigoBarras());
		setDescripcion(serie.getTblproductos().getDescripcion());
		setNumFacturaC(serie.getIdDtoComercialC().getNumRealTransaccion());
		setNumFacturaV(serie.getIdDtoComercialV().getNumRealTransaccion());
		setFechaC(serie.getIdDtoComercialC().getFecha());
		setFechaV(serie.getIdDtoComercialV().getFecha());
		setIdFacC(serie.getIdDtoComercialC().getIdDtoComercial());
		setIdFacV(serie.getIdDtoComercialV().getIdDtoComercial());
		setSerie(serie.getSerie());
		
	}
	
	private void setSerie(String serie) {
		setAttribute("serie", serie);
		
	}

	private void setIdFacC(Integer idDtoComercial) {
		setAttribute("idFacC", idDtoComercial);
	}
	private void setIdFacV(Integer idDtoComercial) {
		setAttribute("idFacV", idDtoComercial);
	}
	private void setFechaC(String fecha) {
		this.setAttribute("fechaC", fecha);
		
	}
	private void setFechaV(String fecha) {
		this.setAttribute("fechaV", fecha);
	}

	private void setNumFacturaV(int numRealTransaccion) {
		setAttribute("numFacturaV",numRealTransaccion);
	}
	private void setNumFacturaC(int numRealTransaccion) {
		setAttribute("numFacturaC",numRealTransaccion);
	}

	private void setDescripcion(String descripcion) {
		setAttribute("descripcion",descripcion);
	}

	private void setCodBarrasProducto(String codigoBarras) {
		setAttribute("CodigoBarras",codigoBarras);
		
	}
	private String getNumFacturaC() {
		return getAttribute("numFacturaC");
	}
	private String getNumFacturaV() {
		return getAttribute("numFacturaV");
	}
	
	private String getDescripcion() {
		return getAttribute("descripcion");
	}
	private String getCodBarrasProducto() {
		return getAttribute("CodigoBarras");
	}

}
