package com.giga.factum.client;

import java.util.Date;
import java.util.List;

import com.smartgwt.client.widgets.layout.VLayout;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.ProveedorDTO;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;

public class frmProveedor extends VLayout{
	DynamicForm dynamicForm;
	DynamicForm dynamicFormFactura = new DynamicForm();
	DynamicForm dynamicFormRetencion = new DynamicForm();
	DynamicForm dynamicFormNota = new DynamicForm();
	
	ListGrid lstProveedor = new ListGrid();
	boolean ban=false;
	TabSet tabSet = new TabSet();
	final FloatItem txtcupoCreditoC = new FloatItem();
	final FloatItem txtmontoCredito = new FloatItem();
	private int idPersona;
	private int idProveedor;
	DateItem dateFecha_expiracionFactura = new DateItem("fecha_expiracionFactura","Fecha&nbspde&nbspCaducidad");
	TextItem txtSerieFactura=new TextItem("txtSerieFactura","Serie");
	TextItem txtAutorizacionFactura=new TextItem("txtAutorizacionFactura","Autorizacion");
	TextItem txtInicioFactura=new TextItem("txtInicioFactura","Desde&nbspla&nbspfactura:");
	TextItem txtFinFactura=new TextItem("txtFinFactura","Hasta&nbspla&nbspfactura:");
	
	DateItem dateFecha_expiracionRetencion = new DateItem("fecha_expiracionRetencion","Fecha&nbspde&nbspCaducidad");
	TextItem txtSerieRetencion=new TextItem("txtSerieRetencion","Serie");
	TextItem txtAutorizacionRetencion=new TextItem("txtAutorizacionRetencion","Autorizacion");
	TextItem txtInicioRetencion=new TextItem("txtInicioRetencion","Desde&nbspla&nbspretencion:");
	TextItem txtFinRetencion=new TextItem("txtFinRetencion","Hasta&nbspla&nbspretencion:");
	
	DateItem dateFecha_expiracionNota = new DateItem("fecha_expiracionNota","Fecha&nbspde&nbspCaducidad");
	TextItem txtSerieNota=new TextItem("txtSerieNota","Serie");
	TextItem txtAutorizacionNota=new TextItem("txtAutorizacionNota","Autorizacion");
	TextItem txtInicioNota=new TextItem("txtInicioNota","Desde&nbspla&nbspnota&nbspde&nbspcredito:");
	TextItem txtFinNota=new TextItem("txtFinNota","Hasta&nbspla&nbspnota&nbspde&nbspcredito:");
	
	Label lblRegisros = new Label("# Registros");
	int contador=20;
	int registros=0;
	SearchForm searchForm = new SearchForm();
	
	 Factura fact = null;
	public frmProveedor(Factura f) {
		
		this.setMembersMargin(10);  
		Tab tabListado = new Tab("Listado de Proveedores");
		fact = f;
		getService().numeroRegistrosPersona("tblproveedors", objbackI);
		
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("buscar"));
		PickerIcon buscarPickerlst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerlst.addFormItemClickHandler(new ManejadorBotones("buscarfrm"));
		
		FloatRangeValidator floatRangeValidator = new FloatRangeValidator(); 
		RegExpValidator regExpValidator = new RegExpValidator();  
        regExpValidator.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");  
        tabSet.setSize("100%", "100%");
		Tab tab = new Tab("Ingreso Proveedores");
		VLayout layout = new VLayout();
		layout.setSize("100%", "100%");
		dynamicForm = new DynamicForm();
		dynamicForm.setSize("70%", "25%");
		dynamicForm.setMinColWidth(50);
		dynamicForm.setItemLayout(FormLayoutType.TABLE);
		
		dynamicForm.setWidth100();
		
		dynamicForm.setNumCols(4);
		TextItem txtRazonSocial = new TextItem("txtRazonSocial", "Razon Social");
		txtRazonSocial.setLength(50);
		txtRazonSocial.setTabIndex(2);
		txtRazonSocial.setLeft(133);
		txtRazonSocial.setTop(34);
	//	txtNombre.setKeyPressFilter("[a-zA-Z\u00F1\u00D1 ]");
		txtRazonSocial.setRequired(true);
		
		TextItem txtNombreComercial = new TextItem("txtNombreComercial", "Nombre Comercial");
		txtNombreComercial.setLength(50);
		txtNombreComercial.setTabIndex(3);
		txtNombreComercial.setLeft(10);
		txtNombreComercial.setTop(34);
		txtNombreComercial.setWidth(250);
		
		TextItem txtCedula = new TextItem("txtCedula", "C\u00E9dula/RUC");
		txtCedula.setTabIndex(1);
		txtCedula.setTooltip("Ingrese la C�dula/RUC");
		txtCedula.setLeft(133);
		txtCedula.setTop(6);
		txtCedula.setHint("Solo numeros");
		txtCedula.setKeyPressFilter("[0-9]");
		txtCedula.setRequired(true); 
		
		TextItem txtDireccion = new TextItem("txtDireccion", "Direcci\u00F3n");
		txtDireccion.setRequired(true);
		txtDireccion.setLength(100);
		
		TextItem txtTelefono = new TextItem("txtTelefono", "Tel\u00E9fono");
		txtTelefono.setTooltip("Tel\u00E9fono");
		txtTelefono.setKeyPressFilter("[0-9]");
		txtTelefono.setLength(15);
		
		TextItem txtObservaciones = new TextItem("txtObservaciones", "Observaciones");
		txtObservaciones.setLength(100);
		
		TextItem txtEmail = new TextItem("txtEmail", "e-mail");
		txtEmail.setTooltip("Ingrese el email del Proveedor");
		txtEmail.setShowHint(false);
		txtEmail.setLength(100);
		txtEmail.setValidators(regExpValidator);
		
		FloatItem txtCupoCredito = new FloatItem();
		txtCupoCredito.setTitle("Cupo Cr\u00E9dito");
		txtCupoCredito.setName("txtCupoCredito");
		txtCupoCredito.setValidators(floatRangeValidator);
		txtCupoCredito.setRequired(true);
		txtCupoCredito.setValue(0.0);
		txtCupoCredito.setVisible(false);
		
		TextItem txtContacto = new TextItem();
		txtContacto.setRequired(true);
		txtContacto.setTitle("Contacto");
		txtContacto.setName("txtContacto");
		txtcupoCreditoC.setTitle("Cupo Cr\u00E9dito Cliente");
		txtcupoCreditoC.setName("txtcupoCreditoC");
		txtcupoCreditoC.setValue("0");
		
		txtmontoCredito.setName("txtMonto");
		txtmontoCredito.setTitle("Monto Cr\u00E9dito Cliente");
		txtmontoCredito.setValidators(floatRangeValidator);
		CheckboxItem chkCliente =new CheckboxItem("chkCliente", "Asignar este Proveedor como Cliente");
		chkCliente.setRedrawOnChange(true);
		txtmontoCredito.setShowIfCondition(new FuncionSI());
		txtcupoCreditoC.setShowIfCondition(new FuncionSI());
		txtmontoCredito.setVisible(false);
		txtmontoCredito.setValue("0");
		txtcupoCreditoC.setVisible(false);
		
		dateFecha_expiracionFactura.setVisible(false);
		txtSerieFactura.setVisible(false);
		txtAutorizacionFactura.setVisible(false);
		txtInicioFactura.setVisible(false);
		txtFinFactura.setVisible(false);
		dateFecha_expiracionRetencion.setVisible(false);
		txtSerieRetencion.setVisible(false);
		txtAutorizacionRetencion.setVisible(false);
		txtInicioRetencion.setVisible(false);
		txtFinRetencion.setVisible(false);
		
		dateFecha_expiracionNota.setVisible(false);
		txtSerieNota.setVisible(false);
		txtAutorizacionNota.setVisible(false);
		txtInicioNota.setVisible(false);
		txtFinNota.setVisible(false);
		
		dynamicForm.setFields(new FormItem[] {txtCedula,txtDireccion, txtRazonSocial, txtNombreComercial,txtContacto,txtTelefono, txtObservaciones, txtEmail, 
				txtCupoCredito,chkCliente
				,txtcupoCreditoC,txtmontoCredito});
		layout.addMember(dynamicForm);
		
		/*
		dynamicFormFactura.setSize("70%", "25%");
		dynamicFormFactura.setIsGroup(true);
		dynamicFormFactura.setGroupTitle("Factura");
		
		dynamicFormFactura.setNumCols(4);
		dateFecha_expiracionFactura.setUseTextField(true);
		dateFecha_expiracionFactura.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_expiracionFactura.setMaskDateSeparator("-");
		dateFecha_expiracionFactura.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_expiracionFactura.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_expiracionFactura.setValue(new Date());
		txtInicioFactura.setValue("0");
		txtFinFactura.setValue("0");
		dynamicFormFactura.setFields(new FormItem[] { txtSerieFactura, txtAutorizacionFactura, txtInicioFactura, dateFecha_expiracionFactura,txtFinFactura});
		dateFecha_expiracionRetencion.setUseTextField(true);
		dateFecha_expiracionRetencion.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_expiracionRetencion.setMaskDateSeparator("-");
		dateFecha_expiracionRetencion.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		dateFecha_expiracionRetencion.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		
		layout.addMember(dynamicFormFactura);
		
		txtInicioRetencion.setValue("0");
		txtFinRetencion.setValue("0");
		dateFecha_expiracionRetencion.setValue(new Date());
		dynamicFormRetencion.setIsGroup(true);
		dynamicFormRetencion.setGroupTitle("Retenciones");
		dynamicFormRetencion.setSize("70%", "25%");
		dynamicFormRetencion.setNumCols(4);
		dynamicFormRetencion.setFields(new FormItem[] { txtSerieRetencion, txtAutorizacionRetencion, txtInicioRetencion,dateFecha_expiracionRetencion,txtFinRetencion});
		
		layout.addMember(dynamicFormRetencion);
		
		
		dynamicFormNota.setSize("70%", "25%");
		dynamicFormNota.setTitle("Nota de Cr�dito");
		dynamicFormNota.setIsGroup(true);
		dynamicFormNota.setGroupTitle("Nota de Cr�dito");
		dynamicFormNota.setNumCols(4);
		txtInicioNota.setValue("0");
		txtFinNota.setValue("0");
		dateFecha_expiracionNota.setUseTextField(true);
		dateFecha_expiracionNota.setValue(new Date());
		dateFecha_expiracionNota.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		dateFecha_expiracionNota.setMaskDateSeparator("/");
		
		dynamicFormNota.setFields(new FormItem[] { txtSerieNota, txtAutorizacionNota, txtInicioNota, dateFecha_expiracionNota,txtFinNota });
		
		layout.addMember(dynamicFormNota);
		
		*/
		Canvas canvas = new Canvas();
		
		Button btnEliminar = new Button("Eliminar");
		canvas.addChild(btnEliminar);
		btnEliminar.moveTo(324, 6);
		layout.addMember(canvas);
		Button btnGrabar = new Button("Grabar");
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(6, 6);
		btnGrabar.setSize("100px", "22px");
		Button btnNuevo = new Button("Nuevo");
		canvas.addChild(btnNuevo);
		btnNuevo.moveTo(112, 6);
		IButton btnModificar = new IButton("Modificar");
		canvas.addChild(btnModificar);
		btnModificar.moveTo(218, 6);
		
		btnNuevo.addClickHandler(new ManejadorBotones("Eliminar"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnModificar.addClickHandler(new ManejadorBotones("Modificar"));
		
		tab.setPane(layout);
		tabSet.addTab(tab);
		
		
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
		
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "5%");
		
		searchForm.setSize("88%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n");
        
        
        TextItem txtBuscarLst=new TextItem();
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerlst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(124);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%","100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        
        btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        
		hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		
		
		tabListado.setPane(layout_1);
		lstProveedor.setSize("100%", "90%");
		lstProveedor.setFields(
				new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), 
						new ListGridField("RazonSocial", "Razon Social",250),new ListGridField("NombreComercial", "Nombre Comercial",250),  
        		new ListGridField("Telefonos", "Tel\u00E9fono",175),new ListGridField("Direccion", "Direcci\u00F3n",400),
        		new ListGridField("CupoCredito", "Cupo de Credito",100),new ListGridField("Contacto", "Contacto",150)});
		layout_1.addMember(lstProveedor);
		lblRegisros.setSize("100%", "4%");
	    layout_1.addMember(lblRegisros);
    	HStack hStack_1 = new HStack();
		IButton btnExportar = new IButton("Exportar");
		btnExportar.addClickHandler(new ManejadorBotones("exportar"));
		hStack_1.addMember(btnExportar);
		layout_1.addMember(hStack_1);
		tabSet.addTab(tabListado);
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().ListJoinPersona("tblproveedors",0, contador, objbacklst);
		lstProveedor.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
	
	}
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			String campo=null;
			if(tabla.equals("Cedula")){
				campo="cedulaRuc";
			}else if(tabla.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(tabla.equals("Razon Social")||tabla.equals("")){
				campo="razonSocial";
			}else if(tabla.equals("Direcci\u00F3n")){
				campo="direccion";
			}
			//validacion de caja de busqueda, debe contener algo a buscar
			 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
				 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			 else if(campo.equals("cedulaRuc")||campo.equals("nombreComercial")||campo.equals("razonSocial")||campo.equals("direccion")){
			
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().findJoin(nombre, "tblproveedors", campo, objbacklst); 
			 }
		}catch(Exception e){
			SC.say(e.getMessage());
		}
			
	}
	public void limpiar(){
		dynamicForm.setValue("txtCedula", "");
		dynamicForm.setValue("txtRazonSocial", "");
		dynamicForm.setValue("txtNombreComercial", "");
		dynamicForm.setValue("txtCupoCredito", "0.0");
		dynamicForm.setValue("txtContacto", "");
		dynamicForm.setValue("txtEmail", "");
		dynamicForm.setValue("txtTelefono", "");
		dynamicForm.setValue("txtDireccion", "");
		dynamicForm.setValue("txtObservaciones", "");
		dynamicForm.setValue("txtSerie", "");
		dynamicForm.setValue("txtAutorizacion", "");
		dynamicFormFactura.setValue("fecha_expiracionFactura","");
		dynamicFormFactura.setValue("txtSerieFactura","");
		dynamicFormFactura.setValue("txtAutorizacionFactura","");
		dynamicFormFactura.setValue("txtInicioFactura","");
		dynamicFormFactura.setValue("txtFinFactura","");
		
		dynamicFormRetencion.setValue("fecha_expiracionRetencion","");
		dynamicFormRetencion.setValue("txtSerieRetencion","");
		dynamicFormRetencion.setValue("txtAutorizacionRetencion","");
		dynamicFormRetencion.setValue("txtInicioRetencion","");
		dynamicFormRetencion.setValue("txtFinRetencion","");
		
		dynamicFormNota.setValue("fecha_expiracionNota","");
		dynamicFormNota.setValue("txtSerieRetencionNota","");
		dynamicFormNota.setValue("txtAutorizacionRetencionNota","");
		dynamicFormNota.setValue("txtInicioRetencionNota","");
		dynamicFormNota.setValue("txtFinRetencionNota","");
	}
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers

	}*/
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("grabar")){
				if(dynamicForm.validate()){
					String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
				//	CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
					
						try{
							////com.google.gwt.user.client.Window.alert("datos");
							String nombre=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
							String razon=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
							String CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
							String contacto=dynamicForm.getItem("txtContacto").getDisplayValue();
							String email=dynamicForm.getItem("txtEmail").getDisplayValue();
							String telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
							String direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
							String observaciones=dynamicForm.getItem("txtObservaciones").getDisplayValue();
							////com.google.gwt.user.client.Window.alert("despues datos");
							
							
							PersonaDTO perDTO=new PersonaDTO(cedula,nombre,razon,direccion,telefono,"" +
									" ",observaciones,email,'1');
							//perDTO.setClienteU(new ClienteDTO(perDTO,Double.parseDouble(montoCredito),Double.parseDouble(cupoCliente)));
							
							perDTO.setProveedorU(new ProveedorDTO(Factum.empresa.getIdEmpresa(),perDTO,contacto,Double.parseDouble(CupoCredito)));
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							//if(inicioF<finF ){
								getService().GrabarProv(perDTO,objback );
								contador=20;
							//}else{
								//SC.say("Error rango de facturas incorrecto...");
							//}
								if(ban){
									ban=false; 
								}
							
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("C\u00E9dula/RUC incorrecto");
					}
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("nuevo")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("Modificar")){
//				//com.google.gwt.user.client.Window.alert("En MODIFICAR personametodo");
				if(dynamicForm.validate()){
					String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
				//	CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
					
						try{
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR cedula valida");
							String razon=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
							String nombre=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
							String CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
							String contacto=dynamicForm.getItem("txtContacto").getDisplayValue();
							String email=dynamicForm.getItem("txtEmail").getDisplayValue();
							String telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
							String direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
							String observaciones=dynamicForm.getItem("txtObservaciones").getDisplayValue();
							String montoCredito=txtmontoCredito.getDisplayValue();
							String cupoCliente=txtcupoCreditoC.getDisplayValue();
							
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR tomados datos");
							int inicioF=0;
							int finF=0;
							String serieF="";
							String autorizacionF="";
							String fechaF="";
							
							int inicioR=0;
							int finR=0;
							String serieR="";
							String autorizacionR="";
							String fechaR="";
							
							int inicioN=0;
							int finN=0;
							String serieN="";
							String autorizacionN="";
							String fechaN="";
//							int inicioF=Integer.parseInt(txtInicioFactura.getDisplayValue());
//							int finF=Integer.parseInt(txtFinFactura.getDisplayValue());
//							String serieF=txtSerieFactura.getDisplayValue();
//							String autorizacionF=txtAutorizacionFactura.getDisplayValue();
//							String fechaF=dateFecha_expiracionFactura.getDisplayValue();
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR datos otros");
//							int inicioR=Integer.parseInt(txtInicioRetencion.getDisplayValue());
//							int finR=Integer.parseInt(txtFinRetencion.getDisplayValue());
//							String serieR=txtSerieRetencion.getDisplayValue();
//							String autorizacionR=txtAutorizacionRetencion.getDisplayValue();
//							String fechaR=dateFecha_expiracionRetencion.getDisplayValue();
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR datos otros 2");
//							int inicioN=Integer.parseInt(txtInicioNota.getDisplayValue());
//							int finN=Integer.parseInt(txtFinNota.getDisplayValue());
//							String serieN=txtSerieNota.getDisplayValue();
//							String autorizacionN=txtAutorizacionNota.getDisplayValue();
//							String fechaN=dateFecha_expiracionNota.getDisplayValue();
							//com.google.gwt.user.client.Window.alert("En MODIFICAR datos otros 3");
							
							PersonaDTO perDTO=new PersonaDTO(cedula,nombre,razon,direccion,telefono,"" +
									" ",observaciones,email,'1');
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR persona dto");
							//perDTO.setClienteU(new ClienteDTO(perDTO,Double.parseDouble(montoCredito),Double.parseDouble(cupoCliente)));
							if(ban){
								ban=false; 
							} 
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR bandera");
							perDTO.setIdPersona(idPersona);
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR idpersona");
							
							perDTO.setProveedorU(new ProveedorDTO(Factum.empresa.getIdEmpresa(),idProveedor,perDTO,contacto,Double.parseDouble(CupoCredito),autorizacionF,serieF,fechaF,inicioF,finF
									,autorizacionR,serieR,fechaR,inicioR,finR,autorizacionN,serieN,fechaN,inicioN,finN));
//							//com.google.gwt.user.client.Window.alert("En MODIFICAR set proveedor");
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							//if(inicioF<finF ){
								getService().modificarPersona(perDTO,objback );
//								//com.google.gwt.user.client.Window.alert("En MODIFICAR modificar persona service");
								contador=20;
							//}else{
								//SC.say("Error rango de facturas incorrecto...");
							//}
							SC.say(String.valueOf(perDTO.getIdPersona()));
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("C\u00E9dula/RUC incorrecto");
					}
				}
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblproveedors",contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblproveedors",contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblproveedors",contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblproveedors",registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblproveedors",contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
			}else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstProveedor);
			}

		}
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
		
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
				buscarL();
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			try{
				dynamicForm.setValue("txtRazonSocial",lstProveedor.getSelectedRecord().getAttribute("RazonSocial"));  
				dynamicForm.setValue("txtNombreComercial",lstProveedor.getSelectedRecord().getAttribute("NombreComercial"));  
				dynamicForm.setValue("txtCedula", lstProveedor.getSelectedRecord().getAttribute("Cedula"));
				dynamicForm.setValue("txtTelefono", lstProveedor.getSelectedRecord().getAttribute("Telefonos"));
				dynamicForm.setValue("txtDireccion", lstProveedor.getSelectedRecord().getAttribute("Direccion"));
				dynamicForm.setValue("txtCupoCredito", lstProveedor.getSelectedRecord().getAttribute("CupoCredito"));
				dynamicForm.setValue("txtContacto", lstProveedor.getSelectedRecord().getAttribute("Contacto"));
				dynamicForm.setValue("txtObservaciones", lstProveedor.getSelectedRecord().getAttribute("observaciones"));
				dynamicForm.setValue("txtEmail", lstProveedor.getSelectedRecord().getAttribute("E-mail"));
				
				dynamicFormFactura.setValue("fecha_expiracionFactura",lstProveedor.getSelectedRecord().getAttribute("fechaCaducidadF"));
				dynamicFormFactura.setValue("txtSerieFactura",lstProveedor.getSelectedRecord().getAttribute("serieF"));
				dynamicFormFactura.setValue("txtAutorizacionFactura",lstProveedor.getSelectedRecord().getAttribute("autorizacionF"));
				dynamicFormFactura.setValue("txtInicioFactura",lstProveedor.getSelectedRecord().getAttribute("inicioF"));
				dynamicFormFactura.setValue("txtFinFactura",lstProveedor.getSelectedRecord().getAttribute("finF"));
				
				dynamicFormRetencion.setValue("fecha_expiracionRetencion",lstProveedor.getSelectedRecord().getAttribute("fechaCaducidadR"));
				dynamicFormRetencion.setValue("txtSerieRetencion",lstProveedor.getSelectedRecord().getAttribute("serieR"));
				dynamicFormRetencion.setValue("txtAutorizacionRetencion",lstProveedor.getSelectedRecord().getAttribute("autorizacionR"));
				dynamicFormRetencion.setValue("txtInicioRetencion",lstProveedor.getSelectedRecord().getAttribute("inicioR"));
				dynamicFormRetencion.setValue("txtFinRetencion",lstProveedor.getSelectedRecord().getAttribute("finR"));
				
				dynamicFormNota.setValue("fecha_expiracionNota",lstProveedor.getSelectedRecord().getAttribute("fechaCaducidadN"));
				dynamicFormNota.setValue("txtSerieRetencionNota",lstProveedor.getSelectedRecord().getAttribute("serieN"));
				dynamicFormNota.setValue("txtAutorizacionRetencionNota",lstProveedor.getSelectedRecord().getAttribute("autorizacionN"));
				dynamicFormNota.setValue("txtInicioRetencionNota",lstProveedor.getSelectedRecord().getAttribute("inicioN"));
				dynamicFormNota.setValue("txtFinRetencionNota",lstProveedor.getSelectedRecord().getAttribute("finN"));
				idPersona=lstProveedor.getSelectedRecord().getAttributeAsInt("idPersona");
				idProveedor=lstProveedor.getSelectedRecord().getAttributeAsInt("idProveedor");
				tabSet.selectTab(0);
			}catch(Exception e){
				SC.say("error en la interfaz");
			}
		}
	}
	private class FuncionSI implements FormItemIfFunction {

        public boolean execute(FormItem item, Object value, DynamicForm frmPantallaEmpleado) {
        	ban=(Boolean) frmPantallaEmpleado.getValue("chkCliente");;
        	return (Boolean)	frmPantallaEmpleado.getValue("chkCliente");
         }
	}
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			SC.say(result);
			fact.cargarcamposcliente(dynamicForm.getItem("txtCedula").getDisplayValue(), 2);
			getService().ListJoinPersona("tblproveedors",0, contador, objbacklst);
			
		}
		
	};
	final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
			
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else{
				lblRegisros.setText(contador+" de "+registros);
			}
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			lstProveedor.setData(listado);
			lstProveedor.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
	final AsyncCallback<PersonaDTO>  objbackb=new AsyncCallback<PersonaDTO>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		
		@Override
		public void onSuccess(PersonaDTO result) {
			if(result!=null){
				dynamicForm.setValue("txtEmail",result.getMail());  
				dynamicForm.setValue("txtRazonSocial",result.getRazonSocial());
				dynamicForm.setValue("txtNombreComercial",result.getNombreComercial());  
				dynamicForm.setValue("txtCedula", result.getCedulaRuc());
				dynamicForm.setValue("txtObservaciones", result.getObservaciones());
				dynamicForm.setValue("txtTelefono", result.getTelefono1()+" / "+ result.getTelefono2());
				dynamicForm.setValue("txtDireccion", result.getDireccion());
				dynamicForm.setValue("txtCupoCredito", result.getProveedorU().getCupoCredito());
				dynamicForm.setValue("txtContacto", result.getProveedorU().getContacto());
				}else{
					SC.say("Elemento No Encontrado");
				}
		}
		
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};
}
