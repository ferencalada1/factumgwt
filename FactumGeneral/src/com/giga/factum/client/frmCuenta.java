package com.giga.factum.client;
import java.util.LinkedHashMap;
import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.layout.HStack;  
//import com.giga.factum.client.DTO.CuentaDTO;
//import com.giga.factum.client.frmCuenta.ManejadorBotones;
//import com.giga.factum.client.regGrillas.CuentaRecords;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.TipoCuentaDTO;
import com.giga.factum.client.regGrillas.CuentaRecords;
import com.giga.factum.client.regGrillas.TipoCuentaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.types.Alignment;

public class frmCuenta extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	ComboBoxItem cmbtipocuenta =new ComboBoxItem("cmbtipocuenta", "Tipo Cuenta");
	IButton btnGrabar = new IButton("Grabar");
	//ComboBoxItem cmbPlancuenta = new ComboBoxItem("cmbPlancuenta", "Plan de Cuenta");
	LinkedHashMap<String, String> MapTCuenta = new LinkedHashMap<String, String>();
	//LinkedHashMap<String,String> MapPCuenta = new LinkedHashMap<String,String>();
	List<String> MapPCuenta = null;
	SearchForm searchFormPadre = new SearchForm();
	final TextItem txtCuenta = new TextItem("txtCuenta", "Nombre");
	TabSet tabCuenta = new TabSet();//sera el que contenga a todas las pesta�as
	ListGrid lstPadre = new ListGrid();
	ListGrid listCuenta = new ListGrid();
	public int pad=0;
	public String tipocuenta;
	TextItem txtBuscarLstPadre = new TextItem("txtBuscarLstPadre", " ");
	TextItem txtBuscar = new TextItem("txtBuscar", "");
	ComboBoxItem cmbBuscarPadre =new ComboBoxItem("cmbBuscarPadre", " ");
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	IButton btnModificar = new IButton("Modificar");
	IButton btnEliminar = new IButton("Eliminar");
	IButton btnNuevo = new IButton("Nuevo");
	
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	String planCuentaID;
	
	frmCuenta()
	{
		getService().getUserFromSession(callbackUser);
	 	setSize("910px", "600px");
	 	final HLayout hLayoutPadre=new  HLayout();
		Tab lTbCuenta_mant = new Tab("Ingreso");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "45%");
	 	txtCuenta.setShouldSaveValue(true);
	 	txtCuenta.setRequired(true);
	 	txtCuenta.setDisabled(false);
	 	
	 	
	 	TextItem txtCodigo = new TextItem("txtCodigo", "C\u00F3digo");
	 	txtCodigo.setRequired(true);
	 	//txtCodigo.setKeyPressFilter("[0-9]");
	 	
	 	cmbtipocuenta.setRequired(true);
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon searchPickerList = new PickerIcon(PickerIcon.SEARCH);
		
		searchPickerList.addFormItemClickHandler(new FormItemClickHandler(){
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				buscarL();
			}
		});

		
	 	
	 	TextItem txtNivel = new TextItem("txtNivel", "Nivel");
	 	txtNivel.setRequired(true);
	 	txtNivel.setDisabled(true);
	 	
	 	txtNivel.setKeyPressFilter("[0-9]");
	 	TextItem txtPadre =new TextItem("txtPadre", "Padre");
	 	txtPadre.setValue(pad);
	 	txtPadre.setDisabled(true);
	 	TextItem txtidCuenta =new TextItem("txtidCuenta", "Id Cuenta");
	 	txtidCuenta.setDisabled(true);
	 	final CheckboxItem chkpadre = new CheckboxItem("chkpadre", "Padre");
	 	
	 	//cmbPlancuenta.setRequired(true);
	 	dynamicForm.setFields(new FormItem[] {txtidCuenta, txtCodigo, txtCuenta, txtNivel, txtPadre, cmbtipocuenta/*, cmbPlancuenta*/,chkpadre  });
	 	txtNivel.setValue("0");
	 	layout.addMember(dynamicForm);
	 	
	 	
	 	hLayoutPadre.setVisible(false);
	 	hLayoutPadre.setSize("100%", "5%");
	 	searchFormPadre.setSize("88%", "100%");
		searchFormPadre.setItemLayout(FormLayoutType.ABSOLUTE);
		searchFormPadre.setNumCols(4);
		
		
		
		txtBuscarLstPadre.setLeft(6);
		txtBuscarLstPadre.setTop(6);
		txtBuscarLstPadre.setWidth(146);
		txtBuscarLstPadre.setHeight(22);
		txtBuscarLstPadre.setIcons(searchPicker);
		txtBuscarLstPadre.setHint("Buscar");
		txtBuscarLstPadre.setShowHintInField(true);
		searchPicker.addFormItemClickHandler(new FormItemClickHandler(){

			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				buscarLPadre();	
			}			
		});
		txtBuscarLstPadre.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					buscarLPadre();				
				}				
			}			
		});
		
		cmbBuscarPadre.setLeft(158);
		cmbBuscarPadre.setTop(6);
		cmbBuscarPadre.setWidth(146);
		cmbBuscarPadre.setHeight(22);
		cmbBuscarPadre.setHint("Buscar Por");
		cmbBuscarPadre.setShowHintInField(true);
		cmbBuscarPadre.setValueMap("C\u00F3digo","Nombre");
		
		searchFormPadre.setFields(new FormItem[] { txtBuscarLstPadre, cmbBuscarPadre});
		hLayoutPadre.addMember(searchFormPadre);
		HStack hStackPadre = new HStack();
		hStackPadre.setSize("12%", "100%");
		TransferImgButton btnSiguienteP = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnteriorP = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicioP = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFinP = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        hStackPadre.addMember(btnInicioP);
        hStackPadre.addMember(btnAnteriorP);
        hStackPadre.addMember(btnSiguienteP);
        hStackPadre.addMember(btnFinP);
        hLayoutPadre.addMember(hStackPadre);
		layout.addMember(hLayoutPadre);
		layout.addMember(lstPadre);
		lstPadre.setSize("100%", "30%");
		
		HLayout hLayout_1 = new HLayout();
		hLayout_1.setSize("100%", "10%");
		
		//hLayout_1.addMember(searchFormPadre);
		
		hLayout_1.addMember(btnGrabar);
		hLayout_1.addMember(btnModificar);
		hLayout_1.addMember(btnEliminar);
		hLayout_1.addMember(btnNuevo);
		
		layout.addMember(hLayout_1);
		lstPadre.setVisible(false);
		lstPadre.addRecordClickHandler(new ManejadorBotones("Padre"));
		lstPadre.addRecordDoubleClickHandler(new ManejadorBotones("Padre"));
	 	lTbCuenta_mant.setPane(layout);
		
		VLayout layoutPadre = new VLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPickerPadre = new PickerIcon(PickerIcon.SEARCH);
		
		
		ListGridField idCuenta = new ListGridField("idCuenta", "Id Cuenta");
		ListGridField nombreCuenta= new ListGridField("nombreCuenta", "Nombre");
	 	ListGridField idTipocuenta= new ListGridField("idTipocuenta", "C\u00F3digo Tipo de cuenta");
	 	ListGridField Padre= new ListGridField("Padre", "Padre");
	 	ListGridField nivel= new ListGridField("nivel", "nivel");
	 	ListGridField codigo= new ListGridField("codigo", "C\u00F3digo");
	 	
	 	
	 	
		
		//lstCuenta.setFields(new ListGridField[] {idCuenta,codigo,nombreCuenta,idTipocuenta,Padre,nivel});asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
		lstPadre.setFields(new ListGridField[] { idCuenta,codigo,nombreCuenta,idTipocuenta,Padre,nivel});
        
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        /*para poner en un hstack los botones de desplazamiento*/
		chkpadre.addChangeHandler(new ChangeHandler() {
	 		public void onChange(ChangeEvent event) {
	 			lstPadre.setVisible(!chkpadre.getValueAsBoolean());
	 			hLayoutPadre.setVisible(!chkpadre.getValueAsBoolean());
	 			
	 		}
	 	});
        
        
	    tabCuenta.addTab(lTbCuenta_mant);
	    
	    Tab tabListado = new Tab("Listado");
	    
	    VLayout vLayoutListado = new VLayout();
	    vLayoutListado.setSize("100%", "100%");
	    
	    HLayout hLayout_2 = new HLayout();
	    hLayout_2.setSize("100%", "5%");
	    
	    SearchForm searchForm = new SearchForm();
	    searchForm.setSize("50%", "100%");
	    searchForm.setMinColWidth(20);
	    searchForm.setNumCols(4);
	    txtBuscar.setLeft(6);
	    txtBuscar.setTop(6);
	    txtBuscar.setWidth(146);
	    txtBuscar.setHeight(22);
	    txtBuscar.setIcons(searchPickerList);
	    txtBuscar.setHint("Buscar");
	    txtBuscar.setShowHintInField(true);
	    txtBuscar.setShowTitle(false);
	    searchPickerList.addFormItemClickHandler(new FormItemClickHandler(){

			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				buscarL();	
			}			
		});
	    
	    
	    txtBuscar.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					buscarL();
				}				
			}			
		});
	    
	    cmbBuscar.setLeft(158);
	    cmbBuscar.setTop(6);
	    cmbBuscar.setWidth(146);
	    cmbBuscar.setHeight(22);
	    cmbBuscar.setHint("Buscar Por");
	    cmbBuscar.setShowHintInField(true);
	    cmbBuscar.setShowTitle(false);
	    cmbBuscar.setValueMap("C\u00F3digo","Nombre");
	    
	    searchForm.setFields(new FormItem[] { txtBuscar, cmbBuscar});
	    hLayout_2.addMember(searchForm);
	    vLayoutListado.addMember(hLayout_2);
	    listCuenta.addRecordDoubleClickHandler(new ManejadorBotones("Buscar"));
	    listCuenta.setFields(new ListGridField[] { idCuenta,codigo,nombreCuenta,idTipocuenta,Padre,nivel});
	    vLayoutListado.addMember(listCuenta);
	    tabListado.setPane(vLayoutListado);
	    tabCuenta.addTab(tabListado);
	    addMember(tabCuenta);
	    btnGrabar.addClickHandler(new ManejadorBotones("grabar"));
	    btnNuevo.addClickHandler(new ManejadorBotones("nuevo"));
	    btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
	    btnModificar.addClickHandler(new ManejadorBotones("modificar"));
       
	}
	public void limpiar(){
		dynamicForm.setValue("cmbtipocuenta"," ");  
		dynamicForm.setValue("txtidCuenta", "");
		dynamicForm.setValue("txtCuenta", " ");
		dynamicForm.setValue("txtNivel", "0");
		dynamicForm.setValue("txtPadre","");
		dynamicForm.setValue("txtCodigo"," ");
		
	}
	public void buscarL(){
		String nombre=txtBuscar.getValue().toString();
		String campo="nombre";
		if(!cmbBuscar.getDisplayValue().equals("")){
			campo=cmbBuscar.getDisplayValue();
		}		
		if(campo.equalsIgnoreCase("nombre") || campo.equalsIgnoreCase("")){
			campo="nombreCuenta";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idCuenta";
		}
		//lblRegisros.setText(contador+" de "+registros);
		getService().listarCuentaLike(nombre, campo, planCuentaID, objbacklst);
	}
	public void buscarLPadre(){
		String nombre=txtBuscarLstPadre.getValue().toString();
		String campo="nombre";
		if(!cmbBuscarPadre.getDisplayValue().equals("")){
			campo=cmbBuscarPadre.getDisplayValue();
		}		
		if(campo.equalsIgnoreCase("nombre") || campo.equalsIgnoreCase("")){
			campo="nombreCuenta";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idCuenta";
		}
		//lblRegisros.setText(contador+" de "+registros);
		getService().listarCuentaLike(nombre, campo, planCuentaID, objbacklstPadre);
	}
	/**
	 * Manejador de Botones para pantalla Cuenta
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("nuevo")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
					String punto=codigo.substring(codigo.length()-1);
					if(!punto.equals(".")){
						try{
							String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
							String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
							String idTipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getValue();
							String TipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getDisplayValue();
							String idPlan=planCuentaID;
							CuentaDTO cuenta=new CuentaDTO();
							cuenta.setIdEmpresa(Factum.empresa.getIdEmpresa());
							cuenta.setNivel(Integer.parseInt(nivel));
							cuenta.setNombreCuenta(nombre);
							cuenta.setPadre((pad));
							cuenta.setTbltipocuenta(new TipoCuentaDTO(Integer.parseInt(idTipoCuenta),TipoCuenta));
							cuenta.setCodigo(codigo);
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().grabarCuenta(cuenta,idPlan,objback);
							contador=20;
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("Ingrese un C\u00F3digo correcto");
					}
				}
			}else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
					String punto=codigo.substring(codigo.length()-1);
					if(!punto.equals(".")){
						try{
							String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
							String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
							String idTipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getValue();
							String TipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getDisplayValue();
							String idcuenta=dynamicForm.getItem("txtidCuenta").getDisplayValue();
							CuentaDTO cuenta=new CuentaDTO();
							cuenta.setIdEmpresa(Factum.empresa.getIdEmpresa());
							cuenta.setIdCuenta(Integer.parseInt(idcuenta));
							cuenta.setNivel(Integer.parseInt(nivel));
							cuenta.setNombreCuenta(nombre);
							cuenta.setPadre((pad));
							cuenta.setTbltipocuenta(new TipoCuentaDTO(Integer.parseInt(idTipoCuenta),TipoCuenta));
							cuenta.setCodigo(codigo);
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().modificarCuenta(cuenta,objback);
							contador=20;
						}catch(Exception e){
							SC.say(e.getMessage());
						}
					}else{
						SC.say("Ingrese un C\u00F3digo correcto");
					}
				}
			}else if(indicador.equalsIgnoreCase("eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	try{
                        		String idcuenta=dynamicForm.getItem("txtidCuenta").getDisplayValue();
                        		String nombre=dynamicForm.getItem("txtCuenta").getDisplayValue();
            					String codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
            					String nivel=dynamicForm.getItem("txtNivel").getDisplayValue();
            					String idTipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getValue();
            					String TipoCuenta=(String) dynamicForm.getItem("cmbtipocuenta").getDisplayValue();
            					getService().buscarCuenta(idcuenta, objbackCuentapad);
            					TipoCuentaDTO tipocuenta =new TipoCuentaDTO(Integer.parseInt(idTipoCuenta),TipoCuenta);
            					CuentaDTO CuentaDTO=new CuentaDTO(Factum.empresa.getIdEmpresa(),tipocuenta,Integer.parseInt(idcuenta), nombre,pad,Integer.parseInt(nivel),codigo); 
            					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
            					getService().eliminarCuenta(CuentaDTO,objback);
            					limpiar();
                				//lstCuenta.removeData(lstCuenta.getSelectedRecord());
                        	}catch(Exception e){
                        		SC.say(e.getMessage());
                        	}
                       } else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
				
			}else if(indicador.equalsIgnoreCase("Buscar")){
				String idCuenta=dynamicForm.getItem("txtCuenta").getDisplayValue();
				getService().buscarCuentaNombre(idCuenta,"nombreCuenta", objbackCuenta);
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCuenta(contador-20,contador, planCuentaID, objbacklstPadre);
					//lblRegisros.setText(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCuenta(contador-20,contador, planCuentaID, objbacklstPadre);
					//lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCuenta(contador-20,contador, planCuentaID, objbacklstPadre);
					//lblRegisros.setText(contador+" de "+registros);
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCuenta(registros-registros%20,registros, planCuentaID, objbacklstPadre);
					//lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarCuenta(contador-20,contador, planCuentaID, objbacklstPadre);
					//lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
				//SC.say("Listar textbox");
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			if(indicador.equalsIgnoreCase("padre")){
				try{
					String codigo=lstPadre.getSelectedRecord().getAttribute("codigo")+".";
					int nivel=Integer.parseInt(lstPadre.getSelectedRecord().getAttribute("nivel"));
					nivel++;
					dynamicForm.setValue("txtCodigo",codigo);
					dynamicForm.setValue("txtNivel",String.valueOf(nivel)); 
					dynamicForm.setValue("txtPadre", lstPadre.getSelectedRecord().getAttribute("nombreCuenta"));
					pad=Integer.parseInt(lstPadre.getSelectedRecord().getAttribute("idCuenta"));
					
				}catch (Exception  e){
					SC.say(e.getMessage());
				}
				
			}
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Buscar")){
				pad=Integer.parseInt(listCuenta.getSelectedRecord().getAttribute("idCuenta"));
				dynamicForm.setValue("cmbtipocuenta",listCuenta.getSelectedRecord().getAttribute("idTipocuenta"));  
				dynamicForm.setValue("txtidCuenta", listCuenta.getSelectedRecord().getAttribute("idCuenta"));
				dynamicForm.setValue("txtCuenta", listCuenta.getSelectedRecord().getAttribute("nombreCuenta"));
				dynamicForm.setValue("txtNivel", listCuenta.getSelectedRecord().getAttribute("nivel"));
				dynamicForm.setValue("txtPadre", listCuenta.getSelectedRecord().getAttribute("nombreCuenta"));
				dynamicForm.setValue("txtCodigo", listCuenta.getSelectedRecord().getAttribute("codigo"));
				tabCuenta.selectTab(0);
				
			}
			
		}

		
			
	}
	/*
	final AsyncCallback<List<PlanCuentaDTO>>  objbacklstPlanCuenta=new AsyncCallback<List<PlanCuentaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			
		}
		@Override
		public void onSuccess(List<PlanCuentaDTO> result) {
			if(result.size()!=0){
				String[] lista =new String[result.size()];
				for(int i=0;i<result.size();i++) {
	            	lista[i]=String.valueOf(result.get(i).getIdPlan());
					//cmbPlancuenta.setValue(String.valueOf(new PlanCuentaRecords((PlanCuentaDTO)result.get(i)).getidPlanCuenta()));
				}
				cmbPlancuenta.setValueMap(lista);
				btnGrabar.setDisabled(false);
			}else{
				btnGrabar.setDisabled(true);
				SC.say("Primero Ingrese un Plan de Cuentas");
				
				
			}
			
		}
		
		
	};
	*/
	final AsyncCallback<List<TipoCuentaDTO>>  objbacklstCuenta=new AsyncCallback<List<TipoCuentaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(List<TipoCuentaDTO> result) {
			MapTCuenta.clear();
            for(int i=0;i<result.size();i++) {
				MapTCuenta.put(String.valueOf(new TipoCuentaRecords((TipoCuentaDTO)result.get(i)).getidTipoCuenta()), 
						new TipoCuentaRecords((TipoCuentaDTO)result.get(i)).getTipoCuenta());
			}
			cmbtipocuenta.setValueMap(MapTCuenta); 
       }
		
	};
		
	final AsyncCallback<List<CuentaDTO>>  objbacklst=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<CuentaDTO> result) {
			getService().numeroRegistroscuenta("Tblcuenta", planCuentaID,objbackI);
			/*if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else{
				lblRegisros.setText(contador+" de "+registros);
			}
			*/
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new CuentaRecords((CuentaDTO)result.get(i)));
			}
			
			listCuenta.setData(listado);
			listCuenta.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
       }
		
	};
	
	final AsyncCallback<List<CuentaDTO>>  objbacklstPadre=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<CuentaDTO> result) {
			getService().numeroRegistroscuenta("Tblcuenta",planCuentaID,objbackI);
			/*if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else{
				lblRegisros.setText(contador+" de "+registros);
			}*/
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new CuentaRecords((CuentaDTO)result.get(i)));
			}
			
			lstPadre.setData(listado);
			lstPadre.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
       }
		
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			
			SC.say("No se conecta con la base "+caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			//getService().listarCuenta(0,20, objbacklst);
			SC.say(result);
			contador=20;
			pad=0;
			getService().listarCuenta(0,2000, planCuentaID,objbacklst);
		 	getService().listarTipoCuenta(0,2000, objbacklstCuenta);
			//getService().listarPlanCuenta(0,2000,objbacklstPlanCuenta);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		 	
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;
			
		}
	};
	final AsyncCallback<CuentaDTO>  objbackCuenta=new AsyncCallback<CuentaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(CuentaDTO result) {
			if(result!=null){
				dynamicForm.setValue("cmbtipocuenta",result.getTbltipocuenta().getNombre());  
				dynamicForm.setValue("txtidCuenta", result.getIdCuenta());
				dynamicForm.setValue("txtCuenta",result.getNombreCuenta());
				dynamicForm.setValue("txtNivel", result.getNivel());
				dynamicForm.setValue("txtPadre", result.getNombreCuenta());
				dynamicForm.setValue("txtCodigo", result.getcodigo());
				pad=result.getPadre();
				tabCuenta.selectTab(0);
				ban=true;
				SC.say("Elemento  encontrado");
				
				
				
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
				pad=0;
			}
		}
	};
	final AsyncCallback<CuentaDTO>  objbackCuentapad=new AsyncCallback<CuentaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(CuentaDTO result) {
			if(result!=null){
				pad=result.getPadre();
				tipocuenta=result.getTbltipocuenta().getNombre();
				ban=true;
				SC.say("Elemento  encontrado");
				
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
				pad=0;
			}
		}
	};
	
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
				planCuentaID=result.getPlanCuenta();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");			 	
			 	getService().listarCuenta(0,2000, planCuentaID,objbacklst);
			 	getService().listarCuenta(0,2000, planCuentaID,objbacklstPadre);
			 	getService().listarTipoCuenta(0,2000, objbacklstCuenta);
				//getService().listarPlanCuenta(0,2000, objbacklstPlanCuenta); 
			}
		}
		public void onFailure(Throwable caught) {
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
		}
	};
	
	
}
