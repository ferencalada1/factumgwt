package com.giga.factum.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.TmpAuxresultadosDTO;
import com.giga.factum.client.regGrillas.EstadoResultadosRecord;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmEstadoResultados extends VLayout{



	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");

	private final ListGrid listGrid = new ListGrid();
	Date fechaFactura;
	String planCuentaID;
	public frmEstadoResultados() {
		getService().getUserFromSession(callbackUser);		
		getService().fechaServidor(callbackFecha);
		txtFechaInicial.setValue(fechaFactura);
		txtFechaFinal.setValue(fechaFactura);
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		
		DynamicForm dynamicForm = new DynamicForm();
		
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal});

		ListGridField codigo =new ListGridField("codigo", "Codigo",70);
		codigo.setAlign(Alignment.CENTER);
		
		ListGridField cuenta =new ListGridField("cuenta", "Nombre Cuenta",280);
		cuenta.setAlign(Alignment.CENTER);
		
		ListGridField resta =new ListGridField("resta", "Resta",80);
		resta.setAlign(Alignment.CENTER);
		
		addMember(dynamicForm); 
		listGrid.setFields(new ListGridField[] {codigo,cuenta,resta });
		listGrid.setShowGridSummary(true);
		listGrid.setShowGroupSummary(true); 
		listGrid.setSize("40%", "85%");
		addMember(listGrid);
		HStack hStack = new HStack();
		IButton btnGenerar = new IButton("Generar");
		btnGenerar.addClickHandler(new ManejadorBotones("generar"));
		hStack.addMember(btnGenerar);
		
		IButton btnExportar = new IButton("Exportar Excel");
		btnExportar.addClickHandler(new ManejadorBotones("ExportarExcel"));
		hStack.addMember(btnExportar);

		addMember(hStack);
	}
	private class ManejadorBotones implements  com.smartgwt.client.widgets.events.ClickHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		public ManejadorBotones(String s){
			indicador=s;
			
		}

		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				//datosfrm();
				HashMap <String,Object> param=new HashMap<String,Object>();
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				param.put("desde", fechaI);
				param.put("hasta", fechaF);
	   			
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				
				getService().ejecutarEstado("EstResultados", param, planCuentaID,Factum.banderaNombreBD,Factum.banderaUsuarioBD,Factum.banderaPasswordBD,listaCallback);
				
				
				
			}else if(indicador.equalsIgnoreCase("ExportarExcel")){
				CreateExelDTO exel=new CreateExelDTO(listGrid);
				}
			
		}
		final AsyncCallback<List<TmpAuxresultadosDTO>>  listaCallback=new AsyncCallback<List<TmpAuxresultadosDTO>>(){

			@Override
			public void onFailure(Throwable caught) {
				SC.say(caught.toString()+ "error");
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}

			@Override
			public void onSuccess(List<TmpAuxresultadosDTO> result) {
				
				if (result.size()>0)
				{	
					for(int i=0;i<result.size();i++) 
					{
						EstadoResultadosRecord record=(new EstadoResultadosRecord((TmpAuxresultadosDTO)result.get(i)));
						listGrid.addData(record);
					}
				}
				else{SC.say("Nose encontraron registros.");}
				listGrid.show();
				listGrid.redraw();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
		};
		
	}
	
	final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
			
		}
		public void onSuccess(Date result) {
			fechaFactura=result;
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
		}
	};
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}

	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
				planCuentaID=result.getPlanCuenta();}
		}
		public void onFailure(Throwable caught) {
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
		}
	};





}
