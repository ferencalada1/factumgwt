package com.giga.factum.client;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Iterator;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.giga.factum.client.Factura;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.giga.factum.client.regGrillas.Producto;
import com.giga.factum.client.regGrillas.TotalesRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;

public class frmReporteCaja extends VLayout{
	frmListClientes listClientes =new frmListClientes("tblclientes");
	frmListVendedor listVendedor= new frmListVendedor();
	//frmProducto listProductos = new frmProducto();
	/*
	 * METODO PARA AGREGAR BOTON EN EL LIST GRID
	 */
	
	ListGrid lstReporteCaja = new ListGrid();
	ListGrid lstTotales = new ListGrid();
	ListProductos listProductos = new ListProductos();
	DynamicForm dynamicForm = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	ListGrid lstVendedor = new ListGrid();
	VLayout layout_1 = new VLayout();
	String idVendedor="";
	String idCliente="";
	String idProducto="";
	Integer rowNumGlobdal = 0;
	//String Tipo="0";
	String Tipo;
	Window winCliente = new Window();  
	Window winVendedor = new Window();
	Window winProducto = new Window();
	Window winFactura1 = new Window();
	TextItem txtCliente =new TextItem("txtCliente", "Cliente");
	TextItem txtVendedor =new TextItem("txtVendedor", "Vendedor");
	TextItem txtProducto=new TextItem("txtProducto", "Producto");
	TextItem txtNumDocumento=new TextItem("txtNumDocumento", "#");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton buttonEnviar=new IButton("Enviar Todo");
	Boolean enviarTodo=false;
	/************************************************************/
	ListGrid lstProductos = new ListGrid();
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	HStack hStack = new HStack();
	HLayout hLayoutPr = new HLayout();
	VentanaEmergente listadoProd ;
	SearchForm searchForm = new SearchForm();
	
	final ComboBoxItem cmbCategoria=new ComboBoxItem("cbmCategoria","Categor\u00EDa");
	final ComboBoxItem cmbUnidad=new ComboBoxItem("cbmUnidad","Unidad");
	final ComboBoxItem cmbMarca=new ComboBoxItem("cbmMarca","Marca");
	final ComboBoxItem cmdBod = new ComboBoxItem("cmbBod", "Bodega");
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	List<CategoriaDTO> listcat=null;
	List<MarcaDTO> listmar=null;
	List<UnidadDTO> listuni=null;
	List<BodegaDTO> listBodega=null;
	List<TipoPrecioDTO> listTipoP=null;
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	int contador=20;
	int registros=0;
	ListGridField SubTotal =new ListGridField("Total", "Total");
	/************************************************************/
	//
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	int banderaPersona=0;
	public frmReporteCaja(String valor) {
		try{
			
			txtFechaInicial.setValue(new Date());
			txtFechaFinal.setValue(new Date());
			searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
			searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar, cmdBod}); 
			lstProductos.addRecordDoubleClickHandler( new ManejadorBotones("Seleccionar"));
			lstProductos.setFields(new ListGridField[] { 
	    			new ListGridField("codigoBarras", "C\u00F3digo de Barras"), 
	    			new ListGridField("descripcion", "Descripci\u00F3n"), 
	    			new ListGridField("stock", "Stock"),
	    			new ListGridField("impuesto", "Impuesto"),
	    			new ListGridField("lifo", "LIFO"),
	    			new ListGridField("fifo", "FIFO"),
	    			new ListGridField("promedio", "Promedio"),
	    			new ListGridField("Unidad", "Unidad"),
	    			new ListGridField("Categoria", "Categoria"),
	    			new ListGridField("Marca", "Marca")});
		dynamicForm.setSize("60px", "20%");
		dynamicForm.setNumCols(4);
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		 
		 
		txtVendedor.addKeyPressHandler( new ManejadorBotones("vendedor"));
		txtProducto.addKeyPressHandler( new ManejadorBotones("producto"));
		txtCliente.addKeyPressHandler( new ManejadorBotones("cliente"));
		
		PickerIcon pckLimpiarCliente = new PickerIcon(PickerIcon.CLEAR);
		txtCliente.setIcons(pckLimpiarCliente);
		pckLimpiarCliente.addFormItemClickHandler(new ManejadorBotones("borrarCliente"));
		PickerIcon pckLimpiarProducto = new PickerIcon(PickerIcon.CLEAR);
		txtProducto.setIcons(pckLimpiarProducto);
		pckLimpiarProducto.addFormItemClickHandler(new ManejadorBotones("borrarProducto"));
		PickerIcon pckLimpiarVendedor = new PickerIcon(PickerIcon.CLEAR);
		txtVendedor.setIcons(pckLimpiarVendedor);
		pckLimpiarVendedor.addFormItemClickHandler(new ManejadorBotones("borrarVendedor"));
		
		
		cmbDocumento.setRequired(true);
		
		if(Factum.banderaMenuProforma==1){
		cmbDocumento.setValueMap("Facturas de Venta",
				"Notas de Venta",
				"Notas de Entrega","Facturas de Compra",
				"Notas de Venta en Compras",
				"Notas de Compra",
				"Notas de Credito","Anticipo Clientes","Ajustes de Inventario","Gastos","Ingresos",
				"Nota de Credito Proveedores","Anticipo Proveedores", "Retencion","Retencion de Compras","Proformas",
				"Ajustes de Inventario Inicial","Consignacion");
		}else{
		cmbDocumento.setValueMap("Facturas de Venta",
				"Notas de Venta",
				"Notas de Entrega","Facturas de Compra",
				"Notas de Venta en Compras",
				"Notas de Compra",
				"Notas de Credito","Anticipo Clientes","Ajustes de Inventario","Gastos","Ingresos",
				"Nota de Credito Proveedores","Anticipo Proveedores", "Retencion","Retencion de Compras",
				"Ajustes de Inventario Inicial","Consignacion");
		}
		FormItem formitem=new FormItem();
		formitem.setShowTitle(false);
		//txtNumDocumento.setVisible(false);
		dynamicForm.setFields(new FormItem[] {txtFechaInicial, formitem ,txtFechaFinal, formitem, txtCliente, formitem, txtVendedor, formitem, txtProducto, formitem, cmbDocumento, txtNumDocumento});
		addMember(dynamicForm);
		
		Label lblIngresosDelDa = new Label("Ingresos del D\u00EDa");
		addMember(lblIngresosDelDa);
		lblIngresosDelDa.setSize("100%", "5%");
		lstReporteCaja = new ListGrid() {
			
			@Override  
            protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {  
				String fieldName = this.getFieldName(colNum); 
                if (fieldName.equals("Estado")) {  
                	ListGridRecord listrecord =  record;  
                    if (listrecord.getAttributeAsString("Estado").equals("CONFIRMADO")) {  
                        return "font-weight:bold; color:#298a08;"; 
                    } else if (listrecord.getAttributeAsString("Estado").equals("NO CONFIRMADO")) {  
                        return "font-weight:bold; color:#287fd6;";  
                    } else if(listrecord.getAttributeAsString("Estado").equals("ANULADA")){  
                    	return "font-weight:bold; color:#d64949;"; 
                    }else{
                    	return super.getCellCSSText(record, rowNum, colNum);  
                    }
                }else if(fieldName.equals("EstadoElectronico")){
                	final String estadosri=record.getAttributeAsString("EstadoElectronico");
                	if(estadosri.equals("GENERADO")){
                		return "font-weight:bold; color:#287fd6;";  
		     		}else if(estadosri.equals("FIRMADO")){
		     			return "font-weight:bold; color:#287fd6;";
		     		}else if(estadosri.equals("RIDE")){
		     			return "font-weight:bold; color:#287fd6;"; 
		     		}else if(estadosri.equals("REGISTRADO")){
		     			return "font-weight:bold; color:#287fd6;";
		     		}else if(estadosri.equals("AUTORIZADO")){
		     			return "font-weight:bold; color:#287fd6;"; 
		     		}else if(estadosri.equals("FINALIZADO")){
		     			return "font-weight:bold; color:#298a08;";  
		     		}else{
		     			return "font-weight:bold; color:#d64949;";    
		     		}
                }
                else {  
                    return super.getCellCSSText(record, rowNum, colNum);   
            	}
			}
			
			 @Override  
		     protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				 String fieldName = this.getFieldName(colNum); 
		         
		         if(fieldName.equals("buttonFieldRIDE")){
		        	 final String tipoemision=record.getAttributeAsString("TipoEmision");
		        	 final String estadosri=record.getAttributeAsString("EstadoElectronico");
		        	 IButton button = new IButton();  
		             button.setHeight(20);  
		             button.setWidth(65);                        
		             button.setTitle("RIDE");  
		             button.addClickHandler(new ClickHandler() {                  
						@Override
						public void onClick(ClickEvent event) {
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
							String baseURL=GWT.getModuleBaseURL();
							String url = baseURL+ "rideServlet?fileName="+record.getAttributeAsString("NombreRIDE")+".pdf&typeDoc="+Tipo+"&SO="+Factum.banderaSO;
							com.google.gwt.user.client.Window.open( url, "", "");
						}  
		             });  
		             if(tipoemision.equals("ELECTRONICO")){
//		            	 if(estadosri.equals("RIDE") || estadosri.equals("ENVIADO")){
//		            		 button.setDisabled(false);
//		            	 }
		            	 if(!estadosri.equals("GENERADO") && !estadosri.equals("FIRMADO")){
		            		 button.setDisabled(false);
		            	 }
		            	 else{
		            		 button.setDisabled(true);
		            	 }
		             }else{
		            	 button.setDisabled(true);
		            	 button.setVisible(false);
		             }
	                 return button;
		        	 
		         }else if (fieldName.equals("buttonFieldSRI")) {  
		        	 final String tipoemision=record.getAttributeAsString("TipoEmision");
		        	 final String estadosri=record.getAttributeAsString("EstadoElectronico");
		        	 final String estadodto=record.getAttributeAsString("Estado");
		             IButton button = new IButton();  
		             button.setHeight(20);  
		             button.setWidth(65);
		             
//		            if(estadosri.equals("GENERADO")){
//		            	 button.setTitle("FIRMAR");  
//		     		}else if(estadosri.equals("FIRMADO")){
//		     			button.setTitle("REGISTRAR"); 
//		     		}else if(estadosri.equals("REGISTRADO")){
//		     			button.setTitle("AUTORIZAR"); 
//		     		}else if(estadosri.equals("AUTORIZADO")){
//		     			button.setTitle("RIDE"); 
//		     		}else if(estadosri.equals("RIDE")){
//		     			button.setTitle("ENVIAR"); 
//		     		}else if(estadosri.equals("ENVIADO")){
//		     			button.setTitle("FINALIZADO"); 
//		     		}else{
//		     			button.setTitle("FACT. ELEC."); 
//		     		}
		             if(estadosri.equals("GENERADO")){
		            	 button.setTitle("FIRMAR");  
		     		}else if(estadosri.equals("FIRMADO")){
		     			button.setTitle("RIDE"); 
		     		}else if(estadosri.equals("RIDE")){
		     			button.setTitle("REGISTRAR"); 
		     		}else if(estadosri.equals("REGISTRADO")){
		     			button.setTitle("AUTORIZAR"); 
		     		}else if(estadosri.equals("AUTORIZADO")){
		     			button.setTitle("FINALIZAR"); 
		     		}else if(estadosri.equals("FINALIZADO")){
		     			button.setTitle("FINALIZADO"); 
		     		}else{
		     			button.setTitle("FACT. ELEC."); 
		     		}
		             
		             button.addClickHandler(new ClickHandler() {                  
						@Override
						public void onClick(ClickEvent event) {
							DtocomercialDTO dtocomercialdto = new DtocomercialDTO();
							int idDtoComercial=record.getAttributeAsInt("id");
							dtocomercialdto.setTipoTransaccion(Integer.valueOf(Tipo));
							dtocomercialdto.setIdDtoComercial(idDtoComercial);
							getService().documentoElectronico(dtocomercialdto,Factum.banderaNombrePlugFacturacionElectronica,Factum.banderaRutaCertificado, 
//									Factum.banderaAmbienteFacturacionElectronica, 
									Factum.banderaSO, objback);
							
						}  
		             });  
		             
			             if(tipoemision.equals("ELECTRONICO")){
			            	 if(estadosri.equals("FINALIZADO")){
			            		 button.setVisible(false);
			            		 button.setDisabled(true);
			            	 }else{
			            		 button.setDisabled(false);
			            	 }
			             }else{
			            	 if(estadodto.equals("CONFIRMADO")){
			            		 button.setDisabled(false);
				            	 button.setVisible(true);
			            	 }else{
			            		 button.setDisabled(true);
				            	 button.setVisible(false);				             
			            	 }			            	 
			             }
		             
		             return button;  
		         } else {  
		             return null;  
		         }  

		    }  
			}; 
		lstReporteCaja.setShowRecordComponents(true);          
		lstReporteCaja.setShowRecordComponentsByCell(true); 
		lstReporteCaja.setSize("100%", "60%");
		lstReporteCaja.setCanDragRecordsOut(true);//Para permitir que los registros se puedan extraer
		lstReporteCaja.setCanAcceptDroppedRecords(true);
		ListGridField id =new ListGridField("id", "id");
		ListGridField NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num. Trans.");
		ListGridField TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
		ListGridField Fecha =new ListGridField("Fecha", "Fecha");		
		ListGridField FechadeExpiracion=new ListGridField("FechadeExpiracion", "Fechade Expiracion");		
		ListGridField Vendedor=new ListGridField("Vendedor", "Vendedor");
		Vendedor.setWidth("8%");
		ListGridField IdVendedor=new ListGridField("idVendedor", "idVendedor");		
		ListGridField Cliente =new ListGridField("Cliente", "Cliente");

		Cliente.setWidth("25%");
		ListGridField IdCliente =new ListGridField("idCliente", "idCliente");		
		ListGridField Autorizacion =new ListGridField("Autorizacion", "Autorizacion");		
		ListGridField NumPagos =new ListGridField("NumPagos", "Num. de Pagos");
		NumPagos.setWidth("5%");
		ListGridField Estado =new ListGridField("Estado", "Estado");
		Estado.setAlign(Alignment.CENTER);
		Estado.setWidth("5%");
		ListGridField EstadoElectronico =new ListGridField("EstadoElectronico", "Estado SRI");
		EstadoElectronico.setAlign(Alignment.CENTER);
		EstadoElectronico.setWidth("5%");
		ListGridField TipoEmision =new ListGridField("TipoEmision", "Emision");
		TipoEmision.setWidth("8%");
		TipoEmision.setAlign(Alignment.CENTER);
		ListGridField NombreRIDE =new ListGridField("NombreRIDE", "Nombre");
		
		ListGridField EnviarElectronico =new ListGridField("buttonFieldSRI", "SRI");
		EnviarElectronico.setWidth("8%");
		EnviarElectronico.setAlign(Alignment.CENTER);
		ListGridField DescargarRIDE =new ListGridField("buttonFieldRIDE", "RIDE");
		DescargarRIDE.setWidth("8%");
		DescargarRIDE.setAlign(Alignment.CENTER);
		DescargarRIDE.setType(ListGridFieldType.IMAGE);
		DescargarRIDE.setImgDir("pdf.png");
		DescargarRIDE.setCanEdit(false);
		DescargarRIDE.setDefaultValue("   ");
		DescargarRIDE.addRecordClickHandler(new ManejadorBotones("Eliminar"));
		
		
		id.setWidth(30);
		NumRealTransaccion.setWidth(30);
		NumRealTransaccion.setAlign(Alignment.CENTER);
		TipoTransaccion.setAlign(Alignment.CENTER);
		Fecha.setAlign(Alignment.CENTER);
		FechadeExpiracion.setHidden(true);
		IdVendedor.setHidden(true);
		IdCliente.setHidden(true);
		Autorizacion.setHidden(true);
		NumPagos.setAlign(Alignment.CENTER);
		EnviarElectronico.setAlign(Alignment.CENTER);
		NombreRIDE.setHidden(true);
		
		SubTotal.setAlign(Alignment.RIGHT);
		SubTotal.setType(ListGridFieldType.FLOAT);
		SubTotal.setWidth("6%");

		
		if(Factum.banderaMenuFacturacionElectronica==0){
			lstReporteCaja.setFields(new ListGridField[] { NumRealTransaccion, Fecha,FechadeExpiracion ,Cliente,
					Autorizacion,NumPagos,Estado,SubTotal, Vendedor,id ,TipoTransaccion,IdCliente,IdVendedor});
//			lstReporteCaja.setFields(new ListGridField[] { id ,TipoTransaccion,NumRealTransaccion, Fecha,FechadeExpiracion , Vendedor,Cliente,
//					Autorizacion,NumPagos,Estado,SubTotal,IdCliente,IdVendedor});
		}else{
			lstReporteCaja.setFields(new ListGridField[] { NumRealTransaccion, Fecha,FechadeExpiracion ,Cliente,
						Autorizacion,NumPagos,Estado,SubTotal,IdCliente,IdVendedor, TipoEmision,EstadoElectronico, EnviarElectronico, DescargarRIDE, NombreRIDE, Vendedor,id ,TipoTransaccion});
//			lstReporteCaja.setFields(new ListGridField[] { id ,TipoTransaccion,NumRealTransaccion, Fecha,FechadeExpiracion , Vendedor,Cliente,
//					Autorizacion,NumPagos,Estado,SubTotal,IdCliente,IdVendedor, TipoEmision,EstadoElectronico, EnviarElectronico, DescargarRIDE, NombreRIDE});
		}
		cmbDocumento.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				String nombreDocumento=event.getValue().toString();
					if(nombreDocumento.equals("Facturas de Venta")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
						//lstReporteCaja.getField("Cliente").setTitle("Cliente");
					}else if(nombreDocumento.equals("Notas de Venta")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
					}else if(nombreDocumento.equals("Notas de Entrega")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
					}else if(nombreDocumento.equals("Notas de Credito")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
					}else if(nombreDocumento.equals("Anticipo Clientes")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
					}else if(nombreDocumento.equals("Facturas de Compra")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
						//lstReporteCaja.getField("Cliente").setTitle("Proveedor");
					}else if(nombreDocumento.equals("Notas de Venta en Compras")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Notas de Compra")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Anticipo Proveedores")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Notas de Credito Proveedores")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Gastos")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Ajustes de Inventario")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}else if(nombreDocumento.equals("Ingresos")){
						txtCliente.setTitle("Cliente/Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente/Proveedor");
					}else if(nombreDocumento.equals("Retencion")){
						txtCliente.setTitle("Cliente");
						lstReporteCaja.setFieldTitle("Cliente", "Cliente");
					}else if(nombreDocumento.equals("Retencion de Compras")){
						txtCliente.setTitle("Proveedor");
						lstReporteCaja.setFieldTitle("Cliente", "Proveedor");
					}
					if(txtCliente.getTitle().equals("Cliente")&& banderaPersona==2){
						txtCliente.setValue("");
						lstReporteCaja.setFieldTitle("Cliente", "");
					}else if(txtCliente.getTitle().equals("Proveedor") && banderaPersona==1){
						txtCliente.setValue("");
						lstReporteCaja.setFieldTitle("Cliente", "");
					}
					if(txtCliente.getTitle().equals("Cliente")){
						banderaPersona=1;
						if(nombreDocumento.equals("Facturas de Venta") || nombreDocumento.equals("Notas de Venta") || nombreDocumento.equals("Notas de Entrega")){
							txtNumDocumento.setVisible(true);
						}else{
							txtNumDocumento.setVisible(false);
						}
					}else if(txtCliente.getTitle().equals("Proveedor")){
						banderaPersona=2;
						if(nombreDocumento.equals("Facturas de Compra") || nombreDocumento.equals("Notas de Venta en Compras") || nombreDocumento.equals("Notas de Compra")){
							txtNumDocumento.setVisible(true);
						}else{
							txtNumDocumento.setVisible(false);
						}
					}
					
					txtCliente.redraw();
				// TODO Auto-generated method stub
				
			}
		});
		
		lstReporteCaja.setShowRowNumbers(true);
		//lstReporteCaja.setShowGridSummary(true);
		lstReporteCaja.sort("NumRealTransaccion", SortDirection.ASCENDING);
		lstReporteCaja.setWidth100();
		addMember(lstReporteCaja);
		Double confirmados=0.0;
		Double anulados=0.0;
		Double sinconfirmados=0.0;

		ListGridField Nombre =new ListGridField("Nombre", "Nombre");
		ListGridField Total =new ListGridField("Total", "Total");
		lstTotales.setFields(new ListGridField[] {Nombre,Total});
		
		ListGridRecord[] lis = new ListGridRecord[2];
		
		
		lis[0]=new TotalesRecords("TOTAL CONFIRMADOS", confirmados);
		lis[1]=new TotalesRecords("TOTAL ANULADOS", anulados);
		lis[2]=new TotalesRecords("TOTAL SIN CONFIRMAR", sinconfirmados);
		lstTotales.setData(lis);
		lstTotales.redraw();
		addMember(lstTotales);
		
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		
		hStack.addMember(btnGenerarReporte);
		btnGenerarReporte.addClickHandler(new ManejadorBotones("generar")) ;
		
		if (Factum.banderaMenuFacturacionElectronica==1){
//			buttonEnviar = new ButtonItem("Enviar Todo");
			buttonEnviar.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					enviarTodo=true;
					for (ListGridRecord record:lstReporteCaja.getRecords()){
							final String tipoemision = record.getAttributeAsString("TipoEmision");
							final String estadosri = record.getAttributeAsString("EstadoElectronico");
							final String estadodto = record.getAttributeAsString("Estado");
							if ((tipoemision.equals("ELECTRONICO") && !estadosri.equals("FINALIZADO"))|| (!tipoemision.equals("ELECTRONICO") && estadodto.equals("CONFIRMADO")) ) {
									System.out.println("****************/////////////////////////////*******************");
									
									DtocomercialDTO dtocomercialdto = new DtocomercialDTO();
									int idDtoComercial=record.getAttributeAsInt("id");
									dtocomercialdto.setTipoTransaccion(Integer.valueOf(Tipo));
									dtocomercialdto.setIdDtoComercial(idDtoComercial);
									System.out.println("****************/////////////////////////////*******************  "
											+ idDtoComercial);
									getService().documentoElectronico(dtocomercialdto,
											Factum.banderaNombrePlugFacturacionElectronica,
											Factum.banderaRutaCertificado, 
//											Factum.banderaAmbienteFacturacionElectronica, 
											Factum.banderaSO, objback);
									
//									DtocomercialDTO dtocomercialdto = new DtocomercialDTO();
//									int idDtoComercial = record.getAttributeAsInt("id");
//									dtocomercialdto.setTipoTransaccion(Integer.valueOf(0));
//									dtocomercialdto.setIdDtoComercial(idDtoComercial);
//									System.out.println("****************/////////////////////////////*******************  "
//											+ idDtoComercial);
//					
//									getService().documentoElectronico(dtocomercialdto,
//											Factum.banderaNombrePlugFacturacionElectronica,
//											Factum.banderaRutaCertificado, Factum.banderaAmbienteFacturacionElectronica,
//											Factum.banderaSO, objback);
									
							}
						
					}
					SC.say("DOCUMENTOS ELECTRONICOS PROCESADOS", "Se han procesado todos los archivos, porfavor presione el boton ACTUALIZAR y revise los estados de los documentos.");
					enviarTodo=false;
				}
			});
//			buttonEnviar.setStartRow(false);
//			buttonEnviar.setEndRow(false);
			
//			buttonActualizar = new ButtonItem("Actualizar");
//			buttonActualizar.addClickHandler(new ClickHandler() {
//				@Override
//				public void onClick(ClickEvent event) {
//					reporte();
//				}
//			});
//			buttonActualizar.setStartRow(false);
//			buttonActualizar.setEndRow(false);
			hStack.addMember(buttonEnviar);
			}
		
		
		
		IButton btnImprimir = new IButton("Imprimir");
		hStack.addMember(btnImprimir);
		btnImprimir.addClickHandler(new ManejadorBotones("imprimir")) ;
		IButton btnLimpiar = new IButton("Limpiar");
		//hStack.addMember(btnLimpiar);
		btnLimpiar.addClickHandler(new ManejadorBotones("limpiar")) ;
		
		IButton btnVerDocumento = new IButton("Ver Documento");
		hStack.addMember(btnVerDocumento);
		btnVerDocumento.addClickHandler(new ManejadorBotones("verDto")) ;
		
		
		
		addMember(hStack);
		
		idCliente="";
		idVendedor="";
		idProducto="";
		txtCliente.setValue("");
		txtVendedor.setValue("");
		txtProducto.setValue("");
		btnGenerarReporte.setDisabled(false);
		//cmbDocumento.setDefaultToFirstOption(true);
		//cmbDocumento.redraw();
		cmbDocumento.setValue(valor);
		identificarDocumento();
		generarReporte();
		//SC.say("Tipo: "+Tipo);
		}catch(Exception e ){SC.say("Error: "+e);}
		
		
	}
	
	 
	
	public void identificarDocumento(){
		if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
			Tipo="0";
		}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
			Tipo="1";
		}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
			Tipo="2";
		}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
			Tipo="3";
		}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
			Tipo="4";
		}else if(cmbDocumento.getDisplayValue().equals("Retencion")){
			Tipo="5";	
		}else if(cmbDocumento.getDisplayValue().equals("Gastos")){
			Tipo="7";
		}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario")){
			Tipo="16";
		}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
			Tipo="10";
		}else if(cmbDocumento.getDisplayValue().equals("Ingresos")){
			Tipo="12";
		}else if(cmbDocumento.getDisplayValue().equals("Facturas de Venta Grandes")){
			Tipo="13";
		}else if(cmbDocumento.getDisplayValue().equals("Nota de Credito Proveedores")){
			Tipo="14";
		}else if(cmbDocumento.getDisplayValue().equals("Anticipo Proveedores")){
			Tipo="15";
		}else if(cmbDocumento.getDisplayValue().equals("Proformas")){
			Tipo="20";
		}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario Inicial")){
			Tipo="17";
		}else if(cmbDocumento.getDisplayValue().equals("Consignacion")){
			Tipo="18";
		}else if(cmbDocumento.getDisplayValue().equals("Retencion de Compras")){
			Tipo="19";
		}else if(cmbDocumento.getDisplayValue().equals("Notas de Venta")){
			Tipo="26";
		}else if(cmbDocumento.getDisplayValue().equals("Notas de Venta en Compras")){
			Tipo="27";
		} 
		
	}
	
	private class ManejadorBotones implements DoubleClickHandler,KeyPressHandler,RecordDoubleClickHandler,FormItemClickHandler, com.smartgwt.client.widgets.events.ClickHandler, RecordClickHandler{
		String indicador="";
		
		
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		 
		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				
				generarReporte();
			}else if(indicador.equalsIgnoreCase("limpiar")){
				idCliente="";
				idVendedor="";
				idProducto="";
				txtCliente.setValue("");
				txtVendedor.setValue("");
				txtProducto.setValue("");
				btnGenerarReporte.setDisabled(false);
			}else if(indicador.equalsIgnoreCase("imprimir")){
				VLayout espacio = new VLayout(); 
				espacio.setSize("100%","5%");
				Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +				  
			   		"&nbsp;&nbsp;&nbsp;Listado de Documentos",espacio,dynamicForm,lstReporteCaja};					   
							
		        Canvas.showPrintPreview(a);
			}else if(indicador.equalsIgnoreCase("verDto")){
				Integer doc=0;
				try{
					doc=Integer.parseInt(lstReporteCaja.getSelectedRecord().getAttributeAsString("id"));
					if (Integer.parseInt(Tipo)!=16 && Integer.parseInt(Tipo)!=17 && Integer.parseInt(Tipo)!=18 
							&& Integer.parseInt(Tipo)!=19 
							&& Integer.parseInt(Tipo)!=7
							){
						winFactura1 = new Window();
		            	winFactura1.setWidth("95%");
						winFactura1.setHeight("95%");
		            	winFactura1.setTitle("Documento Comercial");  
		            	winFactura1.setShowMinimizeButton(false);  
		            	winFactura1.setIsModal(true);  
		            	winFactura1.setShowModalMask(true);  
		            	winFactura1.centerInPage();  
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		            	Factura fac=new Factura(Integer.parseInt(Tipo),doc);//lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
						winFactura1.addCloseClickHandler(new CloseClickHandler() {  
			                public void onCloseClick(CloseClientEvent event) {		                	
			                	generarReporte();
			                	winFactura1.destroy();  
			                }  
			            });
						VLayout form = new VLayout();  
			            form.setSize("100%","100%"); 
			           // form.setPadding(5);  
			           // form.setLayoutAlign(VerticalAlignment.BOTTOM);
			            form.addMember(fac);
			            winFactura1.addItem(form);
			            winFactura1.centerInPage();
			            winFactura1.show();
					}else if (Integer.parseInt(Tipo)!=19 
							&& Integer.parseInt(Tipo)!=7
							){
						winFactura1 = new Window();
		            	winFactura1.setWidth("95%");
						winFactura1.setHeight("95%");
		            	winFactura1.setTitle("Documento Comercial");  
		            	winFactura1.setShowMinimizeButton(false);  
		            	winFactura1.setIsModal(true);  
		            	winFactura1.setShowModalMask(true);  
		            	winFactura1.centerInPage();  
		            	frmAjuste kardex =new frmAjuste(doc.toString());
						winFactura1.addCloseClickHandler(new CloseClickHandler() {  
			                public void onCloseClick(CloseClientEvent event) {		                	
			                	generarReporte();
			                	winFactura1.destroy();  
			                }  
			            });
						VLayout form = new VLayout();  
			            form.setSize("100%","100%"); 
			            form.addMember(kardex);
			            winFactura1.addItem(form);
			            winFactura1.centerInPage();
			            winFactura1.show();
						

					}else 
						if (
							Integer.parseInt(Tipo)!=7
							)	
					{
						winFactura1 = new Window();
		            	winFactura1.setWidth("95%");
						winFactura1.setHeight("95%");
		            	winFactura1.setTitle("Documento Comercial");  
		            	winFactura1.setShowMinimizeButton(false);  
		            	winFactura1.setIsModal(true);  
		            	winFactura1.setShowModalMask(true);  
		            	winFactura1.centerInPage();  
//		            	//com.google.gwt.user.client.Window.alert("En verdto retencion "+doc.toString());
		            	frmRetencionCompra retenc =new frmRetencionCompra(doc.toString(),1);
						winFactura1.addCloseClickHandler(new CloseClickHandler() {  
			                public void onCloseClick(CloseClientEvent event) {		                	
			                	generarReporte();
			                	winFactura1.destroy();  
			                }  
			            });
						VLayout form = new VLayout();  
			            form.setSize("100%","100%"); 
			            form.addMember(retenc);
			            winFactura1.addItem(form);
			            winFactura1.centerInPage();
			            winFactura1.show();
					}
					else{
						winFactura1 = new Window();
		            	winFactura1.setWidth("95%");
						winFactura1.setHeight("95%");
		            	winFactura1.setTitle("Documento Comercial");  
		            	winFactura1.setShowMinimizeButton(false);  
		            	winFactura1.setIsModal(true);  
		            	winFactura1.setShowModalMask(true);  
		            	winFactura1.centerInPage();  
		            	frmGasto gasto =new frmGasto(doc.toString());
						winFactura1.addCloseClickHandler(new CloseClickHandler() {  
			                public void onCloseClick(CloseClientEvent event) {		                	
			                	generarReporte();
			                	winFactura1.destroy();  
			                }  
			            });
						VLayout form = new VLayout();  
			            form.setSize("100%","100%"); 
			            form.addMember(gasto);
			            winFactura1.addItem(form);
			            winFactura1.centerInPage();
			            winFactura1.show();
					}
				}catch(Exception e){
					SC.say("Primero debe seleccionar una fila");
				}
				
			} 
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equalsIgnoreCase("buscar")){
				buscarL();
			}else if(indicador.equalsIgnoreCase("borrarcliente")){
				idCliente="";
				txtCliente.setValue("");
			}else if(indicador.equalsIgnoreCase("borrarproducto")){
				idProducto="";
				txtProducto.setValue("");
			}else if(indicador.equalsIgnoreCase("borrarvendedor")){
				idVendedor="";
				txtVendedor.setValue("");
			}
		} 

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(indicador.equalsIgnoreCase("cliente")){
				winCliente= new Window();
				winCliente.setWidth(930);  
				winCliente.setHeight(610);  
				winCliente.setTitle("Listado Clientes");  
				winCliente.setShowMinimizeButton(false);  
				winCliente.setIsModal(true);  
				winCliente.setShowModalMask(true);  
				winCliente.centerInPage();  
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
            	getService().ListJoinPersona("tblclientes",0,20, objbacklstCliente);
				winCliente.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	idCliente="";
	                	txtCliente.setValue("");
	                	winCliente.destroy();  
	                }  
	            });
				VLayout layout_1 = new VLayout();
				//SC.say("Titulo: "+txtCliente.getTitle()+" Bandera: "+banderaPersona);
				if(txtCliente.getTitle().equals("Cliente")){
					listClientes=new frmListClientes("tblclientes");
					listClientes.lstCliente.addDoubleClickHandler(new ManejadorBotones("Cliente"));
					layout_1.addChild(listClientes);
				}else if(txtCliente.getTitle().equals("Proveedor")){
					listClientes=new frmListClientes("tblproveedors");
					listClientes.lstCliente.addDoubleClickHandler(new ManejadorBotones("Cliente"));
					layout_1.addChild(listClientes);
				}else if(txtCliente.getTitle().equals("Cliente/Proveedor")){
					listClientes=new frmListClientes("tblclientes");
					listClientes.lstCliente.addDoubleClickHandler(new ManejadorBotones("Cliente"));
					layout_1.addChild(listClientes);
					listClientes=new frmListClientes("tblproveedors");
					listClientes.lstCliente.addDoubleClickHandler(new ManejadorBotones("Cliente"));
					layout_1.addChild(listClientes);
				}
				winCliente.addItem(layout_1);
	            winCliente.show();
			}else if(indicador.equalsIgnoreCase("vendedor")){
				winVendedor.setWidth(930);  
				winVendedor.setHeight(610);  
				winVendedor.setTitle("Listado de Vendedores");  
				winVendedor.setShowMinimizeButton(false);  
				winVendedor.setIsModal(true);  
				winVendedor.setShowModalMask(true);  
				winVendedor.centerInPage();  
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().ListEmpleados(0,20, objbacklstEmpleado);
            	winVendedor.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	idVendedor="";
	                	txtVendedor.setValue("");
	                	winVendedor.destroy(); 
	                }   
	            });
            	VLayout layout_1 = new VLayout();
				listVendedor=new frmListVendedor();
				listVendedor.lstEmpleado.addDoubleClickHandler(new ManejadorBotones("Empleado"));
				layout_1.addChild(listVendedor);
				winVendedor.addItem(layout_1);
				winVendedor.show();
            	
				
			}else if(indicador.equalsIgnoreCase("producto")){
				listProductos = new ListProductos();
				listProductos.lstProductos.addDoubleClickHandler( new ManejadorBotones("Producto"));
				winProducto=new Window();
				winProducto.setWidth(930);  
				winProducto.setHeight(610);  
				winProducto.setTitle("Seleccione un producto");  
				winProducto.setShowMinimizeButton(false);  
				winProducto.setIsModal(true);  
				winProducto.setShowModalMask(true);  
				winProducto.centerInPage();  
				winProducto.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	winProducto.destroy();
	                }  
	            });
				VLayout form = new VLayout();  
	            form.setSize("100%","100%"); 
	            form.setPadding(5);  
	            form.setLayoutAlign(VerticalAlignment.BOTTOM);
	            
	            listProductos.setSize("100%","100%"); 
	            form.addMember(listProductos);
	            winProducto.addItem(form);
	            winProducto.show();
			}
			
		}		
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			CargarProducto(lstProductos.getSelectedRecord());
		} 

		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Cliente")){
				idCliente=listClientes.lstCliente.getSelectedRecord().getAttribute("idPersona");
				txtCliente.setValue(
//						listClientes.lstCliente.getSelectedRecord().getAttribute("NombreComercial")+" "+
						listClientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
				winCliente.destroy();
			}else if(indicador.equalsIgnoreCase("Empleado")){
				idVendedor=listVendedor.lstEmpleado.getSelectedRecord().getAttribute("idPersona");
				txtVendedor.setValue(
//						listVendedor.lstEmpleado.getSelectedRecord().getAttribute("NombreComercial")+" "+
						listVendedor.lstEmpleado.getSelectedRecord().getAttribute("RazonSocial"));
				winVendedor.destroy();
			}else if(indicador.equalsIgnoreCase("Producto")){
				idProducto=listProductos.lstProductos.getSelectedRecord().getAttribute("idProducto");
				txtProducto.setValue(listProductos.lstProductos.getSelectedRecord().getAttribute("descripcion"));
				winProducto.destroy();
			}
		}

		@Override
		public void onRecordClick(RecordClickEvent event) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			//String url = ""+"factum\\rideServlet?fileName="+event.getRecord().getAttributeAsString("NombreRIDE")+".pdf&typeDoc="+Tipo;
			String baseURL=GWT.getModuleBaseURL();
			String url = baseURL+ "rideServlet?fileName="+event.getRecord().getAttributeAsString("NombreRIDE")+".pdf&typeDoc="+Tipo+"&SO="+Factum.banderaSO;
			com.google.gwt.user.client.Window.open( url, "", "");
			
		}
		
		
	}
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			String campo=null;
			if(tabla.equals("Cedula")){
				campo="cedulaRuc";
			}else if(tabla.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(tabla.equals("Razon Social")||tabla.equals("")){
				campo="razonSocial";
			}else if(tabla.equals("Direcci\u00F3n")){
				campo="direccion";
			}
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().findJoin(nombre, "tblclientes", campo, objbacklst); 
			
		}catch(Exception e){
			SC.say(e.getMessage());
		}
			
	}
	
	
	
	
	final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			idCliente="";
			idVendedor="";
			idProducto="";
			txtCliente.setValue("");
			txtVendedor.setValue("");
			txtProducto.setValue("");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];	
			Double confirmados=0.0;
			Double anulados=0.0;
			Double sinconfirmados=0.0;
			String mensaje="";
			for(int i=0;i<result.size();i++){
				DtocomercialRecords dtocomercialrecords = new DtocomercialRecords(result.get(i));
				listado[i]=(dtocomercialrecords);				
				Double total = Double.valueOf(dtocomercialrecords.getAttribute("Total")).doubleValue();
				String estadodto=dtocomercialrecords.getAttributeAsString("Estado");
				mensaje=mensaje+".**."+estadodto+"-"+total;
				if(estadodto.equals("CONFIRMADO")){
					confirmados=confirmados+total;
				}else if(estadodto.equals("ANULADA")){
					anulados=anulados+total;
				}else if(estadodto.equals("NO CONFIRMADO")){
					sinconfirmados=sinconfirmados+total;
				}
				
			}			
			//SC.say(mensaje);
			lstReporteCaja.setData(listado);
			ListGridRecord[] lis = new ListGridRecord[2];
			lis[0]=new TotalesRecords("TOTAL CONFIRMADOS",confirmados);
			lis[1]=new TotalesRecords("TOTAL ANULADOS", anulados);
			lis[2]=new TotalesRecords("TOTAL SIN CONFIRMAR", sinconfirmados);
			lstTotales.setData(lis);
			lstTotales.redraw();
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        //btnGenerarReporte.setDisabled(true);
	    }
	};
	final AsyncCallback<List<PersonaDTO>>  objbacklstCliente=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
		
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			listClientes.lstCliente.setData(listado);
			listClientes.lstCliente.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	final AsyncCallback<List<PersonaDTO>>  objbacklstEmpleado=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
			
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				if(result.get(i).getEstado()=='1'){
					listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
				}
			}
			lstVendedor.setData(listado);
			lstVendedor.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
	final AsyncCallback<List<CategoriaDTO>>  objbacklstCat=new AsyncCallback<List<CategoriaDTO>>(){

 		public void onFailure(Throwable caught) {
 			SC.say(caught.toString());
 			
 		}

 		public void onSuccess(List<CategoriaDTO> result) {
 			listcat=result;
 			MapCategoria.clear();
 			MapCategoria.put("","");
             for(int i=0;i<result.size();i++) {
             	MapCategoria.put(String.valueOf(result.get(i).getIdCategoria()), 
 						result.get(i).getCategoria());
 			}
             cmbCategoria.setValueMap(MapCategoria); 
 		}
 		
 	};
 	final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
		
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			listClientes.lstCliente.setData(listado);
			listClientes.lstCliente.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
 	final AsyncCallback<List<MarcaDTO>>  objbacklstMarca=new AsyncCallback<List<MarcaDTO>>(){

 		public void onFailure(Throwable caught) {
 			// TODO Auto-generated method stub
 			SC.say(caught.toString());
 			SC.say("Error no se conecta  a la base");
 			
 		}
 		public void onSuccess(List<MarcaDTO> result) {
 			listmar=result;
 			MapMarca.clear();
 			MapMarca.put("","");
             for(int i=0;i<result.size();i++) {
             	MapMarca.put(String.valueOf(result.get(i).getIdMarca()), 
 						result.get(i).getMarca());
 			}
             cmbMarca.setValueMap(MapMarca); 
             
 		}
 	};
 	final AsyncCallback<List<UnidadDTO>>  objbacklstUnidad=new AsyncCallback<List<UnidadDTO>>(){

 		public void onFailure(Throwable caught) {
 			// TODO Auto-generated method stub
 			SC.say(caught.toString());
 			SC.say("Error no se conecta  a la base");
 		}

 		public void onSuccess(List<UnidadDTO> result) {
 			listuni=result;
 			MapUnidad.clear();
 			MapUnidad.put("","");
             for(int i=0;i<result.size();i++) {
             	MapUnidad.put(String.valueOf(result.get(i).getIdUnidad()), 
 						result.get(i).getUnidad());
 			}
             cmbUnidad.setValueMap(MapUnidad); 
 		}
 	};

 	final AsyncCallback<List<TipoPrecioDTO>>  objbacklstTip=new AsyncCallback<List<TipoPrecioDTO>>(){

 		public void onFailure(Throwable caught) {
 			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
 			SC.say("Error no se conecta  a la base");
 			
 		}
 		public void onSuccess(List<TipoPrecioDTO> result) {
 			listTipoP=result;
 			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
 		}
 		
 	};

 	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 			
 		}
 		public void onSuccess(Integer result) {
 			registros=result;
 			//SC.say("Numero de registros "+registros);
 		}
 	};
 	
 	
 	public void CargarProducto(ListGridRecord registro) {
		// Cargo el producto a la grilla	
		
 		idProducto = registro.getAttributeAsString("idProducto");
		txtProducto.setValue(registro.getAttributeAsString("descripcion"));
		listadoProd.destroy();
		//mostrarBodegas(registro.getAttributeAsString("idProducto"));
	}
 	
 	final AsyncCallback<List<BodegaDTO>>  objbacklstBod=new AsyncCallback<List<BodegaDTO>>(){

 		public void onFailure(Throwable caught) {
 			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
 			SC.say("Error no se conecta  a la base");
 			
 		}
 		public void onSuccess(List<BodegaDTO> result) {
 			MapBodega.clear();
 			MapBodega.put("","");
             listBodega=result;
 			for(int i=0;i<result.size();i++) {
             	MapBodega.put(String.valueOf(result.get(i).getIdBodega()), 
 						result.get(i).getBodega());
 			}
             cmdBod.setValueMap(MapBodega);
 		}
 		
 	};
 	
 	public void generarReporte(){
 		String fechaI=txtFechaInicial.getDisplayValue();
		String fechaF=txtFechaFinal.getDisplayValue();
		identificarDocumento();				
		if(txtNumDocumento.getDisplayValue().isEmpty()){
		if(!idCliente.equals("") && !idVendedor.equals("") && idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");					
				getService().listarFacturasVendedorCliente(fechaI, fechaF, idCliente, idVendedor,Tipo, listaCallback);					
		}else if(idCliente.equals("") && idVendedor.equals("") && idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturas(fechaI, fechaF,Tipo, listaCallback);
		}else if(!idCliente.equals("") && idVendedor.equals("") && idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");			
			getService().listarFacturasCliente(fechaI, fechaF,idCliente,Tipo, listaCallback);										
		}else if(idCliente.equals("") && !idVendedor.equals("") && idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturasVendedor(fechaI, fechaF,idVendedor,Tipo, listaCallback);
		}else if(idCliente.equals("") && idVendedor.equals("") && !idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturasProducto(fechaI, fechaF, idProducto+"", Tipo, listaCallback);
		}if(!idCliente.equals("") && !idVendedor.equals("") && !idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");				
			getService().listarFacturasVendedorClienteProducto(fechaI, fechaF, idCliente, idVendedor, idProducto, Tipo, listaCallback);						
		}else if(!idCliente.equals("") && idVendedor.equals("") && !idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturasClienteProducto(fechaI, fechaF,idCliente, idProducto,Tipo, listaCallback);
		}else if(idCliente.equals("") && !idVendedor.equals("") && !idProducto.equals("")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturasVendedorProducto(fechaI, fechaF,idVendedor, idProducto,Tipo, listaCallback);
		}
		}else{
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");			
			getService().listarFacturasNumDto(fechaI, fechaF,Tipo,txtNumDocumento.getDisplayValue(),listaCallback);
		}
	
 	}
 	
 	final AsyncCallback<String> objback = new AsyncCallback<String>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

		public void onSuccess(String result) {
			if (!result.isEmpty() && !enviarTodo) {
				SC.say(result);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
						"display", "none");

			}

		}

	};
 	
 	
}
