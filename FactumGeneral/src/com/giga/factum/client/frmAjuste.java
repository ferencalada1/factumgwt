package com.giga.factum.client;

import java.util.HashMap;
import java.util.Map;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmAjuste extends VLayout{
 public frmAjuste(String id, String vend){
	 HTMLPane htmlPane = new HTMLPane();
		htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/InventarioInicialConsignacion/Inventario/page");
		//?id="+id
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		map.put("vend", vend);
		htmlPane.setContentsURLParams(map);
		htmlPane.setContentsType(ContentsType.PAGE);
		addMember(htmlPane );
 }
 public frmAjuste(String id){
	 HTMLPane htmlPane = new HTMLPane();
		htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/InventarioInicialConsignacion/Inventario/page");
		//?id="+id
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		map.put("vend", "0");
		htmlPane.setContentsURLParams(map);
		htmlPane.setContentsType(ContentsType.PAGE);
		addMember(htmlPane );
 }
}
