package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;

import java.util.List;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.gargoylesoftware.htmlunit.javascript.host.Document;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.layout.client.Layout;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.layout.LayoutData;
import com.itextpdf.text.Anchor;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.VLayout;
//import com.google.gwt.user.client.Window;

public class frmListadoBasico extends VLayout{
	ListProductos listProductos = new ListProductos();
	DynamicForm dynamicForm = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtProducto = new TextItem("txtProducto", "Buscar Producto");
	ListGrid lstDocumento = new ListGrid();
	VentanaEmergente listadoProd ;
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnExportar = new IButton("Exportar Excel");
	
	
	String idProducto;
	Window winProducto;  
	Window winFactura1;  
	
	public frmListadoBasico() {
		
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/ListadoProductos/ListadoProductos/page");  
		htmlPane.setContentsType(ContentsType.PAGE);
		addMember(htmlPane );
	}
	
}
