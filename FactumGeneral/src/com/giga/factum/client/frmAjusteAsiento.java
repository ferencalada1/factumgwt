package com.giga.factum.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.internal.compiler.parser.NLSTag;

import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.CuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.LetrasANumeros;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaDTO;
import com.giga.factum.client.DTO.MovimientoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.regGrillas.AjusteAsientoRecord;
import com.giga.factum.client.regGrillas.CuentaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmAjusteAsiento extends VLayout{
	//CABECERA
	DynamicForm frmCabecera = new DynamicForm();
	DateItem txtFechaEmision = new DateItem("fechaEmision","FECHA&nbspEMISI\u00D3N");
	//ComboBoxItem cmbTipoRegistro=new ComboBoxItem("tipoRegistro","TIPO REGISTRO");
	SelectItem sltTipoRegistro = new SelectItem("tipoRegistro","TIPO REGISTRO");
	TextItem txtNumeroComprobante = new TextItem("numeroComprobante", "NUM. COMPROBANTE");
	Integer Tipo_Registro=0;
	//CUERPO
		Integer rowNumGlobal=0;
		//TEXTAREA DE OBSERVACIONES
		DynamicForm dfDescripcion = new DynamicForm();
		TextAreaItem txtaConcepto = new TextAreaItem();
		//LISTGRID DE CUENTAS
		ListGrid listAsiento = new ListGrid();
	//PIE
	DynamicForm frmBotones = new DynamicForm();
	ButtonItem btnGuardar = new ButtonItem("Guardar", "Guardar");
	//VENTANAS EXTRAS
	PickerIcon searchPicker;
	ListGridField idAsiento = new ListGridField("idAsiento", "Asiento");
	ListGridField idCuentaAsiento= new ListGridField("idCuentaAsiento", "Cuenta");
	ListGridField nombreCuentaAsiento= new ListGridField("nombreCuentaAsiento", "Nombre Cuenta");		
 	ListGridField valorDebeAsiento= new ListGridField("valorDebeAsiento", "DEBE");
 	ListGridField valorHaberAsiento= new ListGridField("valorHaberAsiento", "HABER");
 	ListGridField lgfEliminar = new ListGridField("eliminarAsiento", " ", 25);
	VLayout layout_1 = new VLayout();
	HStack hStack = new HStack();
	HLayout hLayoutPr = new HLayout();
	VentanaEmergente listadoCuenta;
	TextItem txtBuscarLstPadre = new TextItem("txtBuscarLstPadre", " ");
	ComboBoxItem cmbBuscarPadre =new ComboBoxItem("cmbBuscarPadre", " ");	
	SearchForm searchFormPadre = new SearchForm();
	ListGrid lstPadre = new ListGrid();
	int contador=20;
	int registros=0;
	//FUNCIONES EXTRA
	Double debe;
	Double haber;
	Integer id;
	Integer idVendedor=0;
	Integer tipoUsuario = 0;
	Integer idDto = null;
	String responsable = null;
	Integer Numero_factura=0;
	String planCuentaID;
	Set<DtoComDetalleDTO> DtoDetalles = null;
	Set<RetencionDTO> tblRetenciones=null;
	Set<TblpagoDTO> PagosDTO = null;
	Set<DtocomercialTbltipopagoDTO> PagosAsociados = null;
	Set<MovimientoDTO> Movimiento = null;
	List<MovimientoCuentaPlanCuentaDTO> MovimientoCuentaPlanCuenta=null;
	public frmAjusteAsiento() {
		Cabecera();
		Cuerpo();
		Pie();
		getService().getUserFromSession(callbackUser);
		obtener_UltimoDto("where TipoTransaccion = '1' ", "idDtoComercial");
	}
	
	public void Cabecera(){
		LinkedHashMap<String, String> linkedhashmaptiporegistro = new LinkedHashMap<String, String>();
		linkedhashmaptiporegistro.put("25", "INGRESO BANCOS");
		linkedhashmaptiporegistro.put("22", "COMPRAS");
		linkedhashmaptiporegistro.put("23", "COMPRAS DE INVENTARIO");
		linkedhashmaptiporegistro.put("24", "EGRESOS DE BANCOS");
		sltTipoRegistro.setValueMap(linkedhashmaptiporegistro);
		sltTipoRegistro.addChangedHandler(new ManejadorBotones("tiporegistro"));
		
		txtNumeroComprobante.setRequired(true);
		
		txtFechaEmision.setRequired(true);
		txtFechaEmision.setUseTextField(true);
		txtFechaEmision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaEmision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaEmision.setValue(new Date());
	
		frmCabecera.setNumCols(3);
		frmCabecera.setFields(new FormItem[]{txtFechaEmision, sltTipoRegistro, txtNumeroComprobante});
		this.addMember(frmCabecera);
		
	}
	
	public void Cuerpo(){
		//TEXTAREA DE OBSERVACIONES
		rowNumGlobal = 0;
		dfDescripcion = new DynamicForm();
		txtaConcepto.setTitle("Descripci\u00f3n");
		dfDescripcion.setHeight("150px");
		dfDescripcion.setWidth100();
		dfDescripcion.setFields(txtaConcepto);
		txtaConcepto.setHeight("100%");
		txtaConcepto.setWidth("100%");
		txtaConcepto.setRequired(true);
		this.addMember(dfDescripcion);
		//LISTGRID DE LOS ASIENTOS
		idAsiento = new ListGridField("idAsiento", "Asiento");
		idCuentaAsiento= new ListGridField("idCuentaAsiento", "Cuenta");
		nombreCuentaAsiento = new ListGridField("nombreCuentaAsiento", "Nombre Cuenta");		
		valorDebeAsiento = new ListGridField("valorDebeAsiento", "DEBE");	 	
	 	valorHaberAsiento = new ListGridField("valorHaberAsiento", "HABER");
	 	idAsiento.setCanEdit(false);
	 	idCuentaAsiento.setCanEdit(false);
	 	lgfEliminar = new ListGridField("eliminarAsiento", " ", 25);	 	
	 	lgfEliminar.setAlign(Alignment.CENTER);
		lgfEliminar.setType(ListGridFieldType.IMAGE);
		lgfEliminar.setImgDir("delete.png");
		lgfEliminar.setCanEdit(false);
		lgfEliminar.setDefaultValue("   ");
		lgfEliminar.addRecordClickHandler(new ManejadorGrid("eliminarAsiento"));
	 	nombreCuentaAsiento .addEditorExitHandler(new ManejadorGrid("nombreCuenta"));
	 	nombreCuentaAsiento .addChangeHandler(new ManejadorGrid("nombreCuenta"));
	 	valorDebeAsiento .setType(ListGridFieldType.FLOAT);
	 	valorHaberAsiento .setType(ListGridFieldType.FLOAT);
	 	valorDebeAsiento .addEditorExitHandler(new ManejadorGrid("valorDebe"));
	 	valorHaberAsiento .addEditorExitHandler(new ManejadorGrid("valorHaber"));	 	
	 	listAsiento = new ListGrid();
		listAsiento.setCanEdit(true);
		listAsiento.setModalEditing(true);
		listAsiento.setAutoSaveEdits(true);
		listAsiento.setWidth("100%");
		listAsiento.setEditEvent(ListGridEditEvent.DOUBLECLICK);
		listAsiento.setFields(new ListGridField[] { idAsiento,idCuentaAsiento ,nombreCuentaAsiento ,valorDebeAsiento ,valorHaberAsiento , lgfEliminar});
		listAsiento.setCanFocus(true);	
		this.addMember(listAsiento);	
		listAsiento.startEditingNew();
		listAsiento.draw();
		listAsiento.saveAllEdits();		
		//LISTGRID DE CUENTAS
		lstPadre= new ListGrid();
		ListGridField idCuenta = new ListGridField("idCuenta", "Id Cuenta");
		ListGridField nombreCuenta= new ListGridField("nombreCuenta", "Nombre");
	 	ListGridField idTipocuenta= new ListGridField("idTipocuenta", "C\u00F3digo Tipo de cuenta");
	 	ListGridField Padre= new ListGridField("Padre", "Padre");
	 	ListGridField nivel= new ListGridField("nivel", "nivel");
	 	ListGridField codigo= new ListGridField("codigo", "C\u00F3digo");	 		
		lstPadre.setFields(new ListGridField[] { idCuenta,codigo,nombreCuenta,idTipocuenta,Padre,nivel});
		lstPadre.setHeight100();
		lstPadre.setWidth100();
		lstPadre.addRecordDoubleClickHandler(new ManejadorGrid("Seleccionar"));
		searchFormPadre.setItemLayout(FormLayoutType.ABSOLUTE);
		searchFormPadre.setNumCols(4);
		txtBuscarLstPadre.setLeft(6);
		txtBuscarLstPadre.setTop(6);
		txtBuscarLstPadre.setWidth(146);
		txtBuscarLstPadre.setHeight(22);
		txtBuscarLstPadre.setIcons(searchPicker);
		txtBuscarLstPadre.setHint("Buscar");
		txtBuscarLstPadre.setShowHintInField(true);
		searchPicker = new PickerIcon(PickerIcon.SEARCH);
		searchPicker.addFormItemClickHandler(new FormItemClickHandler(){
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				buscarL();				
			}			
		});
		txtBuscarLstPadre.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")) {
					buscarL();				
				}				
			}			
		});
		
		cmbBuscarPadre.setLeft(158);
		cmbBuscarPadre.setTop(6);
		cmbBuscarPadre.setWidth(146);
		cmbBuscarPadre.setHeight(22);
		cmbBuscarPadre.setHint("Buscar Por");
		cmbBuscarPadre.setShowHintInField(true);
		cmbBuscarPadre.setValueMap("C\u00F3digo","Nombre");		
		searchFormPadre.setFields(new FormItem[] { txtBuscarLstPadre, cmbBuscarPadre});		
	}
	
	public void Pie(){
		frmBotones = new DynamicForm();
		btnGuardar = new ButtonItem("Guardar", "Guardar");
		btnGuardar.addClickHandler(new  ManejadorBotones("Guardar"));
		frmBotones.setFields(new FormItem[]{btnGuardar});
		addMember(frmBotones);
	}
	/*
	 * METODOS DE BUSQUEDA
	 */
		public void buscarL(){
		String nombre=txtBuscarLstPadre.getValue().toString();
		String campo="nombre";
			if(!cmbBuscarPadre.getDisplayValue().equals("")){
				campo=cmbBuscarPadre.getDisplayValue();
			}		
			if(campo.equalsIgnoreCase("nombre") || campo.equalsIgnoreCase("")){
				campo="nombreCuenta";
			}else if(campo.equalsIgnoreCase("C\u00F3digo")){
				campo="idCuenta";
			}
		//lblRegisros.setText(contador+" de "+registros);
		getService().listarCuentaLike(nombre, campo, planCuentaID,objbacklst);
		}
	/*
	 * MAANEJADOR DE EVENTOS
	 */
	private class ManejadorBotones implements ClickHandler , ChangedHandler{
		private String indicador;
		public ManejadorBotones(String indicador){
			this.indicador=indicador;
		}
		@Override
		public void onClick(ClickEvent event) {		
			if(indicador.equals("Guardar")){
				if(frmCabecera.validate() && rowNumGlobal>0 && dfDescripcion.validate()){
					calculo_Totales();
					if(debe.compareTo(haber)==0){	
						//SC.say("entro  guardar "+idVendedor);
						PersonaDTO vendedor = new PersonaDTO();
						vendedor.setIdPersona(idVendedor);
						DtoDetalles = new HashSet<DtoComDetalleDTO>(0);
						tblRetenciones = new HashSet<RetencionDTO>(0);
						PagosDTO = new HashSet<TblpagoDTO>(0);
						Movimiento = new HashSet<MovimientoDTO>(0);
						PersonaDTO facturaPor = new PersonaDTO();
						facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
						DtocomercialDTO dto = new DtocomercialDTO(
								Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento(),Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision(),
								idDto, vendedor,
								vendedor,
								facturaPor,
								Numero_factura,
								Tipo_Registro,// Tipo de transaccion
								txtFechaEmision.getDisplayValue(),
								txtFechaEmision.getDisplayValue(),// new Date(),
																	// //fecha
																	// expiracion ????
								txtaConcepto.getValue().toString(), 0,
								'1',// estado, //aqui cambie
								0, DtoDetalles,
								tblRetenciones, // Set tblretencions
								PagosDTO, // Set tblpagos
								null,// Set tblmovimientos
								0, 0, 0, PagosAsociados,
								0, txtNumeroComprobante.getDisplayValue(), 0, responsable, 0);	
						//SC.say("finalizo  guardar");
					//SC.say(txtNumeroComprobante.getDisplayValue());
					getService().grabarDtomovimiento(dto, MovimientoCuentaPlanCuenta, objbackDtoMovimiento);
						 
					}else{
						SC.say("La suma del debe y haber deben ser iguales. "+debe+" -"+haber);
					}
			}	
		
		}
	}
		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("tiporegistro")){
				String values =event.getValue().toString();				
				//SC.say("values: "+ values);
				Tipo_Registro = Integer.valueOf(values);
			}
			
		}
	}
	private class ManejadorGrid implements RecordDoubleClickHandler, EditorExitHandler, ChangeHandler, RecordClickHandler{
		private String indicador;
		public ManejadorGrid(String indicador){
			this.indicador= indicador;			
	}
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equals("Seleccionar")){				
				Record recordcuenta=event.getRecord();
				//SC.say(recordcuenta.getAttribute("idCuenta"));
				Map<String, Object> resultMap = new HashMap<String, Object>();// Utils.getMapFromRow(dsFields,
				resultMap.put("idCuentaAsiento",recordcuenta.getAttribute("idCuenta"));
				resultMap.put("nombreCuentaAsiento",recordcuenta.getAttributeAsString("nombreCuenta"));
				resultMap.put("idAsiento", rowNumGlobal + 1);	
				resultMap.put("valorDebeAsiento","0");
				resultMap.put("valorHaberAsiento","0");							
				listAsiento.setEditValues(rowNumGlobal, resultMap);
				listAsiento.refreshRow(rowNumGlobal);
				listAsiento.refreshCell(rowNumGlobal, 0);
				listAsiento.startEditingNew();
				listAsiento.startEditing(rowNumGlobal, 3, false);
				listAsiento.saveAllEdits();
				listAsiento.redraw();
				listadoCuenta.destroy();
				rowNumGlobal += 1;
				//calculo_Totales();
			}			
		}

		@Override
		public void onEditorExit(EditorExitEvent event) {
			if(indicador.equals("valorDebeAsiento")){
				event.getRecord().setAttribute("valorHaberAsiento", 0.0);
			}else if(indicador.equals("valorHaberAsiento")){
				event.getRecord().setAttribute("valorDebeAsiento", 0.0);
			}
			
		}

		@Override
		public void onChange(
				com.smartgwt.client.widgets.grid.events.ChangeEvent event) {
			if(indicador.equals("nombreCuenta")){
				//SC.say("Entro evento CHANGED");
				listarCuentaPlanCuenta();
			 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");	 	
			 	
			}
			
		}

		@Override
		public void onRecordClick(RecordClickEvent event) {
			if(indicador.equals("eliminarAsiento")){
				try {
					// grdProductos.refreshFields();
					listAsiento.removeData(event.getRecord());
					int numProductos = listAsiento.getRecords().length;
					for (int j = 0; j < numProductos; j++) {
						listAsiento.setEditValue(j, "idAsiento", j + 1);
					}
					listAsiento.refreshFields();
					rowNumGlobal -= 1;
				} catch (Exception e) {
				}				
			}
			// TODO Auto-generated method stub			
		}			
	}
	
	/*
	 * EVENTOS ASINCRONOS
	 */
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
	final AsyncCallback<List<CuentaDTO>>  objbacklst=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<CuentaDTO> result) {
			
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new CuentaRecords((CuentaDTO)result.get(i)));
			}
			
			lstPadre.setData(listado);
			lstPadre.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");			
       }		
	};
	
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;		
		}
	};
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {
		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
				idVendedor = result.getIdUsuario();
				tipoUsuario = result.getNivelAcceso();
				responsable = result.getUserName();
				planCuentaID=result.getPlanCuenta();
			}
		}
		public void onFailure(Throwable caught) {
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			tipoUsuario = null;
			SC.say("No se puede obtener el nombre de usuario");
		}
	};
	
	final AsyncCallback<String>  objbackDtoMovimiento=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(String result) {
			SC.say(result);		
			Limpiar();
		}
	};

		
	
	/*
	 * VENTANAS ADICIONALES
	 */
	public void listarCuentaPlanCuenta(){	
		//SC.say("Entro ventana");
		
		rowNumGlobal = listAsiento.getEditRow();
		getService().listarCuentaPlanCuentas(planCuentaID, objbacklst);	
		
		
		layout_1= new VLayout();
		hLayoutPr= new HLayout();
		hStack= new HStack();
		
		layout_1.setSize("100%", "100%");
		hLayoutPr.setSize("100%", "6%");
		hStack.setSize("12%", "100%");
		
		searchFormPadre.setSize("85%", "100%");
		
		
		lstPadre.setAutoFitData(Autofit.VERTICAL);
		lstPadre.setAutoFitMaxRecords(10);
		lstPadre.setAutoFetchData(true);
		lstPadre.setSize("100%", "80%");
		
		hLayoutPr.addMember(searchFormPadre);
		hLayoutPr.addMember(hStack);
		layout_1.addMember(hLayoutPr);
		layout_1.addMember(lstPadre);
		listadoCuenta = new VentanaEmergente("Listado de Productos", layout_1,
				false, true);
		listadoCuenta.setWidth(930);
		listadoCuenta.setHeight(610);
		listadoCuenta.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClientEvent event) {
				//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display", "block");
				//calculo_Individual_Total();
				listadoCuenta.destroy();
			}
		});
		
		listadoCuenta.show();
	}
	
	private void obtener_UltimoDto(String filtro, String campoOrdenamiento) {
		// Analizamos de que tipo de documento se trata
		final AsyncCallback<DtocomercialDTO> Funcioncallback = new AsyncCallback<DtocomercialDTO>() {
			// String coincidencias = "";
			public void onSuccess(DtocomercialDTO result) {
				if (result != null) {
					Integer c = result.getNumRealTransaccion();
					c += 1;
					Numero_factura=c;
				} else {
					Numero_factura=1;
				}
			}

			public void onFailure(Throwable caught) {
				SC.say("Error al acceder al servidor: " + caught);
			}
		};
		getService().ultimoDocumento(filtro, campoOrdenamiento, Funcioncallback);
	}
	/*
	 * FUNCIONES ADICIONALES
	 */
	/*
	 * FUNCION PARA RECORRER EL GRID DE LAS CUENTAS INGRESADAS
	 */
	public void calculo_Totales() {
	//SC.say("Entroa calculo subtotales");
	listAsiento.saveAllEdits();
	ListGridRecord[] selectedRecords = listAsiento.getRecords();
	debe = 0.0;
	haber = 0.0;
	Double debeAsiento=0.0;
	Double haberAsiento=0.0;
	Integer id=0;
	String debehaber="";
	//SC.say("Total cuentas: "+selectedRecords.length+"-"+rowNumGlobal);
	MovimientoCuentaPlanCuentaDTO movimientocuentaplancuentadto= new MovimientoCuentaPlanCuentaDTO();
	MovimientoCuentaPlanCuenta = new ArrayList<MovimientoCuentaPlanCuentaDTO>();
			for (int i=0; i<rowNumGlobal; i++) {
				movimientocuentaplancuentadto= new MovimientoCuentaPlanCuentaDTO();
				debeAsiento=Double.valueOf(selectedRecords[i].getAttribute("valorDebeAsiento"));
				debe=debe+debeAsiento;
				haberAsiento=Double.valueOf(selectedRecords[i].getAttribute("valorHaberAsiento"));
				haber=haber+haberAsiento;
				id=Integer.valueOf(selectedRecords[i].getAttribute("idCuentaAsiento"));
				//debehaber=debehaber+"debe:"+selectedRecords[i].getAttribute("valorDebeAsiento")+"haber:"+selectedRecords[i].getAttribute("valorHaberAsiento")+" "; 
				
				if(debeAsiento>haberAsiento){
					movimientocuentaplancuentadto.setSigno('d');
					movimientocuentaplancuentadto.setValor(Double.valueOf(selectedRecords[i].getAttribute("valorDebeAsiento")));
				}else if(haberAsiento>debeAsiento){
					movimientocuentaplancuentadto.setSigno('h');
					movimientocuentaplancuentadto.setValor(Double.valueOf(selectedRecords[i].getAttribute("valorHaberAsiento")));
				}
				
				CuentaPlanCuentaDTO cuentaplancuentadto=new CuentaPlanCuentaDTO();
				CuentaDTO cuentadto = new CuentaDTO();
				cuentadto.setIdCuenta(id);
				cuentaplancuentadto.setCuenta(cuentadto);
				movimientocuentaplancuentadto.setTblcuentaPlancuenta(cuentaplancuentadto);
				MovimientoCuentaPlanCuenta.add(movimientocuentaplancuentadto);
				
			}
			//SC.say("Total cuentas: debe"+debe+"haber"+haber+"-"+debehaber);
	}
	
	/*
	 * FUNCION PARA LIMPIAR CAMPOS 
	 */
	public void Limpiar(){
		txtaConcepto.setValue("");
		txtFechaEmision.setValue(new Date());
		for (ListGridRecord rec : listAsiento.getRecords()) {
			listAsiento.removeData(rec);
		}
		
	}
	
	


}
