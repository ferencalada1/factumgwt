package com.giga.factum.client;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.regGrillas.EmpresaRecords;
import com.giga.factum.client.regGrillas.ImpuestoRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.fields.FloatItem;

public class frmImpuesto extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtNombre = new TextItem("txtImpuesto", "Nombre Impuesto");
	ListGrid lstImpuesto = new ListGrid();//crea una grilla
	TabSet tabImpuesto = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	ComboBoxItem cmbPlanCuenta = new ComboBoxItem("cmbPlanCuenta", "Plan de Cuenta");
	ComboBoxItem cmbTipoImpuesto = new ComboBoxItem("cmbTipoImpuesto", "Tipo Impuesto");
 	ComboBoxItem cmbCuenta = new ComboBoxItem("cmbCuenta", "Cuenta");
 	LinkedHashMap<String, String> MapCuenta = new LinkedHashMap<String, String>();
 	LinkedHashMap<String, String> MapEmpresa = new LinkedHashMap<String, String>();
	EmpresaDTO empresaDTO =new EmpresaDTO();
	LinkedList<String[]> cuentaplan=new LinkedList<String[]>();
	Boolean ban=false;
	int contador=20;
	int registros=0;
	TextItem txtidImpuesto = new TextItem("txtidImpuesto", "id Impuesto");
	TextItem txtCodigo = new TextItem("txtCodigo", "C\u00F3digo");
	TextItem txtCodigoElec = new TextItem("txtCodigoElec", "C\u00F3digo Elect.");
	FloatItem txtPorcentage = new FloatItem();
	int contadorNivel=0;
	int cuentaPadre=0;
	String planCuentaID;
	public frmImpuesto(){
		getService().getUserFromSession(callbackUser);
		getService().numeroRegistrosImpuesto("TblImpuesto", objbackI);
		this.setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Impuesto");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "95%");
	 	txtNombre.setShouldSaveValue(true);
	 	txtNombre.setRequired(true);
	 	txtNombre.setTextAlign(Alignment.LEFT);
	 	txtNombre.setDisabled(false);
	 	
	 	
	 	
	 	txtidImpuesto.setDisabled(true);
	 	txtidImpuesto.setKeyPressFilter("[0-9]");
	 	
	 	txtCodigo.setRequired(true);
	 	txtCodigo.setKeyPressFilter("[0-9]");
	 	txtCodigoElec.setRequired(true);
	 	txtCodigoElec.setKeyPressFilter("[0-9]");
	 	
	 	txtPorcentage.setRequired(true);
	 	txtPorcentage.setName("txtPorcentage");
	 	txtPorcentage.setTitle("Porcentage");
	 	cmbPlanCuenta.setRequired(true);
	 	cmbCuenta.setRequired(true);
	 	cmbCuenta.addChangedHandler(new ManejadorBotones("Cuenta"));
	 	cmbTipoImpuesto.setValueMap("IVA","RENTA");
	 	dynamicForm.setFields(new FormItem[] { txtidImpuesto, txtCodigo, txtNombre,txtCodigoElec, txtPorcentage, cmbCuenta, cmbPlanCuenta,cmbTipoImpuesto});
	 	layout.addMember(dynamicForm);
	 	cmbTipoImpuesto.setRequired(true);
	 	cmbTipoImpuesto.setDefaultToFirstOption(true);
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "70%");
	 	
	 	IButton btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	IButton btnModificar = new IButton("Modificar");
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	IButton btnEliminar = new IButton("Eliminar");
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(324, 6);
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_Impuesto = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		
	
		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
	    TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
	    TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
	    TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
	    TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
	    hStack.addMember(btnInicio);
	    hStack.addMember(btnAnterior);
	    hStack.addMember(btnSiguiente);
	    hStack.addMember(btnFin);
	    hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstImpuesto.setSize("100%", "80%");
		ListGridField lstidImpuesto = new ListGridField("lstidImpuesto", "C\u00F3digo");
		ListGridField lstCodigo =new ListGridField("lstCodigo", "C\u00F3digo Impuesto");
		ListGridField lstNombre = new ListGridField("lstNombre", "Nombre");
		ListGridField lstPorcentage= new ListGridField("lstPorcentage", "% Retenci\u00F3n");
		ListGridField lstidPlan = new ListGridField("lstidPlan", "Plan de Cuentas");
		ListGridField lstCuenta = new ListGridField("lstCuenta", "Cuenta");
		ListGridField lstCodigoElec =new ListGridField("lstCodigoElectronico", "C\u00F3digo Elect.");
		ListGridField lstTipo = new ListGridField("lstTipo", "Tipo");
		lstImpuesto.setFields(new ListGridField[] {lstidImpuesto,lstCodigo,lstNombre,lstPorcentage,lstidPlan,lstCuenta,lstCodigoElec,lstTipo});
		//getService().listarImpuesto(0, contador, objbacklst);
		layout_1.addMember(lstImpuesto);//para que se agregue el listado al vertical
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_Impuesto.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
	    btnFin.addClickHandler(new ManejadorBotones("right_all"));
	    btnSiguiente.addClickHandler(new ManejadorBotones("right"));
	    lstImpuesto.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
	    lstImpuesto.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
	    txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
	    searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
	    tabImpuesto.setSize("100%", "100%");
	    
	    tabImpuesto.addTab(lTbCuenta_mant);  
	    tabImpuesto.addTab(lTbListado_Impuesto);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabImpuesto);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		
		getService().listarEmpresa(0,100, objbacklstEmpresa);
		
		getService().listarImpuesto(0,100, objbacklstImpuesto);
		cmbPlanCuenta.addChangeHandler(  new ManejadorBotones("combo"));
		this.txtPorcentage.setValue("0");
		
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		if(campo.equalsIgnoreCase("nombre") || campo.equalsIgnoreCase("")){
			campo="nombre";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idImpuesto";
		}
		
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarImpuestoLike(nombre, campo, objbacklstImpuesto);
	}
	public void limpiar(){
		dynamicForm.setValue("txtImpuesto", "");
		dynamicForm.setValue("txtidImpuesto","");
		this.txtCodigo.setValue("");
		this.txtCodigoElec.setValue("");
		this.txtNombre.setValue("");
		this.txtPorcentage.setValue("0");
		this.cmbCuenta.setValue("");
		this.cmbPlanCuenta.setValue("");
	}
	/**
	 * Manejador de Botones para pantalla Impuesto
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,
		FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler,ChangeHandler,ChangedHandler{
		String indicador="";
		String nombre="";
		String codigo="";
		String codigoElec="";
		Double porcentaje=00.0;
		String idPlanCuenta="";
		String idCuenta="";
		int tipo=0;
		
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void datosfrm(){
			try{
				nombre=dynamicForm.getItem("txtImpuesto").getDisplayValue();
				codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
				codigoElec=dynamicForm.getItem("txtCodigoElec").getDisplayValue();
				if(cmbTipoImpuesto.getDisplayValue().equalsIgnoreCase("iva")){
					tipo=0;
				}else if(cmbTipoImpuesto.getDisplayValue().equalsIgnoreCase("renta")){
					tipo=1;
				} 
				porcentaje=Double.valueOf(dynamicForm.getItem("txtPorcentage").getDisplayValue());
				idCuenta=(String) dynamicForm.getItem("cmbCuenta").getValue();
				for(int i=0;i<cuentaplan.size();i++){
					String[] cuen=new String[3];
					cuen=cuentaplan.get(i);
					if(cuen[0].equals(idCuenta)){
						idPlanCuenta=cuen[2];
						break;
					}
				}
				
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					datosfrm();
					try{
						ImpuestoDTO ImpuestoDTO=new ImpuestoDTO();
						ImpuestoDTO.setNombre(nombre); 
						ImpuestoDTO.setCodigo(Integer.parseInt(codigo));
						ImpuestoDTO.setCodigoElectronico(codigoElec);
						ImpuestoDTO.setRetencion(porcentaje);
						ImpuestoDTO.setIdCuenta(Integer.parseInt(idCuenta));
						ImpuestoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
						ImpuestoDTO.setTipo(tipo);
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().grabarImpuesto(ImpuestoDTO,objback);
						contador=20;
					}catch(Exception e){
						SC.say(e.getMessage());
					}
					
					
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
	                public void execute(Boolean value) {  
	                    if (value != null && value) {
	                    	datosfrm();
	    					try{
	    						ImpuestoDTO ImpuestoDTO=new ImpuestoDTO();
	    						ImpuestoDTO.setNombre(nombre);
	    						SC.say(nombre);
	    						ImpuestoDTO.setCodigo(Integer.parseInt(codigo));
	    						SC.say(codigo+nombre);
	    						ImpuestoDTO.setCodigoElectronico(codigoElec);
	    						ImpuestoDTO.setRetencion(porcentaje);
	    						SC.say(codigo+nombre+String.valueOf(porcentaje));
	    						ImpuestoDTO.setIdCuenta(Integer.parseInt(idCuenta));
	    						SC.say(codigo+nombre+String.valueOf(porcentaje)+idCuenta);
	    						ImpuestoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
	    						SC.say(codigo+nombre+String.valueOf(porcentaje)+idCuenta+idPlanCuenta);
	    						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	    						getService().grabarImpuesto(ImpuestoDTO,objback);
	    						lstImpuesto.getSelectedRecord().getAttribute("Impuesto");
		                    	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		    			    	getService().eliminarImpuesto(ImpuestoDTO, objback);
		                    	limpiar();
		        				lstImpuesto.removeData(lstImpuesto.getSelectedRecord());
		        				getService().listarImpuesto(0,100, objbacklstImpuesto);
	    						contador=20;
	    					}catch(Exception e){
	    						SC.say(e.getMessage());
	    					}
	                    } else {  
	                        SC.say("Objeto no Eliminado");
	                    }  
	                }  
	            });  
				
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					try{
						nombre=dynamicForm.getItem("txtImpuesto").getDisplayValue();
						codigo=dynamicForm.getItem("txtCodigo").getDisplayValue();
						codigoElec=dynamicForm.getItem("txtCodigoElec").getDisplayValue();
						porcentaje=Double.valueOf(dynamicForm.getItem("txtPorcentage").getDisplayValue());
						if(cmbTipoImpuesto.getDisplayValue().equalsIgnoreCase("iva")){
							tipo=0;
						}else if(cmbTipoImpuesto.getDisplayValue().equalsIgnoreCase("renta")){
							tipo=1;
						} 
						idCuenta=(String) dynamicForm.getItem("cmbCuenta").getValue();
						for(int i=0;i<cuentaplan.size();i++){
							String[] cuen=new String[3];
							cuen=cuentaplan.get(i);
							if(cuen[0].equals(idCuenta)){
								idPlanCuenta=cuen[2];
								break;
							}
						}
						idPlanCuenta="1";
						SC.say(codigo+" "+nombre+" "+String.valueOf(porcentaje)+" "+idCuenta+" "+idPlanCuenta);
						ImpuestoDTO ImpuestoDTO=new ImpuestoDTO();
						ImpuestoDTO.setIdImpuesto(Integer.parseInt(txtidImpuesto.getDisplayValue()));
						ImpuestoDTO.setNombre(nombre);
						ImpuestoDTO.setCodigo(Integer.parseInt(codigo));
						ImpuestoDTO.setCodigoElectronico(codigoElec);
						ImpuestoDTO.setRetencion(porcentaje);
						ImpuestoDTO.setIdCuenta(Integer.parseInt(idCuenta));
						ImpuestoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
						ImpuestoDTO.setTipo(tipo);
    			    	limpiar();
        				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
                    	getService().modificarImpuesto(ImpuestoDTO,objback);
        				getService().listarImpuesto(0,100, objbacklstImpuesto);
						contador=20;
					}catch(Exception e){
						//SC.say(e.getMessage()); 
					}
				}
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarImpuesto(contador-20,contador, objbacklstImpuesto);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarImpuesto(contador-20,contador, objbacklstImpuesto);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarImpuesto(contador-20,contador, objbacklstImpuesto);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarImpuesto(registros-registros%20,registros, objbacklstImpuesto);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarImpuesto(contador-20,contador, objbacklstImpuesto);
					lblRegisros.setText(contador+" de "+registros);
				}
		}
	
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
			
		}
	
		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}
	
		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				txtNombre.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstNombre"));
				txtPorcentage.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstPorcentage"));
				cmbPlanCuenta.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstidPlan"));
				idCuenta=lstImpuesto.getSelectedRecord().getAttribute("lstCuenta");
				cmbCuenta.setValueMap(MapCuenta);  
				txtidImpuesto.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstidImpuesto"));
				txtCodigo.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstCodigo"));
				txtCodigoElec.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstCodigoElectronico"));
				tabImpuesto.selectTab(0);
		/*		if(lstImpuesto.getSelectedRecord().getAttribute("lstTipo")=="0")
				{
					cmbTipoImpuesto.setValue("IVA");
				}
				if(lstImpuesto.getSelectedRecord().getAttribute("lstTipo")=="1")
				{
					cmbTipoImpuesto.setValue("RENTA");
				}
			*/	
				cmbTipoImpuesto.setValue(lstImpuesto.getSelectedRecord().getAttribute("lstTipo"));
			}
			
		}
		public void onChange(ChangeEvent event) {
			String id=(String) event.getValue();
			if(!id.equals(null))
				getService().listarCuentaPlanCuenta(id,objbacklstCuentaPlan);
			
		}
		@Override
		public void onChanged(ChangedEvent event) {
			SC.say("ENTRO A CHANGED: "+event.getValue());
			if(indicador.equalsIgnoreCase("Cuenta")){
				cuentaPadre=Integer.valueOf(event.getValue().toString());
				if(event.getValue().toString().equals("0")){
					contadorNivel=contadorNivel-1;				
						if(contadorNivel==0){
							getService().listarCuentaQuery( "where u.tblcuenta.nivel='0' and u.tblplancuenta.idPlan='"+planCuentaID+"'", objbacklstCuenta);
						}else{
							getService().listarCuentaQuery("where u.tblcuenta.padre='"+cuentaPadre+"' and u.tblcuenta.nivel='"+contadorNivel+"' and u.tblplancuenta.idPlan='"+planCuentaID+"'",objbacklstCuenta);
						}
				}else{
					cuentaPadre=Integer.valueOf(event.getValue().toString());
					getService().listarCuentaQuery("where u.tblcuenta.padre='"+event.getValue().toString()+"' and u.tblcuenta.nivel='"+contadorNivel+"' and u.tblplancuenta.idPlan='"+planCuentaID+"'",objbacklstCuenta);
					
				}
			}
			
		}
	
		
			
	}
		
	final AsyncCallback<List<ImpuestoDTO>>  objbacklstImpuesto=new AsyncCallback<List<ImpuestoDTO>>(){
	
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<ImpuestoDTO> result) {
			getService().numeroRegistrosImpuesto("TblImpuesto", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new ImpuestoRecords((ImpuestoDTO)result.get(i)));
			}
			lstImpuesto.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
		
	};
	final AsyncCallback<ImpuestoDTO>  objbackImpuesto=new AsyncCallback<ImpuestoDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(ImpuestoDTO result) {
			if(result!=null){
			//	dynamicForm.setValue("txtImpuesto", result.getDescripcion());  
				dynamicForm.setValue("txtidImpuesto", result.getIdImpuesto()); 
				ban=true;
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
			}
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};
	
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			SC.say(result);
			getService().listarImpuesto(0,100, objbacklstImpuesto);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
	};
	final AsyncCallback<List<CuentaDTO>>  objbacklstCuenta=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<CuentaDTO> result) {
			List<CuentaDTO> listcuenta=null;
			listcuenta=result;
			MapCuenta.clear();			
            for(int i=0;i<result.size();i++) {
            	MapCuenta.put(String.valueOf(result.get(i).getIdCuenta()), 
						result.get(i).getNombreCuenta());
			}
            MapCuenta.put("0","<b>REGRESAR</b>");
            cmbCuenta.setValueMap(MapCuenta);  
            contadorNivel=contadorNivel+1;
		}
	};
	final AsyncCallback<LinkedList<String[]>>  objbacklstCuentaPlan=new AsyncCallback<LinkedList<String[]>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
		@Override
		public void onSuccess(LinkedList<String[]> result) {
			cuentaplan=result;
			MapCuenta.clear();
			MapCuenta.put("0","Volver");
            for(int i=0;i<result.size();i++) {
            	String[] cuentas=new String[3];
            	cuentas=result.get(i);
            	MapCuenta.put(cuentas[0],cuentas[1]);
			}
			cmbCuenta.setValueMap(MapCuenta);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
	};
	final AsyncCallback<List<EmpresaDTO>>  objbacklstEmpresa=new AsyncCallback<List<EmpresaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(List<EmpresaDTO> result) {
			MapEmpresa.clear();
            for(int i=0;i<result.size();i++) {
            	MapEmpresa.put(String.valueOf(new EmpresaRecords((EmpresaDTO)result.get(i)).getidEmpresa()), 
						new EmpresaRecords((EmpresaDTO)result.get(i)).getEmpresa());
			}
			cmbPlanCuenta.setValueMap(MapEmpresa); 
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
       }
	};
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {
		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
				planCuentaID=result.getPlanCuenta();
				getService().listarCuentaQuery( "where u.tblcuenta.nivel='0' and u.tblplancuenta.idPlan='"+planCuentaID+"'", objbacklstCuenta);
			}
		}
		public void onFailure(Throwable caught) {
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
		}
	};
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
