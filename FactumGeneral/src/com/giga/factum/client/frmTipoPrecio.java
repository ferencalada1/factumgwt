package com.giga.factum.client;
import java.util.List;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.regGrillas.TipoPrecioRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;

public class frmTipoPrecio extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	//SearchForm dynamicForm_buscar = new SearchForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtTipoPrecio = new TextItem("txtTipoPrecio", "Nombre");
	ListGrid lstTipoPrecio = new ListGrid();//crea una grilla
	TabSet tabTipoPrecio = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	
	int orden=0;
	
	frmTipoPrecio()
	{
		getService().numeroRegistrosTipoprecio("TblTipoPrecio", objbackI);
		
		setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Tipo Precio");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "95%");
	 	txtTipoPrecio.setShouldSaveValue(true);
	 	txtTipoPrecio.setRequired(true);
	 	txtTipoPrecio.setTextAlign(Alignment.LEFT);
	 	txtTipoPrecio.setDisabled(false);
	 	
	 	
	 	TextItem textidTipoPrecio = new TextItem("txtidTipoPrecio", "C\u00F3digo");
	 	textidTipoPrecio.setDisabled(true);
	 	textidTipoPrecio.setKeyPressFilter("[0-9]");
	 	dynamicForm.setFields(new FormItem[] { textidTipoPrecio, txtTipoPrecio});
	 	layout.addMember(dynamicForm);
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "70%");
	 	
	 	IButton btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	IButton btnModificar = new IButton("Modificar");
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	IButton btnEliminar = new IButton("Eliminar");
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(324, 6);
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_TipoPrecio = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstTipoPrecio.setSize("100%", "80%");
		
		ListGridField idTipoPrecio = new ListGridField("idTipoPrecio", "C\u00F3digo");
		ListGridField TipoPrecio = new ListGridField("TipoPrecio", "Nombre");
		
		lstTipoPrecio.setFields(new ListGridField[] {idTipoPrecio,TipoPrecio});
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarTipoprecio(0, contador, objbacklst);
		layout_1.addMember(lstTipoPrecio);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_TipoPrecio.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstTipoPrecio.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstTipoPrecio.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        
	    tabTipoPrecio.addTab(lTbCuenta_mant);  
	    tabTipoPrecio.addTab(lTbListado_TipoPrecio);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabTipoPrecio);
  
       
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		if(campo.equalsIgnoreCase("nombre")){
			campo="tipoPrecio";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idTipoPrecio";
		}
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarTipoprecioLike(nombre, campo, objbacklst);
	}
	public void limpiar(){
		dynamicForm.setValue("txtTipoPrecio", "");
		dynamicForm.setValue("txtidTipoPrecio","");
	}
	/**
	 * Manejador de Botones para pantalla TipoPrecio
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					String nombre=dynamicForm.getItem("txtTipoPrecio").getDisplayValue();
					TipoPrecioDTO TipoPrecioDTO=new TipoPrecioDTO(Factum.empresa.getIdEmpresa(),nombre, orden);
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarTipoprecio(TipoPrecioDTO,objback);
					contador=20;
					lstTipoPrecio.redraw();
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.say("se pulso el boton eliminar");
				SC.confirm("�Est� seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	TipoPrecioDTO TipoPrecio= new TipoPrecioDTO(Integer.parseInt(lstTipoPrecio.getSelectedRecord().getAttribute("idTipoPrecio")),
                        	lstTipoPrecio.getSelectedRecord().getAttribute("TipoPrecio"), orden);
                        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
        					getService().eliminarTipoprecio(TipoPrecio, objback);
                        	limpiar();
            				lstTipoPrecio.removeData(lstTipoPrecio.getSelectedRecord());
            			} else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
				
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtTipoPrecio").getDisplayValue();
					String id=dynamicForm.getItem("txtidTipoPrecio").getDisplayValue();
					TipoPrecioDTO TipoPrecioDTO=new TipoPrecioDTO(Integer.parseInt(id),nombre, orden); 
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().modificarTipoprecio(TipoPrecioDTO,objback);
				}
			}else if(indicador.equalsIgnoreCase("Buscar")){
				String idTipoPrecio=dynamicForm.getItem("txtidTipoPrecio").getDisplayValue();
				getService().buscarTipoprecio(idTipoPrecio, objbackTipoPrecio);
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipoprecio(contador-20,contador, objbacklst);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipoprecio(contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipoprecio(contador-20,contador, objbacklst);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipoprecio(registros-registros%20,registros, objbacklst);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipoprecio(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtTipoPrecio",lstTipoPrecio.getSelectedRecord().getAttribute("TipoPrecio"));  
				dynamicForm.setValue("txtidTipoPrecio", lstTipoPrecio.getSelectedRecord().getAttribute("idTipoPrecio"));
				tabTipoPrecio.selectTab(0);
			}
			
		}

		
			
	}
		
	final AsyncCallback<List<TipoPrecioDTO>>  objbacklst=new AsyncCallback<List<TipoPrecioDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<TipoPrecioDTO> result) {
			getService().numeroRegistrosTipoprecio("TblTipoPrecio", objbackI);
//			if(contador>registros){
//				lblRegisros.setText(registros+" de "+registros);
//			}else
//				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new TipoPrecioRecords((TipoPrecioDTO)result.get(i)));
				//SC.say(listado[i].getTipoPrecio()+" "+listado[i].getidTipoPrecio());
            }
			lstTipoPrecio.setData(listado);
			//lstTipoPrecio.draw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
	};
	final AsyncCallback<TipoPrecioDTO>  objbackTipoPrecio=new AsyncCallback<TipoPrecioDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			//SC.say("Error no se conecta  a la base");
			ban=false;
		}
		@Override
		public void onSuccess(TipoPrecioDTO result) {
			if(result!=null){
				dynamicForm.setValue("txtTipoPrecio", result.getTipoPrecio());  
				dynamicForm.setValue("txtidTipoPrecio", result.getIdTipoPrecio()); 
				ban=true;
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado "+result);
				ban=false;
			}
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
			orden=registros+1;
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else{
				lblRegisros.setText(contador+" de "+registros);
			}
			//SC.say("Numero de registros "+registros);
		}
	};

	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			//SC.say("Error no se conecta  a la base");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			SC.say(result);
			getService().listarTipoprecio(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}