package com.giga.factum.client;

import java.util.List;

import com.smartgwt.client.widgets.layout.VLayout;

import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.FormLayoutType;

import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.MaskValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.TransferImgButton;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.google.gwt.user.client.ui.Label;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;



public class frmCliente extends VLayout{
	DynamicForm dynamicForm;
	ListGrid lstCliente = new ListGrid();
	TabSet tabSet = new TabSet();
	Label lblRegisros = new Label();
	
	int contador=20;
	int registros=0;
   SearchForm searchForm = new SearchForm();
   Factura fact = null;
  
   String tarjeta="";
   int puntos=0;
   
   CheckboxItem chkPassport; 
   TextItem txtcontribuyenteEspecial;
   CheckboxItem chkobligadoContabilidad; 
   
   IButton btnGrabar;
   IButton btnModificar;
   IButton btnEliminar;
   char obligado='0';
	public frmCliente (Factura f) 
	{   fact = f;
		cargarelementos ();
	
	}
   
   
	public void  cargarelementos (){
		getService().numeroRegistrosPersona("tblclientes", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
//		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Grabar"));
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("buscarfrm"));

		PickerIcon buscarPickerLst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerLst.addFormItemClickHandler(new ManejadorBotones("buscar"));

		setSize("1100", "800");
		FloatRangeValidator floatRangeValidator = new FloatRangeValidator(); 
		
		RegExpValidator emailexp = new RegExpValidator();  
		emailexp.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");  
		RegExpValidator floatexp = new RegExpValidator(); 
		floatexp.setExpression("([0-9])+[.]+[0-9]");
		
		MaskValidator maskValidator = new MaskValidator();  
        
		maskValidator.setMask("^\\s*(1?)\\s*\\(?\\s*(\\d{3})\\s*\\)?\\s*-?\\s*(\\d{3})\\s*-?\\s*(\\d{4})\\s*$");  
        maskValidator.setTransformTo("$1($2) $3 - $4"); 
		tabSet.setSize("100%", "100%");
		
		Tab tabIngreso = new Tab("Ingreso Clientes");
		
		VLayout layout = new VLayout();
		layout.setSize("100%", "100%");
		dynamicForm = new DynamicForm();
		dynamicForm.setSize("100%", "90%");
		dynamicForm.setSectionVisibilityMode(VisibilityMode.MULTIPLE);
		dynamicForm.setMinColWidth(50);
		dynamicForm.setItemLayout(FormLayoutType.TABLE);
		dynamicForm.setWidth100();
		dynamicForm.setNumCols(6);
		dynamicForm.setCellPadding(30);
		dynamicForm.setPadding(30);
		//dynamicForm.setMargin(10);;
//		dynamicForm.setSnapTo("TL");
//		dynamicForm.setSnapOffsetTop(100);
//		dynamicForm.setTop(50);
		TextItem txtNombreComercial = new TextItem("txtNombreComercial", "Nombre Comercial");
//		txtNombreComercial.setLength(50);
		txtNombreComercial.setTabIndex(5);
//		txtNombreComercial.setLeft(10);
//		txtNombreComercial.setTop(34);
		txtNombreComercial.setColSpan(5);
		//txtNombre.setKeyPressFilter("[a-zA-Z\u00F1\u00D1 ]");
		//txtNombreComercial.setRequired(true);
		txtNombreComercial.setWidth(1030);
		txtNombreComercial.setTitleStyle("lblFrm");
		txtNombreComercial.setTextBoxStyle("textFrm");
		txtNombreComercial.setHeight(30);
		txtNombreComercial.setShowFocused(false);
		
		TextItem txtCedula = new TextItem("txtCedula", "C\u00E9dula/RUC");
		buscarPicker.setHeight(30);
		buscarPicker.setWidth(30);
 		txtCedula.setIcons(buscarPicker);
 		txtCedula.setTabIndex(1);
		txtCedula.setTooltip("Ingrese la C\u00E9dula");
		
		txtCedula.setTitleStyle("lblFrm");
		txtCedula.setTextBoxStyle("textFrm");
		txtCedula.setHeight(30);
		txtCedula.setWidth(250);
		
		txtCedula.setShowFocused(false);
//		txtCedula.setLeft(10);
//		txtCedula.setTop(6);
		//txtCedula.setHint("Solo numeros");
		//txtCedula.setKeyPressFilter("[0-9]");
		txtCedula.setRequired(true);
		
		txtCedula.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				dynamicForm.setValue("txtCedula", dynamicForm.getItem("txtCedula").getDisplayValue());
//				txtCedula.setValue(((String)event.getValue()).replace(" ", ""));
			}
		});
		
		chkPassport = new CheckboxItem("Pasaporte");
		chkPassport.setTitle("Pasaporte?");
		chkPassport.setWidth(250);
		chkPassport.setTabIndex(1);
		chkPassport.setTop(6);
		chkPassport.setLeft(150);
		chkPassport.setTitleStyle("lblFrm");
		chkPassport.setTextBoxStyle("textFrm");
		chkPassport.setHeight(30);
		chkPassport.setShowFocused(true);
		
		txtcontribuyenteEspecial = new TextItem("txtContribuyenteEspecial","Contribuyente Especial");
		txtcontribuyenteEspecial.setValue("0000");
		txtcontribuyenteEspecial.setTabIndex(4);
		txtcontribuyenteEspecial.setTitleStyle("lblFrm");
		txtcontribuyenteEspecial.setTextBoxStyle("textFrm");
		txtcontribuyenteEspecial.setHeight(30);
		txtcontribuyenteEspecial.setShowFocused(false);
		
		chkobligadoContabilidad = new CheckboxItem("chkObligadoContabilidad");
	    chkobligadoContabilidad.setTitle("Obligado Contabilidad");
//	    chkobligadoContabilidad.setWidth(250);
	    chkobligadoContabilidad.setTabIndex(2);
//	    chkobligadoContabilidad.setTop(34);
//	    chkobligadoContabilidad.setLeft(150);
	    chkobligadoContabilidad.addChangedHandler(new ManejadorBotones("obligado"));
	    chkobligadoContabilidad.setTitleStyle("lblFrm");
	    chkobligadoContabilidad.setTextBoxStyle("textFrm");
	    chkobligadoContabilidad.setHeight(30);
	    chkobligadoContabilidad.setShowFocused(true);
		
	    
		TextItem txtRazonSocial = new TextItem("txtRazonSocial", "Razon Social");
//		txtRazonSocial.setLeft(266);
//		txtRazonSocial.setTop(34);
		txtRazonSocial.setLength(50);
		txtRazonSocial.setRequired(true);
		txtRazonSocial.setTabIndex(3);
		txtRazonSocial.setWidth(640);
		txtRazonSocial.setColSpan(3);
		txtRazonSocial.setTitleStyle("lblFrm");
		txtRazonSocial.setTextBoxStyle("textFrm");
		txtRazonSocial.setHeight(30);
		txtRazonSocial.setShowFocused(false);
		
		TextItem txtDireccion = new TextItem("txtDireccion", "Direcci\u00F3n");
//		txtDireccion.setLeft(266);
//		txtDireccion.setTop(62);
		txtDireccion.setRequired(true);
//		txtDireccion.setLength(100);
		txtDireccion.setTabIndex(6);
		txtDireccion.setColSpan(6);
		txtDireccion.setWidth(1030);
		txtDireccion.setTitleStyle("lblFrm");
		txtDireccion.setTextBoxStyle("textFrm");
		txtDireccion.setHeight(30);
		txtDireccion.setShowFocused(false);
		
		
		TextItem txtTelefono = new TextItem("txtTelefono", "Tel\u00E9fono:");
//		txtTelefono.setLeft(10);
//		txtTelefono.setTop(146);
		txtTelefono.setTooltip("Tel\u00E9fono");
		txtTelefono.setKeyPressFilter("[0-9]");
		txtTelefono.setWidth(250);
//		txtTelefono.setLength(15);
		txtTelefono.setTabIndex(8);
		txtTelefono.setTitleStyle("lblFrm");
		txtTelefono.setTextBoxStyle("textFrm");
		txtTelefono.setHeight(30);
		txtTelefono.setShowFocused(false);
		
		TextAreaItem txtObservaciones = new TextAreaItem("txtObservaciones", "Observaci\u00F3n:");
		txtObservaciones.setTabIndex(13);
		//txtObservaciones.setLeft(10);
		//txtObservaciones.setTop(222);
		//txtObservaciones.setLength(100);
		txtObservaciones.setColSpan(5);
		txtObservaciones.setWidth(1030);
		txtObservaciones.setTitleStyle("lblFrm");
		txtObservaciones.setTextBoxStyle("textFrm");
		txtObservaciones.setHeight(120);
		txtObservaciones.setShowFocused(false);
		txtObservaciones.setRowSpan(3);
		txtObservaciones.setLength(250);
		
		TextItem txtLocalizacion = new TextItem("txtLocalizacion", "Localizacion");
		txtLocalizacion.setTabIndex(4);
		//txtLocalizacion.setLeft(10);
		//txtLocalizacion.setTop(222);
		//txtLocalizacion.setLength(100);
		txtLocalizacion.setTabIndex(7);
		txtLocalizacion.setWidth(250);
		txtLocalizacion.setTitleStyle("lblFrm");
		txtLocalizacion.setTextBoxStyle("textFrm");
		txtLocalizacion.setHeight(30);
		txtLocalizacion.setShowFocused(false);
		
		TextItem txtEmail = new TextItem("txtEmail", "e-mail");
		txtEmail.setTabIndex(10);
		//txtEmail.setLeft(10);
		//txtEmail.setTop(62);
		txtEmail.setTooltip("Ingrese el email del cliente");
		txtEmail.setShowHint(false);
		//txtEmail.setLength(100);
		txtEmail.setValidators(emailexp);
		txtEmail.setWidth(250);
		txtEmail.setTitleStyle("lblFrm");
		txtEmail.setTextBoxStyle("textFrm");
		txtEmail.setHeight(30);
		txtEmail.setShowFocused(false);
		
		FloatItem txtMontoCredito = new FloatItem();
//		//txtMontoCredito.setLeft(10);
		//txtMontoCredito.setTop(118);
		txtMontoCredito.setTitle("Monto Cr\u00E9dito");
		txtMontoCredito.setName("txtMontoCredito");
		txtMontoCredito.setRequired(true);
		txtMontoCredito.setValidators(floatRangeValidator);
		txtMontoCredito.setValue(0.0);
		txtMontoCredito.setTabIndex(12);
		// Version no requiere mostrar esos campos
		txtMontoCredito.setVisible(false);
		
		FloatItem txtCupoCredito = new FloatItem();
		txtCupoCredito.setLeft(10);
		txtCupoCredito.setTop(90);
		txtCupoCredito.setTabIndex(11);
		txtCupoCredito.setTitle("Cupo Cr\u00E9dito");
		txtCupoCredito.setName("txtCupoCredito");
		txtCupoCredito.setValidators(floatRangeValidator);
		txtCupoCredito.setValue(0.0);
		txtCupoCredito.setRequired(true);
		// Version no requiere mostrar esos campos
		TextItem txtTelefono2 =new TextItem("txtTelefono2", "Tel\u00E9fono 2");
		txtTelefono2.setKeyPressFilter("[0-9]");
		txtTelefono2.setTitleStyle("lblFrm");
		txtTelefono2.setWidth(250);
		txtTelefono2.setTextBoxStyle("textFrm");
		txtTelefono2.setHeight(30);
		txtTelefono2.setShowFocused(false);
		txtTelefono2.setTabIndex(9);
		//txtTelefono2.setLength(15);
		txtCupoCredito.setVisible(false);
	
		
//		dynamicForm.setFields(new FormItem[] { txtCedula, chkPassport, chkobligadoContabilidad, 
//				txtDireccion, txtRazonSocial, 
//				txtcontribuyenteEspecial, 
//				txtNombreComercial,
//				txtTelefono, 
//				txtCupoCredito, txtMontoCredito, txtTelefono2, 
//				txtLocalizacion, txtEmail, txtObservaciones});
		dynamicForm.setFields(new FormItem[] { txtCedula, chkPassport, chkobligadoContabilidad, 
				txtRazonSocial, txtcontribuyenteEspecial, 
				txtNombreComercial,
				txtDireccion, 
				txtLocalizacion, txtTelefono, txtTelefono2,
				txtEmail, txtCupoCredito, txtMontoCredito,
				txtObservaciones});
//		dynamicForm.setFields(new FormItem[] { txtCedula, chkPassport, txtDireccion, txtRazonSocial, txtTelefono, txtNombreComercial, txtCupoCredito, txtObservaciones, txtMontoCredito, txtEmail, txtcontribuyenteEspecial, chkobligadoContabilidad});
		layout.addMember(dynamicForm);
		
		HStack hStack_1 = new HStack();
		
		
		btnGrabar = new IButton("Grabar");
		hStack_1.addMember(btnGrabar);
		btnGrabar.setSize("100px", "22px");
		btnGrabar.setTabIndex(10);
		btnGrabar.addClickHandler(new ManejadorBotones("grabar"));
		//btnGrabar.setIcon("icons/16/icon_add_files.png");
		
		btnModificar = new IButton("Modificar");
		hStack_1.addMember(btnModificar);
		btnModificar.setTabIndex(11);
		IButton btnNuevo = new IButton("Nuevo");
		hStack_1.addMember(btnNuevo);
		btnModificar.addClickHandler(new ManejadorBotones("modificar")); 
		
//		IButton btnTarjeta = new IButton("Crear Tarjeta");
//		hStack_1.addMember(btnTarjeta);
//		btnTarjeta.addClickHandler(new ManejadorBotones("tarjeta")); 
		
		btnEliminar = new IButton("Eliminar");
		hStack_1.addMember(btnEliminar);
		btnNuevo.addClickHandler(new ManejadorBotones("nuevo"));
		
		layout.addMember(hStack_1);
		tabIngreso.setPane(layout);
		tabSet.addTab(tabIngreso);
		
		Tab tabListado = new Tab("Listado Clientes");
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
         
        HLayout hLayout = new HLayout();
        hLayout.setSize("100%", "5%");
         
        searchForm.setSize("88%", "100%");
        searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
        searchForm.setNumCols(4);
        ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n");
        
        TextItem txtBuscarLst=new TextItem();
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerLst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones("buscar"));
        
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(56);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        hLayout.addMember(searchForm);
        // hLayout.addMember(canvas_1);
         //layout_1.addMember(hLayout);
          
        HStack hStack = new HStack();
        hLayout.addMember(hStack);
        hStack.setSize("12%", "100%");
        TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        layout_1.addMember(hLayout);
        
        btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        
        lstCliente.setSize("100%", "80%");
        lstCliente.setFields(new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), new ListGridField("RazonSocial", "Razon Social",250), new ListGridField("NombreComercial", "Nombre Comercial:",150), new ListGridField("Telefonos", "Tel\u00E9fono",125)
                ,new ListGridField("E-mail", "Correo",175),new ListGridField("Localizacion", "Localizacion",175),new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("Telefonos2", "Tel\u00E9fono2",175), new ListGridField ("observaciones","Observaciones:",300)});
//        lstCliente.setFields(new ListGridField[] { new ListGridField("Cedula", "C\u00E9dula/RUC",150), new ListGridField("NombreComercial", "Nombre Comercial:",250), new ListGridField("RazonSocial", "Razon Social",250), new ListGridField("Telefonos", "Tel\u00E9fono",175)
//        ,new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("tarjeta", "C�digo Tarjeta",150),new ListGridField("puntos", "Puntos",150)});
        lstCliente.setShowRowNumbers(true);
        layout_1.addMember(lstCliente);
        
        //HLayout hLayout = new HLayout();
        //hLayout.setSize("100%", "10%");
        
        
        
        
        tabListado.setPane(layout_1);
        tabSet.addTab(tabListado);
		
		
		
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().ListJoinPersona("tblclientes",0,contador, objbacklst);
		lstCliente.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
	}


	public frmCliente() {
		
		cargarelementos ();
	}
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers

	}*/
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			String campo=null;
			if(tabla.equals("Cedula")){
				campo="cedulaRuc";
//			}else if(tabla.equals("Nombre Comercial")||tabla.equals("")){
//				campo="nombreComercial";
//			}else if(tabla.equals("Razon Social")){
			}else if(tabla.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(tabla.equals("Razon Social")||tabla.equals("")){
				campo="razonSocial";
			}else if(tabla.equals("Direcci\u00F3n")){
				campo="direccion";
			}
			
			//validacion de caja de busqueda, debe contener algo a buscar
			 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
				 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			 else if(campo.equals("cedulaRuc")||campo.equals("nombreComercial")||campo.equals("razonSocial")||campo.equals("direccion")){		
			   DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			    getService().findJoin(nombre, "tblclientes", campo, objbacklst); 
		    }
		}catch(Exception e){
			SC.say(e.getMessage());
		}
			
	}
//	public void limpiar(){
//		dynamicForm.setValue("txtCedula", "");
//		dynamicForm.setValue("txtNombreComercial", "");
//		dynamicForm.setValue("txtRazonSocial", "");
//		dynamicForm.setValue("txtEmail", "");
//		dynamicForm.setValue("txtObsevaciones", "");
//		dynamicForm.setValue("txtTelefono", "");
//		dynamicForm.setValue("txtDireccion", "");
//		dynamicForm.setValue("txtNivelAcceso", "");
//		dynamicForm.setValue("txtCupoCredito", "");
//		dynamicForm.setValue("txtMontoCredito", "");
//		dynamicForm.setValue("txtContribuyenteEspecial", "0000");
//		dynamicForm.setValue("chkObligadoContabilidad", false);
//	}
	
	public void limpiar(){
		dynamicForm.setValue("txtCedula", "");
		dynamicForm.setValue("txtNombreComercial", "");
		dynamicForm.setValue("txtRazonSocial", "");
		dynamicForm.setValue("txtEmail", "");
		dynamicForm.setValue("txtObservaciones", "");
		dynamicForm.setValue("txtLocalizacion", "");
		dynamicForm.setValue("txtTelefono", "");
		dynamicForm.setValue("txtTelefono2", "");
		dynamicForm.setValue("txtDireccion", "");
		dynamicForm.setValue("txtNivelAcceso", "");
		dynamicForm.setValue("txtCupoCredito", 0);
		dynamicForm.setValue("txtMontoCredito", 0);
		dynamicForm.setValue("txtContribuyenteEspecial", "0000");
		chkobligadoContabilidad.setValue(false);
//		dynamicForm.setValue("chkObligadoContabilidad", false);
		chkPassport.setValue(false);
//		dynamicForm.setValue("Pasaporte", false);
	}
	
//	public void ingresarPersona(Boolean ingresa, String cedula){
//		String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
//		String telefono="",direccion="" , telefono2="", contribuyenteEspecial="";
//		char obligadoContabilidad;
//		nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
//		razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
//		CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
//		MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
//		email=dynamicForm.getItem("txtEmail").getDisplayValue();
//		observaciones=dynamicForm.getItem("txtObsevaciones").getDisplayValue();
//		observaciones=observaciones.toUpperCase();
//		telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
//		direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
//		contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
//		obligadoContabilidad=obligado;
//		PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,"" +
//				"telefono2",observaciones,email,'1');
//		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//		perDTO.setClienteU(new ClienteDTO(perDTO,Double.parseDouble(MontoCredito), 
//				Double.parseDouble(CupoCredito), contribuyenteEspecial, obligadoContabilidad));		
//		
//		getService().GrabarCli(perDTO,objback );
//	}
	
	public void ingresarPersona(Boolean ingresa, String cedula){
		String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
		String telefono="",direccion="" ,localizacion="", telefono2="", contribuyenteEspecial="";
		////com.google.gwt.user.client.Window.alert("En ingresar persona");
		char obligadoContabilidad;
		////com.google.gwt.user.client.Window.alert("tomando datos");
		nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("nomb comer");
		razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("razon");
		CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("cupo");
		MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("monto");
		email=dynamicForm.getItem("txtEmail").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("email");
		////com.google.gwt.user.client.Window.alert("observaciones "+dynamicForm.getItem("txtObservaciones").getDisplayValue());
		observaciones=dynamicForm.getItem("txtObservaciones").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("observaciones");
		contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("obligado");
		obligadoContabilidad=obligado;
		////com.google.gwt.user.client.Window.alert("obligado "+obligado);
		
		//observaciones=observaciones.toUpperCase();
		////com.google.gwt.user.client.Window.alert("tomando datos 2");
		telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
		telefono2= dynamicForm.getItem("txtTelefono2").getDisplayValue();
		direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
		localizacion=dynamicForm.getItem("txtLocalizacion").getDisplayValue();
		////com.google.gwt.user.client.Window.alert("tomando datos FIN");
		PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,localizacion,telefono,
        telefono2,observaciones,email,'1');
		////com.google.gwt.user.client.Window.alert("persona creada");
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		
		ClienteDTO cli=new ClienteDTO(Factum.empresa.getIdEmpresa(),perDTO,Double.parseDouble(MontoCredito), 
				Double.parseDouble(CupoCredito), contribuyenteEspecial, obligadoContabilidad);
		////com.google.gwt.user.client.Window.alert("cliente creado");
		cli.setCodigoTarjeta(tarjeta);
		////com.google.gwt.user.client.Window.alert("set tarjeta");
		cli.setPuntos(puntos);
		////com.google.gwt.user.client.Window.alert("set puntos");
		perDTO.setClienteU(cli);	
		////com.google.gwt.user.client.Window.alert("ser cliente en persona");
		getService().GrabarCli(perDTO,objback );
		////com.google.gwt.user.client.Window.alert("despues grabar cli");
	}
	
	public void modificarPersona(String cedula){
		String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
		String telefono="",direccion="" ,localizacion="", telefono2="", contribuyenteEspecial="";
		char obligadoContabilidad;
		
		nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
		razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
		CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
		MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
		email=dynamicForm.getItem("txtEmail").getDisplayValue();
		observaciones=dynamicForm.getItem("txtObservaciones").getDisplayValue();
		observaciones=observaciones.toUpperCase();
		telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
		telefono2=dynamicForm.getItem("txtTelefono2").getDisplayValue();
		direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
		localizacion=dynamicForm.getItem("txtLocalizacion").getDisplayValue();
		contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
		obligadoContabilidad=obligado;
		String idCliente=lstCliente.getSelectedRecord().getAttribute("idCliente");
		String idPersona=lstCliente.getSelectedRecord().getAttribute("idPersona");
		PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,localizacion,telefono,"" +
				telefono2,observaciones,email,'1');
		perDTO.setIdPersona(Integer.parseInt(idPersona));
		ClienteDTO cli= new ClienteDTO();
		cli.setIdEmpresa(Factum.empresa.getIdEmpresa());
		cli.setIdCliente(Integer.parseInt(idCliente));
		cli.setCupoCredito(Double.parseDouble(CupoCredito));
		cli.setMontoCredito(Double.parseDouble(MontoCredito));
		cli.setTblpersona(perDTO);
		cli.setCodigoTarjeta(tarjeta);
		cli.setPuntos(puntos);
		cli.setContribuyenteEspecial(contribuyenteEspecial);
		cli.setObligadoContabilidad(obligadoContabilidad);
		perDTO.setClienteU(cli);	
		SC.say(String.valueOf(perDTO.getClienteU().getIdCliente()));
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().modificarPersona(perDTO, objback);
	}
	
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler, ChangedHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void onClick(ClickEvent event){
//			String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones;
//			String telefono="",direccion="", contribuyenteEspecial="";
			String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
			String telefono="",direccion="", localizacion="" , telefono2="", contribuyenteEspecial="";
			char obligadoContabilidad;
			if(indicador.equalsIgnoreCase("grabar")){
				////com.google.gwt.user.client.Window.alert("En grabar");
				if(dynamicForm.validate()){
					////com.google.gwt.user.client.Window.alert("En validate");
					final String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
					////com.google.gwt.user.client.Window.alert("tomando cedula");
					//	CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
						////com.google.gwt.user.client.Window.alert("cedula valida");
						ingresarPersona(true,cedula);
						chkPassport.setValue(false);
					}else if(chkPassport.getValueAsBoolean()){
						////com.google.gwt.user.client.Window.alert("pasaporte");
						ingresarPersona(true,cedula);
					}
					else {
						SC.confirm("C\u00E9dula Incorrecta, se trata de Pasaporte Extranjero?", new BooleanCallback() {  
			                public void execute(Boolean value) {
			                	if(value)
			                	{
			                		chkPassport.setValue(true);
			                		ingresarPersona(true,cedula);
			                		
			                	}
			                	else
			                	{SC.say("C\u00E9dula incorrecta");}
			                }  
			            });
					}
					
						
				}else{
					////com.google.gwt.user.client.Window.alert("En no validate");
				}
					
			}
			else if(indicador.equalsIgnoreCase("nuevo")){
				limpiar();
				btnGrabar.setDisabled(false);
			    btnModificar.setDisabled(true);
			    btnEliminar.setDisabled(true);
			}else if(indicador.equalsIgnoreCase("tarjeta")){
				SC.say("Hola tarjeta");
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				
//				if(dynamicForm.validate()){
//					String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
//			//		CValidarDato valida=new CValidarDato();
//					ValidarRuc valida = new ValidarRuc();
//					if(valida.validarRucCed(cedula)){
//						nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
//						razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
//						CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
//						MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
//						email=dynamicForm.getItem("txtEmail").getDisplayValue();
//						observaciones=dynamicForm.getItem("txtObsevaciones").getDisplayValue();
//						observaciones=observaciones.toUpperCase();
//						telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
//						direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
//						contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
//						obligadoContabilidad=obligado;
//						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,"" +
//								"telefono2",observaciones,email,'1');
//						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//						perDTO.setClienteU(new ClienteDTO(perDTO,Double.parseDouble(MontoCredito), 
//								Double.parseDouble(CupoCredito),contribuyenteEspecial,obligadoContabilidad));		
//						getService().eliminarPersona(perDTO, objback);
//					}else{
//						SC.say("C\u00E9dula incorrecta");
//					}
//				}
				if(dynamicForm.validate()){
					String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
					//CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
						String idPersona=lstCliente.getSelectedRecord().getAttribute("idPersona");
						nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
						razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
						CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
						MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
						email=dynamicForm.getItem("txtEmail").getDisplayValue();
						observaciones=dynamicForm.getItem("txtObservaciones").getDisplayValue();
						observaciones=observaciones.toUpperCase();
						telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
						telefono2=dynamicForm.getItem("txtTelefono2").getDisplayValue();
						direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
						contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
						obligadoContabilidad=obligado;
						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,"" +
								telefono2,observaciones,email,'1');
						perDTO.setIdPersona(Integer.parseInt(idPersona));
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						perDTO.setClienteU(new ClienteDTO(Factum.empresa.getIdEmpresa(),perDTO,Double.parseDouble(MontoCredito), 
								Double.parseDouble(CupoCredito), contribuyenteEspecial, obligadoContabilidad));		
						getService().eliminarPersona(perDTO, objback);
					}else{
						SC.say("C�dula incorrecta");
					}
				}
				limpiar();
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					final String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
					//CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
						modificarPersona(cedula);
						chkPassport.setValue(false);
					}else if(chkPassport.getValueAsBoolean()){
						modificarPersona(cedula);
					}
					else {
						SC.confirm("C\u00E9dula Incorrecta, se trata de Pasaporte Extranjero?", new BooleanCallback() {  
			                public void execute(Boolean value) {
			                	if(value)
			                	{
			                		chkPassport.setValue(true);
			                		modificarPersona(cedula);
			                		
			                	}
			                	else
			                	{SC.say("C\u00E9dula incorrecta");}
			                }  
			            });
					}			
						
				}
//				try{
//					String cedula=dynamicForm.getItem("txtCedula").getDisplayValue();
//				//	CValidarDato valida=new CValidarDato();
//					ValidarRuc valida = new ValidarRuc();
//					if(valida.validarRucCed(cedula)){
//						//SC.say("cedula v�lida");
//						nombreComercial=dynamicForm.getItem("txtNombreComercial").getDisplayValue();
//						razonSocial=dynamicForm.getItem("txtRazonSocial").getDisplayValue();
//						CupoCredito=dynamicForm.getItem("txtCupoCredito").getDisplayValue();
//						MontoCredito=dynamicForm.getItem("txtMontoCredito").getDisplayValue();
//						email=dynamicForm.getItem("txtEmail").getDisplayValue();
//						observaciones=dynamicForm.getItem("txtObsevaciones").getDisplayValue();
//						observaciones=observaciones.toUpperCase();
//						telefono=dynamicForm.getItem("txtTelefono").getDisplayValue();
//						direccion=dynamicForm.getItem("txtDireccion").getDisplayValue();
//						String idCliente=lstCliente.getSelectedRecord().getAttribute("idCliente");
//						String idPersona=lstCliente.getSelectedRecord().getAttribute("idPersona");
//						contribuyenteEspecial=dynamicForm.getItem("txtContribuyenteEspecial").getDisplayValue();
//						obligadoContabilidad=obligado;
//						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,"" +
//								"telefono2",observaciones,email,'1');
//						perDTO.setIdPersona(Integer.parseInt(idPersona));
//						ClienteDTO cli= new ClienteDTO();
//						cli.setIdCliente(Integer.parseInt(idCliente));
//						cli.setCupoCredito(Double.parseDouble(CupoCredito));
//						cli.setMontoCredito(Double.parseDouble(MontoCredito));
//						cli.setTblpersona(perDTO);
//						cli.setContribuyenteEspecial(contribuyenteEspecial);
//						cli.setObligadoContabilidad(obligadoContabilidad);
//						perDTO.setClienteU(cli);	
//						SC.say(String.valueOf(perDTO.getClienteU().getIdCliente()));
//						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//						getService().modificarPersona(perDTO, objback);
//					}else{
//						SC.say("C\u00E9dula incorrecta");
//					}
//				}catch(Exception e){
//					SC.say(e.getMessage());
//				}
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblclientes",contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblclientes",contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblclientes",contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblclientes",registros-registros%20,registros, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListJoinPersona("tblclientes",contador-20,contador, objbacklst);
					//lblRegisros.setText(contador+" de "+registros);
				}

		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
			
		}
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equalsIgnoreCase("buscar")){
				buscarL();
			
			}else if(indicador.equalsIgnoreCase("buscarfrm")){
				String ced=dynamicForm.getItem("txtCedula").getDisplayValue();
				PersonaDTO per=new PersonaDTO();
				per.setCedulaRuc(ced);
				//SC.say(per.getCedulaRuc());
				getService().buscarPersona(per,0, objbackb);
			}
				
			
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			dynamicForm.setValue("txtNombreComercial",lstCliente.getSelectedRecord().getAttribute("NombreComercial"));  
			dynamicForm.setValue("txtCedula", lstCliente.getSelectedRecord().getAttribute("Cedula"));
			ValidarRuc valida = new ValidarRuc();
			if(!valida.validarRucCed(lstCliente.getSelectedRecord().getAttribute("Cedula"))){
				chkPassport.setValue(true);
			}
			dynamicForm.setValue("txtTelefono", lstCliente.getSelectedRecord().getAttribute("Telefonos"));
			dynamicForm.setValue("txtTelefono2", lstCliente.getSelectedRecord().getAttribute("Telefonos2"));
			dynamicForm.setValue("txtDireccion", lstCliente.getSelectedRecord().getAttribute("Direccion"));
			dynamicForm.setValue("txtCupoCredito", lstCliente.getSelectedRecord().getAttribute("CupoCreditoCliente"));
			dynamicForm.setValue("txtMontoCredito", lstCliente.getSelectedRecord().getAttribute("MontoCredito"));
			dynamicForm.setValue("txtObservaciones", lstCliente.getSelectedRecord().getAttribute("observaciones"));
			dynamicForm.setValue("txtLocalizacion", lstCliente.getSelectedRecord().getAttribute("Localizacion"));
			dynamicForm.setValue("txtRazonSocial",lstCliente.getSelectedRecord().getAttribute("RazonSocial")); 
			dynamicForm.setValue("txtEmail",lstCliente.getSelectedRecord().getAttribute("E-mail"));  
			dynamicForm.setValue("txtContribuyenteEspecial",lstCliente.getSelectedRecord().getAttribute("contribuyenteEspecial"));
			dynamicForm.setValue("chkObligadoContabilidad",lstCliente.getSelectedRecord().getAttributeAsBoolean("obligadoContabilidad"));
			obligado=(lstCliente.getSelectedRecord().getAttributeAsBoolean("obligadoContabilidad"))?'1':'0';
			tabSet.selectTab(0);
			btnGrabar.setDisabled(true);
		    btnModificar.setDisabled(false);
		    btnEliminar.setDisabled(false);
		}
		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("obligado")){
				//SC.say("ESTADO CHECK: "+chkobligadoContabilidad.getValueAsBoolean());
				if(chkobligadoContabilidad.getValueAsBoolean()){
					obligado='1';
				}else{
					obligado='0';
				}
			}
			
		}	
	}
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			fact.cargarcamposcliente(dynamicForm.getItem("txtCedula").getDisplayValue(),0);
			getService().ListJoinPersona("tblclientes",0,contador, objbacklst);

		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PersonaDTO> result) {
		
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
			}
			lstCliente.setData(listado);
			lstCliente.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	final AsyncCallback<PersonaDTO>  objbackb=new AsyncCallback<PersonaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		@Override
		public void onSuccess(PersonaDTO result) {
			if(result!=null){
//					dynamicForm.setValue("txtCupoCredito", result.getClienteU().getCupoCredito());
//					dynamicForm.setValue("txtMontoCredito", result.getClienteU().getMontoCredito());
//					dynamicForm.setValue("txtRazonSocial",result.getRazonSocial());  
//					dynamicForm.setValue("txtEmail",result.getMail());  
//					dynamicForm.setValue("txtNombreComercial",result.getNombreComercial());  
//					dynamicForm.setValue("txtCedula", result.getCedulaRuc());
//					dynamicForm.setValue("txtObservaciones", result.getObservaciones());
//					dynamicForm.setValue("txtTelefono", result.getTelefono1()+" / "+result.getTelefono2());
//					dynamicForm.setValue("txtDireccion", result.getDireccion());
//					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//					getService().ListJoinPersona("tblclientes",0,contador, objbacklst);
				dynamicForm.setValue("txtCupoCredito", result.getClienteU().getCupoCredito());
				dynamicForm.setValue("txtMontoCredito", result.getClienteU().getMontoCredito());
				dynamicForm.setValue("txtRazonSocial",result.getRazonSocial());  
				dynamicForm.setValue("txtEmail",result.getMail());  
				dynamicForm.setValue("txtNombreComercial",result.getNombreComercial());  
				dynamicForm.setValue("txtCedula", result.getCedulaRuc());
				ValidarRuc valida = new ValidarRuc();
				if(!valida.validarRucCed(result.getCedulaRuc())){
					chkPassport.setValue(true);
				}
				dynamicForm.setValue("txtObservaciones", result.getObservaciones());
				dynamicForm.setValue("txtLocalizacion", result.getLocalizacion());
				dynamicForm.setValue("txtTelefono", result.getTelefono1());
				dynamicForm.setValue("txtTelefono2", result.getTelefono2());
				dynamicForm.setValue("txtContribuyenteEspecial",result.getClienteU().getContribuyenteEspecial());
				boolean val=(result.getClienteU().getObligadoContabilidad()=='0')?false:true;
				chkobligadoContabilidad.setValue(val);
				////com.google.gwt.user.client.Window.alert("antes obligado");
				obligado=(chkobligadoContabilidad.getValueAsBoolean())?'1':'0';
				////com.google.gwt.user.client.Window.alert("despues obligado");
//				+" / "+result.getTelefono2());
				dynamicForm.setValue("txtDireccion", result.getDireccion());
				btnGrabar.setDisabled(true);
			    btnModificar.setDisabled(false);
			    btnEliminar.setDisabled(false);
			}else{
				SC.say("Elemento No Encontrado");
			}
			
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};

}

