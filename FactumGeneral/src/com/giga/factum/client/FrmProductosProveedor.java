package com.giga.factum.client;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.regGrillas.TipoPrecioRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;

public class FrmProductosProveedor extends VLayout
{
	ListGrid lstProductoProveedor = new ListGrid();//+++ LISTADO GRANDE
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtStockMinimo =new TextItem("txtStockMinimo", "<b>Stock Min</b>");
	TextItem txtStockMaximo =new TextItem("txtStockMaximo", "<b>Stock Max</b>");
	TextItem txtProducto =new TextItem("txtProducto", "<b>Producto</b>");
	TextItem txtidPro =new TextItem("txtidPro", "<b>Cedula/RUC Proveedor</b>");
	TextItem txtnomPro =new TextItem("txtnomPro", "<b>Nombre Proveedor</b>");
	TextItem txtapePro =new TextItem("txtapePro", "<b>Razon Social Proveedor</b>");
	//TextItem txtMarca =new TextItem("txtMarca", "<b>Marca</b>");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnGrabar = new IButton("Grabar");
	LinkedList <ProductoTipoPrecioDTO> disProTipPrecio = new LinkedList <ProductoTipoPrecioDTO>();//para grabar el dist
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	ListGrid lstPrecios = new ListGrid();//+++++ LISTADO PEQUE�O
	double PrecioCompraSeleccionado =0;
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	Date fechaFactura;// Para la fecha del sistema
	int contador=20;
	int registros=0;
	Label lblRegisros = new Label("# Registros");
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Window winClientes = new Window();
	frmListClientes frmlisCli;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Window winProducto;  
	ListProductos listProductos;
	String idProductoSeleccionado="";
	String idProveedorSeleccionado="";
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	public FrmProductosProveedor() {
		getService().fechaServidor(callbackFecha);
//		formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	//++++++++++++++++++++++  Tipo Precio ++++++++++++++++++++++++++++++++++++++++
		try{
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
			ListGridField idProducto = new ListGridField("idProducto", "IdProducto");
			//idProducto.setHidden(true);
			ListGridField idTipoPrecio = new ListGridField("idTipo", "IdTipo");
			idTipoPrecio.setHidden(true);
			ListGridField TipoPrecio = new ListGridField("tipo", "Tipo");
			//TipoPrecio.setHidden(true);
			ListGridField precioC = new ListGridField("precioC", "Precio Compra");
			ListGridField precioV = new ListGridField("precioV", "Precio Venta");
			precioV.addChangeHandler(new ManejadorBotones(""));
			precioV.setCanEdit(true);
			ListGridField porcentaje = new ListGridField("porcentaje", "% Utilidad");
			lstPrecios.setFields(new ListGridField[] {idProducto,idTipoPrecio,TipoPrecio,precioC,precioV,porcentaje});
			lstPrecios.setSize("40%", "100%");
		dynamicForm.setSize("20%", "20%");
		dynamicForm2.setSize("20%", "20%");
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		  
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, txtStockMinimo,txtStockMaximo});
		//dynamicForm2.setFields(new FormItem[] { txtnomPro, txtapePro, txtProducto,txtMarca});
		dynamicForm2.setFields(new FormItem[] { txtidPro, txtnomPro, txtapePro, txtProducto});
		HStack hStackEspacio = new HStack();
		hStackEspacio.setSize("4%", "20%");
		HStack hStackEspacio1 = new HStack();
		hStackEspacio1.setSize("4%", "20%");
		
		btnGrabar.setSize("10%", "20%");
		btnGrabar.addClickHandler(new ClickHandler()
		{
	       public void onClick(ClickEvent event)
	       {		
	    	   int numRegTipo=lstPrecios.getRecords().length;
	    	   disProTipPrecio = new LinkedList <ProductoTipoPrecioDTO>();	       			
	    	   for (int i = 0; i<numRegTipo; i++)
	    	   {
	    		   ProductoDTO proDTO = new ProductoDTO();
	    		   proDTO.setIdProducto(Integer.valueOf(lstPrecios.getRecord(i).getAttribute("idProducto")));
	    		   proDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
	    		   proDTO.setEstablecimiento(Factum.user.getEstablecimiento());
	    		   
	    		   TipoPrecioDTO tipDTO = new TipoPrecioDTO();
	    		   tipDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
	    		   tipDTO.setIdTipoPrecio(Integer.valueOf(lstPrecios.getRecord(i).getAttribute("idTipo")));
	    		   tipDTO.setTipoPrecio(lstPrecios.getRecord(i).getAttribute("tipo"));	    		   
				   
	    		   ProductoTipoPrecioDTO proTipPre = new ProductoTipoPrecioDTO();	
	    		   proTipPre.setTblproducto(proDTO);
	    		   proTipPre.setTbltipoprecio(tipDTO);
	    		   proTipPre.setPorcentaje(Double.valueOf(lstPrecios.getRecord(i).getAttribute("precioV")));
	    		   disProTipPrecio.add(proTipPre);
			}//fin for
   			//		getService().grabarDistributivo2(disList, codigoProfesor,anioSistema,cicloSistema,objback);
	    	   //DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	    	   		getService().grabarProductoTipoPrecio(disProTipPrecio, objback);
   					
		           }
		       });
		HStack hStackSuperior = new HStack();
		hStackSuperior.setSize("100%", "20%");
		hStackSuperior.addMember(dynamicForm);
		hStackSuperior.addMember(dynamicForm2);
		hStackSuperior.addMember(hStackEspacio);
		hStackSuperior.addMember(lstPrecios);
		hStackSuperior.addMember(hStackEspacio1);
		//hStackSuperior.addMember(btnGrabar);
		
		VStack vStackFinal = new VStack();
		vStackFinal.setSize("10%", "100%");
		HStack hStackSup1 = new HStack();
		hStackSup1.setSize("100%", "35%");
	//	hStackSup1.setBackgroundColor("blue");
		HStack hStackSup2 = new HStack();
		hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
		btnGrabar.setSize("100%", "30%");
		
		
		vStackFinal.addMember(hStackSup1);
		vStackFinal.addMember(btnGrabar);
		vStackFinal.addMember(hStackSup2);
		
		hStackSuperior.addMember(vStackFinal);
		addMember(hStackSuperior);
		lstProductoProveedor.setSize("100%", "60%");
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		ListGridField Id =new ListGridField("idProducto", "<b>id</b>");
		Id.setWidth("1%");
		Id.setHidden(true);
		ListGridField Descripcion =new ListGridField("nomProducto", "<b>Descripci&oacute;n</b>");
		Descripcion.setWidth("36%");
		ListGridField Cantidad =new ListGridField("cantidad", "<b>Cantidad</b>");
		Cantidad.setWidth("5%");
		Cantidad.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		Cantidad.setAlign(Alignment.RIGHT);
//		Cantidad.setSummaryFunction(SummaryFunctionType.SUM);
		Cantidad.setShowGridSummary(true);
		
		Cantidad.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("cantidad"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		
		ListGridField Stock= new ListGridField("stock", "<b>Stock</b>");
		Stock.setWidth("5%");
		Stock.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		Stock.setAlign(Alignment.RIGHT);
		ListGridField PrecioCompra =new ListGridField("precioCompra", "<b>Precio Compra</b>");
		PrecioCompra.setWidth("6%");
		PrecioCompra.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		PrecioCompra.setAlign(Alignment.RIGHT);
		PrecioCompra.setShowGridSummary(true);
//		PrecioCompra.setSummaryFunction(SummaryFunctionType.SUM);
		PrecioCompra.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("precioCompra"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
//		PrecioCompra.sum
		ListGridField PrecioVenta =new ListGridField("precioVenta", "<b>Precio Venta</b>");
		PrecioVenta.setWidth("6%");
		PrecioVenta.setCellFormatter(new CellFormatter() {
	        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            if(value == null) return null;
	            NumberFormat nf = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	            try {
	                return nf.format(((Number)value).floatValue());
	            } catch (Exception e) {
	                return value.toString();
	            }
	        }
	    });
		PrecioVenta.setAlign(Alignment.RIGHT);
		PrecioVenta.setShowGridSummary(true);
//		PrecioVenta.setSummaryFunction(SummaryFunctionType.SUM);
		PrecioVenta.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("precioVenta"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +formatoDecimalN.format(sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + formatoDecimalN.format(sum) + "</span>";
                }
            }
        });
		ListGridField IdPersona =new ListGridField("idPersona", "<b>idPersona</b>");
		IdPersona.setWidth("1%");
		IdPersona.setHidden(true);
		ListGridField Proveedor =new ListGridField("Proveedor", "<b>Proveedor</b>");
		Proveedor.setWidth("30%");
		ListGridField FechaCompra=new ListGridField("fechaCompra", "<b>Fecha de Compra</b>");
		FechaCompra.setWidth("8%");
		FechaCompra.setAlign(Alignment.CENTER);
		
		ListGridField vacio =new ListGridField("vacio", "&nbsp;&nbsp; ");
		vacio.setWidth("2%");
		
		lstProductoProveedor.setFields(new ListGridField[] { Id,Descripcion, Cantidad,Stock,PrecioCompra, 
				PrecioVenta,vacio,IdPersona,Proveedor,FechaCompra});
		lstProductoProveedor.setShowRowNumbers(true);
		lstProductoProveedor.setShowGridSummary(true);
		lstProductoProveedor.addSelectionChangedHandler(new SelectionChangedHandler() {  
            public void onSelectionChanged(SelectionEvent event) 
            {  
            	PrecioCompraSeleccionado=Double.valueOf(lstProductoProveedor.getSelectedRecord().getAttribute("precioCompra"));
            	Integer codPro = Integer.valueOf(lstProductoProveedor.getSelectedRecord().getAttribute("idProducto"));
				getService().listarTipoPrecio(codPro, objbacklstTipoPrecio);
        }  
    });
		addMember(lstProductoProveedor);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		hStack.addMember(btnGenerarReporte);
		
		IButton btnLimpiar = new IButton("Limpiar");
		//IButton btnVerDocumento = new IButton("Ver Documento");
		txtStockMinimo.setValue("0");
		txtStockMaximo.setValue("10000");
		
		PickerIcon pckLimpiarProducto = new PickerIcon(PickerIcon.CLEAR);
		txtProducto.setIcons(pckLimpiarProducto);
		pckLimpiarProducto.addFormItemClickHandler(new ManejadorBotones("borrarProducto"));
		
		txtProducto.setValue("");
		txtProducto.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				listProductos = new ListProductos();
				listProductos.lstProductos.addDoubleClickHandler( new ManejadorBotones("producto"));
				winProducto=new Window();
				winProducto.setWidth(930);  
				winProducto.setHeight(610);  
				winProducto.setTitle("Seleccione un producto");  
				winProducto.setShowMinimizeButton(false);  
				winProducto.setIsModal(true);  
				winProducto.setShowModalMask(true);  
				winProducto.centerInPage();  
				winProducto.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winProducto.destroy();
                    }  
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                
                listProductos.setSize("100%","100%"); 
                form.addMember(listProductos);
                winProducto.addItem(form);
                winProducto.show();
			}
		});
		
		PickerIcon pckLimpiarProveedor = new PickerIcon(PickerIcon.CLEAR);
		txtidPro.setIcons(pckLimpiarProveedor);
		pckLimpiarProveedor.addFormItemClickHandler(new ManejadorBotones("borrarProveedor"));
		
		txtidPro.setValue("");
		txtidPro.setDisabled(false);
		txtidPro.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {				
				winClientes = new Window();
				winClientes.setWidth(930);
				winClientes.setHeight(610);
				winClientes.setShowMinimizeButton(false);
				winClientes.setIsModal(true);
				winClientes.setShowModalMask(true);
				winClientes.setKeepInParentRect(true);
				winClientes.centerInPage();
				winClientes.addCloseClickHandler(new CloseClickHandler() {
					@Override
					public void onCloseClick(CloseClientEvent event) {
						winClientes.destroy();

					}
				});
				VLayout form = new VLayout();
				form.setSize("100%", "100%");
				form.setPadding(5);
				form.setLayoutAlign(VerticalAlignment.BOTTOM);
				
				frmlisCli = new frmListClientes("tblproveedors");
				frmlisCli.lstCliente.addDoubleClickHandler(new ManejadorBotones("proveedor"));
				frmlisCli.setSize("100%", "100%");
				winClientes.setTitle("Buscar Proveedor");
				form.addMember(frmlisCli);
			
				frmlisCli.txtBuscarLst.setSelectOnFocus(true);
				winClientes.addItem(form);
				winClientes.show();				
			}
		});
		txtnomPro.setValue("");
		txtnomPro.setDisabled(true);
		txtapePro.setValue("");
		txtapePro.setDisabled(true);
		//txtMarca.setValue("");
		btnGenerarReporte.setDisabled(false);
		btnGenerarReporte.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event){     	  
	        	getService().listarProductosProveedor(txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(), Integer.valueOf(txtStockMinimo.getValue().toString()), Integer.valueOf(txtStockMaximo.getValue().toString()),
	        	idProveedorSeleccionado,idProductoSeleccionado,objProductoProveedor);
	//        	SC.say("proveeN: "+txtProveedorN.getDisplayValue().toString()+"proveeA: "+txtProveedorA.getDisplayValue().toString()+"producto: "+txtProducto.getDisplayValue().toString());
	   			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	           }
	       });
		btnLimpiar.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event)
	           {
	        	txtStockMinimo.setValue("0");
	   			txtStockMaximo.setValue("0");
	   			txtProducto.setValue("");
	   			txtidPro.setValue("");
	   			txtnomPro.setValue("");
	   			txtapePro.setValue("");
	   			//txtMarca.setValue("");
	   			btnGenerarReporte.setDisabled(false);
	           }
	       });
		
	/*	btnVerDocumento.addClickHandler(new ClickHandler(){
	           @Override
	           public void onClick(ClickEvent event){
	           	
	          
	           }
	       });
*/
		hStack.addMember(btnLimpiar);
	//	hStack.addMember(btnVerDocumento);
		
		addMember(hStack);
		}catch(Exception e ){SC.say("Error: "+e);}
	}
	
	NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	final AsyncCallback<List<ProductoProveedor>>  objProductoProveedor = new AsyncCallback<List<ProductoProveedor>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			txtStockMinimo.setValue("0");
			txtStockMaximo.setValue("0");
		}
		public void onSuccess(List<ProductoProveedor> resultProd) {
			ListGridRecord[] listadoProd = new ListGridRecord[resultProd.size()];
			int contadorProdPro=0;
			if (resultProd.size()>0)
			{	
				for(int i=0;i<resultProd.size();i++) 
				{		
					String[] descripciones =resultProd.get(i).getNomProducto().split(";");
					listadoProd[contadorProdPro]=new ListGridRecord();
					listadoProd[contadorProdPro].setAttribute("idProducto", resultProd.get(i).getIdProducto());
					listadoProd[contadorProdPro].setAttribute("nomProducto", descripciones[1]);
					listadoProd[contadorProdPro].setAttribute("cantidad", resultProd.get(i).getCantidad());
					listadoProd[contadorProdPro].setAttribute("stock", resultProd.get(i).getStock());
					listadoProd[contadorProdPro].setAttribute("precioCompra", resultProd.get(i).getPrecioCompra());
					listadoProd[contadorProdPro].setAttribute("precioVenta", resultProd.get(i).getPrecioVenta());
					listadoProd[contadorProdPro].setAttribute("idPersona", resultProd.get(i).getIdPersona());
					listadoProd[contadorProdPro].setAttribute("Proveedor", "  "+resultProd.get(i).getProveedor());
					listadoProd[contadorProdPro].setAttribute("fechaCompra", resultProd.get(i).getFechaCompra());
					contadorProdPro++;	
				}
			}
			else
			{
				SC.say("No hay productos que cumplan los parametros establecidos");
			}
			lstProductoProveedor.setData(listadoProd);
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        lstProductoProveedor.show();
	        lstProductoProveedor.redraw();
		}
	};

 	 	
 	
 	final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
			
		}
		public void onSuccess(Date result) {
			fechaFactura=result;
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
		}
	};
	
	
//+++++++++++++++++++++++++++++++++++++++   TIPO DE PRECIOS    +++++++++++++++++++++++++++++++++++++++++++
	final AsyncCallback<List<ProductoTipoPrecio>>  objbacklstTipoPrecio = new AsyncCallback<List<ProductoTipoPrecio>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
			}
			public void onSuccess(List<ProductoTipoPrecio> resultTip) {
				Collections.sort(resultTip,new ProductoTipoPrecioCompare());
				//resultTip.sort(new ProductoTipoPrecioCompare());
				ListGridRecord[] listadoTip = new ListGridRecord[resultTip.size()];
				int contadorTip=0;
				if (resultTip.size()>0){	
					int res = resultTip.size();
					for(int i=0; i<res; i++) 
					{
						Double pCompra = Double.valueOf(PrecioCompraSeleccionado);
						Double pVenta = Double.valueOf(resultTip.get(i).getPorcentaje());
						Double porcentaje = pVenta-pCompra;
						porcentaje = (porcentaje*100)/pCompra;
						porcentaje=Math.rint(porcentaje*100)/100;//redondear a 2 decimales
						listadoTip[contadorTip]=new ListGridRecord();
						listadoTip[contadorTip].setAttribute("idProducto", resultTip.get(i).getIdProducto());
						listadoTip[contadorTip].setAttribute("idTipo", resultTip.get(i).getIdTipo());
						listadoTip[contadorTip].setAttribute("tipo", resultTip.get(i).getTipo());
						listadoTip[contadorTip].setAttribute("precioC", String.valueOf(PrecioCompraSeleccionado));
						listadoTip[contadorTip].setAttribute("precioV", resultTip.get(i).getPorcentaje());
						listadoTip[contadorTip].setAttribute("porcentaje", String.valueOf(porcentaje));
						contadorTip++;
					}
					lstPrecios.show();
					lstPrecios.setData(listadoTip);
					lstPrecios.redraw();
				}
				else{
					SC.say("No existen tipos de precios");
					lstPrecios.hide();
				}		
			}
		};
		
		private class ManejadorBotones implements ChangeHandler, DoubleClickHandler, FormItemClickHandler
		{
		String indicador="";
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		@Override
		public void onChange(ChangeEvent event) {
			try{
				Double pCompra = Double.valueOf(PrecioCompraSeleccionado);
				Double pVenta = Double.valueOf(String.valueOf(event.getValue()));
				Double porcentaje = pVenta-pCompra;
				porcentaje = (porcentaje*100)/pCompra;
				porcentaje=Math.rint(porcentaje*100)/100;//redondear a 2 decimales
				lstPrecios.getSelectedRecord().setAttribute("porcentaje", String.valueOf(porcentaje));
				lstPrecios.refreshRow(lstPrecios.getEditRow());
			}catch(Exception e){
			//	SC.say(e.getMessage());
			}
		}
		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equals("producto")){
				idProductoSeleccionado=listProductos.lstProductos.getSelectedRecord().getAttribute("idProducto");
				txtProducto.setValue(listProductos.lstProductos.getSelectedRecord().getAttribute("descripcion"));
				winProducto.destroy();
			}else if(indicador.equals("proveedor")){
				txtidPro.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("Cedula"));
				txtnomPro.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("NombreComercial"));
				txtapePro.setValue(frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("RazonSocial"));
				idProveedorSeleccionado=frmlisCli.lstCliente.getSelectedRecord()
						.getAttribute("Cedula");
				winClientes.destroy();
			}
			
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equals("borrarProveedor")){				
				idProveedorSeleccionado="";
				txtidPro.setValue("");
				txtnomPro.setValue("");
				txtapePro.setValue("");
			}else if(indicador.equals("borrarProducto")){
				idProductoSeleccionado="";
				txtProducto.setValue("");
			}
			
		} 
		}
		
		final AsyncCallback<String>  objback=new AsyncCallback<String>(){
				public void onFailure(Throwable caught) {
					SC.say("Error dado:" + caught.toString());
				}
				public void onSuccess(String result) {
					SC.say(result);
				}
			};
}
