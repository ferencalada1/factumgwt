package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.tile.TileLayout;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.Button;

public class ComprobanteVenta extends HLayout{
	public ComprobanteVenta() {
		
		VStack vStack = new VStack();
		vStack.setWidth("100%");
		
		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setSize("100%", "65px");
		dynamicForm.setIsGroup(true);
		dynamicForm.setGroupTitle("Info. Factura");
		
		dynamicForm.setFields(new FormItem[] {});
	
		vStack.addMember(dynamicForm);
		
		DynamicForm dynamicForm_1 = new DynamicForm();
		dynamicForm_1.setHeight("99px");
		vStack.addMember(dynamicForm_1);
		
		ListGrid listGrid = new ListGrid();
		listGrid.setHeight("216px");
		vStack.addMember(listGrid);
		addMember(vStack);
		
		VStack vStack_1 = new VStack();
		vStack_1.setSize("100%", "456px");
		
		DynamicForm dynamicForm_2 = new DynamicForm();
		dynamicForm_2.setHeight("36px");
		CanvasItem canvasItem = new CanvasItem("newCanvasItem_1", "New CanvasItem");
		
		Button btnNewButton = new Button("New Button");
		canvasItem.setCanvas(btnNewButton);
		dynamicForm_2.setFields(new FormItem[] { canvasItem});
		vStack_1.addMember(dynamicForm_2);
		
		DynamicForm dynamicForm_3 = new DynamicForm();
		dynamicForm_3.setHeight("43px");
		vStack_1.addMember(dynamicForm_3);
		
		ListGrid listGrid_1 = new ListGrid();
		listGrid_1.setHeight("369px");
		vStack_1.addMember(listGrid_1);
		addMember(vStack_1);
	}

}
