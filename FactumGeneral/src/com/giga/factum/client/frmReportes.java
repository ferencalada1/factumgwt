package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.ReporteClientesDTO;
import com.giga.factum.client.DTO.ReportesDTO;
import com.giga.factum.client.regGrillas.DetallesRecords;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.data.Record;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.types.Alignment;

public class frmReportes extends VLayout{
	
	ListGrid lstReporteCaja = new ListGrid();
	ListGrid lstReporteCajaDetalle = new ListGrid();
	IButton btnExportarDet;
	String fechaI="";
	String fechaF="";
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();//para el nombre apellido ruc
	HStack hStackSuperior = new HStack();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	VLayout layout_1 = new VLayout();
	String Tipo="";
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	HStack hStack = new HStack();
	HLayout hLayoutPr = new HLayout();
	VentanaEmergente listadoProd ;
	//double Valoriva=0;
	//double Valoriva0=0;
	//double ValorSubtotal=0;
	//double ValorTotal=0;
	
	TextItem txtRuc =new TextItem("txtRuc", "<b>Ruc</b>");
	TextItem txtNom =new TextItem("txtNomComercial", "<b>Nombre Comercial</b>");
	TextItem txtApe =new TextItem("txtRazonSocial", "<b>Razon Social</b>");
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	ListGrid ListGridTotales = new ListGrid();
	//DateRangeItem dateRangeItem = new DateRangeItem("txtRango", "Rango de Fechas");
	
	ListGridField id;
	ListGridField NumRealTransaccion;
	ListGridField TipoTransaccion;
	ListGridField Fecha;
	ListGridField FechadeExpiracion;
	ListGridField Vendedor;
	ListGridField Cliente;
	ListGridField Autorizacion;
	ListGridField NumPagos;
	ListGridField Estado;
	ListGridField Iva0;
	ListGridField Iva14;
	ListGridField Iva; 
	ListGridField Total;
	ListGridField SubTotal;
	ListGridField Descuento;
	ListGridField Retencion;
	ListGridField RetencionIVA;
	ListGridField RetencionRENTA;
	ListGridField NumeroRetencion;
	ListGridField NumeroFactura;
	ListGridField Ruc;
	
	ListGridField lstNumRealTransaccion;
	ListGridField lstFecha;
	ListGridField lstCliente;
	ListGridField lstEstado;
	ListGridField lstDescripcion;
	ListGridField lstCantidad;
	ListGridField lstPrecio;
	ListGridField lstTotal;
	ListGridField lstDescuento;
	ListGridField lstRuc;
	ListGridField lstVend;
	
	
	public frmReportes() {
		try{
			lstReporteCaja = new ListGrid() {  
	            @Override  
	            protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {  
	                if (getFieldName(colNum).equals("Estado")) {  
	                	ListGridRecord listrecord =  record;  
	                    if (listrecord.getAttributeAsString("Estado").equals("CONFIRMADO")) {  
	                        return "font-weight:bold; color:#298a08;"; 
	                    } else if (listrecord.getAttributeAsString("Estado").equals("NO CONFIRMADO")) {  
	                        return "font-weight:bold; color:#287fd6;";  
	                    } else if(listrecord.getAttributeAsString("Estado").equals("ANULADA")){  
	                    	return "font-weight:bold; color:#d64949;"; 
	                    }else{
	                    	return super.getCellCSSText(record, rowNum, colNum);  
	                    }
	                }
	                else
	                {	
	                	if(getFieldName(colNum).equals("Autorizacion"))
	                	{
	                		ListGridRecord listrecord =  record;
	                		if (listrecord.getAttributeAsString("Autorizacion").equals("TOTALES:")) 
	                		{  
		                        return "font-weight:bold; color:#FF0000;"; 
		                    } 
	                		else
	                		{
		                    	return super.getCellCSSText(record, rowNum, colNum);  
		                    }
	                	}	
	                	else {  
	                		return super.getCellCSSText(record, rowNum, colNum);  
	                	}
	                }	
	            }  
	        };  
			dynamicForm.setSize("40%", "15%");
			dynamicForm2.setSize("60%", "15%");

			dynamicForm2.setFields(new FormItem[] { txtRuc, txtApe, txtNom});
			
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setUseTextField(true);
			txtFechaInicial.setValue(new Date());
			txtFechaFinal.setUseTextField(true);
			txtFechaFinal.setValue(new Date());
			
			//+++++++++++++ nom, ape, ruc ++++++++++++++
			txtRuc.setValue("");
			txtNom.setValue("");
			txtApe.setValue("");

			cmbDocumento.setRequired(true);
			cmbDocumento.setValueMap("Facturas de Venta","Notas de Entrega","Facturas de Compra",
					"Notas de Compra","Notas de Credito","Notas de Credito Proveedores","Anticipo Clientes","Ajustes de Inventario",
					"Gastos","Ajustes de Inventario Inicial","Consignacion");			
			cmbDocumento.addChangedHandler(new ManejadorBotones("cmbDocumento"));
			cmbDocumento.setDefaultToFirstOption(true);
			dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, cmbDocumento});
			//addMember(dynamicForm);		
			hStackSuperior.setSize("100%", "20%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(dynamicForm2);
			//hStackSuperior.addMember(hStackEspacio);
			addMember(hStackSuperior);
//			lstReporteCaja.setSize("100%", "50%");
			id =new ListGridField("id", "id");
			NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
			TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
			Fecha =new ListGridField("Fecha", "Fecha");
			Fecha.setAlign(Alignment.CENTER);
			FechadeExpiracion=new ListGridField("FechadeExpiracion", "Fechade Expiracion");
			Vendedor=new ListGridField("Vendedor", "Vendedor");
			Cliente =new ListGridField("Cliente", "Cliente");
			Autorizacion =new ListGridField("Autorizacion", "Autorizacion",100);
			NumPagos =new ListGridField("NumPagos", "Numero de Pagos");
			Estado =new ListGridField("Estado", "Estado");
			Iva14 =new ListGridField("Iva14", "Subt. IVA 12%");
			Iva0 =new ListGridField("Iva0", "Subt. IVA 0%");
			//Iva0.setSummaryFunction(SummaryFunctionType.SUM);
			//Iva0.setShowGridSummary(true);
			Iva =new ListGridField("Iva", "IVA",50); 
			//Iva.setSummaryFunction(SummaryFunctionType.SUM);
			//Iva.setShowGridSummary(true);
			Total =new ListGridField("Total", "Total",50);
			//Total.setSummaryFunction(SummaryFunctionType.SUM);
			//Total.setShowGridSummary(true);
			SubTotal =new ListGridField("SubTotal", "SubTotal Neto");
			//SubTotal.setSummaryFunction(SummaryFunctionType.SUM);
			//SubTotal.setShowGridSummary(true);
			Descuento =new ListGridField("Descuento", "Descuento");
			//Descuento.setSummaryFunction(SummaryFunctionType.SUM);
			//Descuento.setShowGridSummary(true);
			Retencion =new ListGridField("retencion", "Total Ret.");
			//Retencion.setSummaryFunction(SummaryFunctionType.SUM);
			//Retencion.setShowGridSummary(true);
			RetencionIVA =new ListGridField("retencionIVA", "Ret. IVA",50);
			//RetencionIVA.setSummaryFunction(SummaryFunctionType.SUM);
			//RetencionIVA.setShowGridSummary(true);
			RetencionRENTA =new ListGridField("retencionRENTA", "Ret. RENTA",65);
			//RetencionRENTA.setSummaryFunction(SummaryFunctionType.SUM);
			//RetencionRENTA.setShowGridSummary(true);
			NumeroRetencion =new ListGridField("numeroRETENCION", "Num. Ret.",100);
			NumeroFactura =new ListGridField("numeroFACTURA", "Num. Factura.",100);
			Ruc =new ListGridField("RUC", "RUC");

			lstDescripcion =new ListGridField("lstDescripcion", "Descripcion",400);
			lstCantidad =new ListGridField("lstCantidad", "Cantidad",70);
			lstPrecio =new ListGridField("lstPrecio", "Precio Unitario",70);
			lstTotal =new ListGridField("lstTotal", "Total",70);
			lstDescuento =new ListGridField("lstDescuento", "Descuento",70);
			lstEstado=new ListGridField("lstEstado", "Estado",70);
			lstNumRealTransaccion =new ListGridField("lstNumRealTransaccion", "Num Real Transaccion");
			lstFecha =new ListGridField("lstFecha", "Fecha");
			lstCliente =new ListGridField("lstCliente", "Cliente");
			lstRuc =new ListGridField("lstRUC", "RUC");
			lstVend =new ListGridField("lstVend","Vendedor");
			
			Cliente.setWidth("25%");
			Fecha.setWidth("7%"); 
			NumRealTransaccion.setWidth("5%");
			NumRealTransaccion.setAlign(Alignment.LEFT);
			Autorizacion.setWidth("10%");
			SubTotal.setWidth("6%");
			SubTotal.setAlign(Alignment.RIGHT);
			Iva.setWidth("6%");
			Iva.setAlign(Alignment.RIGHT);
			Iva14.setWidth("6%");
			Iva14.setAlign(Alignment.RIGHT);
			Iva0.setWidth("6%");
			Iva0.setAlign(Alignment.RIGHT);
			Descuento.setWidth("5%");
			Descuento.setAlign(Alignment.RIGHT);
			Retencion.setWidth("5%");
			Retencion.setAlign(Alignment.RIGHT);
			Estado.setWidth("8%");
			Estado.setAlign(Alignment.CENTER);
			Ruc.setWidth("8%");
			NumeroRetencion.setWidth("8%");
			NumeroFactura.setWidth("6%");
			NumeroRetencion.setHidden(false);
			NumeroFactura.setHidden(true);
			Total.setAlign(Alignment.RIGHT);
			Total.setWidth("7%");
			RetencionIVA.setAlign(Alignment.RIGHT);
			RetencionRENTA.setAlign(Alignment.RIGHT);
			
			
			//lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,SubTotal,Descuento,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,Estado});
//			lstReporteCaja = new ListGrid();
			lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,NumeroFactura,SubTotal,Descuento,Iva14,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,Estado,NumeroRetencion});
			//lstReporteCaja.setShowAllRecords(true);
			//lstReporteCaja.setShowGridSummary(true);
			/*ListGridField expDat =new ListGridField();
			expDat.setCanExport(false);
			lstReporteCaja.setRowNumberFieldProperties(expDat);
			lstReporteCaja.setShowRowNumbers(true);*/
			lstReporteCajaDetalle = new ListGrid();
			lstReporteCajaDetalle.setFields(new ListGridField[] {lstFecha,lstNumRealTransaccion,lstVend,lstCliente,lstRuc,lstDescripcion,lstCantidad,lstPrecio,lstDescuento,lstTotal,lstEstado});
		//	lstReporteCaja.setCanReorderRecords(false);
			//lstReporteCaja.sort("Cliente", SortDirection.ASCENDING);
			//lstReporteCaja.groupBy("Cliente");
			//lstReporteCaja.setGroupStartOpen(GroupStartOpen.ALL);
			addMember(lstReporteCaja);
			
			HStack hStack = new HStack();
			hStack.setSize("100%", "5%");
			
			
			hStack.addMember(btnGenerarReporte);
			btnGenerarReporte.addClickHandler(new ManejadorBotones("generar"));
			
			IButton btnImprimir = new IButton("Imprimir");
			IButton btnExportar = new IButton("Exportar");
			IButton btnLimpiar = new IButton("Limpiar");
			btnExportarDet = new IButton("Exportar Detalle");
			btnExportarDet.setVisible(true);
			hStack.addMember(btnLimpiar);
			hStack.addMember(btnImprimir);
			hStack.addMember(btnExportar);
			hStack.addMember(btnExportarDet);
			btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
			btnExportar.addClickHandler(new ManejadorBotones("exportar"));
			btnExportarDet.addClickHandler(new ManejadorBotones("exportarDet"));
			btnLimpiar.addClickHandler(new ManejadorBotones("limpiar"));
			
			ListGridTotales.setSize("100%", "30%");
			ListGridField lstSubtNeto = new ListGridField("lstSubtNeto", "Subtotal Neto");
			lstSubtNeto.setAlign(Alignment.RIGHT);
			ListGridField lstSubtIVA14 = new ListGridField("lstSubtIVA14", "Subtotal IVA 12%");
			lstSubtIVA14.setAlign(Alignment.RIGHT);
			ListGridField lstSubtIVA0 = new ListGridField("lstSubtIVA0", "Subtotal IVA 0%");
			lstSubtIVA0.setAlign(Alignment.RIGHT);
			ListGridField lstIVA = new ListGridField("lstIVA", "IVA");
			lstIVA.setAlign(Alignment.RIGHT);
			ListGridField lstTOTAL = new ListGridField("lstTOTAL", "TOTAL");
			lstTOTAL.setAlign(Alignment.RIGHT);
			ListGridField lstRetencion = new ListGridField("retencion", "RETENCION"); 
			lstRetencion.setAlign(Alignment.RIGHT);
			ListGridField lstDescuento = new ListGridField("Descuento", "DESCUENTO");
			lstDescuento.setAlign(Alignment.RIGHT);
			ListGridField lstRETIVA = new ListGridField("totretencionIVA", "RET. IVA");
			lstRETIVA.setAlign(Alignment.RIGHT);
			ListGridField lstRETRENTA = new ListGridField("totretencionRENTA", "RET. RENTA");
			lstRETRENTA.setAlign(Alignment.RIGHT);
			ListGridTotales.setFields(new ListGridField[] { lstSubtNeto,lstDescuento,lstSubtIVA14,lstSubtIVA0,lstIVA,lstTOTAL,lstRETIVA,lstRETRENTA,lstRetencion});
			addMember(ListGridTotales); 
			
			addMember(hStack);

			//cmbDocumento.redraw();
		}catch(Exception e ){
			SC.say("Error: "+e);
		}
		//cargaInicial();
	}
	
	
	private class ManejadorBotones implements DoubleClickHandler,KeyPressHandler,RecordDoubleClickHandler,FormItemClickHandler, com.smartgwt.client.widgets.events.ClickHandler, ChangedHandler{
		String indicador="";
		
		
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					btnExportarDet.setVisible(true);
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					btnExportarDet.setVisible(true);
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					btnExportarDet.setVisible(true);
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito Proveedores")){
					Tipo="14";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario")){
					Tipo="16";
				}else if(cmbDocumento.getDisplayValue().equals("Gastos")){
					Tipo="7";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
					btnExportarDet.setVisible(true);
					Tipo="10";
				}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario Inicial")){
					Tipo="17";
				}else if(cmbDocumento.getDisplayValue().equals("Consignacion")){
					Tipo="18";
				}
			else if(cmbDocumento.getDisplayValue().equals("Facturas Grandes")){
				Tipo="13";
			}
				//SC.say(idCliente+" vendedor="+idVendedor+" Tipo "+Tipo);
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		

		 
		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				datosfrm();
				//	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					
				
					if (txtNom.getDisplayValue().toString().compareTo("")==0&&txtApe.getDisplayValue().toString().compareTo("")==0&&txtRuc.getDisplayValue().toString().compareTo("")==0)
					{
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.BLOCK);
						getService().listarFacturas(fechaI, fechaF,Tipo, listaCallback);
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					}
					else
					{
						
						//SC.say("cedula: "+txtRuc.getValue().toString()+" nombres: "+txtNomComercial.getValue().toString()+" apellidos: "+txtRazonSocial.getValue().toString());
				//		SC.say("entre en el else de generar");
			
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");			
						getService().listarFacturasNumDto(fechaI, fechaF,Tipo,Tipo,listaCallback);
						
						// Esto cambi�
//						getService().listarReportesCliente(fechaI, fechaF, Tipo, txtRuc.getDisplayValue().toString(), txtNom.getDisplayValue().toString(), txtApe.getDisplayValue().toString(), listaCallback);
						
						
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					}	
					
			}else if(indicador.equalsIgnoreCase("limpiar")){
				txtRuc.setValue("");
				txtNom.setValue("");
				txtApe.setValue("");
			}else if(indicador.equalsIgnoreCase("imprimir")){
				VLayout espacio = new VLayout(); 
				espacio.setSize("100%","5%");
				Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +				  
			   		"&nbsp;&nbsp;&nbsp;Reporte",espacio,hStackSuperior,lstReporteCaja,ListGridTotales};					   
							
		        Canvas.showPrintPreview(a);
			}else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstReporteCaja);
					
			}else if(indicador.equalsIgnoreCase("exportarDet")){
				CreateExelDTO exel=new CreateExelDTO(lstReporteCajaDetalle);
			}
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			
			
		} 

		@Override
		public void onKeyPress(KeyPressEvent event) {

		}		
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
		} 

		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			
		}

		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("cmbDocumento")){
				btnExportarDet.setVisible(false);
				if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){	

					NumeroRetencion.setHidden(false);
					NumeroFactura.setHidden(false);
					lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,NumeroFactura,SubTotal,Descuento,Iva14,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,NumeroRetencion,Estado});
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					NumeroRetencion.setHidden(false);
					NumeroFactura.setHidden(true);
					lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,NumeroFactura,SubTotal,Descuento,Iva14,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,NumeroRetencion,Estado});
				}else{
					NumeroRetencion.setHidden(true);
					NumeroFactura.setHidden(true);
					lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,NumeroFactura,SubTotal,Descuento,Iva14,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,NumeroRetencion,Estado});
				}
				
				
				//lstReporteCaja.setShowRowNumbers(true);
				redraw();
			}
			
		}

		

		
	}
	final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.NONE);
			//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
		if (result.size()>0)
		{	
			//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			//ListGridRecord[] listadoDet = new ListGridRecord[result.size()];
			LinkedList<ListGridRecord> listadoDetAux = new LinkedList<ListGridRecord>();
			for(int i=0;i<result.size();i++)
			{
				//SC.say("Tama?o: "+result.size());
				double subtNeto=0;
				DtocomercialRecords dtocomercialrecords = new DtocomercialRecords(result.get(i));
				listado[i]=(dtocomercialrecords);
				/*if (!Tipo.equals("7") && !Tipo.equals("4") && !Tipo.equals("15") && !Tipo.equals("12")){
				subtNeto = listado[i].getAttributeAsDouble("Iva14");
				listado[i].setAttribute("SubTotal",Math.rint(subtNeto*100)/100);
				}*/
				if(Tipo.equals("1") || Tipo.equals("10")){
					listado[i].setAttribute("numeroRETENCION",result.get(i).getNumeroRetencion());
					listado[i].setAttribute("numeroFACTURA",result.get(i).getNumCompra());
					for (DtoComDetalleDTO detalleList: result.get(i).getTbldtocomercialdetalles()){
						ListGridRecord listAux= new ListGridRecord();
						listAux.setAttribute("lstFecha", result.get(i).getFecha());
						listAux.setAttribute("lstNumRealTransaccion", listado[i].getAttribute("NumRealTransaccion"));
						listAux.setAttribute("lstCliente", listado[i].getAttribute("Cliente"));
						listAux.setAttribute("lstRUC", listado[i].getAttribute("RUC"));
						listAux.setAttribute("lstVend", listado[i].getAttribute("Vendedor"));
						listAux.setAttribute("lstDescripcion", detalleList.getDesProducto());
						listAux.setAttribute("lstCantidad", detalleList.getCantidad());
						listAux.setAttribute("lstPrecio", detalleList.getPrecioUnitario());
						listAux.setAttribute("lstDescuento", detalleList.getDepartamento());
						listAux.setAttribute("lstTotal", detalleList.getTotal());
						listAux.setAttribute("lstEstado", listado[i].getAttribute("Estado"));
						
						listadoDetAux.add(listAux);
					}
				}else if(Tipo.equals("0") || Tipo.equals("2")){
					listado[i].setAttribute("numeroRETENCION",result.get(i).getNumeroRetencion());
					for (DtoComDetalleDTO detalleList: result.get(i).getTbldtocomercialdetalles()){
						ListGridRecord listAux= new ListGridRecord();
						listAux.setAttribute("lstFecha", result.get(i).getFecha());
						listAux.setAttribute("lstNumRealTransaccion", listado[i].getAttribute("NumRealTransaccion"));
						listAux.setAttribute("lstCliente", listado[i].getAttribute("Cliente"));
						listAux.setAttribute("lstRUC", listado[i].getAttribute("RUC"));
						listAux.setAttribute("lstVend", listado[i].getAttribute("Vendedor"));
						listAux.setAttribute("lstDescripcion", detalleList.getDesProducto());
						listAux.setAttribute("lstCantidad", detalleList.getCantidad());
						listAux.setAttribute("lstPrecio", detalleList.getPrecioUnitario());
						listAux.setAttribute("lstDescuento", detalleList.getDepartamento());
						listAux.setAttribute("lstTotal", detalleList.getTotal());
						listAux.setAttribute("lstEstado", listado[i].getAttribute("Estado"));
						listadoDetAux.add(listAux);
					}
				}
				/*DtoComDetalleDTO det = new DtoComDetalleDTO();
				Iterator resultadoDet = result.get(i).getTbldtocomercialdetalles().iterator();
				int cont=0;
				while (resultadoDet.hasNext()) {
					ListGridRecord listAux= new ListGridRecord();
					det = (DtoComDetalleDTO) resultadoDet.next();
					listAux.setAttribute("lstDescripcion", det.getDesProducto());
					listAux.setAttribute("lstCantidad", det.getCantidad());
					listAux.setAttribute("lstPrecio", det.getPrecioUnitario());
					listAux.setAttribute("lstTotal", det.getTotal());
					listAux.setAttribute("lstDescuento", det.getDepartamento());
					listAux.setAttribute("lstEstado", result.get(i).getEstado());
					listAux.setAttribute("lstNumrealTransaccion", result.get(i).getNumRealTransaccion());
					listAux.setAttribute("lstFecha", result.get(i).getFecha());
					listAux.setAttribute("lstCliente", listado[i].getAttribute("Cliente"));
					listAux.setAttribute("lstRUC", listado[i].getAttribute("RUC"));
					listadoDetAux.add(listAux);
					listadoDet[cont].setAttribute("lstDescripcion", det.getDesProducto());
					listadoDet[cont].setAttribute("lstCantidad", det.getCantidad());
					listadoDet[cont].setAttribute("lstPrecio", det.getPrecioUnitario());
					listadoDet[cont].setAttribute("lstTotal", det.getTotal());
					listadoDet[cont].setAttribute("lstDescuento", det.getDepartamento());
					listadoDet[cont].setAttribute("lstEstado", result.get(i).getEstado());
					listadoDet[cont].setAttribute("lstNumrealTransaccion", result.get(i).getNumRealTransaccion());
					listadoDet[cont].setAttribute("lstFecha", result.get(i).getFecha());
					listadoDet[cont].setAttribute("lstCliente", listado[i].getAttribute("Cliente"));
					listadoDet[cont].setAttribute("lstRUC", listado[i].getAttribute("RUC"));
					cont++;
				}*/
				
				/*double iva=0;
				iva=listado[i].getAttributeAsDouble("Iva12");
				iva=iva*12/100;
				listado[i].setAttribute("Iva", Math.rint(iva*100)/100);*/
				//double tot=0;
				//tot=listado[i].getAttributeAsDouble("Total");
				//listado[i].setAttribute("Total",Math.rint((subtNeto+iva+doc.getAttributeAsFloat("Iva0"))*100)/100);
			}
			if(Tipo.equals("0") || Tipo.equals("1") || Tipo.equals("2") || Tipo.equals("10")){
			ListGridRecord[] listadof = new ListGridRecord[listadoDetAux.size()];
			for(int i=0;i<listadoDetAux.size();i++){
				listadof[i] = new ListGridRecord();
				listadof[i]=listadoDetAux.get(i);
			}
			lstReporteCajaDetalle.setData(listadof);
			}
			//++++++++++++++++++++++++   PARA LA PARTE DE TOTALIZAR POR CLIENTE ++++++++++++
			
			lstReporteCaja.setData(listado);
			libro();
			
			
			
		//	libro();
		}//fin if >0
		else
		{
			ListGridRecord[] listadoCliente = new ListGridRecord[result.size()];
			lstReporteCaja.setData(listadoCliente);
			lstReporteCaja.redraw();
			lstReporteCaja.show();
			ListGridRecord[] listadototales = new ListGridRecord[1];
			listadototales[0]=new ListGridRecord();
			listadototales[0].setAttribute("lstSubtNeto","0");
			listadototales[0].setAttribute("lstSubtIVA14","0");
			listadototales[0].setAttribute("lstSubtIVA0","0");
			listadototales[0].setAttribute("lstIVA","0");
			listadototales[0].setAttribute("lstTOTAL","0");
			listadototales[0].setAttribute("retencion","0");
			listadototales[0].setAttribute("Descuento","0");
			ListGridTotales.setData(listadototales);
			SC.say("No hay resultados que cumplan con los par&aacute;metros establecidos");
		}
		RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.NONE);
		//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	    }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public void libro(){
		ListGridRecord[] listado =lstReporteCaja.getRecords();
		
		double totRetIVA=0;
		double totRetRENTA=0;
		double totalClienteGeneral=0;
		double subTotalClienteGeneral=0;			
		double ivaClienteGeneral=0;
		double ivaCliente14General=0;
		double ivaClienteCeroGeneral=0;
		double descuentoClienteGeneral=0;
		double retencionClienteGeneral=0;
		for(int i=0;i<listado.length;i++){
			if(listado[i].getAttributeAsString("Estado").equals("CONFIRMADO")){
				totalClienteGeneral=totalClienteGeneral+Double.valueOf(listado[i].getAttributeAsString("Total"));
				subTotalClienteGeneral=subTotalClienteGeneral+Double.valueOf(listado[i].getAttributeAsString("SubTotal"));			
				ivaClienteGeneral=ivaClienteGeneral+Double.valueOf(listado[i].getAttributeAsString("Iva"));
				ivaCliente14General=ivaCliente14General+Double.valueOf(listado[i].getAttributeAsString("Iva14"));
				ivaClienteCeroGeneral=ivaClienteCeroGeneral+Double.valueOf(listado[i].getAttributeAsString("Iva0"));
				descuentoClienteGeneral=descuentoClienteGeneral+Double.valueOf(listado[i].getAttributeAsString("Descuento"));
				retencionClienteGeneral=retencionClienteGeneral+Double.valueOf(listado[i].getAttributeAsString("retencion"));
				totRetIVA=totRetIVA+Double.valueOf(listado[i].getAttributeAsString("retencionIVA"));
				totRetRENTA=totRetRENTA+Double.valueOf(listado[i].getAttributeAsString("retencionRENTA"));
			}
			
		}
		ListGridRecord[] listadototales = new ListGridRecord[1];
		listadototales[0]=new ListGridRecord();
		listadototales[0].setAttribute("lstSubtNeto",formatoDecimalN.format(subTotalClienteGeneral));
		listadototales[0].setAttribute("lstSubtIVA14",formatoDecimalN.format(ivaCliente14General));
		listadototales[0].setAttribute("lstSubtIVA0",formatoDecimalN.format(ivaClienteCeroGeneral));
		listadototales[0].setAttribute("lstIVA",formatoDecimalN.format(ivaClienteGeneral));
		listadototales[0].setAttribute("lstTOTAL",formatoDecimalN.format(totalClienteGeneral));
		listadototales[0].setAttribute("retencion",formatoDecimalN.format(retencionClienteGeneral));
		listadototales[0].setAttribute("Descuento",formatoDecimalN.format(descuentoClienteGeneral));
		listadototales[0].setAttribute("totretencionIVA",formatoDecimalN.format(totRetIVA));
		listadototales[0].setAttribute("totretencionRENTA",formatoDecimalN.format(totRetRENTA));
//		listadototales[0]=new ListGridRecord();
//		listadototales[0].setAttribute("lstSubtNeto",Math.rint(subTotalClienteGeneral*100)/100);
//		listadototales[0].setAttribute("lstSubtIVA14",Math.rint(ivaCliente14General*100)/100);
//		listadototales[0].setAttribute("lstSubtIVA0",Math.rint(ivaClienteCeroGeneral*100)/100);
//		listadototales[0].setAttribute("lstIVA",Math.rint(ivaClienteGeneral*100)/100);
//		listadototales[0].setAttribute("lstTOTAL",Math.rint(totalClienteGeneral*100)/100);
//		listadototales[0].setAttribute("retencion",Math.rint(retencionClienteGeneral*100)/100);
//		listadototales[0].setAttribute("Descuento",Math.rint(descuentoClienteGeneral*100)/100);
//		listadototales[0].setAttribute("totretencionIVA",Math.rint(totRetIVA*100)/100);
//		listadototales[0].setAttribute("totretencionRENTA",Math.rint(totRetRENTA*100)/100);
		ListGridTotales.setData(listadototales);

	}
 	final AsyncCallback<ReportesDTO>  objbackTotal=new AsyncCallback<ReportesDTO>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 			
 		}
 		@Override
		public void onSuccess(ReportesDTO result) { 
 			ReportesDTO rep=result;
 			ListGridRecord[] listado = new ListGridRecord[1];
 			listado[0]=new ListGridRecord();
 			//,,,}
 			
 			listado[0].setAttribute("lstSubtNeto",Math.rint(rep.getSubtNeto()*100)/100);
 			listado[0].setAttribute("lstSubtIVA0",Math.rint(rep.getSubtIVA0()*100)/100);
 			listado[0].setAttribute("lstIVA",Math.rint(rep.getIVA()*100)/100);
 			listado[0].setAttribute("lstTOTAL",Math.rint(rep.getTOTAL()*100)/100);
 			ListGridTotales.setData(listado);
 			listado[0].getAttributes();
 			
 			//getService().listGridToExel(listado, objbackString);
		}
 	};  
 	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.NONE);
			//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.NONE);
			//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
		}
	};
 	public void cargaInicial(){
 		try{
	 		Date fechaActual = new Date();
	 		String fechaA = fechaActual.toString();
	 		RootPanel.get("cargando").getElement().getStyle().setDisplay(Display.BLOCK);
	 		//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturas(fechaA, fechaA,"0", listaCallback);
			/*getService().ReproteTotalIVADocumentos(fechaA, fechaA,"0", objbackIva);
			getService().ReproteTotalSinIVADocumentos(fechaA, fechaA,"0", objbackIva0);
			getService().ReproteSubTotalDocumentos(fechaA, fechaA,"0", objbackSub);*/
	 		SC.say("ENTRO A LA CARGA INICIAL: "+fechaA);}
 		catch(Exception e){
 			SC.say("ERROR: "+e.getMessage());
 		}
 	}
}
