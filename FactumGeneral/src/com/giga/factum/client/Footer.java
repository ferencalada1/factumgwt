package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.HLayout;

public class Footer extends HLayout{
	public Footer() {
		
		HLayout hLayout = new HLayout();
		hLayout.setWidth100();
		hLayout.setShowEdges(true);
		addMember(hLayout);
	}

}
