package com.giga.factum.client;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.regGrillas.Producto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.CellDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;

public class lstProductos extends VLayout{

	ListGrid lstProductos;
	public ListGridRecord registro[] = new ListGridRecord[1];
	Integer Indicador=0;
	Factura obj;
	public lstProductos(Integer indicador, Factura objFactura){
	//La variable indicador sirve para identificar desde donde fue llamado el form
		//en caso de que esta variable tenga el valor de 1 significa que fue llamada
		//para busqueda y se deben almacenar los variables de la selccion para luego
		//ser utilizados
	Indicador = indicador;
	setSize("100%", "100%");
	obj= objFactura;	
	//Variables para almacenar el producto seleccionado
	
	HLayout hLayout = new HLayout();
	hLayout.setSize("100%", "5%");
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));
	SearchForm searchForm = new SearchForm();
	searchForm.setSize("88%", "100%");
	searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
	searchForm.setNumCols(4);
	ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
    cbmBuscarLst.setLeft(162);
    cbmBuscarLst.setTop(4);
    cbmBuscarLst.setHint("Buscar por");
    cbmBuscarLst.setShowHintInField(true);
    TextItem txtBuscarTxt=new TextItem();
    txtBuscarTxt.setLeft(6);
    txtBuscarTxt.setTop(4);
    txtBuscarTxt.setHint("Buscar");
    txtBuscarTxt.setShowHintInField(true);
    txtBuscarTxt.setIcons(buscarPicker);
    txtBuscarTxt.addKeyPressHandler(new ManejadorBotones(""));
    StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
    staticTextItem.setLeft(648);
    staticTextItem.setTop(6);
    staticTextItem.setWidth(124);
    staticTextItem.setHeight(20);
    searchForm.setFields(new FormItem[] { txtBuscarTxt, cbmBuscarLst, staticTextItem});
	
	hLayout.addMember(searchForm);
	
	HStack hStack = new HStack();
	hStack.setSize("12%", "100%");
	TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
    TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
    TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
    TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
    TransferImgButton btnEliminarLst = new TransferImgButton(TransferImgButton.DELETE);
    
	hStack.addMember(btnInicio);
    hStack.addMember(btnAnterior);
    hStack.addMember(btnSiguiente);
    hStack.addMember(btnFin);
    hStack.addMember(btnEliminarLst);
	hLayout.addMember(hStack);
	addMember(hLayout);
	  
	lstProductos = new ListGrid();
	if(indicador == 1){
		lstProductos.setSize("300", "200");
	}else{
	lstProductos.setSize("100%", "80%");
	}
	lstProductos.setFields(new ListGridField[] { 
			new ListGridField("codigoBarras", "C\u00F3digo de Barras"), 
			new ListGridField("descripcion", "Descripci\u00F3n"), 
			new ListGridField("stock", "Stock"),
			new ListGridField("impuesto:", "Impuesto"),
			new ListGridField("lifo", "LIFO"),
			new ListGridField("fifo", "FIFO"),
			new ListGridField("promedio:", "Promedio"),
			new ListGridField("unidad", "Unidad"),
			new ListGridField("categoria", "Categoria"),
			new ListGridField("marca", "Marca")});
	lstProductos.addCellDoubleClickHandler(new ManejadorDobleClick());
	addMember(lstProductos);
	//Cargamos el listado de Productos
	getService().listaProductos(0, 10,1,0,-1,Factum.banderaStockNegativo,listaCallback);
	}
	
	public ListGrid obtenerListado(){
		return lstProductos;
	}
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler{
		String indicador="";

		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Buscar")){						
			}			
		}
		public void onKeyPress(KeyPressEvent event) {
			// TODO ejemplo de cosas para hacer
			if(event.getKeyName().equals("Enter")){			
			}
			//SC.say(event.getKeyName());	
		}
		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
		}

	}
	
	//Manejador de doble click de la Grilla
	private class ManejadorDobleClick implements CellDoubleClickHandler {

        public void onCellDoubleClick(CellDoubleClickEvent event) {
        	if(Indicador == 1)
        	{	//El contenedor fue llamado desde la facturacion en el doble click, se debe
        		//guardar los datos del producto y cerrar el formulario
        		registro = lstProductos.getSelection();
        		//SC.say("Estamos aca!!");
        		//obj.CargarProducto(registro);
        		//return Integer.parseInt(registro[0].getAttribute("NumOrden"));
        	}
        }
    }
	
	final AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			
		}

		public void onSuccess(LinkedHashMap<Integer, ProductoDTO> result) {
			// TODO Auto-generated method stub
			RecordList listado = new RecordList();
			Iterator iter = result.values().iterator();
            //Size = equiposenTaller.size();
            //ListGridRecord[] allrecords = new ListGridRecord[Size];
            while (iter.hasNext()) {
            	listado.add(new Producto((ProductoDTO)iter.next()));
            }
            lstProductos.setData(listado);
            lstProductos.redraw();
		}
		
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
