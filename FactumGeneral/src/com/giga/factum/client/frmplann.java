package com.giga.factum.client;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmplann extends VLayout{
	ListGrid listGrid = new ListGrid();
	public frmplann(){
		
		
		listGrid.setFields( new ListGridField("Col1"), new ListGridField("Col2"), new ListGridField("Col3"));
		Record[] data = new Record[5];
		int counter = 0;
		for (int i = 0; i < data.length; i++) {
			data[i] = new ListGridRecord();
			data[i].setAttribute("Col1", counter++);
			data[i].setAttribute("Col2", counter++);
			data[i].setAttribute("Col3", counter++);
		}
		listGrid.setData(data);
		
		IButton button = new IButton("Export CSV");
		button.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				StringBuilder exportedCSV = exportCSV(listGrid);
			//	exportedCSV.
				//System.out.println(exportedCSV);
				
				//SC.say(exportedCSV.toString());
			}
		});
		
		/*button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				StringBuilder exportedCSV = exportCSV(listGrid);
				System.out.println(exportedCSV);
				
			}
		});*/

		VLayout layout = new VLayout();
		layout.addMember(button);
		layout.addMember(listGrid);
		layout.draw();
	}
	
		


/**
 * Export data from a listrgrid
 * @param listGrid the {@link ListGrid}
 * @return a {@link StringBuilder} containing data in CSV format
 */
private StringBuilder exportCSV(ListGrid listGrid) {
	StringBuilder stringBuilder = new StringBuilder(); // csv data in here
	
	// column names
	ListGridField[] fields = listGrid.getFields();
	for (int i = 0; i < fields.length; i++) {
		ListGridField listGridField = fields[i];
		stringBuilder.append("\"");
		stringBuilder.append(listGridField.getName());
		stringBuilder.append("\",");
	}
	stringBuilder.deleteCharAt(stringBuilder.length() - 1); // remove last ","
	stringBuilder.append("\n");
	
	// column data
	ListGridRecord[] records = listGrid.getRecords();
	for (int i = 0; i < records.length; i++) {
		ListGridRecord listGridRecord = records[i];
		ListGridField[] listGridFields = listGrid.getFields();
		for (int j = 0; j < listGridFields.length; j++) {
			ListGridField listGridField = listGridFields[j];
			stringBuilder.append("\"");
			stringBuilder.append(listGridRecord.getAttribute(listGridField.getName()));
			stringBuilder.append("\",");
		}
		stringBuilder.deleteCharAt(stringBuilder.length() - 1); // remove last ","
		stringBuilder.append("\n");
	}
	return stringBuilder;
	

}
}
