package com.giga.factum.client;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.toolbar.ToolStripMenuButton;

public class MenuPrincipal extends HLayout{
	
	
	ToolStripMenuButton menuPrincipalPagos = new ToolStripMenuButton("<b id=\"para1\">Pagos</b>");
	public  MenuItem menupagosProveedores; 
	public  MenuItem menugastos;
	public  MenuItem menupagoss;
	public  MenuItem menuingresos;
	public  MenuItem menuCuadreCaja;
	
	ToolStripMenuButton menuVentas = new ToolStripMenuButton("<b id=\"para1\">Ventas</b>");
	public  MenuItem menuFact;
	public  MenuItem menuProforma;
	public  MenuItem menuNotaEntrega;
	
	public MenuItem menuNotaVentas;
		
	public  MenuItem menuRet;
	public  MenuItem menuCuentasPorCobrar;
	public  MenuItem meunFormasPago;
	public  MenuItem menuNotaCredito;
	public  MenuItem menuAnticipoCliente;
	public  MenuItem menuOperacionesAnticipoCliente;
	
	
	ToolStripMenuButton menuReportes = new ToolStripMenuButton("<b id=\"para1\">Reportes</b>");
	public  MenuItem menuReporte = new MenuItem("Reportes");
	public  MenuItem menuReportesEmpleados = new MenuItem("Reporte Ventas/Utilidad por usuario");
	public  MenuItem menuReportesUtilidadDetalle = new MenuItem("Reportes Utilidad Detalle");
	public  MenuItem menuReportesProductoProveedor = new MenuItem("Reportes Productos por Proveedor");
	public  MenuItem menuReportesProductoCliente = new MenuItem("Reportes Productos por Cliente");
	public  MenuItem menuIVAC = new MenuItem("Actualizar IVA");
	public  MenuItem menuReporteCaja = new MenuItem("Listado de Documentos");
	
	public  MenuItem menuDocComercial = new MenuItem("Documentos Comerciales");
	
	public  MenuItem menuReportesCliente = new MenuItem("Reportes Clientes");
	
	ToolStripMenuButton menuCompras = new ToolStripMenuButton("<b id=\"para1\">Compras</b>");
	public  MenuItem menuFactCompra;
	public MenuItem menuNotaVentasCompras;
	public  MenuItem menuNotaCreditoProveedor;
	public  MenuItem menuNotaCompra = new MenuItem("Nota de Compra");
	public  MenuItem menuAnticipoProveedor = new MenuItem("Anticipo Proveedores");
	public  MenuItem menuOpAntPro = new MenuItem("Operaciones Anticipos Proveedores");
	public  MenuItem menuRetCompra;
	
	ToolStripMenuButton menuInventario = new ToolStripMenuButton("<b id=\"para1\">Inventario</b>");
	public  MenuItem listadoItem = new MenuItem("Listado");
	public  MenuItem menuListadoBasico=new MenuItem("Listado Basico","application_form.png");
	public  MenuItem menuCatalogo =new MenuItem("Catalogo","cubes_all.png");
	public  MenuItem productosItem = new MenuItem("Productos");
	public  MenuItem menuUnidad=new MenuItem("Unidad","cube_blue.png");
	public  MenuItem menuProducto =new MenuItem("Producto","star_yellow.png");
	public  MenuItem menuServicio= new MenuItem("Servicio");
	public  MenuItem menuCategoria =new MenuItem("Categoria","cubes_all.png");
	public  MenuItem menuBodega =new MenuItem("Bodega","cube_blue.png");
	public  MenuItem menuAjusteInventario =new MenuItem("Ajuste de Inventario","application_form.png");
	public  MenuItem menuKardex =new MenuItem("Kardex","application_form.png");
	public  MenuItem menuMarca =new MenuItem("Marca");
	public  MenuItem menuTraspasoBodega =new MenuItem("Traspaso de Bodegas");
	public  MenuItem menuSeries =new MenuItem("Series");
	
	ToolStripMenuButton menuPersonas = new ToolStripMenuButton("<b id=\"para1\">Personas</b>");
	public MenuItem menuCliente=new MenuItem("Cliente","application_form.png");
	public MenuItem menuProveedor=new MenuItem("Proveedor","application_form.png");
	public MenuItem menuUsuario=new MenuItem("Usuario","application_form.png");
	
	//public  ToolStripButton menuPersonas;
	public  ToolStripButton menuBackup;
	
	//ToolStripMenuButton menuContabilidad2 = new ToolStripMenuButton("Contabilidad");
	
	//ToolStripMenuButton menuTaller = new ToolStripMenuButton("<b id=\"para1\">Taller</b>");
	public ToolStripButton menuTaller;
	
	/*public  MenuItem ordenMant;
	public  MenuItem crudMarcas;
	public  MenuItem crudEquipos;
	public  MenuItem reporteOrdenes;*/
	
	ToolStripMenuButton menuConfiguraciones = new ToolStripMenuButton("<b id=\"para1\">Configuraciones</b>");
	public  MenuItem menuEmpresa;
	public  MenuItem menuSubCuenta;
	public  MenuItem menuTcuenta; 
	public  MenuItem menuCuenta;
	public  MenuItem menuPcuenta;
	public  MenuItem menuImpuesto;
	public  MenuItem menuSubCargo;
	public  MenuItem menuCargo;
	public  MenuItem menuRoles;
	public  MenuItem menuPrintFac;
	public  MenuItem menuAsoCuentas;
	public  MenuItem menuFormasPago;
	public  MenuItem menuTipoPrecio;	
	
	
	ToolStripMenuButton menuRestaurant = new ToolStripMenuButton("<b id=\"para1\">Restaurant</b>");
	public  MenuItem menuPedido;
	public  MenuItem menuArea;
	public  MenuItem menuMesa;
	public  MenuItem menuOrden;
	
	ToolStripButton menuCore = new ToolStripButton("<b id=\"para1\">Core</b>");
	
	//CONTABILIDAD MENU
	
	ToolStripMenuButton menuContabilidad = new ToolStripMenuButton("<b id=\"para1\">Contabilidad</b>");
	public  MenuItem menuReporteRetenciones;
	public  MenuItem menuListaRetenciones;
	public  MenuItem menuATS;
	public  MenuItem menuReportesItem;
	public  MenuItem menuMayorizacion;
	public  MenuItem menuComprobacion;
	public  MenuItem menuBalanceGeneral;
	public  MenuItem menuEstadoResultados;
	public  MenuItem menuAsientosManuales;
	public  MenuItem menuLibroDiario;
	

	//ToolStripMenuButton menuElectronicos = new ToolStripMenuButton("<b id=\"para1\">Electronicos</b>");
	//public  MenuItem menuReporteElectronicos;
	
	
	public MenuPrincipal()
	{
		setHeight("50");
		setWidth100();
		setAlign(Alignment.LEFT);
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal");
		addMember(menuPrincipalVentas());
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal Ventas");
		addMember(MenuPrincipalCompras());
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal Compras");
		addMember(menuPrincipalPagos());
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal Pagos");
		addMember(MenuPrincipalInventario());
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal Inventario");
		addMember(MenuPrincipalPersonas());
		addMember(MenuPrincipalReportes());
		addMember(MenuPrincipalConfiguraciones());
		if(Factum.banderaMenuBackup==1){
			addMember(MenuPrincipalBackup());
		}
		if(Factum.banderaMenuTaller==1){
			addMember(MenuPrincipalTaller());
		}
		/*if(Factum.banderaMenuRestaurant==1){
			addMember(MenuPrincipalRestaurant());
		}*/
		if(Factum.banderaMenuCore==1){
			addMember(MenuPrincipalCore());
		}
		if(Factum.banderaMenuContabilidad==1){
			addMember(MenuPrincipalContabilidad());						
		}
		/*
		if(Factum.banderaMenuFacturacionElectronica==1){
			addMember(MenuPrincipalElectronicos());
		}
		*/
		//com.google.gwt.user.client.Window.alert("Constructor MenuPrincipal FIN");
	}
	
	
	public Canvas menuPrincipalPagos()
	{
		//menupagosProveedores.addClickHandler(new ManejadorBotones("PagosProveedores"));
		Menu menuP = new Menu();
		menupagosProveedores =new MenuItem("Pagos Proveedores");
		menupagoss = new MenuItem("Pagos Clientes");
		if(Factum.banderaMicroServicios==1) menugastos = new MenuItem("Gastos");
		menuingresos = new MenuItem("Ingresos");
		menuCuadreCaja = new MenuItem("Cuadrar Caja");
		menuP.addItem(menupagoss);
		menuP.addItem(menupagosProveedores);
		if(Factum.banderaMicroServicios==1) menuP.addItem(menugastos);
		menuP.addItem(menuingresos);
		menuP.addItem(menuCuadreCaja);
		menuPrincipalPagos.setMenu(menuP);
		
		return menuPrincipalPagos;
		
	}
	
	public Canvas menuPrincipalVentas()
	{
		menuVentas.setIcon("pawn_blue.png");
		Menu menuV = new Menu();
		menuFact = new MenuItem("Factura");
		menuFact.setIcon("application_form.png");
		menuProforma=new MenuItem("Proforma");
		menuNotaEntrega = new MenuItem("Nota de Entrega");
		menuNotaEntrega.setIcon("cuentasporcobrar.png");
		
		menuNotaVentas=new MenuItem("Nota de Venta");
		
		
		menuRet = new MenuItem("Retenciones");
		menuRet.setIcon("application_form.png");		
		menuNotaCredito= new MenuItem("Nota de Credito");
		menuNotaCredito.setIcon("cuentasporcobrar.png");
		menuAnticipoCliente=new MenuItem("Anticipo Clientes");
		menuAnticipoCliente.setIcon("cuentasporcobrar.png");		
		menuCuentasPorCobrar = new MenuItem("Cuentas por Cobrar");
		menuOperacionesAnticipoCliente= new MenuItem("Operaciones Anticipo Clientes");
			//MenuItem menuOpAntCli = new MenuItem("Operaciones Anticipos Clientes");
			//menuOpAntCli.setIcon("application_form.png");
			//menuOpAntCli.addClickHandler(new ManejadorBotones("Operaciones Anticipos Clientes"));
		//if(Factum.empresa.)
		/*if (Factum.empresa.getTipo_contribuyente()!='2')*/ menuV.addItem(menuFact);
		/*if (Factum.empresa.getTipo_contribuyente()=='2')*/ menuV.addItem(menuNotaVentas);
		if(Factum.banderaMenuProforma==1){
			menuV.addItem(menuProforma);
		}
		menuV.addItem(menuNotaEntrega);
		menuV.addItem(menuNotaCredito);
		if(Factum.banderaMenuCuentasPorCobrar==1){
			menuV.addItem(menuCuentasPorCobrar);
		}
		menuV.addItem(menuAnticipoCliente);
			//menuV.addItem(menuReporteCaja);
			//menuV.addItem(menuCuentasPorCobrar);
		menuV.addItem(menuOperacionesAnticipoCliente);
		menuV.addItem(menuRet);
			//menuV.addItem(menuReporteCaja);
			//menuV.addItem(menuOpAntCli);
		menuVentas.setMenu(menuV);
		return menuVentas;
		
	}
	
	public Canvas MenuPrincipalReportes()
	{
		Menu menuConta = new Menu();
		menuIVAC.setEnabled(false);
		menuConta.addItem(menuReporte);
		menuConta.addItem(menuReporteCaja);
		menuConta.addItem(menuReportesEmpleados);
		menuConta.addItem(menuReportesUtilidadDetalle);
		menuConta.addItem(menuReportesProductoProveedor);
		menuConta.addItem(menuReportesProductoCliente);
		menuConta.addItem(menuReportesCliente);
		menuConta.addItem(menuDocComercial);
			//menuConta.addItem(menuProductosProveedor);
			//menuConta.addItem(menuIVAC);
		menuReportes.setMenu(menuConta);
			//menuConta.addItem(menuReportesClientes);
		menuReporteCaja.setIcon("book_open.png");
		return menuReportes;
	}
	
	public Canvas MenuPrincipalPersonas()
	{
		Menu menuPerson = new Menu();
		//menuPersonas.setIcon("pawn_green.png");
		
		//menuPerson.addItem(menuCliente);
		//menuPerson.addItem(menuProveedor);
		menuPerson.addItem(menuUsuario);
		menuPersonas.setMenu(menuPerson);
		return menuPersonas;
	}
	public Canvas MenuPrincipalTaller()
	{
		menuTaller = new ToolStripButton("<b id=\"para1\">Taller</b>");
		return menuTaller;
	}
	/*public Canvas MenuPrincipalTaller()
	{
		Menu mTaller = new Menu();    
		ordenMant = new MenuItem("Orden de mantenimiento");		
		crudMarcas = new MenuItem("Marcas");	
		crudEquipos = new MenuItem("Equipos");	
		reporteOrdenes = new MenuItem("Reportes de ordenes");
		mTaller.addItem(ordenMant);
		mTaller.addItem(crudMarcas);
		mTaller.addItem(crudEquipos);
		mTaller.addItem(reporteOrdenes);
		menuTaller.setMenu(mTaller);
		return menuTaller;
	}*/
	
	public Canvas MenuPrincipalCompras()
	{
		menuCompras.setIcon("pawn_yellow.png");
		Menu menuComp = new Menu();
		menuFactCompra = new MenuItem("Facturacion Compra");
		menuFactCompra.setIcon("application_form.png");
		
		menuComp.addItem(menuFactCompra);
		
		
		menuNotaVentasCompras=new MenuItem("Nota de Venta en Compras");
		menuNotaVentasCompras.setIcon("application_form.png");	
		menuComp.addItem(menuNotaVentasCompras);
		
		menuNotaCompra.setIcon("application_form.png");
		menuComp.addItem(menuNotaCompra);
		
		menuNotaCreditoProveedor = new MenuItem("Nota de Credito Proveedores");
		menuNotaCreditoProveedor.setIcon("application_form.png");
		menuComp.addItem(menuNotaCreditoProveedor);
		
		menuAnticipoProveedor.setIcon("application_form.png");
		menuComp.addItem(menuAnticipoProveedor);
		//+++++++   PARA REALIZAR OPERACIONES CON LOS ANTICIPOS DE PROVEEDORES   ++++++++++
		menuOpAntPro.setIcon("application_form.png");
		menuComp.addItem(menuOpAntPro);
		
		menuRetCompra = new MenuItem("Retenciones Compras");
		menuRetCompra.setIcon("application_form.png");
		menuComp.addItem(menuRetCompra);
		
		menuCompras.setMenu(menuComp);
		return menuCompras;
	}
	
	public Canvas MenuPrincipalInventario(){
		menuInventario.setIcon("database_edit.png");
		Menu menuI = new Menu();
		productosItem.setIcon("pieces.png");
		Menu exportSM = new Menu();  
		exportSM.addItem(menuUnidad);  
		exportSM.addItem(menuProducto); 
		exportSM.addItem(menuMarca);
		
		exportSM.addItem(menuSeries); 
		if(Factum.banderaMenuServicio==1){
		exportSM.addItem(menuServicio);  
		}
		exportSM.addItem(menuCategoria);  
		if(Factum.banderaMenuBodega==1){
		exportSM.addItem(menuBodega);  // Version basica sin bodegas  
		exportSM.addItem(menuTraspasoBodega);
		}
		if(Factum.banderaMicroServicios==1){
			exportSM.addItem(menuAjusteInventario);  
			exportSM.addItem(menuKardex);  
		}
        productosItem.setSubmenu(exportSM);
        listadoItem.setIcon("pieces.png");
		Menu listadoSM = new Menu();  
		
		if(Factum.banderaMicroServicios==1) listadoSM.addItem(menuListadoBasico);  
		listadoSM.addItem(menuCatalogo);
		listadoItem.setSubmenu(listadoSM);
		menuI.addItem(listadoItem);
		menuI.addItem(productosItem);
		menuInventario.setMenu(menuI);
		
		
		return menuInventario;
	}
	
	public Canvas MenuPrincipalBackup()
	{
		menuBackup = new ToolStripButton("<b id=\"para1\">Backup</b>");
		return menuBackup;
	}
	
	public Canvas MenuPrincipalContabilidad()
	{
		Menu menuConta = new Menu();
		
		if(Factum.banderaMicroServicios==1) menuReporteRetenciones = new MenuItem("Reporte Retenciones");
		menuListaRetenciones = new MenuItem("Lista Retenciones");
		if(Factum.banderaMicroServicios==1) menuATS = new MenuItem("ATS");
		menuReportesItem = new MenuItem("Reportes Contabilidad");
		menuMayorizacion=new MenuItem("Mayorizacion");
		menuComprobacion=new MenuItem("Comprobacion");
		menuBalanceGeneral=new MenuItem("Balance General");
		menuEstadoResultados=new MenuItem("Estado Resultados");
		menuAsientosManuales=new MenuItem("Asientos Manuales");
		menuLibroDiario= new MenuItem("Libro Diario");
		Menu exportRC = new Menu();  
        exportRC.setItems(  
        		menuMayorizacion,
        		menuComprobacion,
        		menuBalanceGeneral,
        		menuEstadoResultados
        		);  
        		
        menuReportesItem.setSubmenu(exportRC);
        if(Factum.banderaMicroServicios==1) menuConta.addItem(menuReporteRetenciones);
		menuConta.addItem(menuListaRetenciones);
		if(Factum.banderaMicroServicios==1) menuConta.addItem(menuATS);
		menuConta.addItem(menuReportesItem);
		if(Factum.banderaMenuLibroDiario==1){
			menuConta.addItem(menuLibroDiario);
		}
		menuConta.addItem(menuAsientosManuales);
		menuContabilidad.setMenu(menuConta);
		return menuContabilidad;
	}	
	
	public Canvas MenuPrincipalConfiguraciones()
	{
		Menu menuC = new Menu();
		menuEmpresa = new MenuItem("Empresa","pawn_white.png");
		menuSubCuenta=new MenuItem("Cuentas");
		menuTcuenta= new MenuItem("Tipo de Cuenta","piece_red.png");
		menuCuenta= new MenuItem("Cuenta","piece_blue.png");
		menuPcuenta = new MenuItem("Plan de Cuentas","piece_green.png");
		menuImpuesto = new MenuItem("Impuestos","piece_green.png");
		menuSubCargo=new MenuItem("Configuracion Personas");
		menuCargo = new MenuItem("Cargos Personas","piece_green.png");
		menuRoles=new MenuItem("Cargos Roles","piece_blue.png");
		menuPrintFac = new MenuItem("Pantalla de Impresion Factura","piece_green.png");
		menuAsoCuentas = new MenuItem("Aso Cuentas","piece_green.png");
		menuTipoPrecio = new MenuItem("Tipo de Precio","piece_green.png");
		menuFormasPago = new MenuItem("Formas de Pago","piece_green.png");
		Menu export = new Menu();
		export.setItems(menuCargo,menuRoles);
		menuSubCargo.setSubmenu(export);
		Menu exportRC = new Menu();  
        exportRC.setItems( menuTcuenta,
        				   menuCuenta,
        				   menuPcuenta,
        				   menuAsoCuentas
        					);
        menuSubCuenta.setSubmenu(exportRC);
		menuC.addItem(menuEmpresa);
		menuC.addItem(menuSubCuenta);
		menuC.addItem(menuSubCargo);
		menuC.addItem(menuImpuesto);
		menuC.addItem(menuPrintFac);
		menuC.addItem(menuTipoPrecio);
		menuC.addItem(menuFormasPago);
		
		menuConfiguraciones.setMenu(menuC);
		return menuConfiguraciones;
	}
	
	public Canvas MenuPrincipalRestaurant()
	{
		Menu menuC = new Menu();
		menuPedido = new MenuItem("Pedido");	
		menuArea = new MenuItem("Area");
		menuMesa = new MenuItem("Mesa");
		menuC.addItem(menuPedido);	
		menuC.addItem(menuArea);	
		menuC.addItem(menuMesa);		
		menuRestaurant.setMenu(menuC);
		return menuRestaurant;
	}
	
	public Canvas MenuPrincipalCore()
	{
		menuCore= new ToolStripButton("<b id=\"para1\">Core</b>");
		menuCore.setIcon("pawn_green.png");
		return menuCore;
	}
	/*
	public Canvas MenuPrincipalElectronicos()
	{
		Menu menuC = new Menu();
		menuReporteElectronicos = new MenuItem("Listado de Documentos Electr&oacute;nicos");
		menuC.addItem(menuReporteElectronicos);
		menuElectronicos.setMenu(menuC);
		return menuElectronicos;
	}
	*/
	
}