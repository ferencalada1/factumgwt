package com.giga.factum.client;

import java.util.HashSet;
import com.smartgwt.client.widgets.events.ClickHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Date;

import com.giga.factum.client.Cuerpo.EmployeeTreeNode;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.MovimientoCuentaPlanCuentaDTO;
import com.giga.factum.client.regGrillas.CuentaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.RecordSummaryFunctionType;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.ListGridSummaryField;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;

public class frmLibroDiario extends VLayout{
	
	ListGrid listGrid = new ListGrid();
	DateTimeItem txtFechaI =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaF =new DateTimeItem("txtFechaFinal","Fecha Final");
	DynamicForm dynamicForm = new DynamicForm();
    String planCuentaID;
    
	public frmLibroDiario() {  
		getService().getUserFromSession(callbackUser);
		txtFechaI.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaI.setRequired(true);
		txtFechaF.setRequired(true);
		txtFechaI.setMaskDateSeparator("-");
		txtFechaF.setMaskDateSeparator("-");
		txtFechaF.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaF.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaI.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaI.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaI.setUseTextField(true);
		txtFechaI.setValue(new Date());
		txtFechaF.setUseTextField(true);
		txtFechaF.setValue(new Date());
		
		ListGridField lstidMovimiento =new ListGridField("lstidMovimiento", "MOVIMIENTO");
        ListGridField lstidPlan =new ListGridField("lstidPlan", "PLAN");
        ListGridField lstcodigoCuenta =new ListGridField("lstcodigoCuenta", "COD. CUENTA");
        ListGridField lstnombreCuenta =new ListGridField("lstnombreCuenta", "CUENTA");
        ListGridField lstvalor =new ListGridField("lstvalor", "VALOR");
        ListGridField lstdebe =new ListGridField("lstdebe", "DEBE");
        ListGridField lsthaber =new ListGridField("lsthaber", "HABER");
        ListGridField lstsigno =new ListGridField("lstsigno", "signo");
        
        
        dynamicForm.setSize("100%", "10%");
        txtFechaF.setAlign(Alignment.LEFT);
        txtFechaI.setAlign(Alignment.LEFT);
        dynamicForm.setFields(new FormItem[] { txtFechaI, txtFechaF});
        addMember(dynamicForm);
        listGrid.setSize("100%", "85%");
        
        listGrid.setFields(new ListGridField[] { lstidMovimiento,lstidPlan,lstcodigoCuenta,lstnombreCuenta,lstdebe,lsthaber});
        addMember(listGrid);
		
        
        
        HStack hStack = new HStack();
        hStack.setSize("100%", "5%");
        
        IButton btnGenerar = new IButton("Generar");
        hStack.addMember(btnGenerar);
        
        IButton btnImprimir = new IButton("Imprimir");
        hStack.addMember(btnImprimir);
        btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
        addMember(hStack);
        btnGenerar.addClickHandler(new ManejadorBotones("Generar"));
        //DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
        //getService().listarCuenta(0,2000,planCuentaID, objbacklstcuenta);
	
    }
	private class ManejadorBotones implements ClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("Generar")){
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				getService().listarLibro(txtFechaI.getDisplayValue(),txtFechaF.getDisplayValue(), planCuentaID,objbacklist);
			}else if(indicador.equals("imprimir")){
				Object[] a=new Object[]{dynamicForm,listGrid,};
                Canvas.showPrintPreview(a);  
			}
			
		}
	}
	
	final AsyncCallback<LinkedList<MovimientoCuentaPlanCuentaDTO>>  objbacklist=new AsyncCallback<LinkedList<MovimientoCuentaPlanCuentaDTO>>(){

		@Override
		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.getMessage());
			
		}

		@Override
		public void onSuccess(LinkedList<MovimientoCuentaPlanCuentaDTO> result) {
			
			ListGridRecord[] list = null;
			if(result.size()>0)
			{	
				list=new ListGridRecord[result.size()];
				int mov=result.get(0).getId().getIdMovimiento();
				list[0]=new ListGridRecord();
				list[0].setAttribute("lstidMovimiento", result.get(0).getId().getIdMovimiento());
				mov=result.get(0).getId().getIdMovimiento();
		
				list[0].setAttribute("lstidPlan", result.get(0).getId().getIdPlan());
				//for(int j=0;j<cuentas.size();j++){
					//if(cuentas.get(j).getIdCuenta()==result.get(0).getId().getIdCuenta()){
				list[0].setAttribute("lstcodigoCuenta",result.get(0).getTblcuentaPlancuenta().getTblcuenta().getcodigo());
				list[0].setAttribute("lstnombreCuenta",result.get(0).getTblcuentaPlancuenta().getTblcuenta().getNombreCuenta());
				
						//break;
					//}
				//}
			
				list[0].setAttribute("lstsigno",result.get(0).getSigno());
				String sig="";
				sig=list[0].getAttributeAsString("lstsigno");
				if(sig.equals("100")){
					list[0].setAttribute("lstdebe", result.get(0).getValor());
				}else{
					list[0].setAttribute("lsthaber", result.get(0).getValor());
				}
				
				for(int i=1;i<result.size();i++){
					list[i]=new ListGridRecord();
					if(mov!=result.get(i).getId().getIdMovimiento()){
					
					list[i].setAttribute("lstidMovimiento", result.get(i).getId().getIdMovimiento());
					mov=result.get(i).getId().getIdMovimiento();
					}
					list[i].setAttribute("lstidPlan", result.get(i).getId().getIdPlan());
					list[i].setAttribute("lstcodigoCuenta",result.get(i).getTblcuentaPlancuenta().getTblcuenta().getcodigo());
					list[i].setAttribute("lstnombreCuenta",result.get(i).getTblcuentaPlancuenta().getTblcuenta().getNombreCuenta());
					list[i].setAttribute("lstsigno",result.get(i).getSigno());
					sig="";
					sig=list[i].getAttributeAsString("lstsigno");
					//SC.say(sig);
					if(sig.equals("100")){
						list[i].setAttribute("lstdebe", result.get(i).getValor());
					}else{
						list[i].setAttribute("lsthaber", result.get(i).getValor());
					}
				
			}
			
		}//fin if size mayor a cero
		else
		{
			SC.say("No existen datos que cumplan con los parametros establecidos");
		}
			listGrid.setData(list);
			listGrid.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			//listGrid.redraw();
		}
		
	
	};
	
	/*
	final AsyncCallback<List<CuentaDTO>>  objbacklstcuenta=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(List<CuentaDTO> result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			cuentas=result;
       }
		
	};
	*/
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {
		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
				planCuentaID=result.getPlanCuenta();}
		}
		public void onFailure(Throwable caught) {
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
		}
	};			
	
	

	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
