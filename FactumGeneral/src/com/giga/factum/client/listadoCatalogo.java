package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;

import java.util.List;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.gargoylesoftware.htmlunit.javascript.host.Document;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.layout.client.Layout;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.layout.LayoutData;
import com.itextpdf.text.Anchor;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.VLayout;
//import com.google.gwt.user.client.Window;

public class listadoCatalogo extends VLayout{
	ListProductos listProductos = new ListProductos();
	DynamicForm dynamicForm = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtProducto = new TextItem("txtProducto", "Buscar Producto");
	ListGrid lstDocumento = new ListGrid();
	VentanaEmergente listadoProd ;
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnExportar = new IButton("Exportar Excel");
	
	
	String idProducto;
	Window winProducto;  
	Window winFactura1;  
	
	public listadoCatalogo() {
		
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Kardex/Kardex/page");  
		htmlPane.setContentsType(ContentsType.PAGE);
		addMember(htmlPane );
		//yourModalWindow.addItem(yourContentLayout );
		//Window wind;
		//String url = "http://localhost:8080/Kardex/Kardex/page";
		//HTML link = new HTML("<a href=\"" + url + "\" class=\"css-styling\" target=\"_parent\">Bla bla</a>");
		
        

     
		/*setSize("800", "600");
		
		lstDocumento = new ListGrid() {  
            @Override  
            protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {  
                if (getFieldName(colNum).equals("Estado")) {  
                	ListGridRecord listrecord =  record;  
                    if (listrecord.getAttributeAsString("Estado").equals("CONFIRMADO")) {  
                        return "font-weight:bold; color:#298a08;"; 
                    } else if (listrecord.getAttributeAsString("Estado").equals("NO CONFIRMADO")) {  
                        return "font-weight:bold; color:#287fd6;";  
                    } else if(listrecord.getAttributeAsString("Estado").equals("ANULADA")){  
                    	return "font-weight:bold; color:#d64949;"; 
                    }else{
                    	return super.getCellCSSText(record, rowNum, colNum);  
                    }
                } else {  
                    return super.getCellCSSText(record, rowNum, colNum);  
                }
            }  
        };  
		
		
		
		txtProducto.setRequired(true);
		txtProducto.setTooltip("Buscar producto");
		txtProducto.addKeyPressHandler( new ManejadorBotones("producto"));
		
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setUseTextField(true);
		txtFechaInicial.setValue(new Date());
		txtFechaFinal.setUseTextField(true);
		txtFechaFinal.setValue(new Date());
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, txtProducto});
		
		addMember(dynamicForm);
		
		
		ListGridField id =new ListGridField("id", "id");
		ListGridField NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
		ListGridField TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
		ListGridField Fecha =new ListGridField("Fecha", "Fecha");
		ListGridField FechadeExpiracion=new ListGridField("FechadeExpiracion", "Fechade Expiracion");
		ListGridField Vendedor=new ListGridField("Vendedor", "Vendedor");
		ListGridField Cliente =new ListGridField("Cliente", "Cliente");
		ListGridField Autorizacion =new ListGridField("Autorizacion", "Autorizacion");
		ListGridField Cantidad =new ListGridField("Cantidad", "Cantidad");
		ListGridField Estado =new ListGridField("Estado", "Estado");
		ListGridField SubTotal =new ListGridField("SubTotal", "SubTotal");
		lstDocumento.setFields(new ListGridField[] {NumRealTransaccion ,TipoTransaccion, Fecha,FechadeExpiracion , Vendedor,Cliente,
				Autorizacion,Cantidad,SubTotal});
		addMember(lstDocumento);
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		
		hStack.addMember(btnGenerarReporte);
		btnGenerarReporte.addClickHandler(new ManejadorBotones("generar")) ;
		IButton btnVerDocumento = new IButton("Ver Documento");
		hStack.addMember(btnVerDocumento);
		btnVerDocumento.addClickHandler(new ManejadorBotones("verDto")) ;
		btnExportar.addClickHandler(new ManejadorBotones("ExportarExcel"));
		hStack.addMember(btnExportar);
		
		addMember(hStack);*/
	}
	/*
	private class ManejadorBotones implements DoubleClickHandler,KeyPressHandler,RecordDoubleClickHandler,FormItemClickHandler, com.smartgwt.client.widgets.events.ClickHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				//SC.say(idCliente+" vendedor="+idVendedor+" Tipo "+Tipo);
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}

		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equals("generar")){
				if(dynamicForm.validate()){
					datosfrm();
					//SC.say("listando");
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().kardex(fechaI, fechaF, idProducto, listaCallback);
				}else{
					SC.say("Ingrese los datos necesarios");
				}
				
			}else if(indicador.equalsIgnoreCase("verDto")){
				Integer doc=0;
				String Tipo="0";
				winFactura1=new Window();
				try{
					if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Facturas de Venta")){
						Tipo="0";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Facturas de Compra")){
						Tipo="1";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Notas de Entrega")){
						Tipo="2";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Notas de Cr�dito")){
						Tipo="3";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Anticipo Clientes")){
						Tipo="4";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Ajustes de Inventario")){
						Tipo="5";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Notas de Compra")){
						Tipo="10";
					}else if(lstDocumento.getSelectedRecord().getAttributeAsString("TipoTransaccion").equals("Gastos")){
						Tipo="7";
					}
					doc=Integer.parseInt(lstDocumento.getSelectedRecord().getAttributeAsString("id"));
					winFactura1.setWidth(1100);  
	            	winFactura1.setHeight(700);  
	            	winFactura1.setTitle("Documento Comercial");  
	            	winFactura1.setShowMinimizeButton(false);  
	            	winFactura1.setIsModal(true);  
	            	winFactura1.setShowModalMask(true);  
	            	winFactura1.centerInPage();  
					Factura fac=new Factura(Integer.parseInt(Tipo),doc);//lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
					winFactura1.addCloseClickHandler(new CloseClickHandler() {  
		                public void onCloseClick(CloseClientEvent event) {  
		                	winFactura1.destroy();  
		                }  
		            });
					VLayout form = new VLayout();  
		            form.setSize("100%","100%"); 
		            form.setPadding(5);  
		            form.setLayoutAlign(VerticalAlignment.BOTTOM);
		            form.addMember(fac);
		            winFactura1.addItem(form);
		            winFactura1.show();
				}catch(Exception e){
					SC.say("Primero debe seleccionar una fila");
				}
				
			}else if(indicador.equalsIgnoreCase("ExportarExcel")){
				CreateExelDTO exel=new CreateExelDTO(lstDocumento);
			}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(indicador.equalsIgnoreCase("producto")){
				listProductos = new ListProductos();
				listProductos.lstProductos.addDoubleClickHandler( new ManejadorBotones("Seleccionar"));
				winProducto=new Window();
				winProducto.setWidth(930);  
				winProducto.setHeight(610);  
				winProducto.setTitle("Seleccione un producto");  
				winProducto.setShowMinimizeButton(false);  
				winProducto.setIsModal(true);  
				winProducto.setShowModalMask(true);  
				winProducto.centerInPage();  
				winProducto.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winProducto.destroy();
                    }  
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                
                listProductos.setSize("100%","100%"); 
                form.addMember(listProductos);
                winProducto.addItem(form);
                winProducto.show();
			}
		}

		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			idProducto=listProductos.lstProductos.getSelectedRecord().getAttribute("idProducto");
			//SC.say(listProductos.lstProductos.getSelectedRecord().getAttribute("descripcion"));
			txtProducto.setValue(listProductos.lstProductos.getSelectedRecord().getAttribute("descripcion"));
			winProducto.destroy();
			
		}
	
	}
	final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			idProducto="";
			txtProducto.setValue("");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];		
			for(int i=0;i<result.size();i++){
				DtocomercialRecords doc =new DtocomercialRecords(result.get(i));
				listado[i]=(new DtocomercialRecords(result.get(i)));
				Iterator iterTP =result.get(i).getTbldtocomercialdetalles().iterator();
				
				while (iterTP.hasNext()) {
					DtoComDetalleDTO det = new DtoComDetalleDTO();
					det = (DtoComDetalleDTO) iterTP.next();
					if(String.valueOf(det.getProducto().getIdProducto()).equals(idProducto)){
						listado[i].setAttribute("Cantidad", det.getCantidad());
						listado[i].setAttribute("SubTotal",Math.rint(det.getTotal()*100)/100 );
						break;
					}
				}
			}
			lstDocumento.setData(listado);
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        
	    }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}	
*/
}
