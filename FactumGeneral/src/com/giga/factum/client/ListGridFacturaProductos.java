package com.giga.factum.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ListGridFacturaProductos extends ListGrid{
	
	static ListGridField impuesto; 
	static ListGridField baseImponible; 
	static ListGridField lstBaseImponible; 
	static ListGridField lstPorcentaje; 
	static ListGridField lgfstockPadre; 
	static ListGridField lgfIDProducto; 
	static ListGridField lgfCantidad; // se declaran los campos de la grilla
	static ListGridField lgfCodigo; 
	static ListGridField lgfDescripcion; 
	static ListGridField lgfPrecio_unitario; 
	static ListGridField lgfValor_total; 
	static ListGridField lgfIva; 
	static ListGridField lgfStock; 
	static ListGridField lgfStockPadre; 
	static ListGridField lgfPrecio_iva; 
	static ListGridField lgfDEscuento; 
	static ListGridField lgfBodega; 
	static ListGridField lgfBodegaN; 
	static ListGridField lgfBodegaU; 
	static ListGridField lgfBodegaT; 
	static ListGridField lgfCosto; 
	static ListGridField lgfEliminar;
	
	static ListGridField lgfNumero; 
	
	public ListGridFacturaProductos()
	{
		impuesto= new ListGridField("lstImpuesto", "Impuesto");
		baseImponible= new ListGridField("baseImponible", "Base Imponible");
		lstBaseImponible= new ListGridField("lstBaseImponible", "Base Valor");
		lstPorcentaje= new ListGridField("lstPorcentaje", "% Retenci\u00F3n");
		lgfstockPadre= new ListGridField("stockPadre", "stockPadre", 1);
		lgfIDProducto= new ListGridField("idProducto", "I", 1);
		lgfCantidad= new ListGridField("cantidad", "Cantidad", 60);
		lgfCodigo= new ListGridField("codigoBarras", "Cod. Barras", 150);
		lgfDescripcion= new ListGridField("descripcion", "Descripci\u00f3n", 400);
		lgfPrecio_unitario= new ListGridField("precioUnitario", "Precio", 60);
		lgfValor_total= new ListGridField("valorTotal", "Subtotal", 100);
		lgfIva= new ListGridField("iva", "Iva", 30);
		lgfStock= new ListGridField("stock", "Stock", 60);
		lgfStockPadre= new ListGridField("stockPadre", "Stock M.P.", 60);
		lgfPrecio_iva= new ListGridField("precioConIva", "P.IVA", 60);
		lgfDEscuento= new ListGridField("descuento", "Descuento", 60);
		lgfBodega= new ListGridField("idbodega", "IdBodega", 1);
		lgfBodegaN= new ListGridField("bodega", "Bodega", 1);
		lgfBodegaU= new ListGridField("ubicacion", "Direccion", 1);
		lgfBodegaT= new ListGridField("telefono", "Cant.", 1);
		lgfCosto= new ListGridField("costo", "C", 1);
		lgfEliminar= new ListGridField("eliminar", " ", 25);
		lgfNumero= new ListGridField("numero", "#", 20);
		
		lgfIDProducto.setAlign(Alignment.CENTER);
		lgfCantidad.setAlign(Alignment.CENTER);
		lgfCodigo.setAlign(Alignment.CENTER);
		lgfDescripcion.setAlign(Alignment.CENTER);
		lgfPrecio_unitario.setAlign(Alignment.RIGHT);
		lgfValor_total.setAlign(Alignment.RIGHT);
		lgfIva.setAlign(Alignment.RIGHT);
		lgfCosto.setAlign(Alignment.RIGHT);
		lgfStock.setAlign(Alignment.CENTER);
		lgfDEscuento.setAlign(Alignment.CENTER);
		lgfBodega.setAlign(Alignment.CENTER);
		lgfBodegaN.setAlign(Alignment.CENTER);
		lgfBodegaU.setAlign(Alignment.CENTER);
		lgfBodegaT.setAlign(Alignment.CENTER);
		lgfPrecio_iva.setAlign(Alignment.RIGHT);
		// NumberFormat formato2 = NumberFormat.getFormat("#.00");
		lgfPrecio_iva.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				NumberFormat nf = NumberFormat.getFormat("#.00");
				try {
					return nf.format(((Number) value).floatValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		lgfNumero.setCellAlign(Alignment.CENTER);
		lgfEliminar.setAlign(Alignment.CENTER);
		lgfEliminar.setType(ListGridFieldType.IMAGE);
		lgfEliminar.setImgDir("delete.png");
		lgfValor_total.setCanEdit(false);
		lgfPrecio_iva.setCanEdit(true);
		
		lgfCantidad.setRequired(true);// los campos que son requeridos
		lgfCodigo.setRequired(true);
		lgfDescripcion.setRequired(true);
		lgfIDProducto.setRequired(true);
		lgfPrecio_unitario.setRequired(true);
		lgfPrecio_unitario.setAlign(Alignment.RIGHT);
		lgfPrecio_unitario.setType(ListGridFieldType.FLOAT);
		lgfPrecio_unitario.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					NumberFormat nf = NumberFormat.getFormat("#,##0.0000");
					return "$" + nf.format(((Number) value).doubleValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		
		lgfValor_total.setRequired(true);
		lgfDEscuento.setType(ListGridFieldType.TEXT);
		lgfBodega.setCanEdit(true);
		lgfBodegaN.setHidden(false);
		lgfBodegaU.setHidden(false);
		lgfBodegaT.setHidden(false);
		lgfCantidad.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setAlign(Alignment.RIGHT);
		lgfValor_total.setType(ListGridFieldType.FLOAT);
		lgfValor_total.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				try {
					NumberFormat nf = NumberFormat.getFormat("###0.0000");
					return "$" + nf.format(((Number) value).doubleValue());
				} catch (Exception e) {
					return value.toString();
				}
			}
		});
		
		lgfPrecio_unitario.setCanEdit(true);
		lgfEliminar.setCanEdit(false);
		lgfEliminar.setDefaultValue("   ");
		lgfStock.setCanEdit(false);
		setFields(lgfIDProducto, lgfNumero, lgfCodigo,lgfCantidad, lgfDescripcion,lgfPrecio_unitario, lgfIva,
				lgfStock,lgfDEscuento, /* lgfBodega, */lgfValor_total, lgfPrecio_iva, lgfEliminar,
				lgfBodega, lgfBodegaN, lgfBodegaU,/* lgfBodegaT, */lgfCosto);

		setHeight("188px");
		
	}

}
