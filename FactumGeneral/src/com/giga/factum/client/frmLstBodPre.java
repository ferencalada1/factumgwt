package com.giga.factum.client;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.ProductoBodegaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecio;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTOCompare;
import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.regGrillas.BodegaRecords;
import com.giga.factum.client.regGrillas.Producto;
import com.giga.factum.client.regGrillas.TipoPrecioRecords;
import com.giga.factum.server.base.TblproductoMultiImpuesto;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.Key;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;  
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;  
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.IButton;  
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;  
import com.smartgwt.client.widgets.grid.ListGridField;  
import com.smartgwt.client.widgets.grid.ListGridRecord;  

import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.ChangedEvent;
import com.smartgwt.client.widgets.grid.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;


public class frmLstBodPre extends VLayout{
	ListGridRecord[] listadoBod =null;
	//IButton btnGrabar = new IButton("Grabar");
	ListGrid listGrid = new ListGrid();
	ListGrid lstTipoPrecio=new ListGrid();
	ListGrid lstMultiImp = new ListGrid();
	ProductoDTO producto=new ProductoDTO();
	public ProductoDTO getProducto() {
		return producto;
	}
	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}
	//Label lblNewLabel;
	public  void setLstMultiData(RecordList listado){
		this.lstMultiImp.setData(listado);
	}
	
	public frmLstBodPre() {
		try{
		
		//En caso de ser 0 se lo llamaria del menu de Producto
		
		
		//lblNewLabel = new Label("Bodegas Disponibles / STOCK: "+pro.getStock());
		//lblNewLabel.setSize("100%", "5%");
		//addMember(lblNewLabel);
		
		/*ListGridField idBodega = new ListGridField("idBodega", "C\u00F3digo");
		//ListGridField Ban = new ListGridField("Ban", "Activo");
		ListGridField Bodega = new ListGridField("Bodega", "Bodega:");
		ListGridField Ubicacion = new ListGridField("Ubicacion", "Ubicaci\u00F3n:");
		ListGridField Telefono = new ListGridField("Telefono", "Tel\u00e9fono:");
		
        ListGridField cantidad = new ListGridField("Cantidad", "Cantidad"); 
        ListGridField stock = new ListGridField("Stock", "Stock"); 
        stock.setAlign(Alignment.CENTER);  
        stock.setCanEdit(false);
        stock.setType(ListGridFieldType.FLOAT);
		stock.setSummaryFunction(SummaryFunctionType.SUM);  
		stock.setShowGridSummary(true);
        cantidad.setAlign(Alignment.CENTER);  
        cantidad.setCanEdit(true);
        
        //cantidad.addEditorExitHandler(new ManejadorBotones("cantidad"));
        
        
        listGrid.setFields(idBodega, Bodega, Ubicacion, Telefono, cantidad, stock);  
        listGrid.setCanResizeFields(true);  
        listGrid.setShowRecordComponents(true);          
        listGrid.setShowRecordComponentsByCell(true);  
        listGrid.setCanRemoveRecords(true);  
        listGrid.setShowAllRecords(true);
        listGrid.setShowGridSummary(true);
        getService().listarBodegaProducto(0, 50, pro.getIdProducto(),objbacklst);*/
		
        
       
		//addMember(listGrid);
		
		Label lblNewLabel_1 = new Label("Tipo de Precios");
		lblNewLabel_1.setSize("100%", "5%");
		addMember(lblNewLabel_1);
		
		ListGridField idTipoPrecio = new ListGridField("idTipoPrecio", "C\u00F3digo");
		ListGridField TipoPrecio = new ListGridField("TipoPrecio", "Nombre");
		ListGridField porcentaje = new ListGridField("Porcentaje","Porcentaje Utilidad");
		ListGridField precioC = new ListGridField("PrecioC", "Precio de Costo");
		ListGridField precioV = new ListGridField("PrecioV","Precio de Venta " );		
		ListGridField precioVSI = new ListGridField("PrecioVSI", "Precio Venta Sin Iva");
		precioV.addChangeHandler(new ManejadorBotones("precio"));
		precioV.setAlign(Alignment.LEFT);
		precioV.setCanEdit(true);
		precioV.setType(ListGridFieldType.FLOAT);
		porcentaje.addChangeHandler(new ManejadorBotones("porcentaje"));
		porcentaje.setAlign(Alignment.LEFT);		
		porcentaje.setCanEdit(true);
		porcentaje.setType(ListGridFieldType.FLOAT);	
		precioVSI.addChangeHandler(new ManejadorBotones("preciosiniva"));
		precioVSI.setAlign(Alignment.LEFT);		
		precioVSI.setCanEdit(true);
		precioVSI.setType(ListGridFieldType.FLOAT);		
		lstTipoPrecio.setFields(new ListGridField[] {idTipoPrecio,TipoPrecio,precioV,precioVSI,precioC,porcentaje});
		//getService().listarTipoprecio(0,10000000, objbacklstTipo); /// cambiar luego para optimizar programa
		//getService().listarTipoPrecio(producto.getIdProducto(), objbacklstTipo);
		addMember(lstTipoPrecio);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		
		/*btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		hStack.addMember(btnGrabar);*/
		
	
		
		addMember(hStack);
		
		}catch(Exception e){
			SC.say(e.getMessage());
		}
		
	}
	public frmLstBodPre(ProductoDTO pro,Integer ind) {
		try{
			this.producto=pro;
		
		//En caso de ser 0 se lo llamaria del menu de Producto
		
		
		//lblNewLabel = new Label("Bodegas Disponibles / STOCK: "+pro.getStock());
		//lblNewLabel.setSize("100%", "5%");
		//addMember(lblNewLabel);
		
		/*ListGridField idBodega = new ListGridField("idBodega", "C\u00F3digo");
		//ListGridField Ban = new ListGridField("Ban", "Activo");
		ListGridField Bodega = new ListGridField("Bodega", "Bodega:");
		ListGridField Ubicacion = new ListGridField("Ubicacion", "Ubicaci\u00F3n:");
		ListGridField Telefono = new ListGridField("Telefono", "Tel\u00e9fono:");
		
        ListGridField cantidad = new ListGridField("Cantidad", "Cantidad"); 
        ListGridField stock = new ListGridField("Stock", "Stock"); 
        stock.setAlign(Alignment.CENTER);  
        stock.setCanEdit(false);
        stock.setType(ListGridFieldType.FLOAT);
		stock.setSummaryFunction(SummaryFunctionType.SUM);  
		stock.setShowGridSummary(true);
        cantidad.setAlign(Alignment.CENTER);  
        cantidad.setCanEdit(true);
        
        //cantidad.addEditorExitHandler(new ManejadorBotones("cantidad"));
        
        
        listGrid.setFields(idBodega, Bodega, Ubicacion, Telefono, cantidad, stock);  
        listGrid.setCanResizeFields(true);  
        listGrid.setShowRecordComponents(true);          
        listGrid.setShowRecordComponentsByCell(true);  
        listGrid.setCanRemoveRecords(true);  
        listGrid.setShowAllRecords(true);
        listGrid.setShowGridSummary(true);
        getService().listarBodegaProducto(0, 50, pro.getIdProducto(),objbacklst);*/
		
        
       
		//addMember(listGrid);
		
		Label lblNewLabel_1 = new Label("Tipo de Precios (Ingrese un porcentaje)");
		lblNewLabel_1.setSize("100%", "5%");
		addMember(lblNewLabel_1);
		
		ListGridField idTipoPrecio = new ListGridField("idTipoPrecio", "C\u00F3digo");
		ListGridField TipoPrecio = new ListGridField("TipoPrecio", "Nombre");
		ListGridField porcentaje = new ListGridField("Porcentaje","Porcentaje Utilidad");
		ListGridField precioC = new ListGridField("PrecioC", "Precio de Costo");
		ListGridField precioV = new ListGridField("PrecioV","Precio de Venta " );		
		ListGridField precioVSI = new ListGridField("PrecioVSI", "Precio Venta Sin Iva");
		precioV.addChangeHandler(new ManejadorBotones("precio"));
		precioV.setAlign(Alignment.LEFT);
		precioV.setCanEdit(true);
		precioV.setType(ListGridFieldType.FLOAT);
		porcentaje.addChangeHandler(new ManejadorBotones("porcentaje"));
		porcentaje.setAlign(Alignment.LEFT);		
		porcentaje.setCanEdit(true);
		porcentaje.setType(ListGridFieldType.FLOAT);	
		precioVSI.addChangeHandler(new ManejadorBotones("preciosiniva"));
		precioVSI.setAlign(Alignment.LEFT);		
		precioVSI.setCanEdit(true);
		precioVSI.setType(ListGridFieldType.FLOAT);		
		lstTipoPrecio.setFields(new ListGridField[] {idTipoPrecio,TipoPrecio,precioV,precioVSI,precioC,porcentaje});
		//getService().listarTipoprecio(0,10000000, objbacklstTipo); /// cambiar luego para optimizar programa
		getService().listarTipoPrecio(producto.getIdProducto(), objbacklstTipo);
		addMember(lstTipoPrecio);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		
		/*btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		hStack.addMember(btnGrabar);*/
		
	
		
		addMember(hStack);
		
		}catch(Exception e){
			SC.say(e.getMessage());
		}
		
	}
	
	public void ActualizarVal(){
		try{
			ListGridRecord record;
			Double valor=producto.getPromedio();
			for(int i=0;i<lstTipoPrecio.getRecords().length;i++) {
				record= lstTipoPrecio.getRecord(i);
				Double pvp =record.getAttributeAsDouble("PrecioV"); 
				Double dif = 0.0;
				Double precioVSI=0.0;
//				if (producto.getImpuesto()==0.0){
//					precioVSI=pvp;
//				}else{
//					precioVSI=pvp/(1+(Factum.banderaIVA/100));
//				}
				precioVSI=pvp;
				if (precioVSI!=0.0){
					//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSI);
					for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+precioVSI+" calculo "+String.valueOf(precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
						precioVSI=(multi.getTblmultiImpuesto().getTipo()=='1')?precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
							precioVSI+multi.getTblmultiImpuesto().getPorcentaje();
					}
					dif=100.0;
				}
				//com.google.gwt.user.client.Window.alert("Valor costo:  "+valor);
				if (valor!=0.0){
					dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
				}else{
					dif=dif+100;
				}
				
				dif=dif-100;
				dif=Math.rint(dif*100)/100;
				precioVSI=Math.rint(precioVSI*100)/100;
				lstTipoPrecio.setEditValue(i,"Porcentaje",dif);
				lstTipoPrecio.setEditValue(i,"PrecioVSI",precioVSI);
				lstTipoPrecio.saveAllEdits();
			 }
			
		}catch(Exception e){
			SC.say(e.getMessage());
			/*SC.say(String.valueOf(producto.getProTip().get(0).getPorcentaje()) +" "+ String.valueOf(producto.getProTip().get(1).getPorcentaje())+" "+
					String.valueOf(producto.getProTip().get(2).getPorcentaje()));*/
			
		}
	}
	
	private class ManejadorBotones implements ClickHandler,ChangeHandler,EditorExitHandler{
		String indicador="";
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("grabar")){
				try {
					String mensaje="";
					/*int countB=0,i=0,j=0,k=0,count=0;
					j=listGrid.getRecords().length;
					for(i=0;i<j;i++){
						if(listGrid.getRecord(i).getAttribute("Cantidad")!=null && !listGrid.getRecord(i).getAttribute("Cantidad").equals("")){
							countB++;
							mensaje=mensaje+" "+listGrid.getRecord(i).getAttributeAsInt("Cantidad")+" ";
						}
					}
					ProductoBodegaDTO[] proBod = new ProductoBodegaDTO[countB];
					mensaje=mensaje+" "+String.valueOf(countB);
					k=0;
					for(i=0;i<j;i++){
						if(listGrid.getRecord(i).getAttribute("Cantidad")!=null && !listGrid.getRecord(i).getAttribute("Cantidad").equals("")){
							ProductoBodegaDTO pb=new ProductoBodegaDTO();
							pb.setCantidad(listGrid.getRecord(i).getAttributeAsInt("Cantidad"));
							BodegaDTO bodega=new BodegaDTO();
							bodega.setBodega(listGrid.getRecord(i).getAttribute("Bodega"));
							bodega.setIdBodega(listGrid.getRecord(i).getAttributeAsInt("idBodega"));
							bodega.setUbicacion(listGrid.getRecord(i).getAttribute("Ubicacion"));
							bodega.setTelefono(listGrid.getRecord(i).getAttribute("Telefono"));
							pb.setTblbodega(bodega);
							pb.setTblproducto(producto);
							proBod[k]=pb;
							mensaje=mensaje+" "+proBod[k].getTblbodega().getBodega();
							k=k+1;
							
						}
					}*/
					int j=0,i=0,k=0,count=0;
					j=lstTipoPrecio.getRecords().length;
					for(i=0;i<j;i++){
						if(lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
							count++;
						} 
					}
					ProductoTipoPrecioDTO[] proTip = new ProductoTipoPrecioDTO[count];
					k=0;
					for(i=0;i<j;i++){
						if(lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
							ProductoTipoPrecioDTO pt =new ProductoTipoPrecioDTO();
							pt.setTblproducto(producto);
							TipoPrecioDTO tipo = new TipoPrecioDTO();
							tipo.setIdEmpresa(Factum.empresa.getIdEmpresa());
							tipo.setIdTipoPrecio(Integer.parseInt(lstTipoPrecio.getRecord(i).getAttribute("idTipoPrecio")));
							tipo.setTipoPrecio(lstTipoPrecio.getRecord(i).getAttribute("TipoPrecio"));
							pt.setTbltipoprecio(tipo);
							pt.setPorcentaje(lstTipoPrecio.getRecord(i).getAttributeAsDouble("PrecioV"));
							//lstTipoPrecio.getSelectedRecord().setAttribute("PrecioVSI",Math.round(pt.getPorcentaje()* 100) / 100);
							
							proTip[k]=pt;
							mensaje=mensaje+" "+proTip[k].getTbltipoprecio().getTipoPrecio()+" ";
							k=k+1;
						}
					}
					if(count==0){
						proTip=null;
					}
					/*if(countB==0){
						proBod=null;
					}*/
					final ProductoTipoPrecioDTO[] proTipF=proTip;
					//final ProductoBodegaDTO[] proBodF=proBod;
					//final int countBF=countB;
					final int countF=count;
					
					/*if (countB==0){
						SC.ask("Esta guardando el producto sin asignarlo a una bodega, �Desea continuar", new BooleanCallback() {
							public void execute(Boolean value1) {
								if(value1!=null && value1){
									DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
									//getService().grabarProductoTipoPrecio(proTip, count, objback);
									getService().grabarProductoBodega(proBodF, countBF, proTipF, countF,objback);
									btnGrabar.setVisible(false);
								}
							}
						}
						);
					}else{
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						//getService().grabarProductoTipoPrecio(proTip, count, objback);
						getService().grabarProductoBodega(proBodF, countBF, proTipF, countF,objback);
						btnGrabar.setVisible(false);
					}*/
					//SC.say(String.valueOf(haveBod));

					/*else{
					*	SC.say("El producto se guardo sin asignarse a ninguna bodega, asigne stock a una bodega para poder realizar transacciones con e");
					}*/
					//SC.say(mensaje);
					
					getService().grabarProductoTipoPrecio(proTipF, countF,objback);
					//btnGrabar.setVisible(false);
				}catch(Exception e){
					SC.say("�Error, Ingresar valores num�ricos en los campos!");
				}
			}
		}
		@Override
		public void onChange(ChangeEvent event) {
			if(indicador.equals("precio")){
			try{
				//******************************************************************************************************************************
				Double pvp = 0.0;
				Double dif = 0.0;
				Double precioVSinIva=0.0;
				Double valor=producto.getPromedio();
				pvp=Double.valueOf(String.valueOf(event.getValue()));//Valor ingresado por el usuario
				//if(pvp>valor){
				precioVSinIva=pvp;
				if (precioVSinIva!=0.0){
					//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSinIva);
					for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
						//com.google.gwt.user.client.Window.alert(" calculo1 "+String.valueOf((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100));
						//com.google.gwt.user.client.Window.alert(" calculo2 "+String.valueOf(1+(multi.getTblmultiImpuesto().getPorcentaje()/100)));
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+precioVSinIva+" calculo "+String.valueOf(precioVSinIva/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
						precioVSinIva=(multi.getTblmultiImpuesto().getTipo()=='1')?precioVSinIva/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
							precioVSinIva+multi.getTblmultiImpuesto().getPorcentaje();
					}
					dif=100.0;
				}
				if (valor!=0.0){
					dif=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				}else{
					dif=dif+100;
				}
//				if (producto.getImpuesto()==0.0){
//					precioVSinIva=pvp;
//				}else{
//					precioVSinIva=pvp/(1+(Factum.banderaIVA/100));
//				}
				//precioVSinIva=pvp/(1+(producto.getImpuesto()/100));
				
//				dif=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				dif=dif-100;
				dif=Math.rint(dif*100)/100;
				precioVSinIva=Math.rint(precioVSinIva*100)/100;
				lstTipoPrecio.getSelectedRecord().setAttribute("Porcentaje", dif);
				lstTipoPrecio.getSelectedRecord().setAttribute("PrecioVSI", precioVSinIva);
				lstTipoPrecio.refreshRow(lstTipoPrecio.getEditRow());
			     }
				catch(Exception e){
				//SC.say(e.getMessage());
			    }
			}else if(indicador.equals("porcentaje")){
				Double pvp = 0.0;
				Double dif = 0.0;
				Double precioVSinIva=0.0;
				Double valor=producto.getPromedio();
				
				dif=Double.valueOf(String.valueOf(event.getValue()));//Valor ingresado por el usuario
				//if(pvp>valor){
				
				precioVSinIva=valor+valor*(dif/100);
				pvp=precioVSinIva;
				//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSinIva);
					for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
						//com.google.gwt.user.client.Window.alert("Impuesto:  "+pvp+" calculo "+String.valueOf(pvp*(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
						pvp=(multi.getTblmultiImpuesto().getTipo()=='1')?pvp*(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
							pvp+multi.getTblmultiImpuesto().getPorcentaje();
					}
//				if (producto.getImpuesto()==0.0){
//					pvp=precioVSinIva;
//				}else{
//					pvp=precioVSinIva*(1+(Factum.banderaIVA/100));
//				}
				pvp=CValidarDato.getDecimal(Factum.banderaNumeroDecimales,pvp);
//				pvp=Math.rint(pvp*100)/100;
				
				
				//dif=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				//dif=dif-100;
				//dif=Math.rint(dif*100)/100;
				precioVSinIva=Math.rint(precioVSinIva*100)/100;
				lstTipoPrecio.getSelectedRecord().setAttribute("PrecioV", pvp);
				lstTipoPrecio.getSelectedRecord().setAttribute("PrecioVSI", precioVSinIva);
				lstTipoPrecio.refreshRow(lstTipoPrecio.getEditRow());
				
			}else if(indicador.equals("preciosiniva")){
				
				Double pvp = 0.0;
				Double porcentaje = 0.0;
				Double precioVSinIva=0.0;
				Double valor=producto.getPromedio();
				
				precioVSinIva=Double.valueOf(String.valueOf(event.getValue()));//Valor ingresado por el usuario
				//if(pvp>valor){y
				pvp=precioVSinIva;
				//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSinIva);
				for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
					//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
					//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
					//com.google.gwt.user.client.Window.alert("Impuesto:  "+pvp+" calculo "+String.valueOf(pvp*(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
					pvp=(multi.getTblmultiImpuesto().getTipo()=='1')?pvp*(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
						pvp+multi.getTblmultiImpuesto().getPorcentaje();
				}
//				if (producto.getImpuesto()==0.0){
//					pvp=precioVSinIva;
//				}else{
//					pvp=precioVSinIva*(1+(Factum.banderaIVA/100));
//				}
				pvp=CValidarDato.getDecimal(Factum.banderaNumeroDecimales,pvp);
//				pvp=Math.rint(pvp*100)/100;
				
				porcentaje=100.0;
				//dif=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				//dif=dif-100;
				//dif=Math.rint(dif*100)/100;
				//precioVSinIva=Math.rint(precioVSinIva*100)/100;
				if (valor!=0.0){
					porcentaje=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				}else{
					porcentaje=porcentaje+100;
				}
//				porcentaje=(precioVSinIva*100)/valor;//Calculamos porcentaje de utilidad
				porcentaje=porcentaje-100;
				porcentaje=Math.rint(porcentaje*100)/100;
				//precioVSinIva=Math.rint(precioVSinIva*100)/100;
				
				lstTipoPrecio.getSelectedRecord().setAttribute("Porcentaje", porcentaje);
				lstTipoPrecio.getSelectedRecord().setAttribute("PrecioV", pvp);
				lstTipoPrecio.refreshRow(lstTipoPrecio.getEditRow());
				
			}
		}
		@Override
		public void onEditorExit(EditorExitEvent event) {
			if (indicador.equals("cantidad")){
				int i=0,j=0,cont=0; 
				if (!event.getNewValue().equals(null) || !event.getNewValue().equals("")){
					Double cant=Double.valueOf(event.getNewValue().toString());
				Double stc=listGrid.getSelectedRecord().getAttributeAsDouble("Stock");
				stc=stc+cant;
				listGrid.getSelectedRecord().setAttribute("Stock", String.valueOf(stc));
				}/*else{
					listGrid.getSelectedRecord().setAttribute("Cantidad", 0);
					listGrid.refreshFields();
				}*/
			}
		} 
		
	}
	/*
	final AsyncCallback<List<TipoPrecioDTO>>  objbacklstTipo=new AsyncCallback<List<TipoPrecioDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
			
		}
		
		//revisar
		public void onSuccess(List<TipoPrecioDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			try{
				for(int i=0;i<result.size();i++) {
					TipoPrecioRecords tipRecords =new TipoPrecioRecords((TipoPrecioDTO)result.get(i));
					tipRecords.setPrecioC(producto.getPromedio());
										
					Double dif = 0.0;
					Double precioVSI=0.0;
					Double pvp=producto.getProTip().get(0).getPorcentaje();
					
					precioVSI=pvp/1.14;
					dif=(precioVSI*100)/producto.getPromedio();//Calculamos porcentaje de utilidad
					dif=dif-100;
					//dif=Math.rint(dif*100)/100;
					dif=Math.rint(dif*100)/100;
					precioVSI=Math.rint(precioVSI*100)/100;
					tipRecords.setPorcentaje(dif);
					listado[i]=(tipRecords);
				 }
				listado[0].setAttribute("PrecioV", producto.getProTip().get(0).getPorcentaje());
				listado[1].setAttribute("PrecioV", producto.getProTip().get(1).getPorcentaje());
				listado[2].setAttribute("PrecioV", producto.getProTip().get(2).getPorcentaje());
				 
				Double pvp = producto.getProTip().get(0).getPorcentaje();
				//pvp=pvp/1.12;
				Double dif = 0.0;
				Double precioVSI=0.0;
				Double valor=producto.getPromedio();
				precioVSI=pvp/1.14;
				dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
				dif=dif-100;
				dif=Math.rint(dif*100)/100;
				precioVSI=Math.rint(precioVSI*100)/100;
				listado[0].setAttribute("Porcentaje",dif);
				listado[0].setAttribute("PrecioVSI",precioVSI);
				pvp = producto.getProTip().get(1).getPorcentaje();
				
				dif = 0.0;
				valor=producto.getPromedio();
				precioVSI=pvp/1.14;
				dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
				dif=dif-100;
				dif=Math.rint(dif*100)/100;
				precioVSI=Math.rint(precioVSI*100)/100;
				listado[1].setAttribute("Porcentaje",dif);
				listado[1].setAttribute("PrecioVSI",precioVSI);
				
				pvp = producto.getProTip().get(2).getPorcentaje();
				dif = 0.0;
				valor=producto.getPromedio();
				precioVSI=pvp/1.14;
				dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
				dif=dif-100;
				dif=Math.rint(dif*100)/100;
				precioVSI=Math.rint(precioVSI*100)/100;
				listado[2].setAttribute("Porcentaje",dif);
				listado[2].setAttribute("PrecioVSI",precioVSI);
				
				
				lstTipoPrecio.setData(listado);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}catch(Exception e){
				SC.say(e.getMessage());
				SC.say(String.valueOf(producto.getProTip().get(0).getPorcentaje()) +" "+ String.valueOf(producto.getProTip().get(1).getPorcentaje())+" "+
						String.valueOf(producto.getProTip().get(2).getPorcentaje()));
				
			}
		}
		
	};
	*/
	final AsyncCallback<List<ProductoTipoPrecio>>  objbacklstTipo=new AsyncCallback<List<ProductoTipoPrecio>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
			
		}
		
		//revisar
		public void onSuccess(List<ProductoTipoPrecio> result) {
			Collections.sort(result,new ProductoTipoPrecioCompare());
			//result.sort(new ProductoTipoPrecioCompare());
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			try{
				ListGridRecord record;
				
				for(int i=0;i<result.size();i++) {
					record = new ListGridRecord();
					record.setAttribute("PrecioV", result.get(i).getPorcentaje());
					Double pvp = result.get(i).getPorcentaje();
					Double dif = 0.0;
					Double precioVSI=0.0;
					Double valor=producto.getPromedio();
//					precioVSI=pvp/(1+(producto.getImpuesto()/100));
					precioVSI=pvp;
					if (precioVSI!=0){
					//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSI);
						for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+precioVSI+" calculo "+String.valueOf(precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
							precioVSI=(multi.getTblmultiImpuesto().getTipo()=='1')?precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
								precioVSI+multi.getTblmultiImpuesto().getPorcentaje();
						}
						dif=100.0;
					}
					//com.google.gwt.user.client.Window.alert("Valor costo:  "+valor);
					if (valor!=0.0){
						dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
					}else{
						dif=dif+100;
					}
//					dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
					dif=dif-100;
					dif=Math.rint(dif*100)/100;
					precioVSI=Math.rint(precioVSI*100)/100;
					record.setAttribute("Porcentaje",dif);
					record.setAttribute("PrecioVSI",precioVSI);
					
					record.setAttribute("idTipoPrecio", result.get(i).getIdTipo());
					record.setAttribute("TipoPrecio",result.get(i).getTipo() );
					record.setAttribute("PrecioC", valor);
					
					listado[i]=record;
				 }
				
				lstTipoPrecio.setData(listado);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
			}catch(Exception e){
				SC.say(e.getMessage());
				/*SC.say(String.valueOf(producto.getProTip().get(0).getPorcentaje()) +" "+ String.valueOf(producto.getProTip().get(1).getPorcentaje())+" "+
						String.valueOf(producto.getProTip().get(2).getPorcentaje()));*/
				
			}
		}
		
	};
	
	final AsyncCallback<List<ProductoTipoPrecioDTO>>  objbacklstTipoD=new AsyncCallback<List<ProductoTipoPrecioDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
			
		}
		
		//revisar
		public void onSuccess(List<ProductoTipoPrecioDTO> result) {
			Collections.sort(result,new ProductoTipoPrecioDTOCompare());
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			try{
				ListGridRecord record;
				producto=result.get(0).getTblproducto();
				for(int i=0;i<result.size();i++) {
					record = new ListGridRecord();
					record.setAttribute("PrecioV", result.get(i).getPorcentaje());
					Double pvp = result.get(i).getPorcentaje();
					Double dif = 0.0;
					Double precioVSI=0.0;
					Double valor=producto.getPromedio();
//					precioVSI=pvp/(1+(producto.getImpuesto()/100));
					precioVSI=pvp;
					if (precioVSI.doubleValue()!=0.0){
						//com.google.gwt.user.client.Window.alert("Precio:  "+precioVSI);
						for (TblproductoMultiImpuestoDTO multi: producto.getTblmultiImpuesto()){
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+multi.getTblmultiImpuesto().getTipo()+"  "+ multi.getTblmultiImpuesto().getPorcentaje());
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+String.valueOf(multi.getTblmultiImpuesto().getTipo()=='1'));
							//com.google.gwt.user.client.Window.alert("Impuesto:  "+precioVSI+" calculo "+String.valueOf(precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100))));
							precioVSI=(multi.getTblmultiImpuesto().getTipo()=='1')?precioVSI/(1+((new Double(multi.getTblmultiImpuesto().getPorcentaje()))/100)):
								precioVSI+multi.getTblmultiImpuesto().getPorcentaje();
						}
						dif=100.0;
					}
					if (valor!=0.0){
						dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
					}else{
						dif=dif+100;
					}
//					dif=(precioVSI*100)/valor;//Calculamos porcentaje de utilidad
					dif=dif-100;
					dif=Math.rint(dif*100)/100;
					precioVSI=Math.rint(precioVSI*100)/100;
					record.setAttribute("Porcentaje",dif);
					record.setAttribute("PrecioVSI",precioVSI);
					
					record.setAttribute("idTipoPrecio", result.get(i).getTbltipoprecio().getIdTipoPrecio());
					record.setAttribute("TipoPrecio",result.get(i).getTbltipoprecio().getTipoPrecio() );
					record.setAttribute("PrecioC", valor);
					
					listado[i]=record;
				 }
				
				lstTipoPrecio.setData(listado);
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				
			}catch(Exception e){
				SC.say(e.getMessage());
				/*SC.say(String.valueOf(producto.getProTip().get(0).getPorcentaje()) +" "+ String.valueOf(producto.getProTip().get(1).getPorcentaje())+" "+
						String.valueOf(producto.getProTip().get(2).getPorcentaje()));*/
				
			}
		}
		
	};
	final AsyncCallback<List<BodegaDTO>>  objbacklst=new AsyncCallback<List<BodegaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error no se conecta a la base");
		}
		public void onSuccess(List<BodegaDTO> result) {
			/*getService().numeroRegistrosBodega("Tblbodega", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);*/
			if (result!=null){
				
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			//lblNewLabel.setContents("Bodegas Disponibles / STOCK: "+"0.0");
			for(int i=0;i<result.size();i++) {
				listado[i]=(new BodegaRecords((BodegaDTO)result.get(i)));
				//listado[i].setAttribute("Cantidad","");
				//listado[i].setAttribute("Stock","");
				/*lblNewLabel.setContents("Bodegas Disponibles / STOCK: "+
						String.valueOf(Double.valueOf(lblNewLabel.getContents())+
						listado[i].getAttributeAsInt("Stock")));*/
            }
			//SC.say(String.valueOf(listado.length));
			listadoBod=listado;
			listGrid.setData(listado);
			//getService().buscarProducto(producto.getIdProducto()+"", "idProducto", retornopro);
			listGrid.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}else{
				getService().listarBodega(0,100, objbacklst);
			}
			
       }
		
	};
	/*final AsyncCallback <ProductoDTO>  retornopro =new AsyncCallback <ProductoDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(ProductoDTO result) {
			
			lblNewLabel = new Label("Bodegas Disponibles / STOCK: "+result.getStock());
			lblNewLabel.redraw();
		}
		
	};*/
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error al grabar: "+caught.toString());
		}
		public void onSuccess(String result) {
			//getService().listarBodegaProducto(0, 50, producto.getIdProducto(),objbacklst);
			getService().listarTipoPrecio(producto.getIdProducto(), objbacklstTipo);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
		}
		
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	public void setDefaultProd() {
		// TODO Auto-generated method stub
		producto.setCantidad(0);
//		TblmultiImpuestoDTO multImp=new TblmultiImpuestoDTO(12,12,"IVA 12",'1');
//		TblproductoMultiImpuestoDTO prodMultImp=new TblproductoMultiImpuestoDTO();
//		prodMultImp.setTblmultiImpuesto(multImp);
//		Set<TblproductoMultiImpuestoDTO> tblmultiImpuesto= new HashSet(0);
//		tblmultiImpuesto.add(prodMultImp);
//		producto.setTblmultiImpuesto(tblmultiImpuesto);
//		producto.setImpuesto(12);
	}

}
