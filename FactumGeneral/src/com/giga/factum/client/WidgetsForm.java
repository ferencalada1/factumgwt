package com.giga.factum.client;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
//import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

public class WidgetsForm {

	
	
	public WidgetsForm(){
		
	}
	/**
	 * Funcion que devuelve un widget tipo SelectItem 
	 * @param nombre Tipo STRING para definir el nombre 
	 * @param titulo Tipo STRING para definir el titulo a mostrar en la interfaz grafica
	 * @param multiple Tipo BOOLEAN para indicar si es de seleccion simple o multiple
	 * @param width Tipo STRING indica el ancho
	 * @param height Tipo STRING indica el alto 
	 * @param visible Tipo BOOLEAN true=visible, false=oculto
	 * @return objeto tipo com.smartgwt.client.widgets.form.fields.SelectItem
	 */
	public SelectItem crearListado(String nombre, String titulo, Boolean multiple, String width, String height,
			Boolean visible, DoubleClickHandler dHandler, KeyPressHandler kHandler){
		SelectItem listado = new SelectItem(nombre, titulo);
        listado.setMultipleAppearance(MultipleAppearance.GRID);
        listado.setMultiple(multiple);
        listado.setWidth(width);
        listado.setRequired(true);
        listado.setHeight(height);
        listado.setVisible(visible);
        listado.addDoubleClickHandler(dHandler);
        listado.addKeyPressHandler(kHandler);
        return listado;
	}
	
	public CheckboxItem crearCheck(String nombre, String titulo, Boolean redraw){
		CheckboxItem chk = new CheckboxItem(nombre,titulo);
		chk.setDefaultValue(false);
		chk.setRedrawOnChange(redraw);
		return chk;
	}
	
	public TextItem crearText(String nombre, String titulo, FormItemIfFunction funcion, Boolean visible, 
			Integer longitud, String msnValidacion, String validacion, KeyPressHandler KeyHandler,
			BlurHandler BlHandler){
		TextItem txt = new TextItem(nombre, titulo);
		txt.setShowIfCondition(funcion);
		txt.setVisible(visible);
		txt.setLength(longitud);
		if(KeyHandler != null)
		txt.addKeyPressHandler(KeyHandler);
		txt.setHint(msnValidacion);
		txt.setKeyPressFilter(validacion);
		txt.addBlurHandler(BlHandler);
		return txt;
	}
	
	
	public TextItem crearText(String nombre, String titulo, FormItemIfFunction funcion, Boolean visible, 
			Integer longitud, String msnValidacion, String validacion, KeyPressHandler KeyHandler,
			BlurHandler BlHandler,Boolean pck1, Boolean pck2, FormItemClickHandler mpck1, FormItemClickHandler mpck2){
		TextItem txt = new TextItem(nombre, titulo);
		txt.setShowIfCondition(funcion);
		txt.setVisible(visible);
		txt.setLength(longitud);
		if(KeyHandler != null)
		txt.addKeyPressHandler(KeyHandler);
		txt.setHint(msnValidacion);
		txt.setKeyPressFilter(validacion);
		txt.addBlurHandler(BlHandler);
		PickerIcon picker1 = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon picker2 = new PickerIcon(PickerIcon.DATE);
		txt.setIcons(picker1,picker2);
		picker1.addFormItemClickHandler(mpck1);
		picker2.addFormItemClickHandler(mpck2);
		return txt;
	}
	
	public TextItem crearText(String nombre, String titulo, FormItemIfFunction funcion, Boolean visible, 
			Integer longitud, String msnValidacion, String validacion, KeyPressHandler KeyHandler,
			BlurHandler BlHandler,Boolean pck1, Boolean pck2, Boolean pck3, FormItemClickHandler mpck1, 
			FormItemClickHandler mpck2, FormItemClickHandler mpck3){
		TextItem txt = new TextItem(nombre, titulo);
		txt.setShowIfCondition(funcion);
		txt.setVisible(visible);
		txt.setLength(longitud);
		if(KeyHandler != null)
		txt.addKeyPressHandler(KeyHandler);
		txt.setHint(msnValidacion);
		txt.setKeyPressFilter(validacion);
		txt.addBlurHandler(BlHandler);
		PickerIcon picker1 = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon picker2 = new PickerIcon(PickerIcon.DATE);
		PickerIcon picker3 = new PickerIcon(PickerIcon.COMBO_BOX);
		if(pck1)
		{
			txt.setIcons(picker1);
			picker1.addFormItemClickHandler(mpck1);
		}
		else if(pck2)
		{
			txt.setIcons(picker2);
			picker2.addFormItemClickHandler(mpck2);
		}
		else if(pck3)
		{
			txt.setIcons(picker3);
			picker3.addFormItemClickHandler(mpck3);
		}
		else if(pck1 && pck2)
		{
			txt.setIcons(picker1,picker2);
			picker1.addFormItemClickHandler(mpck1);
			picker2.addFormItemClickHandler(mpck2);
		}
		else if(pck1 && pck3)
		{
			txt.setIcons(picker1,picker3);
			picker1.addFormItemClickHandler(mpck1);
			picker3.addFormItemClickHandler(mpck3);
		}
		else if(pck2 && pck3)
		{
			txt.setIcons(picker2,picker3);
			picker2.addFormItemClickHandler(mpck2);
			picker3.addFormItemClickHandler(mpck3);
		}
		return txt;
	}
	

	public TextItem crearText(String nombre, String titulo, FormItemIfFunction funcion, Boolean visible, 
			String pista, Integer longitud, KeyPressHandler KeyHandler, String msnValidacion, 
			String validacion){
		TextItem txt = new TextItem(nombre, titulo);
		txt.setShowIfCondition(funcion);
		txt.setVisible(visible);
		txt.setLength(longitud);
		txt.setHint(pista);
		txt.setShowHintInField(true);
		if(KeyHandler != null)
			txt.addKeyPressHandler(KeyHandler);
		txt.setKeyPressFilter(validacion);
		/*RegExpValidator numValidation = new RegExpValidator();
        numValidation.setErrorMessage(msnValidacion);
        numValidation.setExpression(validacion);
        txt.setValidators(numValidation);*/
		return txt;
	}
	
		
	public DateItem crearFecha(String nombre, String titulo){
		DateItem fecha = new DateItem(nombre,titulo);
		fecha.setTitle("Date");  
        fecha.setUseTextField(true);
        fecha.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
        fecha.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
        fecha.setHint("<nobr>Vencimiento</nobr>");
		return fecha;
	}	
	
	public ButtonItem crearBoton(String title, ClickHandler manejador){
		ButtonItem boton = new ButtonItem();
		boton.setTitle(title);
		boton.addClickHandler(manejador);
		return boton;
	}
	
}
