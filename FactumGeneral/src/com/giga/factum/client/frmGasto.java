package com.giga.factum.client;

import java.util.HashMap;
import java.util.Map;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class frmGasto extends VLayout{
	 public frmGasto(String id, String vend){
		 HTMLPane htmlPane = new HTMLPane();
			htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Gastos/Gastos/page");
//		 htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/Gastos/Gastos/page");
			//?id="+id
//			htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":9999/reportes/document_commercial");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", id);
			map.put("vend", vend);
			map.put("url", Factum.banderaIpServidor);
			map.put("port", Factum.banderaPuertoServidor);
			htmlPane.setContentsURLParams(map);
			htmlPane.setContentsType(ContentsType.PAGE);
			addMember(htmlPane );
	 }
	 public frmGasto(String id){
		 HTMLPane htmlPane = new HTMLPane();
			htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Gastos/Gastos/page");
//		 htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/Gastos/Gastos/page");
			//?id="+id
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", id);
			map.put("vend", "0");
			map.put("url", Factum.banderaIpServidor);
			map.put("port", Factum.banderaPuertoServidor);
			htmlPane.setContentsURLParams(map);
			htmlPane.setContentsType(ContentsType.PAGE);
			addMember(htmlPane );
	 }
}
