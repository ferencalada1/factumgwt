package com.giga.factum.client;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.ListGrid;

public class frmPago extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	TextItem txtCliente = new TextItem("txtCliente", "Cliente");
	TextItem txtComprobante = new TextItem("txtComprobante", "Num Comprobante");
	TextItem txtLocalizacion = new TextItem("txtLocalizacion", "Localizacion");
	TextItem txtFactura = new TextItem("txtFactura", "Num Factura");
	FloatItem txtValor = new FloatItem();
	TextItem txtSaldo = new TextItem("txtSaldo", "Saldo");
	TextItem txtTotal = new TextItem("txtTotal", "Total Factura");
	DateTimeItem txtFechaR = new DateTimeItem("txtFechaR", "Fecha de Pago");
	TextItem txtEstado = new TextItem("txtEstado", "Estado");
	TextItem txtFormaPago = new TextItem("txtFormaPago", "Forma de Pago");
	
	LinkedList<TblpagoDTO> listGlobal=new LinkedList<TblpagoDTO>();
	//TextItem txtObservaciones = new TextItem("txtObservaciones", "Observaciones");
	private final Canvas canvas = new Canvas();
	private final IButton btnImprimir = new IButton("Imprimir");
	private final IButton btnImprimirTodo = new IButton("Imprimir Todo");
	//TextItem cmbDocumento = new TextItem("cmbDocumento", "TipoPago");
	private final Label lblPagos = new Label("PAGOS");
	TblpagoDTO pagodto=new TblpagoDTO();
	String tipo="";
	String tipoTrans="0";
	
	
	public frmPago(TblpagoDTO pago, String tipo,double saldo1, String tipoTrans){
		try{
			this.tipo=tipo;
			this.tipoTrans=tipoTrans;
			
			lblPagos.setAlign(Alignment.CENTER); 
			txtFechaR.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaR.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaR.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			dynamicForm.setSize("100%", "40%");
			String persona="";	
			
			txtCliente.setRequired(true);
			txtCliente.setDisabled(true);
			txtFactura.setRequired(true);
			txtFactura.setDisabled(true);
			txtFechaR.setDisabled(true);
			
			txtValor.setName("txtValor");
			txtValor.setRequired(true);
			txtValor.setDisabled(true);
			
			txtValor.setTitle("Valor del Pago");
			txtFechaR.setRequired(true);
			txtEstado.setDisabled(true);
			txtValor.setDisabled(true);
			txtFormaPago.setDisabled(true);
			//txtObservaciones.setDisabled(true);
			
			lblPagos.setSize("100%", "5%");
			addMember(lblPagos);
			txtSaldo.setAlign(Alignment.RIGHT);
			if (tipoTrans=="1"){
				txtCliente.setTitle("Proveedor");
				dynamicForm.setFields(new FormItem[] { txtCliente, txtFactura, txtComprobante, txtValor, txtFechaR, txtSaldo,txtTotal,txtEstado,txtFormaPago});
			}else{
				dynamicForm.setFields(new FormItem[] { txtCliente, txtFactura, txtLocalizacion, txtValor, txtFechaR, txtSaldo,txtTotal,txtEstado,txtFormaPago});
			}
			addMember(dynamicForm);
			canvas.addChild(btnImprimir);
			btnImprimir.moveTo(6, 6);
			addMember(canvas);
			btnImprimir.addClickHandler( new ManejadorBotones("imprimir"));
			
//			canvas.addChild(btnImprimirTodo);
//			btnImprimirTodo.moveTo(8, 8);
//			btnImprimirTodo.addClickHandler( new ManejadorBotones("imprimir"));
			pagodto=pago;
//			txtCliente.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial());	
			txtCliente.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial());	
			txtFactura.setValue(pagodto.getTbldtocomercial().getNumRealTransaccion());
			txtFormaPago.setValue(pagodto.getFormaPago());
			txtValor.setValue(pagodto.getValor());
			if(String.valueOf(pagodto.getEstado()).equals("1")){
				txtValor.setValue(pagodto.getValor());
				txtFechaR.setValue(pagodto.getFechaRealPago());
				if (tipoTrans=="1"){
					txtComprobante.setValue(pagodto.getTbldtocomercial().getNumCompra());
					txtEstado.setValue("PAGADO");
				}else{
					txtLocalizacion.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getLocalizacion());
					txtEstado.setValue("COBRADO ");
				}
				//txtPago.setVisible(false);
				//cmbDocumento.setVisible(false);
				//txtObservaciones.setVisible(false);
			}else{
				txtEstado.setValue("PENDIENTE ");
				btnImprimir.setVisible(false); 
			}
			Iterator iterPago= pagodto.getTbldtocomercial().getTblpagos().iterator();
			TblpagoDTO tPago = new TblpagoDTO();
			double saldo=0;
			while (iterPago.hasNext()) {
				tPago = (TblpagoDTO) iterPago.next();
				if(String.valueOf(tPago.getEstado()).equals("1")){
					saldo=saldo+tPago.getValor();
				}
			}
			if(String.valueOf(pagodto.getTbldtocomercial().getEstado())=="1")
			{	
				Iterator iterTiposPago= pagodto.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
				DtocomercialTbltipopagoDTO tTiposPago = new DtocomercialTbltipopagoDTO();
				while (iterTiposPago.hasNext()) {
					tTiposPago = (DtocomercialTbltipopagoDTO) iterTiposPago.next();
				if(String.valueOf(tTiposPago.getTipoPago()).compareTo("2")!=0)
				{
						saldo=saldo+tTiposPago.getCantidad();
					}
				}
			}
			
			Iterator iterDetalle=pagodto.getTbldtocomercial().getTbldtocomercialdetalles().iterator();
			DtoComDetalleDTO det=new DtoComDetalleDTO();
			double total=0;
			if(tipo=="Nota de Entrega"||tipo=="Nota de Compra")
			{	
			
				while(iterDetalle.hasNext()){
					det=(DtoComDetalleDTO) iterDetalle.next();
					total=total+det.getTotal();
				}
			}
			else
			{
				while(iterDetalle.hasNext()){
					det=(DtoComDetalleDTO) iterDetalle.next();
					
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+det.getTblimpuestos().size());
					for (DtoComDetalleMultiImpuestosDTO detalleImp: det.getTblimpuestos()){
						i1=i1+1;
						impuestos+=detalleImp.getPorcentaje().toString();
						impuestos= (i1<det.getTblimpuestos().size())?impuestos+",":impuestos; 
						impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
						impuestoValor=impuestoValor+((det.getCantidad()*det.getPrecioUnitario()*(1-(det.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					total=total+((det.getImpuesto()*det.getTotal()/100)+det.getTotal());
					total=total+(impuestoValor+det.getTotal());
				}
			}	
			
			total=Math.rint(total*100)/100;
			saldo=total-saldo;
			saldo= Math.rint(saldo*100)/100;
			txtSaldo.setValue(Math.rint((saldo-saldo1)*100)/100);
			txtTotal.setValue(total);
		}catch(Exception e){
			SC.say("error al cargar la pantalla: "+e.getMessage()); 
		}
	}
	public frmPago(TblpagoDTO pago, String tipo, String tipoTrans){
		try{
			
			this.tipo=tipo;
			this.tipoTrans=tipoTrans;
			
			String persona="";	
			if (tipoTrans=="0"){
				persona="Cliente";
			}else if (tipoTrans=="1"){
				persona="Proveedor";
			}
			
			lblPagos.setAlign(Alignment.CENTER); 
			txtFechaR.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaR.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaR.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			dynamicForm.setSize("100%", "40%");
			txtCliente.setTitle(persona);
			txtCliente.setRequired(true);
			txtCliente.setDisabled(true);
			txtFactura.setRequired(true);
			txtFactura.setDisabled(true);
			txtFechaR.setDisabled(true);
			txtValor.setName("txtValor");
			txtValor.setRequired(true);
			txtValor.setDisabled(true);
			txtValor.setTitle("Valor del Pago");
			txtFechaR.setRequired(true);
			txtEstado.setDisabled(true);
			txtValor.setDisabled(true);
			txtFormaPago.setDisabled(true);
			//txtObservaciones.setDisabled(true);
			
			lblPagos.setSize("100%", "5%");
			addMember(lblPagos);
			txtSaldo.setAlign(Alignment.RIGHT);
			if (tipoTrans=="1"){
				txtCliente.setTitle("Proveedor");
				dynamicForm.setFields(new FormItem[] { txtCliente, txtFactura, txtComprobante, txtValor, txtFechaR, txtSaldo,txtTotal,txtEstado,txtFormaPago});
			}else{
				dynamicForm.setFields(new FormItem[] { txtCliente, txtFactura, txtLocalizacion, txtValor, txtFechaR, txtSaldo,txtTotal,txtEstado,txtFormaPago});
			}
			addMember(dynamicForm);
			canvas.addChild(btnImprimir);
			btnImprimir.moveTo(6, 6);
			addMember(canvas);
			btnImprimir.addClickHandler( new ManejadorBotones("imprimir"));
//			
//			btnImprimirTodo.addChild(btnImprimirTodo);
//			canvas.addChild(btnImprimirTodo);
//			btnImprimirTodo.moveTo(8, 8);
			addMember(canvas);
			getService().buscarPagoId(pago.getIdPago(),objbackPagoDTO);
			
			
//			getService().BuscarPagoIdComercial(pago.getIdPago(),objbackPagoDTO);
		}catch(Exception e){
			SC.say("error al cargar la pantalla: "+e.getMessage()); 
		}
	}
	private class ManejadorBotones implements ClickHandler{
		private String indicador;
		public ManejadorBotones(String indi){
			indicador=indi;
		}
		
		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equals("imprimir"))
			{
		/*
				Label lblCliente = new Label("CLIENTE"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+
						"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"RESPONSABLE");
				lblCliente.setAlign(Alignment.CENTER); 
				Label lbl1 = new Label("&nbsp");
				Label lbl2 = new Label("&nbsp");
				Label lbl3 = new Label("&nbsp");
				Label lbl4 = new Label("&nbsp");
				Label lbl5 = new Label("__________________________________________________________________________________________________________");
				lbl5.setAlign(Alignment.CENTER); 
				
				
				Object[] a=new Object[]{lblPagos,dynamicForm,lbl1,lbl1,lbl2,lbl3,lbl4,lbl5,lblCliente};
	            Canvas.showPrintPreview(a);
	          */
				
				//SC.say("click");
				
				LinkedList<TblpagoDTO> listPago=new LinkedList<TblpagoDTO>();
				String idPago= "";
				Object[] a=new Object[]{};
//				for(int i = 0; i < listPago.size() ; i++ ){
////					idPago = String.valueOf(listPago.get(i).getIdPago());
//					idPago = String.valueOf(listPago.get(i).getIdPago());
////					listGlobal = i;
////				SC.say(String.valueOf(listPago.get(i).getTbldtocomercial().getNumRealTransaccion()));
//				//SC.say(idPago);
//				
//					
//				}
				
				
				
			String persona="";	
			if (tipoTrans=="0"){
				persona="Cliente";
			}else if (tipoTrans=="1"){
				
				persona="Proveedor";
			}
			
			if (Factum.banderaConfiguracionImpresionPago==0){
				impresionDirecta(persona);
			}else{
				impresionPrintPreview(persona, idPago);
			}
			
			}
		}

		
	}
	
	public void impresionDirecta(String persona) {
		// AQUI VAMOS A EXTRAER LOS DATOS DE LA FACTURA PARA PROCEDER A
		// ENVIARLOS
		////com.google.gwt.user.client.Window.alert("Imprimir funcion "+NumDto+" id:"+idDtocomercial);
		//String[] ids=idDtocomercial.split("-");
//		String persona="";	
//		if (tipoTrans=="0"){
//			persona="Cliente";
//		}else if (tipoTrans=="1"){
//			persona="Proveedor";
//		}
		String idPago=String.valueOf(pagodto.getIdPago());   
		  String tipoPersona=persona;
		  String nombre=pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial();
		  String razonSocial=(tipoTrans=="0")?pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial()+"&localizacion="+txtLocalizacion.getDisplayValue():pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial();
		  String numFactura=(tipoTrans=="1")?txtFactura.getDisplayValue()+"&numComprobante="+txtComprobante.getDisplayValue():txtFactura.getDisplayValue();
		  //String numComprobante=(tipoTrans=="0")?"-1":txtComprobante.getDisplayValue();
	      String valor=txtValor.getDisplayValue();
	      String fecha=txtFechaR.getDisplayValue();
	      String saldo=txtSaldo.getDisplayValue();
	      String total=txtTotal.getDisplayValue();
	      String estado=txtEstado.getDisplayValue();
	      String forma=txtFormaPago.getDisplayValue();
	      String empresa=Factum.banderaNombreEmpresa;
	      String direccionEmpresa=Factum.banderaDireccionEmpresa;
	      String telefonoEmpresa=Factum.banderaTelefonoCelularEmpresa+" "+Factum.banderaTelefonoConvencionalEmpresa;
		////com.google.gwt.user.client.Window.alert("Imprimir funcion datos 1");
		
		//SC.say("claveAcceso "+impreso);
		//SC.say("IP DE SERVIDOR: "+Factum.banderaIpServidor);
		String archivoPHP="imprimirPago.php";
		String banderaImpresion=Factum.banderaIpImpresion;

			//SC.say(banderaImpresion+"-"+Factum.banderaImprecionPHPProforma);
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
					"http://"+banderaImpresion+"/IMPRESION/"+archivoPHP+"?fecha=" + fecha
							+ "&idPago=" + idPago + "&tipoPersona=" + tipoPersona + "&nombreComercial="+nombre+"&razonSocial="+razonSocial
							+ "&direccion="+ direccionEmpresa + "&telefono=" + telefonoEmpresa + "&total="+ total + "&valor="+ valor
							+ "&total=" + total+ "&numFactura=" + numFactura+ "&saldo=" + saldo
							+ "&estado=" + estado + "&forma=" + forma + "&empresa=" + empresa);
			try {
				rb.setHeader("Access-Control-Allow-Origin", "*");
			    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
			    rb.setHeader("Access-Control-Max-Age", "3600");
			    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
				rb.sendRequest(null, new RequestCallback() {
					public void onError(
							final com.google.gwt.http.client.Request request,
							final Throwable exception) {
						// Window.alert(exception.getMessage());
						SC.say("Error al imprimir verifique el estado de la impresora: "
								+ exception);
					}

					public void onResponseReceived(
							final com.google.gwt.http.client.Request request,
							final Response response) {
						// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
						// LA GRILLA
						
						
					}

				});
			} catch (final Exception e) {
				SC.say("Error "+e);
			}
				
	}
	
//	public void impresionPrintPreview(String persona){
//		String tablaPagos="";
//		tablaPagos="<html>"+
//		"<head>"+        
//		"<title>Pagos Clientes Giga-Computers</title>"+
//	    	
//		"</head>"+
//		"<body style='font-family: Verdana, Geneva, sans-serif, font-size: 12px;'>"+
//
//		
//		
//		
//	        "<table id='items' style='margin: 30px 0 0 0; width: 600px; text-align: center'>"+
//			
//			  "<tr>"+
//			      "<th colspan='3' style='width:600px; padding: 5px;text-align: center'>Pago # "+pagodto.getIdPago()+"</th>"+    
//			  "</tr>"+
//	          
//	           "<tr>"+
//			    "  <th colspan='3' style='padding: 3px;'>&nbsp;</th>"+
//			  "</tr>"+
//								  
//			  "<tr class='item-row' style='vertical-align: top;'>"+
//			  "   <td class='item-name' style='width: 150px;text-align: left'>&nbsp;</td>"+
//			  
//			   "   <td class='item-name' style='padding: 3px; width: 150px;text-align: left'>"+persona+":</td>"+
//	            "  <td class='item-name' style='padding: 3px; width: 300px;text-align: left'>"+pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial()+"</td>"+
////	            "  <td class='item-name' style='padding: 3px; width: 300px;text-align: left'>"+pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial()+" "+pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial()+"</td>"+
//			  "</tr>"+
//			
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Num Transaccion:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFactura.getDisplayValue()+"</td>"+
//			  "</tr>";
//		if (tipoTrans=="1"){
//		
//			
//		tablaPagos=tablaPagos+
//	            
//			"<tr class='item-row' style='vertical-align: top;'>"+
//			"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			 "   <td class='item-name' style='padding: 3px;text-align: left'>Num Comprobante:</td>"+
//			  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtComprobante.getDisplayValue()+"</td>"+
//			"</tr>";
//		}else{
//			tablaPagos=tablaPagos+
//		            
//					"<tr class='item-row' style='vertical-align: top;'>"+
//					"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//					 "   <td class='item-name' style='padding: 3px;text-align: left'>Localizacion:</td>"+
//					  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtLocalizacion.getDisplayValue()+"</td>"+
//					"</tr>";
//		}
//			  
//		tablaPagos=tablaPagos+
//	            
//			"<tr class='item-row' style='vertical-align: top;'>"+
//			"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			 "   <td class='item-name' style='padding: 3px;text-align: left'>Fecha Pago:</td>"+
//			  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFechaR.getDisplayValue()+"</td>"+
//			"</tr>"+
//	          
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Valor Cancelado:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtValor.getDisplayValue()+"</td>"+
//			  "</tr>"+
//
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Saldo:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtSaldo.getDisplayValue()+"</td>"+
//			 " </tr>"+
//
//	         " <tr class='item-row' style='vertical-align: top;'>"+
//	         "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			  "    <td class='item-name' style='padding: 3px;text-align: left'>Total Valor Factura:</td>"+
//	           "   <td class='item-name' style='padding: 3px;text-align: left'>"+txtTotal.getDisplayValue()+"</td>"+
//			  "</tr>"+
//
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Estado:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtEstado.getDisplayValue()+"</td>"+
//			  "</tr>"+
//			  
//			  "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Forma de Pago:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFormaPago.getDisplayValue()+"</td>"+
//			  "</tr>"+
//			  		
//	"		</table>"+
//	        
//	 "       <table id='responsable' style='margin: 30px 0 0 0; width: 600px; text-align: center'>"+
//		
//		"	  <tr class='item-row' style='vertical-align: top;'>"+
//		"	      <td class='item-name' style='width: 50px;'>&nbsp;</td>"+
//		"	      <td class='item-name' style='width: 250px;'>-------------------</td>"+
//	     "        <td class='item-name' style='width: 250px;'>-------------------</td>"+
//	     "	      <td class='item-name' style='width: 50px;'>&nbsp;</td>"+
//			"  </tr>"+
//	          
//	         " <tr class='item-row' style='vertical-align: top;'>"+
//	         "	      <td class='item-name' style='width: 50px;'>&nbsp;</td>"+
//			  "    <td class='item-name' style=' width: 250px;'>"+persona+"</td>"+
//	           "   <td class='item-name' style=' width: 250px;'>Responsable</td>"+
//	           "	      <td class='item-name' style='width: 50px;'>&nbsp;</td>"+
//			  "</tr>"+
//	           	  		
//			"</table>"+
//	        	
//	"</body>"+
//
//	"</html>";
//		
//		Object[] a=new Object[]{tablaPagos};
//        Canvas.showPrintPreview(a);
//	}
	
	
	public void impresionPrintPreview(String persona, String idPago){
		
		
		
//			"<div style='margin-top: 7px; text-align: center'>"+
//	        "<table id='items' style='margin: 0 auto; width: 600px; text-align: center'>"+
//			
//			  "<tr>"+
//			      "<th colspan='3' style='width:600px; padding: 5px;text-align: center'>Pago # "+pagodto.getIdPago()+"</th>"+    
//			  "</tr>"+
//	          
//	           "<tr>"+
//			    "  <th colspan='3' style='padding: 3px;'>&nbsp;</th>"+
//			  "</tr>"+
//								  
//			  "<tr class='item-row' style='vertical-align: top;'>"+
//			  "   <td class='item-name' style='width: 150px;text-align: left'>&nbsp;</td>"+
//			  
//			   "   <td class='item-name' style='padding: 3px; width: 150px;text-align: left'>"+persona+":</td>"+
//	            "  <td class='item-name' style='padding: 3px; width: 300px;text-align: left'>"+pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial()+"</td>"+
//			  "</tr>"+
//			
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Num Transaccion:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFactura.getDisplayValue()+"</td>"+
//			  "</tr>";
//		if (tipoTrans=="1"){
//		
//			
//		tablaPagos=tablaPagos+
//	            
//			"<tr class='item-row' style='vertical-align: top;'>"+
//			"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			 "   <td class='item-name' style='padding: 3px;text-align: left'>Num Comprobante:</td>"+
//			  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtComprobante.getDisplayValue()+"</td>"+
//			"</tr>";
//		}else{
//			tablaPagos=tablaPagos+
//		            
//					"<tr class='item-row' style='vertical-align: top;'>"+
//					"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//					 "   <td class='item-name' style='padding: 3px;text-align: left'>Localizacion:</td>"+
//					  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtLocalizacion.getDisplayValue()+"</td>"+
//					"</tr>";
//		}
//			  
//		tablaPagos=tablaPagos+
//	            
//			"<tr class='item-row' style='vertical-align: top;'>"+
//			"   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			 "   <td class='item-name' style='padding: 3px;text-align: left'>Fecha Pago:</td>"+
//			  "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFechaR.getDisplayValue()+"</td>"+
//			"</tr>"+
//	          
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Valor Cancelado:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtValor.getDisplayValue()+"</td>"+
//			  "</tr>"+
//
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Saldo:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtSaldo.getDisplayValue()+"</td>"+
//			 " </tr>"+
//
//	         " <tr class='item-row' style='vertical-align: top;'>"+
//	         "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			  "    <td class='item-name' style='padding: 3px;text-align: left'>Total Valor Factura:</td>"+
//	           "   <td class='item-name' style='padding: 3px;text-align: left'>"+txtTotal.getDisplayValue()+"</td>"+
//			  "</tr>"+
//
//	          "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Estado:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtEstado.getDisplayValue()+"</td>"+
//			  "</tr>"+
//			  
//			  "<tr class='item-row' style='vertical-align: top;'>"+
//	          "   <td class='item-name' style='text-align: left'>&nbsp;</td>"+
//			   "   <td class='item-name' style='padding: 3px;text-align: left'>Forma de Pago:</td>"+
//	            "  <td class='item-name' style='padding: 3px;text-align: left'>"+txtFormaPago.getDisplayValue()+"</td>"+
//			  "</tr>"+
//			  		
//	"		</table>";
		
		
		
		
		String tablaPagos="";
		tablaPagos="<html>"+
		"<head>"+        
		"<title>Pagos Proveedores</title>"+
	    	
		"</head>"+
		"<body style='font-family: Verdana, Geneva, sans-serif, font-size: 12px;'>"+

		"<div style='margin-top: 15px; text-align: center'>"+
		
		
		"<h3> "+Factum.banderaNombreEmpresa+ "</h3>"+
		
		"<h4> "+idPago+ "</h4>"+
		
		"<h4> PAGOS A PROVEEDORES </h4>";
		
		
		
		
		
//		for(int i = 0; i < pagosList.size(); i++ ){
			
			
			
			tablaPagos=tablaPagos+


		"<div style='display: flex;flex-flow: column;'>"+

		"<p>PROVEEDOR: "+pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial()+"</p>"+

		"<p>FECHA: "+txtFechaR.getDisplayValue()+"</p>"+

//		"<p>DOCUMENTOS: </p>"+
		
		"</div>";
		
		if (tipoTrans=="1"){
		
			
		tablaPagos=tablaPagos+

"<table style='width: 85%; margin: 0 auto;'>" +
	"  <tr> " +
	 "   <td style='border: 1px solid black; background: #a8a5a5'>No. Doc</td> "+
	  "  <td style='border: 1px solid black; background: #a8a5a5'>Total Doc.</td> "+
	   " <td style='border: 1px solid black; background: #a8a5a5;'>Saldo Pend.</td> "+
	   " <td style='border: 1px solid black; background: #a8a5a5'>Pagos</td> "+
	  " </tr> "+
	  " <tr> ";
	  
//	   " <td>"+txtComprobante.getDisplayValue()+"</td> "+
//" <td>"+listPago.get(i).getIdPago()+"</td> "+
		LinkedList<TblpagoDTO> listPago=new LinkedList<TblpagoDTO>();
		String total = "";
				for(int i = 0; i < listPago.size() ; i++ ){
					idPago = String.valueOf(listPago.get(i).getIdPago());
					total = String.valueOf(listPago.get(i).getValor());
							
				}
				tablaPagos=tablaPagos+

						" <td> idPago: "+idPago+"</td> "+
						" <td> total: "+total+"</td> "+
	   " <td>"+txtTotal.getDisplayValue()+"</td> "+
	    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
	    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
	  " </tr> "+
	  " <tr> "+
	   " <td>Totales</td> "+
	    " <td>"+txtTotal.getDisplayValue()+"</td> "+
	    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
	  " </tr> "+ 
	   " <tr> "+
	   " <td>Total Pagado:</td> "+
	   " <td></td> "+
	    " <td></td> "+
	    " <td style='background: #f6f6f6'>"+txtSaldo.getDisplayValue()+"</td>"+
	  " </tr> "+
	  
	  
	" </table>";

		}else{
			tablaPagos=tablaPagos+
					
					" <table style='width: 35%; margin: 0 auto;'>" +
					"  <tr> " +
					 "   <td style='border: 1px solid black; background: #a8a5a5;'>Localizacion</td> "+
					  "  <td style='border: 1px solid black; background: #a8a5a5;'>Total Doc.</td> "+
					   " <td style='border: 1px solid black; background: #a8a5a5;'>Saldo Pend.</td> "+
					   " <td style='border: 1px solid black; background: #a8a5a5;'>Pagos</td> "+
					  " </tr> "+
					  " <tr> "+
					  
					   " <td>"+txtLocalizacion.getDisplayValue()+"</td> "+
					   " <td>"+txtTotal.getDisplayValue()+"</td> "+
					    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
					    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
					  " </tr> "+
					  " <tr> "+
					   " <td>Totales</td> "+
					   " <td>"+txtTotal.getDisplayValue()+"</td> "+
					    " <td>"+txtSaldo.getDisplayValue()+"</td> "+
					  " </tr> "+ 
					   " <tr> "+
					   " <td>Total Pagado:</td> "+
					   " <td></td> "+
					    " <td></td> "+
					    " <td style='background: #f6f6f6'>"+txtSaldo.getDisplayValue()+"</td>"+
					  " </tr> "+
					  
					  
					" </table>";
		}

		tablaPagos=tablaPagos+

			
		
		"<div style='display: flex;flex-flow: column;margin-right: 433px;'>"+
		
		"<br>"+
		
		"<p>DETALLES:</p>"+
		
		"<p>Forma de pago:        "+txtFormaPago.getDisplayValue()+"</p>"+
		
		"<p>Observaciones:</p>"+
		
		"</div>"+
				
				
		"<div style='display: inline-flex;'>"+
			
		"<p>Emitido por: </p> "+
		
		"<p style='margin-left: 240px;'>Recibido por:</p> "+
		
		"</div>"+
		
		"<br>"+
		"<br>"+
		"<br>"+
		
		"<div style='display: flex;flex-flow: column;margin-left: 296px;'>"+
		
		"<p>Nombre:</p> "+
		
		"<p>CI:</p> "+
		
		"</div>"+
		
	

		
		
	"</div>"+
	"</div>"+
	
	
	
	
	"</body>"+

	"</html>";
		
		
//		<div style="display: flex;flex-flow: column;margin-left: 296px;"><p>Nombre: </p> <p style="
//			    /* margin-left: 250px; */
//			">CI:</p> </div>
		
		
		Object[] a=new Object[]{tablaPagos};
        Canvas.showPrintPreview(a);
		}
//	}
	
	
	
	

	
	
	
	
	
	final AsyncCallback<TblpagoDTO>  objbackPagoDTO=new AsyncCallback<TblpagoDTO>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(TblpagoDTO result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			pagodto=result;
//			txtCliente.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getNombreComercial());	
			txtCliente.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getRazonSocial());	
			txtFactura.setValue(pagodto.getTbldtocomercial().getNumRealTransaccion());
			txtFormaPago.setValue(pagodto.getFormaPago());
			txtValor.setValue(pagodto.getValor());
			
			String estado="";	
			if (tipoTrans=="0"){
				txtLocalizacion.setValue(pagodto.getTbldtocomercial().getTblpersonaByIdPersona().getLocalizacion());
				estado="COBRADO";
			}else if (tipoTrans=="1"){
				txtComprobante.setValue(pagodto.getTbldtocomercial().getNumCompra());
				estado="PAGADO";
			}
			
			if(String.valueOf(pagodto.getEstado()).equals("1")){
				txtValor.setValue(pagodto.getValor());
				txtFechaR.setValue(pagodto.getFechaRealPago());
				//txtEstado.setValue("COBRADO ");
				txtEstado.setValue(estado+" ");
				//txtPago.setVisible(false);
				//cmbDocumento.setVisible(false);
				//txtObservaciones.setVisible(false);
			}else{
				txtEstado.setValue("PENDIENTE ");
				btnImprimir.setVisible(false); 
			}
			Iterator iterPago= pagodto.getTbldtocomercial().getTblpagos().iterator();
			TblpagoDTO tPago = new TblpagoDTO();
			double saldo=0;
			while (iterPago.hasNext()) {
				tPago = (TblpagoDTO) iterPago.next();
				if(String.valueOf(tPago.getEstado()).equals("1")){
					saldo=saldo+tPago.getValor();
				}
			}
			if(String.valueOf(pagodto.getTbldtocomercial().getEstado())=="1")
			{	
				Iterator iterTiposPago= pagodto.getTbldtocomercial().getTbldtocomercialTbltipopagos().iterator();
				DtocomercialTbltipopagoDTO tTiposPago = new DtocomercialTbltipopagoDTO();
				while (iterTiposPago.hasNext()) {
					tTiposPago = (DtocomercialTbltipopagoDTO) iterTiposPago.next();
				if(String.valueOf(tTiposPago.getTipoPago()).compareTo("2")!=0)
				{
						saldo=saldo+tTiposPago.getCantidad();
					}
				}
			}
			
			Iterator iterDetalle=pagodto.getTbldtocomercial().getTbldtocomercialdetalles().iterator();
			DtoComDetalleDTO det=new DtoComDetalleDTO();
			double total=0;
			if(tipo=="Nota de Entrega"||tipo=="Nota de Compra"||tipo=="Nota de Venta en Compras")
			{	
			
				while(iterDetalle.hasNext()){
					det=(DtoComDetalleDTO) iterDetalle.next();
					total=total+det.getTotal();
				}
			}
			else
			{
				while(iterDetalle.hasNext()){
					det=(DtoComDetalleDTO) iterDetalle.next();
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+det.getTblimpuestos().size());
					for (DtoComDetalleMultiImpuestosDTO detalleImp: det.getTblimpuestos()){
						i1=i1+1;
						impuestos+=detalleImp.getPorcentaje().toString();
						impuestos= (i1<det.getTblimpuestos().size())?impuestos+",":impuestos; 
						impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
						impuestoValor=impuestoValor+((det.getCantidad()*det.getPrecioUnitario()*(1-(det.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					total=total+((det.getImpuesto()*det.getTotal()/100)+det.getTotal());
					total=total+(impuestoValor+det.getTotal());
				}
			}	
			
			total=Math.rint(total*100)/100;
			saldo=total-saldo;
			saldo= Math.rint(saldo*100)/100;
			txtSaldo.setValue(saldo);
			txtTotal.setValue(total);
			
		}
	};
	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			Label lblCliente = new Label("CLIENTE"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+
					"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+
					"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"RESPONSABLE");
			lblCliente.setAlign(Alignment.CENTER); 
			Label lbl1 = new Label("&nbsp");
			Label lbl2 = new Label("&nbsp");
			Label lbl3 = new Label("&nbsp");
			Label lbl4 = new Label("&nbsp");
			Label lbl5 = new Label("_____________"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+"&nbsp"+
					"_______________");
			lbl5.setAlign(Alignment.CENTER); 
			
			Object[] a=new Object[]{lblPagos,dynamicForm,lbl1,lbl1,lbl2,lbl3,lbl4,lbl5,lblCliente};
            Canvas.showPrintPreview(a);
			SC.say(result);
			
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
