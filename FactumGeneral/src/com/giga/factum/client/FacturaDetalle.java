package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.SC;

public class FacturaDetalle extends VLayout{

	
	DynamicForm dynamicForm = new DynamicForm();
	FormItem []fila2;
	
	public FacturaDetalle(){
		
		HLayout encabezado = new HLayout();
		
		VLayout layout = new VLayout();
		layout.setHeight100();
		layout.setWidth("80%");
		
		
		dynamicForm.setSize("100%", "100%");
		dynamicForm.setItemLayout(FormLayoutType.ABSOLUTE);
			
		
		TextItem codBarras1 = crearItem("codBarras1",0,0,150,22);
		TextItem descripcion1 = crearItem("descripcion1",156,0,244,22);
		TextItem cantidad1 = crearItem("cantidad1",406,0,70,22);
		TextItem precio1 = crearItem("precio1",482,0,70,22);
		TextItem subtotal1 = crearItem("subtotal1",558,0,70,22);
		TextItem stock1 = crearItem("stock1",634,0,50,22);
		TextItem iva1 = crearItem("iva1",690,0,50,22);
		FormItem []fila1 = new FormItem[]{codBarras1,descripcion1,cantidad1,precio1,subtotal1,stock1,iva1};
		
		TextItem codBarras2 = crearItem("codBarras2",0,0,150,22);
		TextItem descripcion2 = crearItem("descripcion2",156,0,244,22);
		TextItem cantidad2 = crearItem("cantidad2",406,0,70,22);
		TextItem precio2 = crearItem("precio2",482,0,70,22);
		TextItem subtotal2 = crearItem("subtotal2",558,0,70,22);
		TextItem stock2 = crearItem("stock2",634,0,50,22);
		TextItem iva2 = crearItem("iva2",690,0,50,22);
		fila2 = new FormItem[]{codBarras2,descripcion2,cantidad2,precio2,subtotal2,stock2,iva2};
		
		descripcion1.addKeyPressHandler(new Manejador());
		descripcion1.setHint("Presione ENTER para buscar");
		descripcion1.setShowHintInField(true);
		
		dynamicForm.setFields(fila1);
		
		layout.addMember(dynamicForm);		
		encabezado.setHeight100();
		encabezado.setWidth("20%");
		encabezado.setShowEdges(true);
		addMember(encabezado);
		addMember(layout);
	}
	
	public TextItem crearItem(String nombre, Integer left, Integer top, Integer width, Integer height){
		TextItem item = new TextItem(nombre);
		item.setLeft(left);
		item.setTop(top);
		item.setWidth(width);
		item.setHeight(height);
		return item;
	}
	
	private class Manejador implements KeyPressHandler {
        public void onKeyPress(KeyPressEvent event) {
            String item = event.getItem().getName();
            if ("Enter".equals(event.getKeyName())) {
               
            }
        }
    }
}
