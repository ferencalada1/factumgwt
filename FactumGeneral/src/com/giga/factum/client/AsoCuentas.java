package com.giga.factum.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.TbltipodocumentoDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;

public class AsoCuentas extends VLayout{

	
	DynamicForm dynamicForm_1 = new DynamicForm();
	DynamicForm dynamicForm_2 = new DynamicForm();
	DynamicForm dynamicForm_3 = new DynamicForm();
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapTipoPago = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapRetencion = new LinkedHashMap<String, String>();
	FormItem[] items;
	FormItem[] itemsPagos;
	FormItem[] itemsImpuestos;
	ButtonItem btnGrabar = new ButtonItem("GRABAR");
	List<TbltipodocumentoDTO> tipoDocumentos = new ArrayList<TbltipodocumentoDTO>();
	List<TbltipodocumentoDTO> tipoDocumentosBase = new ArrayList<TbltipodocumentoDTO>();
	
	List<TipoPagoDTO> tipoPagos = new ArrayList<TipoPagoDTO>();
	List<TipoPagoDTO> tipoPagosBase = new ArrayList<TipoPagoDTO>();
	
	List<ImpuestoDTO> tipoImpuestos = new ArrayList<ImpuestoDTO>();
	List<ImpuestoDTO> tipoImpuestosBase = new ArrayList<ImpuestoDTO>();
	String planCuentaID;
	
	public AsoCuentas(){
		getService().getUserFromSession(callbackUser);
		HLayout hLayout = new HLayout();
		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setShowTitlesWithErrorMessages(true);
		dynamicForm.setTitle("Configuraci�n de Documentos");
		//dynamicForm.setFields(new FormItem[] { new ButtonItem("newButtonItem_1", "New ButtonItem")});
		
		//Aqui vamos a llamar a la funcion de la base para extraer los documentos y que nos muestre
		//un listado dinamico de los documentos existentes 
		//dynamicForm.setFields(new FormItem[] { new ComboBoxItem("newComboBoxItem_1", "New ComboBoxItem"), new ComboBoxItem("newComboBoxItem_2", "New ComboBoxItem")});
		//hLayout.addMember(dynamicForm);
		//dynamicForm_1.setFields(new FormItem[] { new ComboBoxItem("newComboBoxItem_1", "New ComboBoxItem")});
		
		dynamicForm_1.setWidth("33%");
		dynamicForm_2.setWidth("33%");
		dynamicForm_3.setWidth("33%");
		hLayout.addMember(dynamicForm_1);
		hLayout.addMember(dynamicForm_2);
		hLayout.addMember(dynamicForm_3);
		btnGrabar.addClickHandler(new Manejador());
		addMember(hLayout);
		//getService().listarCuenta(0, 40, listaCuentas);
		getService().listarTiposDocumento(0, 60, tiposDocumento);
		getService().listarTipopago(0, 60, tiposPago);
		getService().listarImpuesto(0, 60, tiposImpuesto);
		
		
		

		
	}
	
	final AsyncCallback<List<TbltipodocumentoDTO>>  tiposDocumento =new AsyncCallback<List<TbltipodocumentoDTO>>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 		}
 		public void onSuccess(List<TbltipodocumentoDTO> result) {
 			Iterator iter = result.iterator();
 			items = new FormItem[result.size()];
 			Integer i = 0;
 			tipoDocumentos = result;
 			String documento="";
 			while(iter.hasNext())
 			{
 				TbltipodocumentoDTO tipoDoc = new TbltipodocumentoDTO();
 				tipoDoc = (TbltipodocumentoDTO) iter.next();
 				//documento = tipoDoc.getTipoDoc();
 				//documento.replaceAll(" ", "&nbsp");//.replace(' ', '&nbsp');"
 				items[i]= new ComboBoxItem(tipoDoc.getTipoDoc(), tipoDoc.getTipoDoc());
 				items[i].setValue(tipoDoc.getIdCuenta());
 				      i++;
 			}
 			
 			items[i] = btnGrabar;
 			dynamicForm_1.setFields(items);
 			dynamicForm_1.redraw();
 			//registros=result;
 			//SC.say("Numero de registros "+registros);
 		}
 	};
	
 	final AsyncCallback<List<TipoPagoDTO>>  tiposPago =new AsyncCallback<List<TipoPagoDTO>>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 		}
 		public void onSuccess(List<TipoPagoDTO> result) {
 			Iterator iter = result.iterator();
 			itemsPagos = new FormItem[result.size()];
 			Integer i = 0;
 			tipoPagos = result;
 			while(iter.hasNext())
 			{
 				TipoPagoDTO tipoPago = new TipoPagoDTO();
 				tipoPago = (TipoPagoDTO) iter.next();
 				itemsPagos[i]= new ComboBoxItem(tipoPago.getTipoPago(), tipoPago.getTipoPago());
 				itemsPagos[i].setValue(tipoPago.getIdCuenta());
 				      i++;
 			}
 			
 			//items[i] = btnGrabar;
 			dynamicForm_2.setFields(itemsPagos);
 			dynamicForm_2.redraw();
 			//registros=result;
 			//SC.say("Numero de registros "+registros);
 		}
 	};
 	
 	
 	final AsyncCallback<List<ImpuestoDTO>>  tiposImpuesto =new AsyncCallback<List<ImpuestoDTO>>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 		}
 		public void onSuccess(List<ImpuestoDTO> result) {
 			Iterator iter = result.iterator();
 			itemsImpuestos = new FormItem[result.size()];
 			Integer i = 0;
 			tipoImpuestos = result;
 			while(iter.hasNext())
 			{
 				ImpuestoDTO tipoImpuesto = new ImpuestoDTO();
 				tipoImpuesto = (ImpuestoDTO) iter.next();
 				itemsImpuestos[i]= new ComboBoxItem(tipoImpuesto.getNombre(),tipoImpuesto.getNombre() );
 				itemsImpuestos[i].setValue(tipoImpuesto.getIdCuenta());
 				      i++;
 			}
 			
 			//items[i] = btnGrabar;
 			dynamicForm_3.setFields(itemsImpuestos);
 			dynamicForm_3.redraw();
 			getService().listarCuenta(0, 2000, planCuentaID,listaCuentas);
 			//registros=result;
 			//SC.say("Numero de registros "+registros);
 		}
 	};
 	
 	
 	
 	final AsyncCallback<List<CuentaDTO>>  listaCuentas=new AsyncCallback<List<CuentaDTO>>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 		}
 		public void onSuccess(List<CuentaDTO> result) {
 		
 			MapCategoria.clear();
	 			MapCategoria.put("","");
	             for(int i=0;i<result.size();i++) {
	             	MapCategoria.put(String.valueOf(result.get(i).getIdCuenta()), 
	 						result.get(i).getNombreCuenta());
	 			}
	             for(int j=0;j<items.length-1;j++)
	             {
	            	 dynamicForm_1.getField(items[j].getName()).setValueMap(MapCategoria);
	             }
	             
	             for(int j=0;j<itemsPagos.length;j++)
	             {
	            	 dynamicForm_2.getField(itemsPagos[j].getName()).setValueMap(MapCategoria);
	             } 
	            
	             for(int j=0;j<itemsImpuestos.length;j++)
	             {
	            	 dynamicForm_3.getField(itemsImpuestos[j].getName()).setValueMap(MapCategoria); 
	             }
            	 
	             dynamicForm_1.redraw();
	             dynamicForm_2.redraw();
	             dynamicForm_3.redraw();
 			//registros=result;
 			//SC.say("Numero de registros "+registros);
 		}
 	};
 	
 	
 	
 	
 	
 	
 	final AsyncCallback<String>  actualizarAsociaciones=new AsyncCallback<String>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error, no se pudieron actualizar las asociaciones");
 		}
 		public void onSuccess(String result) {
 			SC.say("Actualizacion exitosa" + result);
 		}
 	};
 	
 	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

		public void onSuccess(User result) {
			if (result == null) {//La sesion expiro
        		
                SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

                    public void execute(Boolean value) {
                        if (value) {
                        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
                            //getService().LeerXML(asyncCallbackXML);
                        }
                    }
                });
            }else{
				planCuentaID=result.getPlanCuenta();
            }
		}
		public void onFailure(Throwable caught) {
			SC.say("No se puede obtener el nombre de usuario");
			com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
		}
	};
 	
 	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
		}
	private class Manejador implements ClickHandler{

		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
			TbltipodocumentoDTO tipoDoc;
			TipoPagoDTO tipoPago;
			ImpuestoDTO tipoImpuesto;
			//Aqui analizamos el listado de objetod TIPODOCUMENTO y comparamos con el id de la cuenta seleccionada
			Integer contador = 0;
			for(int i=0; i<tipoDocumentos.size(); i++)
			{
				tipoDoc = new TbltipodocumentoDTO();
				tipoDoc = tipoDocumentos.get(i);
				tipoDoc.setIdCuenta(Integer.parseInt(items[i].getValue().toString()));
				tipoDocumentosBase.add(tipoDoc);
			}
			
			for(int i=0; i<tipoPagos.size(); i++)
			{
				tipoPago = new TipoPagoDTO();
				tipoPago = tipoPagos.get(i);
				tipoPago.setIdCuenta(Integer.parseInt(itemsPagos[i].getValue().toString()));
				tipoPagosBase.add(tipoPago);
			}
			
			for(int i=0; i<tipoImpuestos.size(); i++)
			{
				tipoImpuesto = new ImpuestoDTO();
				tipoImpuesto = tipoImpuestos.get(i);
				tipoImpuesto.setIdCuenta(Integer.parseInt(itemsImpuestos[i].getValue().toString()));
				tipoImpuestosBase.add(tipoImpuesto);
			}
			
			getService().actualizarAso(tipoDocumentosBase,actualizarAsociaciones);
			getService().actualizarAsoImpuestos(tipoImpuestosBase, actualizarAsociaciones);
			getService().actualizarAsoPagos(tipoPagosBase, actualizarAsociaciones);
		}		
	}
}
