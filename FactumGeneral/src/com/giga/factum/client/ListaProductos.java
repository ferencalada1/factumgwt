package com.giga.factum.client;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.regGrillas.Producto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.IButton;

public class ListaProductos extends VLayout{
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	ListGrid lstProductos = new ListGrid();
	SearchForm searchForm = new SearchForm();
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	public ProductoDTO prod=new ProductoDTO();
	
	
	
	ListaProductos(){
		
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
		
		searchForm.setSize("100%", "5%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n","Categoria"
				,"Unidad","Marca");
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		ListGridField lstcodbarras = new ListGridField("codigoBarras", "C\u00F3digo de Barras",150);
        lstcodbarras.setCellAlign(Alignment.LEFT);
        ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",400); 
        ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",100);
        lstimpuesto.setCellAlign(Alignment.CENTER);
        ListGridField lstlifo = new ListGridField("lifo", "LIFO",100);
        lstlifo.setCellAlign(Alignment.RIGHT);
        ListGridField lstfifo = new ListGridField("fifo", "FIFO",100);
        lstfifo.setCellAlign(Alignment.RIGHT);
        ListGridField lstpromedio = new ListGridField("promedio", "Promedio",100);
        lstpromedio.setCellAlign(Alignment.RIGHT);
        ListGridField lstunidad = new ListGridField("Unidad", "Unidad",150);
        ListGridField listGridField_6 = new ListGridField("stock", "Stock",100);
        listGridField_6.setCellAlign(Alignment.CENTER);
        ListGridField lstcategoria =new ListGridField("Categoria", "Categoria",150);
        ListGridField lstMarca= new ListGridField("Marca", "Marca",150);
        lstProductos.setFields(new ListGridField[] { 
        		lstcodbarras, 
    			lstdescripcion,
    			lstunidad,
    			lstcategoria,
    			lstMarca
        });
        addMember(searchForm);
		addMember(lstProductos);
		
		IButton btnSeleccionar = new IButton("Seleccionar");
		//btnSeleccionar.addClickHandler(new ManejadorBotones("seleccionar"));
		//addMember(btnSeleccionar);
		getService().listaProductos(0,20, 1,0,-1,Factum.banderaStockNegativo,listaCallback);
		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones(" "));
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones("buscar"));
			
		 
		
	}
	private class ManejadorBotones implements KeyPressHandler,RecordDoubleClickHandler,ClickHandler{
		String indicador="";
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")){
				buscarL();
			}
		}
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			prod=new ProductoDTO();
			prod.setIdEmpresa(Factum.empresa.getIdEmpresa());
			prod.setEstablecimiento(Factum.getEstablecimientoCero());
			//getListProductoDTO();
			//SC.say("hola en lista");
			//SC.say((String) lstProductos.getSelectedRecord().getAttribute("descripcion"));
			prod.setDescripcion((String) lstProductos.getSelectedRecord().getAttribute("descripcion"));
			SC.say(prod.getDescripcion() +"en getprod");
			//getListProductoDTO();
			
		}
		@Override
		public void onClick(ClickEvent event) {
			
			
		}
	}
	public void getListProductoDTO(){
		//SC.say("getprod");
		try{
			prod.setIdProducto(lstProductos.getSelectedRecord().getAttributeAsInt("idProducto"));
			prod.setDescripcion((String) lstProductos.getSelectedRecord().getAttribute("descripcion"));
			SC.say(prod.getDescripcion() +"en getprod");
			//return  prod;
		}catch(Exception e){
			SC.say(e.getMessage());
			//return null;
		}
	}
	public ProductoDTO getProd(){
		return prod;
	}
	public void buscarL(){
		int ban=1;
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String tabla=searchForm.getItem("cmbBuscar").getDisplayValue();
		String campo=null;
		if(tabla.equals("Ubicaci\u00F3n")){
			ban=3;
			tabla="codigoBarras";
		}else if(tabla.equals("C\u00F3digo de Barras")){
			ban=1;
			tabla="codigoBarras";
		}else if(tabla.equals("Descripci\u00F3n")||tabla.equals("")){
			ban=1;
			tabla="descripcion";
		}else if(tabla.equals("Marca")){
			ban=0;
			campo="marca";
			tabla="tblmarca";
		}else if(tabla.equals("Categoria")){
			ban=0;
			campo="categoria";
			tabla="tblcategoria";
		}else if(tabla.equals("Unidad")){
			ban=0;
			campo="nombre";
			tabla="tblunidad";
		}
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		if(ban==1){
			getService().listarProductoLike(nombre, tabla, listaCallback);
		}else if(ban==0){
			getService().listarProductoJoin(nombre, tabla, campo,1, 0, -1,Factum.banderaStockNegativo,listaCallback);
		}
	}
	
	final AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(LinkedHashMap<Integer, ProductoDTO> result) {
			RecordList listado = new RecordList();
			Iterator iter = result.values().iterator();
            while (iter.hasNext()) {
            	Producto pro =new Producto((ProductoDTO)iter.next());
            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
            	listado.add(pro);
            }
            lstProductos.setData(new RecordList());
            lstProductos.setData(listado);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
