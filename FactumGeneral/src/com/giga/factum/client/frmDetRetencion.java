package com.giga.factum.client;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.ImpuestoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.PuntoEmisionDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;

public class frmDetRetencion extends VLayout{
	DynamicForm frmCabecera = new DynamicForm();
	LinkedList <ImpuestoDTO> Impuestos=new LinkedList <ImpuestoDTO>();
	LinkedHashMap<String,String> MapImpuesto = new LinkedHashMap<String,String>();
	ListGridField impuesto= new ListGridField("lstImpuesto", "Impuesto");
	ListGrid listGrid = new ListGrid();
	FloatItem txtTotal = new FloatItem(); 
	DtocomercialDTO Fact=new DtocomercialDTO();
	LinkedList<RetencionDTO> det=new LinkedList<RetencionDTO>();
	ListGridField baseImponible= new ListGridField("baseImponible", "Base Imponible");
	ListGridField lstBaseImponible= new ListGridField("lstBaseImponible", "Base Valor");
	ListGridField lstPorcentage=new ListGridField("lstPorcentage", "% Retenci\u00F3n");
	String idImpuesto="";
	TextItem txtRuc = new TextItem("txtRuc", "R.U.C");
	TextItem txtPersona = new TextItem("txtPersona", "Se\u00F1ore(s)");
	TextItem txtDireccion=new TextItem("txtDireccion", "Direcci\u00F3n");
	TextItem txtNumDoc=new TextItem("txtNumDoc", "N&uacute;mero de Documento");
	TextItem txtTipoComprobante = new TextItem("txtTipoComprobante", "Tipo de Comprobante");
	TextItem txtAutorizacion= new TextItem("txtAutorizacion", "N\u00B0 Autorizaci\u00F3n");
	DateItem txtFechaEmision = new DateItem("fecha_emision","FECHA&nbspEMISI\u00D3N");
	final Window winClientes = new Window();  
	final Window winFactura = new Window(); 
	frmReporteCaja facturas;
	frmListClientes frmlisCli=new frmListClientes();
	String idCliente="";
	Integer DocFis=0;
	Integer idVendedor=0;
	User usuario = new User();
	int idFactura=0;
	frmReporteCaja form;
	final Window winPago = new Window(); 
	Double iva=0.0;
	Double SubTotal=0.0;
	TextItem  txtNumero=new TextItem("txtNumero", "N\u00FAmero Retenci&oacute;n");
	int tipoTrans;
	String serie;
	int lastNumRealRet;
//	public frmDetRetencion(DtocomercialDTO fact) {
//		try{
//			Fact=fact;
//			txtNumero.setRequired(true);
//			txtRuc.setRequired(true);
//			txtPersona.setRequired(true);
//		//	txtDireccion.setRequired(true);
//			txtNumDoc.setRequired(true);
//			txtTipoComprobante.setRequired(true);
//			txtAutorizacion.setRequired(true);
//			txtFechaEmision.setRequired(true);
//			VLayout layoutCabecera = new VLayout();
//			layoutCabecera.setSize("100%", "30%");
//			txtFechaEmision.setUseTextField(true);
//			txtFechaEmision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
//			txtFechaEmision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
//			txtFechaEmision.setValue(new Date());
//			frmCabecera.setSize("100%", "100%"); 
//			frmCabecera.setNumCols(4);
//			
//			txtNumero.setDisabled(false);   
//			txtNumero.setColSpan(2);
//			txtNumero.setMask("###-###-#########");
//			txtNumero.setKeyPressFilter("[0-9]");	
//			txtNumero.setRequired(true);
//			txtRuc.setDisabled(false);
//			txtRuc.setKeyPressFilter("[0-9]");
//			txtRuc.setLength(13);
//			//txtRuc.setValue(String.valueOf(fact.getTblpersonaByIdPersona().getCedulaRuc()));
//			//txtRuc.setValue(Fact.getTblpersonaByIdPersona().getCedulaRuc());
//			txtPersona.setDisabled(false);
////			txtPersona.setValue(Fact.getTblpersonaByIdPersona().getNombreComercial());
//			txtPersona.setValue(Fact.getTblpersonaByIdPersona().getRazonSocial());
//			txtPersona.addChangedHandler(new ManejadorBotones("RUC"));
//			
//			txtTipoComprobante.setDisabled(false);
//			txtTipoComprobante.setValue("");
//			
//			txtNumDoc.addKeyPressHandler( new ManejadorBotones("factura"));
//			txtDireccion.setValue(Fact.getTblpersonaByIdPersona().getDireccion());
//		
//			frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtFechaEmision, txtRuc, txtTipoComprobante, txtAutorizacion, txtDireccion,txtNumDoc});
//			layoutCabecera.addMember(frmCabecera);
//			addMember(layoutCabecera);
//			
//			VLayout layoutDetalle = new VLayout();
//			layoutDetalle.setSize("100%", "65%");
//			
//			listGrid.setCanEdit(true);
//			listGrid.setCanSelectAll(true);
//			
//			baseImponible.setValueMap("Iva","Sub Total");
//			baseImponible.addChangeHandler(new ManejadorBotones("base"));
//			baseImponible.setCanEdit(false);
//			lstPorcentage.addChangeHandler(new ManejadorBotones("porcentaje"));
//			lstBaseImponible.addChangeHandler(new ManejadorBotones("valor"));
//			
//			
//			listGrid.setFields(new ListGridField[] { impuesto,baseImponible,lstBaseImponible ,  new ListGridField("lstCodigo", "C\u00F3digo"), lstPorcentage, new ListGridField("lstValor", "Valor Retenido")});
//			layoutDetalle.addMember(listGrid);
//			impuesto.addChangeHandler(new ManejadorBotones("impuesto"));
//	        
//	        HStack hStack_1 = new HStack();
//	        hStack_1.setSize("100%", "8%");
//	        
//	        IButton btnAdd = new IButton("Agregar");
//	        hStack_1.addMember(btnAdd);
//	        
//	        IButton btnNewIbutton = new IButton("Eliminar");
//	        btnNewIbutton.addClickHandler(new ManejadorBotones("EImpuesto"));
//	        hStack_1.addMember(btnNewIbutton);
//	        btnAdd.addClickHandler(new ClickHandler() {  
//	            public void onClick(ClickEvent event) {  
//	            	//listGrid.startEditingNew();  
//	            	listGrid.addData(new ListGridRecord());
//	            	listGrid.selectRecord(listGrid.getRecords().length);
//	            	listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", "Seleccione un Impuesto");
//	            	//SC.say("Seleccione un Impuesto");
//	            }  
//	        });  
//        
//	        DynamicForm dynamicForm_1 = new DynamicForm();
//	        dynamicForm_1.setItemLayout(FormLayoutType.TABLE);
//	        txtTotal.setLeft(300);
//	        txtTotal.setTop(0);
//	        
//	        txtTotal.setTextAlign(Alignment.LEFT);
//	        txtTotal.setTitle("TOTAL");
//	        dynamicForm_1.setFields(new FormItem[] { txtTotal});
//	        layoutDetalle.addMember(dynamicForm_1);
//	        layoutDetalle.addMember(hStack_1);
//			addMember(layoutDetalle);
//			//+++++++++++++++++++++ DESABILITAR
//			txtPersona.disable();
//			txtRuc.disable();
//			txtDireccion.disable();
//			txtTipoComprobante.disable();
//			
//			
//			HStack hStack = new HStack();
//			hStack.setSize("100%", "5%");
//			
//			IButton btnGuardar = new IButton("Guardar");
//			hStack.addMember(btnGuardar);
//			btnGuardar.addClickHandler(new ManejadorBotones("grabar"));
//		
//			IButton btnLimpiar = new IButton("Limpiar");
//			hStack.addMember(btnLimpiar);
//			btnLimpiar.addClickHandler(new ManejadorBotones("limpiar"));
//			
//			IButton btnImprimir = new IButton("Imprimir");
//			hStack.addMember(btnImprimir);
//			addMember(hStack);
//			getService().listarImpuesto(0,20, objbacklst);
//			getService().getUserFromSession(callbackUser);
//		}catch (Exception e){
//			//SC.say(e.getMessage());
//			
//		}
//		
//	}
	
		final AsyncCallback<RetencionDTO> objbackDto = new AsyncCallback<RetencionDTO>() {
			// String coincidencias = "";
			public void onSuccess(RetencionDTO result) {
				if (result != null) {
					txtAutorizacion.setValue(result.getAutorizacionSri());
//					Integer c = Integer.valueOf(result.getNumRealRetencion().split("-")[2]);
//					c += 1;
					//String ct=c.toString();
					String ct=String.valueOf(Integer.valueOf(result.getNumRealRetencion().split("-")[2])+1);
					int cont=9-ct.length();
					while (cont>0)	{ct="0"+ct;cont--;}
					lastNumRealRet=result.getTbldtocomercialByIdDtoComercial().getNumRealTransaccion();
					txtNumero.setValue(serie+"-"+ct.toString());
				} else {
					// Aqui analizamos si es diferente de gasto
					txtNumero.setValue(serie+"-"+"000000001");
				}
			}

			public void onFailure(Throwable caught) {
				SC.say("Error al acceder al servidor: " + caught);
			}
		};
	
	public frmDetRetencion(int tipoTrans) {
		try{
			PuntoEmisionDTO punto=Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0);
			serie=punto.getEstablecimiento()+"-"+punto.getPuntoemision();
			int tipoDoc=(tipoTrans==0)?5:19;
			getService().ultimaRetencion("where tbldtocomercialByIdDtoComercial.tipoTransaccion = "
//					+ "'"+tipoDoc+"' "
					, tipoDoc
					, "tbldtocomercialByIdDtoComercial.idDtoComercial", objbackDto);	
			this.tipoTrans=tipoTrans;
			txtNumero.setRequired(true);
			txtRuc.setRequired(true);
			txtPersona.setRequired(true);
		//	txtDireccion.setRequired(true);
			txtNumDoc.setRequired(true);
			txtTipoComprobante.setRequired(true);
			txtAutorizacion.setRequired(true);
			txtFechaEmision.setRequired(true);
			VLayout layoutCabecera = new VLayout();
			layoutCabecera.setSize("100%", "30%");
			txtFechaEmision.setUseTextField(true);
			txtFechaEmision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaEmision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaEmision.setValue(new Date());
			frmCabecera.setSize("100%", "100%"); 
			frmCabecera.setNumCols(4);
			
			txtNumero.setDisabled(false);   
			txtNumero.setColSpan(2);
			//txtNumero.setMask("###-###-#########");
			txtNumero.setMask("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]");
			txtNumero.setMaskPromptChar("0");
			txtNumero.setMaskPadChar("0");
			txtNumero.addChangedHandler(new com.smartgwt.client.widgets.form.fields.events.ChangedHandler(){

				@Override
				public void onChanged(com.smartgwt.client.widgets.form.fields.events.ChangedEvent event) {
					// TODO Auto-generated method stub
					String val=(String)event.getValue();
					while (val.length()<15){
						val+="0";
					}
					event.getItem().setValue(val);
				}
				
			});
			txtNumero.setMaskOverwriteMode(true);
//			txtNumero.setKeyPressFilter("[0-9]");	
			txtRuc.setDisabled(false);
			txtRuc.setKeyPressFilter("[0-9]");
			txtRuc.setLength(13);
			//txtRuc.setValue(String.valueOf(fact.getTblpersonaByIdPersona().getCedulaRuc()));
			//txtRuc.setValue(Fact.getTblpersonaByIdPersona().getCedulaRuc());
			txtPersona.setDisabled(false);
//			txtPersona.setValue(Fact.getTblpersonaByIdPersona().getNombreComercial());
			txtPersona.setValue("");
			txtPersona.addChangedHandler(new ManejadorBotones("RUC"));
			
			txtTipoComprobante.setDisabled(false);
			txtTipoComprobante.setValue("");
			
			txtNumDoc.addKeyPressHandler( new ManejadorBotones("factura"));
			txtDireccion.setValue("");
		
			frmCabecera.setFields(new FormItem[] {txtNumero, txtPersona, txtFechaEmision, txtRuc, txtTipoComprobante, txtAutorizacion, txtDireccion,txtNumDoc});
			layoutCabecera.addMember(frmCabecera);
			addMember(layoutCabecera);
			
			VLayout layoutDetalle = new VLayout();
			layoutDetalle.setSize("100%", "65%");
			
			listGrid.setCanEdit(true);
			listGrid.setCanSelectAll(true);
			
			baseImponible.setValueMap("Iva","Sub Total");
			baseImponible.addChangeHandler(new ManejadorBotones("base"));
			baseImponible.setCanEdit(false);
			lstPorcentage.addChangeHandler(new ManejadorBotones("porcentaje"));
			lstBaseImponible.addChangeHandler(new ManejadorBotones("valor"));
			
			impuesto.setAutoFetchDisplayMap(true);
			listGrid.setFields(new ListGridField[] { impuesto,baseImponible,lstBaseImponible ,  new ListGridField("lstCodigo", "C\u00F3digo"), lstPorcentage, new ListGridField("lstValor", "Valor Retenido")});
			layoutDetalle.addMember(listGrid);
			impuesto.addChangeHandler(new ManejadorBotones("impuesto"));
			impuesto.addChangedHandler(new ManejadorBotones("impuesto"));
	        
	        HStack hStack_1 = new HStack();
	        hStack_1.setSize("100%", "8%");
	        
	        IButton btnAdd = new IButton("Agregar");
	        hStack_1.addMember(btnAdd);
	        
	        IButton btnNewIbutton = new IButton("Eliminar");
	        btnNewIbutton.addClickHandler(new ManejadorBotones("EImpuesto"));
	        hStack_1.addMember(btnNewIbutton);
	        btnAdd.addClickHandler(new ClickHandler() {  
	            public void onClick(ClickEvent event) {  
	            	//listGrid.startEditingNew();  
	            	listGrid.addData(new ListGridRecord());
	            	listGrid.selectRecord(listGrid.getRecords().length);
	            	listGrid.getRecord(listGrid.getRecords().length-1).setAttribute("lstImpuesto", "Seleccione un Impuesto");
	            	//SC.say("Seleccione un Impuesto");
	            }  
	        });  
        
	        DynamicForm dynamicForm_1 = new DynamicForm();
	        dynamicForm_1.setItemLayout(FormLayoutType.TABLE);
	        txtTotal.setLeft(300);
	        txtTotal.setTop(0);
	        
	        txtTotal.setTextAlign(Alignment.LEFT);
	        txtTotal.setTitle("TOTAL");
	        dynamicForm_1.setFields(new FormItem[] { txtTotal});
	        layoutDetalle.addMember(dynamicForm_1);
	        layoutDetalle.addMember(hStack_1);
			addMember(layoutDetalle);
			//+++++++++++++++++++++ DESABILITAR
			txtPersona.disable();
			txtRuc.disable();
			txtDireccion.disable();
			txtTipoComprobante.disable();
			
			
			HStack hStack = new HStack();
			hStack.setSize("100%", "5%");
			
			IButton btnGuardar = new IButton("Guardar");
			hStack.addMember(btnGuardar);
			btnGuardar.addClickHandler(new ManejadorBotones("grabar"));
		
			IButton btnLimpiar = new IButton("Limpiar");
			hStack.addMember(btnLimpiar);
			btnLimpiar.addClickHandler(new ManejadorBotones("limpiar"));
			
			IButton btnImprimir = new IButton("Imprimir");
			hStack.addMember(btnImprimir);
			addMember(hStack);
			//getService().listarImpuesto(0,100, objbacklst);
			getService().listarImpuesto(tipoTrans+1,objbacklst);
			getService().getUserFromSession(callbackUser);
		}catch (Exception e){
			//SC.say(e.getMessage());
			
		}
		
	}
	
	private class ManejadorBotones implements DoubleClickHandler,com.smartgwt.client.widgets.grid.events.ChangedHandler,ChangedHandler,ChangeHandler,ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		String Nombre="";
		String Direccion="";
		String Ruc="";
		String Autorizacion="";
		String fecha="";
		String tipo="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void datosfrm(){
			try{ 
				Nombre=frmCabecera.getItem("txtPersona").getDisplayValue();
				Ruc=frmCabecera.getItem("txtRuc").getDisplayValue();
				Autorizacion=frmCabecera.getItem("txtAutorizacion").getDisplayValue();
				Direccion=(String)frmCabecera.getItem("txtDireccion").getDisplayValue();
				fecha=frmCabecera.getItem("txtFechaEmision").getAttribute("txtFechaEmision");
				tipo=frmCabecera.getItem("txtTipoComprobante").getDisplayValue();
			}catch(Exception e){
				//SC.say(e.getMessage());
			}
			
		}
		public void Total(){
			int j=listGrid.getRecords().length;
			try{
				Double total = 0.0;
				for(int i=0;i<j;i++){
					total=total+listGrid.getRecord(i).getAttributeAsDouble("lstValor");
				}
				total=Math.rint(total*100)/100;
				
				txtTotal.setValue(total);
			//SC.say("calculando total="+total+" "+ j);
			}catch(Exception e){
				txtTotal.setValue(0.00);
				//SC.say(e.getMessage()); 
			}
			
			
		}
		
		@Override 
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("grabar"))
			{
				Set tblretencions = new HashSet(0);
				if(frmCabecera.validate()){
					try{
						String imp=""; 
						String p="";
						
						det=new LinkedList<RetencionDTO>();
						for(int i=0;i<listGrid.getRecords().length;i++){
							imp=listGrid.getRecord(i).getAttribute("lstImpuesto");
							RetencionDTO ret=new RetencionDTO();	
							ret.setAutorizacionSri(frmCabecera.getItem("txtAutorizacion").getDisplayValue());
							ret.setBaseImponible(listGrid.getRecord(i).getAttributeAsDouble("lstBaseImponible"));
//							ret.setdtocomercial(null);
							ret.setTbldtocomercialByIdDtoComercial(null);
							ret.setEstado('1');
							ret.setNumRealRetencion(txtNumero.getDisplayValue());
							DtocomercialDTO dtoTemp=new DtocomercialDTO();
							dtoTemp.setIdEmpresa(Factum.empresa.getIdEmpresa());
							dtoTemp.setEstablecimiento(Factum.getEstablecimientoCero());
							dtoTemp.setPuntoEmision(Factum.getPuntoEmisionCero());
							dtoTemp.setIdDtoComercial(idFactura);
							ret.setTbldtocomercialByIdFactura(dtoTemp);
//							ret.setTbldtocomercialByIdDtoComercial(dtoTemp);
//							ret.setTbldtocomercialByIdDtoComercial(idFactura);
							ret.setNumero(txtNumDoc.getDisplayValue());
							p=p+" "+String.valueOf(ret.getBaseImponible())+" ";
							for(int j=0;j<Impuestos.size();j++){
								SC.say(" nombre "+imp+" "+String.valueOf(Impuestos.get(j).getIdImpuesto()));
								if(imp.equals(String.valueOf(Impuestos.get(j).getIdImpuesto()))){
									ret.setImpuesto(Impuestos.get(j));
									SC.say(imp+" id "+String.valueOf(Impuestos.get(j).getIdImpuesto()));
									break;
								}
							}
							ret.setValorRetenido(listGrid.getRecord(i).getAttributeAsDouble("lstValor"));
							//SC.say(String.valueOf(ret.getImpuesto().getIdImpuesto()));
							det.add(ret);
							tblretencions.add(ret); 
							tblretencions.size();
						}
						
						DtocomercialDTO doc=new DtocomercialDTO();
						int tipoAux=0;
						if(txtTipoComprobante.getDisplayValue().equals("Facturas de Compra"))
						{
							tipoAux=1;
						}	
						doc.setIdEmpresa(Factum.empresa.getIdEmpresa());
						doc.setEstablecimiento(Factum.getEstablecimientoCero());
						doc.setPuntoEmision(Factum.getPuntoEmisionCero());
						doc.setNumRealTransaccion(lastNumRealRet);
//						doc.setAutorizacion(tipoAux+"//"+txtNumDoc.getDisplayValue());
						doc.setAutorizacion(frmCabecera.getItem("txtAutorizacion").getDisplayValue());
						doc.setEstado('1');
						doc.setExpiracion(txtFechaEmision.getDisplayValue());
						doc.setFecha(txtFechaEmision.getDisplayValue());
						doc.setIdAnticipo(null);
						doc.setNumPagos(0);
						doc.setTbldtocomercialdetalles(null);
						doc.setTbldtocomercialTbltipopagos(null);
						doc.setTblmovimientos(null);
						doc.setTblretencionsForIdDtoComercial(tblretencions); 
						int tipoDoc=(tipoTrans==0)?5:19;
						doc.setTipoTransaccion(tipoDoc);
						
						if (tipoDoc==19){ doc.setNumCompra(txtNumDoc.getDisplayValue());}
//						doc.setTipoTransaccion(5);
						doc.setIdNotaCredito(null);
						doc.setIdRetencion(null);
						doc.setSubtotal(Double.parseDouble(txtTotal.getDisplayValue()));
						doc.setTblpagos(null);
						PersonaDTO per=new PersonaDTO();
						per.setIdPersona(Integer.parseInt(idCliente)); 
						per.setCedulaRuc(txtRuc.getDisplayValue());
						doc.setTblpersonaByIdPersona(per);
						per=new PersonaDTO();
						per.setIdPersona(usuario.getIdUsuario());
						doc.setTblpersonaByIdVendedor(per);
						PersonaDTO facturaPor = new PersonaDTO();
						facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
						doc.setTblpersonaByIdFacturaPor(facturaPor);
						doc.setObservacion(usuario.getUserName());
						//SC.say(String.valueOf(det.getFirst().getImpuesto().getIdImpuesto()));
						getService().grabarRetencion(det,doc,objback);
						//getService().grabarDetalle(det, objback);*/
						//SC.say("correcto "+String.valueOf(tblretencions.size()));
					}catch(Exception e){
						SC.say(e.getMessage()+"idFactura= "+String.valueOf(tblretencions.size()));
					}
				}else{
					SC.say("Por favor ingrese todos los datos");
				}
				
			}else if(indicador.equalsIgnoreCase("eimpuesto")){
				 listGrid.removeSelectedData(); 
				// det.remove(listGrid.getRecordIndex(listGrid.getSelectedRecord()));
				 Total();
				 SC.say("eliminar");
			}
			else if(indicador.equalsIgnoreCase("limpiar"))
			{

				for(int i=0;i<listGrid.getRecords().length;i++){
					listGrid.removeData(listGrid.getRecord(i));
				}
				txtNumero.setValue("");
				txtFechaEmision.setValue("");

				txtPersona.setValue("");
				txtRuc.setValue("");
				txtDireccion.setValue("");
				txtAutorizacion.setValue("");
				txtTipoComprobante.setValue("");
				txtNumDoc.setValue("");
			}
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(indicador.equalsIgnoreCase("factura")){
				winPago.clear();
				winPago.setWidth(930);  
				winPago.setHeight(610);  
				winPago.setTitle("Ingresar Documento");  
				winPago.setShowMinimizeButton(false);  
				winPago.setIsModal(true);  
				winPago.setShowModalMask(true);  
				winPago.centerInPage();  
				String tipoText= (tipoTrans==0)?"Facturas de Venta":"Facturas de Compra";
//				form=new frmReporteCaja("Facturas de Venta");
				form=new frmReporteCaja(tipoText);
				form.cmbDocumento.setVisible(false);
				winPago.addCloseClickHandler(new CloseClickHandler() {  
	                public void onCloseClick(CloseClientEvent event) {  
	                	winPago.destroy();  
	                }  
	            });
				
				form.lstReporteCaja.addDoubleClickHandler(new ManejadorBotones("ListadoFac"));
				form.setSize("100%","100%"); 
	            form.setPadding(5);  
	            form.setLayoutAlign(VerticalAlignment.BOTTOM);
	            winPago.addItem(form);
	            winPago.show();
			}
			
		}
		@Override
		public void onChange(ChangeEvent event) {
			Double base=0.0;
			boolean ban=true;
			String evento=(String)event.getValue();
			if(indicador.equalsIgnoreCase("impuesto")){
				idImpuesto=evento;
//				if(baseImponible.getCanEdit()){
					for(int j=0;j<listGrid.getRecords().length;j++){
						if(listGrid.getRecord(j).getAttribute("lstImpuesto").equals(idImpuesto)){
							ban=false;
							listGrid.removeSelectedData();
							SC.say("Error: Impuesto ya ingresado...");
							break;
						}
						
					}
					if(ban){
						for(int i=0;i<Impuestos.size();i++){
							if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
								int[] rows = new int[1];
								Integer numero = event.getRowNum();
								rows[0] = numero;
								
								String baseImp=(Impuestos.get(i).getTipo()==1)?"Sub Total":"Iva";
								listGrid.getSelectedRecord().setAttribute("baseImponible",baseImp);
								double descuento =0.0;
								double valImp=0.0;
//								com.google.gwt.user.client.Window.alert("dtocomercial detalles "+Fact.getTbldtocomercialdetalles());
								for(DtoComDetalleDTO dtocomercialle :  Fact.getTbldtocomercialdetalles()){
									
									String impuestos="";
									double impuestoPorc=1.0;
									double impuestoValor=0.0;
									int i1=0;
//									com.google.gwt.user.client.Window.alert("impuestos de detalle "+dtocomercialle.getTblimpuestos().size());
									for (DtoComDetalleMultiImpuestosDTO detalleImp: dtocomercialle.getTblimpuestos()){
										i1=i1+1;
										impuestos+=detalleImp.getPorcentaje().toString();
										impuestos= (i1<dtocomercialle.getTblimpuestos().size())?impuestos+",":impuestos; 
										impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
//										com.google.gwt.user.client.Window.alert("impuestos  "+detalleImp.getPorcentaje());
//										com.google.gwt.user.client.Window.alert("calculo "+dtocomercialle.getCantidad()+"; "+
//												dtocomercialle.getPrecioUnitario()+"; "+dtocomercialle.getDepartamento()+"; "+impuestoValor);
										impuestoValor=impuestoValor+((dtocomercialle.getCantidad()*dtocomercialle.getPrecioUnitario()*(1-(dtocomercialle.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
									}
//									com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
									valImp+=impuestoValor;
									descuento = descuento + ((dtocomercialle.getTotal()*dtocomercialle.getDepartamento())/(100-dtocomercialle.getDepartamento()));
								}
								if(baseImp.equalsIgnoreCase("Iva")){
//									base=(Fact.getSubtotal()-descuento)*(Factum.banderaIVA/100);
//									base=(Fact.getSubtotal()-descuento)+valImp;
									base=valImp;
									base=Math.rint(base*100)/100;
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
								}else if(baseImp.equalsIgnoreCase("Sub Total")){
									base=(Fact.getSubtotal()-descuento);
									listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
								}
								
								listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
								listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
								Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
								Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
								valor=porcentaje*valor/100;
								valor=Math.rint(valor*100)/100;
								//SC.say("cambio "+valor);
								listGrid.getSelectedRecord().setAttribute("lstImpuesto",evento);
								listGrid.getSelectedRecord().setAttribute("lstValor",valor);
//								listGrid.saveAllEdits(null,rows);
//								//com.google.gwt.user.client.Window.alert("bef refresh");
////								listGrid.refreshRow(event.getRowNum());
//								//com.google.gwt.user.client.Window.alert("aft refresh");
//								listGrid.saveAllEdits();
//								//com.google.gwt.user.client.Window.alert("after save all edits");
////								listGrid.stopHover();
//								listGrid.endEditing();
////								listGrid.refreshFields();
//								listGrid.deselectAllRecords();
								break;
								
							}
						}
					}
//				}
//				baseImponible.setCanEdit(true);
			}else if(indicador.equalsIgnoreCase("base")){
				//SC.say("hola cambiando la base");
				if(evento.equalsIgnoreCase("iva")){
					base=iva;
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
					//SC.say("base iva "+evento);
				}else if(evento.equalsIgnoreCase("Sub Total")){
					base=SubTotal;
					listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base); 
					//SC.say("base sub Total "+evento+" "); 
				}
//				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstCodigo");
				
				for(int i=0;i<Impuestos.size();i++){
//					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getCodigo()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			
			}else if(indicador.equals("valor")){
				//SC.say("hola cambiando la base");
				//SC.say("base iva "+evento);
				listGrid.getSelectedRecord().setAttribute("lstBaseImponible",event.getValue());
				base=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
				//SC.say("base sub Total "+evento+" "); 
			
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				
				for(int i=0;i<Impuestos.size();i++){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",Impuestos.get(i).getRetencion());
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			}else if(indicador.equals("porcentaje")){
				idImpuesto=listGrid.getSelectedRecord().getAttribute("lstImpuesto");
				for(int i=0;i<Impuestos.size();i++){
					if(idImpuesto.equals(String.valueOf(Impuestos.get(i).getIdImpuesto()))){
						listGrid.getSelectedRecord().setAttribute("lstPorcentage",event.getValue());
						Impuestos.get(i).setRetencion(listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage"));
						listGrid.getSelectedRecord().setAttribute("lstCodigo",Impuestos.get(i).getCodigo());
						listGrid.getSelectedRecord().setAttribute("lstBaseImponible",base);
						Double porcentaje=listGrid.getSelectedRecord().getAttributeAsDouble("lstPorcentage");
						Double valor=listGrid.getSelectedRecord().getAttributeAsDouble("lstBaseImponible");
						valor=porcentaje*valor/100;
						valor=Math.rint(valor*100)/100;
						//SC.say("cambio "+valor);
						listGrid.getSelectedRecord().setAttribute("lstValor",valor);
						break;
					}
				}
			}
			Total();	
		}	
		@Override
		public void onChanged(com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
			String evento=(String)event.getValue();
			if(indicador.equalsIgnoreCase("impuesto")){
				listGrid.saveAllEdits();
				listGrid.endEditing();
				listGrid.deselectAllRecords();
			}
		}
		@Override
		public void onChanged(ChangedEvent event) {

		}
		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			if(indicador.equals("ListadoFac")){
				idFactura=form.lstReporteCaja.getSelectedRecord().getAttributeAsInt("id");
				getService().getDtoID(idFactura, callbackDoc);
			}
			
		}
	} 
	
	final AsyncCallback<DtocomercialDTO>  callbackDoc=new AsyncCallback<DtocomercialDTO>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(DtocomercialDTO result) {
			
			Fact=result;
			if (result.getTblretencionsForIdFactura().size()==0){
				idCliente=form.lstReporteCaja.getSelectedRecord().getAttributeAsString("idPersona");
				if(tipoTrans==0){
					txtPersona.setValue(Factum.banderaNombreEmpresa);
					txtDireccion.setValue(Factum.banderaDireccionEmpresa);
					txtRuc.setValue(Factum.banderaRUCEmpresa);
				}else if(tipoTrans==1){
					txtPersona.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("Cliente"));
					txtDireccion.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("direccion"));
					txtRuc.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("RUC"));
				}
					
	//				txtPersona.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("Cliente"));
	//				txtDireccion.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("direccion"));
	//				txtRuc.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("RUC"));
				
				winClientes.destroy();
				String numDoc=(tipoTrans==0)?form.lstReporteCaja.getSelectedRecord().getAttribute("NumRealTransaccion"):
					form.lstReporteCaja.getSelectedRecord().getAttribute("numCompra");
	//			txtNumDoc.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("NumRealTransaccion"));
				txtNumDoc.setValue(numDoc);
				txtTipoComprobante.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("TipoTransaccion"));
				iva=form.lstReporteCaja.getSelectedRecord().getAttributeAsDouble("Iva");
				//SubTotal=form.lstReporteCaja.getSelectedRecord().getAttributeAsDouble("Iva14");
				SubTotal=form.lstReporteCaja.getSelectedRecord().getAttributeAsDouble("Base");
				SC.say("RUC "+String.valueOf(idFactura)+" "+String.valueOf(SubTotal)+" "+String.valueOf(iva));
				winPago.destroy();
			}else{
				SC.say("El documento correspondiente ya consta con una retencion");
			}
		}
	};
	
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
//			for(int i=0;i<listGrid.getRecords().length;i++){
			for(int i=listGrid.getRecords().length-1;i>=0;i--){
				listGrid.removeData(listGrid.getRecord(i));
			}
			txtNumero.setValue("");
			txtFechaEmision.setValue("");

			txtPersona.setValue("");
			txtRuc.setValue("");
			txtDireccion.setValue("");
			txtAutorizacion.setValue("");
			txtTipoComprobante.setValue("");
			txtNumDoc.setValue("");
			txtTotal.setValue("");
		}
	};
	final AsyncCallback<List<ImpuestoDTO>>  objbacklst=new AsyncCallback<List<ImpuestoDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(List<ImpuestoDTO> result) {
			MapImpuesto.clear();
			Impuestos.clear();
            for(int i=0;i<result.size();i++) {
            	Impuestos.add(result.get(i));
            	MapImpuesto.put(String.valueOf(result.get(i).getIdImpuesto()),result.get(i).getNombre());
			}
			impuesto.setValueMap((Map) MapImpuesto); 
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
       }
	};
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
        		usuario=result;
        		//SC.say(usuario.getUserName());
        	//funcionBloquear(true);
			}
        }

        public void onFailure(Throwable caught) {
            
        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}

