package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.regGrillas.RetencionRecords;
import com.giga.factum.server.base.Tbldtocomercialdetalle;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;

public class frmListaRetenciones extends VLayout{
	
	ListGrid lstRetenciones = new ListGrid();//+++ LISTADO GRANDE
	int contador=0;
	ListGrid prueba=new ListGrid();
	ListGridField cliente;
	ListGridField ruc;
	ListGridField fecha;
	ListGridField tipoDocumento;
	ListGridField tipoDocumento2;
	ListGridField numeroFactura ;
	ListGridField numeroDocumento ;
	ListGridField subTotal14;
	ListGridField valorRetenido14;
	ListGridField baseImponible;
	ListGridField valorRetenido;
	ListGridField subTotal0;
	ListGridField valorRetenido0;
	ListGridField codigoImpuesto;
	ListGridField idimpuesto;
	ListGridField porcentajeRetencion;
	ListGridField numeroRealRetencion;
	frmListClientes clientes;
	Window winclientes=new Window();
	
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	DynamicForm dynamicForm3 = new DynamicForm();
	HStack hStackSuperior = new HStack();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	
	
	PickerIcon lstpicker= new PickerIcon(PickerIcon.SEARCH);
	TextItem txtRuc =new TextItem("txtRuc", "<b>R.U.C</b>");
	
	TextItem txtNombreComercial =new TextItem("txtNombreComercial", "<b>Nombre Comercial</b>");
	TextItem txtRazonSocial =new TextItem("txtRazonSocial", "<b>Razon Social</b>");
	
	TextItem txtCodigoImpuesto = new TextItem("txtCodigoImpuesto","<b>Codigo de Impuesto</b>");
	final ComboBoxItem cmbTipoImpuesto = new ComboBoxItem("tipoimpuesto","<b>Tipo Impuesto</b>");
	
	
	final ComboBoxItem cmbTipoRetencion=new ComboBoxItem("tipoRetencion","<b>Tipo Retenci&oacute;n</b>");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	IButton btnImprimir = new IButton("Imprimir");
	IButton btnExportar = new IButton("Exportar");
	LinkedHashMap<String,String> MapTipoRetencion = new LinkedHashMap<String,String>();
	LinkedHashMap<String,String> MapTipoImpuesto = new LinkedHashMap<String,String>();
	int idtipoImpuesto=0;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	Date fechaFactura;// Para la fecha del sistema
	int idTipoRetencion=0;
	Window winRetenciones = new Window();
	Integer numDto=0;//Para saber cual registro seleccion�
	double TotalRetencionCadaTipo=0;
	//int contadorRetencionTipo=0;
		
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public static GreetingServiceAsync getService()
	{
		return GWT.create(GreetingService.class);
	}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	public frmListaRetenciones(){
		getService().fechaServidor(callbackFecha);
		MapTipoRetencion.put("0", "Retenciones en Venta");//clientes
		MapTipoRetencion.put("1", "Retenciones en Compra");// proveedores
//		MapTipoRetencion.put("7", "Retenciones en Gastos");//proveedores
		
		MapTipoImpuesto.put("0", "IVA");//clientes
		MapTipoImpuesto.put("1", "RENTA");// proveedores
		MapTipoImpuesto.put("2", "TODOS");//proveedores
		
		
		try
		{
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
			dynamicForm.setSize("30%", "20%");
			dynamicForm2.setSize("30%", "20%");
			dynamicForm3.setSize("30%", "20%");
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			cmbTipoImpuesto.setValueMap(MapTipoImpuesto);
		    cmbTipoImpuesto.setDefaultToFirstOption(true);
		    cmbTipoImpuesto.addChangedHandler(new ChangedHandler() {
				
				@Override
				public void onChanged(ChangedEvent event) {
					
					idtipoImpuesto = Integer.valueOf(cmbTipoImpuesto.getValue().toString());					
				}
			});
		    
		    txtRuc.setIcons(lstpicker);
			txtNombreComercial.setDisabled(true);
			txtRazonSocial.setDisabled(true);
		    
		    dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal,cmbTipoRetencion});
			dynamicForm2.setFields(new FormItem[] { txtRuc, txtNombreComercial,txtRazonSocial});
			dynamicForm3.setFields(new FormItem[] { txtCodigoImpuesto, cmbTipoImpuesto});
			
			final HStack hStackEspacio = new HStack();
			hStackEspacio.setSize("4%", "20%");
			HStack hStackEspacio1 = new HStack();
			hStackEspacio1.setSize("4%", "20%");
			HStack hStackEspacio2 = new HStack();
			hStackEspacio2.setSize("4%", "20%");
		
			
			hStackSuperior.setSize("100%", "35%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(hStackEspacio);
			hStackSuperior.addMember(dynamicForm2);
			hStackSuperior.addMember(hStackEspacio1);
			hStackSuperior.addMember(dynamicForm3);
			hStackSuperior.addMember(hStackEspacio2);
			//hStackSuperior.addMember(lstImpuesto);
//hStackSuperior.addMember(btnGrabar);		
			VStack vStackFinal = new VStack();
			vStackFinal.setSize("10%", "100%");
			HStack hStackSup1 = new HStack();
			hStackSup1.setSize("100%", "35%");
//	hStackSup1.setBackgroundColor("blue");
			HStack hStackSup2 = new HStack();
			hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
			HStack hStackSup3 = new HStack();
			hStackSup3.setSize("100%", "35%");
		
			vStackFinal.addMember(hStackSup1);
			vStackFinal.addMember(hStackSup2);
			vStackFinal.addMember(hStackSup3);
			hStackSuperior.addMember(vStackFinal);
			addMember(hStackSuperior);
			lstRetenciones = new ListGrid();
			lstRetenciones.setAlign(Alignment.CENTER);
			lstRetenciones.setSize("100%", "100%");
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		cliente =new ListGridField("cliente", "Cliente");
		cliente.setWidth("20%");
		ruc =new ListGridField("ruc", "Ruc");
		ruc.setWidth("15%");
		fecha =new ListGridField("fecha", "Fecha Retencion");
		fecha.setWidth("15%");
		
		numeroFactura=new ListGridField("numeroFactura","Num.de.Factura");
		numeroFactura.setWidth("15%");
		numeroFactura.setAlign(Alignment.CENTER);
		
		numeroDocumento =new ListGridField("numeroDocumento", "Num.Real.Transaccion");
		numeroDocumento.setWidth("10%");
		numeroDocumento.setAlign(Alignment.CENTER);
		
		tipoDocumento=new ListGridField("tipoDocumento","Tipo de Documento");
		tipoDocumento.setWidth("10%");
		tipoDocumento.setAlign(Alignment.CENTER);
		
		baseImponible =new ListGridField("baseImponible", "Base Imponible");
		baseImponible.setWidth("10%");
		baseImponible.setAlign(Alignment.RIGHT);
		
		valorRetenido =new ListGridField("valorRetenido", "Valor Retenido");
		valorRetenido.setWidth("10%");
		valorRetenido.setAlign(Alignment.RIGHT);
		

		subTotal14 =new ListGridField("subTotal14", "SubTotal "+Factum.banderaIVA);
		subTotal14.setWidth("10%");
		subTotal14.setAlign(Alignment.RIGHT);
		
		valorRetenido14 =new ListGridField("valorRetenido14", "ValorRet "+Factum.banderaIVA);
		valorRetenido14.setWidth("10%");
		valorRetenido14.setAlign(Alignment.RIGHT);

		subTotal0 =new ListGridField("subTotal0", "SubTotal 0");
		subTotal0.setWidth("10%");
		subTotal0.setAlign(Alignment.RIGHT);
		
		valorRetenido0 =new ListGridField("valorRetenido0", "ValorRet 0");
		valorRetenido0.setWidth("10%");
		valorRetenido0.setAlign(Alignment.RIGHT);
		
		codigoImpuesto=new ListGridField("codigoImpuesto", "Cod.Impuesto");
		codigoImpuesto.setWidth("10%");
		codigoImpuesto.setAlign(Alignment.CENTER);
		
		porcentajeRetencion =new ListGridField("porcentajeRetencion", "% Retencion");
		porcentajeRetencion.setWidth("10%");
		porcentajeRetencion.setAlign(Alignment.CENTER);
		
		idimpuesto = new ListGridField("idimpuesto","Id Ret");
		idimpuesto.setWidth("10%");
		idimpuesto.setAlign(Alignment.CENTER);
		
		numeroRealRetencion =new ListGridField("numeroRealRetencion", "# Real.Retencion");
		numeroRealRetencion.setWidth("10%");
		numeroRealRetencion.setAlign(Alignment.CENTER);
		
		
		lstRetenciones.setFields(new ListGridField[] {cliente,ruc,fecha,tipoDocumento,numeroFactura,numeroDocumento,
		baseImponible, valorRetenido, codigoImpuesto,porcentajeRetencion, numeroRealRetencion});
		lstRetenciones.setAlign(Alignment.CENTER);
		lstRetenciones.setShowGroupSummary(true); 
		lstRetenciones.setShowGridSummary(true);

		addMember(lstRetenciones);
	
		IButton btnLimpiar = new IButton("Limpiar");
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "20%");
		hStack.addMember(btnGenerarReporte);
		hStack.addMember(btnLimpiar);
		hStack.addMember(btnImprimir);
		hStack.addMember(btnExportar);
	
		txtRuc.setValue("");
		txtNombreComercial.setValue("");
		txtRazonSocial.setValue("");
		cmbTipoRetencion.setValueMap(MapTipoRetencion);
		cmbTipoRetencion.setDefaultToFirstOption(true);
		cmbTipoRetencion.addChangedHandler(new ChangedHandler() 
		{  
	    	public void onChanged(ChangedEvent event) 
	    	{
	    		idTipoRetencion = Integer.valueOf(cmbTipoRetencion.getValue().toString());
			}  
	    });	    
		
		
		btnGenerarReporte.setDisabled(false);
		btnGenerarReporte.addClickHandler(new ManejadorBotones(""));
		
		
		btnLimpiar.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event)
	           {
	        	   limpiar();
	           }
	       });	
	

	btnImprimir.addClickHandler(new ClickHandler(){
		   public void onClick(ClickEvent event){
			   Object[] a=new Object[]{hStackSuperior,lstRetenciones};
			   Canvas.showPrintPreview(a);
		      
		   }});
	
	btnExportar.addClickHandler(new ClickHandler(){
		   public void onClick(ClickEvent event){			
			CreateExelDTO exel=new CreateExelDTO(lstRetenciones);

		   }});


		addMember(hStack);
		}catch(Exception e ){
			SC.say("Error: "+e);
		}
	
		lstpicker.addFormItemClickHandler(new ManejadorBotones(""));
		txtRuc.addKeyPressHandler(new ManejadorBotones("clientes"));
		txtCodigoImpuesto.addKeyPressHandler(new ManejadorBotones("impuesto"));
		
	}
	
	protected void frmListaRetenciones() {
	// TODO Auto-generated method stub
	
	}
	public void limpiar()
	{
		txtRuc.setValue("");
		txtNombreComercial.setValue("");
		txtRazonSocial.setValue("");
		txtCodigoImpuesto.setValue("");
    	cmbTipoRetencion.setValue("Retenciones en Venta");
    	idTipoRetencion=0;
    	cmbTipoImpuesto.setValue("IVA");
    	idtipoImpuesto=0;
    	txtFechaInicial.setValue(fechaFactura);
		txtFechaFinal.setValue(fechaFactura);
		lstRetenciones.setData(new ListGridRecord[]{});
	}
	private class ManejadorBotones implements DoubleClickHandler,KeyPressHandler,RecordDoubleClickHandler,FormItemClickHandler, ChangedHandler, ClickHandler{
		String indicador="";

		
		
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		public void onChanged(ChangedEvent event) {
			// TODO Auto-generated method stub

			if(indicador.equals("TipoImpuesto")){
				
				//lstReporteCaja.setShowRowNumbers(true);		
				redraw();
			}
			
		}
	
		
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			
			if(idTipoRetencion==0)
			{
				
				winclientes = new Window();
				winclientes.setWidth(930);
				winclientes.setHeight(610);
				winclientes.setTitle("Listado Cliente/Proveedor");
				winclientes.setShowMinimizeButton(false);
				winclientes.setIsModal(true);
				winclientes.setShowModalMask(true);
				winclientes.setKeepInParentRect(true);
				winclientes.centerInPage();
				winclientes.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClientEvent event) {
						winclientes.destroy();
					}
				});
				
				clientes=new frmListClientes("tblclientes");
	    		clientes.setSize("100%","100%");
	    		clientes.lstCliente.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
					
					@Override
					public void onRecordDoubleClick(RecordDoubleClickEvent event) {
						// TODO Auto-generated method stub
						dynamicForm2.setValue("txtRuc",clientes.lstCliente.getSelectedRecord().getAttribute("Cedula"));  
						dynamicForm2.setValue("txtNombreComercial", clientes.lstCliente.getSelectedRecord().getAttribute("NombreComercial"));
						dynamicForm2.setValue("txtRazonSocial",clientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
						
						clientes.tabSet.selectTab(0);
						winclientes.destroy();
					}
				});
	    		winclientes.addItem(clientes);
				winclientes.show();
			}
			else if (idTipoRetencion==1||idTipoRetencion==7)
			{
				
				winclientes = new Window();
				winclientes.setWidth(930);
				winclientes.setHeight(610);
				winclientes.setTitle("Listado Cliente/Proveedor");
				winclientes.setShowMinimizeButton(false);
				winclientes.setIsModal(true);
				winclientes.setShowModalMask(true);
				winclientes.setKeepInParentRect(true);
				winclientes.centerInPage();
				winclientes.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClientEvent event) {
						winclientes.destroy();
					}
				});
				
				clientes=new frmListClientes("tblproveedors");
	    		clientes.setSize("100%","100%");
	    		clientes.lstCliente.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
					
					@Override
					public void onRecordDoubleClick(RecordDoubleClickEvent event) {
						// TODO Auto-generated method stub
						dynamicForm2.setValue("txtRuc",clientes.lstCliente.getSelectedRecord().getAttribute("Cedula"));  
						dynamicForm2.setValue("txtNombreComercial", clientes.lstCliente.getSelectedRecord().getAttribute("NombreComercial"));
						dynamicForm2.setValue("txtRazonSocial",clientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
						
						clientes.tabSet.selectTab(0);
						winclientes.destroy();
					}
				});
	    		winclientes.addItem(clientes);
				winclientes.show();
				
			}
		
		}
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		
		public void onKeyPress(KeyPressEvent event) {
			if(indicador=="clientes")
			{
				if(event.getKeyName().equals("Enter")||event.getKeyName().equals("Space"))
				{
					winclientes = new Window();
					winclientes.setWidth(900);
					winclientes.setHeight(400);
					winclientes.setTitle("Ingreso de Persona");
					winclientes.setShowMinimizeButton(false);
					winclientes.setIsModal(true);
					winclientes.setShowModalMask(true);
					winclientes.setKeepInParentRect(true);
					winclientes.centerInPage();
					winclientes.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClientEvent event) {
							winclientes.destroy();
						}
					});
					
					clientes=new frmListClientes("tblclientes");
		    		clientes.setSize("100%","100%");
					clientes.txtBuscarLst.setSelectOnFocus(true);
		    		clientes.lstCliente.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
						
						@Override
						public void onRecordDoubleClick(RecordDoubleClickEvent event) {
							// TODO Auto-generated method stub
							
							dynamicForm2.setValue("txtRuc",clientes.lstCliente.getSelectedRecord().getAttribute("Cedula"));  
							dynamicForm2.setValue("txtNombreComercial", clientes.lstCliente.getSelectedRecord().getAttribute("NombreComercial"));
							dynamicForm2.setValue("txtRazonSocial",clientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));
							
							clientes.tabSet.selectTab(0);
							winclientes.destroy();
						}
					});
		    		
		    		
		    		winclientes.addItem(clientes);
					winclientes.show();
				}
			}
			else if(indicador=="impuesto")
			{
				if(event.getKeyName().equals("Enter"))
				{
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
						getService().listarRetenciones(txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(), String.valueOf(idTipoRetencion),String.valueOf(idtipoImpuesto),txtRuc.getDisplayValue(),txtCodigoImpuesto.getDisplayValue(), objReporteRetencion);	
						limpiar();
				}
			}
			else{
				SC.say("null");
			}
			
		}

		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onClick(ClickEvent event) {	
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
				getService().listarRetenciones(txtFechaInicial.getDisplayValue(), txtFechaFinal.getDisplayValue(), String.valueOf(idTipoRetencion),String.valueOf(idtipoImpuesto),txtRuc.getDisplayValue(),txtCodigoImpuesto.getDisplayValue(), objReporteRetencion);	
				contador++;
				limpiar();		
		}
	
	}
	
	
	
	
	//#############################EVENTOS ASINCRONOS~############################################
	final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
		public void onFailure(Throwable caught) {
			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
			
		}
		public void onSuccess(Date result) {
			fechaFactura=result;
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
		}
	};
	
	final AsyncCallback<List<DtocomercialDTO>>  objReporteRetencion = new AsyncCallback<List<DtocomercialDTO>>()
	{
		public void onFailure(Throwable caught) 
		{
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		
		
		public void onSuccess(List<DtocomercialDTO> resultDto) 
		{
			if (resultDto.size()>0)
			{
				
				for(int i=0;i<resultDto.size();i++) 
				{
					Iterator it =resultDto.get(i).getTblretencionsForIdFactura().iterator();
					while(it.hasNext())
					{
						Iterator it2 =resultDto.get(i).getTbldtocomercialdetalles().iterator();
						double subtotal0=0;
						double subtotal14=0;
						while(it2.hasNext())
						{
							DtoComDetalleDTO dtocomdetalle = (DtoComDetalleDTO) it2.next();
							
							String impuestos="";
							double impuestoPorc=1.0;
							double impuestoValor=0.0;
							int i1=0;
							//com.google.gwt.user.client.Window.alert("impuestos de detalle "+dtocomdetalle.getTblimpuestos().size());
							for (DtoComDetalleMultiImpuestosDTO detalleImp: dtocomdetalle.getTblimpuestos()){
								i1=i1+1;
								impuestos+=detalleImp.getPorcentaje().toString();
								impuestos= (i1<dtocomdetalle.getTblimpuestos().size())?impuestos+",":impuestos; 
								impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
								impuestoValor=impuestoValor+((dtocomdetalle.getCantidad()*dtocomdetalle.getPrecioUnitario()*(1-(dtocomdetalle.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
							}
							//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
							
//							if(dtocomdetalle.getImpuesto()==0)
							if((impuestoPorc-1)==0.0)
							{
								subtotal0=subtotal0+dtocomdetalle.getTotal();
							}else
							{
								subtotal14=subtotal14+dtocomdetalle.getTotal();
							}
						}
							RetencionRecords retencionrecords = new RetencionRecords ((RetencionDTO) it.next());
							retencionrecords.setCliente(resultDto.get(i).getTblpersonaByIdPersona().getRazonSocial());
//							retencionrecords.setCliente(resultDto.get(i).getTblpersonaByIdPersona().getNombreComercial()+" "+resultDto.get(i).getTblpersonaByIdPersona().getRazonSocial());
							retencionrecords.setRuc(resultDto.get(i).getTblpersonaByIdPersona().getCedulaRuc());
							retencionrecords.settipoDocumento(resultDto.get(i).getTipoTransaccion());
							retencionrecords.setnumeroFatura(resultDto.get(i).getTipoTransaccion(), resultDto.get(i));
							retencionrecords.setfechaRetencion(resultDto.get(i).getFecha());
							retencionrecords.setnumeroRealTransaccion(resultDto.get(i).getNumRealTransaccion());
							retencionrecords.setsubTotal0(subtotal0);
							retencionrecords.setsubTotal14(subtotal14);
							lstRetenciones.addData(retencionrecords);
					}
					
				}
		        
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		        lstRetenciones.show();
		        lstRetenciones.redraw();	
		        
			}
		
			else
			{
				SC.say("Reporte Retenciones","No hay retenciones que cumplan los parametros establecidos");
				//getService().listarImpuesto(0, 20, objbacklst);
			}
			
			
	       
		}
	};  
	
    
}
