package com.giga.factum.client.DTO;

import java.util.List;
import java.util.Set;

import com.giga.factum.server.base.Tblcorreorespaldo;

public class EstablecimientoDTO implements java.io.Serializable{
	private Integer idEmpresa;
	private String direccion;
	private String establecimiento;
	private String nick;
	private String mail;
	private char activo;
	private Set<CorreoRespaldoDTO> correorespaldo;
	
	private List<PuntoEmisionDTO> puntoemision;
	
	public EstablecimientoDTO() {
		
	}
	public EstablecimientoDTO(Integer idEmpresa, String direccion, String establecimiento, String nick,String mail, char activo) {
		this.idEmpresa = idEmpresa;
		this.direccion = direccion;
		this.establecimiento = establecimiento;
		this.nick = nick;
		this.mail = mail;
		this.activo = activo;
	}
	public EstablecimientoDTO(Integer idEmpresa, String direccion, String establecimiento, String nick,String mail, char activo
			,Set<CorreoRespaldoDTO> correoRespaldo) {
		this.idEmpresa = idEmpresa;
		this.direccion = direccion;
		this.establecimiento = establecimiento;
		this.nick = nick;
		this.mail = mail;
		this.activo = activo;
		this.correorespaldo = correoRespaldo;
	}
	

	/**
	 * @return the idEmpresa
	 */
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	/**
	 * @param idEmpresa the idEmpresa to set
	 */
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the establecimiento
	 */
	public String getEstablecimiento() {
		return establecimiento;
	}
	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	public List<PuntoEmisionDTO> getPuntoemision() {
		return puntoemision;
	}
	public void setPuntoemision(List<PuntoEmisionDTO> puntoemision) {
		this.puntoemision = puntoemision;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public char getActivo() {
		return activo;
	}
	public void setActivo(char activo) {
		this.activo = activo;
	}
	
	
}
