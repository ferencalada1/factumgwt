package com.giga.factum.client.DTO;

//import com.giga.factum.server.baseusuarios.TblpermisorolpestanaId;

public class TblpermisorolpestanaIdDTO implements java.io.Serializable{
	private int idRol;
	private int idPermisorolpestana;

	public TblpermisorolpestanaIdDTO() {
	}

	public TblpermisorolpestanaIdDTO(int idRol, int idPermisorolpestana) {
		this.idRol = idRol;
		this.idPermisorolpestana = idPermisorolpestana;
	}

	public int getIdRol() {
		return this.idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public int getIdPermisorolpestana() {
		return this.idPermisorolpestana;
	}

	public void setIdPermisorolpestana(int idPermisorolpestana) {
		this.idPermisorolpestana = idPermisorolpestana;
	}

	/*public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TblpermisorolpestanaId))
			return false;
		TblpermisorolpestanaId castOther = (TblpermisorolpestanaId) other;

		return (this.getIdRol() == castOther.getIdRol())
				&& (this.getIdPermisorolpestana() == castOther.getIdPermisorolpestana());
	}*/

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdRol();
		result = 37 * result + this.getIdPermisorolpestana();
		return result;
	}
}
