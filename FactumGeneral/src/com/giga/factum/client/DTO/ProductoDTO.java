package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.giga.factum.server.base.TblproductoMultiImpuesto;

public class ProductoDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idProducto;
	private int IDcategoria;
	private int IDunidad;
	private int IDmarca;
	private List<BodegaDTO> IDBodega;
	private List<TipoPrecioDTO> IDTipoprecio;
	private String codigoBarras;
	private String descripcion;
	private int idEmpresa;
	private double stock;
//	private double impuesto;
	private double lifo;
	private double fifo;
	private double promedio;
	private double PVP;
	private char estado;
	private double descuento;
	private double bono;
	private char tipo;
	private String imagen;
	private char jerarquia;
	private double cantidadunidad;
	private double cantidad;//cantidad del producto que se va a colocar el producto elaborado
	private double stockMinimo;
	private double stockMax;
	private List<ProductoBodegaDTO> proBod =null;
	private List<ProductoTipoPrecioDTO> proTip = null;
	private List<ProductoDTO> variedades = null;
	private Set<TblproductoMultiImpuestoDTO> tblmultiImpuesto =new HashSet(0);
	private String establecimiento;

	public ProductoDTO() {
	}
	public ProductoDTO(Integer idEmpresa, String establecimiento,Integer idProd,
			 String codigoBarras, String descripcion,
			double stock, Set<TblproductoMultiImpuestoDTO>  impuesto, double lifo, double fifo,
			double promedio, Integer tblunidad, Integer tblcategoria,
			Integer tblmarca, char estado, char tipo,String imagen,char jerarquia, double cantidadunidad,double stockMin, double stockMax) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idProducto = idProd;
		this.IDcategoria = tblcategoria;
		this.IDunidad = tblunidad;
		this.IDmarca = tblmarca;
		this.codigoBarras = codigoBarras.toUpperCase();
		this.descripcion = descripcion.toUpperCase();
		this.stock = stock;
		this.tblmultiImpuesto = impuesto;
		this.lifo = lifo;
		this.fifo = fifo;
		this.promedio = promedio;
		this.estado = estado;
		this.tipo = tipo;
		this.imagen = imagen;
		this.jerarquia=jerarquia;
		this.cantidadunidad= cantidadunidad;
		this.stockMinimo=stockMin;
		this.stockMax=stockMax;
	}
//	public ProductoDTO(Integer idProd,
//			 String codigoBarras, String descripcion,
//			double stock, double impuesto, double lifo, double fifo,
//			double promedio, Integer tblunidad, Integer tblcategoria,
//			Integer tblmarca, char estado, char tipo,String imagen,char jerarquia, double cantidadunidad) {
//		this.idProducto = idProd;
//		this.IDcategoria = tblcategoria;
//		this.IDunidad = tblunidad;
//		this.IDmarca = tblmarca;
//		this.codigoBarras = codigoBarras.toUpperCase();
//		this.descripcion = descripcion.toUpperCase();
//		this.stock = stock;
//		this.impuesto = impuesto;
//		this.lifo = lifo;
//		this.fifo = fifo;
//		this.promedio = promedio;
//		this.estado = estado;
//		this.tipo = tipo;
//		this.imagen = imagen;
//		this.jerarquia=jerarquia;
//		this.cantidadunidad= cantidadunidad;
//	}
	public ProductoDTO(Integer idEmpresa, String establecimiento,Integer idProd,
			 String codigoBarras, String descripcion,
			double stock, Set<TblproductoMultiImpuestoDTO> tblmultiImpuesto , double lifo, double fifo,
			double promedio, Integer tblunidad, Integer tblcategoria,
			Integer tblmarca, char estado, char tipo,double cantidad,double stockMin, double stockMax) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idProducto = idProd;
		this.IDcategoria = tblcategoria;
		this.IDunidad = tblunidad;
		this.IDmarca = tblmarca;
		this.codigoBarras = codigoBarras.toUpperCase();
		this.descripcion = descripcion.toUpperCase();
		this.stock = stock;
//		this.impuesto = impuesto;
		this.tblmultiImpuesto=tblmultiImpuesto;
		this.lifo = lifo;
		this.fifo = fifo;
		this.promedio = promedio;
		this.estado = estado;
		this.cantidad=cantidad;
		this.stockMinimo=stockMin;
		this.stockMax=stockMax;
	}
	
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}	
	public double getBono() {
		return descuento;
	}
	public void setBono(double bono) {
		this.bono = bono;
	}	
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}	
	public void setTbltipoprecio(List<TipoPrecioDTO> tipo) {
		this.IDTipoprecio=tipo;
	}
	public List<TipoPrecioDTO> getTbltipoprecio() {
		return this.IDTipoprecio;
	}
	public void setTblbodega(List<BodegaDTO> idBodega) {
		this.IDBodega=idBodega;
	}
	public List<BodegaDTO> getTblbodega() {
		return this.IDBodega;
	}
	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public Integer getTblcategoria() {
		return this.IDcategoria;
	}

	public void setTblcategoria(Integer tblcategoria) {
		this.IDcategoria = tblcategoria;
	}

	public Integer getTblunidad() {
		return this.IDunidad;
	}

	public void setTblunidad(Integer tblunidad) {
		this.IDunidad = tblunidad;
	}

	public Integer getTblmarca() {
		return this.IDmarca;
	}

	public void setTblmarca(Integer tblmarca) {
		this.IDmarca = tblmarca;
	}

	public String getCodigoBarras() {
		return this.codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras.toUpperCase();
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion.toUpperCase();
	}

	public double getStock() {
		return this.stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

//	public double getImpuesto() {
//		return this.impuesto;
//	}
//
//	public void setImpuesto(double impuesto) {
//		this.impuesto = impuesto;
//	}

	public double getLifo() {
		return this.lifo;
	}

	public void setLifo(double lifo) {
		this.lifo = lifo;
	}

	public double getFifo() {
		return this.fifo;
	}

	public void setFifo(double fifo) {
		this.fifo = fifo;
	}

	public double getPromedio() {
		return this.promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}
	
	public void setProBod(List<ProductoBodegaDTO> proB){
		this.proBod = proB;
	}
	
	public List<ProductoBodegaDTO> getProBod(){
		return this.proBod;
	}
	
	public void setProTip(List<ProductoTipoPrecioDTO> proT){
		this.proTip = proT;
	}
	
	public List<ProductoTipoPrecioDTO> getProTip(){
		return this.proTip;
	}
	
	public void setPVP(double pVP) {
		
		PVP = ((pVP*this.getPromedio())/100)+this.getPromedio();
		PVP=Math.rint(PVP*100)/100;
	}
	public double getPVP() {
		return PVP;
	}
	
	public char getJerarquia() {
		return jerarquia;
	}
	public void setJerarquia(char jerarquia) {
		this.jerarquia = jerarquia;
	}
	public double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}
	public double getCantidadunidad() {
		return cantidadunidad;
	}
	public void setCantidadunidad(double cantidadunidad) {
		this.cantidadunidad = cantidadunidad;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	public List<ProductoDTO> getVariedades() {
		return variedades;
	}
	public void setVariedades(List<ProductoDTO> variedades) {
		this.variedades = variedades;
	}
	public Set<TblproductoMultiImpuestoDTO> getTblmultiImpuesto() {
		return tblmultiImpuesto;
	}
	public void setTblmultiImpuesto(Set<TblproductoMultiImpuestoDTO> tblmultiImpuesto) {
		this.tblmultiImpuesto = tblmultiImpuesto;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getEstablecimiento() {
		return establecimiento;
	}
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	public double getStockMinimo() {
		return stockMinimo;
	}
	public void setStockMinimo(double stockMinimo) {
		this.stockMinimo = stockMinimo;
	}
	public double getStockMax() {
		return stockMax;
	}
	public void setStockMax(double stockMax) {
		this.stockMax = stockMax;
	}
	

}