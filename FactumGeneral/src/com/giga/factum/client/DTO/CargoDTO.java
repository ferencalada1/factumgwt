package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;

public class CargoDTO implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idCargo;
	private String descripcion;
	private EmpleadoDTO tblempleados;

	public CargoDTO() {
	}

	public CargoDTO(int idEmpresa,String descripcion) {
		this.idEmpresa=idEmpresa;
		this.descripcion = descripcion.toUpperCase();
	}

	public CargoDTO(int idEmpresa,String descripcion, EmpleadoDTO tblempleados) {
		this.idEmpresa=idEmpresa;
		this.descripcion = descripcion.toUpperCase();
		this.tblempleados = tblempleados;
	}

	public CargoDTO(int idEmpresa,int id, String descripcion) {
		this.idEmpresa=idEmpresa;
		this.idCargo=(id);
		this.descripcion=descripcion.toUpperCase();
	}

	public Integer getIdCargo() {
		return this.idCargo;
	}

	public void setIdCargo(Integer idCargo) {
		this.idCargo = idCargo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion.toUpperCase();
	}

	public EmpleadoDTO getTblempleados() {
		return this.tblempleados;
	}

	public void setTblempleados(EmpleadoDTO tblempleados) {
		this.tblempleados = tblempleados;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
