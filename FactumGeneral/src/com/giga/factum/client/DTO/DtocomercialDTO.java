package com.giga.factum.client.DTO;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;




public class DtocomercialDTO implements java.io.Serializable{

	private Integer idDtoComercial;
	private PersonaDTO tblpersonaByIdPersona;
	private PersonaDTO tblpersonaByIdVendedor;
	private PersonaDTO tblpersonaByIdFacturaPor;
	private DtoelectronicoDTO dtoelectronico;
	private int numRealTransaccion;
	private int tipoTransaccion;
	private String fecha;
	private String expiracion;
	private String autorizacion;
	private int numPagos;
	private char estado;
	private double subtotal;
	private Set<DtoComDetalleDTO> tbldtocomercialdetalles = new HashSet<DtoComDetalleDTO>(0);
	private Set tblretencionsForIdFactura = new HashSet(0);
//	private Set<Tblretencion> tblretencionsForIdFactura = new HashSet<Tblretencion>(0);
	private Set<RetencionDTO> tblretencionsForIdDtoComercial = new HashSet<RetencionDTO>(0);
	private Set<TblpagoDTO> tblpagos = new HashSet<TblpagoDTO>(0);
	private Set tblmovimientos = new HashSet(0);
	private Integer idAnticipo;
	private Integer idNotaCredito;
	private Integer idRetencion;
	private Set<DtocomercialTbltipopagoDTO> tbldtocomercialTbltipopagos = new HashSet<DtocomercialTbltipopagoDTO>(0);
	private double costo;
	private String numCompra;
	private double TotalRetencion; 
	private double TotalRetencionIVA;
	private double TotalRetencionRENTA;
	private double descuento;
	private String observacion;
	private double propina;
	private String NumeroRetencion;
	
	private Integer idEmpresa;
	private String establecimiento;
	private String puntoEmision;
	
	public DtocomercialDTO() {
	}

	public DtocomercialDTO(Integer idEmpresa, String establecimiento, String puntoEmision,
			Integer idDocumento, int numRealTransaccion,
			int tipoTransaccion, String fecha, String expiracion,
			String autorizacion, int numPagos, char estado, double subtotal, 
			double costo, String numCompra, double descuento, String observacion, double propina) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.puntoEmision=puntoEmision;
		this.idDtoComercial = idDocumento;
		this.numRealTransaccion = numRealTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.expiracion = expiracion;
		this.autorizacion = autorizacion;
		this.numPagos = numPagos;
		this.estado = estado;
		this.subtotal = subtotal;
		this.costo = costo;
		this.numCompra=numCompra;
		this.descuento = descuento;
		this.observacion = observacion;
		this.propina = propina;
	}
	
	public DtocomercialDTO(Integer idEmpresa, String establecimiento, String puntoEmision,
			PersonaDTO tblpersonaByIdPersona,
			PersonaDTO tblpersonaByIdVendedor,
			PersonaDTO tblpersonaByIdFacturaPor,
			int numRealTransaccion,
			int tipoTransaccion, String fecha, String expiracion,
			String autorizacion, int numPagos, char estado, double subtotal, 
			double costo,String numCompra,double descuento, String observacion, double propina) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.puntoEmision=puntoEmision;
		this.tblpersonaByIdPersona = tblpersonaByIdPersona;
		this.tblpersonaByIdVendedor = tblpersonaByIdVendedor;
		this.tblpersonaByIdFacturaPor = tblpersonaByIdFacturaPor;
		this.numRealTransaccion = numRealTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.expiracion = expiracion;
		this.autorizacion = autorizacion;
		this.numPagos = numPagos;
		this.estado = estado;
		this.subtotal = subtotal;
		this.costo = costo;
		this.numCompra=numCompra;
		this.descuento = descuento;
		this.observacion= observacion;
		this.propina = propina;
	}

	public DtocomercialDTO(Integer idEmpresa, String establecimiento, String puntoEmision,
			Integer idDtoComercial, PersonaDTO tblpersonaByIdPersona,
			PersonaDTO tblpersonaByIdVendedor,
			PersonaDTO tblpersonaByIdFacturaPor,
			int numRealTransaccion,
			int tipoTransaccion, String fecha, String expiracion,
			String autorizacion, int numPagos, char estado, double subtotal,
			Set<DtoComDetalleDTO> tbldtocomercialdetalles, Set tblretencions, Set<TblpagoDTO> tblpagos,
			Set tblmovimientos, Integer Anticipo, Integer NotaCredito, Integer Retencion,
			Set<DtocomercialTbltipopagoDTO> dtoPagos, double costo,String numCompra, double descuento, String observacion, double propina) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.puntoEmision=puntoEmision;
		this.idDtoComercial= idDtoComercial;
		this.tblpersonaByIdPersona = tblpersonaByIdPersona;
		this.tblpersonaByIdVendedor = tblpersonaByIdVendedor;
		this.tblpersonaByIdFacturaPor = tblpersonaByIdFacturaPor;
		this.numRealTransaccion = numRealTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.expiracion = expiracion;
		this.autorizacion = autorizacion;
		this.numPagos = numPagos;
		this.estado = estado;
		this.subtotal = subtotal;
		this.tbldtocomercialdetalles = tbldtocomercialdetalles;
		this.tblretencionsForIdFactura = tblretencions;
		this.tblpagos = tblpagos;
		this.tblmovimientos = tblmovimientos;
		this.idAnticipo = Anticipo;
		this.idNotaCredito = NotaCredito;
        this.idRetencion = Retencion;
        this.setTbldtocomercialTbltipopagos(dtoPagos);
        this.costo = costo;
        this.numCompra=numCompra;
        this.descuento = descuento;
        this.observacion= observacion;
        this.propina = propina;
	}
	
	public DtocomercialDTO(Integer idEmpresa, String establecimiento, String puntoEmision,
			Integer idDtoComercial, PersonaDTO tblpersonaByIdPersona,
			PersonaDTO tblpersonaByIdVendedor,
			PersonaDTO tblpersonaByIdFacturaPor,
			int numRealTransaccion,
			int tipoTransaccion, String fecha, String expiracion,
			String autorizacion, int numPagos, char estado, double subtotal,
			Set<DtoComDetalleDTO> tbldtocomercialdetalles, Set tblretencions, Set tblretencionesdtocomercial, Set<TblpagoDTO> tblpagos,
			Set tblmovimientos, Integer Anticipo, Integer NotaCredito, Integer Retencion,
			Set<DtocomercialTbltipopagoDTO> dtoPagos, double costo,String numCompra, double descuento, String observacion, double propina) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.puntoEmision=puntoEmision;
		this.idDtoComercial= idDtoComercial;
		this.tblpersonaByIdPersona = tblpersonaByIdPersona;
		this.tblpersonaByIdVendedor = tblpersonaByIdVendedor;
		this.tblpersonaByIdFacturaPor = tblpersonaByIdFacturaPor;
		this.numRealTransaccion = numRealTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.expiracion = expiracion;
		this.autorizacion = autorizacion;
		this.numPagos = numPagos;
		this.estado = estado;
		this.subtotal = subtotal;
		this.tbldtocomercialdetalles = tbldtocomercialdetalles;
		this.tblretencionsForIdFactura = tblretencions;
		this.tblretencionsForIdDtoComercial = tblretencionesdtocomercial;
		this.tblpagos = tblpagos;
		this.tblmovimientos = tblmovimientos;
		this.idAnticipo = Anticipo;
		this.idNotaCredito = NotaCredito;
        this.idRetencion = Retencion;
        this.setTbldtocomercialTbltipopagos(dtoPagos);
        this.costo = costo;
        this.numCompra=numCompra;
        this.descuento = descuento;
        this.observacion= observacion;
        this.propina = propina;
	}
	
	public DtocomercialDTO(Integer idEmpresa, String establecimiento, String puntoEmision,
			Integer idDtoComercial, PersonaDTO tblpersonaByIdPersona,
			PersonaDTO tblpersonaByIdVendedor,
			PersonaDTO tblpersonaByIdFacturaPor,
			int numRealTransaccion,
			int tipoTransaccion, String fecha, String expiracion,
			String autorizacion, int numPagos, char estado, double subtotal,
			Set tblmovimientos, Integer Anticipo, Integer NotaCredito, Integer Retencion,
			double costo,String numCompra, double descuento, String observacion, double propina) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.puntoEmision=puntoEmision;
		this.idDtoComercial= idDtoComercial;
		this.tblpersonaByIdPersona = tblpersonaByIdPersona;
		this.tblpersonaByIdVendedor = tblpersonaByIdVendedor;
		this.tblpersonaByIdFacturaPor = tblpersonaByIdFacturaPor;
		this.numRealTransaccion = numRealTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.expiracion = expiracion;
		this.autorizacion = autorizacion;
		this.numPagos = numPagos;
		this.estado = estado;
		this.subtotal = subtotal;
		this.tblmovimientos = tblmovimientos;
		this.idAnticipo = Anticipo;
		this.idNotaCredito = NotaCredito;
        this.idRetencion = Retencion;
        this.costo = costo;
        this.numCompra=numCompra;
        this.descuento = descuento;
        this.observacion= observacion;
        this.propina = propina;
	}

	public Integer getIdDtoComercial() {
		return this.idDtoComercial;
	}

	public void setIdDtoComercial(Integer idDtoComercial) {
		this.idDtoComercial = idDtoComercial;
	}

	public PersonaDTO getTblpersonaByIdPersona() {
		return this.tblpersonaByIdPersona;
	}

	public void setTblpersonaByIdPersona(PersonaDTO tblpersonaByIdPersona) {
		this.tblpersonaByIdPersona = tblpersonaByIdPersona;
	}

	public PersonaDTO getTblpersonaByIdVendedor() {
		return this.tblpersonaByIdVendedor;
	}

	public void setTblpersonaByIdVendedor(PersonaDTO tblpersonaByIdVendedor) {
		this.tblpersonaByIdVendedor = tblpersonaByIdVendedor;
	}

	public int getNumRealTransaccion() {
		return this.numRealTransaccion;
	}

	public void setNumRealTransaccion(int numRealTransaccion) {
		this.numRealTransaccion = numRealTransaccion;
	}

	public int getTipoTransaccion() {
		return this.tipoTransaccion;
	}

	public void setTipoTransaccion(int tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public String getFecha() {
		return this.fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getExpiracion() {
		return this.expiracion;
	}

	public void setExpiracion(String expiracion) {
		this.expiracion = expiracion;
	}

	public String getAutorizacion() {
		return this.autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public int getNumPagos() {
		return this.numPagos;
	}

	public void setNumPagos(int numPagos) {
		this.numPagos = numPagos;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	
	public double getCosto() {
		return this.costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public Set<DtoComDetalleDTO> getTbldtocomercialdetalles() {
		return this.tbldtocomercialdetalles;
	}

	public void setTbldtocomercialdetalles(Set<DtoComDetalleDTO> tbldtocomercialdetalles) {
		this.tbldtocomercialdetalles = tbldtocomercialdetalles;
	}

	public Set getTblretencionsForIdFactura() {
		return this.tblretencionsForIdFactura;
	}

	public void setTblretencionsForIdFactura(Set tblretencions) {
		this.tblretencionsForIdFactura = tblretencions;
	}

	public Set<TblpagoDTO> getTblpagos() {
		return this.tblpagos;
	}

	public void setTblpagos(Set<TblpagoDTO> tblpagos) {
		this.tblpagos = tblpagos;
	}

	public Set getTblmovimientos() {
		return this.tblmovimientos;
	}

	public void setTblmovimientos(Set tblmovimientos) {
		this.tblmovimientos = tblmovimientos;
	}
	
	public Integer getIdAnticipo() {
		return this.idAnticipo;
	}

	public void setIdAnticipo(Integer idAnticipo) {
		this.idAnticipo = idAnticipo;
	}
	
	public Integer getIdNotaCredito() {
		return this.idNotaCredito;
	}

	public void setIdNotaCredito(Integer idNotaCredito) {
		this.idNotaCredito = idNotaCredito;
	}
	
	public Integer getIdRetencion() {
		return this.idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}

	public void setTbldtocomercialTbltipopagos(
			Set<DtocomercialTbltipopagoDTO> tbldtocomercialTbltipopagos) {
		this.tbldtocomercialTbltipopagos = tbldtocomercialTbltipopagos;
	}

	public Set<DtocomercialTbltipopagoDTO> getTbldtocomercialTbltipopagos() {
		return tbldtocomercialTbltipopagos;
	}

	public void setNumCompra(String numCompra) {
		this.numCompra = numCompra;
	}

	public String getNumCompra() {
		return numCompra;
	}

	public void setTotalRetencion() {
		TotalRetencion = TotalRetencionIVA+TotalRetencionRENTA;
	}

	public double getTotalRetencion() {
		return TotalRetencion;
	}

	public void setTotalRetencionIVA(double totalRetencionIVA) {
		TotalRetencionIVA = totalRetencionIVA;
	}

	public double getTotalRetencionIVA() {
		return TotalRetencionIVA;
	}

	public void setTotalRetencionRENTA(double totalRetencionRENTA) {
		TotalRetencionRENTA = totalRetencionRENTA;
	}

	public double getTotalRetencionRENTA() {
		return TotalRetencionRENTA;
	}
	
	public double getDescuento() {
		return this.descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	

	public double getPropina() {
		return propina;
	}

	public void setPropina(double propina) {
		this.propina = propina;
	}

	public String getNumeroRetencion() {
		return NumeroRetencion;
	}

	public void setNumeroRetencion(String numeroRetencion) {
		NumeroRetencion = numeroRetencion;
	}

	public DtoelectronicoDTO getDtoelectronico() {
		return dtoelectronico;
	}

	public void setDtoelectronico(DtoelectronicoDTO dtoelectronico) {
		this.dtoelectronico = dtoelectronico;
	}

	public Set<RetencionDTO> getTblretencionsForIdDtoComercial() {
		return tblretencionsForIdDtoComercial;
	}

	public void setTblretencionsForIdDtoComercial(Set<RetencionDTO> tblretencionsForIdDtoComercial) {
		this.tblretencionsForIdDtoComercial = tblretencionsForIdDtoComercial;
	}

	/**
	 * @return the tblpersonaByIdFacturaPor
	 */
	public PersonaDTO getTblpersonaByIdFacturaPor() {
		return tblpersonaByIdFacturaPor;
	}

	/**
	 * @param tblpersonaByIdFacturaPor the tblpersonaByIdFacturaPor to set
	 */
	public void setTblpersonaByIdFacturaPor(PersonaDTO tblpersonaByIdFacturaPor) {
		this.tblpersonaByIdFacturaPor = tblpersonaByIdFacturaPor;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

	public String getPuntoEmision() {
		return puntoEmision;
	}

	public void setPuntoEmision(String puntoEmision) {
		this.puntoEmision = puntoEmision;
	}
	
	
	
}
