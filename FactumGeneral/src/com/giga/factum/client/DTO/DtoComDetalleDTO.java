package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;

public class DtoComDetalleDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idDtoComercialDetalle;
	private DtocomercialDTO tbldtocomercial;
	private BodegaDTO Bodega;
	private ProductoDTO Producto;
	private String descripcion;
	private double cantidad;
	private double precioUnitario;
	private double departamento;
//	private double impuesto;
	private double total;
	private double costoProducto; 
	private Set<DtoComDetalleMultiImpuestosDTO> tblimpuestos = new HashSet<DtoComDetalleMultiImpuestosDTO>(0);

	public DtoComDetalleDTO() {
	}

	public DtoComDetalleDTO(double departamento, 
//			double impuesto,
			double total) {
		
		this.departamento = departamento;
//		this.impuesto = impuesto;
		this.total = total;

	}	
	
	
	public DtoComDetalleDTO(DtocomercialDTO tbldtocomercial,
			BodegaDTO Bodega, ProductoDTO Producto, double cantidad,
			double precioUnitario, double departamento, /*double impuesto,*/
			double total, String descripcion) {
		this.tbldtocomercial = tbldtocomercial;
		this.Bodega = Bodega;
		this.Producto = Producto;
		this.cantidad = cantidad;
		this.precioUnitario = precioUnitario;
		this.departamento = departamento;
//		this.impuesto = impuesto;
		this.total = total;
		this.descripcion = descripcion;
	}
	
	public DtoComDetalleDTO(Integer idDetalle,
			BodegaDTO Bodega, ProductoDTO Producto, double cantidad,
			double precioUnitario, double departamento, 
//			double impuesto,
			double total, String descripcion) {
		this.idDtoComercialDetalle = idDetalle;
		this.Bodega = Bodega;
		this.Producto = Producto;
		this.cantidad = cantidad;
		this.precioUnitario = precioUnitario;
		this.departamento = departamento;
//		this.impuesto = impuesto;
		this.total = total;
		this.descripcion = descripcion;
		System.out.println("DESCRIPCION: "+descripcion);
	}

	public Integer getIdDtoComercialDetalle() {
		return this.idDtoComercialDetalle;
	}

	public void setIdDtoComercialDetalle(Integer idDtoComercialDetalle) {
		this.idDtoComercialDetalle = idDtoComercialDetalle;
	}

	public DtocomercialDTO getTbldtocomercial() {
		return this.tbldtocomercial;
	}

	public void setTbldtocomercial(DtocomercialDTO tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}

	public BodegaDTO getBodega() {
		return this.Bodega;
	}

	public void setBodega(BodegaDTO idBodega) {
		this.Bodega = idBodega;
	}

	public ProductoDTO getProducto() {
		return this.Producto;
	}

	public void setProducto(ProductoDTO Producto) {
		this.Producto = Producto;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public String getDesProducto() {
		return this.descripcion;
	}

	public void setDesProducto(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public double getPrecioUnitario() {
		return this.precioUnitario;
	}

	public void setPrecioUnitario(double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public double getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(double departamento) {
		this.departamento = departamento;
	}

//	public double getImpuesto() {
//		return this.impuesto;
//	}
//
//	public void setImpuesto(double impuesto) {
//		this.impuesto = impuesto;
//	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}




	public void setCostoProducto(double costoProducto) {
		this.costoProducto = costoProducto;
	}




	public double getCostoProducto() {
		return costoProducto;
	}

	public Set<DtoComDetalleMultiImpuestosDTO> getTblimpuestos() {
		return tblimpuestos;
	}

	public void setTblimpuestos(Set<DtoComDetalleMultiImpuestosDTO> tblimpuestos) {
		this.tblimpuestos = tblimpuestos;
	}

}
