package com.giga.factum.client.DTO;
import java.math.BigDecimal;

public class TmpAuxgeneralDTO implements java.io.Serializable {

	private String tmpcodigo;
	private String tmpcuenta;
	private BigDecimal resta;

	public TmpAuxgeneralDTO() {
	}

	public TmpAuxgeneralDTO(String tmpcodigo, String tmpcuenta, BigDecimal resta) {
		this.tmpcodigo = tmpcodigo;
		this.tmpcuenta = tmpcuenta;
		this.resta = resta;
	}

	public String getTmpcodigo() {
		return this.tmpcodigo;
	}

	public void setTmpcodigo(String tmpcodigo) {
		this.tmpcodigo = tmpcodigo;
	}

	public String getTmpcuenta() {
		return this.tmpcuenta;
	}

	public void setTmpcuenta(String tmpcuenta) {
		this.tmpcuenta = tmpcuenta;
	}

	public BigDecimal getResta() {
		return this.resta;
	}

	public void setResta(BigDecimal resta) {
		this.resta = resta;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TmpAuxgeneralDTO))
			return false;
		TmpAuxgeneralDTO castOther = (TmpAuxgeneralDTO) other;

		return ((this.getTmpcodigo() == castOther.getTmpcodigo()) || (this
				.getTmpcodigo() != null && castOther.getTmpcodigo() != null && this
				.getTmpcodigo().equals(castOther.getTmpcodigo())))
				&& ((this.getTmpcuenta() == castOther.getTmpcuenta()) || (this
						.getTmpcuenta() != null
						&& castOther.getTmpcuenta() != null && this
						.getTmpcuenta().equals(castOther.getTmpcuenta())))
				&& ((this.getResta() == castOther.getResta()) || (this
						.getResta() != null && castOther.getResta() != null && this
						.getResta().equals(castOther.getResta())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getTmpcodigo() == null ? 0 : this.getTmpcodigo().hashCode());
		result = 37 * result
				+ (getTmpcuenta() == null ? 0 : this.getTmpcuenta().hashCode());
		result = 37 * result
				+ (getResta() == null ? 0 : this.getResta().hashCode());
		return result;
	}

}
