package com.giga.factum.client.DTO;
import java.math.BigDecimal;

public class TmpComprobacionDTO implements java.io.Serializable {

	private String tmpcodigo;
	private String tmpcuenta;
	private BigDecimal tmpdebe;
	private BigDecimal tmphaber;
	private BigDecimal tmpdeudor;
	private BigDecimal tmpacreedor;

	public TmpComprobacionDTO() {
	}

	public TmpComprobacionDTO(String tmpcodigo, String tmpcuenta,
			BigDecimal tmpdebe, BigDecimal tmphaber, BigDecimal tmpdeudor,
			BigDecimal tmpacreedor) {
		this.tmpcodigo = tmpcodigo;
		this.tmpcuenta = tmpcuenta;
		this.tmpdebe = tmpdebe;
		this.tmphaber = tmphaber;
		this.tmpdeudor = tmpdeudor;
		this.tmpacreedor = tmpacreedor;
	}

	public String getTmpcodigo() {
		return this.tmpcodigo;
	}

	public void setTmpcodigo(String tmpcodigo) {
		this.tmpcodigo = tmpcodigo;
	}

	public String getTmpcuenta() {
		return this.tmpcuenta;
	}

	public void setTmpcuenta(String tmpcuenta) {
		this.tmpcuenta = tmpcuenta;
	}

	public BigDecimal getTmpdebe() {
		return this.tmpdebe;
	}

	public void setTmpdebe(java.math.BigDecimal tmpdebe) {
		this.tmpdebe = tmpdebe;
	}

	public BigDecimal getTmphaber() {
		return this.tmphaber;
	}

	public void setTmphaber(BigDecimal tmphaber) {
		this.tmphaber = tmphaber;
	}

	public BigDecimal getTmpdeudor() {
		return this.tmpdeudor;
	}

	public void setTmpdeudor(BigDecimal tmpdeudor) {
		this.tmpdeudor = tmpdeudor;
	}

	public BigDecimal getTmpacreedor() {
		return this.tmpacreedor;
	}

	public void setTmpacreedor(BigDecimal tmpacreedor) {
		this.tmpacreedor = tmpacreedor;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TmpComprobacionDTO))
			return false;
		TmpComprobacionDTO castOther = (TmpComprobacionDTO) other;

		return ((this.getTmpcodigo() == castOther.getTmpcodigo()) || (this
				.getTmpcodigo() != null && castOther.getTmpcodigo() != null && this
				.getTmpcodigo().equals(castOther.getTmpcodigo())))
				&& ((this.getTmpcuenta() == castOther.getTmpcuenta()) || (this
						.getTmpcuenta() != null
						&& castOther.getTmpcuenta() != null && this
						.getTmpcuenta().equals(castOther.getTmpcuenta())))
				&& ((this.getTmpdebe() == castOther.getTmpdebe()) || (this
						.getTmpdebe() != null && castOther.getTmpdebe() != null && this
						.getTmpdebe().equals(castOther.getTmpdebe())))
				&& ((this.getTmphaber() == castOther.getTmphaber()) || (this
						.getTmphaber() != null
						&& castOther.getTmphaber() != null && this
						.getTmphaber().equals(castOther.getTmphaber())))
				&& ((this.getTmpdeudor() == castOther.getTmpdeudor()) || (this
						.getTmpdeudor() != null
						&& castOther.getTmpdeudor() != null && this
						.getTmpdeudor().equals(castOther.getTmpdeudor())))
				&& ((this.getTmpacreedor() == castOther.getTmpacreedor()) || (this
						.getTmpacreedor() != null
						&& castOther.getTmpacreedor() != null && this
						.getTmpacreedor().equals(castOther.getTmpacreedor())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getTmpcodigo() == null ? 0 : this.getTmpcodigo().hashCode());
		result = 37 * result
				+ (getTmpcuenta() == null ? 0 : this.getTmpcuenta().hashCode());
		result = 37 * result
				+ (getTmpdebe() == null ? 0 : this.getTmpdebe().hashCode());
		result = 37 * result
				+ (getTmphaber() == null ? 0 : this.getTmphaber().hashCode());
		result = 37 * result
				+ (getTmpdeudor() == null ? 0 : this.getTmpdeudor().hashCode());
		result = 37
				* result
				+ (getTmpacreedor() == null ? 0 : this.getTmpacreedor()
						.hashCode());
		return result;
	}

}
