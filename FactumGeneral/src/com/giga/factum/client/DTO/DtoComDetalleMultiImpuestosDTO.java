package com.giga.factum.client.DTO;



public class DtoComDetalleMultiImpuestosDTO  implements java.io.Serializable{
	private Integer idtbldtocomercialdetalle_tblmulti_impuesto;
	private DtoComDetalleDTO tbldtocomercialdetalle;
	private Double porcentaje;
	private char tipo;
	private String nombre;
	
	public DtoComDetalleMultiImpuestosDTO(Double porcentaje, char tipo, String nombre) {
		super();
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}
	public DtoComDetalleMultiImpuestosDTO(Integer idtbldtocomercialdetalle_tblmulti_impuesto, Double porcentaje,
			char tipo, String nombre) {
		super();
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}
	public DtoComDetalleMultiImpuestosDTO(Integer idtbldtocomercialdetalle_tblmulti_impuesto,
			DtoComDetalleDTO tbldtocomercialdetalle, Double porcentaje, char tipo, String nombre) {
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
		this.tbldtocomercialdetalle = tbldtocomercialdetalle;
		this.porcentaje = porcentaje;
		this.tipo = tipo;
		this.nombre = nombre;
	}
	public DtoComDetalleMultiImpuestosDTO() {
	}
	public Integer getIdtbldtocomercialdetalle_tblmulti_impuesto() {
		return idtbldtocomercialdetalle_tblmulti_impuesto;
	}
	public void setIdtbldtocomercialdetalle_tblmulti_impuesto(Integer idtbldtocomercialdetalle_tblmulti_impuesto) {
		this.idtbldtocomercialdetalle_tblmulti_impuesto = idtbldtocomercialdetalle_tblmulti_impuesto;
	}
	public DtoComDetalleDTO getTbldtocomercialdetalle() {
		return tbldtocomercialdetalle;
	}
	public void setTbldtocomercialdetalle(DtoComDetalleDTO tbldtocomercialdetalle) {
		this.tbldtocomercialdetalle = tbldtocomercialdetalle;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
