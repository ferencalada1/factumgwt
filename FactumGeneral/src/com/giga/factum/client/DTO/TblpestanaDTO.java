package com.giga.factum.client.DTO;

public class TblpestanaDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idPestanas;
	private String descripcion;
	
	public Integer getIdPestanas() {
		return idPestanas;
	}
	public void setIdPestanas(Integer idPestanas) {
		this.idPestanas = idPestanas;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
