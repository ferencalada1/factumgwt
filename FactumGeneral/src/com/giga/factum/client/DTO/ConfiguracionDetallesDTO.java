package com.giga.factum.client.DTO;

public class ConfiguracionDetallesDTO implements java.io.Serializable{

	private Integer idDetallesConfiguracion;
	private Integer idConfiguracion;
	private Integer puntoX;
	private Integer puntoY;
	private Integer longitud;
	private Integer imprimir;
	private Integer imprimirlabel;
	private String extra;
		
	public ConfiguracionDetallesDTO(){
		
	}

	public ConfiguracionDetallesDTO(Integer idDetallesConfiguracion,
			Integer idConfiguracion, Integer puntoX, Integer puntoY,
			Integer longitud, Integer imprimir, Integer imprimirlabel,
			String extra) {
		super();
		this.idDetallesConfiguracion = idDetallesConfiguracion;
		this.idConfiguracion = idConfiguracion;
		this.puntoX = puntoX;
		this.puntoY = puntoY;
		this.longitud = longitud;
		this.imprimir = imprimir;
		this.imprimirlabel = imprimirlabel;
		this.extra = extra;
	}

	public Integer getIdDetallesConfiguracion() {
		return idDetallesConfiguracion;
	}

	public void setIdDetallesConfiguracion(Integer idDetallesConfiguracion) {
		this.idDetallesConfiguracion = idDetallesConfiguracion;
	}

	public Integer getIdConfiguracion() {
		return idConfiguracion;
	}

	public void setIdConfiguracion(Integer idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}

	public Integer getPuntoX() {
		return puntoX;
	}

	public void setPuntoX(Integer puntoX) {
		this.puntoX = puntoX;
	}

	public Integer getPuntoY() {
		return puntoY;
	}

	public void setPuntoY(Integer puntoY) {
		this.puntoY = puntoY;
	}

	public Integer getLongitud() {
		return longitud;
	}

	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	public Integer getImprimir() {
		return imprimir;
	}

	public void setImprimir(Integer imprimir) {
		this.imprimir = imprimir;
	}

	public Integer getImprimirlabel() {
		return imprimirlabel;
	}

	public void setImprimirlabel(Integer imprimirlabel) {
		this.imprimirlabel = imprimirlabel;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}



