package com.giga.factum.client.DTO;


public class CuentaPlanCuentaIdDTO implements java.io.Serializable{
	private int idPlan;
	private int idCuenta;

	public CuentaPlanCuentaIdDTO() {
	}

	public CuentaPlanCuentaIdDTO(int idPlan, int idCuenta) {
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
	}

	public int getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CuentaPlanCuentaIdDTO))
			return false;
		CuentaPlanCuentaIdDTO castOther = (CuentaPlanCuentaIdDTO) other;

		return (this.getIdPlan() == castOther.getIdPlan())
				&& (this.getIdCuenta() == castOther.getIdCuenta());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdPlan();
		result = 37 * result + this.getIdCuenta();
		return result;
	}

}
