package com.giga.factum.client.DTO;

public class TbltipodocumentoDTO implements java.io.Serializable{

	private Integer idTipoDoc;
	private String tipoDoc;
	private int idPlan;
	private int idCuenta;
	private int idTipo;

	public TbltipodocumentoDTO() {
	}

	public TbltipodocumentoDTO(String tipoDoc, int idPlan, int idCuenta, int idTipo) {
		this.tipoDoc = tipoDoc;
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
		this.idTipo = idTipo;
	}

	public Integer getIdTipoDoc() {
		return this.idTipoDoc;
	}

	public void setIdTipoDoc(Integer idTipoDoc) {
		this.idTipoDoc = idTipoDoc;
	}

	public String getTipoDoc() {
		return this.tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public int getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}
	
	public int getIdTipo() {
		return this.idTipo;
	}

	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}

	
	
}
