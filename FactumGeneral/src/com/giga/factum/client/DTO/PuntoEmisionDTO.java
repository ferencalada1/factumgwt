package com.giga.factum.client.DTO;

public class PuntoEmisionDTO implements java.io.Serializable{
	private int idEmpresa;
	private String establecimiento;
	private String puntoemision;
	private String descripcion;
	private String nick;
	private char activo;
	
	
	public PuntoEmisionDTO() {
	}



	public PuntoEmisionDTO(int idEmpresa, String establecimiento, String puntoemision, String descripcion,
			String correo,char activo) {
		this.idEmpresa = idEmpresa;
		this.establecimiento = establecimiento;
		this.puntoemision = puntoemision;
		this.descripcion = descripcion;
		this.nick = correo;
		this.activo = activo;
	}



	/**
	 * @return the idEmpresa
	 */
	public Integer getIdEmpresa() {
		return idEmpresa;
	}



	/**
	 * @param idEmpresa the idEmpresa to set
	 */
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}



	/**
	 * @return the establecimiento
	 */
	public String getEstablecimiento() {
		return establecimiento;
	}



	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}



	/**
	 * @return the puntoemision
	 */
	public String getPuntoemision() {
		return puntoemision;
	}



	/**
	 * @param puntoemision the puntoemision to set
	 */
	public void setPuntoemision(String puntoemision) {
		this.puntoemision = puntoemision;
	}



	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}



	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	/**
	 * @return the correo
	 */
	public String getNick() {
		return nick;
	}



	/**
	 * @param correo the correo to set
	 */
	public void setNick(String correo) {
		this.nick = correo;
	}



	/**
	 * @return the activo
	 */
	public char getActivo() {
		return activo;
	}



	/**
	 * @param activo the activo to set
	 */
	public void setActivo(char activo) {
		this.activo = activo;
	}
	
	
}
