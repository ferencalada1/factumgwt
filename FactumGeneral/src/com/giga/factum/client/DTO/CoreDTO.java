package com.giga.factum.client.DTO;

public class CoreDTO implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idCore;
	private String nombreCore;
	private int tipoCore;
	private int padreCore;
	private int nivelCore;
	private String codigoCore;
	public CoreDTO(){

	}
	
	
	public CoreDTO(Integer idCore, String nombreCore,
			int tipoCore, int padreCore, int nivelCore,
			String codigoCore) {
		super();
		this.idCore = idCore;
		this.nombreCore = nombreCore;
		this.tipoCore = tipoCore;
		this.padreCore = padreCore;
		this.nivelCore = nivelCore;
		this.codigoCore = codigoCore;
	}


	public Integer getIdCore() {
		return idCore;
	}
	public void setIdCore(Integer idCore) {
		this.idCore = idCore;
	}
	public String getNombreCore() {
		return nombreCore;
	}
	public void setNombreCore(String nombreCore) {
		this.nombreCore = nombreCore;
	}
	public int getTipoCore() {
		return tipoCore;
	}
	public void setTipoCore(int tipoCore) {
		this.tipoCore = tipoCore;
	}
	public int getPadreCore() {
		return padreCore;
	}
	public void setPadreCore(int padreCore) {
		this.padreCore = padreCore;
	}
	public int getNivelCore() {
		return nivelCore;
	}
	public void setNivelCore(int nivelCore) {
		this.nivelCore = nivelCore;
	}
	public String getCodigoCore() {
		return codigoCore;
	}
	public void setCodigoCore(String codigoCore) {
		this.codigoCore = codigoCore;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
