package com.giga.factum.client.DTO;

import com.giga.factum.client.DTO.PersonaDTO;

public class ClienteDTO implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idCliente;
	private PersonaDTO tblpersona;
	private double montoCredito;
	private double cupoCredito;
	private String codigoTarjeta;
	private Integer puntos;
	private String contribuyenteEspecial;
	private char  obligadoContabilidad;


	public ClienteDTO() {
	}
	
	public ClienteDTO(int idEmpresa,int idCliente, PersonaDTO tblpersona, double montoCredito,
			double cupoCredito, String codigoTarjeta, Integer puntos, String contribuyenteEspecial, char obligadoContabilidad) {
		
		System.out.print("InicioCliente");
		this.idEmpresa = idEmpresa;
		System.out.print(idEmpresa);
		this.tblpersona = tblpersona;
		System.out.print(tblpersona.getIdPersona());
		this.montoCredito = montoCredito;
		System.out.print(montoCredito);
		this.cupoCredito = cupoCredito;
		System.out.print(cupoCredito);
		this.codigoTarjeta = codigoTarjeta;
		System.out.print(codigoTarjeta);
		this.puntos = puntos;
		System.out.print(puntos);
		this.contribuyenteEspecial=contribuyenteEspecial;
		System.out.print(contribuyenteEspecial);
		this.obligadoContabilidad=obligadoContabilidad;
		System.out.print(obligadoContabilidad);
		System.out.print("FinCliente");
	}
	
	public ClienteDTO(int idEmpresa,PersonaDTO tblpersona, double montoCredito,
			double cupoCredito, String contribuyenteEspecial, char obligadoContabilidad) {
		this.idEmpresa=idEmpresa;
		this.tblpersona = tblpersona;
		this.montoCredito = montoCredito;
		this.cupoCredito = cupoCredito;
		this.contribuyenteEspecial=contribuyenteEspecial;
		this.obligadoContabilidad=obligadoContabilidad;
	}

	public Integer getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public PersonaDTO getTblpersona() {
		return this.tblpersona;
	}

	public void setTblpersona(PersonaDTO tblpersona) {
		this.tblpersona = tblpersona;
	}

	public double getMontoCredito() {
		return this.montoCredito;
	}

	public void setMontoCredito(double montoCredito) {
		this.montoCredito = montoCredito;
	}

	public double getCupoCredito() {
		return this.cupoCredito;
	}

	public void setCupoCredito(double cupoCredito) {
		this.cupoCredito = cupoCredito;
	}

	public void setCodigoTarjeta(String codigoTarjeta) {
		this.codigoTarjeta = codigoTarjeta;
	}

	public String getCodigoTarjeta() {
		return codigoTarjeta;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public String getContribuyenteEspecial() {
		return contribuyenteEspecial;
	}

	public void setContribuyenteEspecial(String contribuyenteEspecial) {
		this.contribuyenteEspecial = contribuyenteEspecial;
	}

	public char getObligadoContabilidad() {
		return obligadoContabilidad;
	}

	public void setObligadoContabilidad(char obligadoContabilidad) {
		this.obligadoContabilidad = obligadoContabilidad;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	


}
