package com.giga.factum.client.DTO;

public class CuentaEmpresaIdDTO implements java.io.Serializable{
	private int idEmpresa;
	private int idCuenta;

	public CuentaEmpresaIdDTO() {
	}

	public CuentaEmpresaIdDTO(int idEmpresa, int idCuenta) {
		this.idEmpresa = idEmpresa;
		this.idCuenta = idCuenta;
	}

	public int getIdEmpresa() {
		return this.idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CuentaEmpresaIdDTO))
			return false;
		CuentaEmpresaIdDTO castOther = (CuentaEmpresaIdDTO) other;

		return (this.getIdEmpresa() == castOther.getIdEmpresa())
				&& (this.getIdCuenta() == castOther.getIdCuenta());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdEmpresa();
		result = 37 * result + this.getIdCuenta();
		return result;
	}
}
