package com.giga.factum.client.DTO;

import java.util.List;

public class CategoriaDTO implements java.io.Serializable {
	private Integer idCategoria;
	private Integer idEmpresa;
	private String Categoria;
	private List<ProductoDTO> Productos;
	
	public CategoriaDTO(){
	}
	/**
	 * Constructor 
	 * @param idCategoria integer
	 * @param Categoria   String
	 */
	public CategoriaDTO(Integer idEmpresa,Integer idCategoria,String Categoria){
		this.idEmpresa=idEmpresa;
		this.Categoria = Categoria.toUpperCase();
		this.idCategoria = idCategoria;
	}
	
	public CategoriaDTO(Integer idEmpresa,String Categoria) {
		this.idEmpresa=idEmpresa;
		this.Categoria = Categoria.toUpperCase();
	}
	public Integer getIdCategoria() {
		return idCategoria;
	}
	public List<ProductoDTO> getProductos() {
		return Productos;
	}
	public void setProductos(List<ProductoDTO> productos) {
		Productos = productos;
	}
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getCategoria() {
		return Categoria;
	}
	public void setCategoria(String Categoria) {
		this.Categoria = Categoria.toUpperCase();
	}
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	

}



