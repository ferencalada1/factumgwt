package com.giga.factum.client.DTO;

import java.util.LinkedList;

import com.giga.factum.client.Factura;
import com.giga.factum.client.GreetingService;
import com.giga.factum.client.GreetingServiceAsync;
import com.giga.factum.client.subirarchivo;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CreateExelDTO implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6964573581709121842L;
	private ListGridRecord[] listado;
	private ListGrid listGrid = new ListGrid();
	LinkedList<String[]> datos =new LinkedList<String[]>();	
	LinkedList<String[]> drecibido =new LinkedList<String[]>();	
	Factura fac;
	public CreateExelDTO(){
		
	}
	public CreateExelDTO(ListGrid listGrid){
		int lista=listGrid.getFields().length;
		System.out.println("listgrid "+lista);
		String[] atributos = new String[lista];
		for(int i=0;i<=lista;i++)
		{
			if(listGrid.fieldIsVisible(listGrid.getFieldName(i).toString()))
			{
				System.out.println("listgrid "+listGrid.getFieldName(i).toString());
				atributos[i]=listGrid.getField(listGrid.getFieldName(i)).getTitle();
			}
			
		}
		CreateExelDTO exel=new CreateExelDTO(listGrid,atributos,"Export.xls");
		
	}
	public CreateExelDTO(ListGrid listGrid, String[] atributos,String nombre){
		this.listGrid=listGrid;
		listado=listGrid.getRecords();
		int i=listado.length;
		if(i>0){
			datos.add(atributos);
			String mensaje="";
			for(int j=0;j<listado.length;j++){
				String[] fila=new String[atributos.length];
				String[] atribsAux=listado[j].getAttributes();
				for(i=0;i<atributos.length;i++){
					//fila[i]=listado[j].getAttribute(atributos[i]);
					//mensaje=" "+mensaje+" "+listado[j].getAttribute(atributos[i]);
					fila[i]=listado[j].getAttribute(listGrid.getFieldName(i));
					mensaje=" "+mensaje+" "+listado[j].getAttribute(listGrid.getFieldName(i));
				}
				mensaje=" "+mensaje+"/n";
				datos.add(fila);
			}
			getService().listGridToExel(datos,nombre,objbackString);
			
		}else{
			SC.say("No existen registros");
		}
	}
	
/**/	public CreateExelDTO(LinkedList<String[]>  ld, String[] atributos,String nombre){
         if (ld.size()>0){
			getService().exportarexcel(ld,nombre,objbackString);
			
		}else{
			SC.say("No existen registros");
		}
	}

/**/	public CreateExelDTO( Factura fc , String ruta){
	    fac = fc;
	    getService().importarExcel (ruta , retornolist);
        }
	
	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			//String url = ""+"factum\\pdfServlet?fileName="+result;
			String baseURL=GWT.getModuleBaseURL();
			String url = baseURL+"pdfServlet?fileName="+result;
			com.google.gwt.user.client.Window.open( url, "", "");
			SC.say(result);
		}
	};
	final AsyncCallback <LinkedList>  retornolist =new AsyncCallback <LinkedList>(){ 
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(LinkedList result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			drecibido = result;
		    fac.cargardesdeexcel(drecibido);
			
			//SC.say(result);
		}
	
	
	};
	
	public LinkedList getdatoscarga ()
	{
	   return this.drecibido;
	}
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
