package com.giga.factum.client.DTO;

public class UnidadDTO  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idUnidad;
	private String unidad;
	
	public  UnidadDTO(){
		
	}
	public UnidadDTO(Integer id,String nombr){
		idUnidad=id;
		unidad=nombr.toUpperCase();
	}
	
	public UnidadDTO(String nombre) {
		this.unidad = nombre.toUpperCase();
	}
	public Integer getIdUnidad() {
		return idUnidad;
	}
	public void setIdUnidad(Integer idUnidad) {
		this.idUnidad = idUnidad;
	}
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String nombre) {
		this.unidad =nombre.toUpperCase() ;
	}

}
