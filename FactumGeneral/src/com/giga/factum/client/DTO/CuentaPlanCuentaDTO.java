package com.giga.factum.client.DTO;





public class CuentaPlanCuentaDTO implements java.io.Serializable{
	private CuentaPlanCuentaIdDTO id;
	private CuentaDTO cuenta;
	private PlanCuentaDTO plancuenta;
	private double saldo;
	

	public CuentaPlanCuentaDTO() {
	}

	public CuentaPlanCuentaDTO(CuentaPlanCuentaIdDTO id, CuentaDTO cuenta,
			PlanCuentaDTO plancuenta, double saldo) {
		this.id = id;
		this.cuenta = cuenta;
		this.plancuenta = plancuenta;
		this.saldo = saldo;
	}

	/*public CuentaPlanCuentaDTO(CuentaPlanCuentaIdDTO id, CuentaDTO cuenta,
			PlanCuentaDTO lancuenta, double saldo,
			Set tblmovimientoTblcuentaTblplancuentas) {
		this.id = id;
		this.tblcuenta = tblcuenta;
		this.tblplancuenta = tblplancuenta;
		this.saldo = saldo;
		this.tblmovimientoTblcuentaTblplancuentas = tblmovimientoTblcuentaTblplancuentas;
	}*/

	public CuentaPlanCuentaIdDTO getId() {
		return this.id;
	}

	public void setId(CuentaPlanCuentaIdDTO id) {
		this.id = id;
	}

	public CuentaDTO getTblcuenta() {
		return this.cuenta;
	}

	public void setCuenta(CuentaDTO cuenta) {
		this.cuenta = cuenta;
	}

	public PlanCuentaDTO getPlancuenta() {
		return this.plancuenta;
	}

	public void setPlancuenta(PlanCuentaDTO plancuenta) {
		this.plancuenta = plancuenta;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	/*public Set getTblmovimientoTblcuentaTblplancuentas() {
		return this.tblmovimientoTblcuentaTblplancuentas;
	}

	public void setTblmovimientoTblcuentaTblplancuentas(
			Set tblmovimientoTblcuentaTblplancuentas) {
		this.tblmovimientoTblcuentaTblplancuentas = tblmovimientoTblcuentaTblplancuentas;
	}*/

}
