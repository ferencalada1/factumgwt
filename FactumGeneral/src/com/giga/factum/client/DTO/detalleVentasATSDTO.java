package com.giga.factum.client.DTO;

import com.giga.factum.client.Factum;

public class detalleVentasATSDTO implements java.io.Serializable{
	private PersonaDTO persona=new PersonaDTO();
	private String tpIdCliente; //04 RUC persona Natural, 05 Persona Natural
	private int tipoComprobante;//18 Factura de Venta
	private int numeroComprobantes;
	private double baseNoGraIva=0;    
	private double baseImponible;  //subtotal 0% iva 
	private double baseImpGrav; //subtotal 12% iva
	private double montoIva;		// iva del baseImpGrav
	private double valorRetIva;
	private double valorRetRenta;
	 
	
	public detalleVentasATSDTO() {
		
	}
	
	
	public PersonaDTO getPersona() {
		return persona;
	}
	public void setPersona(PersonaDTO persona) {
		this.persona = persona;
	}
	public String getTpIdCliente() {
		return tpIdCliente;
	}
	public void setTpIdCliente(String tpIdCliente) {
		this.tpIdCliente = tpIdCliente;
	}
	public int getTipoComprobante() {
		return tipoComprobante;
	}
	public void setTipoComprobante(int tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}
	public int getNumeroComprobantes() {
		return numeroComprobantes;
	}
	public void setNumeroComprobantes(int numeroComprobantes) {
		this.numeroComprobantes = numeroComprobantes;
	}
	public double getBaseNoGraIva() {
		return baseNoGraIva;
	}
	public void setBaseNoGraIva(double baseNoGraIva) {
		
		this.baseNoGraIva =  (Math.rint(baseNoGraIva*100)/100);
	}
	public double getBaseImponible() {
		return baseImponible;
	}
	public void setBaseImponible(double baseImponible) {
		this.baseImponible =  (Math.rint(baseImponible*100)/100);;
	}
	public double getBaseImpGrav() {
		return baseImpGrav;
	}
	public void setBaseImpGrav(double baseImpGrav) {
		this.baseImpGrav =  (Math.rint(baseImpGrav*100)/100);
	}
	public double getMontoIva() {
		return montoIva;
	}
	public void setMontoIva() {
		this.montoIva = baseImpGrav*(Factum.banderaIVA/100);
		montoIva= (Math.rint(montoIva*100)/100);;
	}
	public double getValorRetIva() {
		return valorRetIva;
	}
	public void setValorRetIva(double valorRetIva) {
		this.valorRetIva = (Math.rint(valorRetIva*100)/100);;
	}
	public double getValorRetRenta() {
		return valorRetRenta;
	}
	public void setValorRetRenta(double valorRetRenta) {
		this.valorRetRenta = (Math.rint(valorRetRenta*100)/100);
	}
	
	

}
