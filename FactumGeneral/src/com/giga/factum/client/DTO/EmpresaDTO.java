package com.giga.factum.client.DTO;

import java.util.Date;
import java.util.List;

public class EmpresaDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private String nombre;
	private String RUC; 
	private String razonSocial;
	private String direccionMatriz;
	private String contribuyenteEspecial;
	private char obligadoContabilidad;
	
	private String firma;
	private String clave;
	private Date fechaexpiracion;
	
	private String mail;
	private int decimales;
	private int ambiente;
	
	private char tipo_contribuyente;
	private char agente_retencion;
	private char microempresa;
	private char exportador;
	private char sector_publico;
	//private int id_tipo_empresa;
	private String resolucion_agente;
	
	private TbltipoempresaDTO tipoempresa;
	
	private List<EstablecimientoDTO> establecimientos;
	

	public EmpresaDTO(Integer idEmpresa, String nombre, String rUC, String razonSocial, String direccionMatriz,
			String contribuyenteEspecial, char obligadoContabilidad, String firma, String clave, Date fechaexpiracion,
			String mail, int decimales, int ambiente, char tipo_contribuyente, char agente_retencion, char microempresa,
			char exportador, char sector_publico, TbltipoempresaDTO tipoempresa, String resolucion_agente,
			List<EstablecimientoDTO> establecimientos) {
		this.idEmpresa = idEmpresa;
		this.nombre = nombre;
		RUC = rUC;
		this.razonSocial = razonSocial;
		this.direccionMatriz = direccionMatriz;
		this.contribuyenteEspecial = contribuyenteEspecial;
		this.obligadoContabilidad = obligadoContabilidad;
		this.firma = firma;
		this.clave = clave;
		this.fechaexpiracion = fechaexpiracion;
		this.mail = mail;
		this.decimales = decimales;
		this.ambiente = ambiente;
		this.tipo_contribuyente = tipo_contribuyente;
		this.agente_retencion = agente_retencion;
		this.microempresa = microempresa;
		this.exportador = exportador;
		this.sector_publico = sector_publico;
		this.tipoempresa = tipoempresa;
		this.resolucion_agente = resolucion_agente;
		this.establecimientos = establecimientos;
	}

	public EmpresaDTO(Integer idEmpresa, String nombre, String rUC, String razonSocial, String direccionMatriz,
			String contribuyenteEspecial, char obligadoContabilidad, String firma, String clave, Date fechaexpiracion,
			String mail, int decimales, int ambiente, char tipo_contribuyente, char agente_retencion, char microempresa,
			char exportador, char sector_publico, TbltipoempresaDTO tipoempresa, String resolucion_agente) {
		this.idEmpresa = idEmpresa;
		this.nombre = nombre;
		RUC = rUC;
		this.razonSocial = razonSocial;
		this.direccionMatriz = direccionMatriz;
		this.contribuyenteEspecial = contribuyenteEspecial;
		this.obligadoContabilidad = obligadoContabilidad;
		this.firma = firma;
		this.clave = clave;
		this.fechaexpiracion = fechaexpiracion;
		this.mail = mail;
		this.decimales = decimales;
		this.ambiente = ambiente;
		this.tipo_contribuyente = tipo_contribuyente;
		this.agente_retencion = agente_retencion;
		this.microempresa = microempresa;
		this.exportador = exportador;
		this.sector_publico = sector_publico;
		this.tipoempresa = tipoempresa;
		this.resolucion_agente = resolucion_agente;
		this.establecimientos = establecimientos;
	}

	public EmpresaDTO(Integer idEmpresa, String nombre, String RUC, String razonSocial, String direccionMatriz,
			String contribuyenteEspecial, char obligadoContabilidad, String firma, String clave, Date fechaexpiracion) {

		this.idEmpresa = idEmpresa;
		this.nombre = nombre;
		this.RUC = RUC;
		this.razonSocial = razonSocial;
		this.direccionMatriz = direccionMatriz;
		this.contribuyenteEspecial = contribuyenteEspecial;
		this.obligadoContabilidad = obligadoContabilidad;
		this.firma = firma;
		this.clave = clave;
		this.fechaexpiracion = fechaexpiracion;
	}



	public EmpresaDTO() {
	}
	
	

	public EmpresaDTO(String nombre, String rUC, String razonSocial,
			String direccionMatriz, String contribuyenteEspecial,
			char obligadoContabilidad) {
		this.nombre = nombre;
		this.RUC = rUC;
		this.razonSocial = razonSocial;
		this.direccionMatriz = direccionMatriz;
		this.contribuyenteEspecial = contribuyenteEspecial;
		this.obligadoContabilidad = obligadoContabilidad;
	}



	public EmpresaDTO(int idEmpresa, String Empresa ) {
		System.out.println("INICIO EMPRESA");
		this.nombre = Empresa.toUpperCase();
		System.out.println("nombre: "+nombre);
		this.idEmpresa=idEmpresa;
		System.out.println("idEmpresa: "+idEmpresa);
		System.out.println("FIN EMPRESA");
	}



	public Integer getIdEmpresa() {
		return this.idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getRUC() {
		return RUC;
	}

	public void setRUC(String RUC) {
		this.RUC = RUC;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDireccionMatriz() {
		return direccionMatriz;
	}

	public void setDireccionMatriz(String direccionMatriz) {
		this.direccionMatriz = direccionMatriz;
	}

	public String getContribuyenteEspecial() {
		return contribuyenteEspecial;
	}

	public void setContribuyenteEspecial(String contribuyenteEspecial) {
		this.contribuyenteEspecial = contribuyenteEspecial;
	}

	public char getObligadoContabilidad() {
		return obligadoContabilidad;
	}

	public void setObligadoContabilidad(char obligadoContabilidad) {
		this.obligadoContabilidad = obligadoContabilidad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}



	/**
	 * @return the firma
	 */
	public String getFirma() {
		return firma;
	}



	/**
	 * @param firma the firma to set
	 */
	public void setFirma(String firma) {
		this.firma = firma;
	}



	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}



	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}



	/**
	 * @return the fechaexpiracion
	 */
	public Date getFechaexpiracion() {
		return fechaexpiracion;
	}



	/**
	 * @param fechaexpiracion the fechaexpiracion to set
	 */
	public void setFechaexpiracion(Date fechaexpiracion) {
		this.fechaexpiracion = fechaexpiracion;
	}



	public List<EstablecimientoDTO> getEstablecimientos() {
		return establecimientos;
	}



	public void setEstablecimientos(List<EstablecimientoDTO> establecimientos) {
		this.establecimientos = establecimientos;
	}



	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}



	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}



	/**
	 * @return the decimales
	 */
	public int getDecimales() {
		return decimales;
	}



	/**
	 * @param decimales the decimales to set
	 */
	public void setDecimales(int decimales) {
		this.decimales = decimales;
	}



	/**
	 * @return the ambiente
	 */
	public int getAmbiente() {
		return ambiente;
	}



	/**
	 * @param ambiente the ambiente to set
	 */
	public void setAmbiente(int ambiente) {
		this.ambiente = ambiente;
	}



	public char getTipo_contribuyente() {
		return tipo_contribuyente;
	}



	public void setTipo_contribuyente(char tipo_contribuyente) {
		this.tipo_contribuyente = tipo_contribuyente;
	}



	public char getAgente_retencion() {
		return agente_retencion;
	}



	public void setAgente_retencion(char agente_retencion) {
		this.agente_retencion = agente_retencion;
	}



	public char getMicroempresa() {
		return microempresa;
	}



	public void setMicroempresa(char microempresa) {
		this.microempresa = microempresa;
	}



	public char getExportador() {
		return exportador;
	}



	public void setExportador(char exportador) {
		this.exportador = exportador;
	}



	public char getSector_publico() {
		return sector_publico;
	}



	public void setSector_publico(char sector_publico) {
		this.sector_publico = sector_publico;
	}



	public TbltipoempresaDTO getTipoempresa() {
		return tipoempresa;
	}



	public void setTipoempresa(TbltipoempresaDTO tipoempresa) {
		this.tipoempresa = tipoempresa;
	}



	public String getResolucion_agente() {
		return resolucion_agente;
	}



	public void setResolucion_agente(String resolucion_agente) {
		this.resolucion_agente = resolucion_agente;
	}

	


}
