package com.giga.factum.client.DTO;


public class ProductoBodegaDTO implements java.io.Serializable{
	private BodegaDTO bodega;
	private ProductoDTO producto;
	private Double cantidad;

	public ProductoBodegaDTO() {
	}

	public ProductoBodegaDTO(BodegaDTO bodega,
			ProductoDTO producto, Double cantidad) {
		this.bodega = bodega;
		this.producto = producto;
		this.cantidad = cantidad;
	}
	public BodegaDTO getTblbodega() {
		return this.bodega;
	}

	public void setTblbodega(BodegaDTO bodega) {
		this.bodega = bodega;
	}

	public ProductoDTO getTblproducto() {
		return this.producto;
	}

	public void setTblproducto(ProductoDTO producto) {
		this.producto = producto;
	}

	public Double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double d) {
		this.cantidad = d;
	}
}
