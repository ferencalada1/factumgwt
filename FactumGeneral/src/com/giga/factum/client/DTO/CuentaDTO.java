package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;


public class CuentaDTO implements java.io.Serializable{
	private Integer idCuenta;
	private TipoCuentaDTO tbltipocuenta;
	private String nombreCuenta;
	private int padre;
	private int nivel;
	private Integer idEmpresa;
	//private Set tblcuentaPlancuentas = new HashSet(0);
	private String codigo;

	public CuentaDTO() {
	}

	public CuentaDTO(Integer idEmpresa,TipoCuentaDTO tbltipocuenta,int idCuenta, String nombreCuenta,
			int padre, int nivel,String codigo) {
		System.out.println("INICIO DE CuentaDTO");
		this.idEmpresa=idEmpresa;
		this.tbltipocuenta = tbltipocuenta;
		System.out.println("tbltipocuenta: "+tbltipocuenta);
		this.idCuenta=(idCuenta);
		System.out.println("idCuenta: "+idCuenta);
		this.nombreCuenta = nombreCuenta.toUpperCase();
		System.out.println("nombreCuenta: "+nombreCuenta);
		this.padre = padre;
		System.out.println("padre: "+padre);
		this.nivel = nivel;
		System.out.println("nivel: "+nivel);
		this.codigo=codigo;
		System.out.println("codigo: "+codigo);
		System.out.println("FIN DE CuentaDTO");
	}

	public CuentaDTO(Integer idEmpresa,TipoCuentaDTO tbltipocuenta, String nombreCuenta,
			int padre, int nivel,String codigo) {
		this.idEmpresa=idEmpresa;
		this.tbltipocuenta = tbltipocuenta;
		this.nombreCuenta = nombreCuenta.toUpperCase();
		this.padre = padre;
		this.nivel = nivel;
		this.codigo=codigo;
	}

	/*public CuentaDTO(TipoCuentaDTO tbltipocuenta, String nombreCuenta,
			int padre, int nivel,String codigo, Set tblcuentaPlancuentas) {
		this.tbltipocuenta = tbltipocuenta;
		this.nombreCuenta = nombreCuenta;
		this.padre = padre;
		this.nivel = nivel;
		this.tblcuentaPlancuentas = tblcuentaPlancuentas;
		this.codigo=codigo;
	}*/
	public void setCodigo(String cod){
		this.codigo=cod;
	}
	public Integer getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}

	public TipoCuentaDTO getTbltipocuenta() {
		return this.tbltipocuenta;
	}

	public void setTbltipocuenta(TipoCuentaDTO tbltipocuenta) {
		this.tbltipocuenta = tbltipocuenta;
	}

	public String getNombreCuenta() {
		return this.nombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta.toUpperCase();
	}

	public int getPadre() {
		return this.padre;
	}

	public void setPadre(int padre) {
		this.padre = padre;
	}

	public int getNivel() {
		return this.nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public void codigo(String codigo){
		this.codigo=codigo;
	}
	public String getcodigo(){
		return this.codigo;
	}
	/*public Set getTblcuentaPlancuentas() {
		return this.tblcuentaPlancuentas;
	}

	public void setTblcuentaPlancuentas(Set tblcuentaPlancuentas) {
		this.tblcuentaPlancuentas = tblcuentaPlancuentas;
	}*/

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}
