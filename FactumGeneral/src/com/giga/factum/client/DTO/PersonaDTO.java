package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;


public class PersonaDTO implements java.io.Serializable{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idPersona;
	private String cedulaRuc;
	private String nombreComercial;
	private String razonSocial;
	private String direccion;
	private String telefono1;
	private String telefono2;
	private String observaciones;
	private String localizacion="";
	private String mail;
	private char estado;
	private Set tbldtocomercialsForIdPersona = new HashSet(0);
	private Set tblproveedors = new HashSet(0);
	private Set tblempleados = new HashSet(0);
	private Set <ClienteDTO>tblclientes = new HashSet<ClienteDTO>(0);
	private ClienteDTO cliente = new ClienteDTO();
	private EmpleadoDTO empleado = new EmpleadoDTO();
	private ProveedorDTO proveedor = new ProveedorDTO();
	private Set tbldtocomercialsForIdVendedor = new HashSet(0);

	public PersonaDTO() {
	}
	
	public PersonaDTO(int idPersona, String cedulaRuc, String nombreComercial, String razonSocial,
			String direccion, String telefono1) {
		System.out.print("InicioPersona");
		this.idPersona = idPersona;
		System.out.print(idPersona);
		this.cedulaRuc = cedulaRuc;
		System.out.print(cedulaRuc);
		this.nombreComercial = nombreComercial.toUpperCase();
		System.out.print(nombreComercial);
		this.razonSocial = razonSocial.toUpperCase();
		System.out.print(razonSocial);
		this.direccion = direccion.toUpperCase();
		System.out.print(direccion);
		this.telefono1 = telefono1;
		System.out.print(telefono1);
		System.out.print("FinPersona");
	}
	
	public PersonaDTO(String cedulaRuc, String nombreComercial, String apellidos,
			String direccion, String telefono1, String telefono2, String observaciones, 
			String mail,
			char estado) {
		
		this.cedulaRuc = cedulaRuc;
		this.nombreComercial = nombreComercial.toUpperCase();
		this.razonSocial = apellidos.toUpperCase();
		this.direccion = direccion.toUpperCase();
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.observaciones = observaciones.toUpperCase();
		this.mail = mail;
		this.estado = estado;
		this.proveedor = null;
		this.cliente = null;
		this.empleado = null;
	}
	
	public PersonaDTO(String cedulaRuc, String nombreComercial, String apellidos,
			String direccion,String localizacion, String telefono1, String telefono2, String observaciones, 
			String mail,
			char estado) {
		
		this.cedulaRuc = cedulaRuc;
		this.nombreComercial = nombreComercial.toUpperCase();
		this.razonSocial = apellidos.toUpperCase();
		this.direccion = direccion.toUpperCase();
		//this.localizacion=localizacion.toUpperCase();
		this.localizacion=(localizacion==null)?"":localizacion;
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.observaciones = observaciones.toUpperCase();
		this.mail = mail;
		this.estado = estado;
		this.proveedor = null;
		this.cliente = null;
		this.empleado = null;
	}

	public PersonaDTO(String cedulaRuc, String nombreComercial, String apellidos,
			String direccion, String telefono1, String telefono2,
			String observaciones, String mail, char estado,
			Set tbldtocomercialsForIdPersona, Set tblproveedors,
			Set tblempleados, Set tblclientes, Set tbldtocomercialsForIdVendedor) {
		this.cedulaRuc = cedulaRuc;
		this.nombreComercial = nombreComercial.toUpperCase();
		this.razonSocial = apellidos.toUpperCase();
		this.direccion = direccion.toUpperCase();
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.observaciones = observaciones.toUpperCase();
		this.mail = mail;
		this.estado = estado;
		this.tbldtocomercialsForIdPersona = tbldtocomercialsForIdPersona;
		this.tblproveedors = tblproveedors;
		this.tblempleados = tblempleados;
		this.tblclientes = tblclientes;
		this.tbldtocomercialsForIdVendedor = tbldtocomercialsForIdVendedor;
	}

	public PersonaDTO(String cedulaRuc, String nombreComercial, String apellidos,
			String direccion, String telefono1, String telefono2,
			String observaciones, String mail, char estado,
			Set tbldtocomercialsForIdPersona, Set tblproveedors,
			Set tblempleados, Set tblclientes, Set tbldtocomercialsForIdVendedor, String localizacion) {
		this.cedulaRuc = cedulaRuc;
		this.nombreComercial = nombreComercial.toUpperCase();
		this.razonSocial = apellidos.toUpperCase();
		this.direccion = direccion.toUpperCase();
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.observaciones = observaciones.toUpperCase();
		//this.localizacion=localizacion;
		this.localizacion=(localizacion==null)?"":localizacion;
		this.mail = mail;
		this.estado = estado;
		this.tbldtocomercialsForIdPersona = tbldtocomercialsForIdPersona;
		this.tblproveedors = tblproveedors;
		this.tblempleados = tblempleados;
		this.tblclientes = tblclientes;
		this.tbldtocomercialsForIdVendedor = tbldtocomercialsForIdVendedor;
	}

	public Integer getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getCedulaRuc() {
		return this.cedulaRuc;
	}

	public void setCedulaRuc(String cedulaRuc) {
		this.cedulaRuc = cedulaRuc;
	}

	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial.toUpperCase();
	}

	public String getRazonSocial() {
		return this.razonSocial;
	}

	public void setRazonSocial(String apellidos) {
		this.razonSocial = apellidos.toUpperCase();
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion.toUpperCase();
	}

	public String getTelefono1() {
		return this.telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return this.telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones.toUpperCase();
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public Set getTbldtocomercialsForIdPersona() {
		return this.tbldtocomercialsForIdPersona;
	}

	public void setTbldtocomercialsForIdPersona(Set tbldtocomercialsForIdPersona) {
		this.tbldtocomercialsForIdPersona = tbldtocomercialsForIdPersona;
	}

	public Set getTblproveedors() {
		return this.tblproveedors;
	}

	public void setTblproveedors(Set tblproveedors) {
		this.tblproveedors = tblproveedors;
	}

	public Set getTblempleados() {
		return this.tblempleados;
	}

	public void setTblempleados(Set tblempleados) {
		this.tblempleados = tblempleados;
	}

	public Set<ClienteDTO> getTblclientes() {
		return this.tblclientes;
	}
	
	public void setTblclientes(Set<ClienteDTO> tblclientes) {
		this.tblclientes = tblclientes;
	}

	public Set getTbldtocomercialsForIdVendedor() {
		return this.tbldtocomercialsForIdVendedor;
	}

	public void setTbldtocomercialsForIdVendedor(
			Set tbldtocomercialsForIdVendedor) {
		this.tbldtocomercialsForIdVendedor = tbldtocomercialsForIdVendedor;
	}

	public void setProveedorU(ProveedorDTO pro){
		this.proveedor = pro;
	}
	
	public ProveedorDTO getProveedorU(){
		return this.proveedor;
	}
	
	public void setEmpleadoU(EmpleadoDTO emp){
		this.empleado = emp;
	}
	
	public EmpleadoDTO getEmpleadoU(){
		return this.empleado;
	}
	
	public void setClienteU(ClienteDTO cli){
		this.cliente = cli;
	}
	
	public ClienteDTO getClienteU(){
		return this.cliente;
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	

}
