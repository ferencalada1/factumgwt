package com.giga.factum.client.DTO;



public class TipoPrecioDTO implements java.io.Serializable, Comparable<TipoPrecioDTO>{
	
	
	private Integer idTipoPrecio;
	private String tipoPrecio;
	private int idEmpresa;
	private int orden;
	
	public TipoPrecioDTO() {
	}

//	public TipoPrecioDTO(Integer idEmpresa,String tipoPrecio) {
//		this.idEmpresa=idEmpresa;
//		this.tipoPrecio = tipoPrecio.toUpperCase();
//	}
	
//	public TipoPrecioDTO(Integer idEmpresa,int id, String nombre) {
//		this.idEmpresa=idEmpresa;
//		this.idTipoPrecio=id;
//		this.tipoPrecio=nombre.toUpperCase();
//	}
	public TipoPrecioDTO(Integer idEmpresa,String tipoPrecio, Integer orden) {
		this.idEmpresa=idEmpresa;
		this.tipoPrecio = tipoPrecio.toUpperCase();
		this.orden = orden.intValue();
	}
//	
	public TipoPrecioDTO(Integer idEmpresa,int id, String nombre, Integer orden) {
		this.idEmpresa=idEmpresa;
		this.idTipoPrecio=id;
		this.tipoPrecio=nombre.toUpperCase();
		this.orden = orden.intValue();
	}

	public Integer getIdTipoPrecio() {
		return this.idTipoPrecio;
	}

	public void setIdTipoPrecio(Integer idTipoPrecio) {
		this.idTipoPrecio = idTipoPrecio;
	}

	public String getTipoPrecio() {
		return this.tipoPrecio;
	}

	public void setTipoPrecio(String tipoPrecio) {
		this.tipoPrecio = tipoPrecio.toUpperCase();
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	@Override
	public int compareTo(TipoPrecioDTO o) {
		// TODO Auto-generated method stub
		if (getOrden()!=o.getOrden()){
			return getOrden()-o.getOrden();
		}
		return getIdTipoPrecio()-o.getIdTipoPrecio();
	}

	
	
}
