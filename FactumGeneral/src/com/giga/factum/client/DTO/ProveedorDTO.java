package com.giga.factum.client.DTO;

import java.util.Date;


public class ProveedorDTO implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idProveedor;
	private PersonaDTO tblpersona;
	private String contacto;
	private double cupoCredito;
	private String autorizacionF;
	private String serieF;
	private String fechaCaducidadF;
	private int inicioF;
	private int finF;
	private String autorizacionR;
	private String serieR;
	private String fechaCaducidadR;
	private int inicioR;
	private int finR;
	private String autorizacionN;
	private String serieN;
	private String fechaCaducidadN;
	private int inicioN;
	private int finN;
	
	public ProveedorDTO(){
		
	}
	
	public ProveedorDTO(int idEmpresa,PersonaDTO tblpersona, String contacto,
			double cupoCredito) {
		this.idEmpresa=idEmpresa;
		this.tblpersona = tblpersona;
		this.contacto = contacto;
		this.cupoCredito = cupoCredito;
		
		
	}
	
	
	public ProveedorDTO(int idEmpresa,PersonaDTO tblpersona, String contacto,
			double cupoCredito, String autorizacionF,String serieF,String fechaCaducidadF,int inicioF,int finF, String autorizacionR, String serieR, String fechaR, int inicioR, int finR, String autorizacionN, String serieN, String fechaN, int inicioN, int finN) {
		this.idEmpresa=idEmpresa;
		this.tblpersona = tblpersona;
		this.contacto = contacto;
		this.cupoCredito = cupoCredito;
		
		this.autorizacionF=autorizacionF;
		this.serieF=serieF;
		this.fechaCaducidadF=fechaCaducidadF;
		if(this.fechaCaducidadF.length()>11){
			this.fechaCaducidadF=this.fechaCaducidadF.substring(0,9);
		}
		this.inicioF=inicioF;
		this.finF=finF;
		
		this.autorizacionN=autorizacionN;
		this.serieN=serieN;
		this.fechaCaducidadN=fechaN;
		if(this.fechaCaducidadN.length()>11){
			this.fechaCaducidadN=this.fechaCaducidadN.substring(0,9);
		}
		this.inicioN=inicioN;
		this.finN=finN;
		
		this.autorizacionR=autorizacionR;
		this.serieR=serieR;	
		this.fechaCaducidadR=fechaR;
		if(this.fechaCaducidadR.length()>11){
			this.fechaCaducidadR=this.fechaCaducidadR.substring(0,9);
		}
		this.inicioR=inicioR;
		this.finR=finR;
		
	}
	public ProveedorDTO(int idEmpresa,int idProveedor,PersonaDTO tblpersona, String contacto,
			double cupoCredito, String autorizacionF,String serieF,String fechaCaducidadF,int inicioF,int finF, String autorizacionR, String serieR, String fechaR, int inicioR, int finR, String autorizacionN, String serieN, String fechaN, int inicioN, int finN) {
		
		this.idEmpresa=idEmpresa;
		this.idProveedor=idProveedor;
		this.tblpersona = tblpersona;
		this.contacto = contacto;
		this.cupoCredito = cupoCredito;
		
		this.autorizacionF=autorizacionF;
		this.serieF=serieF;
		
		this.fechaCaducidadF=fechaCaducidadF;
		this.inicioF=inicioF;
		this.finF=finF;
		
		this.autorizacionN=autorizacionN;
		this.serieN=serieN;
		this.fechaCaducidadN=fechaN;
		this.inicioN=inicioN;
		this.finN=finN;
		
		this.autorizacionR=autorizacionR;
		this.serieR=serieR;
		this.fechaCaducidadR=fechaR;
		this.inicioR=inicioR;
		this.finR=finR;
	}

	public Integer getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public PersonaDTO getTblpersona() {
		return this.tblpersona;
	}

	public void setTblpersona(PersonaDTO tblpersona) {
		this.tblpersona = tblpersona;
	}

	public String getContacto() {
		return this.contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public double getCupoCredito() {
		return this.cupoCredito;
	}

	public void setCupoCredito(double cupoCredito) {
		this.cupoCredito = cupoCredito;
	}

	public void setAutorizacionF(String autorizacionF) {
		this.autorizacionF = autorizacionF;
	}

	public String getAutorizacionF() {
		return autorizacionF;
	}

	public void setSerieF(String serieF) {
		this.serieF = serieF;
	}

	public String getSerieF() {
		return serieF;
	}

	public void setFechaCaducidadF(String fechaCaducidadF) {
		this.fechaCaducidadF = fechaCaducidadF;
	}

	public String getFechaCaducidadF() {
		return fechaCaducidadF;
	}

	public void setInicioF(int inicioF) {
		this.inicioF = inicioF;
	}

	public int getInicioF() {
		return inicioF;
	}

	public void setFinF(int finF) {
		this.finF = finF;
	}

	public int getFinF() {
		return finF;
	}

	public void setAutorizacionR(String autorizacionR) {
		this.autorizacionR = autorizacionR;
	}

	public String getAutorizacionR() {
		return autorizacionR;
	}

	public void setSerieR(String serieR) {
		this.serieR = serieR;
	}

	public String getSerieR() {
		return serieR;
	}

	public void setFechaCaducidadR(String fechaCaducidadR) {
		this.fechaCaducidadR = fechaCaducidadR;
	}

	public String getFechaCaducidadR() {
		return fechaCaducidadR;
	}

	public void setInicioR(int inicioR) {
		this.inicioR = inicioR;
	}

	public int getInicioR() {
		return inicioR;
	}

	public void setFinR(int finR) {
		this.finR = finR;
	}

	public int getFinR() {
		return finR;
	}

	public void setAutorizacionN(String autorizacionN) {
		this.autorizacionN = autorizacionN;
	}

	public String getAutorizacionN() {
		return autorizacionN;
	}

	public void setSerieN(String serieN) {
		this.serieN = serieN;
	}

	public String getSerieN() {
		return serieN;
	}

	public void setFechaCaducidadN(String fechaCaducidadN) {
		this.fechaCaducidadN = fechaCaducidadN;
	}

	public String getFechaCaducidadN() {
		return fechaCaducidadN;
	}

	public void setInicioN(int inicioN) {
		this.inicioN = inicioN;
	}

	public int getInicioN() {
		return inicioN;
	}

	public void setFinN(int finN) {
		this.finN = finN;
	}

	public int getFinN() {
		return finN;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}
