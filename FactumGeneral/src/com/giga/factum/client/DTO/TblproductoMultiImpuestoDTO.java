package com.giga.factum.client.DTO;

public class TblproductoMultiImpuestoDTO implements java.io.Serializable{

	private Integer idtblproducto_tblmulti_impuesto;
	private ProductoDTO tblproducto;
	private TblmultiImpuestoDTO tblmultiImpuesto;
	
	
	public TblproductoMultiImpuestoDTO() {
	}
	public TblproductoMultiImpuestoDTO(Integer idtblproducto_tblmulti_impuesto, ProductoDTO tblproducto,
			TblmultiImpuestoDTO tblmultiImpuesto) {
		this.idtblproducto_tblmulti_impuesto = idtblproducto_tblmulti_impuesto;
		this.tblproducto = tblproducto;
		this.tblmultiImpuesto = tblmultiImpuesto;
	}
	public Integer getIdtblproducto_tblmulti_impuesto() {
		return idtblproducto_tblmulti_impuesto;
	}
	public void setIdtblproducto_tblmulti_impuesto(Integer idtblproducto_tblmulti_impuesto) {
		this.idtblproducto_tblmulti_impuesto = idtblproducto_tblmulti_impuesto;
	}
	public ProductoDTO getTblproducto() {
		return tblproducto;
	}
	public void setTblproducto(ProductoDTO tblproducto) {
		this.tblproducto = tblproducto;
	}
	public TblmultiImpuestoDTO getTblmultiImpuesto() {
		return tblmultiImpuesto;
	}
	public void setTblmultiImpuesto(TblmultiImpuestoDTO tblmultiImpuesto) {
		this.tblmultiImpuesto = tblmultiImpuesto;
	}
	
	
}
