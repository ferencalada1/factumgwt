package com.giga.factum.client.DTO;
/**
 * Clase MarcaDTO
 * @author Israel Pes�ntez
 *
 */
public class MarcaDTO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idMarca;
	private String marca;
	
	public MarcaDTO(){
	}
	/**
	 * Constructor 
	 * @param idMarca integer
	 * @param marca   String
	 */
	public MarcaDTO(Integer idEmpresa,Integer idMarca,String marca){
		this.idEmpresa=idEmpresa;
		this.marca = marca.toUpperCase();
		this.idMarca = idMarca;
	}
	
	public MarcaDTO(Integer idEmpresa,String marca) {
		this.idEmpresa=idEmpresa;
		this.marca = marca.toUpperCase();
	}
	public Integer getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca.toUpperCase();
	}
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}
