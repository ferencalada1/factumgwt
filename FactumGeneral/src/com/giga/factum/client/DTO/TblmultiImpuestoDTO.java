package com.giga.factum.client.DTO;

public class TblmultiImpuestoDTO implements java.io.Serializable{
	private Integer idmultiImpuesto;
	private int porcentaje;
	private String descripcion;
	private char tipo;
	
	public TblmultiImpuestoDTO(Integer idmultiImpuesto, int porcentaje, String descripcion, char tipo) {
		this.idmultiImpuesto = idmultiImpuesto;
		this.porcentaje = porcentaje;
		this.descripcion = descripcion;
		this.tipo=tipo;
	}
	
	
	
	public TblmultiImpuestoDTO() {
	}



	public Integer getIdmultiImpuesto() {
		return idmultiImpuesto;
	}
	public void setIdmultiImpuesto(Integer idmultiImpuesto) {
		this.idmultiImpuesto = idmultiImpuesto;
	}
	public int getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public char getTipo() {
		return tipo;
	}



	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	
}
