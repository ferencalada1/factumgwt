package com.giga.factum.client.DTO;

public class ConfiguracionDTO implements java.io.Serializable{
	private Integer idConfiguracion;
	private String impresora;
	private Integer maxitems;
	private Integer espacioSuperior;
	private Integer espacioInferior;
	private Integer documento;
	
	
	
	public ConfiguracionDTO(){
		
	}

	public ConfiguracionDTO(Integer id, String impresora, Integer max, Integer superior, Integer inferior, Integer documento){
		this.idConfiguracion = id;
		this.impresora=impresora;
		this.maxitems=max;
		this.espacioSuperior=superior;
		this.espacioInferior=inferior;
		this.documento=documento;
	}

	public Integer getIdConfiguracion() {
		return idConfiguracion;
	}



	public void setIdConfiguracion(Integer idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}



	public String getImpresora() {
		return impresora;
	}



	public void setImpresora(String impresora) {
		this.impresora = impresora;
	}



	public Integer getMaxitems() {
		return maxitems;
	}



	public void setMaxitems(Integer maxitems) {
		this.maxitems = maxitems;
	}



	public Integer getEspacioSuperior() {
		return espacioSuperior;
	}



	public void setEspacioSuperior(Integer espacioSuperior) {
		this.espacioSuperior = espacioSuperior;
	}



	public Integer getEspacioInferior() {
		return espacioInferior;
	}



	public void setEspacioInferior(Integer espacioInferior) {
		this.espacioInferior = espacioInferior;
	}



	public Integer getDocumento() {
		return documento;
	}



	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

}



