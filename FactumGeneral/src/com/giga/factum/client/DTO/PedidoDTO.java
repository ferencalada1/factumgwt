package com.giga.factum.client.DTO;
// Generated 06-dic-2016 11:33:42 by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Tblpedido generated by hbm2java
 */
public class PedidoDTO implements java.io.Serializable {

	private Integer idPedido;
	private MesaDTO tblmesa;
	private EmpleadoDTO empleadoDTO;
	private Date fecha;
	private Set<PedidoProductoelaboradoDTO> tblpedidoTblproductoelaborados = new HashSet<PedidoProductoelaboradoDTO>(0);

	public PedidoDTO() {
	}

	public PedidoDTO(MesaDTO tblmesa, EmpleadoDTO empleadoDTO, Date fecha) {
		this.tblmesa = tblmesa;
		System.out.println("Mesa: "+tblmesa.getIdMesa());
		this.empleadoDTO = empleadoDTO;
		System.out.println("Empleado: "+empleadoDTO.getIdEmpleado());
		this.fecha = fecha;
		System.out.println("Fecha: "+fecha);
	}

	public PedidoDTO(MesaDTO tblmesa, EmpleadoDTO empleadoDTO, Date fecha, Set tblpedidoTblproductoelaborados) {
		this.tblmesa = tblmesa;
		this.empleadoDTO = empleadoDTO;
		this.fecha = fecha;
		this.tblpedidoTblproductoelaborados = tblpedidoTblproductoelaborados;
	}

	public Integer getIdPedido() {
		return this.idPedido;
	}

	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}

	public MesaDTO getTblmesa() {
		return this.tblmesa;
	}

	public void setTblmesa(MesaDTO tblmesa) {
		this.tblmesa = tblmesa;
	}

	public EmpleadoDTO getTblempleado() {
		return this.empleadoDTO;
	}

	public void setTblempleado(EmpleadoDTO empleadoDTO) {
		this.empleadoDTO = empleadoDTO;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Set<PedidoProductoelaboradoDTO> getTblpedidoTblproductoelaborados() {
		return this.tblpedidoTblproductoelaborados;
	}

	public void setTblpedidoTblproductoelaborados(Set<PedidoProductoelaboradoDTO> tblpedidoTblproductoelaborados) {
		this.tblpedidoTblproductoelaborados = tblpedidoTblproductoelaborados;
	}

}
