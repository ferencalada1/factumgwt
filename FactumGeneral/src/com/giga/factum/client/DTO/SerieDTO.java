package com.giga.factum.client.DTO;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class SerieDTO implements java.io.Serializable {
	
	private Integer idSerie;
	private DtocomercialDTO idDtoComercialC;
	private DtocomercialDTO idDtoComercialV;
	private ProductoDTO tblproductos;
	private String serie;
	private Date fechaExpiracion;
	
	public SerieDTO(){
		
	}
	
	public void setIdSerie(Integer idSerie) {
		this.idSerie = idSerie;
	}
	public Integer getIdSerie() {
		return idSerie;
	}
	
	public void setIdDtoComercialV(DtocomercialDTO idDtoComercialV) {
		this.idDtoComercialV = idDtoComercialV;
	}
	public DtocomercialDTO getIdDtoComercialV() {
		return idDtoComercialV;
	}
	
	public void setIdDtoComercialC(DtocomercialDTO idDtoComercialC) {
		this.idDtoComercialC = idDtoComercialC;
	}


	public DtocomercialDTO getIdDtoComercialC() {
		return idDtoComercialC;
	}

	public ProductoDTO getTblproductos() {
		return this.tblproductos;
	}

	public void setTblproductos(ProductoDTO tblproductos) {
		this.tblproductos = tblproductos;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getSerie() {
		return serie;
	}

	public Date getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}
}
