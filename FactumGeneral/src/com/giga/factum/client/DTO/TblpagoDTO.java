package com.giga.factum.client.DTO;

import java.util.Date;


public class TblpagoDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private String establecimiento;
	private Integer idPago;
	private DtocomercialDTO tbldtocomercial;
	private Date fechaVencimiento;
	private Date fechaRealPago;
	private double valor;
	private double valorPagado;
	private char estado;
	private int numEgreso;
	private String Concepto;
	private String FormaPago;
	
	public TblpagoDTO() {
	}

	public TblpagoDTO(Integer idEmpresa,String establecimiento,DtocomercialDTO tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
	}
	public TblpagoDTO(Integer idEmpresa,String establecimiento,DtocomercialDTO tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado,String formapago) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
		this.FormaPago=formapago;
	}

	public TblpagoDTO(Integer idEmpresa,String establecimiento,Integer idpago,DtocomercialDTO tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idPago=idpago;
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
	}
	
	public TblpagoDTO(Integer idEmpresa,String establecimiento,Integer idpago,DtocomercialDTO tbldtocomercial, Date fechaVencimiento,
			Date fechaRealPago, double valor, char estado,String concepto,String formapago) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idPago=idpago;
		this.tbldtocomercial = tbldtocomercial;
		this.fechaVencimiento = fechaVencimiento;
		this.fechaRealPago = fechaRealPago;
		this.valor = valor;
		this.estado = estado;
		this.Concepto=concepto;
		this.FormaPago=formapago;
	}
	
	public Integer getIdPago() {
		return this.idPago;
	}

	public void setIdPago(Integer idPago) {
		this.idPago = idPago;
	}

	public DtocomercialDTO getTbldtocomercial() {
		return this.tbldtocomercial;
	}

	public void setTbldtocomercial(DtocomercialDTO tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaRealPago() {
		return this.fechaRealPago;
	}

	public void setFechaRealPago(Date fechaRealPago) {
		this.fechaRealPago = fechaRealPago;
	}

	public double getValor() {
		return this.valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public void setValorPagado(double valorPagado) {
		this.valorPagado = valorPagado;
	}

	public double getValorPagado() {
		return valorPagado;
	}

	public void setNumEgreso(int numEgreso) {
		this.numEgreso = numEgreso;
	}

	public int getNumEgreso() {
		return numEgreso;
	}

	public void setConcepto(String concepto) {
		Concepto = concepto;
	}

	public String getConcepto() {
		return Concepto;
	}

	public void setFormaPago(String formaPago) {
		FormaPago = formaPago;
	}

	public String getFormaPago() {
		return FormaPago;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
}
