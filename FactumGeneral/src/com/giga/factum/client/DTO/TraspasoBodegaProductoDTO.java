package com.giga.factum.client.DTO;

public class TraspasoBodegaProductoDTO implements java.io.Serializable{
	
	
	private ProductoBodegaDTO productoBodega;
	private int idBodega;
	
	public TraspasoBodegaProductoDTO(){
		
	}
	public void setProductoBodega(ProductoBodegaDTO productoBodega) {
		this.productoBodega = productoBodega;
	}
	public ProductoBodegaDTO getProductoBodega() {
		return productoBodega;
	}

	public void setIdBodega(int idBodega) {
		this.idBodega = idBodega;
	}

	public int getIdBodega() {
		return idBodega;
	}
	
	

}
