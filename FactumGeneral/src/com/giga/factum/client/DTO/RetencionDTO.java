package com.giga.factum.client.DTO;

/*CLASE RETENCION DTO DEMO
 * 
 * 
 */
public class RetencionDTO implements java.io.Serializable{
	
	private Integer idRetencion;
	private DtocomercialDTO tbldtocomercialByIdFactura;
//	private Tbldtocomercial tbldtocomercialByIdFactura;
//	private Tbldtocomercial tbldtocomercialByIdDtoComercial;
	private ImpuestoDTO  impuesto;
	private double baseImponible;
	private String numRealRetencion;
	private String autorizacionSri;
	private char estado;
	private double valorRetenido;
	private String numero;
	private DtocomercialDTO tbldtocomercialByIdDtoComercial;

	public RetencionDTO() {
	}

	public RetencionDTO(DtocomercialDTO dtocomercial,
			ImpuestoDTO impuesto, double baseImponible,
			String numRealRetencion, String autorizacionSri, 
			char estado, String numero) {
		this.setTbldtocomercialByIdFactura(dtocomercial);
		this.impuesto = impuesto;
		this.baseImponible = baseImponible;
		this.numRealRetencion = numRealRetencion;
		this.autorizacionSri = autorizacionSri;
		this.estado = estado;
		this.numero = numero;
	}

	public RetencionDTO(Integer idRetencion, DtocomercialDTO dtocomercial,
			ImpuestoDTO impuesto, double baseImponible,
			String numRealRetencion, String autorizacionSri, 
			char estado, String numero) {
		this.idRetencion=idRetencion;
		this.setTbldtocomercialByIdFactura(dtocomercial);
		this.impuesto = impuesto;
		this.baseImponible = baseImponible;
		this.numRealRetencion = numRealRetencion;
		this.autorizacionSri = autorizacionSri;
		this.estado = estado;
		this.numero = numero;
	}
	
	public RetencionDTO(Integer idRetencion, 
			double baseImponible,
			String numRealRetencion, String autorizacionSri, 
			char estado, double valorRetenido,String numero) {
		this.idRetencion=idRetencion;
		this.baseImponible = baseImponible;
		this.numRealRetencion = numRealRetencion;
		this.autorizacionSri = autorizacionSri;
		this.estado = estado;
		this.numero = numero;
		this.valorRetenido = valorRetenido;

	}
	
	public RetencionDTO(Integer idRetencion, DtocomercialDTO dtocomercial,
			ImpuestoDTO impuesto, double baseImponible,
			String numRealRetencion, String autorizacionSri, 
			char estado, Double valorRetenido, String numero) {
		this.idRetencion=idRetencion;
		this.setTbldtocomercialByIdFactura(dtocomercial);
		this.impuesto = impuesto;
		this.baseImponible = baseImponible;
		this.numRealRetencion = numRealRetencion;
		this.autorizacionSri = autorizacionSri;
		this.estado = estado;
		this.valorRetenido = valorRetenido;
		this.numero = numero;
	}

	public RetencionDTO(Integer idRetencion, DtocomercialDTO factura,DtocomercialDTO documento,
			ImpuestoDTO impuesto, double baseImponible,
			String numRealRetencion, String autorizacionSri, 
			char estado, Double valorRetenido, String numero) {
		this.idRetencion=idRetencion;
		this.setTbldtocomercialByIdFactura(factura);
		this.setTbldtocomercialByIdDtoComercial(documento);
		this.impuesto = impuesto;
		this.baseImponible = baseImponible;
		this.numRealRetencion = numRealRetencion;
		this.autorizacionSri = autorizacionSri;
		this.estado = estado;
		this.valorRetenido = valorRetenido;
		this.numero = numero;
	}
	public Integer getIdRetencion() {
		return this.idRetencion;
	}

	public void setIdRetencion(Integer idRetencion) {
		this.idRetencion = idRetencion;
	}

//	public DtocomercialDTO getdtocomercial() {
//		return this.tbldtocomercialByIdFactura;
//	}
//
//	public void setdtocomercial(DtocomercialDTO dtocomercial) {
//		this.tbldtocomercialByIdFactura = dtocomercial;
//	}

	public ImpuestoDTO getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(ImpuestoDTO impuesto) {
		this.impuesto = impuesto;
	}

	public double getBaseImponible() {
		return this.baseImponible;
	}

	public void setBaseImponible(double baseImponible) {
		this.baseImponible = baseImponible;
	}

	public String getNumRealRetencion() {
		return this.numRealRetencion;
	}

	public void setNumRealRetencion(String numRealRetencion) {
		this.numRealRetencion = numRealRetencion;
	}

	public String getAutorizacionSri() {
		return this.autorizacionSri;
	}

	public void setAutorizacionSri(String autorizacionSri) {
		this.autorizacionSri = autorizacionSri;
	}

	public char getEstado() {
		return this.estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public void setValorRetenido(double ValorRetenido) {
		this.valorRetenido=ValorRetenido;
	}
	public double getValorRetenido() {
		return this.valorRetenido;
	}
	
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setTbldtocomercialByIdDtoComercial(DtocomercialDTO idFactura) {
		this.tbldtocomercialByIdDtoComercial = idFactura;
	}

	public DtocomercialDTO getTbldtocomercialByIdDtoComercial() {
		return tbldtocomercialByIdDtoComercial;
	}

	public DtocomercialDTO getTbldtocomercialByIdFactura() {
		return tbldtocomercialByIdFactura;
	}

	public void setTbldtocomercialByIdFactura(DtocomercialDTO tbldtocomercialByIdFactura) {
		this.tbldtocomercialByIdFactura = tbldtocomercialByIdFactura;
	}


}
