package com.giga.factum.client.DTO;


public class ProductoTipoPrecioDTO implements java.io.Serializable, Comparable<ProductoTipoPrecioDTO>{
	private TipoPrecioDTO tbltipoprecio;
	private ProductoDTO tblproducto;
	private double porcentaje;

	public ProductoTipoPrecioDTO() {
	}

	public TipoPrecioDTO getTbltipoprecio() {
		return this.tbltipoprecio;
	}

	public void setTbltipoprecio(TipoPrecioDTO tbltipoprecio) {
		this.tbltipoprecio = tbltipoprecio;
	}

	public ProductoDTO getTblproducto() {
		return this.tblproducto;
	}

	public void setTblproducto(ProductoDTO tblproducto) {
		this.tblproducto = tblproducto;
	}

	public double getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Override
	public int compareTo(ProductoTipoPrecioDTO o) {
		// TODO Auto-generated method stub
		if (getTbltipoprecio().getOrden()!=o.getTbltipoprecio().getOrden()){
			return getTbltipoprecio().getOrden()-o.getTbltipoprecio().getOrden();
		}
		return Double.compare(getPorcentaje(),o.getPorcentaje());
	}

}
