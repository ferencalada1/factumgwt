package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;

public class ImpuestoDTO implements java.io.Serializable{
	private Integer idImpuesto;
	private String nombre;
	private int codigo;
	private String codigoElectronico;
	private double retencion;
	private int idPlan;
	private int idCuenta;
	private Set tblretencions = new HashSet(0);
	private int tipo;
	private CuentaDTO cuenta;

	public ImpuestoDTO() {
	}

	public ImpuestoDTO(String nombre, int codigo, double retencion, int idPlan,
			int idCuenta, String codigoElectronico) {
		this.nombre = nombre.toUpperCase();
		this.codigo = codigo;
		this.retencion = retencion;
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
		this.codigoElectronico=codigoElectronico;
	}

	/*public Tblimpuesto(String nombre, int codigo, double retencion, int idPlan,
			int idCuenta, Set tblretencions) {
		this.nombre = nombre;
		this.codigo = codigo;
		this.retencion = retencion;
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
		this.tblretencions = tblretencions;
	}*/

	public Integer getIdImpuesto() {
		return this.idImpuesto;
	}

	public void setIdImpuesto(Integer idImpuesto) {
		this.idImpuesto = idImpuesto;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getRetencion() {
		return this.retencion;
	}

	public void setRetencion(double retencion) {
		this.retencion = retencion;
	}

	public int getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Set getTblretencions() {
		return this.tblretencions;
	}

	public void setTblretencions(Set tblretencions) {
		this.tblretencions = tblretencions;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getTipo() {
		return tipo;
	}

	public CuentaDTO getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaDTO cuenta) {
		this.cuenta = cuenta;
	}

	public String getCodigoElectronico() {
		return codigoElectronico;
	}

	public void setCodigoElectronico(String codigoElectronico) {
		this.codigoElectronico = codigoElectronico;
	}

	
	
}