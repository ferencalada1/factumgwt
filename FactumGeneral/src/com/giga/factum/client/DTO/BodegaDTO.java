package com.giga.factum.client.DTO;

/**
 * Clase representación de una Bodega
 */
public class BodegaDTO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idBodega;
	private int idEmpresa;
	private String establecimiento;
	private String Bodega;
	private String ubicacion;
	private String telefono;
	private Double cantidad;
	
	public BodegaDTO(){
	}
	/**
	 * Constructor 
	 * @param idBodega integer
	 * @param Bodega   String
	 */
//	public BodegaDTO(Integer idBodega,String Bodega,String ubicacion,String telefono, double cantidad){
//		this.Bodega = Bodega.toUpperCase();
//		this.idBodega = idBodega;
//		this.ubicacion=ubicacion.toUpperCase();
//		this.telefono=telefono;
//	}
	
	public BodegaDTO(Integer idBodega,String Bodega,String ubicacion,String telefono,int idEmpresa, String establecimiento){
		this.Bodega = Bodega.toUpperCase();
		this.idBodega = idBodega;
		this.ubicacion=ubicacion.toUpperCase();
		this.telefono=telefono;
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
	}
	
	public BodegaDTO(String Bodega,String ubicacion,String telefono){
		this.Bodega = Bodega.toUpperCase();
		this.ubicacion=ubicacion.toUpperCase();
		this.telefono=telefono;
	}
	
	
	public Integer getIdBodega() {
		return idBodega;
	}
	public void setIdBodega(Integer idBodega) {
		this.idBodega = idBodega;
	}
	public String getBodega() {
		return Bodega;
	}
	public void setBodega(String bodega) {
		Bodega = bodega.toUpperCase();
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion.toUpperCase();
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getEstablecimiento() {
		return establecimiento;
	}
	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	
}
