package com.giga.factum.client.DTO;

import java.io.Serializable;

public class FacturaProduccionDTO implements Serializable {
	public String categoria;
	public double total;
	public FacturaProduccionDTO() {
	}
	public FacturaProduccionDTO(String categoria, double total) {
		this.categoria = categoria;
		this.total = total;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	

}
