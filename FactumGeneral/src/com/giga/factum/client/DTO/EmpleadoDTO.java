package com.giga.factum.client.DTO;

import java.util.Date;


public class EmpleadoDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private String establecimiento;
	private Integer idEmpleado;
	private char estado;
	private CargoDTO tblcargo;
	//private TblrolDTO tblrol;
	private PersonaDTO tblpersona;
	private String password;
	private int idRol;
	private int nivelAcceso;
	private float sueldo;
	private float horasExtras;
	private float ingresosExtras;
	private Date fechaContratacion;

	public EmpleadoDTO() {
	}

	//public EmpleadoDTO(CargoDTO tblcargo, PersonaDTO tblpersona,
	public EmpleadoDTO(int idEmpresa,String establecimiento,CargoDTO tblcargo, PersonaDTO tblpersona,
			String password, int nivelAcceso,int idRol, float sueldo, float horasExtras,
			float ingresosExtras, Date fechaContratacion, char estado) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tblcargo = tblcargo;
		this.tblpersona = tblpersona;
		this.password = password;
		this.nivelAcceso = nivelAcceso;
		this.idRol=idRol;
		this.sueldo = sueldo;
		this.horasExtras = horasExtras;
		this.ingresosExtras = ingresosExtras;
		this.fechaContratacion = fechaContratacion;
		this.estado= estado;
	}
	public EmpleadoDTO(int idEmpresa,String establecimiento,PersonaDTO tblpersona,
			String password, int nivelAcceso, int idRol, float sueldo, float horasExtras,
			float ingresosExtras, Date fechaContratacion, char estado) {
		//this.tblcargo = tblcargo;
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.tblpersona = tblpersona;
		this.password = password;
		this.nivelAcceso = nivelAcceso;
		this.idRol=idRol;
		this.sueldo = sueldo;
		this.horasExtras = horasExtras;
		this.ingresosExtras = ingresosExtras;
		this.fechaContratacion = fechaContratacion;
		this.estado= estado;
	}

	public Integer getIdEmpleado() {
		return this.idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public CargoDTO getTblcargo() {
		return this.tblcargo;
	}

	public void setTblcargo(CargoDTO tblcargo) {
		this.tblcargo = tblcargo;
	}

	public PersonaDTO getTblpersona() {
		return this.tblpersona;
	}

	public void setTblpersona(PersonaDTO tblpersona) {
		this.tblpersona = tblpersona;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getNivelAcceso() {
		return this.nivelAcceso;
	}

	public void setNivelAcceso(int nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	public float getSueldo() {
		return this.sueldo;
	}

	public void setSueldo(float sueldo) {
		this.sueldo = sueldo;
	}

	public float getHorasExtras() {
		return this.horasExtras;
	}

	public void setHorasExtras(float horasExtras) {
		this.horasExtras = horasExtras;
	}

	public float getIngresosExtras() {
		return this.ingresosExtras;
	}

	public void setIngresosExtras(float ingresosExtras) {
		this.ingresosExtras = ingresosExtras;
	}

	public Date getFechaContratacion() {
		return this.fechaContratacion;
	}

	public void setFechaContratacion(Date f) {
		this.fechaContratacion = f;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	/*public TblrolDTO getTblrol() {
		return tblrol;
	}

	public void setTblrol(TblrolDTO tblrol) {
		this.tblrol = tblrol;
	}*/

}
