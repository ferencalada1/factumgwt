package com.giga.factum.client.DTO;

public class PlanCuentaDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer idPlan;
	private EmpresaDTO empresaDTO;
	private String fechaCreacion;
	private String periodo;
	

	public PlanCuentaDTO() {
	}

	public PlanCuentaDTO(EmpresaDTO empresaDTO, String fechaCreacion,String periodo) {
		System.out.println("INICIO PLANCUENTADTO");
		this.empresaDTO = empresaDTO;
		System.out.println("empresaDTO: "+empresaDTO.getIdEmpresa());
		this.fechaCreacion = fechaCreacion;
		System.out.println("fechaCreacion: "+fechaCreacion);
		this.periodo = periodo;
		System.out.println("periodo: "+periodo);
		System.out.println("FIN PLANCUENTADTO");
	}

	public PlanCuentaDTO(EmpresaDTO empresaDTO,int idPlan,String fechaCreacion,
			String periodo) {
		System.out.println("INICIO PLAN CUENTA");
		this.idPlan=idPlan;
		System.out.println("idPlan: "+idPlan);
		this.empresaDTO = empresaDTO;
		System.out.println("empresaDTO: "+empresaDTO);
		this.fechaCreacion = fechaCreacion;
		System.out.println("fechaCreacion: "+fechaCreacion);
		this.periodo = periodo;
		System.out.println("periodo: "+periodo);
		System.out.println("FIN PLAN CUENTA");
	}

	public Integer getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}

	public EmpresaDTO getTblempresa() {
		return this.empresaDTO;
	}

	public void setTblempresa(EmpresaDTO empresaDTO) {
		this.empresaDTO = empresaDTO;
	}

	public String getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
}
