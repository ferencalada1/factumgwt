package com.giga.factum.client.DTO;


public class MovimientoCuentaPlanCuentaDTO implements java.io.Serializable {
	
	private MovimientoCuentaPlanCuentaId id;
	private CuentaPlanCuentaDTO tblcuentaPlancuenta;
	private MovimientoDTO tblmovimiento;
	private double valor;
	private char signo;

	public MovimientoCuentaPlanCuentaDTO() {
	}

	
	public MovimientoCuentaPlanCuentaId getId() {
		return this.id;
	}

	public void setId(MovimientoCuentaPlanCuentaId id) {
		this.id = id;
	}

	public CuentaPlanCuentaDTO getTblcuentaPlancuenta() {
		return this.tblcuentaPlancuenta;
	}

	public void setTblcuentaPlancuenta(CuentaPlanCuentaDTO tblcuentaPlancuenta) {
		this.tblcuentaPlancuenta = tblcuentaPlancuenta;
	}

	public MovimientoDTO getTblmovimiento() {
		return this.tblmovimiento;
	}

	public void setTblmovimiento(MovimientoDTO tblmovimiento) {
		this.tblmovimiento = tblmovimiento;
	}

	public double getValor() {
		return this.valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public char getSigno() {
		return this.signo;
	}

	public void setSigno(char signo) {
		this.signo = signo;
	}


}
