package com.giga.factum.client.DTO;

public class DocumentoSaveDTO implements java.io.Serializable{
	private DtocomercialDTO documento;
	private Integer afeccion;
	private Double totalIva;
	private Integer anulacion;
	
	public DocumentoSaveDTO() {
	}
	
	public DocumentoSaveDTO(DtocomercialDTO documento, Integer afeccion, Double totalIva, Integer anulacion) {
		this.documento = documento;
		this.afeccion = afeccion;
		this.totalIva = totalIva;
		this.anulacion = anulacion;
	}
	public DtocomercialDTO getDocumento() {
		return documento;
	}
	public void setDocumento(DtocomercialDTO documento) {
		this.documento = documento;
	}
	public Integer getAfeccion() {
		return afeccion;
	}
	public void setAfeccion(Integer afeccion) {
		this.afeccion = afeccion;
	}
	public Double getTotalIva() {
		return totalIva;
	}
	public void setTotalIva(Double totalIva) {
		this.totalIva = totalIva;
	}
	public Integer getAnulacion() {
		return anulacion;
	}
	public void setAnulacion(Integer anulacion) {
		this.anulacion = anulacion;
	}
	
	

}
