package com.giga.factum.client.DTO;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class MovimientoDTO implements java.io.Serializable{
	
	private Integer idMovimiento;
	private Integer idEmpresa;
	private DtocomercialDTO tbldtocomercial;
	private String concepto;
	private Date fecha;
	
	public MovimientoDTO() {
	}

	
	public Integer getIdMovimiento() {
		return this.idMovimiento;
	}

	public void setIdMovimiento(Integer idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public DtocomercialDTO getTbldtocomercial() {
		return this.tbldtocomercial;
	}

	public void setTbldtocomercial(DtocomercialDTO tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}

	public String getConcepto() {
		return this.concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Integer getIdEmpresa() {
		return idEmpresa;
	}


	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	



}
