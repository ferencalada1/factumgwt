package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;

import com.smartgwt.client.widgets.grid.ListGridField;

public class ReporteClientesDTO implements java.io.Serializable{
	//Cliente,Fecha,NumRealTransaccion,Autorizacion,SubTotal,Iva0,Iva,Total,Estado});
	private static final long serialVersionUID = 1L;
	private String cliente;
	private String cedulaRuc;
	private String fecha;
	private int numRealTransaccion;
	private String autorizacion;
	private double subTotal;
	private double iva0;
	private double iva;
	private double total;
	private double retencion;
	private double retencionIVA;
	private double retencionRENTA;
	private double descuento;
	private String estado;

	public ReporteClientesDTO() {
	}

	public ReporteClientesDTO(String cliente, String cedulaRuc,String fecha, int numRealTransaccion,
			String autorizacion, double subTotal, double iva0, double iva, 
			double total, double retencion,double retencionIVA, double retencionRENTA,String estado) 
	{
		
		this.cliente=cliente.toUpperCase();
		this.cedulaRuc=cedulaRuc;
		this.fecha = fecha;
		this.numRealTransaccion = numRealTransaccion;
		this.autorizacion = autorizacion;
		this.subTotal = subTotal;
		this.iva0 = iva0;
		this.iva = iva;
		this.total = total;
		this.retencion= retencion;
		this.retencionIVA=retencionIVA;
		this.retencionRENTA=retencionRENTA;
		this.estado = estado;
	}
	
	public String getCliente() {
		return this.cliente;
	}
	public void setCliente(String cliente) {
		this.cliente=cliente;
	}
	
	public String getCedulaRuc() {
		return this.cedulaRuc;
	}
	public void setCedulaRuc(String cedulaRuc) {
		this.cedulaRuc=cedulaRuc;
	}
	
	public String getfecha() {
		return this.fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getNumRealTransaccion() {
		return this.numRealTransaccion;
	}
	public void setNumRealTransaccion(Integer numRealTransaccion) {
		this.numRealTransaccion = numRealTransaccion;
	}

	public String getAutorizacion() {
		return this.autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public double getSubTotal() {
		return this.subTotal;
	}
	public void setSubtotal(double subTotal) {
		this.subTotal = subTotal;
	}
	
	public double getIva0() {
		return this.iva0;
	}
	public void setIva0(double iva0) {
		this.iva0 = iva0;
	}
	
	public double getIva() {
		return this.iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	
	public double getTotal() {
		return this.total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getRetencion() {
		return this.retencion;
	}
	public void setRetencion(double retencion) {
		this.retencion = retencion;
	}
	
	public double getRetencionIVA() {
		return this.retencionIVA;
	}
	public void setRetencionIVA(double retencionIVA) {
		this.retencionIVA = retencionIVA;
	}
	
	public double getRetencionRENTA() {
		return this.retencionRENTA;
	}
	public void setRetencionRENTA(double retencionRENTA) {
		this.retencionRENTA = retencionRENTA;
	}
	
	public double getDescuento() {
		return this.descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
