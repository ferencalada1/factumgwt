package com.giga.factum.client.DTO;



public class TipoCuentaDTO implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idTipoCuenta;
	private String nombre;
	
	public TipoCuentaDTO() {
	}

	public TipoCuentaDTO(String nombre) {
		this.nombre = nombre.toUpperCase();
	}
	public TipoCuentaDTO(int id,String nombre){
		this.idTipoCuenta=id;
		this.nombre=nombre.toUpperCase();
	}
	

	public Integer getIdTipoCuenta() {
		return this.idTipoCuenta;
	}

	public void setIdTipoCuenta(Integer idTipoCuenta) {
		this.idTipoCuenta = idTipoCuenta;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}

	

}
