package com.giga.factum.client.DTO;

public class DtocomercialTbltipopagoDTO implements java.io.Serializable{

	//private TbldtocomercialTbltipopagoId id;
	//private Tbldtocomercial tbldtocomercial;
	//private Tbltipopago tbltipopago;
	private Integer tipoPago;
	private double cantidad;
	private Integer referenciaDto;
	private String observaciones;
	private char tipo;

	public DtocomercialTbltipopagoDTO() {
	}

	/*public DtocomercialTbltipopagoDTO(TbldtocomercialTbltipopagoId id,
			Tbldtocomercial tbldtocomercial, Tbltipopago tbltipopago,
			double cantidad) {
		this.id = id;
		this.tbldtocomercial = tbldtocomercial;
		this.tbltipopago = tbltipopago;
		this.cantidad = cantidad;
	}*/

	public DtocomercialTbltipopagoDTO(Integer tipoPago, double cantidad, 
			Integer referenciaDto, String observaciones, char tipo) {
	
		this.tipoPago = tipoPago;
		this.cantidad = cantidad;
		this.referenciaDto = referenciaDto;
		this.observaciones = observaciones;
		this.tipo = tipo;
	}

	/*public DtocomercialTbltipopagoId getId() {
		return this.id;
	}

	public void setId(DtocomercialTbltipopagoId id) {
		this.id = id;
	}

	public Tbldtocomercial getTbldtocomercial() {
		return this.tbldtocomercial;
	}

	public void setTbldtocomercial(Tbldtocomercial tbldtocomercial) {
		this.tbldtocomercial = tbldtocomercial;
	}

	public Tbltipopago getTbltipopago() {
		return this.tbltipopago;
	}

	public void setTbltipopago(Tbltipopago tbltipopago) {
		this.tbltipopago = tbltipopago;
	}*/

	public DtocomercialTbltipopagoDTO(DtocomercialTbltipopagoDTO tPago) {
		this.setCantidad(tPago.getCantidad());
		this.setObservaciones(tPago.getObservaciones());
		this.setReferenciaDto(tPago.getReferenciaDto());
		this.setTipoPago(tPago.getTipoPago());
		this.setTipo(tPago.getTipo());
	}

	public Integer getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getReferenciaDto() {
		return this.referenciaDto;
	}

	public void setReferenciaDto(Integer referenciaDto) {
		this.referenciaDto = referenciaDto;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public char getTipo() {
		return this.tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
}
