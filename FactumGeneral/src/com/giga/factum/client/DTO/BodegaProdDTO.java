package com.giga.factum.client.DTO;

public class BodegaProdDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private Integer idBodega;
	private String Bodega;
	private String ubicacion;
	private Double cantidad;
	
	public BodegaProdDTO(){
	}
	/**
	 * Constructor 
	 * @param idBodega integer
	 * @param Bodega   String
	 * @param ubicacion   String
	 * @param cantidad   Double
	 */
	public BodegaProdDTO(Integer idBodega,String Bodega,String ubicacion,Double cantidad){
		this.Bodega = Bodega.toUpperCase();
		this.idBodega = idBodega;
		this.ubicacion=ubicacion.toUpperCase();
		this.cantidad=cantidad;
	}
	
	public BodegaProdDTO(String Bodega,String ubicacion,Double cantidad){
		this.Bodega = Bodega.toUpperCase();
		this.ubicacion=ubicacion.toUpperCase();
		this.cantidad=cantidad;
	}
	
	
	public Integer getIdBodega() {
		return idBodega;
	}
	public void setIdBodega(Integer idBodega) {
		this.idBodega = idBodega;
	}
	public String getBodega() {
		return Bodega;
	}
	public void setBodega(String bodega) {
		Bodega = bodega.toUpperCase();
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion.toUpperCase();
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	
}
