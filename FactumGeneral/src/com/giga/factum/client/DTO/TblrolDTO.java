package com.giga.factum.client.DTO;

public class TblrolDTO implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idRol;
	private String descripcion;
	private EmpleadoDTO tblempleados;
	
	public TblrolDTO(Integer idEmpresa,int idRol, String descripcion) {
		this.idEmpresa=idEmpresa;
		this.idRol=idRol;
		this.descripcion=descripcion;
	}
	public TblrolDTO() {
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public EmpleadoDTO getTblempleados() {
		return tblempleados;
	}
	public void setTblempleados(EmpleadoDTO tblempleados) {
		this.tblempleados = tblempleados;
	}
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}
