package com.giga.factum.client.DTO;

public class ReporteVentasEmpleadoDTO implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PersonaDTO empleado=new PersonaDTO();
	private float ventas=0;
	private float utilidad=0;
	private float utilidadDetalle=0;
	public ReporteVentasEmpleadoDTO(){
		
	}
	public void setEmpleado(PersonaDTO empleado) {
		this.empleado = empleado;
	}
	public PersonaDTO getEmpleado() {
		return empleado;
	}
	public void setVentas(float ventas) {
		this.ventas = ventas;
	}
	public float getVentas() {
		return ventas;
	}
	public void setUtilidad(float utilidad) {
		this.utilidad = utilidad;
	}
	public float getUtilidad() {
		return utilidad;
	}
	public void setUtilidadDetalle(float utilidadDetalle) {
		this.utilidadDetalle = utilidadDetalle;
	}
	public float getUtilidadDetalle() {
		return utilidadDetalle;
	}
	

}
