package com.giga.factum.client.DTO;

import java.util.Date;

public class MayorizacionDTO implements java.io.Serializable {

	private String periodo;
	private Date fecha;
	private String codigo;
	private int oidTipoCuenta;
	private String cuenta;
	private String descripcion;
	private char signo;
	private int transaccion;
	private double valor;
	private int numero;
	private int nivel;
	private int padre;
	private Double periodoAnterior;

	public MayorizacionDTO() {
	}

	public MayorizacionDTO(Date fecha, String codigo, int oidTipoCuenta,
			String cuenta,int numero, String descripcion, char signo, double valor,
			int nivel, int padre,Double periodoAnterior) {
		this.fecha = fecha;
		this.codigo = codigo;
		this.numero=numero;
		this.oidTipoCuenta = oidTipoCuenta;
		this.cuenta = cuenta;
		this.descripcion = descripcion;
		this.signo = signo;
		this.valor = valor;
		this.nivel = nivel;
		this.padre = padre;
		this.periodoAnterior= periodoAnterior;
	}

	public MayorizacionDTO(String periodo, Date fecha, String codigo,
			int oidTipoCuenta, String cuenta,int numero, String descripcion, char signo,
			double valor, int nivel, int padre, Double periodoAnterior) {
		this.periodo = periodo;
		this.fecha = fecha;
		this.numero=numero;
		this.codigo = codigo;
		this.oidTipoCuenta = oidTipoCuenta;
		this.cuenta = cuenta;
		this.numero=numero;
		this.descripcion = descripcion;
		this.signo = signo;
		this.valor = valor;
		this.nivel = nivel;
		this.padre = padre;
		this.periodoAnterior = periodoAnterior;
	}

	public String getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getOidTipoCuenta() {
		return this.oidTipoCuenta;
	}

	public void setOidTipoCuenta(int oidTipoCuenta) {
		this.oidTipoCuenta = oidTipoCuenta;
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public int getnumeroTransaccion() {
		return this.transaccion;
	}

	public void setnumeroTransaccion(int trans) {
		this.transaccion = trans;
	}
	public int getnumeroDocumento() {
		return this.numero;
	}

	public void setnumeroDocumento(int numero) {
		this.numero = numero;
	}
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public char getSigno() {
		return this.signo;
	}

	public void setSigno(char signo) {
		this.signo = signo;
	}

	public double getValor() {
		return this.valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getNivel() {
		return this.nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public int getnumDocumento() {
		return this.numero;
	}

	public int getPadre() {
		return this.padre;
	}

	public void setPadre(int padre) {
		this.padre = padre;
	}

	public Double getPeriodoAnterior() {
		return this.periodoAnterior;
	}

	public void setPeriodoAnterior(Double periodoAnterior) {
		this.periodoAnterior = periodoAnterior;
	}

//	public boolean equals(Object other) {
//		if ((this == other))
//			return true;
//		if ((other == null))
//			return false;
//		if (!(other instanceof MayorizacionDTO))
//			return false;
//		MayorizacionDTO castOther = (MayorizacionDTO) other;
//
//		return ((this.getPeriodo() == castOther.getPeriodo()) || (this
//				.getPeriodo() != null && castOther.getPeriodo() != null && this
//				.getPeriodo().equals(castOther.getPeriodo())))
//				&& ((this.getFecha() == castOther.getFecha()) || (this
//						.getFecha() != null && castOther.getFecha() != null && this
//						.getFecha().equals(castOther.getFecha())))
//				&& ((this.getCodigo() == castOther.getCodigo()) || (this
//						.getCodigo() != null && castOther.getCodigo() != null && this
//						.getCodigo().equals(castOther.getCodigo())))
//				&& (this.getOidTipoCuenta() == castOther.getOidTipoCuenta())
//				&& ((this.getCuenta() == castOther.getCuenta()) || (this
//						.getCuenta() != null && castOther.getCuenta() != null && this
//						.getCuenta().equals(castOther.getCuenta())))
//				&& ((this.getDescripcion() == castOther.getDescripcion()) || (this
//						.getDescripcion() != null
//						&& castOther.getDescripcion() != null && this
//						.getDescripcion().equals(castOther.getDescripcion())))
//				&& (this.getSigno() == castOther.getSigno())
//				&& (this.getValor() == castOther.getValor())
//				&& (this.getNivel() == castOther.getNivel())
//				&& (this.getPadre() == castOther.getPadre())
//				&& ((this.getPeriodoAnterior() == castOther
//						.getPeriodoAnterior()) || (this.getPeriodoAnterior() != null
//						&& castOther.getPeriodoAnterior() != null && this
//						.getPeriodoAnterior().equals(
//								castOther.getPeriodoAnterior())));
//	}
//
//	public int hashCode() {
//		int result = 17;
//
//		result = 37 * result
//				+ (getPeriodo() == null ? 0 : this.getPeriodo().hashCode());
//		result = 37 * result
//				+ (getFecha() == null ? 0 : this.getFecha().hashCode());
//		result = 37 * result
//				+ (getCodigo() == null ? 0 : this.getCodigo().hashCode());
//		result = 37 * result + this.getOidTipoCuenta();
//		result = 37 * result
//				+ (getCuenta() == null ? 0 : this.getCuenta().hashCode());
//		result = 37
//				* result
//				+ (getDescripcion() == null ? 0 : this.getDescripcion()
//						.hashCode());
//		result = 37 * result + this.getSigno();
//		result = 37 * result + (int) this.getValor();
//		result = 37 * result + this.getNivel();
//		result = 37 * result + this.getPadre();
//		result = 37
//				* result
//				+ (getPeriodoAnterior() == null ? 0 : this.getPeriodoAnterior()
//						.hashCode());
//		return result;
//	}

}
