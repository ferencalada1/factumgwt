package com.giga.factum.client.DTO;

// Generated 19/08/2016 09:43:46 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * Tblequipo generated by hbm2java
 */
public class EquipoDTO implements java.io.Serializable {

	private Integer idEmpresa;
	private String establecimiento;
	private Integer idequipo;
	private int idtipoequipo;
	private int idmarca;
	private String modeloequipo;
	private String numeroserie;
	private int idtipo;
	private Set tblequipoTblordens = new HashSet(0);

	public EquipoDTO() {
	}

	public EquipoDTO(Integer idEmpresa,String establecimiento,int idtipoequipo, int idmarca, String modeloequipo,
			String numeroserie, int idtipo) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idtipoequipo = idtipoequipo;
		this.idmarca = idmarca;
		this.modeloequipo = modeloequipo;
		this.numeroserie = numeroserie;
		this.idtipo = idtipo;
	}

	public EquipoDTO(Integer idEmpresa,String establecimiento,int idtipoequipo, int idmarca, String modeloequipo,
			String numeroserie, int idtipo, Set tblequipoTblordens) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idtipoequipo = idtipoequipo;
		this.idmarca = idmarca;
		this.modeloequipo = modeloequipo;
		this.numeroserie = numeroserie;
		this.idtipo = idtipo;
		this.tblequipoTblordens = tblequipoTblordens;
	}
	
	public EquipoDTO(Integer idEmpresa,String establecimiento,int idequipo,int idtipoequipo, int idmarca, String modeloequipo,
			String numeroserie) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idequipo = idequipo;
		this.idtipoequipo = idtipoequipo;
		this.idmarca = idmarca;
		this.modeloequipo = modeloequipo;
		this.numeroserie = numeroserie;
	}
	

	public EquipoDTO(Integer idEmpresa,String establecimiento,int idtipoequipo, int idmarca, String modeloequipo,
			String numeroserie) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idtipoequipo = idtipoequipo;
		this.idmarca = idmarca;
		this.modeloequipo = modeloequipo;
		this.numeroserie = numeroserie;
	}
	

	public EquipoDTO(Integer idEmpresa,String establecimiento,int idtipoequipo, int idmarca, String modeloequipo,
			String numeroserie, Set tblequipoTblordens) {
		this.idEmpresa=idEmpresa;
		this.establecimiento=establecimiento;
		this.idtipoequipo = idtipoequipo;
		this.idmarca = idmarca;
		this.modeloequipo = modeloequipo;
		this.numeroserie = numeroserie;
		this.tblequipoTblordens = tblequipoTblordens;
	}
	
	public Integer getIdequipo() {
		return this.idequipo;
	}

	public void setIdequipo(Integer idequipo) {
		this.idequipo = idequipo;
	}

	public int getIdtipoequipo() {
		return this.idtipoequipo;
	}

	public void setIdtipoequipo(int idtipoequipo) {
		this.idtipoequipo = idtipoequipo;
	}

	public int getIdmarca() {
		return this.idmarca;
	}

	public void setIdmarca(int idmarca) {
		this.idmarca = idmarca;
	}

	public String getModeloequipo() {
		return this.modeloequipo;
	}

	public void setModeloequipo(String modeloequipo) {
		this.modeloequipo = modeloequipo;
	}

	public String getNumeroserie() {
		return this.numeroserie;
	}

	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}

	public int getIdtipo() {
		return this.idtipo;
	}

	public void setIdtipo(int idtipo) {
		this.idtipo = idtipo;
	}

	public Set getTblequipoTblordens() {
		return this.tblequipoTblordens;
	}

	public void setTblequipoTblordens(Set tblequipoTblordens) {
		this.tblequipoTblordens = tblequipoTblordens;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}

}
