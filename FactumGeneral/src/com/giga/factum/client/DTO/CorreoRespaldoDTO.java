package com.giga.factum.client.DTO;

import com.giga.factum.server.base.Tblcorreorespaldo;

public class CorreoRespaldoDTO implements java.io.Serializable{
	private int idtblcorreo_respaldo;	
	private String mail;
	private EstablecimientoDTO establecimiento;
	public CorreoRespaldoDTO(int idtblcorreo_respaldo, String mail, EstablecimientoDTO establecimiento) {
		this.idtblcorreo_respaldo = idtblcorreo_respaldo;
		this.mail = mail;
		this.establecimiento = establecimiento;
	}
	public CorreoRespaldoDTO(int idtblcorreo_respaldo, String mail) {
		this.idtblcorreo_respaldo = idtblcorreo_respaldo;
		this.mail = mail;
	}
	public CorreoRespaldoDTO() {
	}
	
	/**
	 * @return the idtblcorreo_respaldo
	 */
	public int getIdtblcorreo_respaldo() {
		return idtblcorreo_respaldo;
	}
	/**
	 * @param idtblcorreo_respaldo the idtblcorreo_respaldo to set
	 */
	public void setIdtblcorreo_respaldo(int idtblcorreo_respaldo) {
		this.idtblcorreo_respaldo = idtblcorreo_respaldo;
	}
	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return the establecimiento
	 */
	public EstablecimientoDTO getEstablecimiento() {
		return establecimiento;
	}
	/**
	 * @param establecimiento the establecimiento to set
	 */
	public void setEstablecimiento(EstablecimientoDTO establecimiento) {
		this.establecimiento = establecimiento;
	}
	
	
}
