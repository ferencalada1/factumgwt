package com.giga.factum.client.DTO;

import java.util.HashSet;
import java.util.Set;


public class TblpermisorolpestanaDTO implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TblpermisorolpestanaIdDTO idPermisorolpestana;
	private TblrolDTO tblrolByIdRol;
	private TblpestanaDTO tblpestanaByIdPestana;
	private TblpermisoDTO tblpermisoByIdPermiso;
	private Set <TblrolDTO>tblroles= new HashSet<TblrolDTO>(0);
	private Set <TblpermisoDTO>tblpermisos= new HashSet<TblpermisoDTO>(0);
	private Set <TblpestanaDTO>tblpestanas= new HashSet<TblpestanaDTO>(0);
	
	public TblpermisorolpestanaDTO(){
	}
	public TblpermisorolpestanaDTO(TblpermisorolpestanaIdDTO id, TblpermisoDTO tblpermisos,TblpestanaDTO tblpestanas, TblrolDTO tblroles) {
		// TODO Auto-generated constructor stub
		this.idPermisorolpestana=id;
		this.tblpermisoByIdPermiso=tblpermisos;
		this.tblpestanaByIdPestana=tblpestanas;
		this.tblrolByIdRol=tblroles;		
	}
	public TblpermisorolpestanaIdDTO getIdPermisorolpestana() {
		return idPermisorolpestana;
	}
	public void setIdPermisorolpestana(TblpermisorolpestanaIdDTO idPermisorolpestana) {
		this.idPermisorolpestana = idPermisorolpestana;
	}
	public TblrolDTO getTblrolByIdRol() {
		return tblrolByIdRol;
	}
	public void setTblrolByIdRol(TblrolDTO tblrolByIdRol) {
		this.tblrolByIdRol = tblrolByIdRol;
	}
	public TblpestanaDTO getTblpestanaByIdPestana() {
		return tblpestanaByIdPestana;
	}
	public void setTblpestanaByIdPestana(TblpestanaDTO tblpestanaByIdPestana) {
		this.tblpestanaByIdPestana = tblpestanaByIdPestana;
	}
	public TblpermisoDTO getTblpermisoByIdPermiso() {
		return tblpermisoByIdPermiso;
	}
	public void setTblpermisoByIdPermiso(TblpermisoDTO tblpermisoByIdPermiso) {
		this.tblpermisoByIdPermiso = tblpermisoByIdPermiso;
	}

	public Set <TblrolDTO> getTblroles() {
		return tblroles;
	}

	public void setTblroles(Set <TblrolDTO> tblroles) {
		this.tblroles = tblroles;
	}

	public Set <TblpermisoDTO> getTblpermisos() {
		return tblpermisos;
	}

	public void setTblpermisos(Set <TblpermisoDTO> tblpermisos) {
		this.tblpermisos = tblpermisos;
	}

	public Set <TblpestanaDTO> getTblpestanas() {
		return tblpestanas;
	}

	public void setTblpestanas(Set <TblpestanaDTO> tblpestanas) {
		this.tblpestanas = tblpestanas;
	}


}
