package com.giga.factum.client.DTO;

public class TipoPagoDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private Integer idTipoPago;
	private String tipoPago;
	private int idPlan;
	private int idCuenta;

	public TipoPagoDTO() {
	}

	public TipoPagoDTO(Integer idEmpresa,Integer idtp, String tipoPago, int idPlan, int idCuenta) {
		this.idTipoPago = idtp;
		this.tipoPago = tipoPago.toUpperCase();
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
		this.idEmpresa=idEmpresa;
	}
	
	public TipoPagoDTO(Integer idEmpresa,String tipoPago, int idPlan, int idCuenta) {
		this.tipoPago = tipoPago.toUpperCase();
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
		this.idEmpresa=idEmpresa;
	}

	public Integer getIdTipoPago() {
		return this.idTipoPago;
	}

	public void setIdTipoPago(Integer idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	public String getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago.toUpperCase();
	}

	public int getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
}
