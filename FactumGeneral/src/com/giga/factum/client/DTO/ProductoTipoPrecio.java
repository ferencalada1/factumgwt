package com.giga.factum.client.DTO;

public class ProductoTipoPrecio implements java.io.Serializable, Comparable<ProductoTipoPrecio>{

	private static final long serialVersionUID = 1L;
	
	private int idProducto;
	private double porcentaje;
	private int idTipo;
	private String tipo;
	private int orden;
	
	public ProductoTipoPrecio() {}

	public ProductoTipoPrecio(int idProducto,double porcentaje, int idTipo,String tipo, int orden) 
	{
		this.idProducto=idProducto;
		this.porcentaje=porcentaje;
		this.idTipo=idTipo;
		this.tipo=tipo;
		this.orden=orden;
	}
		
	public int getIdProducto() 
	{return this.idProducto;}
	public void setIdProducto(int idProducto) 
	{this.idProducto = idProducto;}
	
	public double getPorcentaje() 
	{return this.porcentaje;}
	public void setPorcentaje(double porcentaje) 
	{this.porcentaje = porcentaje;}

	public int getIdTipo() 
	{return this.idTipo;}
	public void setIdTipo(int idTipo) 
	{this.idTipo = idTipo;}

	public String getTipo() 
	{return this.tipo;}
	public void setTipo(String tipo) 
	{this.tipo = tipo;}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	@Override
	public int compareTo(ProductoTipoPrecio o) {
		// TODO Auto-generated method stub
		if (getOrden()!=o.getOrden()){
			return getOrden()-o.getOrden();
		}
		return Double.compare(getPorcentaje(),o.getPorcentaje());
	}
	
	
}
