package com.giga.factum.client.DTO;

public class ReportesDTO  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private double TOTAL;
	private double SubtNeto;
	private double SubtIVA0;
	private double IVA;
	
	
	public ReportesDTO(){
		
	}
	public void setTOTAL(double tOTAL) {
		TOTAL = tOTAL;
	}
	public double getTOTAL() {
		return TOTAL;
	}
	public void setSubtNeto(double subtNeto) {
		SubtNeto = subtNeto;
	}
	public double getSubtNeto() {
		return SubtNeto;
	}
	public void setSubtIVA0(double subtIVA0) {
		SubtIVA0 = subtIVA0;
	}
	public double getSubtIVA0() {
		return SubtIVA0;
	}
	public void setIVA(double iVA) {
		IVA = iVA;
	}
	public double getIVA() {
		return IVA;
	}
	

}
