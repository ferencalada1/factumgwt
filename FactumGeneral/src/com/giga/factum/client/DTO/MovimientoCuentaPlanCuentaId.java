package com.giga.factum.client.DTO;


public class MovimientoCuentaPlanCuentaId implements java.io.Serializable{
	
	private int idMovimiento;
	private int idPlan;
	private int idCuenta;

	public MovimientoCuentaPlanCuentaId() {
	}

	public MovimientoCuentaPlanCuentaId(int idMovimiento, int idPlan,
			int idCuenta) {
		this.idMovimiento = idMovimiento;
		this.idPlan = idPlan;
		this.idCuenta = idCuenta;
	}

	public int getIdMovimiento() {
		return this.idMovimiento;
	}

	public void setIdMovimiento(int idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public int getIdPlan() {
		return this.idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}

	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}

	

}
