package com.giga.factum.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.FacturaProduccionDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.regGrillas.CuadreCajaRecords;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.client.regGrillas.Producto;
import com.giga.factum.client.regGrillas.TotalesRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.TimeItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class frmCuadreCaja extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	ListGrid lstReporteCaja = new ListGrid();
	ListGrid lstTotales = new ListGrid();
	ListGridFacturaProducccion lstfacturaProduccion = new ListGridFacturaProducccion();
	DateTimeItem txtFecha =new DateTimeItem("txtFecha","Fecha");
	
	TimeItem txtHoraEntrada = new TimeItem("txtHoraEntrada", "Entrada"); 
	TimeItem txtHoraSalida = new TimeItem("txtHoraSalida", "Salida"); 
   
	
	public frmCuadreCaja() {
		dynamicForm.setSize("100%", "5%");
		dynamicForm.setNumCols(6);
		txtFecha.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFecha.setRequired(true);
		txtFecha.setValue(new Date());
		txtFecha.setMaskDateSeparator("-");
		txtFecha.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFecha.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		/*
		txtHoraEntrada.setSelectorFormat(DateItemSelectorFormat.);
		txtHoraEntrada.setRequired(true);
		txtHoraEntrada.setValue(new Date());
		txtHoraEntrada.setMaskDateSeparator("-");
		txtHoraEntrada.setDateFormatter(DateDisplayFormat.);
		*/
		txtHoraEntrada.setRequired(true);
		txtHoraEntrada.setValue("00:00");
		txtHoraEntrada.setUseMask(true);
		
		txtHoraSalida.setRequired(true);
		txtHoraSalida.setValue("23:59");
		txtHoraSalida.setUseMask(true);
		

		dynamicForm.setFields(new FormItem[] { txtFecha, txtHoraEntrada, txtHoraSalida});
		addMember(dynamicForm);
		lstReporteCaja.setSize("100%", "60%");
		ListGridField NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
		ListGridField TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
		ListGridField Cliente =new ListGridField("Cliente", "Cliente");
		ListGridField Credito =new ListGridField("Credito", "Credito");
		ListGridField Efectivo =new ListGridField("Efectivo", "Efectivo");
		ListGridField Banco =new ListGridField("Banco", "Banco");
		ListGridField Retencion =new ListGridField("Retencion", "Retencion");
		ListGridField Anticipo =new ListGridField("Anticipo", "Anticipo");
		ListGridField NotaCredito =new ListGridField("NotaCredito", "NotaCredito");
		ListGridField TarjetaCredito =new ListGridField("TarjetaCredito", "Tarjeta de Credito");
		ListGridField Observaciones =new ListGridField("Observaciones", "Observaciones");
		ListGridField Tipo =new ListGridField("Tipo", "Tipo");
		
		NumRealTransaccion.setAlign(Alignment.CENTER);
		TipoTransaccion.setAlign(Alignment.CENTER);
		Cliente.setAlign(Alignment.CENTER);
		Cliente.setWidth("36%");
		Credito.setAlign(Alignment.RIGHT);
		Efectivo.setAlign(Alignment.RIGHT);
		Banco.setAlign(Alignment.RIGHT);
		Retencion.setAlign(Alignment.RIGHT);
		Anticipo.setAlign(Alignment.RIGHT);
		NotaCredito.setAlign(Alignment.RIGHT);
		TarjetaCredito.setAlign(Alignment.RIGHT);
		Observaciones.setAlign(Alignment.CENTER);
		Tipo.setAlign(Alignment.CENTER);
		
		lstReporteCaja.setFields(new ListGridField[] { TipoTransaccion,Cliente,NumRealTransaccion ,Credito,Efectivo
				,Banco,Retencion,Anticipo,NotaCredito,TarjetaCredito,Observaciones,Tipo});
//		lstReporteCaja.setShowRowNumbers(false);
		addMember(lstReporteCaja);
		
		HLayout hlayout = new HLayout();
		
		ListGridField Nombre =new ListGridField("Nombre", "Nombre");
		ListGridField Total =new ListGridField("Total", "Total");
		Total.setAlign(Alignment.RIGHT);
		lstTotales.setFields(new ListGridField[] {Nombre,Total});
		//lstTotales.setSize("50%", "30%");
		hlayout.addMember(lstTotales);
		
		//XAVIER ZEAS
		lstfacturaProduccion = new ListGridFacturaProducccion();
		hlayout.addMember(lstfacturaProduccion);
		//XVIER ZEAS
		addMember(hlayout);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		IButton btnGenerarReporte = new IButton("Generar Reporte");
		hStack.addMember(btnGenerarReporte);
		btnGenerarReporte.addClickHandler( new ManejadorBotones("reporte")) ;
		
		IButton btnImprimir = new IButton("Imprimir");
		btnImprimir.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	HLayout hlayout = new HLayout();
            	hlayout.addMember(lstTotales);
            	hlayout.addMember(lstfacturaProduccion);
            	Object[] a=new Object[]{dynamicForm,lstReporteCaja,hlayout};
                Canvas.showPrintPreview(a);  
                
            }
		});
		hStack.addMember(btnImprimir);
		
		IButton btnLimpiar = new IButton("Limpiar");
		hStack.addMember(btnLimpiar);
		btnLimpiar.addClickHandler( new ManejadorBotones("limpiar")) ;
		addMember(hStack);
		reporte();
		
	}
	
	
	private class ManejadorBotones implements ClickHandler{
		String indicador="";
		
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		 
		@Override 
		public void onClick(ClickEvent event) {
			if(indicador.equalsIgnoreCase("reporte")){
				reporte();
				
			}else if(indicador.equalsIgnoreCase("limpiar")){
				
			}else if(indicador.equalsIgnoreCase("imprimir")){
				
			}
		}

		
	}
	
	
	final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			int numRegistros=0;
			for(int i=0;i<result.size();i++){
				//DtocomercialRecords dojjc =new DtocomercialRecords(result.get(i));
				if(result.get(i).getSubtotal()>=0  && result.get(i).getTipoTransaccion()!=3)
				{	
					CuadreCajaRecords record=new CuadreCajaRecords(result.get(i));
					if(!record.getAttributeAsString("TipoTransaccion").equals("")){
						listado[numRegistros]=(record); 
						numRegistros++;
					}
				}
			}
			try{
				lstReporteCaja.setData(listado);
				String t="";
				Double efectivo=0.0;
				Double credito=0.0;
				Double retencion=0.0;
				Double anticipo=0.0;
				Double ncredito=0.0;
				Double tcredito=0.0;
				Double Banco=0.0;
				int j=lstReporteCaja.getRecords().length;
				for(int i=0;i<j;i++)
				{
					ListGridRecord record=lstReporteCaja.getRecord(i);
							{
							if(lstReporteCaja.getRecord(i).getAttributeAsString("Observaciones")!="ANULADO")
							{
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("Efectivo")!=null)
							{
								efectivo=efectivo+lstReporteCaja.getRecord(i).getAttributeAsDouble("Efectivo");
								efectivo=CValidarDato.getDecimal(Factum.banderaNumeroDecimales,efectivo);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("Credito")!=null)
							{
								credito=credito+lstReporteCaja.getRecord(i).getAttributeAsDouble("Credito");
								credito=CValidarDato.getDecimal(Factum.banderaNumeroDecimales,credito);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("Retencion")!=null)
							{
								retencion=retencion+lstReporteCaja.getRecord(i).getAttributeAsDouble("Retencion");
								retencion = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,retencion);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("Anticipo")!=null)
							{
								anticipo=anticipo+lstReporteCaja.getRecord(i).getAttributeAsDouble("Anticipo");
								anticipo = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,anticipo);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("NotaCredito")!=null)
							{
								ncredito=ncredito+lstReporteCaja.getRecord(i).getAttributeAsDouble("NotaCredito");
								ncredito = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,ncredito);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("TarjetaCredito")!=null)
							{
								tcredito=tcredito+lstReporteCaja.getRecord(i).getAttributeAsDouble("TarjetaCredito");
								tcredito = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,tcredito);
							}
							if(lstReporteCaja.getRecord(i).getAttributeAsDouble("Banco")!=null)
							{
								Banco=Banco+lstReporteCaja.getRecord(i).getAttributeAsDouble("Banco");
								Banco = CValidarDato.getDecimal(Factum.banderaNumeroDecimales,Banco);
							}
							
					}
						}
					//	else{
					//	t=String.valueOf(i)+" "+lstReporteCaja.getRecord(i).getAttributeAsString("Tipo");
				//	}
				
				}
				lstReporteCaja.redraw();
				
				ListGridRecord[] lis = new ListGridRecord[7];
				for(int i=0;i<7;i++){
					listado[i]=new ListGridRecord();
				}
				lis[0]=new TotalesRecords("Efectivo", efectivo);
				lis[1]=new TotalesRecords("Credito", credito);
				lis[2]=new TotalesRecords("Retencion", retencion);
				lis[3]=new TotalesRecords("Anticipo", anticipo);
				lis[4]=new TotalesRecords("Nota de Credito", ncredito);
				lis[5]=new TotalesRecords("Tarjeta de Credito", tcredito);
				lis[6]=new TotalesRecords("Banco", Banco);
				lstTotales.setData(lis);
				lstTotales.redraw();
				SC.say("Cuadre de Caja Generado...");
				
				
			}catch(Exception e){
				SC.say("error cargar datos grilla "+e.getMessage());
				//SC.say(t+" "+String.valueOf(efectivo)+" j="+String.valueOf(j));
			}
			
			
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	    }
	};
	
	final AsyncCallback<ArrayList<FacturaProduccionDTO>>  listaCallbackFacturaProduccion=new AsyncCallback<ArrayList<FacturaProduccionDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			
		}

		public void onSuccess(ArrayList<FacturaProduccionDTO> result) {
			RecordList listado = new RecordList();
			Record record;
			for(FacturaProduccionDTO facturaproducciondto:result){
				record= new Record();
				record.setAttribute("lstCategoria", facturaproducciondto.getCategoria());
				record.setAttribute("lstTotal", facturaproducciondto.getTotal());
				listado.add(record);
            }
            lstfacturaProduccion.setData(listado);
            lstfacturaProduccion.redraw();
		}
		
	};
	
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}

	public void reporte(){
		try{
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarDocFecha(txtFecha.getDisplayValue(),listaCallback);
			getService().listarFacturasProduccion(txtFecha.getDisplayValue() ,listaCallbackFacturaProduccion);
			
		}catch(Exception e){
			SC.say("error en pantalla "+e.getMessage());
		}
	}
}
