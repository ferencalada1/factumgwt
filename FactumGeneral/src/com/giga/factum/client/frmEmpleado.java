package com.giga.factum.client;

import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;
import com.smartgwt.client.widgets.layout.VLayout;
import com.giga.factum.client.DTO.CargoDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.EmpleadoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.TblrolDTO;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.ExpansionMode;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;



public class frmEmpleado extends VLayout {
	DynamicForm frmPantallaEmpleado;
	boolean ban=false;
	SearchForm searchForm = new SearchForm();
    String pantalla="";
	ListGrid lstEmpleado = new ListGrid();
	List<CargoDTO> listCargo=null;
	LinkedHashMap<String, String> MapCargo = new LinkedHashMap<String, String>();
	//SelectItem cmbcargo= new SelectItem("cmbcargo", "Cargo");
	TabSet tabSet = new TabSet();
	DateTimeItem txtFechaContratacion = new DateTimeItem("cmbFechaContratcion", "Fecha de Contrataci\u00F3n");
	Label lblRegisros = new Label("# Registros");
	int contador=20;
	int registros=0;
	int tipoUsuario;
	FloatItem txtcupoCredito = new FloatItem();
	FloatItem txtmontoCredito = new FloatItem();
	IButton btnGrabar = new IButton("Grabar");
	Button btnEliminar = new Button("Eliminar");
	IButton btnNuevo = new IButton("Nuevo");
	IButton btnModificar = new IButton("Modificar");
	CheckboxItem chkCliente;
	
	//final ComboBoxItem cmbRol=new ComboBoxItem("cbmRol","Tipo deUsuario");
	SelectItem cmbrol= new SelectItem("cmbrol", "Rol");
	List<TblrolDTO> listrol=null;
	LinkedHashMap<String, String> MapRol= new LinkedHashMap<String, String>();
	
	
	public frmEmpleado() {
		txtFechaContratacion.setRequired(true);
		txtFechaContratacion.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaContratacion.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		getService().numeroRegistrosPersona("tblempleados", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon buscarPickerlst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerlst.addFormItemClickHandler(new ManejadorBotones("buscar"));
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscarfrm"));
		RegExpValidator regExpValidator = new RegExpValidator();  
        regExpValidator.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");
        FloatRangeValidator floatRangeValidator = new FloatRangeValidator(); 
        
		tabSet.setSize("100%", "100%");
		Tab tabIngreso = new Tab("Ingreso Usuario");
		
		
		VLayout layout = new VLayout();
		frmPantallaEmpleado = new DynamicForm();
		frmPantallaEmpleado.setSize("100%", "90%");
		frmPantallaEmpleado.setMinColWidth(50);
		frmPantallaEmpleado.setItemLayout(FormLayoutType.TABLE);
		
		frmPantallaEmpleado.setWidth100();
		
		frmPantallaEmpleado.setNumCols(4);
		TextItem txtNombreComercial = new TextItem("txtNombreComercial", "Nombre Comercial");
		txtNombreComercial.setLength(50);
		txtNombreComercial.setTabIndex(3);
		txtNombreComercial.setLeft(133);
		txtNombreComercial.setTop(34);
		txtNombreComercial.setKeyPressFilter("[a-zA-Z\u00F1\u00D1 ]");
		txtNombreComercial.setRequired(true);
		txtNombreComercial.setWidth(300);
		
		TextItem txtCedula = new TextItem("txtCedula", "C\u00E9dula");
		txtCedula.setTabIndex(1);
		txtCedula.setTooltip("Ingrese la C\u00E9dula");
		txtCedula.setLeft(133);
		txtCedula.setTop(6);
		txtCedula.setHint("Solo n\u00FAmeros");
		txtCedula.setKeyPressFilter("[0-9]");
		txtCedula.setRequired(true);
		txtCedula.setIcons(buscarPicker);
		
		TextItem txtRazonSocial = new TextItem("txtRazonSocial", "Razon Social");
		txtRazonSocial.setLength(50);
		txtRazonSocial.setRequired(true);
		txtRazonSocial.setTabIndex(2);
		txtRazonSocial.setWidth(300);
		
		TextItem txtDireccion = new TextItem("txtDireccion", "Direcci\u00F3n");
		txtDireccion.setTabIndex(4);
		txtDireccion.setRequired(true);
		txtDireccion.setLength(100);
		txtDireccion.setWidth(300);
		
		TextItem txtTelefono = new TextItem("txtTelefono", "Tel\u00E9fono");
		txtTelefono.setTabIndex(7);
		txtTelefono.setTooltip("Tel\u00E9fono");
		txtTelefono.setKeyPressFilter("[0-9]");
		txtTelefono.setLength(15);
		
		TextItem txtObservaciones = new TextItem("txtObservaciones", "Observaciones");
		txtObservaciones.setLength(100);
		txtObservaciones.setWidth(300);
		
		TextItem txtEmail = new TextItem("txtEmail", "e-mail");
		txtEmail.setTooltip("Ingrese el email del cliente");
		txtEmail.setShowHint(false);
		txtEmail.setLength(100);
		txtEmail.setValidators(regExpValidator);
		txtEmail.setWidth(300);
		final FloatItem txtSalario = new FloatItem();
		txtSalario.setTabIndex(6);
		txtSalario.setName("txtSalario");
		txtSalario.setTitle("Salario");
		txtSalario.setValidators(floatRangeValidator);
		txtSalario.setRequired(true);
		
		PasswordItem passwordItem = new PasswordItem();  
		passwordItem.setTabIndex(9);
		passwordItem.setRequired(true);
		passwordItem.setTitle("Password");
        passwordItem.setName("password");  
  
        PasswordItem passwordItem2 = new PasswordItem();  
        passwordItem2.setTabIndex(10);
        passwordItem2.setName("password2");  
        passwordItem2.setTitle("Password Again");  
        passwordItem2.setRequired(true);  
        passwordItem2.setLength(20);  
  
        MatchesFieldValidator matchesValidator = new MatchesFieldValidator();  
        matchesValidator.setOtherField("password");  
        matchesValidator.setErrorMessage("Passwords do not match");          
        passwordItem2.setValidators(matchesValidator);
        
  
		txtcupoCredito.setTitle("Cupo Cr\u00E9dito Cliente:");
		txtcupoCredito.setName("txtcupoCredito");
		
		
		txtmontoCredito.setTitle("Monto Cr\u00E9dito Cliente");
		txtmontoCredito.setName("txtmontoCredito");
		txtmontoCredito.setValidators(floatRangeValidator);
		
		chkCliente =new CheckboxItem("chkCliente", "Asignar este Empleado como cliente");
		chkCliente.setRedrawOnChange(true);
		txtmontoCredito.setValue("0");
		//txtmontoCredito.setShowIfCondition(new FuncionSI());
		//txtcupoCredito.setShowIfCondition(new FuncionSI());
		txtmontoCredito.setVisible(false);
		txtcupoCredito.setVisible(false);
		txtcupoCredito.setValue("0");
		
		cmbrol.setTabIndex(11);		
		cmbrol.setRequired(true);
		//cmbRol.setIcons(NuevaCategoria);
		//SelectItem cmbTipoUsuario= new SelectItem("cmbTipoUsuario", "Tipo de Usuario");
		//cmbTipoUsuario.setTabIndex(11);
		txtFechaContratacion.setTabIndex(5);

		//cmbTipoUsuario.setRequired(true);
		//cmbTipoUsuario.setValueMap("Administrador","Contabilidad","Ventas","Bodega","Caja");
		
		//cmbcargo.setTabIndex(12);
		//cmbcargo.setRequired(true);
		
		TextItem txtTelefono2 = new TextItem("txtTelefono2", "Tel\u00E9fono 2");
		txtTelefono2.setKeyPressFilter("[0-9]");
		txtTelefono2.setLength(15);
		txtTelefono2.setTabIndex(8);
		FormItem formitem = new FormItem();
		formitem.setShowTitle(false);
		frmPantallaEmpleado.setFields(new FormItem[] { txtCedula, passwordItem, txtRazonSocial, passwordItem2, txtNombreComercial, cmbrol,txtEmail ,
				//cmbcargo,
				txtTelefono, 
				txtFechaContratacion,txtTelefono2,txtSalario, txtDireccion ,txtObservaciones, formitem,
				chkCliente, txtcupoCredito, txtmontoCredito});
		layout.addMember(frmPantallaEmpleado);
		Canvas canvas = new Canvas();
		canvas.setSize("100%", "5%");
		
		canvas.addChild(btnEliminar);
		btnEliminar.moveTo(324, 6);
		layout.addMember(canvas);
		
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(6, 6);
		btnGrabar.setSize("100px", "22px");
		
		
		canvas.addChild(btnNuevo);
		btnNuevo.moveTo(112, 6);
		
		
		canvas.addChild(btnModificar);
		btnModificar.moveTo(218, 6);
		
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		
		btnModificar.addClickHandler(new ManejadorBotones("Modificar"));
		tabIngreso.setPane(layout);
		tabSet.addTab(tabIngreso);
		
		 btnGrabar.setDisabled(false) ;
		 btnEliminar.setDisabled(true) ;
		 btnModificar.setDisabled(true) ;
		 
		Tab tabListado = new Tab("Listado de Usuarios");
		

		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
         
        HLayout hLayout = new HLayout();
        hLayout.setSize("100%", "5%");
         
        searchForm.setSize("88%", "100%");
        searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
        searchForm.setNumCols(4);
        ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n");
        
        TextItem txtBuscarLst=new TextItem();
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerlst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        buscarPicker.addFormItemClickHandler(new ManejadorBotones("buscarfrm"));
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(56);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        hLayout.addMember(searchForm);
        HStack hStack = new HStack();
        hLayout.addMember(hStack);
        hStack.setSize("12%", "100%");
        TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        //TransferImgButton btnEliminarLst = new TransferImgButton(TransferImgButton.DELETE);
        
        btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        //hStack.addMember(btnEliminarLst);
        
        layout_1.addMember(hLayout);
        lstEmpleado.setCanExpandRecords(true);  
        lstEmpleado.setExpansionMode(ExpansionMode.DETAIL_FIELD);  
        lstEmpleado.setDetailField("background");  
        
        lstEmpleado.setSize("100%", "80%");
        ListGridField  Cedula=new ListGridField("Cedula", "C\u00E9dula/RUC",150);
        lstEmpleado.setFields(new ListGridField[] { Cedula, new ListGridField("NombreComercial", "Nombre Comercial",250), new ListGridField("RazonSocial", "Razon Social",250), 
        		new ListGridField("Telefonos", "Tel\u00E9fono",150),new ListGridField("Telefonos2", "Tel\u00E9fono",150),new ListGridField("Direccion", "Direcci\u00F3n",400),
        		//new ListGridField("Cargo", "Cargo",150),
        		new ListGridField("NivelA", "Nivel de Acceso",100),new ListGridField("FechaContratacion", "Fecha de Contrataci\u00F3n",100),new ListGridField("observaciones", "Observaciones",150)});
        layout_1.addMember(lstEmpleado);
        lblRegisros.setSize("100%", "4%");
	    layout_1.addMember(lblRegisros);
	    HStack hStack_1 = new HStack();
        hStack_1.setSize("100%", "6%");
        IButton btnExportar = new IButton("Exportar");
        btnExportar.addClickHandler(new ManejadorBotones("exportar"));
        IButton btnImprimir = new IButton("Imprimir");
        btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
        hStack_1.addMember(btnExportar);
        hStack_1.addMember(btnImprimir);
        layout_1.addMember(hStack_1);

	    
	    tabListado.setPane(layout_1);
		tabSet.addTab(tabListado);
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarCargo(0,50, objbacklstCargo);
		contador=20;
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		
		lstEmpleado.addRecordDoubleClickHandler(new ManejadorBotones("mod"));
		getService().numeroRegistrosPersona("tblempleados", objbackI);
		getService().getUserFromSession(callbackUser);
		getService().listarRolCombo(0,20,Asyncallback);
		
	
	}
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			String campo=null;
			if(tabla.equals("Cedula")){
				campo="cedula_ruc";
			}else if(tabla.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(tabla.equals("Razon Social")||tabla.equals("")){
				campo="razonSocial";
			}else if(tabla.equals("Direcci\u00F3n")){
				campo="direccion";
			}
			//validacion de caja de busqueda, debe contener algo a buscar
			 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
				 getService().ListEmpleados(0, registros, objbacklst);
				 //getService().ListJoinPersona("tblproveedors",0,registros, objbacklst);
				 contador = 41;
					lblRegisros.setText(contador+" de "+registros);
					contador = 0;
			}
			 else if(campo.equals("cedula_ruc")||campo.equals("nombreComercial")||campo.equals("razonSocial")||campo.equals("direccion")){
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().findJoin(nombre, "tblempleados", campo, objbacklst); 
		  }
		}catch(Exception e){
			SC.say("Error"+e.getMessage());
		}
			
	}
	public void limpiar(){
		frmPantallaEmpleado.setValue("txtCedula", "");
		frmPantallaEmpleado.setValue("txtNombreComercial", "");
		frmPantallaEmpleado.setValue("txtRazonSocial", "");
		frmPantallaEmpleado.setValue("txtEmail", "");
		frmPantallaEmpleado.setValue("txtObservaciones", "");
		frmPantallaEmpleado.setValue("txtSalario", 0);
		frmPantallaEmpleado.setValue("txtTelefono", "");
		frmPantallaEmpleado.setValue("txtTelefono2", "");
		frmPantallaEmpleado.setValue("txtDireccion", "");
		frmPantallaEmpleado.setValue("txtNivelAcceso", "");
		frmPantallaEmpleado.setValue("password", "");
		frmPantallaEmpleado.setValue("password2", "");
		frmPantallaEmpleado.setValue("txtFechaContratacion", "");
		frmPantallaEmpleado.setValue("cmbrol","");
	}
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers
	}*/
	
	
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		String rol;
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			
			
			if(indicador.equalsIgnoreCase("Modificar")){
				if(frmPantallaEmpleado.validate()){
					String cedula=frmPantallaEmpleado.getItem("txtCedula").getDisplayValue();
				//	CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
						//SC.say("cedula");
						String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
						String telefono="",direccion="",telefono2="";
						nombreComercial=frmPantallaEmpleado.getItem("txtNombreComercial").getDisplayValue();
						razonSocial=frmPantallaEmpleado.getItem("txtRazonSocial").getDisplayValue();
						email=frmPantallaEmpleado.getItem("txtEmail").getDisplayValue();
						observaciones=frmPantallaEmpleado.getItem("txtObservaciones").getDisplayValue();
						telefono=frmPantallaEmpleado.getItem("txtTelefono").getDisplayValue();
						telefono2=frmPantallaEmpleado.getItem("txtTelefono2").getDisplayValue();
						direccion=frmPantallaEmpleado.getItem("txtDireccion").getDisplayValue();
						//SC.say("datos");
						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,telefono2,observaciones,email,'1');
						//SC.say("datos1 ");
						perDTO.setIdPersona(Integer.parseInt(lstEmpleado.getSelectedRecord().getAttribute("idPersona")));
						//SC.say("datos2");
						try {
							//SC.say("contrasena");
							String Contrasenia = frmPantallaEmpleado.getItem("password").getDisplayValue();
							EmpleadoDTO emp=new EmpleadoDTO();
							emp.setIdEmpresa(Factum.empresa.getIdEmpresa());
							emp.setEstablecimiento(Factum.empresa.getEstablecimientos().get(0).getEstablecimiento());
							emp.setIdEmpleado(Integer.parseInt(lstEmpleado.getSelectedRecord().getAttribute("idEmpleado")));
							emp.setHorasExtras(0);
							emp.setIngresosExtras(0);
							emp.setFechaContratacion(Date.valueOf(txtFechaContratacion.getDisplayValue().replace('/','-')));
							emp.setEstado('1');
//							SC.say("rol");
							String rol=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							for (String key:MapRol.keySet()){
								if(rol.equals(MapRol.get(key))){
									emp.setNivelAcceso(Integer.valueOf(key).intValue());
									emp.setIdRol(Integer.valueOf(key).intValue());
									break;
								}
							}
							/*String roldes=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							TblrolDTO rol=new TblrolDTO();
							for(int i=0;i<listrol.size();i++){
								if(listrol.get(i).getDescripcion().equals(roldes)){
									rol=listrol.get(i);
									break;
								}
							}
							rol.setTblempleados(emp);
							emp.setNivelAcceso(rol.getIdRol());*/
							/*if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Administrador")){
								emp.setNivelAcceso(1);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Contabilidad")){
								emp.setNivelAcceso(2);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Ventas")){
								emp.setNivelAcceso(3);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Bodega")){
								emp.setNivelAcceso(4);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Caja")){
								emp.setNivelAcceso(5);
							}*/
//							SC.say("sueldo");
							emp.setSueldo(Float.parseFloat(frmPantallaEmpleado.getItem("txtSalario").getDisplayValue()));
							emp.setPassword(Contrasenia);
							
							//String cargodes=frmPantallaEmpleado.getItem("cmbcargo").getDisplayValue();
			/* --- El cargo se utilizara para el rol por ahora se guarda todos con cargo 100 que es ninguno por lo que 
			cuando se cree el modulo de roles se debera asignar un cargo dentro de ese modulo*/
//							SC.say("cargo");
							CargoDTO cargo=new CargoDTO();
							for(int i=0;i<listCargo.size();i++){
								if(listCargo.get(i).getDescripcion().equals("NINGUNO")){
									cargo=listCargo.get(i);
									break;
								}
							}
							if(chkCliente.getValueAsBoolean()){
								ban=false;
								ClienteDTO cli=new ClienteDTO(Factum.empresa.getIdEmpresa(),perDTO,0, 
										0, "0000", '0');
								perDTO.setClienteU(cli);
								//CupoCredito="5";
							}
//							SC.say("setempl");
							cargo.setTblempleados(emp);
							emp.setTblcargo(cargo);
							emp.setTblpersona(perDTO);
							emp.setTblpersona(perDTO);
							perDTO.setEmpleadoU(emp);
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().modificarPersona(perDTO,objback );
						}catch(Exception e){
							SC.say("Error"+e.getMessage());
						}
					}else{
						SC.say("Cedula incorrecta");
					}
				}	
			}
			else if(indicador.equalsIgnoreCase("Eliminar")){
				if(frmPantallaEmpleado.validate()){
					String cedula=frmPantallaEmpleado.getItem("txtCedula").getDisplayValue();
					//CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					if(valida.validarRucCed(cedula)){
						String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
						String telefono="",direccion="",telefono2="";
						nombreComercial=frmPantallaEmpleado.getItem("txtNombreComercial").getDisplayValue();
						razonSocial=frmPantallaEmpleado.getItem("txtRazonSocial").getDisplayValue();
						email=frmPantallaEmpleado.getItem("txtEmail").getDisplayValue();
						observaciones=frmPantallaEmpleado.getItem("txtObservaciones").getDisplayValue();
						telefono=frmPantallaEmpleado.getItem("txtTelefono").getDisplayValue();
						telefono2=frmPantallaEmpleado.getItem("txtTelefono2").getDisplayValue();
						direccion=frmPantallaEmpleado.getItem("txtDireccion").getDisplayValue();
						SC.say("datos");
						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,telefono2,observaciones,email,'1');
						SC.say("datos1 ");
						perDTO.setIdPersona(Integer.parseInt(lstEmpleado.getSelectedRecord().getAttribute("idPersona")));
						SC.say("datos2");
						try {
							SC.say("contrasena");
							String Contrasenia = frmPantallaEmpleado.getItem("password").getDisplayValue();
							EmpleadoDTO emp=new EmpleadoDTO();
							emp.setIdEmpresa(Factum.empresa.getIdEmpresa());
							emp.setEstablecimiento(Factum.empresa.getEstablecimientos().get(0).getEstablecimiento());
							emp.setIdEmpleado(Integer.parseInt(lstEmpleado.getSelectedRecord().getAttribute("idEmpleado")));
							emp.setHorasExtras(0);
							emp.setIngresosExtras(0);
							emp.setFechaContratacion(Date.valueOf(txtFechaContratacion.getDisplayValue().replace('/','-')));
							emp.setEstado('0');
							SC.say("rol");
							String rol=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							for (String key:MapRol.keySet()){
								if(rol.equals(MapRol.get(key))){
									emp.setNivelAcceso(Integer.valueOf(key).intValue());
									emp.setIdRol(Integer.valueOf(key).intValue());
									break;
								}
							}
							/*String roldes=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							TblrolDTO rol=new TblrolDTO();
							for(int i=0;i<listrol.size();i++){
								if(listrol.get(i).getDescripcion().equals(roldes)){
									rol=listrol.get(i);
									break;
								}
							}
							rol.setTblempleados(emp);
							emp.setNivelAcceso(rol.getIdRol());*/
							/*if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Administrador")){
								emp.setNivelAcceso(1);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Contabilidad")){
								emp.setNivelAcceso(2);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Ventas")){
								emp.setNivelAcceso(3);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Bodega")){
								emp.setNivelAcceso(4);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Caja")){
								emp.setNivelAcceso(5);
							}*/
							SC.say("sueldo");
							emp.setSueldo(Float.parseFloat(frmPantallaEmpleado.getItem("txtSalario").getDisplayValue()));
							emp.setPassword(Contrasenia);
							
							//String cargodes=frmPantallaEmpleado.getItem("cmbcargo").getDisplayValue();
			/* --- El cargo se utilizara para el rol por ahora se guarda todos con cargo 100 que es ninguno por lo que 
			cuando se cree el modulo de roles se debera asignar un cargo dentro de ese modulo*/
							SC.say("cargo");
							CargoDTO cargo=new CargoDTO();
							for(int i=0;i<listCargo.size();i++){
								if(listCargo.get(i).getDescripcion().equals("NINGUNO")){
									cargo=listCargo.get(i);
									break;
								}
							}
							if(chkCliente.getValueAsBoolean()){
								ban=false;
								ClienteDTO cli=new ClienteDTO(Factum.empresa.getIdEmpresa(),perDTO,0, 
										0, "0000", '0');
								perDTO.setClienteU(cli);
								//CupoCredito="5";
							}
							SC.say("setempl");
							cargo.setTblempleados(emp);
							emp.setTblcargo(cargo);
							emp.setTblpersona(perDTO);
							emp.setTblpersona(perDTO);
							perDTO.setEmpleadoU(emp);
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");	
						getService().eliminarPersona(perDTO, objback);
						}catch(Exception e){
							SC.say("Error"+e.getMessage());
						}
					}else{
						SC.say("C�dula incorrecta");
					}
				}
				limpiar();
				
			}else if(indicador.equalsIgnoreCase("Nuevo")){ 
				limpiar();
				 btnGrabar.setDisabled(false) ;
				 btnEliminar.setDisabled(true) ;
				 btnNuevo.setDisabled(false) ;
				 btnModificar.setDisabled(true) ;
				 getService().getUserFromSession(callbackUser);
			}
			else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstEmpleado);
			}
			else if(indicador.equalsIgnoreCase("imprimir")){

				 HStack hStackEspacio = new HStack();
				        hStackEspacio.setSize("100%", "5%");
				   Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Listado de Empleados",hStackEspacio,lstEmpleado};					   
					        Canvas.showPrintPreview(a);  

			}
			else if(indicador.equalsIgnoreCase("grabar")){
				if(frmPantallaEmpleado.validate()){
					String cedula=frmPantallaEmpleado.getItem("txtCedula").getDisplayValue();
					//CValidarDato valida=new CValidarDato();
					ValidarRuc valida = new ValidarRuc();
					
					if(valida.validarRucCed(cedula)){
						String nombreComercial="",razonSocial="",CupoCredito="",MontoCredito="",email="",observaciones="";
						String telefono="",direccion="",telefono2="";
						nombreComercial=frmPantallaEmpleado.getItem("txtNombreComercial").getDisplayValue();
						razonSocial=frmPantallaEmpleado.getItem("txtRazonSocial").getDisplayValue();
						email=frmPantallaEmpleado.getItem("txtEmail").getDisplayValue();
						observaciones=frmPantallaEmpleado.getItem("txtObservaciones").getDisplayValue();
						telefono=frmPantallaEmpleado.getItem("txtTelefono").getDisplayValue();
						telefono2=frmPantallaEmpleado.getItem("txtTelefono2").getDisplayValue();
						direccion=frmPantallaEmpleado.getItem("txtDireccion").getDisplayValue();
						PersonaDTO perDTO=new PersonaDTO(cedula,nombreComercial,razonSocial,direccion,telefono,telefono2,observaciones,email,'1');
						try {
							
							String Contrasenia = frmPantallaEmpleado.getItem("password").getDisplayValue();
							EmpleadoDTO emp=new EmpleadoDTO();
							emp.setIdEmpresa(Factum.empresa.getIdEmpresa());
							emp.setEstablecimiento(Factum.empresa.getEstablecimientos().get(0).getEstablecimiento());
							emp.setHorasExtras(0);
							emp.setIngresosExtras(0);
							emp.setFechaContratacion(Date.valueOf(txtFechaContratacion.getDisplayValue().replace('/','-')));
							emp.setEstado('1');
							//SC.say(String.valueOf(MapRol.size()));
							String rol=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							for (String key:MapRol.keySet()){
								//SC.say(rol);
								if(rol.equals(MapRol.get(key))){
									emp.setNivelAcceso(Integer.valueOf(key).intValue());
									emp.setIdRol(Integer.valueOf(key).intValue());
									break;
								}
							}
							//SC.say(rol);
							//String roldes=frmPantallaEmpleado.getItem("cmbrol").getDisplayValue();
							/*TblrolDTO rol=new TblrolDTO();
							for(int i=0;i<listrol.size();i++){
								if(listrol.get(i).getDescripcion().equals(roldes)){
									rol=listrol.get(i);
									break;
								}
							}
							rol.setTblempleados(emp);
							emp.setTblrol(rol);*/
							
							/*if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Administrador")){
								emp.setNivelAcceso(1);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Contabilidad")){
								emp.setNivelAcceso(2);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Ventas")){
								emp.setNivelAcceso(3);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Bodega")){
								emp.setNivelAcceso(4);
							}else if(frmPantallaEmpleado.getItem("cmbTipoUsuario").getDisplayValue().equalsIgnoreCase("Caja")){
								emp.setNivelAcceso(5);
							}*/
							emp.setSueldo(Float.parseFloat(frmPantallaEmpleado.getItem("txtSalario").getDisplayValue()));
							emp.setPassword(Contrasenia);
							//String cargodes=frmPantallaEmpleado.getItem("cmbcargo").getDisplayValue();
							CargoDTO cargo=new CargoDTO();
							for(int i=0;i<listCargo.size();i++){
								if(listCargo.get(i).getDescripcion().equals("NINGUNO")){
									cargo=listCargo.get(i);
									break;
								}
							}
							cargo.setTblempleados(emp);
							emp.setTblcargo(cargo);
							emp.setTblpersona(perDTO);
							//emp.setTblpersona(perDTO);
							perDTO.setEmpleadoU(emp);
							String Cupo=txtcupoCredito.getDisplayValue();
							String Monto=txtmontoCredito.getDisplayValue();
							//perDTO.setClienteU(new ClienteDTO(perDTO,Double.parseDouble(Monto),Double.parseDouble(Cupo)));
							if(chkCliente.getValueAsBoolean()){
								ban=false;
								ClienteDTO cli=new ClienteDTO(Factum.empresa.getIdEmpresa(),perDTO,Double.parseDouble(Monto), 
										Double.parseDouble(Cupo), "0000", '0');
								perDTO.setClienteU(cli);
								//CupoCredito="5";
							}
							
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().GrabarEmp(perDTO,objback );
							contador=20;
							DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
							getService().listarCargo(0,20, objbacklstCargo);
							contador=20;
						}catch(Exception e){ 
							SC.say("Error"+e.getMessage());
						}
					}else{ 
						SC.say("C�dula incorrecta");
					}
					
				}
			}
			else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,20, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,20, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,20, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(registros-registros%20,20, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,20, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
			}

			else if(indicador.equalsIgnoreCase("buscar")){
				
			}
		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equalsIgnoreCase("buscar")){
					buscarL();
				
			}else if(indicador.equalsIgnoreCase("buscarfrm")){
				String ced=frmPantallaEmpleado.getItem("txtCedula").getDisplayValue();
				PersonaDTO per=new PersonaDTO();
				per.setCedulaRuc(ced);
				getService().buscarPersona(per,1, objbackb);
				
			}
			
		}

		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			frmPantallaEmpleado.setValue("txtRazonSocial",lstEmpleado.getSelectedRecord().getAttribute("RazonSocial"));  
			frmPantallaEmpleado.setValue("txtEmail",lstEmpleado.getSelectedRecord().getAttribute("E-mail"));  
			frmPantallaEmpleado.setValue("txtNombreComercial",lstEmpleado.getSelectedRecord().getAttribute("NombreComercial"));  
			frmPantallaEmpleado.setValue("txtCedula", lstEmpleado.getSelectedRecord().getAttribute("Cedula"));
			frmPantallaEmpleado.setValue("txtObservaciones", lstEmpleado.getSelectedRecord().getAttribute("observaciones"));
			frmPantallaEmpleado.setValue("txtSalario", lstEmpleado.getSelectedRecord().getAttribute("salario"));
			frmPantallaEmpleado.setValue("txtTelefono", lstEmpleado.getSelectedRecord().getAttribute("Telefonos"));
			frmPantallaEmpleado.setValue("txtTelefono2", lstEmpleado.getSelectedRecord().getAttribute("Telefonos2"));
			frmPantallaEmpleado.setValue("txtDireccion", lstEmpleado.getSelectedRecord().getAttribute("Direccion"));
			
			//frmPantallaEmpleado.setValue("cmbrol",lstEmpleado.getSelectedRecord().getAttribute("NivelA"));
			String rol=lstEmpleado.getSelectedRecord().getAttribute("NivelA");
			for (String key:MapRol.keySet()){
				//SC.say(String.valueOf(rol));
				if(rol.equals(MapRol.get(key))){
					//frmPantallaEmpleado.setValue("cmbrol",key);
					cmbrol.setValue(key);
					break;
				}
			}
			//cmbrol.setValue(lstEmpleado.getSelectedRecord().getAttribute("NivelA").trim());
			//frmPantallaEmpleado.setValue("cmbRol",lstEmpleado.getSelectedRecord().getAttribute("NivelA"));
			//frmPantallaEmpleado.setValue("cmbcargo",MapCargo.get(lstEmpleado.getSelectedRecord().getAttribute("idCargo")));
			frmPantallaEmpleado.setValue("cmbFechaContratcion", lstEmpleado.getSelectedRecord().getAttribute("FechaContratacion"));
			frmPantallaEmpleado.setValue("password",lstEmpleado.getSelectedRecord().getAttribute("password"));
			frmPantallaEmpleado.setValue("password2",lstEmpleado.getSelectedRecord().getAttribute("password"));
			
			
			tabSet.selectTab(0);
			
			 btnGrabar.setDisabled(true) ;
			 btnEliminar.setDisabled(false) ;
			 btnNuevo.setDisabled(false) ;
			 btnModificar.setDisabled(false) ;
			 getService().getUserFromSession(callbackUser);
			 //getService().listarRolCombo(0,20,Asyncallback);
			
		}
			
	}
	
	private class FuncionSI implements FormItemIfFunction {  

        public boolean execute(FormItem item, Object value, DynamicForm frmPantallaEmpleado) {
			ban=(Boolean)	frmPantallaEmpleado.getValue("chkCliente");
        	return (Boolean)	frmPantallaEmpleado.getValue("chkCliente");
         }
	}
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			SC.say("Error"+caught.toString());
			
		} 
	@SuppressWarnings("unused")
	
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().ListEmpleados(0,contador, objbacklst);
			getService().numeroRegistrosPersona("tblempleados", objbackI);
			SC.say("Resultado "+result);
			
			
		}
	};
	final AsyncCallback<List<CargoDTO>>  objbacklstCargo=new AsyncCallback<List<CargoDTO>>(){

			public void onFailure(Throwable caught) {
				SC.say("Error"+caught.toString());
			}
			public void onSuccess(List<CargoDTO> result) {
				listCargo=result;
				MapCargo.clear();
				MapCargo.put("","");
	            for(int i=0;i<result.size();i++) {
	            	MapCargo.put(String.valueOf(result.get(i).getIdCargo()), 
							result.get(i).getDescripcion());
				}
				//cmbcargo.setValueMap(MapCargo); 
	       }
			
		};
		final AsyncCallback<List<TblrolDTO>> Asyncallback= new AsyncCallback<List<TblrolDTO>>() {
			@Override
			public void onFailure(Throwable caught) {
				SC.say("ERROR FAILURE: "+caught.toString());
			}
			@Override
			public void onSuccess(List<TblrolDTO> result) {
				// TODO Auto-generated method stub
				listrol=result;
				MapRol.clear();
				//SC.say(String.valueOf(result.size()));
				//MapRol.put("","");
	            for(int i=0;i<result.size();i++) {
	            	MapRol.put(String.valueOf(result.get(i).getIdRol()), 
							result.get(i).getDescripcion());
	            	//SC.say(MapRol.get(i));
				}
	            cmbrol.setValueMap(MapRol);
	            getService().ListEmpleados(0,contador, objbacklst);
			}
		};
		final AsyncCallback<PersonaDTO>  objbackb=new AsyncCallback<PersonaDTO>(){
			public void onFailure(Throwable caught) {
				SC.say("Error"+caught.toString());
			}
			@Override
			public void onSuccess(PersonaDTO result) {
					if(result!=null){
						frmPantallaEmpleado.setValue("txtRazonSocial",result.getRazonSocial());  
						frmPantallaEmpleado.setValue("txtEmail",result.getMail());  
						frmPantallaEmpleado.setValue("txtNombreComercial",result.getNombreComercial());  
						frmPantallaEmpleado.setValue("txtCedula", result.getCedulaRuc());
						frmPantallaEmpleado.setValue("txtObservaciones", result.getObservaciones());
						frmPantallaEmpleado.setValue("txtSalario", result.getEmpleadoU().getSueldo());
						frmPantallaEmpleado.setValue("txtTelefono", result.getTelefono1()+" / "+result.getTelefono2());
						frmPantallaEmpleado.setValue("txtDireccion", result.getDireccion());
						frmPantallaEmpleado.setValue("password",result.getEmpleadoU().getPassword());
						int rol=result.getEmpleadoU().getNivelAcceso();
						for (String key:MapRol.keySet()){
							//SC.say(String.valueOf(rol));
							if(String.valueOf(rol).equals(key)){
								//frmPantallaEmpleado.setValue("cmbrol",MapRol.get(key));
								cmbrol.setValue(key);
								break;
							}
						}
						/*if(result.getEmpleadoU().getNivelAcceso()==1){
							frmPantallaEmpleado.setValue("cmbTipoUsuario","Administrador");
						}else if(result.getEmpleadoU().getNivelAcceso()==2){
							frmPantallaEmpleado.setValue("cmbTipoUsuario","Contabilidad");
						}else if(result.getEmpleadoU().getNivelAcceso()==3){
							frmPantallaEmpleado.setValue("cmbTipoUsuario","Ventas");
						}else if(result.getEmpleadoU().getNivelAcceso()==4){
							frmPantallaEmpleado.setValue("cmbTipoUsuario","Bodega");
						}else if(result.getEmpleadoU().getNivelAcceso()==5){
							frmPantallaEmpleado.setValue("cmbTipoUsuario","Caja");
						}*/
						//frmPantallaEmpleado.setValue("cmbrol",result.getEmpleadoU().getTblrol().getDescripcion());
						//frmPantallaEmpleado.setValue("cmbcargo",result.getEmpleadoU().getTblcargo().getDescripcion());
						txtFechaContratacion.setValue(String.valueOf(result.getEmpleadoU().getFechaContratacion()));
					}else{
						SC.say("Elemento No Encontrado");
					}
				
			}
			
		};
		
		final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(List<PersonaDTO> result) {
				getService().numeroRegistrosPersona("tblempleados", objbackI);
				if(contador>registros){
					lblRegisros.setText(registros+" de "+registros);
				}else
					lblRegisros.setText(contador+" de "+registros);
				
				ListGridRecord[] listado = new ListGridRecord[result.size()];
				for(int i=0;i<result.size();i++) {
					if(result.get(i).getEstado()=='1'){
						listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
						String rol=listado[i].getAttributeAsString("NivelA");
						for (String key:MapRol.keySet()){
							//SC.say(rol+" "+key);
							if(rol.equals(key)){
								listado[i].setAttribute("NivelA", MapRol.get(key));
								break;
							}
						}
					}
				}
				lstEmpleado.setData(listado);
				lstEmpleado.redraw();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		   }
		};
		
		final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
			public void onFailure(Throwable caught) {
				SC.say("Error dado:" + caught.toString());
				
			}
			public void onSuccess(Integer result) {
				registros=result;
			}
		};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
        		//SC.say(String.valueOf(result.getNivelAcceso()));
        		if(result.getNivelAcceso()==1 || result.getNivelAcceso()==2){
        			
        		/*	btnGrabar.setDisabled(false);
        			btnEliminar.setDisabled(false);
        			btnNuevo.setDisabled(false);
        			btnModificar.setDisabled(false);*/
        		}else{
        			btnGrabar.setDisabled(true);
        			btnEliminar.setDisabled(true);
        			btnNuevo.setDisabled(true);
        			btnModificar.setDisabled(true);
        		}
			}
        }

        public void onFailure(Throwable caught) {
            tipoUsuario = 0;
            com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };

}
