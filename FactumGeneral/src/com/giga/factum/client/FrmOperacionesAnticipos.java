package com.giga.factum.client;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.DragAppearance;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FrmOperacionesAnticipos extends VLayout{
//private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

public FrmOperacionesAnticipos()
{
	
}
//++++++++++++++++++++++++++++++FUNCION PARA ASOCIAR ANTICIPOS ++++++++++++++++++++++++++++++
ListGrid grillaActividades = new ListGrid();//Grilla en la cual est�n las actividades
frmReporteCaja listaDocumentos = new frmReporteCaja("Facturas de Venta");
ListGrid grillaAsociar = new ListGrid();//Grilla a la cual se traspasan las asignaturas
final Window winAsActividad = new Window();
Integer idVendedor=0;
String tipPer="";
int numeroRealTransaccion=0;
int tipoTransaccion=0;
int idPersonaSeleccionada=0;
String anticipoDescripcion="";//Para grabar los anticipos que se van a unir
Date fechaFactura;// = new Date();
DateItem dateFecha_emision = new DateItem("fecha_emision","FECHA&nbspEMISI\u00D3N");

NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
public static GreetingServiceAsync getService(){
	return GWT.create(GreetingService.class);
}
public FrmOperacionesAnticipos(String tipoPersona){ //Ventana para asociar Anticipos
	//tipoPersona puede ser "Clientes" o "Proveedores"
	//VGas es para la grilla 1
	getService().fechaServidor(callbackFecha);
	tipPer=tipoPersona;
	VLayout VAs = new VLayout();HLayout HSep0 = new HLayout();
	HLayout HSep1 = new HLayout();
	HLayout HTit = new HLayout();HLayout HTT = new HLayout();
	HLayout HV1 = new HLayout();HLayout HGr = new HLayout();HLayout HSep2 = new HLayout();
	VLayout VGas = new VLayout();HLayout HSep3 = new HLayout();VLayout VSep1 = new VLayout();
	HLayout HSep4 = new HLayout();VLayout VGel = new VLayout();HLayout HSep5 = new HLayout();
	HLayout HBtd = new HLayout();HLayout HSep6 = new HLayout();HLayout HBti = new HLayout();
	HLayout HSep7 = new HLayout();HLayout HBte = new HLayout();HLayout HSep8 = new HLayout();
	HLayout HSep9 = new HLayout();HLayout HSep10 = new HLayout();HLayout HBot = new HLayout();
	HLayout HSep11 = new HLayout();HLayout HSep12 = new HLayout();HLayout HSepF1 = new HLayout();
/*	HLayout HBusc = new HLayout();
	
	DynamicForm Din1 = new DynamicForm();DynamicForm Din2 = new DynamicForm();
	HBusc.setSize("100%", "10%");*/
	DynamicForm dinFecha = new DynamicForm();
	HSepF1.setSize("5%", "100%");
	VAs.setSize("100%", "100%");
	HSep0.setSize("100%", "1%");
	HV1.setSize("100%", "4%");
	HSep1.setSize("31%", "100%");
	HTT.setSize("69%", "100%");
	HTit.setSize("100%", "100%");
	HGr.setSize("100%", "96%");
	  
	HSep2.setSize("2%", "100%");//
	VGas.setSize("46%", "100%");
	HSep3.setSize("1%", "100%");
	VSep1.setSize("2%", "100%");
	HSep4.setSize("1%", "100%");
	VGel.setSize("46%", "100%");
	HSep9.setSize("2%","100%");
	
	HSep5.setSize("100%", "20%");
	HBtd.setSize("100%", "5%");
	HSep6.setSize("100%", "2%");
	HBti.setSize("100%", "5%");
	HSep7.setSize("100%", "2%");
	HBte.setSize("100%", "5%");
	HSep8.setSize("100%", "61%");
	HSep10.setSize("100%", "3%");
	HSep11.setSize("20%", "100%");
	HSep12.setSize("3%", "100%");
	HBot.setSize("80%", "7%");
	dateFecha_emision.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
	dateFecha_emision.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
	dateFecha_emision.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
	dinFecha.setFields(new FormItem[] { 
			dateFecha_emision});
	dinFecha.hide();
	HBot.setMembers(dinFecha);
	Label LbDo = new Label("<h3>Operaciones de Anticipos de "+tipoPersona+"<h3>");
	LbDo.setWidth(300);
	
	getService().getUserFromSession(callbackUser);
	
	//++++++++++++++++++++++++++++GRILLA PARA TRASPASAR LOS ACTIVIDADES ++++++++++++++++++++++++++++++++++++++++++++++
    //la Palabra As al final de cada listGrid es para saber q es la grilla a la q asignamos las actividades
    ListGridField idAs =new ListGridField("id", "id");
    idAs.setHidden(true);
	ListGridField NumRealTransaccionAs =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
	ListGridField TipoTransaccionAs= new ListGridField("TipoTransaccion", "Tipo Transaccion");
	ListGridField FechaAs =new ListGridField("Fecha", "Fecha");
	FechaAs.setHidden(true);
	ListGridField FechadeExpiracionAs=new ListGridField("FechadeExpiracion", "Fechade Expiracion");
	FechadeExpiracionAs.setHidden(true);
	ListGridField VendedorAs=new ListGridField("Vendedor", "Vendedor");
	ListGridField IdVendedorAs=new ListGridField("idVendedor", "idVendedor");
	VendedorAs.setHidden(true);
	IdVendedorAs.setHidden(true);
	ListGridField ClienteAs =new ListGridField("Cliente", "Cliente");
	ListGridField IdClienteAs =new ListGridField("idCliente", "idCliente");
	IdClienteAs.setHidden(true);
	ClienteAs.setHidden(true);
	
	ListGridField AutorizacionAs =new ListGridField("Autorizacion", "Autorizacion");
	AutorizacionAs.setHidden(true);
	ListGridField NumPagosAs =new ListGridField("NumPagos", "Numero de Pagos");
	NumPagosAs.setHidden(true);
	ListGridField EstadoAs =new ListGridField("Estado", "Estado");
//	EstadoAs.setHidden(true);
	EstadoAs.setAlign(Alignment.RIGHT);
	ListGridField SubTotalAs =new ListGridField("SubTotal", "Total");
	//ListGridField SubTotal =new ListGridField("SubTotal", "SubTotal");
	SubTotalAs.setType(ListGridFieldType.FLOAT);
	
	SubTotalAs.setShowGridSummary(true);
	
	SubTotalAs.setSummaryFunction(new SummaryFunction() {

        public Object getSummaryValue(Record[] records, ListGridField field) {
            double sum = 0;
            for (int i = 0; i < records.length; i++) {
                sum +=  Double.valueOf(records[i].getAttribute("SubTotal"));
            }
            if (sum < 0) {
                return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
            } else {
                return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
            }
        }
    });
	
	grillaAsociar.setFields(new ListGridField[] { idAs,NumRealTransaccionAs ,TipoTransaccionAs, FechaAs,FechadeExpiracionAs , VendedorAs,ClienteAs,
			AutorizacionAs,NumPagosAs,SubTotalAs,EstadoAs,IdClienteAs,IdVendedorAs});
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	grillaAsociar.setCanResizeFields(true);
	grillaAsociar.setShowFilterEditor(false);
    grillaAsociar.setEmptyMessage("No existen actividades asociadas aun!"); 
    grillaAsociar.setWidth("100%");  
    grillaAsociar.setHeight("50%"); 
    //grillaAsociar.setAutoFetchData(true); 
    grillaAsociar.setShowRowNumbers(true); 
    grillaAsociar.setCanAcceptDroppedRecords(true);//para que acepte campos arrastrados
    //grillaAsociar.setCanReorderRecords(true);//para que se puedan reordenar las filas
    grillaAsociar.setCanDragRecordsOut(true);//para que se pueda arrastrar hacia la otra grilla
    //grillaAsociar.setCanEdit(true);
    grillaAsociar.setEditEvent(ListGridEditEvent.CLICK);
    grillaAsociar.setDragDataAction(DragDataAction.MOVE);
    grillaAsociar.setShowGridSummary(true);
 /*   grillaAsociar.setCanRemoveRecords(true);
    grillaAsociar.setPreventDuplicates(false);
    grillaAsociar.setValidateOnChange(false);
   */ 
    //GAs1.setCanReorderFields(true);//para que se puedan reordenar los campos de la fila
    
    IButton agAsociacion = new IButton("Unir Anticipos");
	agAsociacion.setSize("30%", "100%");
	agAsociacion.setIcon("Check-icon.png");
	
	agAsociacion.addClickHandler(new ClickHandler(){
   		public void onClick(ClickEvent event){
   			int numAnticipos=0;
   			Double totalAnt=0.0;
   			numAnticipos=grillaAsociar.getRecords().length;
   		if(numAnticipos<2)
   		{
   			SC.say("Debe seleccionar al menos 2 anticipos");
   		}
   		else
   		{	
   			LinkedList<DtocomercialDTO> listAnticipos= new LinkedList<DtocomercialDTO>(); 
   			anticipoDescripcion="Union Anticipos: ";
   			int validarEstado=0;//Para que se pueda unir solo los anticipos que tienen estado confirmado
   			for(int i=0;i<numAnticipos;i++)
   			{
   				if(grillaAsociar.getRecord(i).getAttributeAsString("Estado").compareTo("CONFIRMADO")!=0 || grillaAsociar.getRecord(i).getAttributeAsString("TipoTransaccion").substring(0, 8).toUpperCase().compareTo("ANTICIPO")!=0)
   				{//Si no estan confirmados no permite unir los anticipos
   					validarEstado=i+1;
   					break;
   				}
   				else
   				{
   					anticipoDescripcion=anticipoDescripcion+grillaAsociar.getRecord(i).getAttributeAsString("NumRealTransaccion")+"-";
   					totalAnt=totalAnt+grillaAsociar.getRecord(i).getAttributeAsDouble("SubTotal");

   					Set<DtoComDetalleDTO> DtoDetalles=null;
   	   				DtoDetalles=null; 
   	   		 		//Integer idDto = null;
   	   				int idDTO = 0;
   	   		 		Double subtotal=grillaAsociar.getRecord(i).getAttributeAsDouble("SubTotal");
   	   		 		subtotal=CValidarDato.getDecimal(2, subtotal);
   	   		 		PersonaDTO persona = new PersonaDTO();//Cliente seleccionado
   	   		 		
   	   		 		
   	   		 	//	ERROR: BUSCAR EL ID DE LA PERSONA Y DEL VENDEDOR PARA ASIGNARLE
   	   		 		
   	   		 		persona.setIdPersona(grillaAsociar.getRecord(i).getAttributeAsInt("idCliente"));//Para crear el anticipo a nombre de Giga
   	   		 		
   	   		 		PersonaDTO vendedor = new PersonaDTO();
   	   				vendedor.setIdPersona(grillaAsociar.getRecord(i).getAttributeAsInt("idVendedor"));
   	   				Set<RetencionDTO> tblRetenciones=new HashSet<RetencionDTO>(0);
   	   				int numTransac=grillaAsociar.getRecord(i).getAttributeAsInt("NumRealTransaccion");
   	   				Set<TblpagoDTO> PagosDTO = null;
   	   				Integer Anticipo = null;
   	   				Integer Retencion = null;
   	   				Integer NotaCredito = null;
   	   				Double costo = 0.0;
   	   				idDTO=grillaAsociar.getRecord(i).getAttributeAsInt("id");
   	   				String fechaI=grillaAsociar.getRecord(i).getAttributeAsString("Fecha");
   	   			/*01/2014/13 Exp 01/2014/20
   	   		No agrega verso listAnticipos.size()1
   	   		FechaLixta: 01/2014/07 Exp 01/2014/07
   	   		No agrega verso listAnticipos.size()2
   	   		FechaLixta: 1/21/2014 Exp 1/21/2014*/
   	   				fechaI=fechaI.replace('-', '/');
   	   				String orden[] = fechaI.split("/");
   	 	    		fechaI = orden[0]+"/"+orden[1]+"/"+orden[2];
   	   				String fechaF=grillaAsociar.getRecord(i).getAttributeAsString("FechadeExpiracion");
   	   				fechaF=fechaF.replace('-', '/');
   	   				String orden2[] = fechaF.split("/");
	 	    		fechaF = orden2[1]+"/"+orden2[2]+"/"+orden2[0];
   	   				Set<DtocomercialTbltipopagoDTO> PagosAsociados = new HashSet<DtocomercialTbltipopagoDTO>(0);
   	   		 	//Poner fecha de hoy
   	   			PersonaDTO facturaPor = new PersonaDTO();
				facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
   	   		 		DtocomercialDTO Anticipodto = new DtocomercialDTO(
   	   		 			Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento(),Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision(),
   	   						idDTO,persona,
   	   						vendedor, 
   	   						facturaPor,
   	   						numTransac,
   	   						tipoTransaccion,//Tipo de transaccion
   	   						fechaI,fechaF,
   	   					//grillaAsociar.getRecord(i).getAttributeAsString("Fecha"),
   	   				//grillaAsociar.getRecord(i).getAttributeAsString("FechadeExpiracion"),
   	   						//anticipoDescripcion.substring(0, anticipoDescripcion.length()-1), 0,//O numpagos
   	   					grillaAsociar.getRecord(i).getAttributeAsString("Autorizacion"),0,
   	   						'2',//Confirmar 
   	   						subtotal,
   	   						DtoDetalles, 
   	   						tblRetenciones, //Set tblretencions
   	   						PagosDTO, //Set tblpagos
   	   						null,//Set tblmovimientos
   	   						Anticipo,
   	   						NotaCredito,
   	   						Retencion,
   	   						PagosAsociados,
   	   						costo,
   	   						"",0.0, "",0);
   	   		 		listAnticipos.add(Anticipodto);
   				}	
   			}
   			if(validarEstado==0)//anticipoDescripcion
   			{	
   				idPersonaSeleccionada=grillaAsociar.getRecord(0).getAttributeAsInt("idCliente");
   				Grabar(totalAnt,listAnticipos);
   				
   			}
   			else
   			{
   				SC.say("Solos los ANTICIPOS CONFIRMADOS se pueden unir. Revise el anticipo en la posici&oacute;n "+validarEstado);
   			}
   		}	
   	}});
	
	IButton caAsociacion = new IButton("Dividir Anticipos");
	caAsociacion.setSize("30%", "100%");
	caAsociacion.setIcon("delete1.png");
	caAsociacion.addClickHandler(new ClickHandler(){
   		public void onClick(ClickEvent event){
   			//winAsActividad.destroy();
   			SC.say("Metodo para Dividir los anticipos");
     }});
    //----------------------------------------------------------------------------------------------------------------------
    HTit.addMember(LbDo);
    HTT.addMember(HTit);
    HV1.addMember(HSep1);HV1.addMember(HTT);
	VLayout form = new VLayout();  
    form.setSize("100%","100%"); 
    form.setPadding(5);  
    form.setLayoutAlign(VerticalAlignment.BOTTOM);
           
    
    String tipoDoc="";
    if(tipoPersona=="Clientes")
    {
    	listaDocumentos = new frmReporteCaja("Anticipo Clientes"); 
    	tipoDoc="Anticipo Clientes";
    	tipoTransaccion=4;
    }else
    {
    	listaDocumentos = new frmReporteCaja("Anticipo Proveedores"); 
    	tipoDoc="Anticipo Proveedores";
    	tipoTransaccion=15;
    }
    listaDocumentos.setSize("100%", "100%");
    listaDocumentos.cmbDocumento.setValue(tipoDoc);
    listaDocumentos.cmbDocumento.disable();
    form.setMembers(listaDocumentos);
    listaDocumentos.generarReporte();

    VGas.addMember(form);
    VGel.addMember(grillaAsociar);
    VGel.addMember(HSep10);
    HBot.addMember(HSep11);
    HBot.addMember(agAsociacion);
    HBot.addMember(HSep12);
    HBot.addMember(caAsociacion);
    VGel.addMember(HBot);
    VGel.addMember(HSep10);
    VGel.addMember(HSep10);
    VGel.addMember(HSep10);
    HGr.addMember(HSep3);
    HGr.addMember(VGas);
    HGr.addMember(HSep3);
    HGr.addMember(VGel);
    HGr.addMember(HSep4);
    VAs.addMember(HSep0);
    VAs.addMember(HV1);
    VAs.addMember(HGr);
    //----------------------------------------------------------------------------------------------------------------------
    
    winAsActividad.addCloseClickHandler(new CloseClickHandler() {  
        public void onCloseClick(CloseClientEvent event) {  
        	//idVendedor="";
        	//txtVendedor.setValue("");
        	winAsActividad.destroy(); 
        }   
    });
    winAsActividad.setWidth("80%");
    winAsActividad.setHeight("80%");
    winAsActividad.setTitle("Asociar Actividades");  
    winAsActividad.setShowMinimizeButton(false);//mostrar boton de minimizar
    winAsActividad.setIsModal(true);//para que se bloquee el resto de la pantalla  
    winAsActividad.setShowModalMask(true);//para que se opaque el resto de la pantalla
    winAsActividad.setKeepInParentRect(true);//para que al arrastrar no se salga la ventana de la pantalla
    winAsActividad.centerInPage(); //para que salga centrada la ventana
    winAsActividad.setDragAppearance(DragAppearance.TARGET);  
    winAsActividad.setShowDragShadow(true);
    VAs.show();
    winAsActividad.addItem(VAs);
    winAsActividad.show();
	//return winAsActividad;
}
//+++++++++++++++++++++++++++++++++++++  LISTADO DISTRIBUTIVO  ++++++++++++++++++++++++++++++++++++++++++++++
public void Grabar(Double Subtotal, LinkedList<DtocomercialDTO> listAnticipos)
{
	try
	{
	//	SC.say("lixto1");
		Set<DtoComDetalleDTO> DtoDetalles=null;
		DtoDetalles=null;
 		Subtotal=CValidarDato.getDecimal(2, Subtotal);
 		Integer idDto = null;
 		PersonaDTO persona = new PersonaDTO();//Cliente seleccionado
 //		persona.setIdPersona(2971);//Para crear el anticipo a nombre de Giga
 		persona.setIdPersona(idPersonaSeleccionada);
 		PersonaDTO vendedor = new PersonaDTO();
		vendedor.setIdPersona(idVendedor);
		Integer NumDto = 0;
	//	SC.say("lixto2");
		Set<RetencionDTO> tblRetenciones=new HashSet<RetencionDTO>(0);
		String filtro="where TipoTransaccion =", ordenamiento;
			filtro += tipoTransaccion+"";
		obtener_UltimoDtoFactura(filtro, "numRealTransaccion");
		Set<TblpagoDTO> PagosDTO = null;
		Integer Anticipo = null;
		Integer Retencion = null;
		Integer NotaCredito = null;
		Integer anulacion = 0;
		Double costo = 0.0;
	//	SC.say("lixto3");
		Set<DtocomercialTbltipopagoDTO> PagosAsociados = new HashSet<DtocomercialTbltipopagoDTO>(0);
 	//Poner fecha de hoy
 		if(dateFecha_emision.getSelectorFormat().toString().compareTo("DAY_MONTH_YEAR")==0)
 		{
 		    String fecha=dateFecha_emision.getDisplayValue();
 		    //fecha.replace("00:00"," ");
 		    String orden[] = fecha.split("/");
 	    	//String orden[] = fechaBase[0].split("-");
 	    	String fechaMostrar = orden[2]+"/"+orden[1]+"/"+orden[0];
 		   dateFecha_emision.setValue(fechaMostrar);
 		   //SC.say("si entre al if: "+"la fecha es:"+dateFecha_emision.getDisplayValue());
 		}
 	//	SC.say("lixto4");
 		PersonaDTO facturaPor = new PersonaDTO();
		facturaPor.setIdPersona(Integer.valueOf(Factum.idusuario));
 		DtocomercialDTO dto = new DtocomercialDTO(
 				Factum.empresa.getIdEmpresa(),Factum.user.getEstablecimiento(),Factum.empresa.getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision(),
				idDto,persona,
				vendedor, 
				facturaPor,
				numeroRealTransaccion,
				tipoTransaccion,//Tipo de transaccion 
				dateFecha_emision.getDisplayValue(), 
				dateFecha_emision.getDisplayValue(),//new Date(), //fecha expiracion ????
				anticipoDescripcion.substring(0, anticipoDescripcion.length()-1),0,
				'2',//Confirmar 
				Subtotal,
				DtoDetalles, 
				tblRetenciones, //Set tblretencions
				PagosDTO, //Set tblpagos
				null,//Set tblmovimientos
				Anticipo,
				NotaCredito,
				Retencion,
				PagosAsociados,
				costo,
				"",0.0, "", 0);
 //		SC.say("lixto5");
 		listAnticipos.add(dto);//Para poner en el ultimo registro el anticipo unido
 		grabar(listAnticipos);
 		}catch(Exception e)
 		{
 			SC.say(e.getMessage());
 		}
 }		
//Para obtener el id de el venderor
 final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

 	        public void onSuccess(User result) 
 	        {
 	        	if (result == null) {//La sesion expiro

 					SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

 						public void execute(Boolean value) {
 							if (value) {
 								com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
 								//getService().LeerXML(asyncCallbackXML);
 							}
 						}
 					});
 				}else{
 	        		idVendedor = result.getIdUsuario();}
 	        }

 	        public void onFailure(Throwable caught) {
 	        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
					SC.say("No se puede obtener el nombre de usuario");
 	        }
 	    };

 	   private void obtener_UltimoDtoFactura(String filtro, String campoOrdenamiento) {
	    	//Analizamos de que tipo de documento se trata
	        final AsyncCallback<DtocomercialDTO> Funcioncallback = new AsyncCallback<DtocomercialDTO>() {
	            //String coincidencias = "";
	            public void onSuccess(DtocomercialDTO result) {
	                if (result != null) { 
	                    Integer c= result.getNumRealTransaccion();
	                    c+=1;
	                    numeroRealTransaccion=c;
	                } else {
	                    SC.say("Recargue la pagina por favor");      
	                }
	            }
	            public void onFailure(Throwable caught) {
	                SC.say("Error al acceder al servidor: " + caught);
	            }
	        };
	        getService().ultimoDocumento(filtro, campoOrdenamiento, Funcioncallback);
	        }
 	   
 	   
 	  public void grabar(LinkedList<DtocomercialDTO> listAnticipos){
 			final AsyncCallback<String> Funcioncallback = new AsyncCallback<String>() {
 		            public void onSuccess(String result) {
 		                if (result != null) 
 		                {
 		                	SC.say("Anticipos Unidos Correctamente");		           
 		                }
 		             }   
 		             public void onFailure(Throwable caught) {
 		                SC.say("Error al obtener el id del cliente" + caught);
 		                System.out.println("" + caught);
 		            }
 		        };
 		        getService().grabarAnticipo(listAnticipos, Funcioncallback);
 		        }
 	   
 	 final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
 		public void onFailure(Throwable caught) {
 			SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
 			
 		}
 		public void onSuccess(Date result) {
 			fechaFactura=result;
 			dateFecha_emision.setUseTextField(true);
 			dateFecha_emision.setValue(fechaFactura);
 		}
 	};
 	
 	
}
