package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import oracle.jdbc.aq.AQDequeueOptions.VisibilityOption;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.giga.factum.client.regGrillas.CuentasporCobrarRecords;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.ext.linker.EmittedArtifact.Visibility;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.IButton;

public class frmCuentasPorCobrar extends VLayout{
	ListGrid CuentasPorCobrar = new ListGrid();//crea una grilla
	SearchForm searchForm = new SearchForm();
	Date fechaFactura;
	ComboBoxItem cmbBuscar;
	TextItem txtBuscarLst;
	IButton btnLimpiar;
	IButton btnImprimir;
	IButton btnexportar;
	IButton btnGenerar;
	frmListClientes clientes;
	Window winclientes=new Window();
	PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
	PickerIcon searchPicker2 = new PickerIcon(PickerIcon.DATE);
	DateTimeItem txtFecha ;
	VLayout layout_1 ;//el vertical layout q contendra a los botones y a la grilla
	/*para poner el picker de buscar*/
	HLayout hLayout ;
	 public static GreetingServiceAsync getService()
	 {
		 return GWT.create(GreetingService.class);
	 }
	frmCuentasPorCobrar() {
			getService().fechaServidor(callbackFecha);
			getService().obtenerCuentasPorCobrar(objCuetasPorCobrar);
			try{
				CuentasPorCobrar = new ListGrid() {  
		            @Override  
		            protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {  
		                if (getFieldName(colNum).equals("Estado")) {  
		                	ListGridRecord listrecord =  record;  
		                    if (listrecord.getAttributeAsString("Estado").equals("CANCELADO")) {  
		                        return "font-weight:bold; color:#298a08;"; 
		                    } else if (listrecord.getAttributeAsString("Estado").equals("PENDIENTE")) {  
		                        return "font-weight:bold; color:#287fd6;";  
		                    } else{
		                    	return super.getCellCSSText(record, rowNum, colNum);  
		                    }
		                }
		                else
		                {	
		                	if(getFieldName(colNum).equals("Dias"))
		                	{
		                		ListGridRecord listrecord =  record;
		                		if (listrecord.getAttributeAsString("Dias").equals("--")) 
		                		{  
		                			return "font-weight:bold; color:#FF0000;"; 
			                    } 
		                		else
		                		{
			                    	return super.getCellCSSText(record, rowNum, colNum);  
			                    }
		                	}	
		                	else {  
		                		return super.getCellCSSText(record, rowNum, colNum);  
		                	}
		                }	
		            }  
		        };
			}catch(Exception e ){
				SC.say("Error: "+e);
			}
        layout_1  = new VLayout();
        hLayout= new HLayout();
		hLayout.setSize("100%", "6%");

		/*para poner en un hstack los botones de desplazamiento*/
		txtBuscarLst = new TextItem("txtBuscarLst", "");
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		
		//txtFecha.hide();
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones("listar"));
		txtBuscarLst.setShowHintInField(true);
		searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
		
		txtFecha=new DateTimeItem("txtFecha","Fecha");
		txtFecha.setLeft(6);
		txtFecha.setTop(6);
		txtFecha.setWidth(146);
		txtFecha.setHeight(22);
		txtFecha.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFecha.setMaskDateSeparator("-");
		txtFecha.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFecha.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFecha.setValue(fechaFactura);
		txtFecha.addKeyPressHandler(new ManejadorBotones("buscar2"));
		
		cmbBuscar = new ComboBoxItem("cmbBuscar", "");
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.addChangedHandler(new ManejadorBotones("combo"));
		cmbBuscar.setValueMap("Cliente","Fecha Vencimiento","D\u00edas Vencidos","Fecha de Emisi\u00f3n");
		
		searchForm.setFields(new FormItem[] { txtFecha,txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
        hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		CuentasPorCobrar.setSize("100%", "80%");
		
		ListGridField Cliente = new ListGridField("Cliente", "Cliente");
		
		ListGridField fechadepago = new ListGridField("fechadepago", "Fecha de Pago");
		fechadepago.setAlign(Alignment.CENTER);
		
		ListGridField formadePago = new ListGridField("formadePago", "Forma de Pago");
		formadePago.setAlign(Alignment.CENTER);
		
		ListGridField Factura = new ListGridField("Factura", "Facturas Vencidas");
		Factura.setAlign(Alignment.CENTER);
		ListGridField ValorTotal = new ListGridField("ValorTotal", "Valor Total");
		ValorTotal.setAlign(Alignment.RIGHT);
		ListGridField Dias = new ListGridField("Dias", "D\u00edas Vencidos");
		Dias.setAlign(Alignment.CENTER);
		ListGridField FechaEmision = new ListGridField("FechaEmision", "Fecha de Emisi\u00f3n");
		FechaEmision.setAlign(Alignment.CENTER);
		ListGridField Saldo = new ListGridField("Saldo", "Saldo");
		Saldo.setAlign(Alignment.RIGHT);
		ListGridField Estado = new ListGridField("Estado", "Estado");
		Estado.setAlign(Alignment.CENTER);
		ListGridField impuesto = new ListGridField("impuesto", "impuesto");
		impuesto.setAlign(Alignment.RIGHT);
		ListGridField tipo = new ListGridField("tipo", "tipo");
		tipo.setAlign(Alignment.CENTER);
		
		CuentasPorCobrar.setFields(new ListGridField[] {Cliente,fechadepago,formadePago,Factura,ValorTotal,Dias,FechaEmision,Saldo,Estado});
		//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		layout_1.addMember(CuentasPorCobrar);//para que se agregue el listado al vertical
	    
	    HStack hStack_1 = new HStack();
	    
	    btnGenerar = new IButton("Generar");
	    hStack_1.addMember(btnGenerar);
	    btnGenerar.addClickHandler(new ManejadorBotones("generar"));
	    btnImprimir = new IButton("Imprimir");
	    hStack_1.addMember(btnImprimir);
	    btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
	    
	    btnLimpiar = new IButton("Limpiar");
	    hStack_1.addMember(btnLimpiar);
	    btnLimpiar.addClickHandler(new ManejadorBotones("limpiar"));
	    
	    
	    btnexportar = new IButton("Exportar");
	    hStack_1.addMember(btnexportar);
	    btnexportar.addClickHandler(new ManejadorBotones("exportar"));
	    
	    layout_1.addMember(hStack_1);
	    addMember(layout_1);		
		    
	}
	public void limpiar()
	{
		txtBuscarLst.setValue("");
		txtBuscarLst.show();
		txtFecha.setValue(fechaFactura);
		txtFecha.hide();
		cmbBuscar.setValue("Buscar Por");
		CuentasPorCobrar.setData(new ListGridRecord[]{});
	}
	public void buscar()
	{
		
		String nombre=txtBuscarLst.getDisplayValue().toUpperCase();
		String campo=cmbBuscar.getDisplayValue();
		
		if(campo.equals("Cliente") || campo.equals("")){
			campo="razonSocial";
		}else if(campo.equals("D\u00edas Vencidos")){
			if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals("")))
			{
				SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			else
			{
				campo="Dias";
			}
		}
		if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		}
		limpiar();
		txtBuscarLst.setValue(nombre);
		getService().buscarCuentasCobrar(nombre, campo, objCuetasPorCobrar);
		
		
		DOM.setStyleAttribute(RootPanel.get("Cargando").getElement(), "display", "none");
	}
	public void buscar2()
	{
		
		String nombre=txtFecha.getDisplayValue().toUpperCase();
		String campo=cmbBuscar.getDisplayValue();
		if(campo.equals("Fecha Vencimiento")){
			if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals("")))
			{
				SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			else
			{
			
				campo="fechaVencimiento";
			}
		}/*else if(campo.equals("Valor Total")){
			campo="subtotal";
		}*/else if(campo.equals("Fecha de Emisi\u00f3n")){
			if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals("")))
			{
				SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
			}
			else
			{
				campo="fecha";
			}
		}else if(campo.equals("Buscar Por")){
			campo="nombres";
			}
		 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		}
		limpiar();
		getService().buscarCuentasCobrar(nombre, campo, objCuetasPorCobrar);
		
		
		DOM.setStyleAttribute(RootPanel.get("Cargando").getElement(), "display", "none");
	}
	 private class ManejadorBotones implements FormItemClickHandler, ClickHandler, ChangedHandler, KeyPressHandler{
			String indicador="";
			
			ManejadorBotones(String nombreBoton){
				this.indicador=nombreBoton;
			}

			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				// TODO Auto-generated method stub
			
				String campo=cmbBuscar.getDisplayValue();
				String nombre=txtBuscarLst.getDisplayValue().toUpperCase();
				if(campo.equals("Fecha Vencimiento")||campo.equals("Fecha de Emisi\u00f3n"))
				{
					buscar2();
				}
				else if(campo.equals("Cliente")||campo.equals("D\u00edas Vencidos"))
				{
					buscar();
				}
				else
				{
					if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
						 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
					}
				}
			}

			@Override
			public void onClick(ClickEvent event) {
				
				if(indicador.equals("generar"))
				{
					String campo=cmbBuscar.getDisplayValue();
					String nombre=txtBuscarLst.getDisplayValue().toUpperCase();
					if(campo.equals("Fecha Vencimiento")||campo.equals("Fecha de Emisi\u00f3n"))
					{
						buscar2();
					}
					else if(campo.equals("Cliente")||campo.equals("D\u00edas Vencidos"))
					{
						buscar();
					}
					else
					{
						if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
							 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
						}
					}
				}
				else if(indicador.equals("imprimir"))
				{
					 Object[] a=new Object[]{CuentasPorCobrar};
					 Canvas.showPrintPreview(a);
					
				}
				else if (indicador.equals("limpiar"))
				{
					limpiar();
				}
				else if (indicador.equals("exportar"))
				{
					CreateExelDTO exel=new CreateExelDTO(CuentasPorCobrar);
				}
				
				
			}

			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				if(indicador.equals("combo"))
				{
					String campo=cmbBuscar.getDisplayValue();
					if(campo.equals("Cliente")){
						txtBuscarLst.show();
						txtFecha.hide();
					
					}else if(campo.equals("Fecha Vencimiento")){
						txtFecha.show();
						txtBuscarLst.hide();
					}else if(campo.equals("D\u00edas Vencidos")){
						txtBuscarLst.show();
						txtFecha.hide();
					}else if(campo.equals("Fecha de Emisi\u00f3n")){
						txtFecha.show();
						txtBuscarLst.hide();
					}
				}
				
			}

			@Override
			public void onKeyPress(KeyPressEvent event) {
				// TODO Auto-generated method stub
				if(indicador.equals("listar"))
				{
					String campo=cmbBuscar.getDisplayValue();
					if(campo.equals("Cliente"))	
					{
						if(event.getKeyName().equalsIgnoreCase("space"))
								{
									winclientes = new Window();
									winclientes.setWidth(930);
									winclientes.setHeight(610);
									winclientes.setTitle("Listado Cliente");
									winclientes.setShowMinimizeButton(false);
									winclientes.setIsModal(true);
									winclientes.setShowModalMask(true);
									winclientes.setKeepInParentRect(true);
									winclientes.centerInPage();
									winclientes.addCloseClickHandler(new CloseClickHandler() {
										public void onCloseClick(CloseClientEvent event) {
											winclientes.destroy();
										}
									});
									
									clientes=new frmListClientes("tblclientes");
						    		clientes.setSize("100%","100%");
						    		clientes.lstCliente.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
										
										@Override
										public void onRecordDoubleClick(RecordDoubleClickEvent event) {
											// TODO Auto-generated method stub
											searchForm.setValue("txtBuscarLst",clientes.lstCliente.getSelectedRecord().getAttribute("RazonSocial"));  
											clientes.tabSet.selectTab(0);
											winclientes.destroy();
										}
									});
						    		winclientes.addItem(clientes);
									winclientes.show();
								}
							if(event.getKeyName().equalsIgnoreCase("enter"))
							{
								buscar();
							}
					}
					else 
					{
						if(event.getKeyName().equalsIgnoreCase("enter"))
						{
							buscar();
						}
					}
				}
				if (indicador.equals("buscar2"))
				{
					if(event.getKeyName().equalsIgnoreCase("enter"))
					{
						buscar2();
					}
				}
				
			}
			
	 }
	 
	 final AsyncCallback<List<DtocomercialDTO>>  objCuetasPorCobrar = new AsyncCallback<List<DtocomercialDTO>>()
		{
			public void onFailure(Throwable caught) 
			{
				SC.say(caught.toString()+ "error");
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(List<DtocomercialDTO> resultCuentas) 
			{
				if (resultCuentas.size()>0)
				{	
					for(int i=0;i<resultCuentas.size();i++) 
					{		
						Iterator it =resultCuentas.get(i).getTblpagos().iterator();
						CuentasporCobrarRecords cuentasporcobrar = new CuentasporCobrarRecords((DtocomercialDTO)resultCuentas.get(i));	
						String facturaVencida="";
						Date fechav;
						int tipotransaccion=0;
						String estado="";
//						double impuesto=0;
						tipotransaccion=resultCuentas.get(i).getTipoTransaccion();
						
						long fecha;
						while(it.hasNext())
						{
							Iterator it2 =resultCuentas.get(i).getTbldtocomercialdetalles().iterator();
							double valor2=0;
							double totalvalor2=0;
							double saldo=0;
							double saldo1=0;
							double valor1=0;
							double resultado=0;
							double saldototal=0;
							double valorrestante=0;
							
							TblpagoDTO tblpagosdto=(TblpagoDTO) it.next();
							estado=String.valueOf(tblpagosdto.getEstado());
							fechav=tblpagosdto.getFechaVencimiento();
							saldo=tblpagosdto.getValor();
							
							while(it2.hasNext())
							{
								DtoComDetalleDTO detcom = (DtoComDetalleDTO) it2.next();
								
								String impuestos="";
								double impuestoPorc=1.0;
								double impuestoValor=0.0;
								int i1=0;
								//com.google.gwt.user.client.Window.alert("impuestos de detalle "+detcom.getTblimpuestos().size());
								for (DtoComDetalleMultiImpuestosDTO detalleImp: detcom.getTblimpuestos()){
									i1=i1+1;
									impuestos+=detalleImp.getPorcentaje().toString();
									impuestos= (i1<detcom.getTblimpuestos().size())?impuestos+",":impuestos; 
									impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
									impuestoValor=impuestoValor+((detcom.getCantidad()*detcom.getPrecioUnitario()*(1-(detcom.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
								}
								//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
								
//								impuesto=detcom.getImpuesto();
								valor1=valor1+detcom.getTotal();
								if((impuestoPorc-1)==0.0)
								{
									valor2=valor1;
								}
								else
								{
									if(tipotransaccion==2)
									{
										valor2=valor1;
									}
									else
//										valor2=valor1*(1+(Factum.banderaIVA/100));
//										valor2=valor2+(detcom.getTotal()*(1+(impuestoValor)));
										valor2=valor2+(detcom.getTotal()*(impuestoPorc));
								}
								
							}
							
							if(saldo!=0&&estado.equals("1"))
							{
								saldo1=valor2-saldo;
								if(saldo1<=0)
								{
									saldototal=0;
								}
								else{ 
										saldototal=saldo1;
									}
							}
							else{
								saldototal=saldo;
							}
							
							
							fecha=fechaFactura.getTime()-fechav.getTime();
							double dias = Math.floor(fecha / (1000 * 60 * 60 * 24));
							if(valor2!=0)
							{
								cuentasporcobrar.setCliente(resultCuentas.get(i).getTblpersonaByIdPersona().getRazonSocial());
//								cuentasporcobrar.setCliente(resultCuentas.get(i).getTblpersonaByIdPersona().getNombreComercial()+" "+resultCuentas.get(i).getTblpersonaByIdPersona().getRazonSocial());
								cuentasporcobrar.setfechaRealPago(tblpagosdto.getFechaRealPago());
								cuentasporcobrar.setformadePago(tblpagosdto.getFormaPago());
								cuentasporcobrar.setfacturasVencidas(tblpagosdto.getFechaVencimiento().toString());
								cuentasporcobrar.setvalorTotal(valor2);
								cuentasporcobrar.setsaldo(saldototal);
								cuentasporcobrar.setdiasVencidos(dias,saldototal);
								cuentasporcobrar.setestado(estado,saldototal);
//								cuentasporcobrar.setimpuesto(impuesto);
								cuentasporcobrar.settipo(resultCuentas.get(i).getTipoTransaccion());
								CuentasPorCobrar.addData(cuentasporcobrar);
							}
						}
					}

				}
			
				else
				{
					SC.say("No hay retenciones que cumplan los parametros establecidos");
				}
				
				
		        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		        
		        CuentasPorCobrar.show();
		        CuentasPorCobrar.redraw();
		       
			}
		}; 
		final AsyncCallback<Date>  callbackFecha=new AsyncCallback<Date>(){
			public void onFailure(Throwable caught) {
				SC.say("No es posible cargar la fecha automaticamente, por favor seleccion la fecha");
				
			}
			public void onSuccess(Date result) {
				fechaFactura=result;
			}
		};
		
}

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
