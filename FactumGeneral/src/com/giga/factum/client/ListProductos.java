package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.gwtext.client.widgets.form.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.events.CloseClickHandler;  
import com.smartgwt.client.widgets.events.CloseClientEvent;  
import com.smartgwt.client.widgets.layout.HStack;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.Producto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.types.Alignment;

public class ListProductos extends VLayout{
	TabSet tabSet = new TabSet();
	SearchForm searchForm = new SearchForm();
	ListGrid lstProductos = new ListGrid();
	List<BodegaDTO> listBodega=null;
	List<TipoPrecioDTO> listTipoP=null;
	int contador=20;
	int registros=0;
	Label lblRegistrosProductos = new Label("# Registros");
	
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	VLayout layout_1 = new VLayout();
	Window winFactura1 = new Window(); 
	LinkedHashMap<Integer, ProductoDTO> listadoProductos=null;
	ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",350); 
	public ListProductos() {
		
		lstProductos=new ListGrid();   
        lstProductos.setShowRecordComponents(true);          
        lstProductos.setShowRecordComponentsByCell(true);  
        lstProductos.setCanRemoveRecords(true);  
        lstProductos.setShowAllRecords(true); 
        
		getService().numeroRegistrosProducto("tblproducto", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));
		PickerIcon NuevaMarca = new PickerIcon(new Picker("database_add.png"));
		NuevaMarca.addFormItemClickHandler(new ManejadorBotones("GrabarMarca"));
		PickerIcon NuevaCategoria = new PickerIcon(new Picker("database_add.png"));
		NuevaCategoria.addFormItemClickHandler(new ManejadorBotones("GrabarCategoria"));
		PickerIcon NuevaUnidad = new PickerIcon(new Picker("database_add.png"));
		NuevaUnidad.addFormItemClickHandler(new ManejadorBotones("GrabarUnidad"));
		setSize("850", "300");
		FloatRangeValidator floatRangeValidator = new FloatRangeValidator();
		tabSet.setSize("100%", "100%");
		
		
		Tab tabListado = new Tab("Listado de Productos");
		tabSet.addTab(tabListado);
		
		layout_1.setSize("100%", "100%");
		
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n","Categoria"
				,"Unidad","Marca");
		cmbBuscar.addChangedHandler(new ManejadorBotones("Bodega"));
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstProductos.setSize("100%", "75%");
		lstProductos.setAutoFitData(Autofit.VERTICAL);  
		lstProductos.setAutoFitMaxRecords(10);  
		lstProductos.setAutoFetchData(true);  
		
		
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstProductos.addRecordDoubleClickHandler( new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        buscarPicker.addFormItemClickHandler(new ManejadorBotones(""));
        ListGridField lstidproducto = new ListGridField("idProducto", "Id Producto",100);
        lstidproducto.setCellAlign(Alignment.LEFT);
        lstidproducto.setHidden(true);
        ListGridField lstcodbarras = new ListGridField("codigoBarras", "C\u00F3digo de Barras",100);
        lstcodbarras.setCellAlign(Alignment.LEFT);
        
        ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",100);
        lstimpuesto.setCellAlign(Alignment.CENTER);
        ListGridField lstlifo = new ListGridField("lifo", "LIFO",60);
        lstlifo.setCellAlign(Alignment.CENTER);
        ListGridField lstfifo = new ListGridField("fifo", "FIFO",60);
        lstfifo.setCellAlign(Alignment.CENTER);
        ListGridField lstpromedio = new ListGridField("promedio", "Costo",60);
        lstpromedio.setCellAlign(Alignment.RIGHT);
        ListGridField lstunidad = new ListGridField("Unidad", "Unidad",100);
        ListGridField listGridField_6 = new ListGridField("stock", "Stock",50);
        listGridField_6.setCellAlign(Alignment.CENTER);
        ListGridField lstcategoria =new ListGridField("Categoria", "Categoria",100);
        lstcategoria.setCellAlign(Alignment.CENTER);
        ListGridField lstMarca= new ListGridField("Marca", "Marca",100);
        lstMarca.setCellAlign(Alignment.CENTER);
        ListGridField lstPVP= new ListGridField("PVP", "PVP",60);
        lstPVP.setCellAlign(Alignment.RIGHT);
        ListGridField buttonField = new ListGridField("buttonField", "Series",100); 
        lstProductos.setFields(new ListGridField[] { 
        		lstidproducto,
        		lstcodbarras, 
    			lstdescripcion,
    			listGridField_6,
    			lstimpuesto,
    			lstpromedio,
    			lstPVP,
    			});
        lstProductos.setShowHiddenFields(false);
    	layout_1.addMember(lstProductos);
    	HStack hStack_1 = new HStack();
    	hStack_1.setSize("100%", "5%");
    	layout_1.addMember(hStack_1);
    	layout_1.addMember(lblRegistrosProductos);
    	lblRegistrosProductos.setSize("100%", "4%");
    	tabListado.setPane(layout_1);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listaProductos(0, 20, 1,0,-1,Factum.banderaStockNegativo,listaCallback); 
		getService().listarTipoprecio(0,100,objbacklstTip );
		addMember(tabSet);
		
		
    	 
	}
	public void buscarL(){
		int ban=1;
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String tabla=searchForm.getItem("cmbBuscar").getDisplayValue();
		String campo=null;
		if(tabla.equals("C\u00F3digo de Barras")){
			ban=1;
			tabla="codigoBarras";
		}else if(tabla.equals("Descripci\u00F3n")||tabla.equals("")){
			ban=1;
			tabla="descripcion";
		}else if(tabla.equals("Marca")){
			ban=0;
			campo="marca";
			tabla="tblmarca";
		}else if(tabla.equals("Categoria")){
			ban=0;
			campo="categoria";
			tabla="tblcategoria";
		}else if(tabla.equals("Unidad")){
			ban=0;
			campo="nombre";
			tabla="tblunidad";
		}
		//validacion de caja de busqueda, debe contener algo a buscar
		 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equals(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		}
		 else if(tabla.equals("tblmarca")||tabla.equals("tblunidad")||tabla.equals("tblcategoria")||tabla.equals("tblunidad")||tabla.equals("descripcion")||tabla.equals("codigoBarras")){
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		
		if(ban==1){
			getService().listarProductoLike(nombre, tabla, listaCallback);
		}else if(ban==0){
			getService().listarProductoJoin(nombre, tabla, campo,1, 0, -1,Factum.banderaStockNegativo,listaCallback);
		}
	   }
	}
	
	private class ManejadorBotones implements ClickHandler,RecordDoubleClickHandler,KeyPressHandler,FormItemClickHandler,ChangedHandler{
		String indicador="";
		float lifo=0,fifo=0,promedio=0,stock=0,impuesto=0;
		String marca,codBarras,cat,unidad;
		String descripcion,Tipo,idPro;
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductos(contador-20,contador,1,0,-1,Factum.banderaStockNegativo,listaCallback);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductos(contador-20,contador,1,0,-1,Factum.banderaStockNegativo,listaCallback);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductos(contador-20,contador,1,0,-1,Factum.banderaStockNegativo,listaCallback);
					
				}
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductos(registros-registros%20,registros,1,0,-1,Factum.banderaStockNegativo,listaCallback);
				}
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductos(contador-20,contador,1,0,-1,Factum.banderaStockNegativo,listaCallback);
					lblRegistrosProductos.setText(contador+" de "+registros);
			}else if(indicador.equalsIgnoreCase("inventario")){
				getService().ValorDelInventario(objfloat);
			}
	
			
		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}

			
			
		}

		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			if(indicador.equalsIgnoreCase("Buscar")){ 
				buscarL();
			}
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			
			
			
		}

		@Override
		public void onChanged(ChangedEvent event) {
						
		}
	}
	
	final AsyncCallback<Float>  objfloat=new AsyncCallback<Float>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(Float result) {
			SC.say("El valor del inventario es: "+String.valueOf(result));
			getService().listaProductos(0, 20,1,0,-1,Factum.banderaStockNegativo,listaCallback);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
			
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			SC.say(result);
			getService().listaProductos(0, 20,1,0,-1,Factum.banderaStockNegativo,listaCallback);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
	};
	
	final AsyncCallback<List<TipoPrecioDTO>>  objbacklstTip=new AsyncCallback<List<TipoPrecioDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<TipoPrecioDTO> result) {
			listTipoP=result;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		
	};

	final AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(LinkedHashMap<Integer, ProductoDTO> result) {
			listadoProductos=null;
			listadoProductos=result;
			getService().numeroRegistrosProducto("tblproducto", objbackI);
			if(contador>registros){
				lblRegistrosProductos.setText(registros+" de "+registros);
			}else{
				lblRegistrosProductos.setText(contador+" de "+registros);
			}
			
			RecordList listado = new RecordList();
			Iterator iter = result.values().iterator();
            while (iter.hasNext()) {
            	Producto pro =new Producto((ProductoDTO)iter.next());
            	listado.add(pro);
            }
            lstProductos.setData(new RecordList());
            lstProductos.setData(listado);
            lstdescripcion.setSortDirection(SortDirection.ASCENDING);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
			//SC.say("Numero de registros "+registros);
		}
	};
	private class FuncionSI implements FormItemIfFunction {
        public boolean execute(FormItem item, Object value, SearchForm searchForm) {
        	boolean ban=false;
        	//if(cmbBuscar.getSelectOnFocus()){
        		if(cmbBuscar.getDisplayValue().equals("Ubicaci\u00F3n")){
        			ban= true;
        		}else{
        			ban= false;
        		}
        	//}
			return ban;
         }

		@Override
		public boolean execute(FormItem item, Object value, DynamicForm form) {
			// TODO Auto-generated method stub
			return false;
		}
	}
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
