package com.giga.factum.client;


import com.smartgwt.client.widgets.tile.TileGrid;
import com.smartgwt.client.widgets.viewer.DetailViewerField;

public class TileGridMesas extends TileGrid{
	 public TileGridMesas(){
		    setFields(new DetailViewerField("idMesa"),new DetailViewerField("Mesa"));
	        setWidth100();  
	        setHeight100();
	        setTileWidth(150);  
	        setTileHeight(50);  
	 }
	 

}
