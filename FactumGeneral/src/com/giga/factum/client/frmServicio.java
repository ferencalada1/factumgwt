package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.VLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gwtext.client.widgets.form.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.events.CloseClickHandler;  
import com.smartgwt.client.widgets.events.CloseClientEvent;  
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.ProductoElaboradoDTO;
import com.giga.factum.client.DTO.ProductoTipoPrecioDTO;
import com.giga.factum.client.DTO.TblmultiImpuestoDTO;
import com.giga.factum.client.DTO.TblproductoMultiImpuestoDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.MultiImpuestoRecords;
import com.giga.factum.client.regGrillas.Producto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;

import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tile.TileGrid;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangeEvent;
import com.smartgwt.client.widgets.grid.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.fields.FloatItem;
import com.smartgwt.client.types.Alignment;


public class frmServicio extends VLayout{
	int bandera=0;

	TabSet tabSet = new TabSet();
	SearchForm searchForm = new SearchForm();
	ListGrid lstProductos = new ListGrid();
	ListGrid lstMultiImp = new ListGrid();
	List<BodegaDTO> listBodega=null;
	List<TipoPrecioDTO> listTipoP=null;
	int contador=20;
	int registros=0;
	Integer tipoUsuario=0;
	TextItem txtid=new TextItem("txtid", "Id");
	TextItem txtCodBarras = new TextItem("txtCodBarras", "C\u00F3digo de Barras");
	FloatItem txtPromedio = new FloatItem();
	FloatItem txtDescuento = new FloatItem();
	Label lblRegisros = new Label("");
	//TextItem txtRegistros = new TextItem ("");
	int StockDeseado=2;//Para seleccionar Todos=2, SinStock=0, ConStock=1
	int filtroEliminados=0;//para los eliminados, estado=0
	DynamicForm dynamicForm;
	DynamicForm dynamicFormColumnas;
	final ComboBoxItem cmbCategoria=new ComboBoxItem("cbmCategoria","Categor\u00EDa");
	final ComboBoxItem cmbUnidad=new ComboBoxItem("cbmUnidad","Unidad");
	final ComboBoxItem cmbMarca=new ComboBoxItem("cbmMarca","Marca");
	final SelectItem cmdBod = new SelectItem("cmbBod", "Bodega");
//	final SelectItem cmbImpuesto = new SelectItem("cmbImpuesto", "Impuesto");	
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	List<CategoriaDTO> listcat=null;
	DynamicForm dinReg = new DynamicForm();
	List<MarcaDTO> listmar=null;
	List<UnidadDTO> listuni=null;
	List<TblmultiImpuestoDTO> listmult=null;
	ProductoDTO productoRestaura=new ProductoDTO();
	int op=2;
	String nombreHost;
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapImpuesto = new LinkedHashMap<String, String>();
	
	Button btnEliminar;
	 TransferImgButton btnInicio ;
	
	VLayout layout_2 = new VLayout();
	Window winFactura1 = new Window(); 
	Button btnGrabar = new Button("Grabar");
	IButton btnAgregarABodega  = new IButton("Agregar a Bodega");
	IButton btnModificar = new IButton("Modificar");
	LinkedHashMap<Integer, ProductoDTO> listadoProductos=null;
	ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",350); 


	
	
	VLayout layoutvcuerpo= new VLayout();
	frmLstBodPre frmlstbodpre;

	public frmServicio() {
		
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarCategoriaCombo(0,1000,objbacklstCat);
		getService().listarUnidadCombo(0,2000,objbacklstUnidad);
		getService().listarMarcaCombo(0,1000,objbacklstMarca);
		getService().listarMultiImpuesto(0, 100, multicallback);
		getService().listarBodega(0,100, objbacklstBod);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block"); 
		
		bandera=1;
		//getService().listarProductoLike2("", "descripcion", 2, lista20);
		//Funcion para extraer el nivel de acceso del usuario
		getService().getUserFromSession(callbackUser);
		getService().obtenernombreHost(callbackString);
		//addMember(tabSet);
		//lblRegisros.setText(contador+" de "+registros); 
	}
	public void cargarImpuestos(){
		frmlstbodpre.lstTipoPrecio.saveAllEdits();
		frmlstbodpre.lstTipoPrecio.refreshFields();
		Set<TblproductoMultiImpuestoDTO>  prodImpuesto= new HashSet(0);
		//com.google.gwt.user.client.Window.alert("impuestos records ");
		for (ListGridRecord record:lstMultiImp.getRecords()){
			if (record.getAttributeAsBoolean("checkBox")){
				TblproductoMultiImpuestoDTO prodMult= new TblproductoMultiImpuestoDTO();
				TblmultiImpuestoDTO mult = new TblmultiImpuestoDTO(record.getAttributeAsInt("idImpuestoGrid"),
						record.getAttributeAsInt("porcentajeGrid"),record.getAttributeAsString("impuestoGrid"),record.getAttributeAsString("tipoImpuestoGrid").charAt(0));
				prodMult.setTblmultiImpuesto(mult);
//							prodMult.setTblproducto(pro);
				prodImpuesto.add(prodMult);
			}
    	  }
		//com.google.gwt.user.client.Window.alert("Impuestos:  "+prodImpuesto.size());
		frmlstbodpre.producto.setTblmultiImpuesto(prodImpuesto);
		frmlstbodpre.ActualizarVal();
	}
	public void cargarProducto(){
		dynamicFormColumnas = new DynamicForm();//BORRAR
		lstProductos=new ListGrid() {  
            @Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {             	//final int col=colNum;
                String fieldName = this.getFieldName(colNum);  
                if (fieldName.equals("buttonField")) {    
                    IButton button = new IButton();  
                    button.setHeight(18);  
                    button.setWidth(65);                      
                    button.setTitle("Series");  
                    button.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) { 
                        	winFactura1 = new Window();
                        	winFactura1.setWidth("90%");  
                        	winFactura1.setHeight("80%");   
                        	winFactura1.setTitle("Series");  
                        	winFactura1.setShowMinimizeButton(false);  
                        	winFactura1.setIsModal(true);  
                        	winFactura1.setShowModalMask(true);  
                        	winFactura1.centerInPage();  
        					//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
                        	winFactura1.addCloseClickHandler(new CloseClickHandler() {  
        		                public void onCloseClick(CloseClientEvent event) {  
        		                	winFactura1.destroy();  
        		                }  
        		            });
                        	ProductoDTO prod=new ProductoDTO();
                        	prod.setIdEmpresa(Factum.empresa.getIdEmpresa());
							prod.setEstablecimiento(Factum.getEstablecimientoCero());
                        	prod.setIdProducto((lstProductos.getSelectedRecord().getAttributeAsInt("idProducto")));
                        	frmSerie serie=new frmSerie(prod,1);
        					SC.say(String.valueOf(lstProductos.getSelectedRecord().getAttribute("idDocumento")));
        					VLayout form = new VLayout();  
        		            form.setSize("100%","100%"); 
        		            form.setPadding(5);  
        		            form.setLayoutAlign(VerticalAlignment.BOTTOM);
        		            form.addMember(serie);
        		            winFactura1.addItem(form);
        		            winFactura1.show();
                        }  
                    });  
                    return button;  
                } else {  
                    return null;  
                }  
            }  
        };  
		
        lstProductos.setShowRecordComponents(true);          
        lstProductos.setShowRecordComponentsByCell(true);  
        lstProductos.setCanRemoveRecords(true);  
        lstProductos.setShowAllRecords(true);
        lstProductos.setShowRowNumbers(true);
       // lstProductos.setShowGridSummary(true);
        
		//getService().numeroRegistrosProducto("tblproducto", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));
		PickerIcon NuevaMarca = new PickerIcon(new Picker("database_add.png"));
		NuevaMarca.addFormItemClickHandler(new ManejadorBotones("GrabarMarca"));
		PickerIcon NuevaCategoria = new PickerIcon(new Picker("database_add.png"));
		NuevaCategoria.addFormItemClickHandler(new ManejadorBotones("GrabarCategoria"));
		PickerIcon NuevaUnidad = new PickerIcon(new Picker("database_add.png"));
		NuevaUnidad.addFormItemClickHandler(new ManejadorBotones("GrabarUnidad"));
		setSize("90%", "95%");
		//setSize("850", "300");
		FloatRangeValidator floatRangeValidator = new FloatRangeValidator();
		tabSet.setSize("100%", "100%");
		
		Tab tab = new Tab("Ingreso Servicios");
		VLayout layout = new VLayout();
		layout.setSize("100%", "100%");
		dynamicForm = new DynamicForm();
		dynamicForm.setSize("100%", "90%");
		dynamicForm.setMinColWidth(50);
		dynamicForm.setItemLayout(FormLayoutType.TABLE);
		dynamicForm.setWidth100();
		
		dynamicForm.setNumCols(4);
		TextItem txtDescripcion = new TextItem("txtDescripcion","Descripci\u00F3n");
		txtDescripcion.setColSpan(0);
		txtDescripcion.setTabIndex(1);
		txtDescripcion.setLeft(133);
		txtDescripcion.setTop(34);
		txtDescripcion.setRequired(true);
		txtDescripcion.setWidth(400);
		
		txtCodBarras.setTabIndex(0);
		txtCodBarras.setLength(30);
		txtCodBarras.setRequired(true);
		
		/*if(tipoUsuario!=1) {
		   txtPromedio.setDisabled(false);		   
		  }else{		   
		   txtPromedio.setDisabled(true);
		  }*/
		txtPromedio.setTitle("Costo");
		txtPromedio.setName("txtPromedio");
		txtPromedio.setRequired(true);
		txtPromedio.addChangedHandler(new Manejador("cambioCosto"));
		txtPromedio.setValidators(floatRangeValidator);
		txtPromedio.setValue(0.00);
		txtPromedio.setTabIndex(2);
		
		
//		if(tipoUsuario!=1) {
//			txtDescuento.setDisabled(false);		   
//		  }else{		   
//			  txtDescuento.setDisabled(true);
//		  }
		txtDescuento.setTitle("Descuento");
		txtDescuento.setName("txtDescuento");
		txtDescuento.setRequired(true);
		txtDescuento.setValidators(floatRangeValidator);
		txtDescuento.setValue(0.00);
		txtDescuento.setTabIndex(3);
				
		
				
		cmbMarca.setTabIndex(5);
		cmbMarca.setRequired(true);
		cmbMarca.setType("comboBox");
		cmbMarca.setIcons(NuevaMarca);
		cmbCategoria.setTabIndex(2);
		
		cmbCategoria.setTabIndex(4);		
		cmbCategoria.setRequired(true);
		cmbCategoria.setIcons(NuevaCategoria);
		
		cmbUnidad.setTabIndex(6);
		cmbUnidad.setRequired(true);
		cmbUnidad.setIcons(NuevaUnidad);
		
		
//		cmbImpuesto.setTabIndex(5);
//		cmbImpuesto.setRequired(true);
//		cmbImpuesto.setValueMap(String.valueOf(Factum.banderaIVA),"0");
//		cmbImpuesto.setDefaultToFirstOption(true);
//		cmbImpuesto.addChangedHandler(new Manejador("cmbImpuesto"));
		
//		cmbImpuesto.setDefaultToFirstOption(true);
		
		FloatItem txtLIFO = new FloatItem();
		txtLIFO.setDisabled(true);
		txtLIFO.setRequired(true);
		txtLIFO.setTitle("LIFO");
		txtLIFO.setName("txtLIFO");
		txtLIFO.setValidators(floatRangeValidator);
		txtLIFO.setValue(0.00);
		
		FloatItem txtFIFO = new FloatItem();
		txtFIFO.setDisabled(true);
		txtFIFO.setName("txtFIFO");
		txtFIFO.setRequired(true);
		txtFIFO.setTitle("FIFO");
		txtFIFO.setValidators(floatRangeValidator);
		txtFIFO.setValue(0.00);
		
		
		
		
		//#################################
        txtid.setDisabled(true);

        
        HLayout layouthcabecera= new HLayout();
        
        layouthcabecera.setWidth("100%");
        //layoutvcuerpo.setWidth("100%");
		dynamicForm.setFields(new FormItem[] {txtid, cmbCategoria,txtCodBarras, cmbMarca, txtDescripcion, cmbUnidad,  txtLIFO, 
				/*cmbImpuesto,*/ txtPromedio, txtFIFO, txtDescuento});
		layouthcabecera.addMember(dynamicForm);
		layouthcabecera.addMember(lstMultiImp);
		layouthcabecera.setAlign(Alignment.CENTER);
		layout.addMember(layouthcabecera);
	
		frmlstbodpre=new frmLstBodPre();
		frmlstbodpre.setAlign(Alignment.CENTER);
		frmlstbodpre.setVisible(true);
		frmlstbodpre.setSize("100%", "30%");
		
		frmlstbodpre.setDefaultProd();
//        if(tipoUsuario!=1)
//        {
//        	frmlstbodpre.setVisible(false);
//        }
//        layout.addMember(frmlstbodpre);
		
		Canvas canvas = new Canvas();
		canvas.setSize("100%", "10%");
		
		btnEliminar = new Button("Eliminar");
		btnEliminar.setDisabled(true);
		canvas.addChild(btnEliminar);
		btnEliminar.moveTo(324, 6);
		layout.addMember(canvas);
		btnAgregarABodega.setDisabled(true);
		btnAgregarABodega.addClickHandler(new ManejadorBotones("bodega"));
		//canvas.addChild(btnAgregarABodega);
		btnAgregarABodega.setRect(430, 6, 113, 22);
		
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(6, 6);
		btnGrabar.setSize("100px", "22px");
		
		Button btnNuevo = new Button("Nuevo");
		canvas.addChild(btnNuevo);
		btnNuevo.moveTo(112, 6);
		
		btnModificar.setDisabled(true);
		canvas.addChild(btnModificar);
		btnModificar.moveTo(218, 6);
//		tab.setPane(layout);
//		tabSet.addTab(tab);
		
		Tab tabListado = new Tab("Listado");
		
		
		layout_2.setSize("100%", "100%");
		
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		//cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n","Categoria","Unidad","Marca");//Version basica
		//cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n", "Categoria","Unidad","Marca");//Version basica
		
		if(Factum.banderaMenuBodega==1){
			cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n", "Categoria","Unidad","Marca", "Ubicaci\u00F3n");//Version basica
			}else if(Factum.banderaMenuBodega==0){
			cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n", "Categoria","Unidad","Marca");//Version basica
			}
		
		cmdBod.setLeft(321);
		cmdBod.setTop(6); 
		cmdBod.setHint("Seleccione una ");
		cmdBod.setVisible(false);
		cmdBod.addChangedHandler(new ManejadorBotones("Buscar"));
		cmbBuscar.addChangedHandler(new ManejadorBotones(""));
		/*
		radioStock.setColSpan("*");  //Para que el titulo se ponga en una linea
       	radioStock.setVertical(false);  
       	if(Factum.banderaProduccionProductos==0){
       	radioStock.setValueMap("Todos", "Con Stock", "Sin Stock","Eliminados");
       	}
       	else{
       	radioStock.setValueMap("Todos", "Con Stock", "Sin Stock","Eliminados", "Elaborados");	
       	}
       	radioStock.setShowTitle(false);
       	radioStock.setDefaultValue("Todos");
       	*/
       	DynamicForm DContenedor = new DynamicForm();
       	DContenedor.setSize("40%", "100%");
		searchForm.setFields(new FormItem[] {txtBuscarLst, cmbBuscar, cmdBod});
		//txtRegistros.disable();
		//DContenedor.setItems(radioStock);
		hLayout.addMember(DContenedor);
		//dinReg.setFields(txtRegistros);
		hLayout.addMember(searchForm);
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
      //  btnInicio.setDisabled(true);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_2.addMember(hLayout);
		lstProductos.setSize("100%", "75%");
		lstProductos.setAutoFitData(Autofit.VERTICAL);  
		lstProductos.setAutoFitMaxRecords(10);  
		lstProductos.setAutoFetchData(true);  
		
		
       
        ListGridField[]  listgridfield = new ListGridField[12+listTipoP.size()];
        //1
        ListGridField lstcodbarras = new ListGridField("codigoBarras", "C\u00F3digo de Barras",100);
        lstcodbarras.setCellAlign(Alignment.LEFT);        
        listgridfield[0]=lstcodbarras;
        //1
        //ListGridField lstdesc = new ListGridField("codigoBarras", "C\u00F3digo de Barras",100);
        lstcodbarras.setCellAlign(Alignment.LEFT);        
        listgridfield[1]=lstdescripcion; 
        //2
        ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",100);
        lstimpuesto.setCellAlign(Alignment.CENTER);
        listgridfield[2]=lstimpuesto;
        //3
        ListGridField lstpromedio = new ListGridField("promedio", "Costo",60);
        lstpromedio.setCellAlign(Alignment.RIGHT);
        listgridfield[3]=lstpromedio;
        //4
        ListGridField lststock = new ListGridField("stock", "Stock",50);
        lststock.setCellAlign(Alignment.CENTER);
        listgridfield[4]=lststock;
        //10-*
        int i = 5;
        ListGridRecord[] listado = new ListGridRecord[listTipoP.size()];
        ListGridRecord record;
        for(TipoPrecioDTO tipopreciodto:listTipoP){
        	ListGridField lstPrecio = new ListGridField(tipopreciodto.getTipoPrecio().toUpperCase(), tipopreciodto.getTipoPrecio().toUpperCase(),110);
        	lstPrecio.setCellAlign(Alignment.RIGHT);
        	listgridfield[i]=lstPrecio;
        	record = new ListGridRecord();
        	record.setAttribute("PrecioV", 0.0);
        	record.setAttribute("Porcentaje",0.0);
        	record.setAttribute("PrecioVSI",0.0);
        	record.setAttribute("idTipoPrecio", tipopreciodto.getIdTipoPrecio().intValue());
        	record.setAttribute("TipoPrecio",tipopreciodto.getTipoPrecio());
        	record.setAttribute("PrecioC", 0.0);
        	listado[i-5]=record;
        	i=i+1;	
        }
        frmlstbodpre.lstTipoPrecio.setData(listado);
        if(tipoUsuario!=1)
        {
        	txtPromedio.setVisible(false);
        	frmlstbodpre.setVisible(false);
        	lstpromedio.setHidden(true);
        }
        layout.addMember(frmlstbodpre);
        layout.addMember(canvas);
        tab.setPane(layout);
      //3
        ListGridField lstlifo = new ListGridField("lifo", "LIFO",60);
        lstlifo.setCellAlign(Alignment.CENTER);
        listgridfield[i]=lstlifo;
        lstlifo.setHidden(true);
        i=i+1;
        
        //4
        ListGridField lstfifo = new ListGridField("fifo", "FIFO",60);
        lstfifo.setCellAlign(Alignment.CENTER);
        lstfifo.setHidden(true);
        listgridfield[i]=lstfifo;
        i=i+1;
        
        //6
        ListGridField lstunidad = new ListGridField("Unidad", "Unidad",100);
        lstunidad.setCellAlign(Alignment.CENTER);
        listgridfield[i]=lstunidad;
        
        //8 
        i=i+1;
        ListGridField lstcategoria =new ListGridField("Categoria", "Categoria",120);
        lstcategoria.setCellAlign(Alignment.CENTER);
        listgridfield[i]=lstcategoria;
        //9
        i=i+1;
        ListGridField lstMarca= new ListGridField("Marca", "Marca",120);
        lstMarca.setCellAlign(Alignment.CENTER);
        listgridfield[i]=lstMarca;
        i=i+1;
        /*
        ListGridField lstPVP= new ListGridField("PVP", "PVP",60)
        lstPVP.setCellAlign(Alignment.CENTER)
        ListGridField lstAfiliado= new ListGridField("Afiliado", "Afiliado",60)
        lstAfiliado.setCellAlign(Alignment.CENTER)
        ListGridField lstMayorista= new ListGridField("Mayorista", "Mayorista",60)
        lstMayorista.setCellAlign(Alignment.CENTER)
        */
        //*-10
        ListGridField lstImagen= new ListGridField("Imagen", "Imagen",40);
        lstImagen.setCellAlign(Alignment.CENTER);
        listgridfield[i]=lstImagen;
        
        ListGridField buttonField = new ListGridField("buttonField", "Series",100);
        
        lstProductos.setFields(listgridfield);
        //lstpromedio.setType(ListGridFieldType.FLOAT);
        //lstpromedio.setShowGridSummary(true);
    	layout_2.addMember(lstProductos);
    	layout_2.addMember(lblRegisros);
    	HStack hStack_1 = new HStack();
    	hStack_1.setSize("100%", "4%");
    	IButton btnExportar = new IButton("Exportar");
    	btnExportar.addClickHandler(new ManejadorBotones("exportar"));
    	IButton btnExportarPDF = new IButton("Exportar PDF");
    	btnExportarPDF.addClickHandler(new ManejadorBotones("exportarpdf"));
    	IButton btnValorDelInventario = new IButton("Inventario valorizado");
    	IButton btnImprimir = new IButton("Imprimir");
    	btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
    	btnValorDelInventario.addClickHandler(new ManejadorBotones("inventario"));
    	hStack_1.addMember(btnValorDelInventario);
    	hStack_1.addMember(btnExportar);
    	if(Factum.banderaCatalogoProductos==1){
        	hStack_1.addMember(btnExportarPDF);
        	}
    	hStack_1.addMember(btnImprimir);
    	layout_2.addMember(dinReg);
    	lblRegisros.setSize("100%", "4%");
    	
    	btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("Modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("Eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstProductos.addRecordDoubleClickHandler( new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        buscarPicker.addFormItemClickHandler(new ManejadorBotones(""));
        
        
    	layout_2.addMember(hStack_1);
    	tabListado.setPane(layout_2);
//		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//		getService().listarCategoriaCombo(0,1000,objbacklstCat);
//		getService().listarUnidadCombo(0,2000,objbacklstUnidad);
//		getService().listarMarcaCombo(0,1000,objbacklstMarca);
//		getService().listarBodega(0,100, objbacklstBod);
//		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
//		getService().listarTipoprecio(0,100,objbacklstTip ); 
		
		bandera=1;
		//getService().listarProductoLike2("", "descripcion", 2, lista20);
		//Funcion para extraer el nivel de acceso del usuario
//		getService().getUserFromSession(callbackUser);
//		getService().obtenernombreHost(callbackString);
		tabSet.addTab(tab);
		tabSet.addTab(tabListado);
		
		addMember(tabSet);
		//lblRegisros.setText(contador+" de "+registros); 
	}
	public void buscarL(){
		bandera=1;
		int ban=1;
		contador=20;
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String tabla=searchForm.getItem("cmbBuscar").getDisplayValue();
		String campo=null;
		if(tabla.equals("Ubicaci\u00F3n")){
			ban=3;
			tabla="codigoBarras";
		}else if(tabla.equals("C\u00F3digo de Barras")){
			ban=1;
			tabla="codigoBarras";
		}else if(tabla.equals("Descripci\u00F3n") ||tabla.equals("")||tabla.equals("&nbsp;")){
			ban=1;
			tabla="descripcion";
		}else if(tabla.equals("Marca")){
			ban=0;
			campo="marca";
			tabla="tblmarca";
		}else if(tabla.equals("Categoria")){
			ban=0;
			campo="categoria";
			tabla="tblcategoria";
		}else if(tabla.equals("Unidad")){
			ban=0;
			campo="nombre";
			tabla="tblunidad";
		}
		/*
		if(radioStock.getValue().toString()=="Todos"){
				StockDeseado=2;
				op=2;//OP sirve para las opciones de filtrado al momento de usar los botones direccionales
		}else if(radioStock.getValue().toString()=="Con Stock")
		{		StockDeseado=1;
		        op=1;
		}else if(radioStock.getValue().toString()=="Sin Stock"){
				StockDeseado=0;
				op=0;
		}else if(radioStock.getValue().toString()=="Eliminados"){
			filtroEliminados=3;//para el filtro de eliminados (0 inicialmente, 3 cuando se ha pulsado esta opcion)
			op=3;
		}else if(radioStock.getValue().toString()=="Elaborados"){
			StockDeseado=3;
		}
		*/
		//validando que la busqueda no tenga valores ilogicos o vacios en el combo o caja de busqueda
		StockDeseado=2;
        if(filtroEliminados==3){
        	getService().listarProductoLikeEliminados(nombre, tabla,filtroEliminados, listaCallback);
        	filtroEliminados=0;
        }
        else{
        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
    		if(ban==1){
    			getService().listarProductoLike2(nombre, tabla,StockDeseado, 2,0, -1,Factum.banderaStockNegativo,listaCallback);
    		}else if(ban==0){
    			getService().listarProductoJoin2(nombre, tabla, campo,StockDeseado, 2,0,-1,Factum.banderaStockNegativo,listaCallback);
    		}else if(ban==3){
    			String nombreBodega=(Factum.banderaBodegaUbicacion==1)?cmdBod.getDisplayValue().split("/")[0]
    					:cmdBod.getDisplayValue();
    			getService().listarProductoJoin2(nombreBodega, "tblbodegas", nombre,StockDeseado,2,0,-1,Factum.banderaStockNegativo,listaCallback);
    			//getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,StockDeseado,2,0,-1,Factum.banderaStockNegativo,listaCallback);
    		}	
        }
		
	}
	
	public void limpiar(){
		dynamicForm.setValue("txtid","");
		dynamicForm.setValue("txtCodBarras","");
		dynamicForm.setValue("txtDescripcion","");
		dynamicForm.setValue("txtLIFO","0");
		dynamicForm.setValue("txtFIFO","0");
		dynamicForm.setValue("txtPromedio","0");
		//dynamicForm.setValue("cmbImpuesto",String.valueOf(Factum.banderaIVA));
		dynamicForm.setValue("cmbCategoria","");
		dynamicForm.setValue("cmbUnidad","");
		dynamicForm.setValue("cmbMarca","");
		dynamicForm.setValue("txtStock","0");
		dynamicForm.setValue("cmbTipoPrecio","");
		ListGridRecord[] listado = new ListGridRecord[listTipoP.size()];
        ListGridRecord record;
        int i=0;
        for(TipoPrecioDTO tipopreciodto:listTipoP){
        	record = new ListGridRecord();
        	record.setAttribute("PrecioV", 0.0);
        	record.setAttribute("Porcentaje",0.0);
        	record.setAttribute("PrecioVSI",0.0);
        	record.setAttribute("idTipoPrecio", tipopreciodto.getIdTipoPrecio().intValue());
        	record.setAttribute("TipoPrecio",tipopreciodto.getTipoPrecio());
        	record.setAttribute("PrecioC", 0.0);
        	listado[i]=record;
        	i=i+1;	
        }
        frmlstbodpre.lstTipoPrecio.setData(listado);
	}
	private class ManejadorBotones implements ClickHandler,RecordDoubleClickHandler,KeyPressHandler,FormItemClickHandler,ChangedHandler{
		String indicador="";
		float lifo=0,fifo=0,promedio=0,impuesto=0;
		String marca,codBarras,cat,unidad;
		String descripcion,Tipo,idPro, imagen;
		
		public void datosfrm(){
			idPro=dynamicForm.getItem("txtid").getDisplayValue();
			codBarras=dynamicForm.getItem("txtCodBarras").getDisplayValue();
			descripcion=dynamicForm.getItem("txtDescripcion").getDisplayValue();
			fifo=Float.parseFloat(dynamicForm.getItem("txtFIFO").getDisplayValue());
//			impuesto=Float.parseFloat((String)dynamicForm.getItem("cmbImpuesto").getValue());
			
			lifo=Float.parseFloat(dynamicForm.getItem("txtLIFO").getDisplayValue());
			promedio=Float.parseFloat(dynamicForm.getItem("txtPromedio").getDisplayValue());
			cat=(String) dynamicForm.getItem("cbmCategoria").getValue();
			unidad=(String) dynamicForm.getItem("cbmUnidad").getValue();
			marca=(String) dynamicForm.getItem("cbmMarca").getValue();

		}

		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					//SC.say("Entro a grabar 1");
					ProductoDTO pro=new ProductoDTO();
					try {
						datosfrm();
						//dynamicForm.setValue("txtCantidadUnidad", "Entro a grabar 2");
						pro.setIdEmpresa(Factum.empresa.getIdEmpresa());
						pro.setEstablecimiento(Factum.getEstablecimientoCero());
						pro.setTblcategoria(Integer.parseInt(cat));
						pro.setTblunidad(Integer.parseInt(unidad));
						pro.setTblmarca(Integer.parseInt(marca));
						pro.setCodigoBarras(codBarras);
						pro.setDescripcion(descripcion);
						pro.setEstado('1');
						pro.setFifo((fifo));
//						pro.setImpuesto((impuesto));
						pro.setLifo((lifo));
						pro.setPromedio((promedio));
						pro.setStock(0);
						Set<TblproductoMultiImpuestoDTO>  prodImpuesto= new HashSet(0);
						for (ListGridRecord record:lstMultiImp.getRecords()){
							if (record.getAttributeAsBoolean("checkBox")){
								TblproductoMultiImpuestoDTO prodMult= new TblproductoMultiImpuestoDTO();
								TblmultiImpuestoDTO mult = new TblmultiImpuestoDTO(record.getAttributeAsInt("idImpuestoGrid"),
										record.getAttributeAsInt("porcentajeGrid"),record.getAttributeAsString("impuestoGrid"),record.getAttributeAsString("tipoImpuestoGrid").charAt(0));
								prodMult.setTblmultiImpuesto(mult);
	//							prodMult.setTblproducto(pro);
								prodImpuesto.add(prodMult);
							}
				    	  }
						pro.setTblmultiImpuesto(prodImpuesto);
						String mensaje="";
						List<BodegaDTO> bodegas=new ArrayList<BodegaDTO>();
						if (Factum.banderaBodegaDefecto==1){
							bodegas.add(listBodega.get(0));
						}
						pro.setTblbodega(bodegas);
						//pro.setTblbodega(listBodega);
						
//						String mensaje="";
						int j=0,i=0,k=0,count=0;
						j=frmlstbodpre.lstTipoPrecio.getRecords().length;
						for(i=0;i<j;i++){
							if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
								count++;
							} 
						}
						ProductoTipoPrecioDTO[] proTip = new ProductoTipoPrecioDTO[count];
						k=0;
						for(i=0;i<j;i++){
							if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
								ProductoTipoPrecioDTO pt =new ProductoTipoPrecioDTO();
								pt.setTblproducto(pro);
								TipoPrecioDTO tipo = new TipoPrecioDTO();
								tipo.setIdEmpresa(Factum.empresa.getIdEmpresa());
								tipo.setIdTipoPrecio(Integer.parseInt(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("idTipoPrecio")));
								tipo.setTipoPrecio(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("TipoPrecio"));
								tipo.setIdEmpresa(Factum.user.getIdEmpresa());
								pt.setTbltipoprecio(tipo);
								pt.setPorcentaje(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttributeAsDouble("PrecioV"));

								
								proTip[k]=pt;
								mensaje=mensaje+" "+proTip[k].getTbltipoprecio().getTipoPrecio()+" ";
								k=k+1;
							}
						}
						if(count==0){
							proTip=null;
						}
						final ProductoTipoPrecioDTO[] proTipF=proTip;
						final int countF=count;
						//getService().grabarProductoTipoPrecio(proTipF, countF,frmlstbodpre.objback);
						List<ProductoTipoPrecioDTO> protipprec= Arrays.asList(proTipF);
						pro.setProTip(protipprec);
						pro.setTbltipoprecio(listTipoP);
						pro.setTblbodega(listBodega);
						
						
						pro.setImagen(imagen);
						pro.setTipo('2');
						cat=(String) dynamicForm.getItem("cbmCategoria").getDisplayValue();
						unidad=(String) dynamicForm.getItem("cbmUnidad").getDisplayValue();
						marca=(String) dynamicForm.getItem("cbmMarca").getDisplayValue();
						for(i=0;i<listcat.size();i++){
							if(listcat.get(i).getCategoria().equals(cat)){
								pro.setTblcategoria(listcat.get(i).getIdCategoria());
								break;
							}
						}
						for(i=0;i<listmar.size();i++){
							if(listmar.get(i).getMarca().equals(marca)){
								pro.setTblmarca(listmar.get(i).getIdMarca());
								break;
							}
						}
						for(i=0;i<listuni.size();i++){
							if(listuni.get(i).getUnidad().equals(unidad)){
								pro.setTblunidad(listuni.get(i).getIdUnidad());
								break;
							}
						}			
						pro.setJerarquia('0');
						getService().grabarProducto(pro, objbackGrabar);
						
						productoRestaura=pro;	
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");	
						
						contador=20;
						btnAgregarABodega.setDisabled(true);
						
								
					}catch(Exception e){
						SC.say(e.getMessage()+"Error al recuperar los datos del from");
					}	
				}
			}
			else if(indicador.equalsIgnoreCase("Eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value) {
                        	datosfrm();
            				ProductoDTO pro=new ProductoDTO();
            				pro.setIdEmpresa(Factum.empresa.getIdEmpresa());
							pro.setEstablecimiento(Factum.getEstablecimientoCero());
            				pro.setCodigoBarras(codBarras);
            				getService().eliminarProducto(pro, objback);
            				getService().listaProductos(0, 20,2,0,-1,Factum.banderaStockNegativo,listaCallback);
            				btnAgregarABodega.setDisabled(true);
            				limpiar();
            			} else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
			}else if(indicador.equalsIgnoreCase("nuevo")){
				btnGrabar.setDisabled(false);
				btnModificar.setDisabled(true);
				btnEliminar.setDisabled(true);
				
				
				//dynamicForm.setValue("cmbImpuesto", String.valueOf(Factum.banderaIVA));
				dynamicForm.setValue("cbmCategoria","");
				dynamicForm.setValue("cbmUnidad","");
				dynamicForm.setValue("cbmMarca","");
				
				btnAgregarABodega.setDisabled(true);
				limpiar();
			/*}else if(indicador.equalsIgnoreCase("bodega")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					try { 
						datosfrm(); 
						getService().listarCategoriaCombo(0,1000,objbacklstCat);
						getService().listarUnidadCombo(0,2000,objbacklstUnidad);
						getService().listarMarcaCombo(0,1000,objbacklstMarca);
						ProductoDTO pro=new ProductoDTO();
						String id=String.valueOf(dynamicForm.getValue("txtid"));
						
						pro.setIdProducto(Integer.parseInt(id));
						getService().buscarProducto(id+"", "idProducto", retornopro);
					
		        		
						//
					}catch(Exception e){
						SC.say(e.getMessage()+" Seleccione un Producto de la Lista");
					}
				}*/
				//btnAgregarABodega.setDisabled(true);
			}else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					try {
						Set<TblproductoMultiImpuestoDTO>  prodImpuesto= new HashSet(0);
						//TblproductoMultiImpuestoDTO[] prodMultiImpuestos= new TblproductoMultiImpuestoDTO[lstMultiImp.getRecords().length];
						int cont=0;
						for (ListGridRecord record:lstMultiImp.getRecords()){
							if (record.getAttributeAsBoolean("checkBox")){
								TblproductoMultiImpuestoDTO prodMult= new TblproductoMultiImpuestoDTO();
								TblmultiImpuestoDTO mult = new TblmultiImpuestoDTO(record.getAttributeAsInt("idImpuestoGrid"),
										record.getAttributeAsInt("porcentajeGrid"),record.getAttributeAsString("impuestoGrid"),record.getAttributeAsString("tipoImpuestoGrid").charAt(0));
								prodMult.setTblmultiImpuesto(mult);
//								com.google.gwt.user.client.Window.alert("Modificar prodmulti impuesto "+);
	//							prodMult.setTblproducto(pro);
								//prodMultiImpuestos[cont]=prodMult;
								cont++;
								prodImpuesto.add(prodMult);
							}
				    	  }
						if (prodImpuesto.size()>0){
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().listarCategoriaCombo(0,1000,objbacklstCat);
						getService().listarUnidadCombo(0,2000,objbacklstUnidad);
						getService().listarMarcaCombo(0,1000,objbacklstMarca);
						datosfrm();
						ProductoDTO pro=new ProductoDTO();
						pro.setIdEmpresa(Factum.empresa.getIdEmpresa());
						pro.setEstablecimiento(Factum.getEstablecimientoCero());
						pro.setIdProducto(Integer.parseInt(idPro));
						pro.setCodigoBarras(codBarras);
						pro.setDescripcion(descripcion);
						pro.setEstado('1');
						pro.setFifo((fifo));
//						pro.setImpuesto((impuesto));
						pro.setLifo((lifo));
						pro.setPromedio((promedio));
						pro.setStock(0);
						pro.setImagen((imagen));
						pro.setTipo('2');
						cat=(String) dynamicForm.getItem("cbmCategoria").getDisplayValue();
						unidad=(String) dynamicForm.getItem("cbmUnidad").getDisplayValue();
						marca=(String) dynamicForm.getItem("cbmMarca").getDisplayValue();
						for(int i=0;i<listcat.size();i++){
							if(listcat.get(i).getCategoria().equals(cat)){
								pro.setTblcategoria(listcat.get(i).getIdCategoria());
								break;
							}
						}
						for(int i=0;i<listmar.size();i++){
							if(listmar.get(i).getMarca().equals(marca)){
								pro.setTblmarca(listmar.get(i).getIdMarca());
								break;
							}
						}
						for(int i=0;i<listuni.size();i++){
							if(listuni.get(i).getUnidad().equals(unidad)){
								pro.setTblunidad(listuni.get(i).getIdUnidad());
								break;
							}
						}
						
						getService().modificarProducto(pro, objback);
						//getService().listaProductos(0, 20, listaCallback);
						
						String mensaje="";
						int j=0,i=0,k=0,count=0;
						j=frmlstbodpre.lstTipoPrecio.getRecords().length;
						for(i=0;i<j;i++){
							if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
								count++;
							} 
						}
						ProductoTipoPrecioDTO[] proTip = new ProductoTipoPrecioDTO[count];
						k=0;
						for(i=0;i<j;i++){
							if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
								ProductoTipoPrecioDTO pt =new ProductoTipoPrecioDTO();
								pt.setTblproducto(pro);
								TipoPrecioDTO tipo = new TipoPrecioDTO();
								tipo.setIdEmpresa(Factum.empresa.getIdEmpresa());
								tipo.setIdTipoPrecio(Integer.parseInt(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("idTipoPrecio")));
								tipo.setTipoPrecio(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("TipoPrecio"));
								pt.setTbltipoprecio(tipo);
								pt.setPorcentaje(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttributeAsDouble("PrecioV"));

								
								proTip[k]=pt;
								mensaje=mensaje+" "+proTip[k].getTbltipoprecio().getTipoPrecio()+" ";
								k=k+1;
							}
						}
						if(count==0){
							proTip=null;
						}
						final ProductoTipoPrecioDTO[] proTipF=proTip;
						final int countF=count;
						cont=0;
						TblproductoMultiImpuestoDTO[] prodMultiImpuestos= new TblproductoMultiImpuestoDTO[prodImpuesto.size()];
						//com.google.gwt.user.client.Window.alert("Prod "+prodMultiImpuestos.length);
						for (TblproductoMultiImpuestoDTO prodm:prodImpuesto){
							//com.google.gwt.user.client.Window.alert("Prod "+pro.getIdProducto()+" "+prodm.getTblmultiImpuesto().getIdmultiImpuesto());
							prodm.setTblproducto(pro);
							prodMultiImpuestos[cont]=prodm;
							cont++;
						}
						//com.google.gwt.user.client.Window.alert("Prod "+pro.getIdProducto()+" "+prodMultiImpuestos[0].getTblmultiImpuesto().getIdmultiImpuesto()+
						//		" "+prodMultiImpuestos.length);
						getService().grabarProductoMultiImpuesto(prodMultiImpuestos,multback);
						getService().grabarProductoTipoPrecio(proTipF, countF,frmlstbodpre.objback);
						//
					}else{
						SC.say("Seleccion al menos un impuesto");
					}
					}catch(Exception e){
						SC.say(e.getMessage()+" Seleccione un Producto de la Lista");
					}
				}
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductosConFiltro(contador-20,contador,op, listaCallback);
					//lblRegisros.setText(contador+" de "+registros);
					//btnInicio.setDisabled(false);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductosConFiltro(contador-20,contador, op,listaCallback);
					
				}
				//SC.say("boton izq"+contador);
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductosConFiltro(contador-20,contador,op, listaCallback);
					//lblRegisros.setText(contador+" de "+registros);
					//SC.say("boton derecho"+contador);
				}
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductosConFiltro(registros-registros%20,registros, op,listaCallback);
					//lblRegisros.setText(contador+" de "+registros);
				}
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listaProductosConFiltro(contador-20,contador,op, listaCallback);
					//lblRegisros.setText(contador+" de "+registros);
					
			}else if(indicador.equalsIgnoreCase("inventario")){
				getService().ValorDelInventario(objfloat);
			}
			else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstProductos);
			}else if(indicador.equalsIgnoreCase("imprimir")){
				
				//Object[] a=new Object[]{lstProductos};
				VLayout espacio = new VLayout();  
                espacio.setSize("100%","5%");    
				Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +				  
				   		"&nbsp;&nbsp;&nbsp;Listado de Productos",espacio,lstProductos};					   
				
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				Canvas.showPrintPreview(a);
		      DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}

	
			
		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}

			
			
		}

		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			if(indicador.equalsIgnoreCase("GrabarMarca")){
				final Window winMarca = new Window();  
				winMarca.setWidth(930);  
				winMarca.setHeight(610);  
				winMarca.setTitle("Ingresar Marca");  
				winMarca.setShowMinimizeButton(false);  
				winMarca.setIsModal(true);  
				winMarca.setShowModalMask(true);  
				winMarca.centerInPage();  
				winMarca.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winMarca.destroy();
                    	getService().listarMarcaCombo(0,1000,objbacklstMarca);
                		
                    }  
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                frmMarca frmmarca=new frmMarca();
                frmmarca.setSize("100%","100%"); 
                form.addMember(frmmarca);
                winMarca.addItem(form);
                winMarca.show();
			}
			else if(indicador.equalsIgnoreCase("GrabarUnidad")){
				final Window winUnidad = new Window();  
				winUnidad.setWidth(930);  
				winUnidad.setHeight(610);  
				winUnidad.setTitle("Ingresar Unidad");  
				winUnidad.setShowMinimizeButton(false);  
				winUnidad.setIsModal(true);  
				winUnidad.setShowModalMask(true);  
				winUnidad.centerInPage();  
				winUnidad.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winUnidad.destroy();  
                    	getService().listarUnidad(0,1000,objbacklstUnidad);
                		
                    }  
                });   
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                frmUnidad frmunidad=new frmUnidad();
                frmunidad.setSize("100%","100%"); 
                form.addMember(frmunidad);
                winUnidad.addItem(form);
                winUnidad.show();
			}
			else if(indicador.equalsIgnoreCase("GrabarCategoria")){
				final Window winCategoria = new Window();  
				winCategoria.setWidth(930);  
				winCategoria.setHeight(610);  
				winCategoria.setTitle("Ingresar Categor\u00EDa");  
				winCategoria.setShowMinimizeButton(false);  
				winCategoria.setIsModal(true);  
				winCategoria.setShowModalMask(true);  
				winCategoria.centerInPage();  
				winCategoria.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {  
                    	winCategoria.destroy(); 
                    	getService().listarCategoria(0,1000,objbacklstCat);
                		
                    }  
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                frmCategoriaa frmcategoria=new frmCategoriaa();
                frmcategoria.setSize("100%","100%"); 
                form.addMember(frmcategoria);
                winCategoria.addItem(form);
                winCategoria.show();
			}if(indicador.equalsIgnoreCase("")){ 
				buscarL();
			}
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			//vuelve a cargar todos los items de los combos, no solo 20
			getService().listarCategoriaCombo(0,1000,objbacklstCat);
			getService().listarUnidadCombo(0,2000,objbacklstUnidad);
			getService().listarMarcaCombo(0,1000,objbacklstMarca);
			
			btnGrabar.setDisabled(true);
			btnModificar.setDisabled(false);
			btnEliminar.setDisabled(false);
			btnGrabar.setDisabled(true);
			getService().listarTipoPrecioD(Integer.parseInt(lstProductos.getSelectedRecord().getAttribute("idProducto")), frmlstbodpre.objbacklstTipoD);
			dynamicForm.setValue("txtCodBarras", lstProductos.getSelectedRecord().getAttribute("codigoBarras"));
			dynamicForm.setValue("txtDescripcion", lstProductos.getSelectedRecord().getAttribute("descripcion"));
			dynamicForm.setValue("txtFIFO", lstProductos.getSelectedRecord().getAttribute("fifo"));
			//dynamicForm.setValue("cmbImpuesto", lstProductos.getSelectedRecord().getAttribute("impuesto"));
			
			dynamicForm.setValue("txtLIFO", lstProductos.getSelectedRecord().getAttribute("lifo"));
			dynamicForm.setValue("txtPromedio", lstProductos.getSelectedRecord().getAttribute("promedio"));
			dynamicForm.setValue("txtid", lstProductos.getSelectedRecord().getAttribute("idProducto"));
			dynamicForm.setValue("cbmCategoria",MapCategoria.get(lstProductos.getSelectedRecord().getAttribute("categoria")));
			dynamicForm.setValue("cbmUnidad",MapUnidad.get(lstProductos.getSelectedRecord().getAttribute("unidad")));
			dynamicForm.setValue("cbmMarca",MapMarca.get(lstProductos.getSelectedRecord().getAttribute("marca")));
			Set<TblproductoMultiImpuestoDTO> prodMultiImp=listadoProductos.get(lstProductos.getSelectedRecord().getAttributeAsInt("idProducto")).getTblmultiImpuesto();
//			//com.google.gwt.user.client.Window.alert("multiimpuesto:  "+prodMultiImp.size());
			List<Integer> imps= new ArrayList<Integer>();
			for (TblproductoMultiImpuestoDTO multImp:prodMultiImp){
//				//com.google.gwt.user.client.Window.alert("multiimpuesto:  "+multImp.getIdtblproducto_tblmulti_impuesto());
//				//com.google.gwt.user.client.Window.alert("multiimpuesto hay:  "+String.valueOf(multImp.getTblmultiImpuesto()!=null));
				imps.add(multImp.getTblmultiImpuesto().getIdmultiImpuesto());
//				//com.google.gwt.user.client.Window.alert("multiimpuestoS:  "+imps.size());
			}
//			//com.google.gwt.user.client.Window.alert("multiimpuestoS:  "+imps.size());
			Integer rowNum = 0;
			for (ListGridRecord record:lstMultiImp.getRecords()){
//	    		  String atribs="Valores: ";
//				//com.google.gwt.user.client.Window.alert("multiimp contiene:  "+record.getAttributeAsInt("idImpuestoGrid")+
//						imps.contains(record.getAttributeAsInt("idImpuestoGrid")));
	    		  if(imps.contains(record.getAttributeAsInt("idImpuestoGrid"))) lstMultiImp.setEditValue(rowNum, 0,true);
//					for (String atri:record.getAttributes()){
//						atribs+=""+atri+": "+record.getAttribute(atri);
//					}
//					//com.google.gwt.user.client.Window.alert(atribs);
	    		  rowNum++;
	    	  }
			tabSet.selectTab(0);   
			btnAgregarABodega.setDisabled(false);
			
		}

		@Override
		public void onChanged(ChangedEvent event) {
			//if(indicador.equalsIgnoreCase("Bodega")){
				boolean ban;
				if(cmbBuscar.getDisplayValue().equals("Ubicaci\u00F3n")){
	    			ban= true;	    			
	    		}else{
	    			
	    			ban= false;
	    		}
				cmdBod.setShowHint(true);//muestra el combo de bodegas para seleccionnar una
				cmdBod.setVisible(ban);

			//}else
				if(indicador.equalsIgnoreCase("Buscar")){
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
				//from Tblproducto u join u.tblbodegas where nombre like '%cen%'
				//join u."+tabla+" where "+campo+" like " +"'%"+nombre+"%'")
				String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
					/*
					if(radioStock.getValue().toString()=="Elaborados"){
						StockDeseado=3;
					}else if(radioStock.getValue().toString()=="Todos"){
							StockDeseado=2;
					}else if(radioStock.getValue().toString()=="Con Stock"){
							StockDeseado=1;
					}else{
							StockDeseado=0;
					}
				*/
				getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", nombre,StockDeseado, 2,0,-1,Factum.banderaStockNegativo,listaCallback);
	    		
				//getService().listarProductoJoin2(cmdBod.getDisplayValue(), "tblbodegas", "nombre",StockDeseado, listaCallback);
			}
			
								
				
						
		}
	}
	
	
	
	
	final AsyncCallback<Float>  objfloat=new AsyncCallback<Float>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(Float result) {
			SC.say("El valor del inventario es: "+String.valueOf(result));
			getService().listaProductos(0, 20, 2,0,-1,Factum.banderaStockNegativo,listaCallback);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	
	final AsyncCallback <ProductoDTO>  retornopro =new AsyncCallback <ProductoDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(ProductoDTO result) {
			
			final Window winDetalle = new Window();  
			winDetalle.setWidth(930);  
			winDetalle.setHeight(610);  
			winDetalle.setTitle("Ingresar Detalle Producto "+result.getDescripcion());  
			winDetalle.setShowMinimizeButton(false);  
			winDetalle.setIsModal(true);  
			winDetalle.setShowModalMask(true);  
			winDetalle.centerInPage();  
			winDetalle.addCloseClickHandler(new CloseClickHandler() {  
                public void onCloseClick(CloseClientEvent event) {  
                	winDetalle.destroy(); 
                	getService().listaProductos(0, 20, 2,0,-1,Factum.banderaStockNegativo,listaCallback);
                //	limpiar();
                }   
            });
			VLayout form = new VLayout();  
            form.setSize("100%","100%"); 
            form.setPadding(5);  
            form.setLayoutAlign(VerticalAlignment.BOTTOM);
            frmLstBodPre frmlstbodpre=new frmLstBodPre(result,0);
            //Analizamos si es usuario Administrador, desbloqueamos el boton
            /*if(tipoUsuario!=1)
            {	
            	frmlstbodpre.btnGrabar.setVisible(false);              
            }*/
            frmlstbodpre.setSize("100%","100%"); 
            form.addMember(frmlstbodpre);
            winDetalle.addItem(form);
            winDetalle.show();
        	
			
			
		}
		
	};
	
			
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());//error
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			SC.say(result);
			//COMENTADO TEMPORALMENTE
			//String nombre=txtCodBarras.getDisplayValue().toUpperCase();
			//listamos los productos buscando por codigo de barras, y el 2 significa todos los prod
			
			
			//getService().listarProductoLike2(nombre, "codigoBarras",2, listaCallback);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
	};
	
	final AsyncCallback<String>  objbackGrabar=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+" No se guardo ");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(String result) {
			if(result.equals("baja")) {
				SC.confirm("Este Producto ya existe pero esta dado de baja, desea restaurarlo?", new BooleanCallback() {  
	                public void execute(Boolean value) {  
	                    if (value) {
	                    	try{
	                    		getService().modificarProductoEliminado(productoRestaura, objback);
	                       }catch(Exception e){
							SC.say(e.getMessage());
						   }
	                    } 
	                }  
	            }); 
				
			}
			else{
				SC.say(result);
			}
			
		
			//getService().listaProductos(0, 20, listaCallback);
		
			
			btnAgregarABodega.enable();
			btnModificar.enable();
			getService().ultimoProducto(objbackUltimo);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
	};

	final AsyncCallback<Integer>  objbackUltimo=new AsyncCallback<Integer>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}

		public void onSuccess(Integer result) {
			txtid.setValue(String.valueOf(result));
			productoRestaura.setIdProducto(result);
			String mensaje="";
			int j=0,i=0,k=0,count=0;
			j=frmlstbodpre.lstTipoPrecio.getRecords().length;
			for(i=0;i<j;i++){
				if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
					count++;
				} 
			}
			ProductoTipoPrecioDTO[] proTip = new ProductoTipoPrecioDTO[count];
			k=0;
			for(i=0;i<j;i++){
				if(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("PrecioV")!=null){
					ProductoTipoPrecioDTO pt =new ProductoTipoPrecioDTO();
					pt.setTblproducto(productoRestaura);
					TipoPrecioDTO tipo = new TipoPrecioDTO();
					tipo.setIdEmpresa(Factum.empresa.getIdEmpresa());
					tipo.setIdTipoPrecio(Integer.parseInt(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("idTipoPrecio")));
					tipo.setTipoPrecio(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttribute("TipoPrecio"));
					pt.setTbltipoprecio(tipo);
					pt.setPorcentaje(frmlstbodpre.lstTipoPrecio.getRecord(i).getAttributeAsDouble("PrecioV"));

					
					proTip[k]=pt;
					mensaje=mensaje+" "+proTip[k].getTbltipoprecio().getTipoPrecio()+" ";
					k=k+1;
				}
			}
			if(count==0){
				proTip=null;
			}
			final ProductoTipoPrecioDTO[] proTipF=proTip;
			final int countF=count;
			frmlstbodpre.setProducto(productoRestaura);
			cargarImpuestos();
			Set<TblproductoMultiImpuestoDTO> multImp=frmlstbodpre.producto.getTblmultiImpuesto();
			TblproductoMultiImpuestoDTO[] proMult = new TblproductoMultiImpuestoDTO[multImp.size()];
			int cont=0;
			//com.google.gwt.user.client.Window.alert("Prod "+multImp.size());
			for (TblproductoMultiImpuestoDTO prodm:multImp){
				//com.google.gwt.user.client.Window.alert("Prod "+productoRestaura.getIdProducto());
				prodm.setTblproducto(productoRestaura);
				proMult[cont]=prodm;
				cont++;
			}
			getService().grabarProductoMultiImpuesto(proMult,multback);
			getService().grabarProductoTipoPrecio(proTipF, countF,frmlstbodpre.objback);
			listadoProductos.put(productoRestaura.getIdProducto(),productoRestaura);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			btnGrabar.setDisabled(true);
		}
		
	};

	final AsyncCallback<List<CategoriaDTO>>  objbacklstCat=new AsyncCallback<List<CategoriaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			
		}

		public void onSuccess(List<CategoriaDTO> result) {
			listcat=result;
			MapCategoria.clear();
			MapCategoria.put("","");
            for(int i=0;i<result.size();i++) {
            	MapCategoria.put(String.valueOf(result.get(i).getIdCategoria()), 
						result.get(i).getCategoria());
			}
            cmbCategoria.setValueMap(MapCategoria); 
		}
		
	};
	final AsyncCallback<List<MarcaDTO>>  objbacklstMarca=new AsyncCallback<List<MarcaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<MarcaDTO> result) {
			listmar=result;
			MapMarca.clear();
			MapMarca.put("","");
            for(int i=0;i<result.size();i++) {
            	MapMarca.put(String.valueOf(result.get(i).getIdMarca()), 
						result.get(i).getMarca());
			}
            cmbMarca.setValueMap(MapMarca); 
            
		}
	};
	final AsyncCallback<List<UnidadDTO>>  objbacklstUnidad=new AsyncCallback<List<UnidadDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}

		public void onSuccess(List<UnidadDTO> result) {
			listuni=result;
			MapUnidad.clear();
			MapUnidad.put("","");
            for(int i=0;i<result.size();i++) {
            	MapUnidad.put(String.valueOf(result.get(i).getIdUnidad()), 
						result.get(i).getUnidad());
			}
            cmbUnidad.setValueMap(MapUnidad); 
		}
	};
	final AsyncCallback<List<BodegaDTO>>  objbacklstBod=new AsyncCallback<List<BodegaDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<BodegaDTO> result) {
			MapBodega.clear();
			MapBodega.put("","");
            listBodega=result;
            for(int i=0;i<result.size();i++) {
				if (Factum.banderaBodegaUbicacion==1){
					MapBodega.put(String.valueOf(result.get(i).getIdBodega()), 
							result.get(i).getBodega()+"/"+result.get(i).getUbicacion());
				}else{
					MapBodega.put(String.valueOf(result.get(i).getIdBodega()), 
							result.get(i).getBodega());
				}
            	
			}
//			for(int i=0;i<result.size();i++) {
//            	MapBodega.put(String.valueOf(result.get(i).getIdBodega()), 
//						result.get(i).getBodega());
//			}
            cmdBod.setValueMap(MapBodega);
		}
		
	};
	final AsyncCallback<List<TipoPrecioDTO>>  objbacklstTip=new AsyncCallback<List<TipoPrecioDTO>>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<TipoPrecioDTO> result) {
			listTipoP=result;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			cargarProducto();
		}
		
	};
	
	final AsyncCallback<String>  multback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say("Error al grabar: "+caught.toString());
		}
		public void onSuccess(String result) {
			//getService().listarBodegaProducto(0, 50, producto.getIdProducto(),objbacklst);
			getService().listarProdMultiImp(frmlstbodpre.producto.getIdProducto(), objbacklstMult);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
		}
		
	};
	final AsyncCallback<List<TblproductoMultiImpuestoDTO>>  objbacklstMult=new AsyncCallback<List<TblproductoMultiImpuestoDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say("ERROR FAILURE: "+caught.toString());
			SC.say("ERROR FAILURE: "+"Error no se conecta  a la base");
			
		}
		public void onSuccess(List<TblproductoMultiImpuestoDTO> result) {
//			//com.google.gwt.user.client.Window.alert("multiimpuesto:  "+prodMultiImp.size());
			List<Integer> imps= new ArrayList<Integer>();
			for (TblproductoMultiImpuestoDTO multImp:result){
//				//com.google.gwt.user.client.Window.alert("multiimpuesto:  "+multImp.getIdtblproducto_tblmulti_impuesto());
//				//com.google.gwt.user.client.Window.alert("multiimpuesto hay:  "+String.valueOf(multImp.getTblmultiImpuesto()!=null));
				imps.add(multImp.getTblmultiImpuesto().getIdmultiImpuesto());
//				//com.google.gwt.user.client.Window.alert("multiimpuestoS:  "+imps.size());
			}
//			//com.google.gwt.user.client.Window.alert("multiimpuestoS:  "+imps.size());
			Integer rowNum = 0;
			for (ListGridRecord record:lstMultiImp.getRecords()){
//				//com.google.gwt.user.client.Window.alert("multiimp contiene:  "+record.getAttributeAsInt("idImpuestoGrid")+
//						imps.contains(record.getAttributeAsInt("idImpuestoGrid")));
	    		  if(imps.contains(record.getAttributeAsInt("idImpuestoGrid"))) lstMultiImp.setEditValue(rowNum, 0,true);
	    		  rowNum++;
	    	  }
		}
	};
	final AsyncCallback<List<TblmultiImpuestoDTO>>  multicallback=new AsyncCallback<List<TblmultiImpuestoDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say("ERROR FAILURE: "+caught.toString());
			SC.say("ERROR FAILURE: "+"Error no se conecta  a la base");
			
		}
		public void onSuccess(List<TblmultiImpuestoDTO> result) {
			listmult=result;
			lstMultiImp.setWidth(225);
			lstMultiImp.setHeight(175);
			lstMultiImp.setAlternateRecordStyles(true);
			lstMultiImp.setShowAllRecords(true);
			lstMultiImp.setTabIndex(11);
//			lstMultiImp.setSelectionType(SelectionStyle.SIMPLE);
//			lstMultiImp.setSelectionAppearance(SelectionAppearance.CHECKBOX);
//			lstMultiImp.addSelectionChangedHandler(new SelectionChangedHandler() {
//			      public void onSelectionChanged(SelectionEvent event) {
////			    	  for (ListGridRecord record:lstMultiImp.getSelection()){
//			    	  for (ListGridRecord record:lstMultiImp.getRecords()){
//			    		  String atribs="Valores: ";
//							for (String atri:record.getAttributes()){
//								atribs+=""+atri+": "+record.getAttribute(atri);
//							}
//							//com.google.gwt.user.client.Window.alert(atribs);
//			    	  }
//			      }
//			    });
			
//			lstMultiImp.setStyle();
			ListGridField checkBoxField = new ListGridField("checkBox", " ");
			checkBoxField.setWidth("7%");
			checkBoxField.setAlign(Alignment.LEFT);
			checkBoxField.setType(ListGridFieldType.BOOLEAN);
//			checkBoxField.setCanFilter(false);
//			checkBoxField.setCanEdit(true);
//			checkBoxField.setCanToggle(true);
			checkBoxField.setDefaultValue(Boolean.FALSE);
//			checkBoxField.addChangedHandler(new com.smartgwt.client.widgets.grid.events.ChangedHandler(){
//				@Override
//				public void onChanged(com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
//					// TODO Auto-generated method stub
//					//com.google.gwt.user.client.Window.alert("Cambia check impuesto ");
//					//com.google.gwt.user.client.Window.alert("Cambia check impuesto "+event.getValue());
//					lstMultiImp.saveAllEdits();
//					cargarImpuestos();
//					frmlstbodpre.ActualizarVal();
//				}
//			});
//			checkBoxField.addChangedHandler(new com.smartgwt.client.widgets.grid.events.ChangedHandler(){
//				@Override
//				public void onChanged(com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
//					// TODO Auto-generated method stub
//					//com.google.gwt.user.client.Window.alert("Cambia check impuesto ");
//					//com.google.gwt.user.client.Window.alert("Cambia check impuesto "+event.getValue());
//					cargarImpuestos();
//					frmlstbodpre.ActualizarVal();
//				}
//			});
			ListGridField idImpuestoGrid = new ListGridField("idImpuestoGrid", "id");
			idImpuestoGrid.setWidth("5%");
			idImpuestoGrid.setHidden(true);
			ListGridField impuestoGrid = new ListGridField("impuestoGrid", "Impuesto");
			impuestoGrid.setWidth("50%");
			ListGridField porcentajeGrid = new ListGridField("porcentajeGrid", "Porcentaje");
			porcentajeGrid.setWidth("35%");
			ListGridField tipoImpuestoGrid = new ListGridField("tipoImpuestoGrid", "tipo");
			tipoImpuestoGrid.setWidth("5%");
			tipoImpuestoGrid.setHidden(true);
			
			lstMultiImp.setFields(new ListGridField[]{checkBoxField,idImpuestoGrid,impuestoGrid,porcentajeGrid,tipoImpuestoGrid});
			RecordList listado = new RecordList();
            
			for (TblmultiImpuestoDTO multImp:result){
//				//com.google.gwt.user.client.Window.alert("Impuesto:  "+multImp.getDescripcion()+"  "+ multImp.getPorcentaje());
//				ListGridRecord recordret=new ListGridRecord();
				MultiImpuestoRecords recordret=new MultiImpuestoRecords();
				recordret.setAttribute("checkBox", false);
				recordret.setAttribute("idImpuestoGrid", multImp.getIdmultiImpuesto());
				recordret.setAttribute("impuestoGrid", multImp.getDescripcion());
				recordret.setAttribute("porcentajeGrid", multImp.getPorcentaje());
				recordret.setAttribute("tipoImpuestoGrid", String.valueOf(multImp.getTipo()));
				listado.add(recordret);
			}
			lstMultiImp.setData(new RecordList());
			lstMultiImp.setData(listado);
			lstMultiImp.addRecordClickHandler(new RecordClickHandler(){

				@Override
				public void onRecordClick(RecordClickEvent event) {
					// TODO Auto-generated method stub
					
					//com.google.gwt.user.client.Window.alert("Record click ");
					Record rec = event.getRecord();

	                boolean checked = rec.getAttributeAsBoolean("checkBox");
	                
	                rec.setAttribute("checkBox", !checked);
	                //com.google.gwt.user.client.Window.alert("Record check change ");
	                lstMultiImp.saveAllEdits();
	                lstMultiImp.refreshFields();
	                //com.google.gwt.user.client.Window.alert("Change checks end");
	                cargarImpuestos();
	                //com.google.gwt.user.client.Window.alert("Despues cargar impuestos");
				}
				
			});
			cargarImpuestos();
//			ListGridRecord[] selectedRecords = lstMultiImp.getRecords();
//			for (ListGridRecord rec : selectedRecords) {
//				String atribs="Valores: ";
//				for (String atri:rec.getAttributes()){
//					atribs+=""+atri+": "+rec.getAttribute(atri);
//				}
//				//com.google.gwt.user.client.Window.alert(atribs);
//				
//			}
		}
	};
	
	
	/*final AsyncCallback<List<ProductoDTO>>  lista20=new AsyncCallback<List<ProductoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<ProductoDTO> result) {
			listadoProductos=null;
			listadoProductos=result;
			
			
			if(bandera==1){
			getService().numeroRegistrosProducto("tblproducto", objbackI);
			int numProductos=result.size();	
			registros=numProductos;
			}
			
		RecordList listado = new RecordList();
		Iterator iter = result.iterator();
			
			
			int numProd=0;
           for (int i=0;i<20;i++){
            	Producto pro =new Producto((ProductoDTO) result.get(i));
            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
            	listado.add(pro);      	
             	numProd++;
            }
     
            lblRegisros.setText(contador+" de "+registros);
            bandera=0;
		
            lstProductos.setData(new RecordList());
            lstProductos.setData(listado);
            lstdescripcion.setSortDirection(SortDirection.ASCENDING);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};*/
	
	
	final AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer,ProductoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(LinkedHashMap<Integer,ProductoDTO> result) {
			listadoProductos=null;
			listadoProductos=result;
			
			
			if(bandera==1){
			getService().numeroRegistrosProducto("tblproducto", objbackI);
			int numProductos=result.size();	
			registros=numProductos;
			}
			
			RecordList listado = new RecordList();
		//	ListGridRecord[] listadoProd = new ListGridRecord[result.size()];
			Iterator iter = result.values().iterator();
			
			
			int numProd=0;
            while (iter.hasNext()) {
            	Producto pro =new Producto((ProductoDTO)iter.next());
            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
            	listado.add(pro);      	
             	numProd++;
            }
     
            lblRegisros.setText(contador+" def "+registros);
            bandera=0;
		
            lstProductos.setData(new RecordList());
            lstProductos.setData(listado);
            lstdescripcion.setSortDirection(SortDirection.ASCENDING);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
		//	registros=result;
		
		}
	};
	private class FuncionSI implements FormItemIfFunction {
        public boolean execute(FormItem item, Object value, SearchForm searchForm) {
        	boolean ban=false;
        	//if(cmbBuscar.getSelectOnFocus()){
        		if(cmbBuscar.getDisplayValue().equals("Ubicaci\u00F3n")){
        			ban= true;
        		}else{
        			ban= false;
        		}
        	//}
			return ban;
         }

		@Override
		public boolean execute(FormItem item, Object value, DynamicForm form) {
			// TODO Auto-generated method stub
			return false;
		}
	}
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
	//Funcion para identificar el tipo de usuario
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
      
        		tipoUsuario = result.getNivelAcceso();
        		getService().listarTipoprecio(0,100,objbacklstTip );
        	//funcionBloquear(true);
			}
        }

        public void onFailure(Throwable caught) {
            tipoUsuario = null;
            com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };
    
	
	
    
    final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			//String url = ""+"factum\\pdfServlet?fileName=ReporteServicios.pdf";
			String baseURL=GWT.getModuleBaseURL();
			String url = baseURL+"pdfServlet?fileName=ReporteServicios.pdf";
			com.google.gwt.user.client.Window.open( url, "", "");
			SC.say(result);
		}
	};    
	
	
	  //**************************************************	
	final AsyncCallback<String> callbackString = new AsyncCallback<String>() {

        public void onSuccess(String result){
        	nombreHost=result;
        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
        }
        public void onFailure(Throwable caught) {          
        	SC.say("No se puede obtener la lista las imagenes;");
        }
    }; 	
    private class Manejador implements  ChangedHandler{
		// manejador de cuando presione esc,etc
		String indicador = "";// para indicar que fue lo que se presiono tendra
								// el nombre
		
		Manejador(String nombreBoton) {
			this.indicador = nombreBoton;
		}
								
//								@Override
//								public void onChanged(ChangedEvent event) {
//									// TODO Auto-generated method stub
//									if (indicador.equals("cambioCosto")){
//										int numProductos=frmlstbodpre.lstTipoPrecio.getRecords().length;
//										for (int j=0;j<numProductos;j++)
//										{
//											frmlstbodpre.lstTipoPrecio.setEditValue(j,"PrecioC", txtPromedio.getDisplayValue());
//										}
//										frmlstbodpre.lstTipoPrecio.refreshFields();
//									}
//								}
								@Override
								public void onChanged(ChangedEvent event) {
									// TODO Auto-generated method stub
									if (indicador.equals("cambioCosto")){
										int numProductos=frmlstbodpre.lstTipoPrecio.getRecords().length;
										frmlstbodpre.producto.setPromedio(Double.valueOf(txtPromedio.getDisplayValue()));
										for (int j=0;j<numProductos;j++)
										{
											frmlstbodpre.lstTipoPrecio.setEditValue(j,"PrecioC", txtPromedio.getDisplayValue());
											frmlstbodpre.lstTipoPrecio.saveAllEdits();
										}
										frmlstbodpre.ActualizarVal();
										frmlstbodpre.lstTipoPrecio.refreshFields();
									}else if (indicador.equals("cmbImpuesto")){
//										frmlstbodpre.producto.setImpuesto(Double.valueOf((String)dynamicForm.getItem("cmbImpuesto").getValue()));
										frmlstbodpre.ActualizarVal();
										frmlstbodpre.lstTipoPrecio.refreshFields();
									}
								}
		
		}
    
}



