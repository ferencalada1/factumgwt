package com.giga.factum.client;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.giga.factum.client.DTO.CuentaDTO;
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.TipoPagoDTO;
import com.giga.factum.client.regGrillas.EmpresaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.types.CharacterCasing;

public class frmFormaPago extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	ComboBoxItem cmbPlanCuenta = new ComboBoxItem("cmbPlanCuenta", "Plan de Cuenta");
 	ComboBoxItem cmbCuenta = new ComboBoxItem("cmbCuenta", "Cuenta");
 	Label lblRegisros = new Label("# Registros");
 	LinkedHashMap<String, String> MapCuenta = new LinkedHashMap<String, String>();
 	LinkedHashMap<String, String> MapEmpresa = new LinkedHashMap<String, String>();
	EmpresaDTO empresaDTO =new EmpresaDTO();
	LinkedList<String[]> cuentaplan=new LinkedList<String[]>();
	Boolean ban=false;
	int contador=20;
	int registros=0;
	TabSet tabSet = new TabSet();
	ListGrid LstFormaPago = new ListGrid();
	
	public frmFormaPago() {
		setSize("910", "600");
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Grabar"));
		
		
		tabSet.setSize("100%", "100%");
		Tab tabIngreso = new Tab("Ingreso FormaPago");
		VLayout layout = new VLayout();
		layout.setSize("100%", "100%");
		
		dynamicForm.setSize("100%", "30%");
		dynamicForm.setMinColWidth(50);
		dynamicForm.setItemLayout(FormLayoutType.TABLE);
		
		dynamicForm.setWidth100();
		
		dynamicForm.setNumCols(2);
		TextItem txtFormaPago = new TextItem("txtFormaPago", "Forma de Pago");
		txtFormaPago.setCharacterCasing(CharacterCasing.UPPER);
		txtFormaPago.setLength(20);
		txtFormaPago.setTabIndex(2);
		txtFormaPago.setLeft(133);
		txtFormaPago.setTop(34);
		txtFormaPago.setRequired(true);
		
		TextItem textItem = new TextItem("txtIdPago", "C\u00F3digo Forma de Pago");
		textItem.setDisabled(true);
		cmbPlanCuenta.setRequired(true);
	 	cmbCuenta.setRequired(true);
		dynamicForm.setFields(new FormItem[] { textItem, txtFormaPago,cmbCuenta, cmbPlanCuenta});
		layout.addMember(dynamicForm);
		
		Canvas canvas = new Canvas();
		canvas.setSize("100%", "70%");
		
		Button btnEliminar = new Button("Eliminar");
		canvas.addChild(btnEliminar);
		btnEliminar.moveTo(324, 6);
		layout.addMember(canvas);
		
		
		Button btnGrabar = new Button("Grabar");
		canvas.addChild(btnGrabar);
		btnGrabar.moveTo(6, 6);
		btnGrabar.setSize("100px", "22px");
		
		Button btnNuevo = new Button("Nuevo");
		canvas.addChild(btnNuevo);
		btnNuevo.moveTo(112, 6);
		
		IButton btnModificar = new IButton("Eliminar");
		canvas.addChild(btnModificar);
		btnModificar.moveTo(218, 6);
		
		btnNuevo.addClickHandler(new ManejadorBotones("Eliminar"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("Modificar"));
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		
		tabIngreso.setPane(layout);
		tabSet.addTab(tabIngreso);
		
		Tab tabListado = new Tab("Listado Forma de Pagos");
		
		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
		
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		
		SearchForm searchForm = new SearchForm();
		searchForm.setSize("88%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		TextItem txtBuscarLst = new TextItem("newTextItem_1", "");
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);  
		ComboBoxItem cmdBuscarLst = new ComboBoxItem("newComboBoxItem_2", "");
		cmdBuscarLst.setLeft(158);
		cmdBuscarLst.setTop(6);
		cmdBuscarLst.setWidth(146);
		cmdBuscarLst.setHeight(22);
		cmdBuscarLst.setHint("Buscar Por");
		cmdBuscarLst.setShowHintInField(true);
		StaticTextItem txtNumLst = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
		txtNumLst.setLeft(699);
		txtNumLst.setTop(14);
		txtNumLst.setWidth(76);
		txtNumLst.setHeight(14);
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmdBuscarLst, txtNumLst});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnelimi = new TransferImgButton(TransferImgButton.DELETE);
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
		hLayout.addMember(hStack);
		hLayout.addMember(btnelimi);
		
		layout_1.addMember(hLayout);
		
		LstFormaPago.setSize("100%", "80%");
		ListGridField lstidTipoPago=new ListGridField("lstidTipoPago", "C\u00F3digo");
		ListGridField lstNombre= new ListGridField("lstNombre", "Forma de Pago");
		LstFormaPago.setFields(new ListGridField[] { lstidTipoPago,lstNombre });
		layout_1.addMember(LstFormaPago);
		tabListado.setPane(layout_1);
		tabSet.addTab(tabListado);
		addMember(tabSet);
		getService().listarEmpresa(0,20, objbacklstEmpresa);
		cmbPlanCuenta.addChangeHandler(  new ManejadorBotones("combo"));
		
	} 
	public void limpiar(){
		dynamicForm.setValue("txtFormaPago", "");
		dynamicForm.setValue("txtidTipopago","");
	}
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,
	FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler,ChangeHandler{
		String indicador="";
		String nombre="";
		String idPlanCuenta="";
		String idCuenta="";
		public void datosfrm(){
			try{
				nombre=dynamicForm.getItem("txtFormaPago").getDisplayValue();
				SC.say(nombre);
				
				
				idCuenta=(String) dynamicForm.getItem("cmbCuenta").getValue();
				for(int i=0;i<cuentaplan.size();i++){
					String[] cuen=new String[3];
					cuen=cuentaplan.get(i);
					if(cuen[0].equals(idCuenta)){
						idPlanCuenta=cuen[2];
						break;
					}
				}
				SC.say(idCuenta+idPlanCuenta);
				
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					datosfrm();
					try{
						TipoPagoDTO TipoPagoDTO=new TipoPagoDTO();
						TipoPagoDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
						TipoPagoDTO.setTipoPago(nombre);
						SC.say(nombre);
						TipoPagoDTO.setIdCuenta(Integer.parseInt(idCuenta));
						TipoPagoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().grabarTipopago(TipoPagoDTO,objback);
						contador=20;
					}catch(Exception e){
						SC.say(e.getMessage());
					}
					
					
					
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
	                public void execute(Boolean value) {  
	                    if (value != null && value) {
	                    	datosfrm();
	    					try{
	    						TipoPagoDTO TipoPagoDTO=new TipoPagoDTO();
	    						TipoPagoDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
	    						TipoPagoDTO.setTipoPago(nombre);
	    						SC.say(nombre);
	    						TipoPagoDTO.setIdCuenta(Integer.parseInt(idCuenta));
	    						TipoPagoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
	    						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	    						getService().grabarTipopago(TipoPagoDTO,objback);
	    						LstFormaPago.getSelectedRecord().getAttribute("Tipopago");
		                    	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		    			    	getService().eliminarTipopago(TipoPagoDTO, objback);
		                    	limpiar();
		                    	LstFormaPago.removeData(LstFormaPago.getSelectedRecord());
		        				getService().listarTipopago(0,20, objbacklst);
	    						contador=20;
	    					}catch(Exception e){
	    						SC.say(e.getMessage());
	    					}
	                  	} else {  
	                        SC.say("Objeto no Eliminado");
	                    }  
	                }  
	            });  
				
			}
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					//SC.say("se pulso el boton grabar");
					//llamamos al RPC
					try{
						TipoPagoDTO TipopagoDTO=new TipoPagoDTO();
						TipopagoDTO.setIdEmpresa(Factum.empresa.getIdEmpresa());
						TipopagoDTO.setTipoPago(nombre);
						TipopagoDTO.setIdCuenta(Integer.parseInt(idCuenta));
						TipopagoDTO.setIdPlan(Integer.parseInt(idPlanCuenta));
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().grabarTipopago(TipopagoDTO,objback);
						LstFormaPago.getSelectedRecord().getAttribute("Tipopago");
                    	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
    			    	limpiar();
                    	LstFormaPago.removeData(LstFormaPago.getSelectedRecord());
        				getService().modificarTipopago(TipopagoDTO,objback);
        				getService().listarTipopago(0,20, objbacklst);
						contador=20;
					}catch(Exception e){
						SC.say(e.getMessage());
					}
					
					
					
					
					
					
				}
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipopago(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipopago(contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipopago(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipopago(registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarTipopago(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}
			
		
		
		
		public void onKeyPress(KeyPressEvent event) {
			// TODO ejemplo de cosas para hacer
			//SC.say(event.getKeyName());
			
			
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onChange(ChangeEvent event) {
			String id=(String) event.getValue();
			if(!id.equals(null))
				getService().listarCuentaPlanCuenta(id,objbacklstCuentaPlan);
			
		}
		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtFormaPago",LstFormaPago.getSelectedRecord().getAttribute("lstFormaPago"));  
				dynamicForm.setValue("txtidTipopago", LstFormaPago.getSelectedRecord().getAttribute("lstCodigo"));
				tabSet.selectTab(0);
			}
			
		}
	}
		final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
			
		}

		public void onSuccess(String result) {
			// TODO Auto-generated method stub
			SC.say("todo bien  con el servidor retorno "+result);
			
			
		}
		
	};
	
	final AsyncCallback<List<CuentaDTO>>  objbacklstCuenta=new AsyncCallback<List<CuentaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
			
		}
		public void onSuccess(List<CuentaDTO> result) {
			List<CuentaDTO> listcuenta=null;
			listcuenta=result;
			MapCuenta.clear();
			MapCuenta.put("","");
            for(int i=0;i<result.size();i++) {
            	MapCuenta.put(String.valueOf(result.get(i).getIdCuenta()), 
						result.get(i).getNombreCuenta());
			}
            cmbCuenta.setValueMap(MapCuenta);  
            
		}
	};
	final AsyncCallback<LinkedList<String[]>>  objbacklstCuentaPlan=new AsyncCallback<LinkedList<String[]>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		
		@Override
		public void onSuccess(LinkedList<String[]> result) {
			cuentaplan=result;
			MapCuenta.clear();
			MapCuenta.put("","");
            for(int i=0;i<result.size();i++) {
            	String[] cuentas=new String[3];
            	cuentas=result.get(i);
            	MapCuenta.put(cuentas[0],cuentas[1]);
			}
			cmbCuenta.setValueMap(MapCuenta);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
	};
	final AsyncCallback<List<TipoPagoDTO>>  objbacklst=new AsyncCallback<List<TipoPagoDTO>>(){
		
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<TipoPagoDTO> result) {
			getService().numeroRegistrosTipopago("TblTipopago", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				//listado[i]=(new TipoagoRecords((TipoPagoDTO)result.get(i)));
			}
			LstFormaPago.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
			
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			
		}
		public void onSuccess(Integer result) {
			registros=result;
		}
	};
	final AsyncCallback<List<EmpresaDTO>>  objbacklstEmpresa=new AsyncCallback<List<EmpresaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
		}
		public void onSuccess(List<EmpresaDTO> result) {
			MapEmpresa.clear();
            for(int i=0;i<result.size();i++) {
            	MapEmpresa.put(String.valueOf(new EmpresaRecords((EmpresaDTO)result.get(i)).getidEmpresa()), 
						new EmpresaRecords((EmpresaDTO)result.get(i)).getEmpresa());
			}
			cmbPlanCuenta.setValueMap(MapEmpresa); 
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			
       }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
