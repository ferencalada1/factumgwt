package com.giga.factum.client;

import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.smartgwt.client.widgets.events.CloseClickHandler;  
import com.smartgwt.client.widgets.events.CloseClientEvent;  
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.giga.factum.client.DTO.BodegaDTO;
import com.giga.factum.client.DTO.CategoriaDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.MarcaDTO;
import com.giga.factum.client.DTO.ProductoDTO;
import com.giga.factum.client.DTO.SerieDTO;
import com.giga.factum.client.DTO.TipoPrecioDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.client.regGrillas.Producto;
import com.giga.factum.client.regGrillas.SerieRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;



public class frmSerie extends VLayout{
	TextItem txtIdProducto = new TextItem("txtIdProducto", "Id Producto");
	TextItem txtDescripcion = new TextItem("txtDescripcion", "Descripcion");
	TextItem txtFacCompra = new TextItem("txtFacCompra", "Factura de Compra");
	TextItem txtSerie = new TextItem("txtSerie", "Buscar por Serie");
	LinkedList<SerieDTO> series=new LinkedList<SerieDTO>();
	private final ListGrid ListSeries = new ListGrid();
	private HStack hStack = new HStack();
	private final IButton btnGrabar = new IButton("Grabar");
	private final IButton btnLimpiar = new IButton("Limpiar");
	private final IButton btnAnadir = new IButton("Agregar");
	private final IButton btnQuitar = new IButton("Quitar");
	PickerIcon buscarProductos = new PickerIcon(PickerIcon.SEARCH);
	PickerIcon buscarFacturas = new PickerIcon(PickerIcon.SEARCH);
	PickerIcon buscarSeries = new PickerIcon(PickerIcon.SEARCH);
	DynamicForm dynamicForm = new DynamicForm();
	
	LinkedHashMap<String, String> MapCategoria = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapUnidad = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapMarca = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> MapBodega = new LinkedHashMap<String, String>();
	ListGrid lstProductos = new ListGrid();
	//SearchForm searchForm = new SearchForm();
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	public ProductoDTO prod=new ProductoDTO();
	//VLayout ListaProducto=new VLayout();
	
	
	ListGrid lstReporteCaja = new ListGrid();
	DynamicForm dynamicFormDoc = new DynamicForm();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	ListGrid lstCliente = new ListGrid();
	ListGrid lstVendedor = new ListGrid();
	VLayout layout_1 = new VLayout();
	String idVendedor="";
	String idCliente="";
	String idProducto="";
	Integer rowNumGlobdal = 0;
	String Tipo="";
	final Window winCliente = new Window();  
	final Window winVendedor = new Window();  
	Window winFactura1 = new Window(); 
	TextItem txtCliente =new TextItem("txtCliente", "Cliente");
	TextItem txtVendedor =new TextItem("txtVendedor", "Vendedor");
	TextItem txtProducto=new TextItem("txtProducto", "Producto");
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	
	/************************************************************/
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLstDoc = new TextItem("txtBuscarLstDoc", "");
	HStack hStackDoc = new HStack();
	HLayout hLayoutPr = new HLayout();
	VentanaEmergente listadoProd ;
	SearchForm searchFormDoc = new SearchForm();
	ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	List<CategoriaDTO> listcat=null;
	List<MarcaDTO> listmar=null;
	List<UnidadDTO> listuni=null;
	List<BodegaDTO> listBodega=null;
	List<TipoPrecioDTO> listTipoP=null;
	int contador=20;
	int registros=0;
	Label lblRegisros = new Label("# Registros");
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	VLayout ListDocumentos =new VLayout();
	Window winDetalle = new Window(); 
	Window winfacturas = new Window();  
	
	ListGridField listSerie =new ListGridField("serie", "Serie",200);
	ListGridField listCodigoBarras =new ListGridField("CodigoBarras", "C�digo de Barras",100);
	ListGridField listDescripcion =new ListGridField("descripcion", "Descripci�n",200);
	ListGridField listNumFacturaV =new ListGridField("numFacturaV", "Numero de Factura de Venta",200);
	ListGridField listNumFacturaC =new ListGridField("numFacturaC", "Numero de Factura de Compra",200);
	
	frmReporteCaja form;
	Label lblRegistrosFactura = new Label("# Registros");
	SearchForm searchFormProducto = new SearchForm();
	
	final CheckboxItem chkServicio=new CheckboxItem("servicio","Servicio");
	final CheckboxItem chkProductoElaborado=new CheckboxItem("productoelaborado","ProductoElaborado");
	
	final ComboBoxItem cmbCategoria = new ComboBoxItem("cbmCategoria",
			"Categor\u00EDa");
	final ComboBoxItem cmbUnidad = new ComboBoxItem("cbmUnidad", "Unidad");
	final ComboBoxItem cmbMarca = new ComboBoxItem("cbmMarca", "Marca");
	final ComboBoxItem cmdBod = new ComboBoxItem("cmbBod", "Bodega");
	
	public frmSerie(ProductoDTO producto,/*float cantidad,DtocomercialDTO doc,*/int tipo) {
		dynamicForm.setNumCols(4);
		ListSeries.setCanEdit(true);
		buscarProductos.addFormItemClickHandler(new FormItemClickHandler() {  
            public void onFormItemClick(FormItemIconClickEvent event) {  
//            	try{
//            		//VentanaEmergente winDetalle =new VentanaEmergente("Listado de Producto ",ListaProducto, true, true);
//            		
//            		lstProductos.redraw();
//            		searchForm.redraw();
//            		winDetalle.redraw();
//            		ListaProducto.redraw();
//            		winDetalle.show();
//            		
//            		winDetalle.setWidth(930);  
//					winDetalle.setHeight(610);  
//					winDetalle.setTitle("Listado de Producto ");  
//					winDetalle.setShowMinimizeButton(false);  
//					winDetalle.setIsModal(true);  
//					winDetalle.setShowModalMask(true);  
//					winDetalle.centerInPage();  
//					winDetalle.addCloseClickHandler(new CloseClickHandler() {  
//	                    public void onCloseClick(CloseClientEvent event) {
//							winDetalle.destroy();  
//						}   
//	                });
//					//frmproducto.lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("producto"));
//	                ListaProducto.setSize("100%","100%"); 
//	                winDetalle.redraw();
//	                winDetalle.addItem(ListaProducto);
//	                winDetalle.show();
//            	}catch(Exception e){
//            		SC.say(e.getMessage());
//            	}
//_________________________________________________________________________________________________________________________________________________
            	//rowNumGlobal = grdProductos.getEditRow();

        		getService().listarCategoria(0, 40, objbacklstCat);
        		getService().listarUnidad(0, 20, objbacklstUnidad);
        		getService().listarMarca(0, 40, objbacklstMarca);
        		getService().listaProductos(0, 20, 1,0,-1, Factum.banderaStockNegativo, listaCallback);
        		getService().listarBodega(0, 100, objbacklstBod);
        		getService().listarTipoprecio(0, 20, objbacklstTip);
        		
        		layout_1= new VLayout();
        		hLayoutPr= new HLayout();
        		hStack= new HStack();
        		
        		layout_1.setSize("100%", "100%");
        		hLayoutPr.setSize("100%", "6%");
        		searchFormProducto.setSize("75%", "80%");
        		hStack.setSize("12%", "100%");
        		
        		lstProductos.setAutoFitData(Autofit.VERTICAL);
        		lstProductos.setAutoFitMaxRecords(10);
        		lstProductos.setAutoFetchData(true);
        		lstProductos.setSize("100%", "80%");
        		lblRegistrosFactura.setSize("100%", "4%");
        		hLayoutPr.addMember(searchFormProducto);
        		hLayoutPr.addMember(hStack);
        		layout_1.addMember(hLayoutPr);
        		layout_1.addMember(lstProductos);
        		layout_1.addMember(lblRegistrosFactura);
        		listadoProd = new VentanaEmergente("Listado de Productos", layout_1,
        				false, true);
        		listadoProd.setWidth(930);
        		listadoProd.setHeight(610);
        		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("producto"));
        		listadoProd.addCloseClickHandler(new CloseClickHandler() {
        			public void onCloseClick(CloseClientEvent event) {
//        				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
//        						"display", "block");
        				listadoProd.destroy();
        			}
        		});
        		listadoProd.show();
            	
            }  
        });  
		txtIdProducto.setRequired(true);
		txtDescripcion.setRequired(true);
		txtFacCompra.setRequired(true);
		buscarFacturas.addFormItemClickHandler(new ManejadorBotones("facturas"));
		buscarSeries.addFormItemClickHandler(new FormItemClickHandler() {  
            public void onFormItemClick(FormItemIconClickEvent event) {  
            	getService().buscarSerie(String.valueOf(txtSerie.getValue()), objbackDTO);
            	
            }
		});
		txtIdProducto.setIcons(buscarProductos);
		txtDescripcion.setIcons(buscarProductos);
		txtFacCompra.setIcons(buscarFacturas);
		txtSerie.setIcons(buscarSeries);
		dynamicForm.setFields(new FormItem[] { txtIdProducto,txtDescripcion,txtFacCompra,txtSerie});
		addMember(dynamicForm);
		
		
		if(tipo==0){
			ListSeries.setFields(new ListGridField[] {listSerie});
			addMember(ListSeries);
		}else if(tipo==1){
			getService().listarSeries(producto.getIdProducto(),listaCallbackSeries);
			txtIdProducto.setVisible(false);
			txtDescripcion.setVisible(false);
			txtFacCompra.setVisible(false);
			ListSeries.setFields(new ListGridField[] {listSerie,listCodigoBarras,listDescripcion,listNumFacturaC,listNumFacturaV});
			addMember(ListSeries);
		}
		
		hStack.addMember(btnAnadir);
		hStack.addMember(btnQuitar);
		hStack.addMember(btnGrabar);
		hStack.addMember(btnLimpiar);
		addMember(hStack);
		btnAnadir.addClickHandler(new ManejadorBotones("aniadir"));
		btnQuitar.addClickHandler(new ManejadorBotones("quitar"));
		btnGrabar.addClickHandler(new ManejadorBotones("grabar"));
		
		//ListaProducto
		//ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
		//searchFormProducto.setSize("100%", "5%");
		//searchFormProducto.setItemLayout(FormLayoutType.ABSOLUTE);
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setHint("Buscar");
				txtBuscarLst.setIcons(buscarPicker);
		txtBuscarLst.setShowHintInField(true);
		txtBuscarLst.setShowHintInField(true);
		txtBuscarLst.setShowTitle(false);
		
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscar"));
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo de Barras","Descripci\u00F3n","Categoria"
				,"Unidad","Marca");
		cmbBuscar.setShowTitle(false);
		cmbBuscar.addChangedHandler(new ManejadorBotones("Bodega"));
		
		chkServicio.setValue(false);
		chkServicio.addChangedHandler(new ManejadorBotonesServicio("Servicio"));
		chkProductoElaborado.setValue(false);
		chkProductoElaborado.addChangedHandler(new ManejadorBotonesProductoElaborado("ProductoElaborado"));

		searchFormProducto.setNumCols(6);
		searchFormProducto.setFields(new FormItem[] { txtBuscarLst, cmbBuscar, cmdBod, chkServicio,chkProductoElaborado });
		if(Factum.banderaMenuServicio==1){
			chkServicio.setVisible(true);
			chkServicio.setDisabled(false);
		}else{
			chkServicio.setVisible(false);
			chkServicio.setDisabled(true);
		}
		if(Factum.banderaProduccionProductos==1){
			chkProductoElaborado.setVisible(true);
			chkProductoElaborado.setDisabled(false);
		}else{
			chkProductoElaborado.setVisible(false);
			chkProductoElaborado.setDisabled(true);
		}
		
		//searchFormProducto.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		
		
		
		ListGridField lstcodbarras = new ListGridField("codigoBarras", "C\u00F3digo de Barras",150);
        lstcodbarras.setCellAlign(Alignment.LEFT);
        ListGridField lstdescripcion =new ListGridField("descripcion", "Descripci\u00F3n",400); 
        ListGridField lstimpuesto = new ListGridField("impuesto", "Impuesto",100);
        lstimpuesto.setCellAlign(Alignment.CENTER);
        ListGridField lstlifo = new ListGridField("lifo", "LIFO",100);
        lstlifo.setCellAlign(Alignment.CENTER);
        ListGridField lstfifo = new ListGridField("fifo", "FIFO",100);
        lstfifo.setCellAlign(Alignment.CENTER);
        ListGridField lstpromedio = new ListGridField("promedio", "Promedio",100);
        lstpromedio.setCellAlign(Alignment.CENTER);
        ListGridField lstunidad = new ListGridField("Unidad", "Unidad",150);
        ListGridField listGridField_6 = new ListGridField("stock", "Stock",100);
        listGridField_6.setCellAlign(Alignment.CENTER);
        ListGridField lstcategoria =new ListGridField("Categoria", "Categoria",150);
        ListGridField lstMarca= new ListGridField("Marca", "Marca",150);
        lstProductos.setFields(new ListGridField[] { 
        		lstcodbarras, 
    			lstdescripcion,
    			lstunidad,
    			lstcategoria,
    			lstMarca
        });
        
        //listadoProd.addMember(searchFormProducto);
        //ListaProducto.addMember(searchFormProducto);
        //listadoProd.addMember(lstProductos);
        //ListaProducto.addMember(lstProductos);
		
		IButton btnSeleccionar = new IButton("Seleccionar");
		//btnSeleccionar.addClickHandler(new ManejadorBotones("seleccionar"));
		//addMember(btnSeleccionar);
		getService().listaProductos(0,20, 1,0,-1,Factum.banderaStockNegativo,listaCallback);
		lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("producto"));
		txtBuscarLst.addKeyPressHandler(new ManejadorBotones("producto"));
		//FIN	ListaProducto
		
		//Lista de Documentos
		try{
			
			dynamicFormDoc.setSize("60px", "20%");
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaInicial.setValue(new Date());
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setValue(new Date());
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			 
			 
			txtVendedor.addKeyPressHandler( new ManejadorBotones("vendedor"));
			txtProducto.addKeyPressHandler( new ManejadorBotones("producto"));
			txtCliente.addKeyPressHandler( new ManejadorBotones("cliente"));
			
			
			cmbDocumento.setRequired(true);
			cmbDocumento.setValueMap("Facturas de Venta","Facturas de Compra","Notas de Entrega","Notas de Credito","Anticipo Clientes","Ajustes de Inventario","Gastos");
			cmbDocumento.setValue("Facturas de Compra");
			dynamicFormDoc.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, txtCliente,txtVendedor, txtProducto, cmbDocumento});
			ListDocumentos.addMember(dynamicFormDoc);
			
			Label lblIngresosDelDa = new Label("Ingresos del D\u00EDa");
			ListDocumentos.addMember(lblIngresosDelDa);
			lblIngresosDelDa.setSize("100%", "5%");
			
			lstReporteCaja.setSize("100%", "60%");
			ListGridField id =new ListGridField("id", "id");
			ListGridField NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
			ListGridField TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
			ListGridField Fecha =new ListGridField("Fecha", "Fecha");
			ListGridField FechadeExpiracion=new ListGridField("FechadeExpiracion", "Fechade Expiracion");
			ListGridField Vendedor=new ListGridField("Vendedor", "Vendedor");
			ListGridField Cliente =new ListGridField("Cliente", "Cliente");
			ListGridField Autorizacion =new ListGridField("Autorizacion", "Autorizacion");
			ListGridField NumPagos =new ListGridField("NumPagos", "Numero de Pagos");
			ListGridField Estado =new ListGridField("Estado", "Estado");
			ListGridField SubTotal =new ListGridField("SubTotal", "SubTotal");
			lstReporteCaja.setFields(new ListGridField[] { id,NumRealTransaccion ,TipoTransaccion, Fecha,FechadeExpiracion , Vendedor,Cliente,
					Autorizacion,NumPagos,Estado,SubTotal});
			ListDocumentos.addMember(lstReporteCaja);
			
			HStack hStackDoc = new HStack();
			hStackDoc.setSize("100%", "5%");
			
			lstReporteCaja.addRecordDoubleClickHandler(new ManejadorBotones("documento"));
			hStackDoc.addMember(btnGenerarReporte);
			btnGenerarReporte.addClickHandler(new ManejadorBotones("generar")) ;
			
			IButton btnImprimir = new IButton("Imprimir");
			hStackDoc.addMember(btnImprimir);
			
			//btnLimpiar = new IButton("Limpiar");assssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
			hStackDoc.addMember(btnLimpiar);
			btnLimpiar.addClickHandler(new ClickHandler() {  
	            public void onClick(ClickEvent event) { 
	            	SC.say("limpiar");
				
	            }
			}); 
			
			IButton btnVerDocumento = new IButton("Ver Documento");
			hStackDoc.addMember(btnVerDocumento);
			btnVerDocumento.addClickHandler(new ManejadorBotones("verDto")) ;
			
			
			ListDocumentos.addMember(hStackDoc);
		}catch(Exception e ){SC.say("Error: "+e);}
			// fin Lista de Documentos
	}
	
	private class ManejadorBotonesServicio implements ChangedHandler{
		String indicador = "";


		ManejadorBotonesServicio(String nombreBoton) {
			this.indicador = nombreBoton;
		}


		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("Servicio")){
				if(chkServicio.getValueAsBoolean()){
					cmbBuscar.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n");		
					cmbBuscar.redraw();
				}else{
					cmbBuscar.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n",
							"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
					cmbBuscar.redraw();
				}
				if(chkProductoElaborado.getValueAsBoolean()){
					chkProductoElaborado.setValue(false);
				}

			}
			
		}
	}
	
	private class ManejadorBotonesProductoElaborado implements ChangedHandler{
		String indicador = "";


		ManejadorBotonesProductoElaborado(String nombreBoton) {
			this.indicador = nombreBoton;
		}


		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("ProductoElaborado")){
				if(chkServicio.getValueAsBoolean()){
					cmbBuscar.setValueMap("C\u00F3digo de Barras", "Descripci\u00F3n",
							"Ubicaci\u00F3n", "Categoria", "Unidad", "Marca");
					cmbBuscar.redraw();
				}
				if(chkServicio.getValueAsBoolean()){
					chkServicio.setValue(false);
				}
			}
		}


		
	}
	
	final AsyncCallback<List<CategoriaDTO>> objbacklstCat = new AsyncCallback<List<CategoriaDTO>>() {

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());

		}

		public void onSuccess(List<CategoriaDTO> result) {
			listcat = result;
			MapCategoria.clear();
			MapCategoria.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapCategoria.put(
						String.valueOf(result.get(i).getIdCategoria()), result
								.get(i).getCategoria());
			}
			cmbCategoria.setValueMap(MapCategoria);
		}

	};
	final AsyncCallback<List<MarcaDTO>> objbacklstMarca = new AsyncCallback<List<MarcaDTO>>() {

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<MarcaDTO> result) {
			listmar = result;
			MapMarca.clear();
			MapMarca.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapMarca.put(String.valueOf(result.get(i).getIdMarca()), result
						.get(i).getMarca());
			}
			cmbMarca.setValueMap(MapMarca);

		}
	};
	final AsyncCallback<List<UnidadDTO>> objbacklstUnidad = new AsyncCallback<List<UnidadDTO>>() {

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}

		public void onSuccess(List<UnidadDTO> result) {
			listuni = result;
			MapUnidad.clear();
			MapUnidad.put("", "");
			for (int i = 0; i < result.size(); i++) {
				MapUnidad.put(String.valueOf(result.get(i).getIdUnidad()),
						result.get(i).getUnidad());
			}
			cmbUnidad.setValueMap(MapUnidad);
		}
	};
	final AsyncCallback<List<BodegaDTO>> objbacklstBod = new AsyncCallback<List<BodegaDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<BodegaDTO> result) {
			MapBodega.clear();
			MapBodega.put("", "");
			listBodega = result;
			for (int i = 0; i < result.size(); i++) {
				MapBodega.put(String.valueOf(result.get(i).getIdBodega()),
						result.get(i).getBodega());
			}
			cmdBod.setValueMap(MapBodega);
		}

	};
	final AsyncCallback<List<TipoPrecioDTO>> objbacklstTip = new AsyncCallback<List<TipoPrecioDTO>>() {

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
			SC.say("Error no se conecta  a la base");

		}

		public void onSuccess(List<TipoPrecioDTO> result) {
			listTipoP = result;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),
					"display", "none");
		}

	};
	
	public void buscarL(){
		int ban=1;
		String nombre=searchFormProducto.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String tabla=searchFormProducto.getItem("cmbBuscar").getDisplayValue();
		String campo=null;
		if(tabla.equals("Ubicaci\u00F3n")){
			ban=3;
			tabla="codigoBarras";
		}else if(tabla.equals("C\u00F3digo de Barras")){
			ban=1;
			tabla="codigoBarras";
		}else if(tabla.equals("Descripci\u00F3n")||tabla.equals("")){
			ban=1;
			tabla="descripcion";
		}else if(tabla.equals("Marca")){
			ban=0;
			campo="marca";
			tabla="tblmarca";
		}else if(tabla.equals("Categoria")){
			ban=0;
			campo="categoria";
			tabla="tblcategoria";
		}else if(tabla.equals("Unidad")){
			ban=0;
			campo="nombre";
			tabla="tblunidad";
		}
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		if(ban==1){
			getService().listarProductoLike(nombre, tabla, listaCallback);
		}else if(ban==0){
			getService().listarProductoJoin(nombre, tabla, campo, 1, 0,-1,Factum.banderaStockNegativo,listaCallback);
		}
	}
	private class ManejadorBotones implements ClickHandler,RecordDoubleClickHandler,KeyPressHandler,FormItemClickHandler,ChangedHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
        
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario")){
					Tipo="5";
				}else if(cmbDocumento.getDisplayValue().equals("Gastos")){
					Tipo="7";
				}
				//SC.say(idCliente+" vendedor="+idVendedor+" Tipo "+Tipo);
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equals("grabar"))
			{
				if(dynamicForm.validate()){
					LinkedList<SerieDTO> series =new LinkedList<SerieDTO>();
					boolean ban=true;
					try{
						int count=ListSeries.getRecords().length;
						SC.say(String.valueOf(count));
						for(int i=0;i<count;i++){
							SerieDTO serie=new SerieDTO();
							DtocomercialDTO doc=new DtocomercialDTO();
							doc.setIdEmpresa(Factum.empresa.getIdEmpresa());
							doc.setEstablecimiento(Factum.getEstablecimientoCero());
							doc.setPuntoEmision(Factum.getPuntoEmisionCero());
							doc.setIdDtoComercial(Integer.parseInt((String)txtFacCompra.getValue()));
							serie.setIdDtoComercialC(doc);
							ProductoDTO prod=new ProductoDTO();
							prod.setIdEmpresa(Factum.empresa.getIdEmpresa());
							prod.setEstablecimiento(Factum.getEstablecimientoCero());
							prod.setIdProducto(Integer.parseInt((String)txtIdProducto.getValue()));
							serie.setTblproductos(prod);
							serie.setSerie(ListSeries.getRecord(i).getAttribute("serie"));
							serie.setFechaExpiracion(new Date());
//							if(serie.getSerie().length()!=13){
//								ban=false;
//							}else{
								series.add(serie);
							//}
						}
						if(ban){
							getService().grabarSeries(series,objback);
						}else{
							SC.say("Ingrese Series correctas, las series tienen una longitud de 13 d�gitos");
						}
					}catch(Exception e){
						SC.say("Error en Pantalla "+e.getMessage());
					}
				}
				
			}else if(indicador.equals("limpiarFormulario")){
				SC.say("limpiar");
				
			}else if(indicador.equals("limpiar")){
				idCliente="";
				idVendedor="";
				idProducto="";
				txtCliente.setValue("");
				txtVendedor.setValue("");
				txtProducto.setValue("");
				btnGenerarReporte.setDisabled(false);
			}else if(indicador.equals("seleccionar")){
				ProductoDTO prod=new ProductoDTO();
				//prod=frmproducto.getProd();
				SC.say(prod.getDescripcion()+ "en serie");
			}else if(indicador.equalsIgnoreCase("generar")){
				datosfrm();
				if(!idCliente.equals("") && !idVendedor.equals("") && idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasVendedorCliente(fechaI, fechaF, idCliente, idVendedor,Tipo, listaCallbackDoc);
				}else if(idCliente.equals("") && idVendedor.equals("") && idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturas(fechaI, fechaF,Tipo, listaCallbackDoc);
				}else if(!idCliente.equals("") && idVendedor.equals("") && idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasCliente(fechaI, fechaF,idCliente,Tipo, listaCallbackDoc);
				}else if(idCliente.equals("") && !idVendedor.equals("") && idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasVendedor(fechaI, fechaF,idVendedor,Tipo, listaCallbackDoc);
				}else if(idCliente.equals("") && idVendedor.equals("") && !idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasProducto(fechaI, fechaF, idProducto+"", Tipo, listaCallbackDoc);
				}if(!idCliente.equals("") && !idVendedor.equals("") && !idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasVendedorClienteProducto(fechaI, fechaF, idCliente, idVendedor, idProducto, Tipo, listaCallbackDoc);
				}else if(!idCliente.equals("") && idVendedor.equals("") && !idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasClienteProducto(fechaI, fechaF,idCliente, idProducto,Tipo, listaCallbackDoc);
				}else if(idCliente.equals("") && !idVendedor.equals("") && !idProducto.equals("")){
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarFacturasVendedorProducto(fechaI, fechaF,idVendedor, idProducto,Tipo, listaCallbackDoc);
				}
			}else if(indicador.equalsIgnoreCase("limpiar")){
				idCliente="";
				idVendedor="";
				idProducto="";
				txtCliente.setValue("");
				txtVendedor.setValue("");
				txtProducto.setValue("");
				btnGenerarReporte.setDisabled(false);
			}else if(indicador.equalsIgnoreCase("verDto")){
				Integer doc=0;
				try{
					doc=Integer.parseInt(lstReporteCaja.getSelectedRecord().getAttributeAsString("id"));
					winFactura1 = new Window();
	            	winFactura1.setWidth(1100);  
	            	winFactura1.setHeight(700);  
	            	winFactura1.setTitle("Documento Comercial");  
	            	winFactura1.setShowMinimizeButton(false);  
	            	winFactura1.setIsModal(true);  
	            	winFactura1.setShowModalMask(true);  
	            	winFactura1.centerInPage();  
					//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	            	Factura fac=new Factura(Integer.parseInt(Tipo),doc);//lstReporteCaja.getRecord(col).getAttribute("idDocumento")));
					winFactura1.addCloseClickHandler(new CloseClickHandler() {  
		                public void onCloseClick(CloseClientEvent event) {  
		                	winFactura1.destroy();  
		                }  
		            });
					VLayout form = new VLayout();  
		            form.setSize("100%","100%"); 
		            form.setPadding(5);  
		            form.setLayoutAlign(VerticalAlignment.BOTTOM);
		            form.addMember(fac);
		            winFactura1.addItem(form);
		            winFactura1.show();
				}catch(Exception e){
					SC.say("Primero debe seleccionar una fila");
				}
				
			}else if(indicador.equals("aniadir")){
				ListGridRecord listGridRecord= new ListGridRecord();
				listGridRecord.setAttribute("serie", "Ingrese una serie");
				ListSeries.addData(listGridRecord);
				ListSeries.selectRecord(ListSeries.getRecords().length);
            	
			}else if(indicador.equals("quitar")){
				ListSeries.removeSelectedData();
			}else if(indicador.equals("listarSerie")){
				SC.say("listarSerie");
			}
			
		}
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			//frmproducto.getListProductoDTO();
			
			if(indicador.equals("producto")){
				SC.say((String) lstProductos.getSelectedRecord().getAttribute("descripcion"));
				txtIdProducto.setValue((String)  lstProductos.getSelectedRecord().getAttribute("idProducto"));
				txtDescripcion.setValue((String) lstProductos.getSelectedRecord().getAttribute("descripcion"));
				//SC.say(frmproducto.lstProductos.getSelectedRecord().getAttributeAsString("descripcion")+"   en serie");
				//lstProductos.setVisible(false);
				listadoProd.setVisible(false);
				//lstProductos.destroy();
				listadoProd.destroy();
				//winDetalle.setVisible(false);
				//winDetalle.destroy();
				
			}else if(indicador.equals("documento")){
				//txtFacCompra.setValue(lstReporteCaja.getSelectedRecord().getAttribute("id"));
				txtFacCompra.setValue(form.lstReporteCaja.getSelectedRecord().getAttribute("id"));
				//winfacturas = new Window();
				winfacturas.setVisible(false);
				winfacturas.destroy();
			}
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equals("productos")){
				/*winDetalle = new Window(); 
				winDetalle.setWidth(930);  
				winDetalle.setHeight(610);  
				winDetalle.setTitle("Listado de Producto ");  
				winDetalle.setShowMinimizeButton(false);  
				winDetalle.setIsModal(true);  
				winDetalle.setShowModalMask(true);  
				winDetalle.centerInPage();  
				winDetalle.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {
						winDetalle.destroy();  
					}   
                });
				VLayout form = new VLayout();  
                form.setSize("100%","100%"); 
                form.setPadding(5);  
                form.setLayoutAlign(VerticalAlignment.BOTTOM);
                //frmproducto.lstProductos.addRecordDoubleClickHandler(new ManejadorBotones("producto"));
                ListaProducto.setSize("100%","100%"); 
                form.addMember(ListaProducto);
                form.redraw();
                winDetalle.addItem(form);
                winDetalle.show();*/
                
			}else if(indicador.equals("facturas")){
				
				winfacturas = new Window();
				winfacturas.setWidth(930);  
				winfacturas.setHeight(610);  
				winfacturas.setTitle("Listado de Documentos ");  
				winfacturas.setShowMinimizeButton(false);  
				winfacturas.setIsModal(true);  
				winfacturas.setShowModalMask(true);  
				winfacturas.centerInPage();
				form=new frmReporteCaja("Facturas de Compra"); 
				winfacturas.addCloseClickHandler(new CloseClickHandler() {  
                    public void onCloseClick(CloseClientEvent event) {
                    	winfacturas.destroy();  
					}   
                });
				form.lstReporteCaja.addRecordDoubleClickHandler(new ManejadorBotones("documento"));
				form.setSize("100%","100%"); 
	            form.setPadding(5);  
	            form.setLayoutAlign(VerticalAlignment.BOTTOM);
				//winfacturas.addItem(ListDocumentos);
				winfacturas.addItem(form);
				winfacturas.show();
				
			}else if(indicador.equals("Buscar")){
				buscarL();
			}		
			
		}
		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(indicador.equalsIgnoreCase("producto")){
				if(event.getKeyName().equalsIgnoreCase("enter")){
					buscarL();
				}
			}
			
			
		}

		@Override
		public void onChanged(ChangedEvent event) {
			if (indicador.equalsIgnoreCase("Bodega")) {
				boolean ban;
				if (cmbBuscar.getDisplayValue().equals("Ubicaci\u00F3n")) {
					ban = true;

				} else {
					ban = false;
				}
				cmdBod.setShowHint(true);// muestra el combo de bodegas para
											// seleccionnar una
				cmdBod.setVisible(ban);

			} else if (indicador.equalsIgnoreCase("Buscar")) {
	
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display", "block");
				if(chkServicio.getValueAsBoolean()){
					getService().listarProductoJoin(cmdBod.getDisplayValue(),"tblbodegas", "nombre", 2, 0,-1,Factum.banderaStockNegativo,listaCallback);
				}else{
					getService().listarProductoJoin(cmdBod.getDisplayValue(),"tblbodegas", "nombre", 1, 0,-1,Factum.banderaStockNegativo,listaCallback);
				}
				//SC.say("Buscar servicio");
			}
			// TODO Auto-generated method stub
			
		}
		
	}
	final AsyncCallback<List<DtocomercialDTO>>  listaCallbackDoc=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			idCliente="";
			idVendedor="";
			idProducto="";
			txtCliente.setValue("");
			txtVendedor.setValue("");
			txtProducto.setValue("");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
			ListGridRecord[] listado = new ListGridRecord[result.size()];		
			for(int i=0;i<result.size();i++){
				DtocomercialRecords doc =new DtocomercialRecords(result.get(i));
				listado[i]=(new DtocomercialRecords(result.get(i)));
			}
			lstReporteCaja.setData(listado);
	        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	        btnGenerarReporte.setDisabled(true);
	    }
	};
	final AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>  listaCallback=new AsyncCallback<LinkedHashMap<Integer, ProductoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(LinkedHashMap<Integer, ProductoDTO> result) {
			RecordList listado = new RecordList();
			Iterator iter = result.values().iterator();
            while (iter.hasNext()) {
            	Producto pro =new Producto((ProductoDTO)iter.next());
            	pro.setMarca(MapMarca.get(pro.getAttribute("marca")));
            	pro.setCategoria(MapCategoria.get(pro.getAttribute("categoria")));
            	pro.setUnidad(MapUnidad.get(pro.getAttribute("unidad")));
            	listado.add(pro);
            }
            lstProductos.setData(new RecordList());
            lstProductos.setData(listado);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<List<SerieDTO>>  listaCallbackSeries=new AsyncCallback<List<SerieDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<SerieDTO> result) {
			RecordList listado = new RecordList();
			for(int i=0;i<result.size();i++) {
            	SerieRecords serie =new SerieRecords(result.get(i));
            	listado.add(serie);
            }
			txtIdProducto.setValue(result.get(0).getTblproductos().getIdProducto());
			txtDescripcion.setValue(result.get(0).getTblproductos().getDescripcion());
			txtFacCompra.setRequired(true);
			ListSeries.setData(new RecordList());
			ListSeries.setData(listado);
            DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			SC.say(result);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<SerieDTO>  objbackDTO=new AsyncCallback<SerieDTO>(){
		public void onFailure(Throwable caught) {
			RecordList listado = new RecordList();
			ListSeries.setData(listado);
        	ListSeries.redraw();
            SC.say("Serie No Encontrada");
		}
		public void onSuccess(SerieDTO result) {
			ListSeries.setFields(new ListGridField[] {listSerie,listCodigoBarras,listDescripcion,listNumFacturaC,listNumFacturaV});
			txtIdProducto.setValue(result.getTblproductos().getIdProducto());
			txtDescripcion.setValue(result.getTblproductos().getDescripcion());
			txtSerie.setValue(result.getSerie());
			RecordList listado = new RecordList();
			SerieRecords serie =new SerieRecords(result);
        	listado.add(serie);
        	ListSeries.setData(listado);
        	ListSeries.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	
}
