package com.giga.factum.client;

import java.sql.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.DtocomercialTbltipopagoDTO;
import com.giga.factum.client.DTO.TblpagoDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DragMoveEvent;
import com.smartgwt.client.widgets.events.DragMoveHandler;
import com.smartgwt.client.widgets.events.DragStopEvent;
import com.smartgwt.client.widgets.events.DragStopHandler;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.IButton;

public class frmIngersos extends VLayout{
	DateTimeItem txtFecha = new DateTimeItem("txtFecha", "Fecha");
	TextItem txtNumEgreso = new TextItem("txtNumEgreso", "Num Ingreso");
	TextItem txtConcepto = new TextItem("txtConcepto", "Concepto");
	ListGrid ListDetalle = new ListGrid();
	ComboBoxItem cmbFormaPago = new ComboBoxItem("cmbFormaPago", "Forma de Pago");
	List<TblpagoDTO> pagos=null;
	HStack hStack = new HStack();
	IButton btnGrabarEgreso = new IButton("Grabar INGRESO"); 
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();
	HStack hStackSuperior = new HStack(); 
	User usuario = new User();
	NumberFormat formatoDecimalN= NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	public frmIngersos() {
		
		dynamicForm.setHeight("73px");
		txtConcepto.setColSpan(4);
		txtConcepto.setRequired(true);
		
		txtNumEgreso.setRequired(true); 
		
		txtFecha.setRequired(true);
		txtFecha.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFecha.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFecha.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		cmbFormaPago.setValueMap("Efectivo","Banco","Tarjeta de Credito","Nota de Credito","Retencion");
		cmbFormaPago.setDefaultToFirstOption(true);
		cmbFormaPago.setRequired(true);
		
		//dynamicForm.setFields(new FormItem[] { txtNumEgreso,txtFecha, txtConcepto, cmbFormaPago});
		//addMember(dynamicForm);
		dynamicForm.setFields(new FormItem[] { txtNumEgreso,txtFecha, });
		dynamicForm2.setFields(new FormItem[] {txtConcepto, cmbFormaPago});
		dynamicForm.setWidth("20%");
		dynamicForm2.setWidth("80%");
		hStackSuperior.setSize("100%", "20%");
		hStackSuperior.addMember(dynamicForm);
		hStackSuperior.addMember(dynamicForm2);
		addMember(hStackSuperior);
		ListGridField lstValor=new ListGridField("lstValor", "Valor",50);
		ListGridField lstFechaV =new ListGridField("lstFechaV", "FechaVencimiento",100);
		ListGridField lstCliente =new ListGridField("lstCliente", "Cliente"); 
		ListGridField lstId =new ListGridField("lstId", "Id Pago",0);
		ListGridField lstValorPagar =new ListGridField("lstValorPagar", "Valor a Pagar",75);
		lstValorPagar.setAlign(Alignment.RIGHT);
		lstValorPagar.setType(ListGridFieldType.FLOAT);
		lstValorPagar.setShowGridSummary(true);
		 
		lstValorPagar.setSummaryFunction(new SummaryFunction() {

            public Object getSummaryValue(Record[] records, ListGridField field) {
                double sum = 0;
                for (int i = 0; i < records.length; i++) {
                    sum +=  Double.valueOf(records[i].getAttribute("lstValorPagar"));
                }
                if (sum < 0) {
                    return "<span style='color:red'>" +CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum)+ "</span>";
                } else {
                    return "<span style='color:green'>" + CValidarDato.getDecimal(Factum.banderaNumeroDecimales,sum) + "</span>";
                }
            }
        });
		lstValor.setAlign(Alignment.RIGHT);
		ListDetalle.setFields(lstId,lstCliente, lstFechaV,lstValor,lstValorPagar);  
		ListDetalle.setSize("50%", "100%");
		ListDetalle.setShowRowNumbers(true);
		ListDetalle.setShowGridSummary(true);
		ListDetalle.setCanReorderFields(true);  
		ListDetalle.setCanDragRecordsOut(true);  
		ListDetalle.setCanAcceptDroppedRecords(true);  
		ListDetalle.setDragDataAction(DragDataAction.MOVE);  
		ListDetalle.setCanRemoveRecords(true); 
		ListDetalle.setCanEdit(true);
		ListDetalle.addDropHandler(new ManejadorBotones(""));
		//ListDetalle.addDragMoveHandler(new ManejadorBotones(""));
		//ListDetalle.addDragStopHandler(new ManejadorBotones(""));
		addMember(ListDetalle);
		btnGrabarEgreso.addClickHandler(new ManejadorBotones("grabar"));
		hStack.addMember(btnGrabarEgreso);
		getService().ultimoIngreso(objbackInt);
		getService().getUserFromSession(callbackUser);
		addMember(hStack);
		
	}
	private class ManejadorBotones implements ClickHandler ,DropHandler{
		String indicador="";
		ManejadorBotones(String ind){
			this.indicador=ind;
		} 
		@Override
		public void onClick(ClickEvent event) {
			if(indicador.equals("grabar")){
				try{
					if(dynamicForm.validate()){
						LinkedList<TblpagoDTO> Listpago=new LinkedList<TblpagoDTO>();
						for(int i=0;i<ListDetalle.getRecordList().getLength();i++){
							String idpago=String.valueOf(ListDetalle.getRecord(i).getAttribute("lstId")); 
							double valor=Double.valueOf(ListDetalle.getRecord(i).getAttribute("lstValor"));
								for(int j=0;j<pagos.size();j++){
									if(idpago.equals(String.valueOf(pagos.get(j).getIdPago()))){
										TblpagoDTO pagodto=new TblpagoDTO();
										pagodto=pagos.get(j);
										int tipoPago=1;
										if(cmbFormaPago.getDisplayValue().equals("Efectivo")){
											tipoPago=1; 
										}else if(cmbFormaPago.getDisplayValue().equals("Banco")){
											tipoPago=7;
										}else if(cmbFormaPago.getDisplayValue().equals("Tarjeta de Credito")){
											tipoPago=6;
										}else if(cmbFormaPago.getDisplayValue().equals("Nota de Credito")){
											tipoPago=5;
										}  
										pagodto.setIdEmpresa(Factum.empresa.getIdEmpresa());
										pagodto.setEstablecimiento(Factum.getEstablecimientoCero());
										pagodto.setValorPagado(Double.valueOf(ListDetalle.getRecord(i).getAttribute("lstValorPagar")));
										pagodto.setNumEgreso(Integer.parseInt((txtNumEgreso.getDisplayValue()))); 
										pagodto.setConcepto(txtConcepto.getDisplayValue().toUpperCase());
										pagodto.setFormaPago(cmbFormaPago.getDisplayValue());
										DtocomercialTbltipopagoDTO tipo=new DtocomercialTbltipopagoDTO();
										tipo.setTipoPago(tipoPago);
										tipo.setObservaciones("PAGADO CON INGRESO #"+pagodto.getNumEgreso());
										pagodto.setEstado('1');
										Date d=null;
										String da=String.valueOf(txtFecha.getDisplayValue()).replace('/','-');
										pagodto.setFechaRealPago(d.valueOf(da));
										HashSet<DtocomercialTbltipopagoDTO> tipospago = new HashSet<DtocomercialTbltipopagoDTO>(0);
										tipospago.add(tipo); 
										pagodto.getTbldtocomercial().setTbldtocomercialTbltipopagos(tipospago);
										pagodto.setValor(valor);
										Listpago.add(pagodto);
										SC.say(String.valueOf(tipoPago));
										break;
									}
							}
						} 
						if(Listpago.isEmpty()){
							SC.say("Error  No se puede Guardar los Pagos Verifique valores o seleccione un pago");
						}else{
							getService().grabarEgreso(Listpago, usuario,objbackString);
						}						
				
					}
				}catch(Exception e){
					SC.say("error en interfaz"+e.getMessage()+" "+String.valueOf(txtFecha.getDisplayValue()).replace('/','-'));
				}
				
			}
		}
		@Override
		public void onDrop(DropEvent event) {
			for(int i=0;i<ListDetalle.getRecords().length;i++){
				ListDetalle.getRecord(i).setAttribute("lstValorPagar", ListDetalle.getRecord(i).getAttribute("lstValor"));
				ListDetalle.refreshRow(i);
			}	
		}
	}
	final AsyncCallback<Integer>  objbackInt=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		
		@Override
		public void onSuccess(Integer result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			txtNumEgreso.setValue(result+1);
		}
	};
	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
			ListGridRecord[] selectedRecords = ListDetalle.getRecords();  
            for(ListGridRecord rec: selectedRecords) {  
            	ListDetalle.removeData(rec);  
            }  
            txtFecha.setValue("");
            txtConcepto.setValue("");
            txtNumEgreso.setValue("");
            getService().ultimoIngreso(objbackInt);
		}
	};
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

        public void onSuccess(User result) {
        	if (result == null) {//La sesion expiro

				SC.say("Advertencia", "Expiro su sesion, debe ingresar nuevamente", new BooleanCallback() {

					public void execute(Boolean value) {
						if (value) {
							com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
							//getService().LeerXML(asyncCallbackXML);
						}
					}
				});
			}else{
            //Segun sea el tipo de Usuario que este grabando la factura sabemos si se deben grabar
        	//los asientos contables respectivos o no
        	//El tipo de usuario 0 va a ser el ADMINISTRADOR y quien podra CONFIRMAR el dto
        		usuario=result;
        		//SC.say(usuario.getUserName());
        	//funcionBloquear(true);
			}
        }

        public void onFailure(Throwable caught) {
            
        	com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
			SC.say("No se puede obtener el nombre de usuario");
        }
    };
}
