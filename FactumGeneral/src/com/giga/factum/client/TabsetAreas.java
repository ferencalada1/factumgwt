package com.giga.factum.client;

import java.util.ArrayList;
import java.util.List;

import com.giga.factum.client.DTO.AreaDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.TabSet;

public class TabsetAreas extends TabSet{

	public TabsetAreas(){					
        setWidth100();  
        setHeight100();
        setTabBarPosition(Side.TOP);
        setTabBarAlign(Side.RIGHT);
	}
	
	
	
}
