package com.giga.factum.client;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.smartgwt.client.widgets.layout.VLayout;
import com.giga.factum.client.DTO.CargoDTO;
import com.giga.factum.client.DTO.ClienteDTO;
import com.giga.factum.client.DTO.EmpleadoDTO;
import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.regGrillas.PersonaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.ExpansionMode;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FloatItem;


public class frmListVendedor extends VLayout {
	boolean ban=false;
	SearchForm searchForm = new SearchForm();
    String pantalla="";
	ListGrid lstEmpleado = new ListGrid();
	List<CargoDTO> listCargo=null;
	LinkedHashMap<String, String> MapCargo = new LinkedHashMap<String, String>();
	TabSet tabSet = new TabSet();
	Label lblRegistrosVendedor = new Label("# Registros");
	int contador=20;
	int registros=0;
	
	public frmListVendedor() {
		getService().numeroRegistrosPersona("tblempleados", objbackI);
		PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
		PickerIcon buscarPickerlst = new PickerIcon(PickerIcon.SEARCH);
		buscarPickerlst.addFormItemClickHandler(new ManejadorBotones("buscar"));
		buscarPicker.addFormItemClickHandler(new ManejadorBotones("Buscarfrm"));
		RegExpValidator regExpValidator = new RegExpValidator();  
        regExpValidator.setExpression("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");
        FloatRangeValidator floatRangeValidator = new FloatRangeValidator(); 
        setSize("900", "600");
		tabSet.setSize("100%", "100%");
		
  
        MatchesFieldValidator matchesValidator = new MatchesFieldValidator();  
        matchesValidator.setOtherField("password");  
        matchesValidator.setErrorMessage("Passwords do not match");
		
		Tab tabListado = new Tab("Listado de Empleados");
		

		VLayout layout_1 = new VLayout();
		layout_1.setSize("100%", "100%");
         
        HLayout hLayout = new HLayout();
        hLayout.setSize("100%", "5%");
         
        searchForm.setSize("88%", "100%");
        searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
        searchForm.setNumCols(4);
        ComboBoxItem cbmBuscarLst = new ComboBoxItem("cbmBuscarLst", "Buscar por:");
        cbmBuscarLst.setLeft(162);
        cbmBuscarLst.setTop(4);
        cbmBuscarLst.setHint("Buscar por");
        cbmBuscarLst.setShowHintInField(true);
        cbmBuscarLst.setValueMap("Cedula","Nombre Comercial","Razon Social","Direcci\u00F3n");
        
        TextItem txtBuscarLst=new TextItem();
        txtBuscarLst.setName("txtBuscarLst");
        txtBuscarLst.setLeft(6);
        txtBuscarLst.setTop(4);
        txtBuscarLst.setHint("Buscar");
        txtBuscarLst.setShowHintInField(true);
        txtBuscarLst.setIcons(buscarPickerlst);
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        buscarPicker.addFormItemClickHandler(new ManejadorBotones("buscarfrm"));
        StaticTextItem staticTextItem = new StaticTextItem("newStaticTextItem_3", "New StaticTextItem");
        staticTextItem.setLeft(648);
        staticTextItem.setTop(6);
        staticTextItem.setWidth(56);
        staticTextItem.setHeight(20);
        searchForm.setFields(new FormItem[] { txtBuscarLst, cbmBuscarLst, staticTextItem});
        hLayout.addMember(searchForm);
        HStack hStack = new HStack();
        hLayout.addMember(hStack);
        hStack.setSize("12%", "100%");
        TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        //TransferImgButton btnEliminarLst = new TransferImgButton(TransferImgButton.DELETE);
        
        btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        
        
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        //hStack.addMember(btnEliminarLst);
        
        layout_1.addMember(hLayout);
        lstEmpleado.setCanExpandRecords(true);  
        lstEmpleado.setExpansionMode(ExpansionMode.DETAIL_FIELD);  
        lstEmpleado.setDetailField("background");  
        
        lstEmpleado.setSize("100%", "80%");
        ListGridField  Cedula=new ListGridField("Cedula", "C\u00E9dula/RUC",150);
        lstEmpleado.setFields(new ListGridField[] { Cedula, new ListGridField("NombreComercial", "Nombre Comercial",250), new ListGridField("RazonSocial", "Razon Social",250), 
        		new ListGridField("Telefonos", "Tel\u00E9fono",150),new ListGridField("Direccion", "Direcci\u00F3n",400),new ListGridField("Cargo", "Cargo",150),
        		new ListGridField("NivelA", "Nivel de Acceso",100),new ListGridField("FechaContratacion", "Fecha de Contrataci\u00F3n",100)});
        layout_1.addMember(lstEmpleado);
        lblRegistrosVendedor.setSize("100%", "4%");
	    layout_1.addMember(lblRegistrosVendedor);
	    tabListado.setPane(layout_1);
		tabSet.addTab(tabListado);
		addMember(tabSet);
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		contador=20;
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().ListEmpleados(0,contador, objbacklst);
		lstEmpleado.addRecordDoubleClickHandler(new ManejadorBotones("mod"));
		getService().numeroRegistrosPersona("tblempleados", objbackI);
		
	
	
	}
	public void buscarL(){
		try{
			String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
			String tabla=searchForm.getItem("cbmBuscarLst").getDisplayValue();
			String campo=null;
			if(tabla.equals("Cedula")){
				campo="cedula_ruc";
			}else if(tabla.equals("Nombre Comercial")){
				campo="nombreComercial";
			}else if(tabla.equals("Razon Social")||tabla.equals("")){
				campo="razonSocial";
			}else if(tabla.equals("Direcci\u00F3n")){
				campo="direccion";
			}
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().findJoin(nombre, "tblempleados", campo, objbacklst); 
			
		}catch(Exception e){
			SC.say(e.getMessage());
		}
			
	}
	
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers
	}*/
	
	
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,contador, objbacklst);
					lblRegistrosVendedor.setText(contador+" de "+registros);
					
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,contador, objbacklst);
					
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,contador, objbacklst);
					lblRegistrosVendedor.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(registros-registros%20,registros, objbacklst);
					lblRegistrosVendedor.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().ListEmpleados(contador-20,contador, objbacklst);
					lblRegistrosVendedor.setText(contador+" de "+registros);
			}

			else if(indicador.equalsIgnoreCase("buscar")){
				
			}
		}
		
		
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			if(indicador.equalsIgnoreCase("buscar")){
					buscarL();
				
			}
		}

		@Override
		public void onRecordClick(RecordClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
			
		}
	}
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			
		} 

		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().ListEmpleados(0,contador, objbacklst);
			getService().numeroRegistrosPersona("tblempleados", objbackI);
			SC.say(result);
			
			
		}
	};
		
		
		
		final AsyncCallback<List<PersonaDTO>>  objbacklst=new AsyncCallback<List<PersonaDTO>>(){
			public void onFailure(Throwable caught) {
				SC.say(caught.toString());
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			}
			public void onSuccess(List<PersonaDTO> result) {
				getService().numeroRegistrosPersona("tblempleados", objbackI);
				if(contador>registros){
					lblRegistrosVendedor.setText(registros+" de "+registros);
				}else{
					lblRegistrosVendedor.setText(contador+" de "+registros);
				}
				
				ListGridRecord[] listado = new ListGridRecord[result.size()];
				for(int i=0;i<result.size();i++) {
					if(result.get(i).getEstado()=='1'){
						listado[i]=(new PersonaRecords((PersonaDTO)result.get(i)));
					}
				}
				lstEmpleado.setData(listado);
				lstEmpleado.redraw();
				DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		   }
		};
		
		final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
			public void onFailure(Throwable caught) {
				SC.say("Error dado:" + caught.toString());
				
			}
			public void onSuccess(Integer result) {
				registros=result;
			}
		};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}


}
