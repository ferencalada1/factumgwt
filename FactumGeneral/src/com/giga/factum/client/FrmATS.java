package com.giga.factum.client;


import java.util.Date;
import java.util.HashMap;


import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;


import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.google.gwt.http.client.RequestException;



import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;



public class FrmATS extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	//DynamicForm dynamicForm2 = new DynamicForm();
	HStack hStackSuperior = new HStack();
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	TextItem txtRuc =new TextItem("txtRuc", "<b>R.U.C</b>");
	TextItem txtNombres =new TextItem("txtNombreComercial", "<b>Razon Social</b>");
	TextItem txtNumEstab =new TextItem("txtNumEstab", "<b>Num. Estab.</b>");
	TextItem txtCodOper =new TextItem("txtCodOper", "<b>Cod. Oper.</b>");
	
	TextItem txtPeriodo =new TextItem("txtPeriodo", "<b>Periodo</b>");
	
	IButton btnGenerarReporteATS= new IButton("Generar ATS");	
	IButton btnGenerarReportePDF= new IButton("Generar Reporte");	
	int idreporte;
	Date fechaFactura;// Para la fecha del sistema
	String planCuentaID;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}	
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public FrmATS(int idReporte) 
	{
	try
		{
		idreporte=idReporte;
		if(idReporte==0){
			HTMLPane htmlPane = new HTMLPane();
			htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/ATS/ATS/page");  
			htmlPane.setContentsType(ContentsType.PAGE);
			addMember(htmlPane );
		}else{
			txtFechaInicial.setValue(fechaFactura);
			txtFechaFinal.setValue(fechaFactura);
//			txtProveedor.addChangedHandler(new ManejadorBotones("persona"));
			dynamicForm.setSize("20%", "100%");
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);

		  
			dynamicForm.setFields(new FormItem[] {txtFechaInicial, txtFechaFinal, txtRuc, txtNombres, txtNumEstab, txtCodOper, txtPeriodo});
			HStack hStackEspacio = new HStack();
			hStackEspacio.setSize("4%", "20%");
			HStack hStackEspacio1 = new HStack();
			hStackEspacio1.setSize("4%", "20%");
		
			
			hStackSuperior.setSize("100%", "35%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(hStackEspacio);
			hStackSuperior.addMember(hStackEspacio1);
//hStackSuperior.addMember(btnGrabar);		
			VStack vStackFinal = new VStack();
			vStackFinal.setSize("10%", "100%");
			HStack hStackSup1 = new HStack();
			hStackSup1.setSize("100%", "35%");
//	hStackSup1.setBackgroundColor("blue");
			HStack hStackSup2 = new HStack();
			hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
		
			vStackFinal.addMember(hStackSup1);
			vStackFinal.addMember(hStackSup2);
			hStackSuperior.addMember(vStackFinal);
			addMember(hStackSuperior);
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		hStack.addMember(btnGenerarReporteATS);
		hStack.addMember(btnGenerarReportePDF);
		addMember(hStack);
		
		//IButton btnVerDocumento = new IButton("Ver Documento");
		txtRuc.setValue("");
		txtNombres.setValue("");
		txtPeriodo.setValue("");
		
		btnGenerarReporteATS.setDisabled(false);
		btnGenerarReporteATS.addClickHandler(new ManejadorBotones("ATS"));

		btnGenerarReportePDF.setDisabled(false);
		btnGenerarReportePDF.addClickHandler(new ManejadorBotones("PDF"));
		
		/*Codigo Agregado por Xavier Zeas L.*/
//		if(idReporte==0){
//			txtPeriodo.setVisible(false);
//			btnGenerarReportePDF.setVisible(false);
//		}
		
		if(idReporte!=0){	
			btnGenerarReporteATS.setVisible(false);
			txtRuc.setVisible(false);
			txtNombres.setVisible(false);
			txtNumEstab.setVisible(false);
			txtCodOper.setVisible(false);
			if(idReporte!=1){
				txtPeriodo.setVisible(false);				
			}
		}
		/*Fin codigo agregado*/
		}
		}
	catch(Exception e)
	{
		}
}
	
	private class ManejadorBotones implements ClickHandler{
		private String indicador;
 		ManejadorBotones(String nombreBoton){
 			this.indicador=nombreBoton;
 		}
 		


		@Override
		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
 			if(indicador.equalsIgnoreCase("ATS")){
 				RequestBuilder rb=null;
 				/*
 				rb = new RequestBuilder(RequestBuilder.GET,
	       				"http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoATS+"/"+Factum.banderaNombreProyecto+"/"+Factum.banderaPHPATS+"?fechaInicial=" + txtFechaInicial.getDisplayValue()
	       						+ "&fechaFinal=" + txtFechaFinal.getDisplayValue() + "&numEstablecimiento=" + txtNumEstab.getValue().toString() + "&codigoOperativo="
	       						+ txtCodOper.getValue().toString() + "&ruc=" + txtRuc.getValue().toString() + "&razonSocial="
	       						+ txtNombreComercial.getValue().toString());
	       		*/
	       		try {
	       			rb.sendRequest(null, new RequestCallback() {
	       				public void onError(
	       						final com.google.gwt.http.client.Request request,
	       						final Throwable exception) {
	       					// Window.alert(exception.getMessage());
	       					SC.say("Error al generar el ATS: "
	       							+ exception);
	       				}


						@Override
						public void onResponseReceived(Request request,
								Response response) {
							// TODO Auto-generated method stub
							
						}

	       			});
	       		} catch (final Exception e) {
	       			// Window.alert(e.getMessage());
	       		} 				
 			}
 			if(indicador.equalsIgnoreCase("PDF")){
 				if(dynamicForm.validate()){
 					HashMap <String,Object> param=new HashMap<String,Object>();
 					String fileName=null;
 				   String fechaI=dynamicForm.getItem("txtFechaInicial").getDisplayValue();
	   			   String fechaF=dynamicForm.getItem("txtFechaFinal").getDisplayValue();
	   			   String periodo=dynamicForm.getItem("txtPeriodo").getDisplayValue();
	   			   fechaI=fechaI.replace('/', '-');
	   			   fechaF=fechaF.replace('/', '-');
	   			   if(idreporte==1){
	   			   param.put("desde", fechaI);
	   			   param.put("hasta", fechaF);
	   			   param.put("Periodo", periodo);
	   			   }else{
	   			   param.put("desde", fechaI);
	   			   param.put("hasta", fechaF);
	   			   }

	   			//case 1: mayorizacion
	   			//default: 2-comprobacion, 3-general, 4-estadoresultados
	   			   switch(idreporte){
	   			   case 1:
	   				   fileName="Mayorizacion";
	   				break;
	   			   case 2:
	   				   fileName="Comprobacion";
	   				break;
	   			   case 3:
	   				   fileName="General";
	   				break;
	   			   case 4:
	   				   fileName="EstadoResultados";
	   				break;	   				
	   			   }
	   			   getService().ejecutarProcedimiento(fileName, param, true, planCuentaID,Factum.banderaNombreBD,Factum.banderaUsuarioBD,Factum.banderaPasswordBD,objbackPDFjasper); //creamos el reporte   	  
 				}
 			} 			
			
		}
	}
	
	
/***/

	final AsyncCallback<String>  objbackPDFjasper=new AsyncCallback<String>(){

		public void onFailure(Throwable caught) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			String fileName=result;
			SC.say("Generado "+fileName);		
			//String url = ""+"factum\\pdfServlet?fileName=" + fileName;
			String baseURL=GWT.getModuleBaseURL();
			String url = baseURL+ "pdfServlet?fileName=" + fileName;
			Window.open( url, "", "");
		//	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			//getService().ListJoinPersona("tblclientes",0,contador, objbacklst);
		}
	};	
	
	

    }
	
	
	

	

	
	
	
