package com.giga.factum.client;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.PersonaDTO;
import com.giga.factum.client.DTO.ReporteVentasEmpleadoDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.layout.HStack;

public class frmReporteEmpleado extends VLayout{
	ListGrid listGrid = new ListGrid();
	DateTimeItem txtFechaInicial = new DateTimeItem("txtFechaInicial", "Fecha Inicial");
	DateTimeItem txtFechaFinal = new DateTimeItem("txtFechaFinal", "Fecha Final");
	DynamicForm dynamicForm = new DynamicForm();
	public frmReporteEmpleado() {
		setSize("800", "600");
		txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		txtFechaInicial.setMaskDateSeparator("-");
		txtFechaFinal.setMaskDateSeparator("-");
		txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
		txtFechaInicial.setUseTextField(true);
		txtFechaInicial.setValue(new Date());
		txtFechaFinal.setUseTextField(true);
		txtFechaFinal.setValue(new Date());
		
		
		dynamicForm.setSize("100%", "10%");
		txtFechaInicial.setRequired(true);
		txtFechaFinal.setRequired(true);
		dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal});
		addMember(dynamicForm);
		
		
		listGrid.setSize("100%", "85%");
		ListGridField lstEmpleado = new ListGridField("lstEmpleado", "Usuario");
		lstEmpleado.setAlign(Alignment.LEFT);
		ListGridField lstValor = new ListGridField("lstValor", "Valor Facturado");
		lstValor.setAlign(Alignment.RIGHT);
		ListGridField lstUtilidad = new ListGridField("lstUtilidad", "Utilidad Promedio");
		lstUtilidad.setAlign(Alignment.RIGHT);
		ListGridField lstUtilidadDetalle = new ListGridField("lstUtilidadDetalle", "Utilidad Detalle");
		lstUtilidadDetalle.setAlign(Alignment.RIGHT);
		listGrid.setFields(new ListGridField[] { lstEmpleado, lstValor, lstUtilidad,lstUtilidadDetalle});
		addMember(listGrid);
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		
		IButton btnGenerarReporte = new IButton("Generar Reporte");
		hStack.addMember(btnGenerarReporte);
		btnGenerarReporte.addClickHandler(new ManejadorBotones("generar"));
		IButton btnImprimir = new IButton("Imprimir");
		btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
		hStack.addMember(btnImprimir);
		addMember(hStack);
		
		
	}
	
	private class ManejadorBotones implements ClickHandler{
		String indicador="";
		String fechaI="";
		String fechaF="";
		
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}

		@Override
		public void onClick(ClickEvent event) {
		//	SC.say("click");
			if(indicador.equals("generar")){
				datosfrm();
				
				getService().ReporteVentasEmpleado(fechaI,fechaF,Factum.banderaIVA,objbackFloat);
			}
			if(indicador.equals("imprimir"))
			{

				Object[] a=new Object[]{dynamicForm,listGrid};
				Canvas.showPrintPreview(a);
			}
			
		}
	}
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	
	final AsyncCallback<LinkedList<ReporteVentasEmpleadoDTO>>  objbackFloat=new AsyncCallback<LinkedList<ReporteVentasEmpleadoDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		@Override
		public void onSuccess(LinkedList<ReporteVentasEmpleadoDTO> result) {
			try{
				ListGridRecord[] lst=new ListGridRecord[result.size()+1];
				float total=0,utilidad=0,utilidadD=0;
				int i=0;
				for(i=0;i<result.size();i++){
					lst[i]=new ListGridRecord();
					//lst[i].setAttribute("lstEmpleado", result.get(i).getEmpleado().getNombreComercial()+" "+result.get(i).getEmpleado().getRazonSocial());
//					lst[i].setAttribute("lstEmpleado", result.get(i).getEmpleado().getRazonSocial());
					lst[i].setAttribute("lstEmpleado", " "+result.get(i).getEmpleado().getNombreComercial());
					lst[i].setAttribute("lstValor",formatoDecimalN.format(result.get(i).getVentas()));
					lst[i].setAttribute("lstUtilidad",formatoDecimalN.format(result.get(i).getUtilidad()));
					lst[i].setAttribute("lstUtilidadDetalle",formatoDecimalN.format(result.get(i).getUtilidadDetalle()));

//					lst[i].setAttribute("lstValor",CValidarDato.getDecimal(Factum.banderaNumeroDecimales, result.get(i).getVentas()));
//					lst[i].setAttribute("lstUtilidad",CValidarDato.getDecimal(Factum.banderaNumeroDecimales,result.get(i).getUtilidad()));
//					lst[i].setAttribute("lstUtilidadDetalle",CValidarDato.getDecimal(Factum.banderaNumeroDecimales,result.get(i).getUtilidadDetalle()));

//					lst[i].setAttribute("lstValor",Math.rint(result.get(i).getVentas()*100)/100);
//					lst[i].setAttribute("lstUtilidad",Math.rint(result.get(i).getUtilidad()*100)/100);
//					lst[i].setAttribute("lstUtilidadDetalle",Math.rint(result.get(i).getUtilidadDetalle()*100)/100);
					total=total+result.get(i).getVentas();
					utilidad=utilidad+result.get(i).getUtilidad();
					utilidadD=utilidadD+result.get(i).getUtilidadDetalle();
				}
				total=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,total));
				utilidad=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,utilidad));
				utilidadD=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,utilidadD));
//				total=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,total));
//				utilidad=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,utilidad));
//				utilidadD=(float) (CValidarDato.getDecimal(Factum.banderaNumeroDecimales,utilidadD));
//				total=(float) (Math.rint(total*100)/100);
//				utilidad=(float) (Math.rint(utilidad*100)/100);
//				utilidadD=(float) (Math.rint(utilidadD*100)/100);
				lst[i+1]=new ListGridRecord();
				lst[i+1].setAttribute("lstEmpleado","TOTALES");
				lst[i+1].setAttribute("lstValor",formatoDecimalN.format(total));
				lst[i+1].setAttribute("lstUtilidad",formatoDecimalN.format(utilidad));
				lst[i+1].setAttribute("lstUtilidadDetalle",formatoDecimalN.format(utilidadD));
				listGrid.setData(lst); 
				listGrid.redraw();

			}catch(Exception e){
				SC.say("Error en pantalla "+e.getMessage());
			}
						
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}

}
