package com.giga.factum.client;

import com.smartgwt.client.util.SC;

public class CValidarDato {
	public static final int num_provincias = 24;
    private static int[] coeficientes = {4,3,2,7,6,5,4,3,2};
    private static int constante = 11;
    public static Boolean validacionCedula(String cedula){
        try{
        //verifica que los dos primeros d�gitos correspondan a un valor entre 1 y NUMERO_DE_PROVINCIAS
        int prov = Integer.parseInt(cedula.substring(0, 2));
        if (!((prov > 0) && (prov <= num_provincias))) {
            //addError(�La c�dula ingresada no es v�lida�);
            System.out.println("Error: cedula ingresada mal");
            return false;
        }

        //verifica que el �ltimo d�gito de la c�dula sea v�lido
        int[] d = new int[10];
        //Asignamos el string a un array
        for (int i = 0; i < d.length; i++) {
        d[i] = Integer.parseInt(cedula.charAt(i) + "");
        }

        int imp = 0;
        int par = 0;

        //sumamos los duplos de posici�n impar
        for (int i = 0; i < d.length; i += 2) {
        d[i]=((d[i]*2)>9)?((d[i]*2)-9):(d[i]*2);
        imp += d[i];
        }

        //sumamos los digitos de posici�n par
        for (int i = 1;i<(d.length-1);i+=2) {
            par += d[i];
        }

        //Sumamos los dos resultados
        int suma = imp + par;

        //Restamos de la decena superior
        int d10 = Integer.parseInt(String.valueOf(suma + 10).substring(0, 1) +
        "0")- suma;

        //Si es diez el d�cimo d�gito es cero
        d10 = (d10 == 10) ? 0 : d10;

        //si el d�cimo d�gito calculado es igual al digitado la c�dula es correcta
        if (d10 == d[9]) {
            return true;
        }else {
            //addError(�La c�dula ingresada no es v�lida�);
            return false;
        }
        }catch(Exception e){
            return false;
        }
    }
    public static Boolean validaRucEP(String ruc){
        try{
        int prov = Integer.parseInt(ruc.substring(0, 2));
        boolean val = false;

        if (!((prov > 0) && (prov <= num_provincias))) {
            return val;
        }

        Integer v1,v2,v3,v4,v5,v6,v7,v8,v9;
        Integer sumatoria;
        Integer modulo;
        Integer digito;
        Integer sustraendo;
        int[] d = new int[ruc.length()];

        for (int i = 0; i < d.length; i++) {
            d[i] = Integer.parseInt(ruc.charAt(i) + "");
        }

        v1 = d[0]* 3;
        v2 = d[1]* 2;
        v3 = d[2]* 7;
        v4 = d[3]* 6;
        v5 = d[4]* 5;
        v6 = d[5]* 4;
        v7 = d[6]* 3;
        v8 = d[7]* 2;
        v9 = d[8];

        sumatoria = v1+v2+v3+v4+v5+v6+v7+v8;
        modulo = sumatoria % 11;
        sustraendo = modulo * 11;
        digito = 11-(sumatoria - sustraendo);
        System.out.println("Digito RUC       ?> "+digito);
        System.out.println("Digito Calculado ?> "+v9);

        if(digito == v9){
            val = true;
        }else
            val = false;
        return val;
        }catch(Exception e){
            return false;
        }
    }
    public static Boolean validacionRUC(String ruc){
        try{
        //verifica que los dos primeros d�gitos correspondan a un valor entre 1 y NUMERO_DE_PROVINCIAS
        int prov = Integer.parseInt(ruc.substring(0, 2));

        if (!((prov > 0) && (prov <= num_provincias))) {
            System.out.println("Error: ruc ingresada mal");
            return false;
        }

        //verifica que el �ltimo d�gito de la c�dula sea v�lido
        int[] d = new int[10];
        int suma = 0;

        //Asignamos el string a un array
        for (int i = 0; i < d.length; i++) {
            d[i] = Integer.parseInt(ruc.charAt(i) + "");
        }

        for (int i=0; i< d.length - 1; i++) {
            d[i] = d[i] * coeficientes[i];
            suma += d[i];
            //System.out.println(�Vector d en � + i + � es � + d[i]);
        }

        System.out.println("Suma es: " + suma);

        int aux, resp;

        aux = suma % constante;
        resp = constante - aux;

        resp = (resp == 10) ? 0 : resp;

        System.out.println("Aux: " + aux);
        System.out.println("Resp " + resp);
        System.out.println("d[9] " + d[9]);

        if (resp == d[9]) {
            return true;
        }
        else{
            return false;
        }
        }catch(Exception e){
            return false;
        }
    }

    public Boolean validarRucCed(String rucCed){
        boolean ban=false;
        if(rucCed.length()==13 || rucCed.length()==10){
            if(rucCed.length()==13){
                if(rucCed.substring(10,13).equals("001")){
                    if(validacionCedula(rucCed)){
                        ban=true;
                    }else if(validaRucEP(rucCed)){
                        ban=true;
                    }else if(validacionRUC(rucCed)){
                        ban=true;
                    }
                }
            }else if(rucCed.length()==10){
                if(validacionCedula(rucCed)){
                    ban=true;
                }else if(validaRucEP(rucCed)){
                    ban=true;
                }else if(validacionRUC(rucCed)){
                    ban=true;
                }
            }
        }else{
            ban=false;
        }
        
        return ban;
    }


	/*public static Double getDecimal(int numeroDecimales,double decimal){
		decimal = decimal*(java.lang.Math.pow(10, numeroDecimales));
		decimal = java.lang.Math.round(decimal);
		decimal = decimal/java.lang.Math.pow(10, numeroDecimales);
		//SC.say("VALOR CALCULADO: "+decimal);
		return decimal;
	}*/
	
	public static double getDecimal(int numeroDecimales,double decimal){
		decimal = decimal*(java.lang.Math.pow(10, numeroDecimales));
		decimal = java.lang.Math.round(decimal);
		decimal = decimal/java.lang.Math.pow(10, numeroDecimales);
		//SC.say("VALOR CALCULADO: "+decimal);
		return decimal;
	}


}
