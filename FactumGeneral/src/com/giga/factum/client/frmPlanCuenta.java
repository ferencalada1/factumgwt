package com.giga.factum.client;


import java.util.LinkedHashMap;
import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.layout.HStack;  
import com.giga.factum.client.DTO.EmpresaDTO;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.regGrillas.EmpresaRecords;
import com.giga.factum.client.regGrillas.PlanCuentaRecords;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.form.fields.DateItem;

public class frmPlanCuenta extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	ComboBoxItem cmbEmpresa =new ComboBoxItem("cmbEmpresa", "Empresa");
	LinkedHashMap<String, String> MapEmpresa = new LinkedHashMap<String, String>();
	EmpresaDTO empresaDTO =new EmpresaDTO();
	SearchForm searchForm = new SearchForm();
	final TextItem txtPeriodo = new TextItem("txtPeriodo", "Periodo");
	ListGrid lstPlanCuenta = new ListGrid();//crea una grilla
	TabSet tabPlanCuenta = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	Boolean ban=false;
	int contador=20;
	int registros=0;
	
	frmPlanCuenta()
	{
		setSize("450", "300");
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbPlanCuenta_mant = new Tab("Ingreso");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "50%");
	 	txtPeriodo.setKeyPressFilter("[0-9]");
	 	txtPeriodo.setLength(4);
	 	txtPeriodo.setShouldSaveValue(true);
	 	txtPeriodo.setRequired(true);
	 	txtPeriodo.setDisabled(false);
	 	cmbEmpresa.setRequired(true);
	 	TextItem txtidPlanCuenta =new TextItem("txtidPlanCuenta", "Id PlanCuenta");
	 	txtidPlanCuenta.setDisabled(true);
	 	DateItem txtFechaCreacion = new DateItem("txtFechaCreacion", "Fecha de Creaci\u00F3n");
	 	txtFechaCreacion.setRequired(true);
	 	txtFechaCreacion.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
	 	txtFechaCreacion.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
	 	txtFechaCreacion.setMaskDateSeparator("-");
	 	dynamicForm.setFields(new FormItem[] {txtidPlanCuenta, txtFechaCreacion, txtPeriodo, cmbEmpresa  });
	 	layout.addMember(dynamicForm);
	 	
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "15%");
	 	
	 	IButton btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6,6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	
	 	IButton btnEliminar = new IButton("Eliminar");
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(218, 6);
	 	layout.addMember(canvas);
	 	canvas.setRect(0, 481, 898, 84);
	 	lTbPlanCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_PlanCuenta = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		layout_1.setSize("100%", "100%");
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Fecha de Creaci\u00F3n","Periodo");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        //TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        //hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstPlanCuenta.setSize("100%", "80%");
		ListGridField idPlanCuenta = new ListGridField("idPlanCuenta", "C\u00F3digo");
		ListGridField FechaCreacion= new ListGridField("FechaCreacion", "Fecha de Creaci\u00F3n");
	 	ListGridField Periodo= new ListGridField("Periodo", "Periodo");
	 	ListGridField Empresa= new ListGridField("Empresa", "Empresa");
	 	ListGridField idEmpresa= new ListGridField("idEmpresa", "idEmpresa");
	 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	 	getService().listarEmpresa(0,20, objbacklstEmpresa);
	 	lstPlanCuenta.setFields(new ListGridField[] {idPlanCuenta,FechaCreacion,Periodo,Empresa,idEmpresa});
	 	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
	 	getService().listarPlanCuenta(0,20, objbacklst);
		
		
		layout_1.addMember(lstPlanCuenta);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_PlanCuenta.setPane(layout_1);
	    
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		//btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar1"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstPlanCuenta.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
	    tabPlanCuenta.setSize("100%", "100%");
        
	    tabPlanCuenta.addTab(lTbPlanCuenta_mant);  
	    tabPlanCuenta.addTab(lTbListado_PlanCuenta);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		
	   
	    
	    addMember(tabPlanCuenta);
  
       
	
	
	}
	public PlanCuentaDTO DatosFrm(){
		String fecha = null;
		String periodo=null;
		String idPlanCuenta=dynamicForm.getItem("txtidPlanCuenta").getDisplayValue();
		fecha=dynamicForm.getItem("txtFechaCreacion").getDisplayValue();
		periodo=dynamicForm.getItem("txtPeriodo").getDisplayValue();
		String idempresa=(String) dynamicForm.getItem("cmbEmpresa").getValue();
		empresaDTO.setIdEmpresa(Integer.parseInt(idempresa));
		empresaDTO.setNombre(MapEmpresa.get(idempresa));
		return new PlanCuentaDTO(empresaDTO, fecha,periodo); 
	}
	public void Buscar(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		if(campo.equalsIgnoreCase("Periodo")){
			campo="periodo";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idPlan";
		}else if(campo.equalsIgnoreCase("Fecha de Creaci\u00F3n")){
			campo="fechaCreacion";
		}
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarPlanCuentaLike(nombre, campo, objbacklst);
		
	}
	public void limpiar(){
		dynamicForm.setValue("cmbEmpresa"," ");  
		dynamicForm.setValue("txtidPlanCuenta", "");
		dynamicForm.setValue("txtPeriodo", " ");
		dynamicForm.setValue("txtFechaCreacion", "");
	}
	/**
	 * Manejador de Botones para pantalla PlanCuenta
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					try {
						PlanCuentaDTO PlanCuentaDTO=new PlanCuentaDTO(); 
						PlanCuentaDTO=DatosFrm();
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
    					getService().grabarPlanCuenta(PlanCuentaDTO, objback);
    					limpiar();
    					lstPlanCuenta.redraw();
	        			
					}catch(Exception e){
						
						
					}
				}
					
			}
			else if(indicador.equalsIgnoreCase("eliminar")){
				SC.confirm("\u00BFEst\u00e1 seguro de eliminar el Objeto?", new BooleanCallback() {  
                    public void execute(Boolean value) {  
                        if (value != null && value) {
                        	try{
                        		/*PlanCuentaDTO plan =new PlanCuentaDTO();
                        		plan=DatosFrm();
                        		String empresa=(String) dynamicForm.getItem("cmbEmpresa").getDisplayValue();*/
                        		String idem=lstPlanCuenta.getSelectedRecord().getAttribute("idPlanCuenta");
                        		SC.say(idem);
                        		/*EmpresaDTO empre=new EmpresaDTO(Integer.parseInt(idem),empresa);
                        		plan.setTblempresa(empre);
                        		SC.say(empre.getIdEmpresa()+ " "+empre.getNombre());
                        		plan.setFechaCreacion(lstPlanCuenta.getSelectedRecord().getAttribute("FechaCreacion"));*/
                        		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
                        		getService().eliminarPlanCuenta(idem,objback);
            					limpiar();
                    			lstPlanCuenta.removeData(lstPlanCuenta.getSelectedRecord());	
            				}catch(Exception e){
                        		SC.say(e.getMessage());
            				}
                        } else {  
                            SC.say("Objeto no Eliminado");
                        }  
                    }  
                });  
			}
			else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarPlanCuenta(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarPlanCuenta(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarPlanCuenta(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
					
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarPlanCuenta(registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarPlanCuenta(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			Buscar();
		}
		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				Buscar();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
				
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("seleccionar")){
				dynamicForm.setValue("cmbEmpresa",lstPlanCuenta.getSelectedRecord().getAttribute("Empresa"));  
				dynamicForm.setValue("txtidPlanCuenta", lstPlanCuenta.getSelectedRecord().getAttribute("idPlanCuenta"));
				dynamicForm.setValue("txtFechaCreacion", lstPlanCuenta.getSelectedRecord().getAttribute("FechaCreacion"));
				dynamicForm.setValue("txtPeriodo", lstPlanCuenta.getSelectedRecord().getAttribute("Periodo"));
				tabPlanCuenta.selectTab(0);
			}
		}
	}
	
	final AsyncCallback<List<EmpresaDTO>>  objbacklstEmpresa=new AsyncCallback<List<EmpresaDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(List<EmpresaDTO> result) {
			MapEmpresa.clear();
            for(int i=0;i<result.size();i++) {
            	MapEmpresa.put(String.valueOf(new EmpresaRecords((EmpresaDTO)result.get(i)).getidEmpresa()), 
						new EmpresaRecords((EmpresaDTO)result.get(i)).getEmpresa());
			}
			cmbEmpresa.setValueMap(MapEmpresa); 
       }
	};
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			SC.say(result);
			getService().listarPlanCuenta(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<EmpresaDTO>  objbackEmpresa=new AsyncCallback<EmpresaDTO>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(EmpresaDTO result) {
			empresaDTO=result;
			SC.say(result.getNombre());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	final AsyncCallback<List<PlanCuentaDTO>>  objbacklst=new AsyncCallback<List<PlanCuentaDTO>>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<PlanCuentaDTO> result) {
			getService().numeroRegistrosPlanCuenta("Tblplancuenta",objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else{
				lblRegisros.setText(contador+" de "+registros);
			}
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new PlanCuentaRecords((PlanCuentaDTO)result.get(i)));
			}
			lstPlanCuenta.setData(listado);
			lstPlanCuenta.redraw();
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	   }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;
			
		}
	};
	
}
