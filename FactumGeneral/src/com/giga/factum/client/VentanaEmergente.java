package com.giga.factum.client;


import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
/**
 *
 * @author Johny
 */
          //      VENTANA EMERGENTE PARA EL INGRESO DE CLIENTES
        public class VentanaEmergente extends Window {

            /**
             * FUNCION PARA CRAR UNA WINMODAL
             * @param titulo Titulo de la ventana tipo String
             * @param contenido Contenido de la ventana tipo Canvas, puede ser un Form, Layout, etc.
             * @param autosize Variable booleana que indica si se debe ajustar o no el tama�o
             */
            public VentanaEmergente(String titulo, Canvas contenido, Boolean autosize, Boolean modal) {
                this.setTitle(titulo);
                this.setShowMinimizeButton(false);
                this.setIsModal(modal);
                this.setShowModalMask(modal);
                this.setKeepInParentRect(true);
                this.setAutoCenter(true);
                this.addItem(contenido);
                this.setAutoSize(autosize);
             this.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClientEvent event) {
                destruir();
            }
        });
            }

            /**
             * FUNCION PARA CRAR UNA WINMODAL
             * @param titulo Titulo de la ventana tipo String
             * @param contenido Contenido de la ventana tipo Canvas, puede ser un Form, Layout, etc.
             * @param autosize Variable booleana que indica si se debe ajustar o no el tama�o
             */
            public VentanaEmergente(String titulo, Canvas contenido, Boolean autosize, Boolean modal, CloseClickHandler manejador) {
                this.setTitle(titulo);
                this.setShowMinimizeButton(false);
                this.setIsModal(modal);
                this.setShowModalMask(true);
                this.setAutoCenter(true);
                this.addItem(contenido);
                this.setAutoSize(autosize);
             this.addCloseClickHandler(manejador);
            }

            public void destruir(){
                this.destroy();
            }
        }
