package com.giga.factum.client;

import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;

public class GridPedido extends ListGrid{
	ListGridField listgridfieldid;
	ListGridField listgridfieldmesa;
	ListGridField listgridfieldempleado;
	ListGridField listgridfieldfecha;
	
		public GridPedido (){
			listgridfieldid= new ListGridField("Num Pedido");
			listgridfieldid.setWidth("20%");
			listgridfieldmesa= new ListGridField("Mesa");
			listgridfieldmesa.setWidth("20%");
			listgridfieldempleado= new ListGridField("Usuario");
			listgridfieldempleado.setWidth("30%");
			listgridfieldfecha= new ListGridField("Fecha");
			listgridfieldfecha.setWidth("30%");
			setFields(listgridfieldid, listgridfieldmesa, listgridfieldempleado, listgridfieldfecha);			
		}
}
