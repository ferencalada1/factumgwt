package com.giga.factum.client;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.giga.factum.client.DTO.PlanCuentaDTO;
import com.giga.factum.client.DTO.TblpermisorolpestanaDTO;
import com.google.gwt.core.client.GWT;
//import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Label;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;

import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.IButton;

import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AnimationEffect;
import com.smartgwt.client.types.BkgndRepeat;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.types.FormMethod;
import com.smartgwt.client.types.Visibility;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.types.LayoutResizeBarPolicy;

import com.smartgwt.client.util.SC;

 
import com.smartgwt.client.widgets.Window;  

//import com.smartgwt.client.widgets.events.CloseClickEvent;  


public class Cabecera extends VLayout {
	HLayout layoutPrincipal= new HLayout();
	VLayout layoutPrincipalIzquierdo = new VLayout();
	VLayout layoutPrincipalDerecho = new VLayout();
	
	VLayout layoutIzquierdo = new VLayout();	
	VLayout layoutDerecho = new VLayout();
	
	VLayout layoutDerechoSuperior = new VLayout();
	VLayout layoutDerechoInferior = new VLayout();
	
	VLayout layoutIzquierdoSuperior = new VLayout();
	VLayout layoutIzquierdoInferior = new VLayout();
	
	VLayout layoutCentroSuperior = new VLayout();
	VLayout layoutCentroInferior = new VLayout();
	
	
	PasswordItem passwordItem ;
	TextItem txtUsuario;
	ComboBoxItem cmbPeriodo=new ComboBoxItem("Periodo");
	
	DynamicForm dynamicForm = new DynamicForm();
	
	IButton btnIngresar = new IButton("Ingresar");
	
	boolean ban=false;
	
	Cuerpo cuerpo = new Cuerpo();
	
	Window winModal;
	
	//HTMLPane paneLink = new HTMLPane(); 	 
	//Label Imagen = new Label();
	
	LinkedHashMap<String, String> MapPeriodo = new LinkedHashMap<String, String>();
	//HashMap<String, MenuItem> MapMenu = new HashMap<String, MenuItem>();
	String planCuentaID;
	
	WindowBarraProgreso windowbarraprogresoplancuentas;
	WindowBarraProgreso windowbarraprogresousuario;
	
	public Cabecera(Cuerpo objeto) {
		cmbPeriodo= new ComboBoxItem("cmbPeriodo", "<b>Periodo</b>");
		cmbPeriodo.addChangedHandler(new Manejador("Periodo"));
		cuerpo =objeto;
		windowbarraprogresoplancuentas = new WindowBarraProgreso("CONFIGURACION", "CARGANDO CONFIGURACION - PLAN CUENTAS");
		windowbarraprogresoplancuentas.show();
		getService().listarPlanCuenta(0, 20,callbackPlanCuenta);		
	}
	public void cargarVentanaLoggin(){
		//com.google.gwt.user.client.Window.alert("Cargar loggin ");
		windowbarraprogresousuario = new WindowBarraProgreso("CONFIGURACION", "VERIFICANDO INFORMACION DE USUARIO");
		windowbarraprogresousuario.show();
		winModal = new Window();  
		//cuerpo = objeto;
		cuerpo.menuPrincipal.setBackgroundImage(Factum.banderaNombreLogo);
		this.setBackgroundImage(Factum.banderaNombreLogo);
		
		dynamicForm.setVisibility(Visibility.VISIBLE);
		dynamicForm.setMethod(FormMethod.GET);
		txtUsuario = new TextItem("txtUsuario", "<b>Usuario</b>");
		txtUsuario.setRequired(true);
		passwordItem = new PasswordItem("txtPassword", "<b>Password</b>");
		passwordItem.setRequired(true);
		passwordItem.addKeyPressHandler(new Manejador(""));
		if(Factum.banderaMenuContabilidad==1){
			dynamicForm.setFields(new FormItem[] { txtUsuario, passwordItem, cmbPeriodo});			
			dynamicForm.setSize("100%", "40px");
			//winModal.setSize("340px", "160px");
		}else{
			dynamicForm.setFields(new FormItem[] { txtUsuario, passwordItem});
			dynamicForm.setSize("100%", "30px");
			//winModal.setSize("340px", "140px");
		}
		winModal.setWidth100();
		winModal.setHeight100();
		
		dynamicForm.setAlign(Alignment.CENTER);		
		btnIngresar.setVisibility(Visibility.VISIBLE);
		btnIngresar.addClickHandler(new Manejador("ingresar"));		
		cuerpo.linkItemIn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
		       
			@Override
			public void onClick(
				com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
//				HTMLPane htmlPane = new HTMLPane();
//			 htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul);
//				htmlPane.setContentsType(ContentsType.PAGE);
//				cuerpo.hLayout.addMember(htmlPane );
				com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/");
				
			}
		});
		cuerpo.linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
		       
			@Override
			public void onClick(
				com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				cuerpo.setDisabled(true);
	       		 passwordItem.setValue("");
	       		 txtUsuario.setValue("");
	       		 
	       		 Factum.Password=null;
	       		 Factum.idusuario=null;
	       		 Factum.UserName=null;
	       		 Factum.Menu=null;
	       		 Factum.loggeado=false;
				 Factum.Usuario =null;
				 Factum.planCuenta=null;
				 
				 Cookies.removeCookieNative("user", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("password", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("idUsuario", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("menu", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("username", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("planCuenta", "/"+Factum.banderaNombreProyecto+"/");
				getService().cerrar_sesion(Factum.banderaNombreBD,Factum.banderaPasswordBD,Factum.banderaUsuarioBD,Factum.banderaSO,Factum.banderaPasswordComprimido, Factum.destinoBackup,callbackCerrarSesion);
//				com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/login");
			}
		});
//		getService().getUserFromSession(callbackUser);
		
		//layoutPrincipalIzquierdo.setBackgroundImage(Factum.banderaServidorFactum+"/"+Factum.banderaNombreProyecto+"/images/fondoizquierdo.png");
//		layoutPrincipalIzquierdo.setBackgroundImage(Factum.banderaServidorFactum+"/"+Factum.banderaImagenes+"/fondo/fondoizquierdo.png");
		layoutPrincipalIzquierdo.setBackgroundImage(Factum.banderaImagenes+"/fondo/fondoizquierdo.png");
		layoutPrincipalIzquierdo.setBackgroundRepeat(BkgndRepeat.NO_REPEAT);
		layoutPrincipalIzquierdo.setWidth("70%");
		
		//layoutPrincipalDerecho.setBackgroundImage(Factum.banderaServidorFactum+"/"+Factum.banderaNombreProyecto+"/images/fondoderecho.png");
//		layoutPrincipalDerecho.setBackgroundImage(Factum.banderaServidorFactum+"/"+Factum.banderaImagenes+"/fondo/fondoderecho.png");
		layoutPrincipalDerecho.setBackgroundImage(Factum.banderaImagenes+"/fondo/fondoderecho.png");
		layoutPrincipalDerecho.setBackgroundRepeat(BkgndRepeat.NO_REPEAT);
		layoutPrincipalDerecho.setWidth("30%");
		
       // layoutIzquierdoSuperior.setBackgroundImage(Factum.banderaServidorRestaurant+"/Restaurant/images/logo.jpg");
        layoutIzquierdoSuperior.setBackgroundPosition("center");
        //layoutIzquierdoSuperior.setBackgroundRepeat(BackgroundRepeat.NO_REPEAT);
        layoutIzquierdoInferior.setHeight("50%");
        //layoutIzquierdoInferior.setBackgroundImage(EjemploGWT.banderaServidorRestaurant+"/Restaurant/images/logogiga.png");
        layoutIzquierdoInferior.setBackgroundPosition("left 10px bottom");
        //layoutIzquierdoInferior.setBackgroundRepeat(BackgroundRepeat.NO_REPEAT);
        layoutIzquierdo.addMember(layoutIzquierdoSuperior);
        layoutIzquierdo.addMember(layoutIzquierdoInferior);
        layoutIzquierdo.setWidth("70%");

        
        layoutDerechoInferior.setWidth("33%");
        layoutDerecho.addMember(layoutDerechoSuperior);
        layoutDerecho.addMember(layoutDerechoInferior);
        
        layoutPrincipalIzquierdo.addMember(layoutIzquierdo);
        layoutPrincipalIzquierdo.addMember(layoutDerecho);
        
        layoutPrincipal.addMember(layoutPrincipalIzquierdo);
        layoutPrincipal.addMember(layoutPrincipalDerecho);
        winModal.addItem(layoutPrincipal);
				
        winModal.setTitle("Acceso al sistema");
        winModal.setShowMinimizeButton(false);  
        winModal.setIsModal(true);  
        winModal.setShowCloseButton(false);
        winModal.setShowModalMask(true); 
        DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
        
        if(Factum.Menu==null){
        	//com.google.gwt.user.client.Window.alert("Logeado: "+Factum.loggeado);
        	if(!Factum.loggeado){        		
        		// Dialogo modal Acceso
        		HStack hStack = new HStack();
                hStack.setShowEdges(false);  
                hStack.setHeight("20px");  
                hStack.setMembersMargin(4);  
                hStack.setLayoutMargin(0);  
                hStack.addMember(btnIngresar);
                hStack.setAlign(Alignment.CENTER);
                VStack vStack = new VStack();  
                vStack.setShowEdges(false);  
                vStack.setWidth("100%");  
                vStack.setMembersMargin(0);  
                vStack.setLayoutMargin(20);  
                vStack.addMember(dynamicForm);
                vStack.addMember(hStack);
                vStack.setBackgroundColor("#4CB3CA");
                layoutIzquierdo.addMember(vStack);
                layoutIzquierdo.redraw();
        		winModal.centerInPage();         
                winModal.show();
                DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
        	}else{
        		//com.google.gwt.user.client.Window.alert("Check login");
        		getService().checkLogin(Factum.Usuario, Factum.planCuenta, callbackUSerF5);	
        	}
        }
        else{
        	//com.google.gwt.user.client.Window.alert("Menu ");
        	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
        	dynamicForm.setVisible(false);
        }
		
	}
	
	public static class Factory implements Esquema{
		private String id;
		public Canvas create(){
			Cabecera view=new Cabecera(null);
			id=view.getID();
			return view;
		}
		public String getID() {
			return id;
		}
		public String getDescription() {
			return "Cabecera";
		}
	}
	
	
	class BlueBox extends Label {  
		  
        public BlueBox(String contents) {  
            setAlign(Alignment.CENTER);  
            setBorder("1px solid #808080");  
            setBackgroundColor("#FFFFFF");  
            setContents(contents);  
        }  
  
        public BlueBox(Integer width, Integer height, String contents) {  
            this(contents);  
            if (width != null) setWidth(width);  
            if (height != null) setHeight(height);  
        }  
    }
	
	private class Manejador implements ClickHandler,KeyPressHandler, ChangedHandler{
		String indicador = "";
		Manejador(String nombreBoton) {
			this.indicador = nombreBoton;
		}
		public void onClick(ClickEvent event) {
			if(indicador.equals("ingresar")){
				if(dynamicForm.validate()){
					getService().checkLogin(dynamicForm.getField("txtUsuario").getDisplayValue(), 
							dynamicForm.getField("txtPassword").getDisplayValue(), planCuentaID,callbackUSer);
					
				}
			}else if(indicador.equals("cerrarsesion"))
			{
				//falta eliminar la sesion
				getService().cerrar_sesion(Factum.banderaNombreBD,Factum.banderaPasswordBD,Factum.banderaUsuarioBD,Factum.banderaSO,Factum.banderaPasswordComprimido, Factum.destinoBackup,callbackCerrarSesion);
			}
		}
		@Override
		public void onKeyPress(KeyPressEvent event) {
			if(event.getKeyName().equalsIgnoreCase("enter")){
				getService().checkLogin(dynamicForm.getField("txtUsuario").getDisplayValue(), 
						dynamicForm.getField("txtPassword").getDisplayValue(), planCuentaID,callbackUSer);
			}
		}
		@Override
		public void onChanged(ChangedEvent event) {
			if(indicador.equals("Periodo")){
				planCuentaID=(String)event.getValue();
				//SC.say("PLAN CUENTA: "+planCuentaID);
			}
		}	
	}

		

	final AsyncCallback<TblpermisorolpestanaDTO> asyncallbackTblprpE = new AsyncCallback<TblpermisorolpestanaDTO>() {
		 public void onFailure(Throwable caught) {
			SC.say("Error: "+ caught);
		 	         }
					@Override
					public void onSuccess(TblpermisorolpestanaDTO result) {
						System.out.println(result.toString());
						if (result!=null){
							String desc=result.getTblpestanaByIdPestana().getDescripcion();
							if((desc!="Nota de Venta")&&(desc!="Fact" || Factum.empresa.getTipo_contribuyente()!='2')){
							if (result.getTblpermisoByIdPermiso().getIdPermiso().equals(2)){
								cuerpo.getMapMenu().get(result.getTblpestanaByIdPestana().getDescripcion()).setEnabled(true);
							}else{
								cuerpo.getMapMenu().get(result.getTblpestanaByIdPestana().getDescripcion()).setEnabled(false);
							}}
							else{
								if (Factum.empresa.getTipo_contribuyente()=='2') {
		    		    			cuerpo.menuPrincipal.menuFact.setEnabled(false);
		    		    			cuerpo.getMapMenu().get("Factura").setEnabled(false);
		    		    		}else{
		    		    			cuerpo.menuPrincipal.menuNotaVentas.setEnabled(false);
		    		    			cuerpo.getMapMenu().get("Nota de Venta").setEnabled(false);
		    		    		}
							}
						}
						/*com.google.gwt.user.client.Window.alert("Antes tipo contribuyente");
    		        	com.google.gwt.user.client.Window.alert("Tipo contribuyente "+Factum.empresa.getTipo_contribuyente()+"");
    		        	com.google.gwt.user.client.Window.alert(String.valueOf(Factum.empresa.getTipo_contribuyente()=='2'));*/
    		        	
					}
			
	};
	/*
     * Manejador asincrono para la funcion de carga de Documento comercial
     * 
     */
	
     final AsyncCallback<User> callbackUSer = new AsyncCallback<User>() {
         public void onSuccess(User result) {  
        	 if(result == null)
        	 {//Si el usuario es nulo, no ha ingresado
        		 SC.say("El usuario no existe");
        		 
        		 Factum.loggeado = false;
        		 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
        		 
        	 }//Si el nombre del usuario es "ERRORCLAVE", la clave es incorrecta
        	 else {
        		 if(result.getUserName().equals("ERRORCLAVE")){
        			 SC.say("Clave Incorrecta, por favor verifique los datos");
        			 
        			 Factum.loggeado = false;
        			 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
            		 
        		 }//Si el nombre del usuario es "INACTIVO", el usuario existe pero esta deshabilitado
        		 else if(result.getUserName().equals("INACTIVO")){
        			 SC.say("El usuario esta inactivo, comuniquese con el Administrador del Sistema");
        			 
        			 Factum.loggeado = false;
        			 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
            		 
        		 }else{//El usuario esta registrado y se debe desbloquear los menus
        			 		//Oculto modal
        			 		winModal.hide();
        			 		windowbarraprogresousuario.hide();
        		        	Integer nivel = result.getNivelAcceso();
        		        	//NIVEL 1=Administrador, 2=Vendedor, 5=Cajero
        		        	cuerpo.setDisabled(false);
        		        	Factum f;
        		        	//com.google.gwt.user.client.Window.alert("Antes tipo contribuyente");
        		        	for (com.smartgwt.client.widgets.menu.MenuItem item: cuerpo.getMapMenu().values()){
        		        		getService().listarpermisosLecturap(nivel, item.getTitle(), asyncallbackTblprpE);
        		        		
        		        	}
        		        	//com.google.gwt.user.client.Window.alert("Antes tipo contribuyente");
        		        	//com.google.gwt.user.client.Window.alert("Tipo contribuyente "+Factum.empresa.getTipo_contribuyente()+"");
        		        	//com.google.gwt.user.client.Window.alert(String.valueOf(Factum.empresa.getTipo_contribuyente()=='2'));
        		        	if (Factum.empresa.getTipo_contribuyente()=='2') {
        		    			cuerpo.menuPrincipal.menuFact.setEnabled(false);
        		    			cuerpo.getMapMenu().get("Factura").setEnabled(false);
        		    		}else{
        		    			cuerpo.menuPrincipal.menuNotaVentas.setEnabled(false);
        		    			cuerpo.getMapMenu().get("Nota de Venta").setEnabled(false);
        		    		}
        		        	/*switch(nivel)
        		        	{
        		        	case 1:{//ADMINISTRADOR
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(true);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(true);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuContabilidad.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(false);
    		        			//planCuentaID=Integer.valueOf(cmbPeriodo.getAccessKey());
    		        			break;
    		        		}
        		        	case 2:{//CONTABILIDAD
        		        		//SC.say(nivel.toString());
    		        			//cuerpo.menuPrincipal.menuFactCompra.setEnabled(true);
    		        			//cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(true);
    		        			//cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuCompras.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuInventario.enable();
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuContabilidad.getMenu().getItem(2).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(0).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(1).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(1).getTitle();
    		        			//planCuentaID=Integer.valueOf(cmbPeriodo.getValue().toString());
    		        			break;
        		        	}
        		        	case 3:{//VENDEDOR
    		        			//SC.say(nivel.toString());
    		        			//cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			//cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			//cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuInventario.setDisabled(false);
    		        			//cuerpo.menuContabilidad.setDisabled(true);
    		        			//cuerpo.menuPrincipal.menuInventario.enable();
        		        		cuerpo.menuPrincipal.menuCompras.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(1).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(2).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(3).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuBackup.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuTaller.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuCore.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuContabilidad.setDisabled(true);
    		        			if (Factum.banderaConfiguracionTipoFacturacion==0) {
    		        				cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        				cuerpo.menuPrincipal.menuReportes.setDisabled(true);
    		        			}
    		        			//SC.say(nivel.toString());
    		        			break;}
    		        		case 4:{//BODEGA//TALLER
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuReporte.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			break;}    		        		    		        		
    		        		case 5:{//CAJERO
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuReportes.setDisabled(true);
    		        			//menuContabilidad.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			break;}
        		        		//case 0:{;break;}
        		        	}*/
        			 cuerpo.redraw();
        			 //SC.say("Bienvenido al Sistema "+result.getUserName());
        			 //cuerpo.setDisabled(false);
        			 dynamicForm.animateHide(AnimationEffect.WIPE);
 					 btnIngresar.animateHide(AnimationEffect.WIPE);
 					 //layout.animateMove(1000,0);
 					 ban=true;

 					 Factum.Usuario = dynamicForm.getField("txtUsuario").getDisplayValue();
 					 Factum.UserName = result.getUserName().trim();
 					 Factum.Password = dynamicForm.getField("txtPassword").getDisplayValue();
 					 Factum.planCuenta=result.getPlanCuenta();
 					 Factum.nivelAcceso=result.getNivelAcceso();
 					 //SC.say("PLAN CUENTA: "+planCuentaID);
 					 Factum.Menu = null;
 					 Factum.idusuario =String.valueOf(result.getIdUsuario());
 					 Factum.loggeado = true;
 					Date now = new Date();
 					long nowLong = now.getTime();
 					nowLong = nowLong + (1000 * 60 * 60 * 12);//8 horas
 					now.setTime(nowLong);
 					Cookies.removeCookie("menu");
 					Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
 	        		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
 	        		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
					Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("planCuenta",Factum.planCuenta, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
					Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);					
					Cookies.removeCookie("menu");
 					 
					getService().ultimoPlanCuenta(String.valueOf(result.getIdEmpresa()), callbackUltimoPlanCuentaConId);
        		 }   
        	 }     	 
         }
         public void onFailure(Throwable caught) {
         	SC.say("Error: "+ caught);
         }
     };
     
     final AsyncCallback<User> callbackUSerF5 = new AsyncCallback<User>() {
         public void onSuccess(User result) {  
        	 //SC.say("RECARGAR PAGINA: "+result.getPlanCuenta() );
        	 if(result == null){//Si el usuario es nulo, no ha ingresado
        		 SC.say("El usuario no existe");        
        		 //com.google.gwt.user.client.Window.alert("Result null");
                 cuerpo.setDisabled(true);
        		 Factum.loggeado = false;
        		 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
        		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
        		 
        	 }//Si el nombre del usuario es "ERRORCLAVE", la clave es incorrecta
        	 else {
        		 if(result.getUserName().equals("ERRORCLAVE")){
        			 SC.say("Clave Incorrecta, por favor verifique los datos");
        			 
        			 Factum.loggeado = false;
        			 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
            		 
        		 }//Si el nombre del usuario es "INACTIVO", el usuario existe pero esta deshabilitado
        		 else if(result.getUserName().equals("INACTIVO")){
        			 SC.say("El usuario esta inactivo, comuniquese con el Administrador del Sistema");
        			 
        			 Factum.loggeado = false;
        			 Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
            		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
        		 }else{//El usuario esta registrado y se debe desbloquear los menus
        			/* SC.say(String.valueOf(Factum.banderaConfiguracionTipoFacturacion==0)
	        					+" ,"+String.valueOf(Factum.banderaConfiguracionTipoFacturacion==1));*/
        			 		//Oculto modal
        			 		winModal.hide();

        		            	 

        			 		windowbarraprogresousuario.hide();
        		        	Integer nivel = result.getNivelAcceso();
        		        	//NIVEL 1=gerente, 2=vendedor, 5=cajero
        		        	//cuerpo.setDisabled(false);
        		        	/*for (com.smartgwt.client.widgets.menu.MenuItem item: cuerpo.getMapMenu().values()){
        		        		//MenuItem it=(MenuItem)item;
        		        		getService().listarpermisosLecturap(nivel, item.getTitle(), asyncallbackTblprpE);
        		        	}*/
        		        	/*switch(nivel)
        		        	{
        		        	case 1://ADMINISTRADOR
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(true);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(true);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuContabilidad.setDisabled(false);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(false);
    		        			//planCuentaID=Integer.valueOf(cmbPeriodo.getAccessKey());
    		        			break;
        		        	case 2://CONTABILIDAD
        		        		//SC.say(nivel.toString());
    		        			//cuerpo.menuPrincipal.menuFactCompra.setEnabled(true);
    		        			//cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(true);
    		        			//cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuCompras.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuInventario.enable();
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuContabilidad.getMenu().getItem(2).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(0).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(1).setEnabled(false);
    		        			//planCuentaID=Integer.valueOf(cmbPeriodo.getValue().toString());
    		        			break;
        		        	case 3://VENDEDOR
    		        			//SC.say(nivel.toString());
    		        			//cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			//cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			//cuerpo.menuPrincipal.menuPrincipalPagos.setDisabled(false);
    		        			//cuerpo.menuPrincipal.menuInventario.setDisabled(false);
    		        			//cuerpo.menuContabilidad.setDisabled(true);
    		        			//cuerpo.menuPrincipal.menuInventario.enable();
        		        		cuerpo.menuPrincipal.menuCompras.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(1).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(2).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPrincipalPagos.getMenu().getItem(3).setEnabled(false);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuBackup.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuTaller.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuCore.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuContabilidad.setDisabled(true);
    		        			if (Factum.banderaConfiguracionTipoFacturacion==0) {
    		        				cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        				cuerpo.menuPrincipal.menuReportes.setDisabled(true);
    		        			}
    		        			//SC.say(nivel.toString());
    		        			break;
    		        		case 4://BODEGA//TALLER
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuReporte.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			break;   		        		    		        		
    		        		case 5://CAJERO
    		        			//SC.say(nivel.toString());
    		        			cuerpo.menuPrincipal.menuFactCompra.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuNotaCreditoProveedor.setEnabled(false);
    		        			cuerpo.menuPrincipal.menuInventario.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuReportes.setDisabled(true);
    		        			//menuContabilidad.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuPersonas.setDisabled(true);
    		        			cuerpo.menuPrincipal.menuConfiguraciones.setDisabled(true);
    		        			break;
        		        		//case 0:{;break;}
        		        	}*/
        		        	for (com.smartgwt.client.widgets.menu.MenuItem item: cuerpo.getMapMenu().values()){
        		        		getService().listarpermisosLecturap(nivel, item.getTitle(), asyncallbackTblprpE);
        		        		
        		        	}
        		        	//com.google.gwt.user.client.Window.alert("Antes tipo contribuyente F5");
        		        	//com.google.gwt.user.client.Window.alert("Tipo contribuyente "+Factum.empresa.getTipo_contribuyente()+"");
        		        	//com.google.gwt.user.client.Window.alert(String.valueOf(Factum.empresa.getTipo_contribuyente()=='2'));
        		    		if (Factum.empresa.getTipo_contribuyente()=='2') {
        		    			cuerpo.menuPrincipal.menuFact.setEnabled(false);
        		    			cuerpo.getMapMenu().get("Factura").setEnabled(false);
        		    		}else{
        		    			cuerpo.menuPrincipal.menuNotaVentas.setEnabled(false);
        		    			cuerpo.getMapMenu().get("Nota de Venta").setEnabled(false);
        		    		}
        		        	//com.google.gwt.user.client.Window.alert("Result no null F5");
   		            	 cuerpo.setDisabled(false);
        			 cuerpo.redraw();
        			 //SC.say("Bienvenido al Sistema "+result.getUserName());
        			 //cuerpo.setDisabled(false);
        			 dynamicForm.animateHide(AnimationEffect.WIPE);
 					 btnIngresar.animateHide(AnimationEffect.WIPE);
 					 //layout.animateMove(1000,0);
 					 ban=true;
 					 	
 					Factum.loggeado = true;
 					Date now = new Date();
 					long nowLong = now.getTime();
 					nowLong = nowLong + (1000 * 60 * 60 * 12);// milisegunds, segundos minutos horas
 					now.setTime(nowLong);
 					Factum.user=result;
 					Factum.idusuario = String.valueOf(result.getIdUsuario());
 					Factum.UserName = result.getUserName();
 					//com.google.gwt.user.client.Window.alert("Result plancuenta "+result.getPlanCuenta());
 					Factum.planCuenta=result.getPlanCuenta();
 					//com.google.gwt.user.client.Window.alert("Plancuenta "+Factum.planCuenta);
 					Factum.nivelAcceso=result.getNivelAcceso();
 					//SC.say("PLAN CUENTA: "+planCuentaID);
 					
 					Cookies.removeCookie("user","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("password","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyecto+"/");
 	        		 Cookies.removeCookie("planCuenta","/"+Factum.banderaNombreProyecto+"/");
 	        		Cookies.removeCookie("idUsuario","/"+Factum.banderaNombreProyectoTaller+"/");
 	        		Cookies.removeCookie("username","/"+Factum.banderaNombreProyectoTaller+"/");
 					
 					//Cookies.setCookie("user",Factum.Usuario, now,"localhost","/"+Factum.banderaNombreProyecto+"/",false);
 					Cookies.setCookie("user",Factum.Usuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("password",Factum.Password, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("planCuenta",Factum.planCuenta, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyecto+"/",false);
					Cookies.setCookie("idUsuario",Factum.idusuario, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
					Cookies.setCookie("username",Factum.UserName, now,Factum.banderaIpServidor,"/"+Factum.banderaNombreProyectoTaller+"/",false);
					
					getService().ultimoPlanCuenta(String.valueOf(result.getIdEmpresa()), callbackUltimoPlanCuentaConId);
        		 }   
        	 }    
        	 DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
         }
         public void onFailure(Throwable caught) {
         	SC.say("Error: "+ caught);
         }
     };
     
     final AsyncCallback<String> callbackCerrarSesion = new AsyncCallback<String>() {
         public void onSuccess(String result) {  
        	 if(result.equals("La sesion ha terminado"))
        	 {//Si el usuario es nulo, no ha ingresado
        		 SC.say(result);
        		 cuerpo.setDisabled(true);
        		 passwordItem.setValue("");
        		 txtUsuario.setValue("");
        		 
        		 Factum.Password=null;
        		 Factum.idusuario=null;
        		 Factum.UserName=null;
        		 Factum.Menu=null;
        		 Factum.loggeado=false;
				 Factum.Usuario =null;
				 Factum.planCuenta=null;
				 
				 Cookies.removeCookieNative("user", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("password", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("idUsuario", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("menu", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("username", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("planCuenta", "/"+Factum.banderaNombreProyecto+"/");

//        		 com.google.gwt.user.client.Window.Location.reload();
				 com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/login");
        	 }//Si el nombre del usuario es "ERRORCLAVE", la clave es incorrecta
        	 else {
        		 SC.say(result);
        		 cuerpo.setDisabled(true);
        		 passwordItem.setValue("");
        		 txtUsuario.setValue("");
        		 
        		 Factum.Password=null;
        		 Factum.idusuario=null;
        		 Factum.UserName=null;
        		 Factum.Menu=null;
        		 Factum.loggeado=false;
				 Factum.Usuario =null;
				 Factum.planCuenta=null;
				 
				 Cookies.removeCookieNative("user", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("password", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("idUsuario", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("menu", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("username", "/"+Factum.banderaNombreProyecto+"/");
				 Cookies.removeCookieNative("planCuenta", "/"+Factum.banderaNombreProyecto+"/");
				 
				 com.google.gwt.user.client.Window.Location.replace("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoZuul+"/login");
//        		 com.google.gwt.user.client.Window.Location.reload();
        	 }    
			 ban=true;
         }
         public void onFailure(Throwable caught) {
         	SC.say("Error: "+ caught);
         }
     };

     final AsyncCallback<User> callbackUser = new AsyncCallback<User>() {

         public void onSuccess(User result) {
        	 //com.google.gwt.user.client.Window.alert("After call user");
        	 windowbarraprogresousuario.hide();
         	if (result == null) {
         		//com.google.gwt.user.client.Window.alert("Result null");
                 cuerpo.setDisabled(true);
             }else{
            	 //com.google.gwt.user.client.Window.alert("Result no null");
            	 cuerpo.setDisabled(false);
             }
         }
         public void onFailure(Throwable caught) {
         	cuerpo.setDisabled(true);
         }
     };
     
     final AsyncCallback<List<PlanCuentaDTO>> callbackPlanCuenta = new AsyncCallback<List<PlanCuentaDTO>>() {
		@Override
		public void onFailure(Throwable caught) {
			SC.say("Error: "+ caught);			
		}
		@Override
		public void onSuccess(List<PlanCuentaDTO> result) {
			MapPeriodo.clear();
			//MapPeriodo.put("","");
			for(int i=0;i<result.size();i++) {
            	MapPeriodo.put(String.valueOf(result.get(i).getIdPlan()), 
            	result.get(i).getPeriodo());
			}
            cmbPeriodo.setValueMap(MapPeriodo);
    	    getService().ultimoPlanCuenta(callbackUltimoPlanCuenta);
		}
	};
	
	final AsyncCallback<PlanCuentaDTO> callbackUltimoPlanCuenta = new AsyncCallback<PlanCuentaDTO>() {
		@Override
		public void onFailure(Throwable caught) {
			//windowbarraprogresoplancuentas.hide();
			
			SC.say("Error: "+ caught);
			getService().listarPlanCuenta(0, 20,callbackPlanCuenta);
			//reload();
		}
		@Override
		public void onSuccess(PlanCuentaDTO result) {
			windowbarraprogresoplancuentas.hide();
			
			//com.google.gwt.user.client.Window.alert("En sucess plan cuenta");
			//com.google.gwt.user.client.Window.alert("Plan cuenta "+result.getIdPlan().toString());
			planCuentaID=result.getIdPlan().toString();
			Factum.planCuenta=result.getIdPlan().toString();
			//com.google.gwt.user.client.Window.alert(result.getTblempresa().getNombre());
			Factum.banderaNombreEmpresa=result.getTblempresa().getNombre();
			//com.google.gwt.user.client.Window.alert(result.getTblempresa().getEstablecimientos().get(0).getDireccion());
			//com.google.gwt.user.client.Window.alert(result.getTblempresa().getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision());
			Factum.banderaDireccionEmpresa=result.getTblempresa().getEstablecimientos().get(0).getDireccion();//MULTIEMPRESA
			Factum.banderaRUCEmpresa=result.getTblempresa().getRUC();
			Factum.empresa=result.getTblempresa();
			//com.google.gwt.user.client.Window.alert(result.getTblempresa().getTipo_contribuyente()+"");
			cmbPeriodo.setValue(result.getPeriodo());		
			cargarVentanaLoggin();
			
		}
	};
	
	final AsyncCallback<PlanCuentaDTO> callbackUltimoPlanCuentaConId = new AsyncCallback<PlanCuentaDTO>() {
		@Override
		public void onFailure(Throwable caught) {
			//windowbarraprogresoplancuentas.hide();
			
			SC.say("Error: "+ caught);
			getService().listarPlanCuenta(0, 20,callbackPlanCuenta);
			//reload();
		}
		@Override
		public void onSuccess(PlanCuentaDTO result) {
			windowbarraprogresoplancuentas.hide();
			
			//com.google.gwt.user.client.Window.alert("En sucess plan cuenta");
			//com.google.gwt.user.client.Window.alert("Plan cuenta "+result.getIdPlan().toString());
			planCuentaID=result.getIdPlan().toString();
			Factum.planCuenta=result.getIdPlan().toString();
			//com.google.gwt.user.client.Window.alert(result.getTblempresa().getNombre());
			Factum.banderaNombreEmpresa=result.getTblempresa().getNombre();
			////com.google.gwt.user.client.Window.alert(result.getTblempresa().getEstablecimientos().get(0).getDireccion());
			////com.google.gwt.user.client.Window.alert(result.getTblempresa().getEstablecimientos().get(0).getPuntoemision().get(0).getPuntoemision());
			Factum.banderaDireccionEmpresa=result.getTblempresa().getEstablecimientos().get(0).getDireccion();//MULTIEMPRESA
			Factum.banderaRUCEmpresa=result.getTblempresa().getRUC();
			Factum.empresa=result.getTblempresa();
			cmbPeriodo.setValue(result.getPeriodo());
			
		}
	};
     
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
		}

}
