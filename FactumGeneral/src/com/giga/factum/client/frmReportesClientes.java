package com.giga.factum.client;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.giga.factum.client.DTO.DtoComDetalleDTO;
import com.giga.factum.client.DTO.DtoComDetalleMultiImpuestosDTO;
import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.ReporteClientesDTO;
import com.giga.factum.client.DTO.ReportesDTO;
import com.giga.factum.client.regGrillas.DtocomercialRecords;
import com.giga.factum.server.base.TbldtocomercialdetalleTblmultiImpuesto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.types.Alignment;

public class frmReportesClientes extends VLayout{
	
	ListGrid lstReporteCaja = new ListGrid();
	String fechaI="";
	String fechaF="";
	HStack hStackSuperior = new HStack();
	DynamicForm dynamicForm = new DynamicForm();
	DynamicForm dynamicForm2 = new DynamicForm();//para el nombre apellido ruc
	DateTimeItem txtFechaInicial =new DateTimeItem("txtFechaInicial","Fecha Inicial");
	DateTimeItem txtFechaFinal =new DateTimeItem("txtFechaFinal","Fecha Final");
	VLayout layout_1 = new VLayout();
	String Tipo="";
	IButton btnGenerarReporte = new IButton("Generar Reporte");
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	PickerIcon buscarPicker = new PickerIcon(PickerIcon.SEARCH);
	TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
	HStack hStack = new HStack();
	HLayout hLayoutPr = new HLayout();
	VentanaEmergente listadoProd ;
	double iva=0;
	double iva0=0;
	double Subtotal=0;
	double Total=0;
	
	TextItem txtRuc =new TextItem("txtRuc", "<b>Ruc</b>");
	TextItem txtNomComercial =new TextItem("txtNomComercial", "<b>Nombre Comercial</b>");
	TextItem txtRazonSocial =new TextItem("txtRazonSocial", "<b>Razon Social</b>");
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	ComboBoxItem cmbDocumento = new ComboBoxItem("cmbDocumento", "Tipo de Documento");
	ListGrid ListGridTotales = new ListGrid();
	//DateRangeItem dateRangeItem = new DateRangeItem("txtRango", "Rango de Fechas");
	public frmReportesClientes() {
		try{
			lstReporteCaja = new ListGrid() {  
	            @Override  
	            protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {  
	                if (getFieldName(colNum).equals("Estado")) {  
	                	ListGridRecord listrecord =  record;  
	                    if (listrecord.getAttributeAsString("Estado").equals("CONFIRMADO")) {  
	                        return "font-weight:bold; color:#298a08;"; 
	                    } else if (listrecord.getAttributeAsString("Estado").equals("NO CONFIRMADO")) {  
	                        return "font-weight:bold; color:#287fd6;";  
	                    } else if(listrecord.getAttributeAsString("Estado").equals("ANULADA")){  
	                    	return "font-weight:bold; color:#d64949;"; 
	                    }else{
	                    	return super.getCellCSSText(record, rowNum, colNum);  
	                    }
	                }
	                else
	                {	
	                	if(getFieldName(colNum).equals("Autorizacion"))
	                	{
	                		ListGridRecord listrecord =  record;
	                		if (listrecord.getAttributeAsString("Autorizacion").equals("TOTALES:")) 
	                		{  
		                        return "font-weight:bold; color:#FF0000;"; 
		                    } 
	                		else
	                		{
		                    	return super.getCellCSSText(record, rowNum, colNum);  
		                    }
	                	}	
	                	else {  
	                		ListGridRecord listrecord =  record;
	                		if (listrecord.getAttributeAsString("Autorizacion").equals("TOTALES:")) 
	                		{  
		                        return "font-weight:bold; color:#000000;"; 
		                    } 
	                		else
	                		{
		                    	return super.getCellCSSText(record, rowNum, colNum);  
		                    }  
	                	}
	                }	
	            }  
	        };  
			dynamicForm.setSize("40%", "15%");
			dynamicForm2.setSize("60%", "15%");

			dynamicForm2.setFields(new FormItem[] { txtRuc, txtRazonSocial, txtNomComercial});
			
			txtFechaInicial.setSelectorFormat(DateItemSelectorFormat.YEAR_MONTH_DAY);
			txtFechaInicial.setRequired(true);
			txtFechaFinal.setRequired(true);
			txtFechaInicial.setMaskDateSeparator("-");
			txtFechaFinal.setMaskDateSeparator("-");
			txtFechaFinal.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaFinal.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDateFormatter(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setDisplayFormat(DateDisplayFormat.TOJAPANSHORTDATE);
			txtFechaInicial.setUseTextField(true);
			txtFechaInicial.setValue(new Date());
			txtFechaFinal.setUseTextField(true);
			txtFechaFinal.setValue(new Date());
			
			//+++++++++++++ nom, ape, ruc ++++++++++++++
			txtRuc.setValue("");
			txtNomComercial.setValue("");
			txtRazonSocial.setValue("");
			
			cmbDocumento.setRequired(true);
			cmbDocumento.setValueMap("Facturas de Venta","Facturas de Compra","Notas de Entrega","Notas de Compra","Notas de Credito","Anticipo Clientes","Ajustes de Inventario","Gastos");
			
			dynamicForm.setFields(new FormItem[] { txtFechaInicial, txtFechaFinal, cmbDocumento});
			//addMember(dynamicForm);
			
			hStackSuperior.setSize("100%", "20%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(dynamicForm2);
			//hStackSuperior.addMember(hStackEspacio);
			addMember(hStackSuperior);
			lstReporteCaja.setSize("100%", "50%");
			ListGridField id =new ListGridField("id", "id");
			ListGridField NumRealTransaccion =new ListGridField("NumRealTransaccion", "Num Real Transaccion");
			ListGridField TipoTransaccion= new ListGridField("TipoTransaccion", "Tipo Transaccion");
			ListGridField Fecha =new ListGridField("Fecha", "Fecha");
			Fecha.setAlign(Alignment.CENTER);
			ListGridField FechadeExpiracion=new ListGridField("FechadeExpiracion", "Fechade Expiracion");
			ListGridField Vendedor=new ListGridField("Vendedor", "Vendedor");
			ListGridField Cliente =new ListGridField("Cliente", "Cliente");
			ListGridField Autorizacion =new ListGridField("Autorizacion", "Autorizacion");
			ListGridField NumPagos =new ListGridField("NumPagos", "Numero de Pagos");
			ListGridField Estado =new ListGridField("Estado", "Estado");
			ListGridField Iva0 =new ListGridField("Iva0", "Subt. IVA 0%");
			ListGridField Iva =new ListGridField("Iva", "IVA");
			ListGridField Total =new ListGridField("Total", "Total");
			ListGridField SubTotal =new ListGridField("SubTotal", "SubTotal Neto");
			ListGridField Descuento =new ListGridField("Descuento", "Descuento");
			ListGridField Retencion =new ListGridField("retencion", "Total Retencion");
			ListGridField RetencionIVA =new ListGridField("retencionIVA", "Ret. IVA",50);
			ListGridField RetencionRENTA =new ListGridField("retencionRENTA", "Ret. RENTA",65);
			ListGridField Ruc =new ListGridField("RUC", "RUC");
			Cliente.setWidth("20%");
			Fecha.setWidth("7%");
			NumRealTransaccion.setWidth("6%");
			NumRealTransaccion.setAlign(Alignment.CENTER);
			Autorizacion.setWidth("7%");
			SubTotal.setWidth("6%");
			SubTotal.setAlign(Alignment.RIGHT);
			Iva.setWidth("6%");
			Iva.setAlign(Alignment.RIGHT);
			Iva0.setWidth("6%");
			Iva0.setAlign(Alignment.RIGHT);
			Total.setWidth("6%");
			Total.setAlign(Alignment.RIGHT);
			Descuento.setWidth("6%");
			Descuento.setAlign(Alignment.RIGHT);
			Retencion.setWidth("6%");
			Retencion.setAlign(Alignment.RIGHT);
			Estado.setWidth("7%");
			Estado.setAlign(Alignment.CENTER);
			Ruc.setWidth("7%");
			RetencionIVA.setWidth("5%");
			RetencionIVA.setAlign(Alignment.RIGHT);
			RetencionRENTA.setWidth("5%");
			RetencionRENTA.setAlign(Alignment.RIGHT);
			//lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,Autorizacion,SubTotal,Iva0,Iva,Total,Descuento,Retencion,Estado});
			lstReporteCaja.setFields(new ListGridField[] {Cliente,Ruc,Fecha,NumRealTransaccion,Autorizacion,SubTotal,Descuento,Iva0,Iva,Total,RetencionIVA,RetencionRENTA,Retencion,Estado});
			lstReporteCaja.setShowRowNumbers(true);
			
		//	lstReporteCaja.setCanReorderRecords(false);
			//lstReporteCaja.sort("Cliente", SortDirection.ASCENDING);
			//lstReporteCaja.groupBy("Cliente");
			//lstReporteCaja.setGroupStartOpen(GroupStartOpen.ALL);
			addMember(lstReporteCaja);
			
			HStack hStack = new HStack();
			hStack.setSize("100%", "5%");
			
			
			hStack.addMember(btnGenerarReporte);
			btnGenerarReporte.addClickHandler(new ManejadorBotones("generar"));
			
			IButton btnImprimir = new IButton("Imprimir");
			IButton btnExportar = new IButton("Exportar");
			IButton btnLimpiar = new IButton("Limpiar");
			hStack.addMember(btnLimpiar);
			hStack.addMember(btnImprimir);
			hStack.addMember(btnExportar);
			btnImprimir.addClickHandler(new ManejadorBotones("imprimir"));
			btnExportar.addClickHandler(new ManejadorBotones("exportar"));
			btnLimpiar.addClickHandler(new ManejadorBotones("limpiar"));
			
			ListGridTotales.setSize("100%", "30%");
			ListGridField lstSubtNeto = new ListGridField("lstSubtNeto", "Subtotal Neto");
			lstSubtNeto.setAlign(Alignment.RIGHT);
			ListGridField lstSubtIVA0 = new ListGridField("lstSubtIVA0", "Subtotal IVA 0%");
			lstSubtIVA0.setAlign(Alignment.RIGHT);
			ListGridField lstIVA = new ListGridField("lstIVA", "IVA");
			lstIVA.setAlign(Alignment.RIGHT);
			ListGridField lstTOTAL = new ListGridField("lstTOTAL", "TOTAL");
			lstTOTAL.setAlign(Alignment.RIGHT);
			ListGridField lstRetencion = new ListGridField("retencion", "RETENCION");
			lstRetencion.setAlign(Alignment.RIGHT);
			ListGridField lstDescuento = new ListGridField("Descuento", "DESCUENTO");
			lstDescuento.setAlign(Alignment.RIGHT);
			ListGridField lstRETIVA = new ListGridField("totretencionIVA", "RET. IVA");
			lstRETIVA.setAlign(Alignment.RIGHT);
			ListGridField lstRETRENTA = new ListGridField("totretencionRENTA", "RET. RENTA");
			lstRETRENTA.setAlign(Alignment.RIGHT);
			ListGridTotales.setFields(new ListGridField[] { lstSubtNeto,lstDescuento,lstSubtIVA0,lstIVA,lstTOTAL,lstRETIVA,lstRETRENTA,lstRetencion});
			addMember(ListGridTotales);
			
			addMember(hStack);
			
			cmbDocumento.setDefaultToFirstOption(true);
			cmbDocumento.redraw();
		}catch(Exception e ){
			SC.say("Error: "+e);
		}
		//cargaInicial();
	}
	
	NumberFormat formatoDecimalN = NumberFormat.getFormat("####0."+new String(new char[Factum.banderaNumeroDecimales]).replace("\0", "0"));
	private class ManejadorBotones implements DoubleClickHandler,KeyPressHandler,RecordDoubleClickHandler,FormItemClickHandler, com.smartgwt.client.widgets.events.ClickHandler{
		String indicador="";
		
		
		public void datosfrm(){
			try{
				
				fechaI=txtFechaInicial.getDisplayValue();
				fechaF=txtFechaFinal.getDisplayValue();
				if(cmbDocumento.getDisplayValue().equals("Facturas de Venta")){
					Tipo="0";
				}else if(cmbDocumento.getDisplayValue().equals("Facturas de Compra")){
					Tipo="1";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Entrega")){
					Tipo="2";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Credito")){
					Tipo="3";
				}else if(cmbDocumento.getDisplayValue().equals("Anticipo Clientes")){
					Tipo="4";
				}else if(cmbDocumento.getDisplayValue().equals("Ajustes de Inventario")){
					Tipo="5";
				}else if(cmbDocumento.getDisplayValue().equals("Gastos")){
					Tipo="7";
				}else if(cmbDocumento.getDisplayValue().equals("Notas de Compra")){
					Tipo="10";
				}
			else if(cmbDocumento.getDisplayValue().equals("Facturas Grandes")){
				Tipo="13";
			}
				//SC.say(idCliente+" vendedor="+idVendedor+" Tipo "+Tipo);
			}catch(Exception e){
				SC.say(e.getMessage());
			}
			
		}
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		 
		@Override
		public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
			if(indicador.equalsIgnoreCase("generar")){
				datosfrm();
				//	DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					
				
					if (txtNomComercial.getDisplayValue().toString().compareTo("")==0&&txtRazonSocial.getDisplayValue().toString().compareTo("")==0&&txtRuc.getDisplayValue().toString().compareTo("")==0)
					{
						DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
						getService().listarFacturas(fechaI, fechaF,Tipo, listaCallback);
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					}
					else
					{
						
						//SC.say("cedula: "+txtRuc.getValue().toString()+" nombres: "+txtNomComercial.getValue().toString()+" apellidos: "+txtRazonSocial.getValue().toString());
				//		SC.say("entre en el else de generar");
			
						getService().listarReportesCliente(fechaI, fechaF, Tipo, txtRuc.getDisplayValue().toString(), txtNomComercial.getDisplayValue().toString(), txtRazonSocial.getDisplayValue().toString(), listaCallback);
						//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					}	
					
			}else if(indicador.equalsIgnoreCase("limpiar")){
				txtRuc.setValue("");
				txtNomComercial.setValue("");
				txtRazonSocial.setValue("");

			}else if(indicador.equalsIgnoreCase("imprimir")){

				VLayout espacio = new VLayout(); 
				espacio.setSize("100%","5%");
				Object[] a=new Object[]{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +				  
			   		"&nbsp;&nbsp;&nbsp;Reporte Clientes Totalizados",espacio,hStackSuperior,lstReporteCaja,ListGridTotales};					   
							
		        Canvas.showPrintPreview(a);
			}else if(indicador.equalsIgnoreCase("exportar")){
				CreateExelDTO exel=new CreateExelDTO(lstReporteCaja);
				
				}
		}
		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			
			
		} 

		@Override
		public void onKeyPress(KeyPressEvent event) {

		}		
		
		@Override
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			// TODO Auto-generated method stub
		} 

		@Override
		public void onDoubleClick(DoubleClickEvent event) {
			
		}

		
	}
	
	
	final AsyncCallback<List<DtocomercialDTO>>  listaCallback=new AsyncCallback<List<DtocomercialDTO>>(){

		public void onFailure(Throwable caught) {
			SC.say(caught.toString()+ "error");
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(List<DtocomercialDTO> result) {
		if (result.size()>0)
		{	
			//DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			ListGridRecord[] listado = new ListGridRecord[result.size()];		
			for(int i=0;i<result.size();i++)
			{
				DtocomercialRecords doc =new DtocomercialRecords(result.get(i));
				Iterator iterTP= result.get(i).getTbldtocomercialdetalles().iterator();
				DtoComDetalleDTO det = new DtoComDetalleDTO();
				double subtNeto=0;
				
				double subNetoImpuesto=0.0;
				double subImp=0.0;
				
				while (iterTP.hasNext()) {
					det = (DtoComDetalleDTO) iterTP.next();
					
					String impuestos="";
					double impuestoPorc=1.0;
					double impuestoValor=0.0;
					int i1=0;
					//com.google.gwt.user.client.Window.alert("impuestos de detalle "+det.getTblimpuestos().size());
					for (DtoComDetalleMultiImpuestosDTO detalleImp: det.getTblimpuestos()){
						i1=i1+1;
						impuestos+=detalleImp.getPorcentaje().toString();
						impuestos= (i1<det.getTblimpuestos().size())?impuestos+",":impuestos; 
						impuestoPorc=impuestoPorc*(1+(detalleImp.getPorcentaje()/100));
						impuestoValor=impuestoValor+((det.getCantidad()*det.getPrecioUnitario()*(1-(det.getDepartamento()/100))+impuestoValor)*(detalleImp.getPorcentaje()/100));
					}
					//com.google.gwt.user.client.Window.alert("impuestos de detalle despues "+impuestos+" porc "+impuestoPorc+" valor "+impuestoValor);
					
//					if(det.getImpuesto()>0){
					if((impuestoPorc-1)>0){
						//subtNeto=subtNeto+(det.getTotal()-(det.getTotal()*det.getDepartamento()/100));
						subtNeto=subtNeto+(det.getTotal());
						subNetoImpuesto=subNetoImpuesto+(det.getTotal()*impuestoPorc);
						subImp+=impuestoValor;
					}
				}
				listado[i]=(new DtocomercialRecords(result.get(i)));
				listado[i].setAttribute("SubTotal",Math.rint(subtNeto*100)/100);
				
//				double iva=0;
//				iva=listado[i].getAttributeAsDouble("SubTotal");
//				iva=iva*(Factum.banderaIVA/100);
				listado[i].setAttribute("Iva", Math.rint(subImp*100)/100);
				double tot=0;
				tot=Double.valueOf(listado[i].getAttributeAsString("Total"));
				listado[i].setAttribute("Total",Math.rint((subtNeto+subImp+Double.valueOf(doc.getAttributeAsString("Iva0")))*100)/100);
			}
			//++++++++++++++++++++++++   PARA LA PARTE DE TOTALIZAR POR CLIENTE ++++++++++++
			
			lstReporteCaja.setData(listado);
		//	libro();
			
			LinkedList<ReporteClientesDTO> linkLstRep = new LinkedList<ReporteClientesDTO>();
			int numRegReporte=result.size();
			int numRegRepetidos=0;
			int numFacturasEmitidas=0;
			String nomCliente=listado[0].getAttributeAsString("Cliente");
			String nomClienteAux="";
			String cedCliente=listado[0].getAttributeAsString("RUC");
			String cedClienteAux="";
			double totalCliente=0;
			double subTotalCliente=0;			
			double ivaCliente=0;
			double ivaClienteCero=0;
			double totalClienteGeneral=0;
			double subTotalClienteGeneral=0;			
			double ivaClienteGeneral=0;
			double ivaClienteCeroGeneral=0;
			double descuentoCliente=0;
			double retencionCliente=0;
			double descuentoClienteGeneral=0;
			double retencionClienteGeneral=0;
			double retencionClienteIVA=0;
			double retencionClienteGeneralIVA=0;
			double retencionClienteRENTA=0;
			double retencionClienteGeneralRENTA=0;
			//+++++++++  Si esta confirmado pongo valores caso contrario dejo con cero
			if(listado[0].getAttributeAsString("Estado").equals("CONFIRMADO"))
			{
				totalCliente=Double.valueOf(listado[0].getAttributeAsString("Total"));
				subTotalCliente=Double.valueOf(listado[0].getAttributeAsString("SubTotal"));			
				ivaCliente=Double.valueOf(listado[0].getAttributeAsString("Iva"));
				ivaClienteCero=Double.valueOf(listado[0].getAttributeAsString("Iva0"));
				totalClienteGeneral=Double.valueOf(listado[0].getAttributeAsString("Total"));
				subTotalClienteGeneral=Double.valueOf(listado[0].getAttributeAsString("SubTotal"));			
				ivaClienteGeneral=Double.valueOf(listado[0].getAttributeAsString("Iva"));
				ivaClienteCeroGeneral=Double.valueOf(listado[0].getAttributeAsString("Iva0"));
				descuentoCliente=Double.valueOf(listado[0].getAttributeAsString("Descuento"));
				retencionCliente=Double.valueOf(listado[0].getAttributeAsString("retencion"));
				descuentoClienteGeneral=Double.valueOf(listado[0].getAttributeAsString("Descuento"));
				retencionClienteGeneral=Double.valueOf(listado[0].getAttributeAsString("retencion"));
				retencionClienteIVA=Double.valueOf(listado[0].getAttributeAsString("retencionIVA"));
				retencionClienteGeneralIVA=Double.valueOf(listado[0].getAttributeAsString("retencionIVA"));
				retencionClienteRENTA=Double.valueOf(listado[0].getAttributeAsString("retencionRENTA"));
				retencionClienteGeneralRENTA=Double.valueOf(listado[0].getAttributeAsString("retencionRENTA"));
				
				numFacturasEmitidas++;
			}
			for(int k=0;k<numRegReporte;k++)
			{
				ReporteClientesDTO repCli = new ReporteClientesDTO();
				repCli.setAutorizacion(lstReporteCaja.getRecord(k).getAttributeAsString("Autorizacion"));
				repCli.setCliente(lstReporteCaja.getRecord(k).getAttributeAsString("Cliente"));
				repCli.setFecha(lstReporteCaja.getRecord(k).getAttributeAsString("Fecha"));
				repCli.setEstado(lstReporteCaja.getRecord(k).getAttributeAsString("Estado"));
				repCli.setIva(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("Iva")));
				repCli.setIva0(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("Iva0")));
				repCli.setNumRealTransaccion(lstReporteCaja.getRecord(k).getAttributeAsInt("NumRealTransaccion"));
				repCli.setSubtotal(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("SubTotal")));
				repCli.setTotal(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("Total")));
				repCli.setRetencion(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("retencion")));
				repCli.setDescuento(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("Descuento")));
				repCli.setCedulaRuc(lstReporteCaja.getRecord(k).getAttributeAsString("RUC"));
				repCli.setRetencionIVA(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("retencionIVA")));
				repCli.setRetencionRENTA(Double.valueOf(lstReporteCaja.getRecord(k).getAttributeAsString("retencionRENTA")));
				if(k!=0)
				{
					nomClienteAux=repCli.getCliente();
					cedClienteAux=repCli.getCedulaRuc();
//					if(nomClienteAux.equals(nomCliente))
					if(cedClienteAux.equals(cedCliente))
					{
						
						if(repCli.getEstado().equals("CONFIRMADO"))
						{	
							totalCliente=totalCliente+repCli.getTotal();
							subTotalCliente=subTotalCliente+repCli.getSubTotal();
							ivaCliente=ivaCliente+repCli.getIva();
							ivaClienteCero=ivaClienteCero+repCli.getIva0();
							descuentoCliente=descuentoCliente+repCli.getDescuento();
							retencionCliente=retencionCliente+repCli.getRetencion();
							descuentoClienteGeneral=descuentoClienteGeneral+repCli.getDescuento();
							retencionClienteGeneral=retencionClienteGeneral+repCli.getRetencion();
							totalClienteGeneral=totalClienteGeneral+repCli.getTotal();
							subTotalClienteGeneral=subTotalClienteGeneral+repCli.getSubTotal();
							ivaClienteGeneral=ivaClienteGeneral+repCli.getIva();
							ivaClienteCeroGeneral=ivaClienteCeroGeneral+repCli.getIva0();
							retencionClienteIVA=retencionClienteIVA+repCli.getRetencionIVA();
							retencionClienteGeneralIVA=retencionClienteGeneralIVA+repCli.getRetencionIVA();
							retencionClienteRENTA=retencionClienteRENTA+repCli.getRetencionRENTA();
							retencionClienteGeneralRENTA=retencionClienteGeneralRENTA+repCli.getRetencionRENTA();
							numFacturasEmitidas++;
						}	
						numRegRepetidos++;
					}
					else
					{
						if (numRegRepetidos>-1)//DEJAR COMO MAYOR A 0 EN EL CASO DE QUE SI HAYAN REPETIDOS 
						{	
							ReporteClientesDTO repCliAux = new ReporteClientesDTO();
							repCliAux.setCliente(nomCliente);
							repCliAux.setCedulaRuc(cedCliente);
							totalCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, totalCliente);
							ivaCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, ivaCliente);
							ivaClienteCero=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, ivaClienteCero);
							subTotalCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, subTotalCliente);
							retencionCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionCliente);
							retencionClienteIVA=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionClienteIVA);
							retencionClienteRENTA=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionClienteRENTA);
							descuentoCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, descuentoCliente);

							repCliAux.setTotal(totalCliente);
							repCliAux.setIva(ivaCliente);
							repCliAux.setIva0(ivaClienteCero);
							repCliAux.setNumRealTransaccion(numFacturasEmitidas);
							repCliAux.setSubtotal(subTotalCliente);
							repCliAux.setAutorizacion("TOTALES:");
							repCliAux.setFecha("************");
							repCliAux.setDescuento(descuentoCliente);
							repCliAux.setRetencion(retencionCliente);
							repCliAux.setRetencionIVA(retencionClienteIVA);
							repCliAux.setRetencionRENTA(retencionClienteRENTA);
							
							linkLstRep.add(repCliAux);
							numFacturasEmitidas=0;
							nomCliente=nomClienteAux;
							cedCliente=cedClienteAux;
							if(repCli.getEstado().equals("CONFIRMADO"))
							{
								totalCliente=repCli.getTotal();
								subTotalCliente=repCli.getSubTotal();
								ivaCliente=repCli.getIva();
								ivaClienteCero=repCli.getIva0();
								retencionClienteIVA=repCli.getRetencionIVA();
								retencionClienteRENTA=repCli.getRetencionRENTA();
								descuentoCliente=repCli.getDescuento();
								retencionCliente=repCli.getRetencion();
								
								numFacturasEmitidas++;
								totalClienteGeneral=totalClienteGeneral+repCli.getTotal();
								subTotalClienteGeneral=subTotalClienteGeneral+repCli.getSubTotal();
								ivaClienteGeneral=ivaClienteGeneral+repCli.getIva();
								ivaClienteCeroGeneral=ivaClienteCeroGeneral+repCli.getIva0();
								//descuentoCliente=descuentoCliente+repCli.getDescuento();
								//retencionCliente=retencionCliente+repCli.getRetencion();
								descuentoClienteGeneral=descuentoClienteGeneral+repCli.getDescuento();
								retencionClienteGeneral=retencionClienteGeneral+repCli.getRetencion();
								
								retencionClienteGeneralIVA=retencionClienteGeneralIVA+repCli.getRetencionIVA();
								
								retencionClienteGeneralRENTA=retencionClienteGeneralRENTA+repCli.getRetencionRENTA();

			
							}
							else
							{
								totalCliente=0;
								subTotalCliente=0;
								ivaCliente=0;
								ivaClienteCero=0;
								descuentoCliente=0;
								retencionCliente=0;
								retencionClienteIVA=0;
								retencionClienteRENTA=0;
								
							}	
							numRegRepetidos=0;
						}//fin if numrepetidos > 0
						else
						{
							nomCliente=nomClienteAux;
							cedCliente=cedClienteAux;
							if(repCli.getEstado().equals("CONFIRMADO"))
							{
								totalCliente=repCli.getTotal();
								subTotalCliente=repCli.getSubTotal();
								ivaCliente=repCli.getIva();
								ivaClienteCero=repCli.getIva0();
								retencionClienteIVA=repCli.getRetencionIVA();
								retencionClienteRENTA=repCli.getRetencionRENTA();
								retencionCliente=repCli.getRetencion();
								descuentoClienteGeneral=repCli.getDescuento();
								
								totalClienteGeneral=totalClienteGeneral+repCli.getTotal();
								subTotalClienteGeneral=subTotalClienteGeneral+repCli.getSubTotal();
								ivaClienteGeneral=ivaClienteGeneral+repCli.getIva();
								ivaClienteCeroGeneral=ivaClienteCeroGeneral+repCli.getIva0();
								descuentoCliente=descuentoCliente+repCli.getDescuento();
								//retencionCliente=retencionCliente+repCli.getRetencion();
								//descuentoClienteGeneral=descuentoClienteGeneral+repCli.getDescuento();
								retencionClienteGeneral=retencionClienteGeneral+repCli.getRetencion();
								//retencionClienteIVA=retencionClienteIVA+repCli.getRetencionIVA();
								retencionClienteGeneralIVA=retencionClienteGeneralIVA+repCli.getRetencionIVA();
								//retencionClienteRENTA=retencionClienteRENTA+repCli.getRetencionRENTA();
								retencionClienteGeneralRENTA=retencionClienteGeneralRENTA+repCli.getRetencionRENTA();			
							}
							else
							{
								totalCliente=0;
								subTotalCliente=0;
								ivaCliente=0;
								ivaClienteCero=0;
								descuentoCliente=0;
								retencionCliente=0;
								retencionClienteIVA=0;
								retencionClienteRENTA=0;
							}
							numRegRepetidos=0;
						}	
					}
				}
				linkLstRep.add(repCli);				
			}//fin del for
			
		//	if(numRegRepetidos>0)
		//	{
				ReporteClientesDTO repCliAux = new ReporteClientesDTO();
				totalCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, totalCliente);
				ivaCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, ivaCliente);
				ivaClienteCero=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, ivaClienteCero);
				subTotalCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, subTotalCliente);
				retencionCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionCliente);
				retencionClienteIVA=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionClienteIVA);
				retencionClienteRENTA=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, retencionClienteRENTA);
				descuentoCliente=CValidarDato.getDecimal(Factum.banderaNumeroDecimales, descuentoCliente);
				//repCliAux.setCliente(nomCliente);
				repCliAux.setCliente(nomCliente);
				repCliAux.setCedulaRuc(cedCliente);
				repCliAux.setTotal(totalCliente);
				repCliAux.setIva(ivaCliente);
				repCliAux.setIva0(ivaClienteCero);
				repCliAux.setSubtotal(subTotalCliente);
				repCliAux.setRetencion(retencionCliente);
				repCliAux.setDescuento(descuentoCliente);
				
				repCliAux.setNumRealTransaccion(numFacturasEmitidas);
				repCliAux.setFecha("************");
				repCliAux.setAutorizacion("TOTALES:");
				repCliAux.setRetencionIVA(retencionClienteIVA);
				repCliAux.setRetencionRENTA(retencionClienteRENTA);
				linkLstRep.add(repCliAux);
				
				
				
		//	}	

			
			ListGridRecord[] listadoCliente = new ListGridRecord[linkLstRep.size()];
			for (int a=0;a<linkLstRep.size();a++)
			{
				listadoCliente[a] = new ListGridRecord();
				listadoCliente[a].setAttribute("Cliente", linkLstRep.get(a).getCliente());
				listadoCliente[a].setAttribute("RUC", linkLstRep.get(a).getCedulaRuc());
				listadoCliente[a].setAttribute("Autorizacion", linkLstRep.get(a).getAutorizacion());
				listadoCliente[a].setAttribute("Fecha", linkLstRep.get(a).getfecha());
				listadoCliente[a].setAttribute("Estado", linkLstRep.get(a).getEstado());
				listadoCliente[a].setAttribute("Iva", formatoDecimalN.format(linkLstRep.get(a).getIva()));
				listadoCliente[a].setAttribute("Iva0", formatoDecimalN.format(linkLstRep.get(a).getIva0()));
//				listadoCliente[a].setAttribute("Iva", linkLstRep.get(a).getIva());
//				listadoCliente[a].setAttribute("Iva0", linkLstRep.get(a).getIva0());
				listadoCliente[a].setAttribute("NumRealTransaccion", linkLstRep.get(a).getNumRealTransaccion());
				listadoCliente[a].setAttribute("SubTotal", formatoDecimalN.format(linkLstRep.get(a).getSubTotal()));
				listadoCliente[a].setAttribute("Total", formatoDecimalN.format(linkLstRep.get(a).getTotal()));
				listadoCliente[a].setAttribute("retencion", formatoDecimalN.format(linkLstRep.get(a).getRetencion()));
				listadoCliente[a].setAttribute("Descuento", formatoDecimalN.format(linkLstRep.get(a).getDescuento()));
				listadoCliente[a].setAttribute("retencionIVA", formatoDecimalN.format(linkLstRep.get(a).getRetencionIVA()));
				listadoCliente[a].setAttribute("retencionRENTA", formatoDecimalN.format(linkLstRep.get(a).getRetencionRENTA()));
//				listadoCliente[a].setAttribute("SubTotal", linkLstRep.get(a).getSubTotal());
//				listadoCliente[a].setAttribute("Total", linkLstRep.get(a).getTotal());
//				listadoCliente[a].setAttribute("retencion", linkLstRep.get(a).getRetencion());
//				listadoCliente[a].setAttribute("Descuento", linkLstRep.get(a).getDescuento());
//				listadoCliente[a].setAttribute("retencionIVA", linkLstRep.get(a).getRetencionIVA());
//				listadoCliente[a].setAttribute("retencionRENTA", linkLstRep.get(a).getRetencionRENTA());
			}
			lstReporteCaja.setData(listadoCliente);
			lstReporteCaja.setGroupStartOpen(GroupStartOpen.ALL);
		
			lstReporteCaja.show();
			lstReporteCaja.redraw();
			//lstReporteCaja.setCanReorderRecords(false);
			
			
			ListGridRecord[] listadototales = new ListGridRecord[1];
			listadototales[0]=new ListGridRecord();

			listadototales[0].setAttribute("lstSubtNeto",Math.rint(subTotalClienteGeneral*100)/100);
			listadototales[0].setAttribute("lstSubtIVA0",Math.rint(ivaClienteCeroGeneral*100)/100);
			listadototales[0].setAttribute("lstIVA",Math.rint(ivaClienteGeneral*100)/100);
			listadototales[0].setAttribute("lstTOTAL",Math.rint(totalClienteGeneral*100)/100);
			listadototales[0].setAttribute("retencion",Math.rint(retencionClienteGeneral*100)/100);
			listadototales[0].setAttribute("totretencionIVA",Math.rint(retencionClienteGeneralIVA*100)/100);
			listadototales[0].setAttribute("totretencionRENTA",Math.rint(retencionClienteGeneralRENTA*100)/100);
			listadototales[0].setAttribute("Descuento",Math.rint(descuentoClienteGeneral*100)/100);
			ListGridTotales.setData(listadototales);
		//	libro();
		}//fin if >0
		else
		{
			ListGridRecord[] listadoCliente = new ListGridRecord[result.size()];
			lstReporteCaja.setData(listadoCliente);
			lstReporteCaja.redraw();
			lstReporteCaja.show();
			SC.say("No hay resultados que cumplan con los par&aacute;metros establecidos");
		}
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
	    }
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
	public void libro(){
		ListGridRecord[] listado =lstReporteCaja.getRecords();
		double iva=0,iva0=0,subneto=0,tot=0;
		for(int i=0;i<listado.length;i++){
			if(listado[i].getAttributeAsString("Estado").equals("CONFIRMADO")){
				subneto=subneto+Double.valueOf(listado[i].getAttributeAsString("SubTotal"));
				iva0=iva0+Double.valueOf(listado[i].getAttributeAsString("Iva0"));
				iva=iva+Double.valueOf(listado[i].getAttributeAsString("Iva"));
			}
			
		}
		tot=subneto+iva0+iva;
		ListGridRecord[] listadototales = new ListGridRecord[1];
			listadototales[0]=new ListGridRecord();
			
			listadototales[0].setAttribute("lstSubtNeto",Math.rint(subneto*100)/100);
			listadototales[0].setAttribute("lstSubtIVA0",Math.rint(iva0*100)/100);
			listadototales[0].setAttribute("lstIVA",Math.rint(iva*100)/100);
			listadototales[0].setAttribute("lstTOTAL",Math.rint(tot*100)/100);
			ListGridTotales.setData(listadototales);	
	}
 	final AsyncCallback<ReportesDTO>  objbackTotal=new AsyncCallback<ReportesDTO>(){
 		public void onFailure(Throwable caught) {
 			SC.say("Error dado:" + caught.toString());
 			
 		}
 		@Override
		public void onSuccess(ReportesDTO result) { 
 			ReportesDTO rep=result;
 			ListGridRecord[] listado = new ListGridRecord[1];
 			listado[0]=new ListGridRecord();
 			//,,,}
 			
 			listado[0].setAttribute("lstSubtNeto",Math.rint(rep.getSubtNeto()*100)/100);
 			listado[0].setAttribute("lstSubtIVA0",Math.rint(rep.getSubtIVA0()*100)/100);
 			listado[0].setAttribute("lstIVA",Math.rint(rep.getIVA()*100)/100);
 			listado[0].setAttribute("lstTOTAL",Math.rint(rep.getTOTAL()*100)/100);
 			ListGridTotales.setData(listado);
 			listado[0].getAttributes();
 			
 			//getService().listGridToExel(listado, objbackString);
		}
 	};  
 	final AsyncCallback<String>  objbackString=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
		public void onSuccess(String result) {
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
			SC.say(result);
		}
	};
 	public void cargaInicial(){
 		try{
	 		Date fechaActual = new Date();
	 		String fechaA = fechaActual.toString();
	 		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
			getService().listarFacturas(fechaA, fechaA,"0", listaCallback);
			/*getService().ReproteTotalIVADocumentos(fechaA, fechaA,"0", objbackIva);
			getService().ReproteTotalSinIVADocumentos(fechaA, fechaA,"0", objbackIva0);
			getService().ReproteSubTotalDocumentos(fechaA, fechaA,"0", objbackSub);*/
	 		SC.say("ENTRO A LA CARGA INICIAL: "+fechaA);}
 		catch(Exception e){
 			SC.say("ERROR: "+e.getMessage());
 		}
 	}
}
