package com.giga.factum.client;

import java.util.Date;

import com.giga.factum.client.DTO.CreateExelDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.DateItemSelectorFormat;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateTimeItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;

public class FrmIVA extends VLayout{
	DynamicForm dynamicForm = new DynamicForm();
	//DynamicForm dynamicForm2 = new DynamicForm();
	HStack hStackSuperior = new HStack();
	
	IButton btnGenerarReporte = new IButton("Actualizar IVA");
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	Date fechaFactura;// Para la fecha del sistema
	
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public FrmIVA() 
	{
	try
		{
			
//			txtProveedor.addChangedHandler(new ManejadorBotones("persona"));
			HStack hStackEspacio = new HStack();
			hStackEspacio.setSize("4%", "20%");
			HStack hStackEspacio1 = new HStack();
			hStackEspacio1.setSize("4%", "20%");
		
			
			hStackSuperior.setSize("100%", "35%");
			hStackSuperior.addMember(dynamicForm);
			hStackSuperior.addMember(hStackEspacio);
			hStackSuperior.addMember(hStackEspacio1);
//hStackSuperior.addMember(btnGrabar);		
			VStack vStackFinal = new VStack();
			vStackFinal.setSize("10%", "100%");
			HStack hStackSup1 = new HStack();
			hStackSup1.setSize("100%", "35%");
//	hStackSup1.setBackgroundColor("blue");
			HStack hStackSup2 = new HStack();
			hStackSup2.setSize("100%", "35%");
//		hStackSup2.setBackgroundColor("red");
		
			vStackFinal.addMember(hStackSup1);
			vStackFinal.addMember(hStackSup2);
			hStackSuperior.addMember(vStackFinal);
			addMember(hStackSuperior);
//++++++++++++++++++++++++   LISTADO GRANDE   ++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		
		HStack hStack = new HStack();
		hStack.setSize("100%", "5%");
		hStack.addMember(btnGenerarReporte);;
		addMember(hStack);
		
		
		btnGenerarReporte.setDisabled(false);
		btnGenerarReporte.addClickHandler(new ClickHandler(){
	           public void onClick(ClickEvent event){  
	        	   RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
		       				"http://localhost/FACTUM/ivaCatorce.php?fechaInicial=2016/01/01");
		       		try {
		       			rb.sendRequest(null, new RequestCallback() {
		       				public void onError(
		       						final com.google.gwt.http.client.Request request,
		       						final Throwable exception) {
		       					// Window.alert(exception.getMessage());
		       					SC.say("Error al actualizar el iva a 14%: "
		       							+ exception);
		       				}

		       				public void onResponseReceived(
		       						final com.google.gwt.http.client.Request request,
		       						final Response response) {
		       					// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
		       					// LA GRILLA
		       					SC.say("Actulizado con exito");
		       				}

		       			});
		       		} catch (final Exception e) {
		       			// Window.alert(e.getMessage());
		       		}
	        	   }
	       });
		}
	catch(Exception e)
	{
		}
}
	
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
		}
}