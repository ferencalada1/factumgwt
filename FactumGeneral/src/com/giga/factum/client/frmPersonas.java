package com.giga.factum.client;


import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.SelectItem;
/**
 * Pantalla para Mantenimiento de Personas
 * @author Israel Pes�ntez
 *
 */
public class frmPersonas extends VLayout{
	String pantalla="";
	DynamicForm dynamicForm_1 = new DynamicForm();
	public frmPersonas() {
		setSize("910", "600");
		dynamicForm_1.setSize("100%", "5%");
		final SelectItem cbmPersonas=new SelectItem("cbmPersonas", "Seleccione un tipo de Persona:");
		cbmPersonas.setValueMap("Cliente","Proveedor","Usuario");
		dynamicForm_1.setFields(new FormItem[] { cbmPersonas });
		addMember(dynamicForm_1);
		cbmPersonas.addChangedHandler(new ChangedHandler() {  
        	public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
        		pantalla=cbmPersonas.getValue().toString();
        		if(pantalla.equals("Usuario")){
        			frmEmpleado empleado =new frmEmpleado();
        			empleado.setSize("100%","95%");
        			setMembers(dynamicForm_1);
        			addMember(empleado);
        		}else if(pantalla.equals("Proveedor")){
        			frmProveedor2 proveedor=new frmProveedor2();
        			proveedor.setSize("100%","95%");
        			setMembers(dynamicForm_1);
        			addMember(proveedor);
        			
        		}	else if(pantalla.equals("Cliente")){
        			frmCliente2 cliente=new frmCliente2();
        			cliente.setSize("100%","95%");
        			setMembers(dynamicForm_1);
        			addMember(cliente);
        		}
    		}  
        });  
	}
}
