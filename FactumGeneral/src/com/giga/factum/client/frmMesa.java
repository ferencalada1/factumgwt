package com.giga.factum.client;

import java.util.LinkedHashMap;
import java.util.List;
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.layout.HLayout;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.SearchForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.form.fields.PickerIcon;  
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;  
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.TransferImgButton;  
import com.smartgwt.client.widgets.grid.ListGridRecord;  
import com.smartgwt.client.widgets.layout.HStack;  
import com.giga.factum.client.DTO.AreaDTO;
import com.giga.factum.client.DTO.MesaDTO;
import com.giga.factum.client.DTO.UnidadDTO;
import com.giga.factum.client.regGrillas.MesaRecords;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;


public class frmMesa extends VLayout {
	DynamicForm dynamicForm = new DynamicForm();
	//SearchForm dynamicForm_buscar = new SearchForm();
	SearchForm searchForm = new SearchForm();
	final TextItem txtMesa = new TextItem("txtMesa", "Nombre Mesa");
	final ComboBoxItem cmbArea = new ComboBoxItem("cmbArea", "Nombre Area");
	ListGrid lstMesa = new ListGrid();//crea una grilla
	TabSet tabMesa = new TabSet();//sera el que contenga a todas las pesta�as
	Label lblRegisros = new Label("# Registros");
	LinkedHashMap<String, String> MapArea = new LinkedHashMap<String, String>();
	List<AreaDTO> listarea=null;
	IButton btnGrabar ;
	IButton btnModificar;
	IButton btnEliminar;
	
	Boolean ban=false;
	int contador=20;
	int registros=0;
	String estado;
	int idarea=0;
	frmMesa()
	{
		getService().numeroRegistrosMesa("Tblmesa", objbackI);
		
		setSize("910px", "600px");
		
		TextItem txtBuscarLst = new TextItem("txtBuscarLst", "");
		ComboBoxItem cmbBuscar = new ComboBoxItem("cmbBuscar", "");
	 	Tab lTbCuenta_mant = new Tab("Ingreso Mesa");  	          	          	  		 	
	 	VLayout layout = new VLayout(); 		 	
	 	layout.setSize("100%", "100%");
	 	dynamicForm.setSize("100%", "50%");
	 	txtMesa.setShouldSaveValue(true);
	 	txtMesa.setRequired(true);
	 	txtMesa.setTextAlign(Alignment.LEFT);
	 	txtMesa.setDisabled(false);
	 	
	 	
	 	TextItem textidMesa = new TextItem("txtidMesa", "C\u00F3digo Mesa");
	 	textidMesa.setDisabled(true);
	 	textidMesa.setKeyPressFilter("[0-9]");
	 	
	 	 cmbArea.addChangedHandler(new ChangedHandler() 
 		{  
 	    	public void onChanged(ChangedEvent event) 
 	    	{
 	    		idarea=Integer.valueOf((String)event.getValue());
 	    		//SC.say("MARCA: "+idArea);
 	    		}
 			
 	    });
	 	
	 	dynamicForm.setFields(new FormItem[] { textidMesa, txtMesa, cmbArea});
	 	layout.addMember(dynamicForm);
	 	
	 	Canvas canvas = new Canvas();
	 	canvas.setSize("100%", "50%");
	 	
	 	btnGrabar = new IButton("Grabar");
	 	canvas.addChild(btnGrabar);
	 	btnGrabar.moveTo(6, 6);
	 	
	 	IButton btnNuevo = new IButton("Nuevo");
	 	canvas.addChild(btnNuevo);
	 	btnNuevo.moveTo(112, 6);
	 	
	 	btnModificar = new IButton("Modificar");
	 	btnModificar.setDisabled(true);
	 	canvas.addChild(btnModificar);
	 	btnModificar.moveTo(218, 6);
	 	
	 	btnEliminar = new IButton("Eliminar");
	 	btnEliminar.setDisabled(true);
	 	canvas.addChild(btnEliminar);
	 	btnEliminar.moveTo(324, 6);
	 	layout.addMember(canvas);
	 	lTbCuenta_mant.setPane(layout);	
	 	
	 	Tab lTbListado_Mesa = new Tab("Listado");//el tab del listado
		
		VLayout layout_1 = new VLayout();//el vertical layout q contendra a los botones y a la grilla
		/*para poner el picker de buscar*/
		HLayout hLayout = new HLayout();
		hLayout.setSize("100%", "6%");
		PickerIcon searchPicker = new PickerIcon(PickerIcon.SEARCH);
		
		TextItem txtBuscar = new TextItem("buscar", "");  
		txtBuscar.setShowOverIcons(false);
		txtBuscar.setEndRow(false);
		txtBuscar.setAlign(Alignment.LEFT);	          
		txtBuscar.setIcons(searchPicker);
		txtBuscar.setHint("Buscar");
		

		/*para poner en un hstack los botones de desplazamiento*/
		
	 	
		searchForm.setSize("85%", "100%");
		searchForm.setItemLayout(FormLayoutType.ABSOLUTE);
		
		txtBuscarLst.setLeft(6);
		txtBuscarLst.setTop(6);
		txtBuscarLst.setWidth(146);
		txtBuscarLst.setHeight(22);
		txtBuscarLst.setIcons(searchPicker);
		txtBuscarLst.setHint("Buscar");
		txtBuscarLst.setShowHintInField(true);
		
		cmbBuscar.setLeft(158);
		cmbBuscar.setTop(6);
		cmbBuscar.setWidth(146);
		cmbBuscar.setHeight(22);
		cmbBuscar.setHint("Buscar Por");
		cmbBuscar.setShowHintInField(true);
		cmbBuscar.setValueMap("C\u00F3digo","Nombre");
		
		searchForm.setFields(new FormItem[] { txtBuscarLst, cmbBuscar});
		hLayout.addMember(searchForm);
		
		HStack hStack = new HStack();
		hStack.setSize("12%", "100%");
		TransferImgButton btnSiguiente = new TransferImgButton(TransferImgButton.RIGHT);  
        TransferImgButton btnAnterior = new TransferImgButton(TransferImgButton.LEFT);
        TransferImgButton btnInicio = new TransferImgButton(TransferImgButton.LEFT_ALL);
        TransferImgButton btnFin = new TransferImgButton(TransferImgButton.RIGHT_ALL);
        TransferImgButton btnEliminarlst = new TransferImgButton(TransferImgButton.DELETE);
        hStack.addMember(btnInicio);
        hStack.addMember(btnAnterior);
        hStack.addMember(btnSiguiente);
        hStack.addMember(btnFin);
        hStack.addMember(btnEliminarlst);
		hLayout.addMember(hStack);
		layout_1.addMember(hLayout);
		lstMesa.setSize("100%", "80%");
		ListGridField idMesa = new ListGridField("idMesa", "C\u00F3digo");
		ListGridField Mesa = new ListGridField("Mesa", "Nombre");
		ListGridField Area = new ListGridField("Area", "Area");
		ListGridField Estado = new ListGridField("Estado", "Estado");
		ListGridField idArea = new ListGridField("idArea", "idArea");
		idArea.setHidden(true);		
		Estado.setHidden(true);
		lstMesa.setFields(new ListGridField[] {idMesa,Mesa, Estado,idArea,Area});
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarMesa(0, contador, objbacklst);
		layout_1.addMember(lstMesa);//para que se agregue el listado al vertical
	    
	    
	    layout_1.addMember(lblRegisros);
	    lblRegisros.setSize("100%", "4%");
	    lTbListado_Mesa.setPane(layout_1);
		btnNuevo.addClickHandler(new ManejadorBotones("Nuevo"));
		btnGrabar.addClickHandler(new ManejadorBotones("Grabar"));
		btnModificar.addClickHandler(new ManejadorBotones("modificar"));
		btnEliminar.addClickHandler(new ManejadorBotones("eliminar"));
		btnEliminarlst.addClickHandler(new ManejadorBotones("eliminar"));
		btnAnterior.addClickHandler(new ManejadorBotones("left"));                 
		btnInicio.addClickHandler(new ManejadorBotones("left_all"));
        btnFin.addClickHandler(new ManejadorBotones("right_all"));
        btnSiguiente.addClickHandler(new ManejadorBotones("right"));
        lstMesa.addRecordClickHandler(new ManejadorBotones("Seleccionar"));
        lstMesa.addRecordDoubleClickHandler(new ManejadorBotones("Seleccionar"));
        txtBuscarLst.addKeyPressHandler(new ManejadorBotones(""));
        searchPicker.addFormItemClickHandler(new ManejadorBotones(""));
        
	    tabMesa.addTab(lTbCuenta_mant);  
	    tabMesa.addTab(lTbListado_Mesa);//para que agregue el tab con los botones y con la grilla a la segunda pesta�a
		addMember(tabMesa);
		idarea=-1;
		getService().listarAreaCombo(0,1000,objbacklstArea);
  
       
	}
	public void buscarL(){
		String nombre=searchForm.getItem("txtBuscarLst").getDisplayValue().toUpperCase();
		String campo=searchForm.getItem("cmbBuscar").getDisplayValue();
		
		if(campo.equalsIgnoreCase("nombre")||(campo.equalsIgnoreCase(""))){ 
			campo="nombre";
		}else if(campo.equalsIgnoreCase("C\u00F3digo")){
			campo="idMesa";
		}
		//validando que la busqueda no sea il�gica y tenga al menos una opcion v�lida
		 if(nombre.equalsIgnoreCase("Buscar")||(nombre.equalsIgnoreCase(""))){
			 SC.say("Debe especificar una opci\u00F3n v\u00E1lida para comenzar la b\u00FAsqueda");
		 }else if(campo.equals("nombre")||campo.equals("idMesa")){
		DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
		getService().listarMesaLike(nombre, campo, objbacklst);
		 }
	}
	public void limpiar(){
		dynamicForm.setValue("txtMesa", "");  
		dynamicForm.setValue("txtidMesa", "");  
	}
	/**
	 * Manejador de Botones para pantalla Mesa
	 * @author Israel Pes�ntez
	 *
	 */
	private class ManejadorBotones implements ClickHandler,KeyPressHandler,FormItemClickHandler,RecordDoubleClickHandler,RecordClickHandler{
		String indicador="";
		
		ManejadorBotones(String nombreBoton){
			this.indicador=nombreBoton;
		}
		
		public void onClick(ClickEvent event){
			if(indicador.equalsIgnoreCase("Grabar")){
				if(dynamicForm.validate()){
					//llamamos al RPC
					String nombre=dynamicForm.getItem("txtMesa").getDisplayValue();
					AreaDTO areadto=new AreaDTO();
					areadto.setIdEmpresa(Factum.empresa.getIdEmpresa());
					areadto.setEstablecimiento(Factum.getEstablecimientoCero());
					if(idarea!=-1){
					areadto.setIdArea(idarea);
					MesaDTO mesadto=new MesaDTO(Factum.empresa.getIdEmpresa(),Factum.getEstablecimientoCero(),nombre, "0", "mesa.png", "mesa.png", 0.0, 0.0); 
					mesadto.setTblarea(areadto);
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().grabarMesa(mesadto,objback);
					contador=20;
					lstMesa.redraw();
					}else{
						SC.say("Seleccione una Area");
					}
					
				}
					
			}
			
			else if(indicador.equalsIgnoreCase("Listado")){
				
			}
			else if(indicador.equalsIgnoreCase("modificar")){
				if(dynamicForm.validate()){
					AreaDTO areadto=new AreaDTO();
					String nombre=dynamicForm.getItem("txtMesa").getDisplayValue();
					String id=dynamicForm.getItem("txtidMesa").getDisplayValue();
					areadto.setIdEmpresa(Factum.empresa.getIdEmpresa());
					areadto.setEstablecimiento(Factum.getEstablecimientoCero());
					areadto.setIdArea(idarea);
					MesaDTO mesaDTO=new MesaDTO(Factum.empresa.getIdEmpresa(),Factum.getEstablecimientoCero(),Integer.parseInt(id),nombre,estado,"mesa.png", "mesa.png", 0.0, 0.0); 
					mesaDTO.setTblarea(areadto);
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().modificarMesa(mesaDTO,objback);
					
				}
			}else if(indicador.equalsIgnoreCase("Nuevo")){
				limpiar();
				//deshabilita botones despues de ingresar un nuevo
			    btnEliminar.setDisabled(true);
			    btnModificar.setDisabled(true);
			    btnGrabar.setDisabled(false);
			
			}else if(indicador.equalsIgnoreCase("left")){
				if(contador>20){
					contador=contador-20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMesa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}else{
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMesa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("right")){
				if(contador<registros) {
					contador=contador+20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMesa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
					
			}else if(indicador.equalsIgnoreCase("right_all")){
				if(registros>20){
					contador=registros-registros%20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMesa(registros-registros%20,registros, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
				
			}else if(indicador.equalsIgnoreCase("left_all")){
					contador=20;
					DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","block");
					getService().listarMesa(contador-20,contador, objbacklst);
					lblRegisros.setText(contador+" de "+registros);
				}
		}

		@Override
		public void onFormItemClick(FormItemIconClickEvent event) {
			buscarL();			
		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
		
			if(event.getKeyName().equalsIgnoreCase("enter")) {
				buscarL();
			}
			
		}
		/**
		 * Metodo que controlo el click en la grilla
		 */
		public void onRecordClick(RecordClickEvent event) {
			
		}

		/**
		 * Metodo que controlo el doubleclick en la grilla
		 */
		public void onRecordDoubleClick(RecordDoubleClickEvent event) {
			if(indicador.equalsIgnoreCase("Seleccionar")){
				//SC.say(message)
				dynamicForm.setValue("txtMesa",lstMesa.getSelectedRecord().getAttribute("Mesa"));  
				dynamicForm.setValue("txtidMesa", lstMesa.getSelectedRecord().getAttribute("idMesa"));
				estado=lstMesa.getSelectedRecord().getAttributeAsString("Estado");
				idarea=lstMesa.getSelectedRecord().getAttributeAsInt("idArea");
				cmbArea.setValue(idarea);
				tabMesa.selectTab(0);
				
				//habilito despues de hacer doble clic sobre el registro deseado
				btnEliminar.setDisabled(false);
				btnModificar.setDisabled(false);
				btnGrabar.setDisabled(true);
				
			}
		}
	}
		
	
	/*private class ManejadorTabs implements fireEvent{
		Specified by: addTabSelectedHandler(...) in HasTabSelectedHandlers

	}*/
	
	
	final AsyncCallback<String>  objback=new AsyncCallback<String>(){
		public void onFailure(Throwable caught) {
			SC.say(caught.toString());
		}
		public void onSuccess(String result) {
			SC.say(result);
			getService().listarMesa(0,20, objbacklst);
			contador=20;
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
		}
	};
	/**
	 * LLamada asincrona para buscar un objeto
	 */
	final AsyncCallback<MesaDTO>  objbackMesa=new AsyncCallback<MesaDTO>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			
		}
		@Override
		public void onSuccess(MesaDTO result) {
			// TODO Auto-generated method stub
			if(result!=null){
				dynamicForm.setValue("txtMesa", result.getNombreMesa());  
				dynamicForm.setValue("txtIdMesa", result.getIdMesa());  
				SC.say("Elemento  encontrado");
			}else{
				SC.say("Elemento no encontrado");
			}
		}
	};
	/**
	 * LLamada asincrona para construir una grilla
	 */
	final AsyncCallback<List<MesaDTO>>  objbacklst=new AsyncCallback<List<MesaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
		}
		public void onSuccess(List<MesaDTO> result) {
			getService().numeroRegistrosMesa("Tblmesa", objbackI);
			if(contador>registros){
				lblRegisros.setText(registros+" de "+registros);
			}else
				lblRegisros.setText(contador+" de "+registros);
			ListGridRecord[] listado = new ListGridRecord[result.size()];
			for(int i=0;i<result.size();i++) {
				listado[i]=(new MesaRecords((MesaDTO)result.get(i)));
				
            }
			lstMesa.setData(listado);
			DOM.setStyleAttribute(RootPanel.get("cargando").getElement(),"display","none");
            
		}
	};
	final AsyncCallback<Integer>  objbackI=new AsyncCallback<Integer>(){
		public void onFailure(Throwable caught) {
			SC.say("Error dado:" + caught.toString());
		}
		public void onSuccess(Integer result) {
			registros=result;
			
		}
	};
	
	final AsyncCallback<List<AreaDTO>>  objbacklstArea=new AsyncCallback<List<AreaDTO>>(){

		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			SC.say(caught.toString());
			SC.say("Error no se conecta  a la base");
		}

		public void onSuccess(List<AreaDTO> result) {
			listarea=result;
			MapArea.clear();
			MapArea.put("","");
            for(int i=0;i<result.size();i++) {
            	MapArea.put(String.valueOf(result.get(i).getIdArea()), 
						result.get(i).getNombre());
			}
            cmbArea.setValueMap(MapArea); 
		}
	};
	public static GreetingServiceAsync getService(){
		return GWT.create(GreetingService.class);
	}
}
