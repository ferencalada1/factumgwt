package com.giga.factum.client;

import java.util.HashMap;
import java.util.Map;

import com.giga.factum.client.DTO.DtocomercialDTO;
import com.giga.factum.client.DTO.RetencionDTO;
import com.giga.factum.client.mappers.DtocomercialJSON;
import com.giga.factum.client.mappers.RetencionJSON;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Set;

public class frmRetencionCompra extends VLayout{
	 public frmRetencionCompra(String id, String vend,int indTipo){
		 HTMLPane htmlPane = new HTMLPane();
			htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Retencion/Retencion/page");
			//?id="+id
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", id);
			map.put("vend", vend);
			map.put("indicador", String.valueOf(indTipo));
			htmlPane.setContentsURLParams(map);
			htmlPane.setContentsType(ContentsType.PAGE);
			addMember(htmlPane );
	 }
	 public frmRetencionCompra(String id,int indTipo){
		 ////com.google.gwt.user.client.Window.alert("EN frmretencioncompra "+id);
		 getService().getDtoID(Integer.valueOf(id), callbackDoc);
		 
	 }
	 
	 public static GreetingServiceAsync getService() {
			return GWT.create(GreetingService.class);
		}
	 
	 final AsyncCallback<DtocomercialDTO> callbackDoc = new AsyncCallback<DtocomercialDTO>() {

			public void onSuccess(DtocomercialDTO result) {
//				//com.google.gwt.user.client.Window.alert("RESULT retenciones "+result.getIdDtoComercial());
				Set<RetencionDTO> retenciones=result.getTblretencionsForIdDtoComercial();
//				//com.google.gwt.user.client.Window.alert("TAMANO RETENCIONES0 "+retenciones.size());
				DtocomercialJSON dtocomercial=JavaScriptObject.createObject().cast();
				dtocomercial.setValues(result);
//				//com.google.gwt.user.client.Window.alert("TAMANO RETENCIONES "+retenciones.size());
				JsArray<RetencionJSON> array = JavaScriptObject.createArray().cast();
//				//com.google.gwt.user.client.Window.alert("ARRAY JSON");
				for (RetencionDTO retencion:retenciones){
					RetencionJSON retencionJson=JavaScriptObject.createObject().cast();
//					//com.google.gwt.user.client.Window.alert("ARRAY JSON EN FOR");
					retencion.setTbldtocomercialByIdDtoComercial(result);
					retencionJson.setValues(retencion);
//					//com.google.gwt.user.client.Window.alert("ARRAY JSON EN FOR VALUES");
					array.push(retencionJson);
//					//com.google.gwt.user.client.Window.alert("ARRAY JSON AGREGADO A ARRAY");
				}
//				//com.google.gwt.user.client.Window.alert("ARRAY JSON FUERA FOR");
//				String doc=new JSONObject(dtocomercial).toString();
				String doc = stringfy(dtocomercial);
//				//com.google.gwt.user.client.Window.alert("ARRAY JSON CREADO DOC");
//				String lista=new JSONObject(array).toString();
				String lista=stringfy(array);
//				//com.google.gwt.user.client.Window.alert("ARRAY JSON CREADA LISTA");

				RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,
						"http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Retencion/Retencion/datosretencionvista");
				try {
					rb.setHeader("Access-Control-Allow-Origin", "*");
				    rb.setHeader("Access-Control-Allow-Methods", "POST, GET");
				    rb.setHeader("Access-Control-Max-Age", "3600");
				    rb.setHeader("Access-Control-Allow-Headers", "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age");
					rb.sendRequest(lista+"//"+doc, new RequestCallback() {
						public void onError(
								final com.google.gwt.http.client.Request request,
								final Throwable exception) {
							// Window.alert(exception.getMessage());
							SC.say("Error al imprimir verifique el estado de la impresora: "
									+ exception);
						}

						public void onResponseReceived(
								final com.google.gwt.http.client.Request request,
								final Response response) {
							// AQUI PONEMOS EL FOCO EN LA LINEA DE CODIGO DE BARRAS DE
							// LA GRILLA
							
//							grdProductos.startEditing(0, 2, false);
//							if (response.getStatusCode()!=200){
//								SC.say(response.getStatusText());
//							}else{
//								SC.say(response.getStatusText());
//										//+": "+response.getText());
//							}
//							SC.say(response.getStatusText()
//									+" texto: "+response.getText()
//									);
							//response.
							HTMLPane htmlPane = new HTMLPane();
							htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Retencion/Retencion/ventanaretencionvista");
							htmlPane.setContentsType(ContentsType.PAGE);
							addMember(htmlPane);
						}

					});
				} catch (final Exception e) {
					SC.say("Error "+e);
				}
//				JsonUtils.stringify(dtocomercial);
//				HTMLPane htmlPane = new HTMLPane();
//				htmlPane.setContentsURL("http://"+Factum.banderaIpServidor+":"+Factum.banderaPuertoServidor+"/Retencion/Retencion/page");
//				//?id="+id
//				Map<String, String> map = new HashMap<String, String>();
//				map.put("id", id);
//				map.put("vend", "0");
//				map.put("indicador", String.valueOf(indTipo));
//				htmlPane.setContentsURLParams(map);
//				htmlPane.setContentsType(ContentsType.PAGE);
//				addMember(htmlPane);
			}

			public void onFailure(Throwable caught) {
				SC.say("No se puede cargar el documento solicitado");
			}
			
		};
		

		private native String stringfy(JavaScriptObject obj) /*-{
		  return JSON.stringify(obj);
		}-*/;
}
